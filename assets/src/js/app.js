import * as $ from 'jquery'
window.$ = $

import 'bootstrap'
import 'selectize'
import 'air-datepicker'
import './../../node_modules/air-datepicker/dist/js/i18n/datepicker.en.js'
import 'phoenix_html'
import './core'
import './components/editor'
