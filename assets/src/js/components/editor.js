import 'summernote';
import 'summernote/dist/summernote-lite.css';

$(document).ready(function() {

  const els = $('.summernote');
  if (els.length > 0) {
    for (var i = 0; i < els.length; i++) {
      initEditor(els[i]);
    }
  }

  function initEditor(element) {
    let editor = $(element).summernote({
      height: 500,
      callbacks: {
        onImageUpload: (files) => {
          const url = $(element).attr("data-url") || "";
          [...files].forEach((file) => {
            saveToServer(file, url);
          })
        }
      }
    });

    editor.on('text-change', function() {
      var el = document.getElementById(e.getAttribute('data-input-id'))
      if (el) {
        el.value = editor.root.innerHTML
      }
    })

    // Quill upload image
    function selectLocalImage() {
      const input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.click();

      // Listen upload local image and save to server
      input.onchange = () => {
        const file = input.files[0];

        // file type is only image.
        if (/^image\//.test(file.type)) {
          saveToServer(file);
        } else {
          console.warn('You could only upload images.');
        }
      };
    }

    /**
     * Step2. save to server
     *
     * @param {File} file
     */
    function saveToServer(file, url) {
      var csrf = $("meta[name='csrf']").attr('content');

      const fd = new FormData();
      fd.append('path', file);
      fd.append('X-CSRF-Token', csrf);

      const xhr = new XMLHttpRequest();
      xhr.open('POST', url, true);
      xhr.onload = () => {
        if (xhr.status === 200) {
          // this is callback data: url
          const url = JSON.parse(xhr.responseText).data.url;
          insertToEditor(url);
        } else {
          alert('Cannot upload photo');
        }
      };
      xhr.send(fd);
    }

    /**
     * Step3. insert image url to rich editor.
     *
     * @param {string} url
     */
    function insertToEditor(url) {
      // push image url to rich editor.
      editor.summernote('insertImage', url, null);
    }

    // quill editor add image handler
    // editor.getModule('toolbar').addHandler('image', () => {
    //   selectLocalImage();
    // });
  }
})


// export default { init: initEditor };
