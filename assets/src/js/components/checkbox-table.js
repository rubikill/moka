function checkboxTable(container) {
  var action = container.querySelector("select[data-toggle=checkbox-table-action]");
  var submit = container.querySelector("button[data-toggle=checkbox-table-submit]");
  submit.setAttribute("formaction", action.value);

  action.addEventListener('input', function() {
    container.setAttribute("action", this.value)
  });

  container.addEventListener("submit", function (e) {
    e.preventDefault();
    container.querySelectorAll("input[data-toggle=checkbox-table-input]").forEach(function (input) {
      if (input.checked) {
        input.setAttribute("name", input.getAttribute("data-name"));
      }
    });
    if (container.action) {
      container.submit();
    }
  })

  return {};
}

export default checkboxTable;