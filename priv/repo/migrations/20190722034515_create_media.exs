defmodule Moka.Repo.Migrations.CreateMedia do
  use Ecto.Migration

  def up do
    execute "CREATE SCHEMA common;"

    create table(:media, prefix: :common) do
      add :path, :text
      add :filename, :text
      add :content_type, :text

      timestamps()
    end
  end

  def down do
    drop table(:media, prefix: :common)

    execute "DROP SCHEMA common;"
  end
end
