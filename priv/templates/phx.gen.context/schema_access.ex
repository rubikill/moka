
  alias <%= inspect schema.module %>

  @doc """
  Returns the list of <%= schema.plural %>.

  ## Examples

      iex> list_<%= schema.plural %>()
      [%<%= inspect schema.alias %>{}, ...]

  """
  def list_<%= schema.plural %>(params \\ %{})

  def list_<%= schema.plural %>(params) do
    from(i in <%= inspect schema.alias %>)
    |> Moka.Filter.apply(params)
    |> Repo.all()
  end

  @doc """
  """
  def preload_<%= schema.plural %>(<%= schema.plural %>, fields \\ [])

  def preload_<%= schema.plural %>(<%= schema.plural %>, fields) when is_list(<%= schema.plural %>) do
    Repo.preload(<%= schema.plural %>, fields)
  end

  def preload_<%= schema.plural %>(<%= schema.plural %>, _fields) do
    <%= schema.plural %>
  end

  @doc """
  """
  def preload_<%= schema.singular %>(<%= schema.singular %>, fields \\ [])

  def preload_<%= schema.singular %>(%<%= inspect schema.module %>{} = <%= schema.singular %>, fields) do
    Repo.preload(<%= schema.singular %>, fields)
  end

  def preload_<%= schema.singular %>(<%= schema.singular %>, _fields) do
    <%= schema.singular %>
  end

  @doc """
  """
  def list_<%= schema.plural %>_with_paging(params) do
    from(i in <%= inspect schema.alias %>)
    |> Moka.Filter.apply(params)
    |> Moka.Paginator.new(Repo, params)
  end

  @doc """
  """
  def list_<%= schema.plural %>_with_loadmore(params) do
    from(i in <%= inspect schema.alias %>)
    |> Moka.Filter.apply(params)
    |> Moka.Loadmore.new(Repo, params)
  end

  @doc """
  Gets a single <%= schema.singular %>.

  Raises `Ecto.NoResultsError` if the <%= schema.human_singular %> does not exist.

  ## Examples

      iex> get_<%= schema.singular %>!(123)
      %<%= inspect schema.alias %>{}

      iex> get_<%= schema.singular %>!(456)
      ** (Ecto.NoResultsError)

  """
  def get_<%= schema.singular %>!(id), do: Repo.get!(<%= inspect schema.alias %>, id)

  @doc """
  """
  def get_<%= schema.singular %>(id), do: Repo.get(<%= inspect schema.alias %>, id)

  @doc """
  """
  def get_<%= schema.singular %>_by(params), do: Repo.get_by(<%= inspect schema.alias %>, params)

  @doc """
  """
  def get_<%= schema.singular %>_one(params) do
    from(i in <%= inspect schema.alias %>, limit: 1)
    |> Moka.Filter.apply(params)
    |> Repo.one()
  end

  @doc """
  Creates a <%= schema.singular %>.

  ## Examples

      iex> create_<%= schema.singular %>(%{field: value})
      {:ok, %<%= inspect schema.alias %>{}}

      iex> create_<%= schema.singular %>(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_<%= schema.singular %>(attrs \\ %{}) do
    %<%= inspect schema.alias %>{}
    |> <%= inspect schema.alias %>.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a <%= schema.singular %>.

  ## Examples

      iex> update_<%= schema.singular %>(<%= schema.singular %>, %{field: new_value})
      {:ok, %<%= inspect schema.alias %>{}}

      iex> update_<%= schema.singular %>(<%= schema.singular %>, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>, attrs) do
    <%= schema.singular %>
    |> <%= inspect schema.alias %>.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a <%= inspect schema.alias %>.

  ## Examples

      iex> delete_<%= schema.singular %>(<%= schema.singular %>)
      {:ok, %<%= inspect schema.alias %>{}}

      iex> delete_<%= schema.singular %>(<%= schema.singular %>)
      {:error, %Ecto.Changeset{}}

  """
  def delete_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>) do
    Repo.delete(<%= schema.singular %>)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking <%= schema.singular %> changes.

  ## Examples

      iex> change_<%= schema.singular %>(<%= schema.singular %>)
      %Ecto.Changeset{source: %<%= inspect schema.alias %>{}}

  """
  def change_<%= schema.singular %>(<%= schema.singular %>, params \\ %{})

  def change_<%= schema.singular %>(%<%= inspect schema.alias %>{} = <%= schema.singular %>, params) do
    <%= inspect schema.alias %>.changeset(<%= schema.singular %>, params)
  end
