defmodule <%= inspect context.web_module %>.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use <%= inspect context.web_module %>, :controller
  require Logger

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> assign(:fields, <%= inspect context.web_module %>.ChangesetView.translate_errors(changeset))
    |> render("validation_failed.json")
  end

  def call(conn, {:error, :login_failed}) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> render("login_failed.json")
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> render("404.json")
  end

  def call(conn, {:error, :server_error, err}) do
    Logger.error(err)

    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> render("500.json")
  end

  def call(conn, {:error, :server_error}) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> render("500.json")
  end

  def call(conn, {:error, :validation_failed, changeset = %Ecto.Changeset{}}) do
    call(conn, {:error, changeset})
  end

  def call(conn, {:error, :request_invalid, reason}) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> assign(:message, reason)
    |> render("request_invalid.json")
  end

  def call(conn, {:error, reason}) when is_binary(reason) do
    conn
    |> put_view(<%= inspect context.web_module %>.ErrorView)
    |> assign(:message, reason)
    |> render("request_invalid.json")
  end

  def call(conn, {:ok, data}) do
    json(conn, %{
      status: "OK",
      data: data
    })
  end

  def call(conn, {:ok, data, nil}) do
    json(conn, %{
      status: "OK",
      data: data,
      next_url: nil
    })
  end

  def call(conn, {:ok, data, next_params}) do
    params = Enum.filter(next_params, fn {k, v} -> !is_nil(v) end) |> Enum.into(%{})

    json(conn, %{
      status: "OK",
      data: data,
      next_url: current_url(conn, params)
    })
  end
end
