defmodule <%= inspect context.web_module %>.<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View do
  use <%= inspect context.web_module %>, :view

  def render("<%= schema.plural %>.json", assigns) do
    items = assigns[:<%= schema.plural %>] || []

    fields = MokaWeb.ViewHelper.get_fields(assigns, [])

    Enum.map(items, fn item ->
      item
      |> Map.take(fields)
    end)
  end

  def render("<%= schema.singular %>.json", assigns) do
    item = assigns[:<%= schema.singular %>]

    if is_nil(item) || !Ecto.assoc_loaded?(item) do
      nil
    else
      render("<%= schema.plural %>.json", Map.put(assigns, :<%= schema.plural %>, [item]))
      |> List.first()
    end
  end
end
