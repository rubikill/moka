defmodule <%= inspect context.web_module %>.<%= inspect Module.concat(schema.web_namespace, schema.alias) %>Controller do
  use <%= inspect context.web_module %>, :controller

  alias <%= inspect context.module %>
  alias <%= inspect schema.module %>
  alias <%= inspect context.web_module %>.<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View

  action_fallback <%= inspect context.web_module %>.FallbackController

  def index(conn, params) do
    with {:ok, %{entries: entries, next_params: next_params}} <- <%= inspect context.alias %>.list_<%= schema.plural %>_with_loadmore(params) do

      conn
      |> assign(:<%= schema.plural %>, entries)
      |> MokaWeb.ViewHelper.render_view(<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View, "<%= schema.plural %>.json")
      |> Tuple.append(next_params)
    end
  end

  def create(conn, %{<%= inspect schema.singular %> => <%= schema.singular %>_params}) do
    with {:ok, %<%= inspect schema.alias %>{} = <%= schema.singular %>} <- <%= inspect context.alias %>.create_<%= schema.singular %>(<%= schema.singular %>_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.<%= schema.route_helper %>_path(conn, :show, <%= schema.singular %>))
      |> assign(:<%= schema.singular %>, <%= schema.singular %>)
      |> MokaWeb.ViewHelper.render_view(<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View, "<%= schema.singular %>.json")
    end
  end

  def show(conn, %{"id" => id}) do
    <%= schema.singular %> = <%= inspect context.alias %>.get_<%= schema.singular %>!(id)
    conn
    |> assign(:<%= schema.singular %>, <%= schema.singular %>)
    |> MokaWeb.ViewHelper.render_view(<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View, "<%= schema.singular %>.json")
  end

  def update(conn, %{"id" => id, <%= inspect schema.singular %> => <%= schema.singular %>_params}) do
    <%= schema.singular %> = <%= inspect context.alias %>.get_<%= schema.singular %>!(id)

    with {:ok, %<%= inspect schema.alias %>{} = <%= schema.singular %>} <- <%= inspect context.alias %>.update_<%= schema.singular %>(<%= schema.singular %>, <%= schema.singular %>_params) do
      conn
      |> assign(:<%= schema.singular %>, <%= schema.singular %>)
      |> MokaWeb.ViewHelper.render_view(<%= inspect Module.concat(schema.web_namespace, schema.alias) %>View, "<%= schema.singular %>.json")
    end
  end

  def delete(_conn, %{"id" => id}) do
    <%= schema.singular %> = <%= inspect context.alias %>.get_<%= schema.singular %>!(id)

    with {:ok, %<%= inspect schema.alias %>{}} <- <%= inspect context.alias %>.delete_<%= schema.singular %>(<%= schema.singular %>) do
      {:ok, %{}}
    end
  end
end
