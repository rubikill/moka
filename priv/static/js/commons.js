(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
  ["commons"],
  {
    /***/ "../../node_modules/mini-css-extract-plugin/dist/loader.js?!../../node_modules/css-loader/index.js?!../../node_modules/summernote/dist/summernote-lite.css":
      /*!********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** /home/mofiin/gocode/src/moka/assets/node_modules/mini-css-extract-plugin/dist/loader.js??ref--6-1!/home/mofiin/gocode/src/moka/assets/node_modules/css-loader??ref--6-2!/home/mofiin/gocode/src/moka/assets/node_modules/summernote/dist/summernote-lite.css ***!
  \********************************************************************************************************************************************************************************************************************************************************************************/
      /*! no static exports found */
      /***/ function(module, exports, __webpack_require__) {
        // extracted by mini-css-extract-plugin
        /***/
      },

    /***/ "../../node_modules/style-loader/lib/addStyles.js":
      /*!******************************************************************************************!*\
  !*** /home/mofiin/gocode/src/moka/assets/node_modules/style-loader/lib/addStyles.js ***!
  \******************************************************************************************/
      /*! no static exports found */
      /***/ function(module, exports, __webpack_require__) {
        /*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

        var stylesInDom = {};

        var memoize = function(fn) {
          var memo;

          return function() {
            if (typeof memo === "undefined") memo = fn.apply(this, arguments);
            return memo;
          };
        };

        var isOldIE = memoize(function() {
          // Test for IE <= 9 as proposed by Browserhacks
          // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
          // Tests for existence of standard globals is to allow style-loader
          // to operate correctly into non-standard environments
          // @see https://github.com/webpack-contrib/style-loader/issues/177
          return window && document && document.all && !window.atob;
        });

        var getElement = (function(fn) {
          var memo = {};

          return function(selector) {
            if (typeof memo[selector] === "undefined") {
              var styleTarget = fn.call(this, selector);
              // Special case to return head of iframe instead of iframe itself
              if (styleTarget instanceof window.HTMLIFrameElement) {
                try {
                  // This will throw an exception if access to iframe is blocked
                  // due to cross-origin restrictions
                  styleTarget = styleTarget.contentDocument.head;
                } catch (e) {
                  styleTarget = null;
                }
              }
              memo[selector] = styleTarget;
            }
            return memo[selector];
          };
        })(function(target) {
          return document.querySelector(target);
        });

        var singleton = null;
        var singletonCounter = 0;
        var stylesInsertedAtTop = [];

        var fixUrls = __webpack_require__(
          /*! ./urls */ "../../node_modules/style-loader/lib/urls.js"
        );

        module.exports = function(list, options) {
          if (typeof DEBUG !== "undefined" && DEBUG) {
            if (typeof document !== "object")
              throw new Error(
                "The style-loader cannot be used in a non-browser environment"
              );
          }

          options = options || {};

          options.attrs =
            typeof options.attrs === "object" ? options.attrs : {};

          // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
          // tags it will allow on a page
          if (!options.singleton && typeof options.singleton !== "boolean")
            options.singleton = isOldIE();

          // By default, add <style> tags to the <head> element
          if (!options.insertInto) options.insertInto = "head";

          // By default, add <style> tags to the bottom of the target
          if (!options.insertAt) options.insertAt = "bottom";

          var styles = listToStyles(list, options);

          addStylesToDom(styles, options);

          return function update(newList) {
            var mayRemove = [];

            for (var i = 0; i < styles.length; i++) {
              var item = styles[i];
              var domStyle = stylesInDom[item.id];

              domStyle.refs--;
              mayRemove.push(domStyle);
            }

            if (newList) {
              var newStyles = listToStyles(newList, options);
              addStylesToDom(newStyles, options);
            }

            for (var i = 0; i < mayRemove.length; i++) {
              var domStyle = mayRemove[i];

              if (domStyle.refs === 0) {
                for (var j = 0; j < domStyle.parts.length; j++)
                  domStyle.parts[j]();

                delete stylesInDom[domStyle.id];
              }
            }
          };
        };

        function addStylesToDom(styles, options) {
          for (var i = 0; i < styles.length; i++) {
            var item = styles[i];
            var domStyle = stylesInDom[item.id];

            if (domStyle) {
              domStyle.refs++;

              for (var j = 0; j < domStyle.parts.length; j++) {
                domStyle.parts[j](item.parts[j]);
              }

              for (; j < item.parts.length; j++) {
                domStyle.parts.push(addStyle(item.parts[j], options));
              }
            } else {
              var parts = [];

              for (var j = 0; j < item.parts.length; j++) {
                parts.push(addStyle(item.parts[j], options));
              }

              stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts };
            }
          }
        }

        function listToStyles(list, options) {
          var styles = [];
          var newStyles = {};

          for (var i = 0; i < list.length; i++) {
            var item = list[i];
            var id = options.base ? item[0] + options.base : item[0];
            var css = item[1];
            var media = item[2];
            var sourceMap = item[3];
            var part = { css: css, media: media, sourceMap: sourceMap };

            if (!newStyles[id])
              styles.push((newStyles[id] = { id: id, parts: [part] }));
            else newStyles[id].parts.push(part);
          }

          return styles;
        }

        function insertStyleElement(options, style) {
          var target = getElement(options.insertInto);

          if (!target) {
            throw new Error(
              "Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid."
            );
          }

          var lastStyleElementInsertedAtTop =
            stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

          if (options.insertAt === "top") {
            if (!lastStyleElementInsertedAtTop) {
              target.insertBefore(style, target.firstChild);
            } else if (lastStyleElementInsertedAtTop.nextSibling) {
              target.insertBefore(
                style,
                lastStyleElementInsertedAtTop.nextSibling
              );
            } else {
              target.appendChild(style);
            }
            stylesInsertedAtTop.push(style);
          } else if (options.insertAt === "bottom") {
            target.appendChild(style);
          } else if (
            typeof options.insertAt === "object" &&
            options.insertAt.before
          ) {
            var nextSibling = getElement(
              options.insertInto + " " + options.insertAt.before
            );
            target.insertBefore(style, nextSibling);
          } else {
            throw new Error(
              "[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n"
            );
          }
        }

        function removeStyleElement(style) {
          if (style.parentNode === null) return false;
          style.parentNode.removeChild(style);

          var idx = stylesInsertedAtTop.indexOf(style);
          if (idx >= 0) {
            stylesInsertedAtTop.splice(idx, 1);
          }
        }

        function createStyleElement(options) {
          var style = document.createElement("style");

          options.attrs.type = "text/css";

          addAttrs(style, options.attrs);
          insertStyleElement(options, style);

          return style;
        }

        function createLinkElement(options) {
          var link = document.createElement("link");

          options.attrs.type = "text/css";
          options.attrs.rel = "stylesheet";

          addAttrs(link, options.attrs);
          insertStyleElement(options, link);

          return link;
        }

        function addAttrs(el, attrs) {
          Object.keys(attrs).forEach(function(key) {
            el.setAttribute(key, attrs[key]);
          });
        }

        function addStyle(obj, options) {
          var style, update, remove, result;

          // If a transform function was defined, run it on the css
          if (options.transform && obj.css) {
            result = options.transform(obj.css);

            if (result) {
              // If transform returns a value, use that instead of the original css.
              // This allows running runtime transformations on the css.
              obj.css = result;
            } else {
              // If the transform function returns a falsy value, don't add this css.
              // This allows conditional loading of css
              return function() {
                // noop
              };
            }
          }

          if (options.singleton) {
            var styleIndex = singletonCounter++;

            style = singleton || (singleton = createStyleElement(options));

            update = applyToSingletonTag.bind(null, style, styleIndex, false);
            remove = applyToSingletonTag.bind(null, style, styleIndex, true);
          } else if (
            obj.sourceMap &&
            typeof URL === "function" &&
            typeof URL.createObjectURL === "function" &&
            typeof URL.revokeObjectURL === "function" &&
            typeof Blob === "function" &&
            typeof btoa === "function"
          ) {
            style = createLinkElement(options);
            update = updateLink.bind(null, style, options);
            remove = function() {
              removeStyleElement(style);

              if (style.href) URL.revokeObjectURL(style.href);
            };
          } else {
            style = createStyleElement(options);
            update = applyToTag.bind(null, style);
            remove = function() {
              removeStyleElement(style);
            };
          }

          update(obj);

          return function updateStyle(newObj) {
            if (newObj) {
              if (
                newObj.css === obj.css &&
                newObj.media === obj.media &&
                newObj.sourceMap === obj.sourceMap
              ) {
                return;
              }

              update((obj = newObj));
            } else {
              remove();
            }
          };
        }

        var replaceText = (function() {
          var textStore = [];

          return function(index, replacement) {
            textStore[index] = replacement;

            return textStore.filter(Boolean).join("\n");
          };
        })();

        function applyToSingletonTag(style, index, remove, obj) {
          var css = remove ? "" : obj.css;

          if (style.styleSheet) {
            style.styleSheet.cssText = replaceText(index, css);
          } else {
            var cssNode = document.createTextNode(css);
            var childNodes = style.childNodes;

            if (childNodes[index]) style.removeChild(childNodes[index]);

            if (childNodes.length) {
              style.insertBefore(cssNode, childNodes[index]);
            } else {
              style.appendChild(cssNode);
            }
          }
        }

        function applyToTag(style, obj) {
          var css = obj.css;
          var media = obj.media;

          if (media) {
            style.setAttribute("media", media);
          }

          if (style.styleSheet) {
            style.styleSheet.cssText = css;
          } else {
            while (style.firstChild) {
              style.removeChild(style.firstChild);
            }

            style.appendChild(document.createTextNode(css));
          }
        }

        function updateLink(link, options, obj) {
          var css = obj.css;
          var sourceMap = obj.sourceMap;

          /*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
          var autoFixUrls =
            options.convertToAbsoluteUrls === undefined && sourceMap;

          if (options.convertToAbsoluteUrls || autoFixUrls) {
            css = fixUrls(css);
          }

          if (sourceMap) {
            // http://stackoverflow.com/a/26603875
            css +=
              "\n/*# sourceMappingURL=data:application/json;base64," +
              btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) +
              " */";
          }

          var blob = new Blob([css], { type: "text/css" });

          var oldSrc = link.href;

          link.href = URL.createObjectURL(blob);

          if (oldSrc) URL.revokeObjectURL(oldSrc);
        }

        /***/
      },

    /***/ "../../node_modules/style-loader/lib/urls.js":
      /*!*************************************************************************************!*\
  !*** /home/mofiin/gocode/src/moka/assets/node_modules/style-loader/lib/urls.js ***!
  \*************************************************************************************/
      /*! no static exports found */
      /***/ function(module, exports) {
        /**
         * When source maps are enabled, `style-loader` uses a link element with a data-uri to
         * embed the css on the page. This breaks all relative urls because now they are relative to a
         * bundle instead of the current page.
         *
         * One solution is to only use full urls, but that may be impossible.
         *
         * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
         *
         * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
         *
         */

        module.exports = function(css) {
          // get current location
          var location = typeof window !== "undefined" && window.location;

          if (!location) {
            throw new Error("fixUrls requires window.location");
          }

          // blank or null?
          if (!css || typeof css !== "string") {
            return css;
          }

          var baseUrl = location.protocol + "//" + location.host;
          var currentDir =
            baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

          // convert each url(...)
          /*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
          var fixedCss = css.replace(
            /url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi,
            function(fullMatch, origUrl) {
              // strip quotes (if they exist)
              var unquotedOrigUrl = origUrl
                .trim()
                .replace(/^"(.*)"$/, function(o, $1) {
                  return $1;
                })
                .replace(/^'(.*)'$/, function(o, $1) {
                  return $1;
                });

              // already a full url? no change
              if (
                /^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(
                  unquotedOrigUrl
                )
              ) {
                return fullMatch;
              }

              // convert the url to a full url
              var newUrl;

              if (unquotedOrigUrl.indexOf("//") === 0) {
                //TODO: should we add protocol?
                newUrl = unquotedOrigUrl;
              } else if (unquotedOrigUrl.indexOf("/") === 0) {
                // path should be relative to the base url
                newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
              } else {
                // path should be relative to current directory
                newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
              }

              // send back the fixed url(...)
              return "url(" + JSON.stringify(newUrl) + ")";
            }
          );

          // send back the fixed css
          return fixedCss;
        };

        /***/
      },

    /***/ "../../node_modules/summernote/dist/summernote-lite.css":
      /*!************************************************************************************************!*\
  !*** /home/mofiin/gocode/src/moka/assets/node_modules/summernote/dist/summernote-lite.css ***!
  \************************************************************************************************/
      /*! no static exports found */
      /***/ function(module, exports, __webpack_require__) {
        // style-loader: Adds some css to the DOM by adding a <style> tag

        // load the styles
        var content = __webpack_require__(
          /*! !../../mini-css-extract-plugin/dist/loader.js??ref--6-1!../../css-loader??ref--6-2!./summernote-lite.css */ "../../node_modules/mini-css-extract-plugin/dist/loader.js?!../../node_modules/css-loader/index.js?!../../node_modules/summernote/dist/summernote-lite.css"
        );
        if (typeof content === "string") content = [[module.i, content, ""]];
        // Prepare cssTransformation
        var transform;

        var options = { hmr: true };
        options.transform = transform;
        // add the styles to the DOM
        var update = __webpack_require__(
          /*! ../../style-loader/lib/addStyles.js */ "../../node_modules/style-loader/lib/addStyles.js"
        )(content, options);
        if (content.locals) module.exports = content.locals;
        // Hot Module Replacement
        if (false) {
        }

        /***/
      },

    /***/ "../../node_modules/summernote/dist/summernote.js":
      /*!******************************************************************************************!*\
  !*** /home/mofiin/gocode/src/moka/assets/node_modules/summernote/dist/summernote.js ***!
  \******************************************************************************************/
      /*! no static exports found */
      /***/ function(module, exports, __webpack_require__) {
        /* WEBPACK VAR INJECTION */ (function($, jQuery) {
          /**
           * Super simple wysiwyg editor v0.8.12
           * https://summernote.org
           *
           * Copyright 2013- Alan Hong. and other contributors
           * summernote may be freely distributed under the MIT license.
           *
           * Date: 2019-05-16T08:16Z
           */
          (function(global, factory) {
            true
              ? factory(
                  __webpack_require__(
                    /*! jquery */ "../../node_modules/jquery/dist/jquery.js"
                  )
                )
              : undefined;
          })(this, function($$1) {
            "use strict";

            $$1 = $$1 && $$1.hasOwnProperty("default") ? $$1["default"] : $$1;

            var Renderer = /** @class */ (function() {
              function Renderer(markup, children, options, callback) {
                this.markup = markup;
                this.children = children;
                this.options = options;
                this.callback = callback;
              }
              Renderer.prototype.render = function($parent) {
                var $node = $$1(this.markup);
                if (this.options && this.options.contents) {
                  $node.html(this.options.contents);
                }
                if (this.options && this.options.className) {
                  $node.addClass(this.options.className);
                }
                if (this.options && this.options.data) {
                  $$1.each(this.options.data, function(k, v) {
                    $node.attr("data-" + k, v);
                  });
                }
                if (this.options && this.options.click) {
                  $node.on("click", this.options.click);
                }
                if (this.children) {
                  var $container_1 = $node.find(".note-children-container");
                  this.children.forEach(function(child) {
                    child.render($container_1.length ? $container_1 : $node);
                  });
                }
                if (this.callback) {
                  this.callback($node, this.options);
                }
                if (this.options && this.options.callback) {
                  this.options.callback($node);
                }
                if ($parent) {
                  $parent.append($node);
                }
                return $node;
              };
              return Renderer;
            })();
            var renderer = {
              create: function(markup, callback) {
                return function() {
                  var options =
                    typeof arguments[1] === "object"
                      ? arguments[1]
                      : arguments[0];
                  var children = Array.isArray(arguments[0])
                    ? arguments[0]
                    : [];
                  if (options && options.children) {
                    children = options.children;
                  }
                  return new Renderer(markup, children, options, callback);
                };
              }
            };

            var editor = renderer.create(
              '<div class="note-editor note-frame panel panel-default"/>'
            );
            var toolbar = renderer.create(
              '<div class="note-toolbar panel-heading" role="toolbar"></div></div>'
            );
            var editingArea = renderer.create(
              '<div class="note-editing-area"/>'
            );
            var codable = renderer.create(
              '<textarea class="note-codable" role="textbox" aria-multiline="true"/>'
            );
            var editable = renderer.create(
              '<div class="note-editable" contentEditable="true" role="textbox" aria-multiline="true"/>'
            );
            var statusbar = renderer.create(
              [
                '<output class="note-status-output" aria-live="polite"/>',
                '<div class="note-statusbar" role="status">',
                '  <div class="note-resizebar" role="seperator" aria-orientation="horizontal" aria-label="Resize">',
                '    <div class="note-icon-bar"/>',
                '    <div class="note-icon-bar"/>',
                '    <div class="note-icon-bar"/>',
                "  </div>",
                "</div>"
              ].join("")
            );
            var airEditor = renderer.create('<div class="note-editor"/>');
            var airEditable = renderer.create(
              [
                '<div class="note-editable" contentEditable="true" role="textbox" aria-multiline="true"/>',
                '<output class="note-status-output" aria-live="polite"/>'
              ].join("")
            );
            var buttonGroup = renderer.create(
              '<div class="note-btn-group btn-group">'
            );
            var dropdown = renderer.create(
              '<ul class="dropdown-menu" role="list">',
              function($node, options) {
                var markup = Array.isArray(options.items)
                  ? options.items
                      .map(function(item) {
                        var value =
                          typeof item === "string" ? item : item.value || "";
                        var content = options.template
                          ? options.template(item)
                          : item;
                        var option =
                          typeof item === "object" ? item.option : undefined;
                        var dataValue = 'data-value="' + value + '"';
                        var dataOption =
                          option !== undefined
                            ? ' data-option="' + option + '"'
                            : "";
                        return (
                          '<li role="listitem" aria-label="' +
                          value +
                          '"><a href="#" ' +
                          (dataValue + dataOption) +
                          ">" +
                          content +
                          "</a></li>"
                        );
                      })
                      .join("")
                  : options.items;
                $node.html(markup).attr({ "aria-label": options.title });
              }
            );
            var dropdownButtonContents = function(contents, options) {
              return contents + " " + icon(options.icons.caret, "span");
            };
            var dropdownCheck = renderer.create(
              '<ul class="dropdown-menu note-check" role="list">',
              function($node, options) {
                var markup = Array.isArray(options.items)
                  ? options.items
                      .map(function(item) {
                        var value =
                          typeof item === "string" ? item : item.value || "";
                        var content = options.template
                          ? options.template(item)
                          : item;
                        return (
                          '<li role="listitem" aria-label="' +
                          item +
                          '"><a href="#" data-value="' +
                          value +
                          '">' +
                          icon(options.checkClassName) +
                          " " +
                          content +
                          "</a></li>"
                        );
                      })
                      .join("")
                  : options.items;
                $node.html(markup).attr({ "aria-label": options.title });
              }
            );
            var palette = renderer.create(
              '<div class="note-color-palette"/>',
              function($node, options) {
                var contents = [];
                for (
                  var row = 0, rowSize = options.colors.length;
                  row < rowSize;
                  row++
                ) {
                  var eventName = options.eventName;
                  var colors = options.colors[row];
                  var colorsName = options.colorsName[row];
                  var buttons = [];
                  for (
                    var col = 0, colSize = colors.length;
                    col < colSize;
                    col++
                  ) {
                    var color = colors[col];
                    var colorName = colorsName[col];
                    buttons.push(
                      [
                        '<button type="button" class="note-color-btn"',
                        'style="background-color:',
                        color,
                        '" ',
                        'data-event="',
                        eventName,
                        '" ',
                        'data-value="',
                        color,
                        '" ',
                        'title="',
                        colorName,
                        '" ',
                        'aria-label="',
                        colorName,
                        '" ',
                        'data-toggle="button" tabindex="-1"></button>'
                      ].join("")
                    );
                  }
                  contents.push(
                    '<div class="note-color-row">' + buttons.join("") + "</div>"
                  );
                }
                $node.html(contents.join(""));
                if (options.tooltip) {
                  $node.find(".note-color-btn").tooltip({
                    container: options.container,
                    trigger: "hover",
                    placement: "bottom"
                  });
                }
              }
            );
            var dialog = renderer.create(
              '<div class="modal" aria-hidden="false" tabindex="-1" role="dialog"/>',
              function($node, options) {
                if (options.fade) {
                  $node.addClass("fade");
                }
                $node.attr({
                  "aria-label": options.title
                });
                $node.html(
                  [
                    '<div class="modal-dialog">',
                    '  <div class="modal-content">',
                    options.title
                      ? '    <div class="modal-header">' +
                        '      <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">&times;</button>' +
                        '      <h4 class="modal-title">' +
                        options.title +
                        "</h4>" +
                        "    </div>"
                      : "",
                    '    <div class="modal-body">' + options.body + "</div>",
                    options.footer
                      ? '    <div class="modal-footer">' +
                        options.footer +
                        "</div>"
                      : "",
                    "  </div>",
                    "</div>"
                  ].join("")
                );
              }
            );
            var popover = renderer.create(
              [
                '<div class="note-popover popover in">',
                '  <div class="arrow"/>',
                '  <div class="popover-content note-children-container"/>',
                "</div>"
              ].join(""),
              function($node, options) {
                var direction =
                  typeof options.direction !== "undefined"
                    ? options.direction
                    : "bottom";
                $node.addClass(direction);
                if (options.hideArrow) {
                  $node.find(".arrow").hide();
                }
              }
            );
            var checkbox = renderer.create(
              '<div class="checkbox"></div>',
              function($node, options) {
                $node.html(
                  [
                    "<label" +
                      (options.id ? ' for="' + options.id + '"' : "") +
                      ">",
                    ' <input role="checkbox" type="checkbox"' +
                      (options.id ? ' id="' + options.id + '"' : ""),
                    options.checked ? " checked" : "",
                    ' aria-checked="' +
                      (options.checked ? "true" : "false") +
                      '"/>',
                    options.text ? options.text : "",
                    "</label>"
                  ].join("")
                );
              }
            );
            var icon = function(iconClassName, tagName) {
              tagName = tagName || "i";
              return "<" + tagName + ' class="' + iconClassName + '"/>';
            };
            var ui = {
              editor: editor,
              toolbar: toolbar,
              editingArea: editingArea,
              codable: codable,
              editable: editable,
              statusbar: statusbar,
              airEditor: airEditor,
              airEditable: airEditable,
              buttonGroup: buttonGroup,
              dropdown: dropdown,
              dropdownButtonContents: dropdownButtonContents,
              dropdownCheck: dropdownCheck,
              palette: palette,
              dialog: dialog,
              popover: popover,
              checkbox: checkbox,
              icon: icon,
              options: {},
              button: function($node, options) {
                return renderer.create(
                  '<button type="button" class="note-btn btn btn-default btn-sm" role="button" tabindex="-1">',
                  function($node, options) {
                    if (options && options.tooltip) {
                      $node
                        .attr({
                          title: options.tooltip,
                          "aria-label": options.tooltip
                        })
                        .tooltip({
                          container:
                            options.container !== undefined
                              ? options.container
                              : "body",
                          trigger: "hover",
                          placement: "bottom"
                        })
                        .on("click", function(e) {
                          $$1(e.currentTarget).tooltip("hide");
                        });
                    }
                  }
                )($node, options);
              },
              toggleBtn: function($btn, isEnable) {
                $btn.toggleClass("disabled", !isEnable);
                $btn.attr("disabled", !isEnable);
              },
              toggleBtnActive: function($btn, isActive) {
                $btn.toggleClass("active", isActive);
              },
              onDialogShown: function($dialog, handler) {
                $dialog.one("shown.bs.modal", handler);
              },
              onDialogHidden: function($dialog, handler) {
                $dialog.one("hidden.bs.modal", handler);
              },
              showDialog: function($dialog) {
                $dialog.modal("show");
              },
              hideDialog: function($dialog) {
                $dialog.modal("hide");
              },
              createLayout: function($note, options) {
                var $editor = (options.airMode
                  ? ui.airEditor([ui.editingArea([ui.airEditable()])])
                  : ui.editor([
                      ui.toolbar(),
                      ui.editingArea([ui.codable(), ui.editable()]),
                      ui.statusbar()
                    ])
                ).render();
                $editor.insertAfter($note);
                return {
                  note: $note,
                  editor: $editor,
                  toolbar: $editor.find(".note-toolbar"),
                  editingArea: $editor.find(".note-editing-area"),
                  editable: $editor.find(".note-editable"),
                  codable: $editor.find(".note-codable"),
                  statusbar: $editor.find(".note-statusbar")
                };
              },
              removeLayout: function($note, layoutInfo) {
                $note.html(layoutInfo.editable.html());
                layoutInfo.editor.remove();
                $note.show();
              }
            };

            $$1.summernote = $$1.summernote || {
              lang: {}
            };
            $$1.extend($$1.summernote.lang, {
              "en-US": {
                font: {
                  bold: "Bold",
                  italic: "Italic",
                  underline: "Underline",
                  clear: "Remove Font Style",
                  height: "Line Height",
                  name: "Font Family",
                  strikethrough: "Strikethrough",
                  subscript: "Subscript",
                  superscript: "Superscript",
                  size: "Font Size"
                },
                image: {
                  image: "Picture",
                  insert: "Insert Image",
                  resizeFull: "Resize full",
                  resizeHalf: "Resize half",
                  resizeQuarter: "Resize quarter",
                  resizeNone: "Original size",
                  floatLeft: "Float Left",
                  floatRight: "Float Right",
                  floatNone: "Remove float",
                  shapeRounded: "Shape: Rounded",
                  shapeCircle: "Shape: Circle",
                  shapeThumbnail: "Shape: Thumbnail",
                  shapeNone: "Shape: None",
                  dragImageHere: "Drag image or text here",
                  dropImage: "Drop image or Text",
                  selectFromFiles: "Select from files",
                  maximumFileSize: "Maximum file size",
                  maximumFileSizeError: "Maximum file size exceeded.",
                  url: "Image URL",
                  remove: "Remove Image",
                  original: "Original"
                },
                video: {
                  video: "Video",
                  videoLink: "Video Link",
                  insert: "Insert Video",
                  url: "Video URL",
                  providers:
                    "(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)"
                },
                link: {
                  link: "Link",
                  insert: "Insert Link",
                  unlink: "Unlink",
                  edit: "Edit",
                  textToDisplay: "Text to display",
                  url: "To what URL should this link go?",
                  openInNewWindow: "Open in new window"
                },
                table: {
                  table: "Table",
                  addRowAbove: "Add row above",
                  addRowBelow: "Add row below",
                  addColLeft: "Add column left",
                  addColRight: "Add column right",
                  delRow: "Delete row",
                  delCol: "Delete column",
                  delTable: "Delete table"
                },
                hr: {
                  insert: "Insert Horizontal Rule"
                },
                style: {
                  style: "Style",
                  p: "Normal",
                  blockquote: "Quote",
                  pre: "Code",
                  h1: "Header 1",
                  h2: "Header 2",
                  h3: "Header 3",
                  h4: "Header 4",
                  h5: "Header 5",
                  h6: "Header 6"
                },
                lists: {
                  unordered: "Unordered list",
                  ordered: "Ordered list"
                },
                options: {
                  help: "Help",
                  fullscreen: "Full Screen",
                  codeview: "Code View"
                },
                paragraph: {
                  paragraph: "Paragraph",
                  outdent: "Outdent",
                  indent: "Indent",
                  left: "Align left",
                  center: "Align center",
                  right: "Align right",
                  justify: "Justify full"
                },
                color: {
                  recent: "Recent Color",
                  more: "More Color",
                  background: "Background Color",
                  foreground: "Foreground Color",
                  transparent: "Transparent",
                  setTransparent: "Set transparent",
                  reset: "Reset",
                  resetToDefault: "Reset to default",
                  cpSelect: "Select"
                },
                shortcut: {
                  shortcuts: "Keyboard shortcuts",
                  close: "Close",
                  textFormatting: "Text formatting",
                  action: "Action",
                  paragraphFormatting: "Paragraph formatting",
                  documentStyle: "Document Style",
                  extraKeys: "Extra keys"
                },
                help: {
                  insertParagraph: "Insert Paragraph",
                  undo: "Undoes the last command",
                  redo: "Redoes the last command",
                  tab: "Tab",
                  untab: "Untab",
                  bold: "Set a bold style",
                  italic: "Set a italic style",
                  underline: "Set a underline style",
                  strikethrough: "Set a strikethrough style",
                  removeFormat: "Clean a style",
                  justifyLeft: "Set left align",
                  justifyCenter: "Set center align",
                  justifyRight: "Set right align",
                  justifyFull: "Set full align",
                  insertUnorderedList: "Toggle unordered list",
                  insertOrderedList: "Toggle ordered list",
                  outdent: "Outdent on current paragraph",
                  indent: "Indent on current paragraph",
                  formatPara:
                    "Change current block's format as a paragraph(P tag)",
                  formatH1: "Change current block's format as H1",
                  formatH2: "Change current block's format as H2",
                  formatH3: "Change current block's format as H3",
                  formatH4: "Change current block's format as H4",
                  formatH5: "Change current block's format as H5",
                  formatH6: "Change current block's format as H6",
                  insertHorizontalRule: "Insert horizontal rule",
                  "linkDialog.show": "Show Link Dialog"
                },
                history: {
                  undo: "Undo",
                  redo: "Redo"
                },
                specialChar: {
                  specialChar: "SPECIAL CHARACTERS",
                  select: "Select Special characters"
                }
              }
            });

            var isSupportAmd =
              true &&
              __webpack_require__(
                /*! !webpack amd options */ "../../node_modules/webpack/buildin/amd-options.js"
              ); // eslint-disable-line
            /**
             * returns whether font is installed or not.
             *
             * @param {String} fontName
             * @return {Boolean}
             */
            function isFontInstalled(fontName) {
              var testFontName =
                fontName === "Comic Sans MS" ? "Courier New" : "Comic Sans MS";
              var testText = "mmmmmmmmmmwwwww";
              var testSize = "200px";
              var canvas = document.createElement("canvas");
              var context = canvas.getContext("2d");
              context.font = testSize + " '" + testFontName + "'";
              var originalWidth = context.measureText(testText).width;
              context.font =
                testSize + " '" + fontName + "', '" + testFontName + "'";
              var width = context.measureText(testText).width;
              return originalWidth !== width;
            }
            var userAgent = navigator.userAgent;
            var isMSIE = /MSIE|Trident/i.test(userAgent);
            var browserVersion;
            if (isMSIE) {
              var matches = /MSIE (\d+[.]\d+)/.exec(userAgent);
              if (matches) {
                browserVersion = parseFloat(matches[1]);
              }
              matches = /Trident\/.*rv:([0-9]{1,}[.0-9]{0,})/.exec(userAgent);
              if (matches) {
                browserVersion = parseFloat(matches[1]);
              }
            }
            var isEdge = /Edge\/\d+/.test(userAgent);
            var hasCodeMirror = !!window.CodeMirror;
            var isSupportTouch =
              "ontouchstart" in window ||
              navigator.MaxTouchPoints > 0 ||
              navigator.msMaxTouchPoints > 0;
            // [workaround] IE doesn't have input events for contentEditable
            // - see: https://goo.gl/4bfIvA
            var inputEventName =
              isMSIE || isEdge
                ? "DOMCharacterDataModified DOMSubtreeModified DOMNodeInserted"
                : "input";
            /**
             * @class core.env
             *
             * Object which check platform and agent
             *
             * @singleton
             * @alternateClassName env
             */
            var env = {
              isMac: navigator.appVersion.indexOf("Mac") > -1,
              isMSIE: isMSIE,
              isEdge: isEdge,
              isFF: !isEdge && /firefox/i.test(userAgent),
              isPhantom: /PhantomJS/i.test(userAgent),
              isWebkit: !isEdge && /webkit/i.test(userAgent),
              isChrome: !isEdge && /chrome/i.test(userAgent),
              isSafari: !isEdge && /safari/i.test(userAgent),
              browserVersion: browserVersion,
              jqueryVersion: parseFloat($$1.fn.jquery),
              isSupportAmd: isSupportAmd,
              isSupportTouch: isSupportTouch,
              hasCodeMirror: hasCodeMirror,
              isFontInstalled: isFontInstalled,
              isW3CRangeSupport: !!document.createRange,
              inputEventName: inputEventName
            };

            /**
             * @class core.func
             *
             * func utils (for high-order func's arg)
             *
             * @singleton
             * @alternateClassName func
             */
            function eq(itemA) {
              return function(itemB) {
                return itemA === itemB;
              };
            }
            function eq2(itemA, itemB) {
              return itemA === itemB;
            }
            function peq2(propName) {
              return function(itemA, itemB) {
                return itemA[propName] === itemB[propName];
              };
            }
            function ok() {
              return true;
            }
            function fail() {
              return false;
            }
            function not(f) {
              return function() {
                return !f.apply(f, arguments);
              };
            }
            function and(fA, fB) {
              return function(item) {
                return fA(item) && fB(item);
              };
            }
            function self(a) {
              return a;
            }
            function invoke(obj, method) {
              return function() {
                return obj[method].apply(obj, arguments);
              };
            }
            var idCounter = 0;
            /**
             * generate a globally-unique id
             *
             * @param {String} [prefix]
             */
            function uniqueId(prefix) {
              var id = ++idCounter + "";
              return prefix ? prefix + id : id;
            }
            /**
             * returns bnd (bounds) from rect
             *
             * - IE Compatibility Issue: http://goo.gl/sRLOAo
             * - Scroll Issue: http://goo.gl/sNjUc
             *
             * @param {Rect} rect
             * @return {Object} bounds
             * @return {Number} bounds.top
             * @return {Number} bounds.left
             * @return {Number} bounds.width
             * @return {Number} bounds.height
             */
            function rect2bnd(rect) {
              var $document = $(document);
              return {
                top: rect.top + $document.scrollTop(),
                left: rect.left + $document.scrollLeft(),
                width: rect.right - rect.left,
                height: rect.bottom - rect.top
              };
            }
            /**
             * returns a copy of the object where the keys have become the values and the values the keys.
             * @param {Object} obj
             * @return {Object}
             */
            function invertObject(obj) {
              var inverted = {};
              for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                  inverted[obj[key]] = key;
                }
              }
              return inverted;
            }
            /**
             * @param {String} namespace
             * @param {String} [prefix]
             * @return {String}
             */
            function namespaceToCamel(namespace, prefix) {
              prefix = prefix || "";
              return (
                prefix +
                namespace
                  .split(".")
                  .map(function(name) {
                    return (
                      name.substring(0, 1).toUpperCase() + name.substring(1)
                    );
                  })
                  .join("")
              );
            }
            /**
             * Returns a function, that, as long as it continues to be invoked, will not
             * be triggered. The function will be called after it stops being called for
             * N milliseconds. If `immediate` is passed, trigger the function on the
             * leading edge, instead of the trailing.
             * @param {Function} func
             * @param {Number} wait
             * @param {Boolean} immediate
             * @return {Function}
             */
            function debounce(func, wait, immediate) {
              var timeout;
              return function() {
                var context = this;
                var args = arguments;
                var later = function() {
                  timeout = null;
                  if (!immediate) {
                    func.apply(context, args);
                  }
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) {
                  func.apply(context, args);
                }
              };
            }
            /**
             *
             * @param {String} url
             * @return {Boolean}
             */
            function isValidUrl(url) {
              var expression = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;
              return expression.test(url);
            }
            var func = {
              eq: eq,
              eq2: eq2,
              peq2: peq2,
              ok: ok,
              fail: fail,
              self: self,
              not: not,
              and: and,
              invoke: invoke,
              uniqueId: uniqueId,
              rect2bnd: rect2bnd,
              invertObject: invertObject,
              namespaceToCamel: namespaceToCamel,
              debounce: debounce,
              isValidUrl: isValidUrl
            };

            /**
             * returns the first item of an array.
             *
             * @param {Array} array
             */
            function head(array) {
              return array[0];
            }
            /**
             * returns the last item of an array.
             *
             * @param {Array} array
             */
            function last(array) {
              return array[array.length - 1];
            }
            /**
             * returns everything but the last entry of the array.
             *
             * @param {Array} array
             */
            function initial(array) {
              return array.slice(0, array.length - 1);
            }
            /**
             * returns the rest of the items in an array.
             *
             * @param {Array} array
             */
            function tail(array) {
              return array.slice(1);
            }
            /**
             * returns item of array
             */
            function find(array, pred) {
              for (var idx = 0, len = array.length; idx < len; idx++) {
                var item = array[idx];
                if (pred(item)) {
                  return item;
                }
              }
            }
            /**
             * returns true if all of the values in the array pass the predicate truth test.
             */
            function all(array, pred) {
              for (var idx = 0, len = array.length; idx < len; idx++) {
                if (!pred(array[idx])) {
                  return false;
                }
              }
              return true;
            }
            /**
             * returns true if the value is present in the list.
             */
            function contains(array, item) {
              if (array && array.length && item) {
                return array.indexOf(item) !== -1;
              }
              return false;
            }
            /**
             * get sum from a list
             *
             * @param {Array} array - array
             * @param {Function} fn - iterator
             */
            function sum(array, fn) {
              fn = fn || func.self;
              return array.reduce(function(memo, v) {
                return memo + fn(v);
              }, 0);
            }
            /**
             * returns a copy of the collection with array type.
             * @param {Collection} collection - collection eg) node.childNodes, ...
             */
            function from(collection) {
              var result = [];
              var length = collection.length;
              var idx = -1;
              while (++idx < length) {
                result[idx] = collection[idx];
              }
              return result;
            }
            /**
             * returns whether list is empty or not
             */
            function isEmpty(array) {
              return !array || !array.length;
            }
            /**
             * cluster elements by predicate function.
             *
             * @param {Array} array - array
             * @param {Function} fn - predicate function for cluster rule
             * @param {Array[]}
             */
            function clusterBy(array, fn) {
              if (!array.length) {
                return [];
              }
              var aTail = tail(array);
              return aTail.reduce(
                function(memo, v) {
                  var aLast = last(memo);
                  if (fn(last(aLast), v)) {
                    aLast[aLast.length] = v;
                  } else {
                    memo[memo.length] = [v];
                  }
                  return memo;
                },
                [[head(array)]]
              );
            }
            /**
             * returns a copy of the array with all false values removed
             *
             * @param {Array} array - array
             * @param {Function} fn - predicate function for cluster rule
             */
            function compact(array) {
              var aResult = [];
              for (var idx = 0, len = array.length; idx < len; idx++) {
                if (array[idx]) {
                  aResult.push(array[idx]);
                }
              }
              return aResult;
            }
            /**
             * produces a duplicate-free version of the array
             *
             * @param {Array} array
             */
            function unique(array) {
              var results = [];
              for (var idx = 0, len = array.length; idx < len; idx++) {
                if (!contains(results, array[idx])) {
                  results.push(array[idx]);
                }
              }
              return results;
            }
            /**
             * returns next item.
             * @param {Array} array
             */
            function next(array, item) {
              if (array && array.length && item) {
                var idx = array.indexOf(item);
                return idx === -1 ? null : array[idx + 1];
              }
              return null;
            }
            /**
             * returns prev item.
             * @param {Array} array
             */
            function prev(array, item) {
              if (array && array.length && item) {
                var idx = array.indexOf(item);
                return idx === -1 ? null : array[idx - 1];
              }
              return null;
            }
            /**
             * @class core.list
             *
             * list utils
             *
             * @singleton
             * @alternateClassName list
             */
            var lists = {
              head: head,
              last: last,
              initial: initial,
              tail: tail,
              prev: prev,
              next: next,
              find: find,
              contains: contains,
              all: all,
              sum: sum,
              from: from,
              isEmpty: isEmpty,
              clusterBy: clusterBy,
              compact: compact,
              unique: unique
            };

            var NBSP_CHAR = String.fromCharCode(160);
            var ZERO_WIDTH_NBSP_CHAR = "\ufeff";
            /**
             * @method isEditable
             *
             * returns whether node is `note-editable` or not.
             *
             * @param {Node} node
             * @return {Boolean}
             */
            function isEditable(node) {
              return node && $$1(node).hasClass("note-editable");
            }
            /**
             * @method isControlSizing
             *
             * returns whether node is `note-control-sizing` or not.
             *
             * @param {Node} node
             * @return {Boolean}
             */
            function isControlSizing(node) {
              return node && $$1(node).hasClass("note-control-sizing");
            }
            /**
             * @method makePredByNodeName
             *
             * returns predicate which judge whether nodeName is same
             *
             * @param {String} nodeName
             * @return {Function}
             */
            function makePredByNodeName(nodeName) {
              nodeName = nodeName.toUpperCase();
              return function(node) {
                return node && node.nodeName.toUpperCase() === nodeName;
              };
            }
            /**
             * @method isText
             *
             *
             *
             * @param {Node} node
             * @return {Boolean} true if node's type is text(3)
             */
            function isText(node) {
              return node && node.nodeType === 3;
            }
            /**
             * @method isElement
             *
             *
             *
             * @param {Node} node
             * @return {Boolean} true if node's type is element(1)
             */
            function isElement(node) {
              return node && node.nodeType === 1;
            }
            /**
             * ex) br, col, embed, hr, img, input, ...
             * @see http://www.w3.org/html/wg/drafts/html/master/syntax.html#void-elements
             */
            function isVoid(node) {
              return (
                node &&
                /^BR|^IMG|^HR|^IFRAME|^BUTTON|^INPUT|^AUDIO|^VIDEO|^EMBED/.test(
                  node.nodeName.toUpperCase()
                )
              );
            }
            function isPara(node) {
              if (isEditable(node)) {
                return false;
              }
              // Chrome(v31.0), FF(v25.0.1) use DIV for paragraph
              return (
                node && /^DIV|^P|^LI|^H[1-7]/.test(node.nodeName.toUpperCase())
              );
            }
            function isHeading(node) {
              return node && /^H[1-7]/.test(node.nodeName.toUpperCase());
            }
            var isPre = makePredByNodeName("PRE");
            var isLi = makePredByNodeName("LI");
            function isPurePara(node) {
              return isPara(node) && !isLi(node);
            }
            var isTable = makePredByNodeName("TABLE");
            var isData = makePredByNodeName("DATA");
            function isInline(node) {
              return (
                !isBodyContainer(node) &&
                !isList(node) &&
                !isHr(node) &&
                !isPara(node) &&
                !isTable(node) &&
                !isBlockquote(node) &&
                !isData(node)
              );
            }
            function isList(node) {
              return node && /^UL|^OL/.test(node.nodeName.toUpperCase());
            }
            var isHr = makePredByNodeName("HR");
            function isCell(node) {
              return node && /^TD|^TH/.test(node.nodeName.toUpperCase());
            }
            var isBlockquote = makePredByNodeName("BLOCKQUOTE");
            function isBodyContainer(node) {
              return isCell(node) || isBlockquote(node) || isEditable(node);
            }
            var isAnchor = makePredByNodeName("A");
            function isParaInline(node) {
              return isInline(node) && !!ancestor(node, isPara);
            }
            function isBodyInline(node) {
              return isInline(node) && !ancestor(node, isPara);
            }
            var isBody = makePredByNodeName("BODY");
            /**
             * returns whether nodeB is closest sibling of nodeA
             *
             * @param {Node} nodeA
             * @param {Node} nodeB
             * @return {Boolean}
             */
            function isClosestSibling(nodeA, nodeB) {
              return (
                nodeA.nextSibling === nodeB || nodeA.previousSibling === nodeB
              );
            }
            /**
             * returns array of closest siblings with node
             *
             * @param {Node} node
             * @param {function} [pred] - predicate function
             * @return {Node[]}
             */
            function withClosestSiblings(node, pred) {
              pred = pred || func.ok;
              var siblings = [];
              if (node.previousSibling && pred(node.previousSibling)) {
                siblings.push(node.previousSibling);
              }
              siblings.push(node);
              if (node.nextSibling && pred(node.nextSibling)) {
                siblings.push(node.nextSibling);
              }
              return siblings;
            }
            /**
             * blank HTML for cursor position
             * - [workaround] old IE only works with &nbsp;
             * - [workaround] IE11 and other browser works with bogus br
             */
            var blankHTML =
              env.isMSIE && env.browserVersion < 11 ? "&nbsp;" : "<br>";
            /**
             * @method nodeLength
             *
             * returns #text's text size or element's childNodes size
             *
             * @param {Node} node
             */
            function nodeLength(node) {
              if (isText(node)) {
                return node.nodeValue.length;
              }
              if (node) {
                return node.childNodes.length;
              }
              return 0;
            }
            /**
             * returns whether node is empty or not.
             *
             * @param {Node} node
             * @return {Boolean}
             */
            function isEmpty$1(node) {
              var len = nodeLength(node);
              if (len === 0) {
                return true;
              } else if (
                !isText(node) &&
                len === 1 &&
                node.innerHTML === blankHTML
              ) {
                // ex) <p><br></p>, <span><br></span>
                return true;
              } else if (
                lists.all(node.childNodes, isText) &&
                node.innerHTML === ""
              ) {
                // ex) <p></p>, <span></span>
                return true;
              }
              return false;
            }
            /**
             * padding blankHTML if node is empty (for cursor position)
             */
            function paddingBlankHTML(node) {
              if (!isVoid(node) && !nodeLength(node)) {
                node.innerHTML = blankHTML;
              }
            }
            /**
             * find nearest ancestor predicate hit
             *
             * @param {Node} node
             * @param {Function} pred - predicate function
             */
            function ancestor(node, pred) {
              while (node) {
                if (pred(node)) {
                  return node;
                }
                if (isEditable(node)) {
                  break;
                }
                node = node.parentNode;
              }
              return null;
            }
            /**
             * find nearest ancestor only single child blood line and predicate hit
             *
             * @param {Node} node
             * @param {Function} pred - predicate function
             */
            function singleChildAncestor(node, pred) {
              node = node.parentNode;
              while (node) {
                if (nodeLength(node) !== 1) {
                  break;
                }
                if (pred(node)) {
                  return node;
                }
                if (isEditable(node)) {
                  break;
                }
                node = node.parentNode;
              }
              return null;
            }
            /**
             * returns new array of ancestor nodes (until predicate hit).
             *
             * @param {Node} node
             * @param {Function} [optional] pred - predicate function
             */
            function listAncestor(node, pred) {
              pred = pred || func.fail;
              var ancestors = [];
              ancestor(node, function(el) {
                if (!isEditable(el)) {
                  ancestors.push(el);
                }
                return pred(el);
              });
              return ancestors;
            }
            /**
             * find farthest ancestor predicate hit
             */
            function lastAncestor(node, pred) {
              var ancestors = listAncestor(node);
              return lists.last(ancestors.filter(pred));
            }
            /**
             * returns common ancestor node between two nodes.
             *
             * @param {Node} nodeA
             * @param {Node} nodeB
             */
            function commonAncestor(nodeA, nodeB) {
              var ancestors = listAncestor(nodeA);
              for (var n = nodeB; n; n = n.parentNode) {
                if (ancestors.indexOf(n) > -1) return n;
              }
              return null; // difference document area
            }
            /**
             * listing all previous siblings (until predicate hit).
             *
             * @param {Node} node
             * @param {Function} [optional] pred - predicate function
             */
            function listPrev(node, pred) {
              pred = pred || func.fail;
              var nodes = [];
              while (node) {
                if (pred(node)) {
                  break;
                }
                nodes.push(node);
                node = node.previousSibling;
              }
              return nodes;
            }
            /**
             * listing next siblings (until predicate hit).
             *
             * @param {Node} node
             * @param {Function} [pred] - predicate function
             */
            function listNext(node, pred) {
              pred = pred || func.fail;
              var nodes = [];
              while (node) {
                if (pred(node)) {
                  break;
                }
                nodes.push(node);
                node = node.nextSibling;
              }
              return nodes;
            }
            /**
             * listing descendant nodes
             *
             * @param {Node} node
             * @param {Function} [pred] - predicate function
             */
            function listDescendant(node, pred) {
              var descendants = [];
              pred = pred || func.ok;
              // start DFS(depth first search) with node
              (function fnWalk(current) {
                if (node !== current && pred(current)) {
                  descendants.push(current);
                }
                for (
                  var idx = 0, len = current.childNodes.length;
                  idx < len;
                  idx++
                ) {
                  fnWalk(current.childNodes[idx]);
                }
              })(node);
              return descendants;
            }
            /**
             * wrap node with new tag.
             *
             * @param {Node} node
             * @param {Node} tagName of wrapper
             * @return {Node} - wrapper
             */
            function wrap(node, wrapperName) {
              var parent = node.parentNode;
              var wrapper = $$1("<" + wrapperName + ">")[0];
              parent.insertBefore(wrapper, node);
              wrapper.appendChild(node);
              return wrapper;
            }
            /**
             * insert node after preceding
             *
             * @param {Node} node
             * @param {Node} preceding - predicate function
             */
            function insertAfter(node, preceding) {
              var next = preceding.nextSibling;
              var parent = preceding.parentNode;
              if (next) {
                parent.insertBefore(node, next);
              } else {
                parent.appendChild(node);
              }
              return node;
            }
            /**
             * append elements.
             *
             * @param {Node} node
             * @param {Collection} aChild
             */
            function appendChildNodes(node, aChild) {
              $$1.each(aChild, function(idx, child) {
                node.appendChild(child);
              });
              return node;
            }
            /**
             * returns whether boundaryPoint is left edge or not.
             *
             * @param {BoundaryPoint} point
             * @return {Boolean}
             */
            function isLeftEdgePoint(point) {
              return point.offset === 0;
            }
            /**
             * returns whether boundaryPoint is right edge or not.
             *
             * @param {BoundaryPoint} point
             * @return {Boolean}
             */
            function isRightEdgePoint(point) {
              return point.offset === nodeLength(point.node);
            }
            /**
             * returns whether boundaryPoint is edge or not.
             *
             * @param {BoundaryPoint} point
             * @return {Boolean}
             */
            function isEdgePoint(point) {
              return isLeftEdgePoint(point) || isRightEdgePoint(point);
            }
            /**
             * returns whether node is left edge of ancestor or not.
             *
             * @param {Node} node
             * @param {Node} ancestor
             * @return {Boolean}
             */
            function isLeftEdgeOf(node, ancestor) {
              while (node && node !== ancestor) {
                if (position(node) !== 0) {
                  return false;
                }
                node = node.parentNode;
              }
              return true;
            }
            /**
             * returns whether node is right edge of ancestor or not.
             *
             * @param {Node} node
             * @param {Node} ancestor
             * @return {Boolean}
             */
            function isRightEdgeOf(node, ancestor) {
              if (!ancestor) {
                return false;
              }
              while (node && node !== ancestor) {
                if (position(node) !== nodeLength(node.parentNode) - 1) {
                  return false;
                }
                node = node.parentNode;
              }
              return true;
            }
            /**
             * returns whether point is left edge of ancestor or not.
             * @param {BoundaryPoint} point
             * @param {Node} ancestor
             * @return {Boolean}
             */
            function isLeftEdgePointOf(point, ancestor) {
              return (
                isLeftEdgePoint(point) && isLeftEdgeOf(point.node, ancestor)
              );
            }
            /**
             * returns whether point is right edge of ancestor or not.
             * @param {BoundaryPoint} point
             * @param {Node} ancestor
             * @return {Boolean}
             */
            function isRightEdgePointOf(point, ancestor) {
              return (
                isRightEdgePoint(point) && isRightEdgeOf(point.node, ancestor)
              );
            }
            /**
             * returns offset from parent.
             *
             * @param {Node} node
             */
            function position(node) {
              var offset = 0;
              while ((node = node.previousSibling)) {
                offset += 1;
              }
              return offset;
            }
            function hasChildren(node) {
              return !!(node && node.childNodes && node.childNodes.length);
            }
            /**
             * returns previous boundaryPoint
             *
             * @param {BoundaryPoint} point
             * @param {Boolean} isSkipInnerOffset
             * @return {BoundaryPoint}
             */
            function prevPoint(point, isSkipInnerOffset) {
              var node;
              var offset;
              if (point.offset === 0) {
                if (isEditable(point.node)) {
                  return null;
                }
                node = point.node.parentNode;
                offset = position(point.node);
              } else if (hasChildren(point.node)) {
                node = point.node.childNodes[point.offset - 1];
                offset = nodeLength(node);
              } else {
                node = point.node;
                offset = isSkipInnerOffset ? 0 : point.offset - 1;
              }
              return {
                node: node,
                offset: offset
              };
            }
            /**
             * returns next boundaryPoint
             *
             * @param {BoundaryPoint} point
             * @param {Boolean} isSkipInnerOffset
             * @return {BoundaryPoint}
             */
            function nextPoint(point, isSkipInnerOffset) {
              var node, offset;
              if (nodeLength(point.node) === point.offset) {
                if (isEditable(point.node)) {
                  return null;
                }
                node = point.node.parentNode;
                offset = position(point.node) + 1;
              } else if (hasChildren(point.node)) {
                node = point.node.childNodes[point.offset];
                offset = 0;
              } else {
                node = point.node;
                offset = isSkipInnerOffset
                  ? nodeLength(point.node)
                  : point.offset + 1;
              }
              return {
                node: node,
                offset: offset
              };
            }
            /**
             * returns whether pointA and pointB is same or not.
             *
             * @param {BoundaryPoint} pointA
             * @param {BoundaryPoint} pointB
             * @return {Boolean}
             */
            function isSamePoint(pointA, pointB) {
              return (
                pointA.node === pointB.node && pointA.offset === pointB.offset
              );
            }
            /**
             * returns whether point is visible (can set cursor) or not.
             *
             * @param {BoundaryPoint} point
             * @return {Boolean}
             */
            function isVisiblePoint(point) {
              if (
                isText(point.node) ||
                !hasChildren(point.node) ||
                isEmpty$1(point.node)
              ) {
                return true;
              }
              var leftNode = point.node.childNodes[point.offset - 1];
              var rightNode = point.node.childNodes[point.offset];
              if (
                (!leftNode || isVoid(leftNode)) &&
                (!rightNode || isVoid(rightNode))
              ) {
                return true;
              }
              return false;
            }
            /**
             * @method prevPointUtil
             *
             * @param {BoundaryPoint} point
             * @param {Function} pred
             * @return {BoundaryPoint}
             */
            function prevPointUntil(point, pred) {
              while (point) {
                if (pred(point)) {
                  return point;
                }
                point = prevPoint(point);
              }
              return null;
            }
            /**
             * @method nextPointUntil
             *
             * @param {BoundaryPoint} point
             * @param {Function} pred
             * @return {BoundaryPoint}
             */
            function nextPointUntil(point, pred) {
              while (point) {
                if (pred(point)) {
                  return point;
                }
                point = nextPoint(point);
              }
              return null;
            }
            /**
             * returns whether point has character or not.
             *
             * @param {Point} point
             * @return {Boolean}
             */
            function isCharPoint(point) {
              if (!isText(point.node)) {
                return false;
              }
              var ch = point.node.nodeValue.charAt(point.offset - 1);
              return ch && ch !== " " && ch !== NBSP_CHAR;
            }
            /**
             * @method walkPoint
             *
             * @param {BoundaryPoint} startPoint
             * @param {BoundaryPoint} endPoint
             * @param {Function} handler
             * @param {Boolean} isSkipInnerOffset
             */
            function walkPoint(
              startPoint,
              endPoint,
              handler,
              isSkipInnerOffset
            ) {
              var point = startPoint;
              while (point) {
                handler(point);
                if (isSamePoint(point, endPoint)) {
                  break;
                }
                var isSkipOffset =
                  isSkipInnerOffset &&
                  startPoint.node !== point.node &&
                  endPoint.node !== point.node;
                point = nextPoint(point, isSkipOffset);
              }
            }
            /**
             * @method makeOffsetPath
             *
             * return offsetPath(array of offset) from ancestor
             *
             * @param {Node} ancestor - ancestor node
             * @param {Node} node
             */
            function makeOffsetPath(ancestor, node) {
              var ancestors = listAncestor(node, func.eq(ancestor));
              return ancestors.map(position).reverse();
            }
            /**
             * @method fromOffsetPath
             *
             * return element from offsetPath(array of offset)
             *
             * @param {Node} ancestor - ancestor node
             * @param {array} offsets - offsetPath
             */
            function fromOffsetPath(ancestor, offsets) {
              var current = ancestor;
              for (var i = 0, len = offsets.length; i < len; i++) {
                if (current.childNodes.length <= offsets[i]) {
                  current = current.childNodes[current.childNodes.length - 1];
                } else {
                  current = current.childNodes[offsets[i]];
                }
              }
              return current;
            }
            /**
             * @method splitNode
             *
             * split element or #text
             *
             * @param {BoundaryPoint} point
             * @param {Object} [options]
             * @param {Boolean} [options.isSkipPaddingBlankHTML] - default: false
             * @param {Boolean} [options.isNotSplitEdgePoint] - default: false
             * @param {Boolean} [options.isDiscardEmptySplits] - default: false
             * @return {Node} right node of boundaryPoint
             */
            function splitNode(point, options) {
              var isSkipPaddingBlankHTML =
                options && options.isSkipPaddingBlankHTML;
              var isNotSplitEdgePoint = options && options.isNotSplitEdgePoint;
              var isDiscardEmptySplits =
                options && options.isDiscardEmptySplits;
              if (isDiscardEmptySplits) {
                isSkipPaddingBlankHTML = true;
              }
              // edge case
              if (
                isEdgePoint(point) &&
                (isText(point.node) || isNotSplitEdgePoint)
              ) {
                if (isLeftEdgePoint(point)) {
                  return point.node;
                } else if (isRightEdgePoint(point)) {
                  return point.node.nextSibling;
                }
              }
              // split #text
              if (isText(point.node)) {
                return point.node.splitText(point.offset);
              } else {
                var childNode = point.node.childNodes[point.offset];
                var clone = insertAfter(
                  point.node.cloneNode(false),
                  point.node
                );
                appendChildNodes(clone, listNext(childNode));
                if (!isSkipPaddingBlankHTML) {
                  paddingBlankHTML(point.node);
                  paddingBlankHTML(clone);
                }
                if (isDiscardEmptySplits) {
                  if (isEmpty$1(point.node)) {
                    remove(point.node);
                  }
                  if (isEmpty$1(clone)) {
                    remove(clone);
                    return point.node.nextSibling;
                  }
                }
                return clone;
              }
            }
            /**
             * @method splitTree
             *
             * split tree by point
             *
             * @param {Node} root - split root
             * @param {BoundaryPoint} point
             * @param {Object} [options]
             * @param {Boolean} [options.isSkipPaddingBlankHTML] - default: false
             * @param {Boolean} [options.isNotSplitEdgePoint] - default: false
             * @return {Node} right node of boundaryPoint
             */
            function splitTree(root, point, options) {
              // ex) [#text, <span>, <p>]
              var ancestors = listAncestor(point.node, func.eq(root));
              if (!ancestors.length) {
                return null;
              } else if (ancestors.length === 1) {
                return splitNode(point, options);
              }
              return ancestors.reduce(function(node, parent) {
                if (node === point.node) {
                  node = splitNode(point, options);
                }
                return splitNode(
                  {
                    node: parent,
                    offset: node ? position(node) : nodeLength(parent)
                  },
                  options
                );
              });
            }
            /**
             * split point
             *
             * @param {Point} point
             * @param {Boolean} isInline
             * @return {Object}
             */
            function splitPoint(point, isInline) {
              // find splitRoot, container
              //  - inline: splitRoot is a child of paragraph
              //  - block: splitRoot is a child of bodyContainer
              var pred = isInline ? isPara : isBodyContainer;
              var ancestors = listAncestor(point.node, pred);
              var topAncestor = lists.last(ancestors) || point.node;
              var splitRoot, container;
              if (pred(topAncestor)) {
                splitRoot = ancestors[ancestors.length - 2];
                container = topAncestor;
              } else {
                splitRoot = topAncestor;
                container = splitRoot.parentNode;
              }
              // if splitRoot is exists, split with splitTree
              var pivot =
                splitRoot &&
                splitTree(splitRoot, point, {
                  isSkipPaddingBlankHTML: isInline,
                  isNotSplitEdgePoint: isInline
                });
              // if container is point.node, find pivot with point.offset
              if (!pivot && container === point.node) {
                pivot = point.node.childNodes[point.offset];
              }
              return {
                rightNode: pivot,
                container: container
              };
            }
            function create(nodeName) {
              return document.createElement(nodeName);
            }
            function createText(text) {
              return document.createTextNode(text);
            }
            /**
             * @method remove
             *
             * remove node, (isRemoveChild: remove child or not)
             *
             * @param {Node} node
             * @param {Boolean} isRemoveChild
             */
            function remove(node, isRemoveChild) {
              if (!node || !node.parentNode) {
                return;
              }
              if (node.removeNode) {
                return node.removeNode(isRemoveChild);
              }
              var parent = node.parentNode;
              if (!isRemoveChild) {
                var nodes = [];
                for (var i = 0, len = node.childNodes.length; i < len; i++) {
                  nodes.push(node.childNodes[i]);
                }
                for (var i = 0, len = nodes.length; i < len; i++) {
                  parent.insertBefore(nodes[i], node);
                }
              }
              parent.removeChild(node);
            }
            /**
             * @method removeWhile
             *
             * @param {Node} node
             * @param {Function} pred
             */
            function removeWhile(node, pred) {
              while (node) {
                if (isEditable(node) || !pred(node)) {
                  break;
                }
                var parent = node.parentNode;
                remove(node);
                node = parent;
              }
            }
            /**
             * @method replace
             *
             * replace node with provided nodeName
             *
             * @param {Node} node
             * @param {String} nodeName
             * @return {Node} - new node
             */
            function replace(node, nodeName) {
              if (node.nodeName.toUpperCase() === nodeName.toUpperCase()) {
                return node;
              }
              var newNode = create(nodeName);
              if (node.style.cssText) {
                newNode.style.cssText = node.style.cssText;
              }
              appendChildNodes(newNode, lists.from(node.childNodes));
              insertAfter(newNode, node);
              remove(node);
              return newNode;
            }
            var isTextarea = makePredByNodeName("TEXTAREA");
            /**
             * @param {jQuery} $node
             * @param {Boolean} [stripLinebreaks] - default: false
             */
            function value($node, stripLinebreaks) {
              var val = isTextarea($node[0]) ? $node.val() : $node.html();
              if (stripLinebreaks) {
                return val.replace(/[\n\r]/g, "");
              }
              return val;
            }
            /**
             * @method html
             *
             * get the HTML contents of node
             *
             * @param {jQuery} $node
             * @param {Boolean} [isNewlineOnBlock]
             */
            function html($node, isNewlineOnBlock) {
              var markup = value($node);
              if (isNewlineOnBlock) {
                var regexTag = /<(\/?)(\b(?!!)[^>\s]*)(.*?)(\s*\/?>)/g;
                markup = markup.replace(regexTag, function(
                  match,
                  endSlash,
                  name
                ) {
                  name = name.toUpperCase();
                  var isEndOfInlineContainer =
                    /^DIV|^TD|^TH|^P|^LI|^H[1-7]/.test(name) && !!endSlash;
                  var isBlockNode = /^BLOCKQUOTE|^TABLE|^TBODY|^TR|^HR|^UL|^OL/.test(
                    name
                  );
                  return (
                    match + (isEndOfInlineContainer || isBlockNode ? "\n" : "")
                  );
                });
                markup = markup.trim();
              }
              return markup;
            }
            function posFromPlaceholder(placeholder) {
              var $placeholder = $$1(placeholder);
              var pos = $placeholder.offset();
              var height = $placeholder.outerHeight(true); // include margin
              return {
                left: pos.left,
                top: pos.top + height
              };
            }
            function attachEvents($node, events) {
              Object.keys(events).forEach(function(key) {
                $node.on(key, events[key]);
              });
            }
            function detachEvents($node, events) {
              Object.keys(events).forEach(function(key) {
                $node.off(key, events[key]);
              });
            }
            /**
             * @method isCustomStyleTag
             *
             * assert if a node contains a "note-styletag" class,
             * which implies that's a custom-made style tag node
             *
             * @param {Node} an HTML DOM node
             */
            function isCustomStyleTag(node) {
              return (
                node &&
                !isText(node) &&
                lists.contains(node.classList, "note-styletag")
              );
            }
            var dom = {
              /** @property {String} NBSP_CHAR */
              NBSP_CHAR: NBSP_CHAR,
              /** @property {String} ZERO_WIDTH_NBSP_CHAR */
              ZERO_WIDTH_NBSP_CHAR: ZERO_WIDTH_NBSP_CHAR,
              /** @property {String} blank */
              blank: blankHTML,
              /** @property {String} emptyPara */
              emptyPara: "<p>" + blankHTML + "</p>",
              makePredByNodeName: makePredByNodeName,
              isEditable: isEditable,
              isControlSizing: isControlSizing,
              isText: isText,
              isElement: isElement,
              isVoid: isVoid,
              isPara: isPara,
              isPurePara: isPurePara,
              isHeading: isHeading,
              isInline: isInline,
              isBlock: func.not(isInline),
              isBodyInline: isBodyInline,
              isBody: isBody,
              isParaInline: isParaInline,
              isPre: isPre,
              isList: isList,
              isTable: isTable,
              isData: isData,
              isCell: isCell,
              isBlockquote: isBlockquote,
              isBodyContainer: isBodyContainer,
              isAnchor: isAnchor,
              isDiv: makePredByNodeName("DIV"),
              isLi: isLi,
              isBR: makePredByNodeName("BR"),
              isSpan: makePredByNodeName("SPAN"),
              isB: makePredByNodeName("B"),
              isU: makePredByNodeName("U"),
              isS: makePredByNodeName("S"),
              isI: makePredByNodeName("I"),
              isImg: makePredByNodeName("IMG"),
              isTextarea: isTextarea,
              isEmpty: isEmpty$1,
              isEmptyAnchor: func.and(isAnchor, isEmpty$1),
              isClosestSibling: isClosestSibling,
              withClosestSiblings: withClosestSiblings,
              nodeLength: nodeLength,
              isLeftEdgePoint: isLeftEdgePoint,
              isRightEdgePoint: isRightEdgePoint,
              isEdgePoint: isEdgePoint,
              isLeftEdgeOf: isLeftEdgeOf,
              isRightEdgeOf: isRightEdgeOf,
              isLeftEdgePointOf: isLeftEdgePointOf,
              isRightEdgePointOf: isRightEdgePointOf,
              prevPoint: prevPoint,
              nextPoint: nextPoint,
              isSamePoint: isSamePoint,
              isVisiblePoint: isVisiblePoint,
              prevPointUntil: prevPointUntil,
              nextPointUntil: nextPointUntil,
              isCharPoint: isCharPoint,
              walkPoint: walkPoint,
              ancestor: ancestor,
              singleChildAncestor: singleChildAncestor,
              listAncestor: listAncestor,
              lastAncestor: lastAncestor,
              listNext: listNext,
              listPrev: listPrev,
              listDescendant: listDescendant,
              commonAncestor: commonAncestor,
              wrap: wrap,
              insertAfter: insertAfter,
              appendChildNodes: appendChildNodes,
              position: position,
              hasChildren: hasChildren,
              makeOffsetPath: makeOffsetPath,
              fromOffsetPath: fromOffsetPath,
              splitTree: splitTree,
              splitPoint: splitPoint,
              create: create,
              createText: createText,
              remove: remove,
              removeWhile: removeWhile,
              replace: replace,
              html: html,
              value: value,
              posFromPlaceholder: posFromPlaceholder,
              attachEvents: attachEvents,
              detachEvents: detachEvents,
              isCustomStyleTag: isCustomStyleTag
            };

            var Context = /** @class */ (function() {
              /**
               * @param {jQuery} $note
               * @param {Object} options
               */
              function Context($note, options) {
                this.ui = $$1.summernote.ui;
                this.$note = $note;
                this.memos = {};
                this.modules = {};
                this.layoutInfo = {};
                this.options = options;
                this.initialize();
              }
              /**
               * create layout and initialize modules and other resources
               */
              Context.prototype.initialize = function() {
                this.layoutInfo = this.ui.createLayout(
                  this.$note,
                  this.options
                );
                this._initialize();
                this.$note.hide();
                return this;
              };
              /**
               * destroy modules and other resources and remove layout
               */
              Context.prototype.destroy = function() {
                this._destroy();
                this.$note.removeData("summernote");
                this.ui.removeLayout(this.$note, this.layoutInfo);
              };
              /**
               * destory modules and other resources and initialize it again
               */
              Context.prototype.reset = function() {
                var disabled = this.isDisabled();
                this.code(dom.emptyPara);
                this._destroy();
                this._initialize();
                if (disabled) {
                  this.disable();
                }
              };
              Context.prototype._initialize = function() {
                var _this = this;
                // add optional buttons
                var buttons = $$1.extend({}, this.options.buttons);
                Object.keys(buttons).forEach(function(key) {
                  _this.memo("button." + key, buttons[key]);
                });
                var modules = $$1.extend(
                  {},
                  this.options.modules,
                  $$1.summernote.plugins || {}
                );
                // add and initialize modules
                Object.keys(modules).forEach(function(key) {
                  _this.module(key, modules[key], true);
                });
                Object.keys(this.modules).forEach(function(key) {
                  _this.initializeModule(key);
                });
              };
              Context.prototype._destroy = function() {
                var _this = this;
                // destroy modules with reversed order
                Object.keys(this.modules)
                  .reverse()
                  .forEach(function(key) {
                    _this.removeModule(key);
                  });
                Object.keys(this.memos).forEach(function(key) {
                  _this.removeMemo(key);
                });
                // trigger custom onDestroy callback
                this.triggerEvent("destroy", this);
              };
              Context.prototype.code = function(html) {
                var isActivated = this.invoke("codeview.isActivated");
                if (html === undefined) {
                  this.invoke("codeview.sync");
                  return isActivated
                    ? this.layoutInfo.codable.val()
                    : this.layoutInfo.editable.html();
                } else {
                  if (isActivated) {
                    this.layoutInfo.codable.val(html);
                  } else {
                    this.layoutInfo.editable.html(html);
                  }
                  this.$note.val(html);
                  this.triggerEvent("change", html, this.layoutInfo.editable);
                }
              };
              Context.prototype.isDisabled = function() {
                return (
                  this.layoutInfo.editable.attr("contenteditable") === "false"
                );
              };
              Context.prototype.enable = function() {
                this.layoutInfo.editable.attr("contenteditable", true);
                this.invoke("toolbar.activate", true);
                this.triggerEvent("disable", false);
              };
              Context.prototype.disable = function() {
                // close codeview if codeview is opend
                if (this.invoke("codeview.isActivated")) {
                  this.invoke("codeview.deactivate");
                }
                this.layoutInfo.editable.attr("contenteditable", false);
                this.invoke("toolbar.deactivate", true);
                this.triggerEvent("disable", true);
              };
              Context.prototype.triggerEvent = function() {
                var namespace = lists.head(arguments);
                var args = lists.tail(lists.from(arguments));
                var callback = this.options.callbacks[
                  func.namespaceToCamel(namespace, "on")
                ];
                if (callback) {
                  callback.apply(this.$note[0], args);
                }
                this.$note.trigger("summernote." + namespace, args);
              };
              Context.prototype.initializeModule = function(key) {
                var module = this.modules[key];
                module.shouldInitialize = module.shouldInitialize || func.ok;
                if (!module.shouldInitialize()) {
                  return;
                }
                // initialize module
                if (module.initialize) {
                  module.initialize();
                }
                // attach events
                if (module.events) {
                  dom.attachEvents(this.$note, module.events);
                }
              };
              Context.prototype.module = function(
                key,
                ModuleClass,
                withoutIntialize
              ) {
                if (arguments.length === 1) {
                  return this.modules[key];
                }
                this.modules[key] = new ModuleClass(this);
                if (!withoutIntialize) {
                  this.initializeModule(key);
                }
              };
              Context.prototype.removeModule = function(key) {
                var module = this.modules[key];
                if (module.shouldInitialize()) {
                  if (module.events) {
                    dom.detachEvents(this.$note, module.events);
                  }
                  if (module.destroy) {
                    module.destroy();
                  }
                }
                delete this.modules[key];
              };
              Context.prototype.memo = function(key, obj) {
                if (arguments.length === 1) {
                  return this.memos[key];
                }
                this.memos[key] = obj;
              };
              Context.prototype.removeMemo = function(key) {
                if (this.memos[key] && this.memos[key].destroy) {
                  this.memos[key].destroy();
                }
                delete this.memos[key];
              };
              /**
               * Some buttons need to change their visual style immediately once they get pressed
               */
              Context.prototype.createInvokeHandlerAndUpdateState = function(
                namespace,
                value
              ) {
                var _this = this;
                return function(event) {
                  _this.createInvokeHandler(namespace, value)(event);
                  _this.invoke("buttons.updateCurrentStyle");
                };
              };
              Context.prototype.createInvokeHandler = function(
                namespace,
                value
              ) {
                var _this = this;
                return function(event) {
                  event.preventDefault();
                  var $target = $$1(event.target);
                  _this.invoke(
                    namespace,
                    value || $target.closest("[data-value]").data("value"),
                    $target
                  );
                };
              };
              Context.prototype.invoke = function() {
                var namespace = lists.head(arguments);
                var args = lists.tail(lists.from(arguments));
                var splits = namespace.split(".");
                var hasSeparator = splits.length > 1;
                var moduleName = hasSeparator && lists.head(splits);
                var methodName = hasSeparator
                  ? lists.last(splits)
                  : lists.head(splits);
                var module = this.modules[moduleName || "editor"];
                if (!moduleName && this[methodName]) {
                  return this[methodName].apply(this, args);
                } else if (
                  module &&
                  module[methodName] &&
                  module.shouldInitialize()
                ) {
                  return module[methodName].apply(module, args);
                }
              };
              return Context;
            })();

            $$1.fn.extend({
              /**
               * Summernote API
               *
               * @param {Object|String}
               * @return {this}
               */
              summernote: function() {
                var type = $$1.type(lists.head(arguments));
                var isExternalAPICalled = type === "string";
                var hasInitOptions = type === "object";
                var options = $$1.extend(
                  {},
                  $$1.summernote.options,
                  hasInitOptions ? lists.head(arguments) : {}
                );
                // Update options
                options.langInfo = $$1.extend(
                  true,
                  {},
                  $$1.summernote.lang["en-US"],
                  $$1.summernote.lang[options.lang]
                );
                options.icons = $$1.extend(
                  true,
                  {},
                  $$1.summernote.options.icons,
                  options.icons
                );
                options.tooltip =
                  options.tooltip === "auto"
                    ? !env.isSupportTouch
                    : options.tooltip;
                this.each(function(idx, note) {
                  var $note = $$1(note);
                  if (!$note.data("summernote")) {
                    var context = new Context($note, options);
                    $note.data("summernote", context);
                    $note
                      .data("summernote")
                      .triggerEvent("init", context.layoutInfo);
                  }
                });
                var $note = this.first();
                if ($note.length) {
                  var context = $note.data("summernote");
                  if (isExternalAPICalled) {
                    return context.invoke.apply(context, lists.from(arguments));
                  } else if (options.focus) {
                    context.invoke("editor.focus");
                  }
                }
                return this;
              }
            });

            /**
             * return boundaryPoint from TextRange, inspired by Andy Na's HuskyRange.js
             *
             * @param {TextRange} textRange
             * @param {Boolean} isStart
             * @return {BoundaryPoint}
             *
             * @see http://msdn.microsoft.com/en-us/library/ie/ms535872(v=vs.85).aspx
             */
            function textRangeToPoint(textRange, isStart) {
              var container = textRange.parentElement();
              var offset;
              var tester = document.body.createTextRange();
              var prevContainer;
              var childNodes = lists.from(container.childNodes);
              for (offset = 0; offset < childNodes.length; offset++) {
                if (dom.isText(childNodes[offset])) {
                  continue;
                }
                tester.moveToElementText(childNodes[offset]);
                if (tester.compareEndPoints("StartToStart", textRange) >= 0) {
                  break;
                }
                prevContainer = childNodes[offset];
              }
              if (offset !== 0 && dom.isText(childNodes[offset - 1])) {
                var textRangeStart = document.body.createTextRange();
                var curTextNode = null;
                textRangeStart.moveToElementText(prevContainer || container);
                textRangeStart.collapse(!prevContainer);
                curTextNode = prevContainer
                  ? prevContainer.nextSibling
                  : container.firstChild;
                var pointTester = textRange.duplicate();
                pointTester.setEndPoint("StartToStart", textRangeStart);
                var textCount = pointTester.text.replace(/[\r\n]/g, "").length;
                while (
                  textCount > curTextNode.nodeValue.length &&
                  curTextNode.nextSibling
                ) {
                  textCount -= curTextNode.nodeValue.length;
                  curTextNode = curTextNode.nextSibling;
                }
                // [workaround] enforce IE to re-reference curTextNode, hack
                var dummy = curTextNode.nodeValue; // eslint-disable-line
                if (
                  isStart &&
                  curTextNode.nextSibling &&
                  dom.isText(curTextNode.nextSibling) &&
                  textCount === curTextNode.nodeValue.length
                ) {
                  textCount -= curTextNode.nodeValue.length;
                  curTextNode = curTextNode.nextSibling;
                }
                container = curTextNode;
                offset = textCount;
              }
              return {
                cont: container,
                offset: offset
              };
            }
            /**
             * return TextRange from boundary point (inspired by google closure-library)
             * @param {BoundaryPoint} point
             * @return {TextRange}
             */
            function pointToTextRange(point) {
              var textRangeInfo = function(container, offset) {
                var node, isCollapseToStart;
                if (dom.isText(container)) {
                  var prevTextNodes = dom.listPrev(
                    container,
                    func.not(dom.isText)
                  );
                  var prevContainer = lists.last(prevTextNodes).previousSibling;
                  node = prevContainer || container.parentNode;
                  offset += lists.sum(
                    lists.tail(prevTextNodes),
                    dom.nodeLength
                  );
                  isCollapseToStart = !prevContainer;
                } else {
                  node = container.childNodes[offset] || container;
                  if (dom.isText(node)) {
                    return textRangeInfo(node, 0);
                  }
                  offset = 0;
                  isCollapseToStart = false;
                }
                return {
                  node: node,
                  collapseToStart: isCollapseToStart,
                  offset: offset
                };
              };
              var textRange = document.body.createTextRange();
              var info = textRangeInfo(point.node, point.offset);
              textRange.moveToElementText(info.node);
              textRange.collapse(info.collapseToStart);
              textRange.moveStart("character", info.offset);
              return textRange;
            }
            /**
             * Wrapped Range
             *
             * @constructor
             * @param {Node} sc - start container
             * @param {Number} so - start offset
             * @param {Node} ec - end container
             * @param {Number} eo - end offset
             */
            var WrappedRange = /** @class */ (function() {
              function WrappedRange(sc, so, ec, eo) {
                this.sc = sc;
                this.so = so;
                this.ec = ec;
                this.eo = eo;
                // isOnEditable: judge whether range is on editable or not
                this.isOnEditable = this.makeIsOn(dom.isEditable);
                // isOnList: judge whether range is on list node or not
                this.isOnList = this.makeIsOn(dom.isList);
                // isOnAnchor: judge whether range is on anchor node or not
                this.isOnAnchor = this.makeIsOn(dom.isAnchor);
                // isOnCell: judge whether range is on cell node or not
                this.isOnCell = this.makeIsOn(dom.isCell);
                // isOnData: judge whether range is on data node or not
                this.isOnData = this.makeIsOn(dom.isData);
              }
              // nativeRange: get nativeRange from sc, so, ec, eo
              WrappedRange.prototype.nativeRange = function() {
                if (env.isW3CRangeSupport) {
                  var w3cRange = document.createRange();
                  w3cRange.setStart(
                    this.sc,
                    this.sc.data && this.so > this.sc.data.length ? 0 : this.so
                  );
                  w3cRange.setEnd(
                    this.ec,
                    this.sc.data
                      ? Math.min(this.eo, this.sc.data.length)
                      : this.eo
                  );
                  return w3cRange;
                } else {
                  var textRange = pointToTextRange({
                    node: this.sc,
                    offset: this.so
                  });
                  textRange.setEndPoint(
                    "EndToEnd",
                    pointToTextRange({
                      node: this.ec,
                      offset: this.eo
                    })
                  );
                  return textRange;
                }
              };
              WrappedRange.prototype.getPoints = function() {
                return {
                  sc: this.sc,
                  so: this.so,
                  ec: this.ec,
                  eo: this.eo
                };
              };
              WrappedRange.prototype.getStartPoint = function() {
                return {
                  node: this.sc,
                  offset: this.so
                };
              };
              WrappedRange.prototype.getEndPoint = function() {
                return {
                  node: this.ec,
                  offset: this.eo
                };
              };
              /**
               * select update visible range
               */
              WrappedRange.prototype.select = function() {
                var nativeRng = this.nativeRange();
                if (env.isW3CRangeSupport) {
                  var selection = document.getSelection();
                  if (selection.rangeCount > 0) {
                    selection.removeAllRanges();
                  }
                  selection.addRange(nativeRng);
                } else {
                  nativeRng.select();
                }
                return this;
              };
              /**
               * Moves the scrollbar to start container(sc) of current range
               *
               * @return {WrappedRange}
               */
              WrappedRange.prototype.scrollIntoView = function(container) {
                var height = $$1(container).height();
                if (container.scrollTop + height < this.sc.offsetTop) {
                  container.scrollTop += Math.abs(
                    container.scrollTop + height - this.sc.offsetTop
                  );
                }
                return this;
              };
              /**
               * @return {WrappedRange}
               */
              WrappedRange.prototype.normalize = function() {
                /**
                 * @param {BoundaryPoint} point
                 * @param {Boolean} isLeftToRight - true: prefer to choose right node
                 *                                - false: prefer to choose left node
                 * @return {BoundaryPoint}
                 */
                var getVisiblePoint = function(point, isLeftToRight) {
                  // Just use the given point [XXX:Adhoc]
                  //  - case 01. if the point is on the middle of the node
                  //  - case 02. if the point is on the right edge and prefer to choose left node
                  //  - case 03. if the point is on the left edge and prefer to choose right node
                  //  - case 04. if the point is on the right edge and prefer to choose right node but the node is void
                  //  - case 05. if the point is on the left edge and prefer to choose left node but the node is void
                  //  - case 06. if the point is on the block node and there is no children
                  if (dom.isVisiblePoint(point)) {
                    if (
                      !dom.isEdgePoint(point) ||
                      (dom.isRightEdgePoint(point) && !isLeftToRight) ||
                      (dom.isLeftEdgePoint(point) && isLeftToRight) ||
                      (dom.isRightEdgePoint(point) &&
                        isLeftToRight &&
                        dom.isVoid(point.node.nextSibling)) ||
                      (dom.isLeftEdgePoint(point) &&
                        !isLeftToRight &&
                        dom.isVoid(point.node.previousSibling)) ||
                      (dom.isBlock(point.node) && dom.isEmpty(point.node))
                    ) {
                      return point;
                    }
                  }
                  // point on block's edge
                  var block = dom.ancestor(point.node, dom.isBlock);
                  if (
                    ((dom.isLeftEdgePointOf(point, block) ||
                      dom.isVoid(dom.prevPoint(point).node)) &&
                      !isLeftToRight) ||
                    ((dom.isRightEdgePointOf(point, block) ||
                      dom.isVoid(dom.nextPoint(point).node)) &&
                      isLeftToRight)
                  ) {
                    // returns point already on visible point
                    if (dom.isVisiblePoint(point)) {
                      return point;
                    }
                    // reverse direction
                    isLeftToRight = !isLeftToRight;
                  }
                  var nextPoint = isLeftToRight
                    ? dom.nextPointUntil(
                        dom.nextPoint(point),
                        dom.isVisiblePoint
                      )
                    : dom.prevPointUntil(
                        dom.prevPoint(point),
                        dom.isVisiblePoint
                      );
                  return nextPoint || point;
                };
                var endPoint = getVisiblePoint(this.getEndPoint(), false);
                var startPoint = this.isCollapsed()
                  ? endPoint
                  : getVisiblePoint(this.getStartPoint(), true);
                return new WrappedRange(
                  startPoint.node,
                  startPoint.offset,
                  endPoint.node,
                  endPoint.offset
                );
              };
              /**
               * returns matched nodes on range
               *
               * @param {Function} [pred] - predicate function
               * @param {Object} [options]
               * @param {Boolean} [options.includeAncestor]
               * @param {Boolean} [options.fullyContains]
               * @return {Node[]}
               */
              WrappedRange.prototype.nodes = function(pred, options) {
                pred = pred || func.ok;
                var includeAncestor = options && options.includeAncestor;
                var fullyContains = options && options.fullyContains;
                // TODO compare points and sort
                var startPoint = this.getStartPoint();
                var endPoint = this.getEndPoint();
                var nodes = [];
                var leftEdgeNodes = [];
                dom.walkPoint(
                  startPoint,
                  endPoint,
                  function(point) {
                    if (dom.isEditable(point.node)) {
                      return;
                    }
                    var node;
                    if (fullyContains) {
                      if (dom.isLeftEdgePoint(point)) {
                        leftEdgeNodes.push(point.node);
                      }
                      if (
                        dom.isRightEdgePoint(point) &&
                        lists.contains(leftEdgeNodes, point.node)
                      ) {
                        node = point.node;
                      }
                    } else if (includeAncestor) {
                      node = dom.ancestor(point.node, pred);
                    } else {
                      node = point.node;
                    }
                    if (node && pred(node)) {
                      nodes.push(node);
                    }
                  },
                  true
                );
                return lists.unique(nodes);
              };
              /**
               * returns commonAncestor of range
               * @return {Element} - commonAncestor
               */
              WrappedRange.prototype.commonAncestor = function() {
                return dom.commonAncestor(this.sc, this.ec);
              };
              /**
               * returns expanded range by pred
               *
               * @param {Function} pred - predicate function
               * @return {WrappedRange}
               */
              WrappedRange.prototype.expand = function(pred) {
                var startAncestor = dom.ancestor(this.sc, pred);
                var endAncestor = dom.ancestor(this.ec, pred);
                if (!startAncestor && !endAncestor) {
                  return new WrappedRange(this.sc, this.so, this.ec, this.eo);
                }
                var boundaryPoints = this.getPoints();
                if (startAncestor) {
                  boundaryPoints.sc = startAncestor;
                  boundaryPoints.so = 0;
                }
                if (endAncestor) {
                  boundaryPoints.ec = endAncestor;
                  boundaryPoints.eo = dom.nodeLength(endAncestor);
                }
                return new WrappedRange(
                  boundaryPoints.sc,
                  boundaryPoints.so,
                  boundaryPoints.ec,
                  boundaryPoints.eo
                );
              };
              /**
               * @param {Boolean} isCollapseToStart
               * @return {WrappedRange}
               */
              WrappedRange.prototype.collapse = function(isCollapseToStart) {
                if (isCollapseToStart) {
                  return new WrappedRange(this.sc, this.so, this.sc, this.so);
                } else {
                  return new WrappedRange(this.ec, this.eo, this.ec, this.eo);
                }
              };
              /**
               * splitText on range
               */
              WrappedRange.prototype.splitText = function() {
                var isSameContainer = this.sc === this.ec;
                var boundaryPoints = this.getPoints();
                if (
                  dom.isText(this.ec) &&
                  !dom.isEdgePoint(this.getEndPoint())
                ) {
                  this.ec.splitText(this.eo);
                }
                if (
                  dom.isText(this.sc) &&
                  !dom.isEdgePoint(this.getStartPoint())
                ) {
                  boundaryPoints.sc = this.sc.splitText(this.so);
                  boundaryPoints.so = 0;
                  if (isSameContainer) {
                    boundaryPoints.ec = boundaryPoints.sc;
                    boundaryPoints.eo = this.eo - this.so;
                  }
                }
                return new WrappedRange(
                  boundaryPoints.sc,
                  boundaryPoints.so,
                  boundaryPoints.ec,
                  boundaryPoints.eo
                );
              };
              /**
               * delete contents on range
               * @return {WrappedRange}
               */
              WrappedRange.prototype.deleteContents = function() {
                if (this.isCollapsed()) {
                  return this;
                }
                var rng = this.splitText();
                var nodes = rng.nodes(null, {
                  fullyContains: true
                });
                // find new cursor point
                var point = dom.prevPointUntil(rng.getStartPoint(), function(
                  point
                ) {
                  return !lists.contains(nodes, point.node);
                });
                var emptyParents = [];
                $$1.each(nodes, function(idx, node) {
                  // find empty parents
                  var parent = node.parentNode;
                  if (point.node !== parent && dom.nodeLength(parent) === 1) {
                    emptyParents.push(parent);
                  }
                  dom.remove(node, false);
                });
                // remove empty parents
                $$1.each(emptyParents, function(idx, node) {
                  dom.remove(node, false);
                });
                return new WrappedRange(
                  point.node,
                  point.offset,
                  point.node,
                  point.offset
                ).normalize();
              };
              /**
               * makeIsOn: return isOn(pred) function
               */
              WrappedRange.prototype.makeIsOn = function(pred) {
                return function() {
                  var ancestor = dom.ancestor(this.sc, pred);
                  return !!ancestor && ancestor === dom.ancestor(this.ec, pred);
                };
              };
              /**
               * @param {Function} pred
               * @return {Boolean}
               */
              WrappedRange.prototype.isLeftEdgeOf = function(pred) {
                if (!dom.isLeftEdgePoint(this.getStartPoint())) {
                  return false;
                }
                var node = dom.ancestor(this.sc, pred);
                return node && dom.isLeftEdgeOf(this.sc, node);
              };
              /**
               * returns whether range was collapsed or not
               */
              WrappedRange.prototype.isCollapsed = function() {
                return this.sc === this.ec && this.so === this.eo;
              };
              /**
               * wrap inline nodes which children of body with paragraph
               *
               * @return {WrappedRange}
               */
              WrappedRange.prototype.wrapBodyInlineWithPara = function() {
                if (dom.isBodyContainer(this.sc) && dom.isEmpty(this.sc)) {
                  this.sc.innerHTML = dom.emptyPara;
                  return new WrappedRange(
                    this.sc.firstChild,
                    0,
                    this.sc.firstChild,
                    0
                  );
                }
                /**
                 * [workaround] firefox often create range on not visible point. so normalize here.
                 *  - firefox: |<p>text</p>|
                 *  - chrome: <p>|text|</p>
                 */
                var rng = this.normalize();
                if (dom.isParaInline(this.sc) || dom.isPara(this.sc)) {
                  return rng;
                }
                // find inline top ancestor
                var topAncestor;
                if (dom.isInline(rng.sc)) {
                  var ancestors = dom.listAncestor(
                    rng.sc,
                    func.not(dom.isInline)
                  );
                  topAncestor = lists.last(ancestors);
                  if (!dom.isInline(topAncestor)) {
                    topAncestor =
                      ancestors[ancestors.length - 2] ||
                      rng.sc.childNodes[rng.so];
                  }
                } else {
                  topAncestor = rng.sc.childNodes[rng.so > 0 ? rng.so - 1 : 0];
                }
                // siblings not in paragraph
                var inlineSiblings = dom
                  .listPrev(topAncestor, dom.isParaInline)
                  .reverse();
                inlineSiblings = inlineSiblings.concat(
                  dom.listNext(topAncestor.nextSibling, dom.isParaInline)
                );
                // wrap with paragraph
                if (inlineSiblings.length) {
                  var para = dom.wrap(lists.head(inlineSiblings), "p");
                  dom.appendChildNodes(para, lists.tail(inlineSiblings));
                }
                return this.normalize();
              };
              /**
               * insert node at current cursor
               *
               * @param {Node} node
               * @return {Node}
               */
              WrappedRange.prototype.insertNode = function(node) {
                var rng = this.wrapBodyInlineWithPara().deleteContents();
                var info = dom.splitPoint(
                  rng.getStartPoint(),
                  dom.isInline(node)
                );
                if (info.rightNode) {
                  info.rightNode.parentNode.insertBefore(node, info.rightNode);
                } else {
                  info.container.appendChild(node);
                }
                return node;
              };
              /**
               * insert html at current cursor
               */
              WrappedRange.prototype.pasteHTML = function(markup) {
                var contentsContainer = $$1("<div></div>").html(markup)[0];
                var childNodes = lists.from(contentsContainer.childNodes);
                var rng = this.wrapBodyInlineWithPara().deleteContents();
                if (rng.so > 0) {
                  childNodes = childNodes.reverse();
                }
                childNodes = childNodes.map(function(childNode) {
                  return rng.insertNode(childNode);
                });
                if (rng.so > 0) {
                  childNodes = childNodes.reverse();
                }
                return childNodes;
              };
              /**
               * returns text in range
               *
               * @return {String}
               */
              WrappedRange.prototype.toString = function() {
                var nativeRng = this.nativeRange();
                return env.isW3CRangeSupport
                  ? nativeRng.toString()
                  : nativeRng.text;
              };
              /**
               * returns range for word before cursor
               *
               * @param {Boolean} [findAfter] - find after cursor, default: false
               * @return {WrappedRange}
               */
              WrappedRange.prototype.getWordRange = function(findAfter) {
                var endPoint = this.getEndPoint();
                if (!dom.isCharPoint(endPoint)) {
                  return this;
                }
                var startPoint = dom.prevPointUntil(endPoint, function(point) {
                  return !dom.isCharPoint(point);
                });
                if (findAfter) {
                  endPoint = dom.nextPointUntil(endPoint, function(point) {
                    return !dom.isCharPoint(point);
                  });
                }
                return new WrappedRange(
                  startPoint.node,
                  startPoint.offset,
                  endPoint.node,
                  endPoint.offset
                );
              };
              /**
               * create offsetPath bookmark
               *
               * @param {Node} editable
               */
              WrappedRange.prototype.bookmark = function(editable) {
                return {
                  s: {
                    path: dom.makeOffsetPath(editable, this.sc),
                    offset: this.so
                  },
                  e: {
                    path: dom.makeOffsetPath(editable, this.ec),
                    offset: this.eo
                  }
                };
              };
              /**
               * create offsetPath bookmark base on paragraph
               *
               * @param {Node[]} paras
               */
              WrappedRange.prototype.paraBookmark = function(paras) {
                return {
                  s: {
                    path: lists.tail(
                      dom.makeOffsetPath(lists.head(paras), this.sc)
                    ),
                    offset: this.so
                  },
                  e: {
                    path: lists.tail(
                      dom.makeOffsetPath(lists.last(paras), this.ec)
                    ),
                    offset: this.eo
                  }
                };
              };
              /**
               * getClientRects
               * @return {Rect[]}
               */
              WrappedRange.prototype.getClientRects = function() {
                var nativeRng = this.nativeRange();
                return nativeRng.getClientRects();
              };
              return WrappedRange;
            })();
            /**
             * Data structure
             *  * BoundaryPoint: a point of dom tree
             *  * BoundaryPoints: two boundaryPoints corresponding to the start and the end of the Range
             *
             * See to http://www.w3.org/TR/DOM-Level-2-Traversal-Range/ranges.html#Level-2-Range-Position
             */
            var range = {
              /**
               * create Range Object From arguments or Browser Selection
               *
               * @param {Node} sc - start container
               * @param {Number} so - start offset
               * @param {Node} ec - end container
               * @param {Number} eo - end offset
               * @return {WrappedRange}
               */
              create: function(sc, so, ec, eo) {
                if (arguments.length === 4) {
                  return new WrappedRange(sc, so, ec, eo);
                } else if (arguments.length === 2) {
                  // collapsed
                  ec = sc;
                  eo = so;
                  return new WrappedRange(sc, so, ec, eo);
                } else {
                  var wrappedRange = this.createFromSelection();
                  if (!wrappedRange && arguments.length === 1) {
                    wrappedRange = this.createFromNode(arguments[0]);
                    return wrappedRange.collapse(
                      dom.emptyPara === arguments[0].innerHTML
                    );
                  }
                  return wrappedRange;
                }
              },
              createFromSelection: function() {
                var sc, so, ec, eo;
                if (env.isW3CRangeSupport) {
                  var selection = document.getSelection();
                  if (!selection || selection.rangeCount === 0) {
                    return null;
                  } else if (dom.isBody(selection.anchorNode)) {
                    // Firefox: returns entire body as range on initialization.
                    // We won't never need it.
                    return null;
                  }
                  var nativeRng = selection.getRangeAt(0);
                  sc = nativeRng.startContainer;
                  so = nativeRng.startOffset;
                  ec = nativeRng.endContainer;
                  eo = nativeRng.endOffset;
                } else {
                  // IE8: TextRange
                  var textRange = document.selection.createRange();
                  var textRangeEnd = textRange.duplicate();
                  textRangeEnd.collapse(false);
                  var textRangeStart = textRange;
                  textRangeStart.collapse(true);
                  var startPoint = textRangeToPoint(textRangeStart, true);
                  var endPoint = textRangeToPoint(textRangeEnd, false);
                  // same visible point case: range was collapsed.
                  if (
                    dom.isText(startPoint.node) &&
                    dom.isLeftEdgePoint(startPoint) &&
                    dom.isTextNode(endPoint.node) &&
                    dom.isRightEdgePoint(endPoint) &&
                    endPoint.node.nextSibling === startPoint.node
                  ) {
                    startPoint = endPoint;
                  }
                  sc = startPoint.cont;
                  so = startPoint.offset;
                  ec = endPoint.cont;
                  eo = endPoint.offset;
                }
                return new WrappedRange(sc, so, ec, eo);
              },
              /**
               * @method
               *
               * create WrappedRange from node
               *
               * @param {Node} node
               * @return {WrappedRange}
               */
              createFromNode: function(node) {
                var sc = node;
                var so = 0;
                var ec = node;
                var eo = dom.nodeLength(ec);
                // browsers can't target a picture or void node
                if (dom.isVoid(sc)) {
                  so = dom.listPrev(sc).length - 1;
                  sc = sc.parentNode;
                }
                if (dom.isBR(ec)) {
                  eo = dom.listPrev(ec).length - 1;
                  ec = ec.parentNode;
                } else if (dom.isVoid(ec)) {
                  eo = dom.listPrev(ec).length;
                  ec = ec.parentNode;
                }
                return this.create(sc, so, ec, eo);
              },
              /**
               * create WrappedRange from node after position
               *
               * @param {Node} node
               * @return {WrappedRange}
               */
              createFromNodeBefore: function(node) {
                return this.createFromNode(node).collapse(true);
              },
              /**
               * create WrappedRange from node after position
               *
               * @param {Node} node
               * @return {WrappedRange}
               */
              createFromNodeAfter: function(node) {
                return this.createFromNode(node).collapse();
              },
              /**
               * @method
               *
               * create WrappedRange from bookmark
               *
               * @param {Node} editable
               * @param {Object} bookmark
               * @return {WrappedRange}
               */
              createFromBookmark: function(editable, bookmark) {
                var sc = dom.fromOffsetPath(editable, bookmark.s.path);
                var so = bookmark.s.offset;
                var ec = dom.fromOffsetPath(editable, bookmark.e.path);
                var eo = bookmark.e.offset;
                return new WrappedRange(sc, so, ec, eo);
              },
              /**
               * @method
               *
               * create WrappedRange from paraBookmark
               *
               * @param {Object} bookmark
               * @param {Node[]} paras
               * @return {WrappedRange}
               */
              createFromParaBookmark: function(bookmark, paras) {
                var so = bookmark.s.offset;
                var eo = bookmark.e.offset;
                var sc = dom.fromOffsetPath(lists.head(paras), bookmark.s.path);
                var ec = dom.fromOffsetPath(lists.last(paras), bookmark.e.path);
                return new WrappedRange(sc, so, ec, eo);
              }
            };

            var KEY_MAP = {
              BACKSPACE: 8,
              TAB: 9,
              ENTER: 13,
              SPACE: 32,
              DELETE: 46,
              // Arrow
              LEFT: 37,
              UP: 38,
              RIGHT: 39,
              DOWN: 40,
              // Number: 0-9
              NUM0: 48,
              NUM1: 49,
              NUM2: 50,
              NUM3: 51,
              NUM4: 52,
              NUM5: 53,
              NUM6: 54,
              NUM7: 55,
              NUM8: 56,
              // Alphabet: a-z
              B: 66,
              E: 69,
              I: 73,
              J: 74,
              K: 75,
              L: 76,
              R: 82,
              S: 83,
              U: 85,
              V: 86,
              Y: 89,
              Z: 90,
              SLASH: 191,
              LEFTBRACKET: 219,
              BACKSLASH: 220,
              RIGHTBRACKET: 221
            };
            /**
             * @class core.key
             *
             * Object for keycodes.
             *
             * @singleton
             * @alternateClassName key
             */
            var key = {
              /**
               * @method isEdit
               *
               * @param {Number} keyCode
               * @return {Boolean}
               */
              isEdit: function(keyCode) {
                return lists.contains(
                  [
                    KEY_MAP.BACKSPACE,
                    KEY_MAP.TAB,
                    KEY_MAP.ENTER,
                    KEY_MAP.SPACE,
                    KEY_MAP.DELETE
                  ],
                  keyCode
                );
              },
              /**
               * @method isMove
               *
               * @param {Number} keyCode
               * @return {Boolean}
               */
              isMove: function(keyCode) {
                return lists.contains(
                  [KEY_MAP.LEFT, KEY_MAP.UP, KEY_MAP.RIGHT, KEY_MAP.DOWN],
                  keyCode
                );
              },
              /**
               * @property {Object} nameFromCode
               * @property {String} nameFromCode.8 "BACKSPACE"
               */
              nameFromCode: func.invertObject(KEY_MAP),
              code: KEY_MAP
            };

            /**
             * @method readFileAsDataURL
             *
             * read contents of file as representing URL
             *
             * @param {File} file
             * @return {Promise} - then: dataUrl
             */
            function readFileAsDataURL(file) {
              return $$1
                .Deferred(function(deferred) {
                  $$1
                    .extend(new FileReader(), {
                      onload: function(e) {
                        var dataURL = e.target.result;
                        deferred.resolve(dataURL);
                      },
                      onerror: function(err) {
                        deferred.reject(err);
                      }
                    })
                    .readAsDataURL(file);
                })
                .promise();
            }
            /**
             * @method createImage
             *
             * create `<image>` from url string
             *
             * @param {String} url
             * @return {Promise} - then: $image
             */
            function createImage(url) {
              return $$1
                .Deferred(function(deferred) {
                  var $img = $$1("<img>");
                  $img
                    .one("load", function() {
                      $img.off("error abort");
                      deferred.resolve($img);
                    })
                    .one("error abort", function() {
                      $img.off("load").detach();
                      deferred.reject($img);
                    })
                    .css({
                      display: "none"
                    })
                    .appendTo(document.body)
                    .attr("src", url);
                })
                .promise();
            }

            var History = /** @class */ (function() {
              function History($editable) {
                this.stack = [];
                this.stackOffset = -1;
                this.$editable = $editable;
                this.editable = $editable[0];
              }
              History.prototype.makeSnapshot = function() {
                var rng = range.create(this.editable);
                var emptyBookmark = {
                  s: { path: [], offset: 0 },
                  e: { path: [], offset: 0 }
                };
                return {
                  contents: this.$editable.html(),
                  bookmark:
                    rng && rng.isOnEditable()
                      ? rng.bookmark(this.editable)
                      : emptyBookmark
                };
              };
              History.prototype.applySnapshot = function(snapshot) {
                if (snapshot.contents !== null) {
                  this.$editable.html(snapshot.contents);
                }
                if (snapshot.bookmark !== null) {
                  range
                    .createFromBookmark(this.editable, snapshot.bookmark)
                    .select();
                }
              };
              /**
               * @method rewind
               * Rewinds the history stack back to the first snapshot taken.
               * Leaves the stack intact, so that "Redo" can still be used.
               */
              History.prototype.rewind = function() {
                // Create snap shot if not yet recorded
                if (
                  this.$editable.html() !==
                  this.stack[this.stackOffset].contents
                ) {
                  this.recordUndo();
                }
                // Return to the first available snapshot.
                this.stackOffset = 0;
                // Apply that snapshot.
                this.applySnapshot(this.stack[this.stackOffset]);
              };
              /**
               *  @method commit
               *  Resets history stack, but keeps current editor's content.
               */
              History.prototype.commit = function() {
                // Clear the stack.
                this.stack = [];
                // Restore stackOffset to its original value.
                this.stackOffset = -1;
                // Record our first snapshot (of nothing).
                this.recordUndo();
              };
              /**
               * @method reset
               * Resets the history stack completely; reverting to an empty editor.
               */
              History.prototype.reset = function() {
                // Clear the stack.
                this.stack = [];
                // Restore stackOffset to its original value.
                this.stackOffset = -1;
                // Clear the editable area.
                this.$editable.html("");
                // Record our first snapshot (of nothing).
                this.recordUndo();
              };
              /**
               * undo
               */
              History.prototype.undo = function() {
                // Create snap shot if not yet recorded
                if (
                  this.$editable.html() !==
                  this.stack[this.stackOffset].contents
                ) {
                  this.recordUndo();
                }
                if (this.stackOffset > 0) {
                  this.stackOffset--;
                  this.applySnapshot(this.stack[this.stackOffset]);
                }
              };
              /**
               * redo
               */
              History.prototype.redo = function() {
                if (this.stack.length - 1 > this.stackOffset) {
                  this.stackOffset++;
                  this.applySnapshot(this.stack[this.stackOffset]);
                }
              };
              /**
               * recorded undo
               */
              History.prototype.recordUndo = function() {
                this.stackOffset++;
                // Wash out stack after stackOffset
                if (this.stack.length > this.stackOffset) {
                  this.stack = this.stack.slice(0, this.stackOffset);
                }
                // Create new snapshot and push it to the end
                this.stack.push(this.makeSnapshot());
              };
              return History;
            })();

            var Style = /** @class */ (function() {
              function Style() {}
              /**
               * @method jQueryCSS
               *
               * [workaround] for old jQuery
               * passing an array of style properties to .css()
               * will result in an object of property-value pairs.
               * (compability with version < 1.9)
               *
               * @private
               * @param  {jQuery} $obj
               * @param  {Array} propertyNames - An array of one or more CSS properties.
               * @return {Object}
               */
              Style.prototype.jQueryCSS = function($obj, propertyNames) {
                if (env.jqueryVersion < 1.9) {
                  var result_1 = {};
                  $$1.each(propertyNames, function(idx, propertyName) {
                    result_1[propertyName] = $obj.css(propertyName);
                  });
                  return result_1;
                }
                return $obj.css(propertyNames);
              };
              /**
               * returns style object from node
               *
               * @param {jQuery} $node
               * @return {Object}
               */
              Style.prototype.fromNode = function($node) {
                var properties = [
                  "font-family",
                  "font-size",
                  "text-align",
                  "list-style-type",
                  "line-height"
                ];
                var styleInfo = this.jQueryCSS($node, properties) || {};
                styleInfo["font-size"] = parseInt(styleInfo["font-size"], 10);
                return styleInfo;
              };
              /**
               * paragraph level style
               *
               * @param {WrappedRange} rng
               * @param {Object} styleInfo
               */
              Style.prototype.stylePara = function(rng, styleInfo) {
                $$1.each(
                  rng.nodes(dom.isPara, {
                    includeAncestor: true
                  }),
                  function(idx, para) {
                    $$1(para).css(styleInfo);
                  }
                );
              };
              /**
               * insert and returns styleNodes on range.
               *
               * @param {WrappedRange} rng
               * @param {Object} [options] - options for styleNodes
               * @param {String} [options.nodeName] - default: `SPAN`
               * @param {Boolean} [options.expandClosestSibling] - default: `false`
               * @param {Boolean} [options.onlyPartialContains] - default: `false`
               * @return {Node[]}
               */
              Style.prototype.styleNodes = function(rng, options) {
                rng = rng.splitText();
                var nodeName = (options && options.nodeName) || "SPAN";
                var expandClosestSibling = !!(
                  options && options.expandClosestSibling
                );
                var onlyPartialContains = !!(
                  options && options.onlyPartialContains
                );
                if (rng.isCollapsed()) {
                  return [rng.insertNode(dom.create(nodeName))];
                }
                var pred = dom.makePredByNodeName(nodeName);
                var nodes = rng
                  .nodes(dom.isText, {
                    fullyContains: true
                  })
                  .map(function(text) {
                    return (
                      dom.singleChildAncestor(text, pred) ||
                      dom.wrap(text, nodeName)
                    );
                  });
                if (expandClosestSibling) {
                  if (onlyPartialContains) {
                    var nodesInRange_1 = rng.nodes();
                    // compose with partial contains predication
                    pred = func.and(pred, function(node) {
                      return lists.contains(nodesInRange_1, node);
                    });
                  }
                  return nodes.map(function(node) {
                    var siblings = dom.withClosestSiblings(node, pred);
                    var head = lists.head(siblings);
                    var tails = lists.tail(siblings);
                    $$1.each(tails, function(idx, elem) {
                      dom.appendChildNodes(head, elem.childNodes);
                      dom.remove(elem);
                    });
                    return lists.head(siblings);
                  });
                } else {
                  return nodes;
                }
              };
              /**
               * get current style on cursor
               *
               * @param {WrappedRange} rng
               * @return {Object} - object contains style properties.
               */
              Style.prototype.current = function(rng) {
                var $cont = $$1(
                  !dom.isElement(rng.sc) ? rng.sc.parentNode : rng.sc
                );
                var styleInfo = this.fromNode($cont);
                // document.queryCommandState for toggle state
                // [workaround] prevent Firefox nsresult: "0x80004005 (NS_ERROR_FAILURE)"
                try {
                  styleInfo = $$1.extend(styleInfo, {
                    "font-bold": document.queryCommandState("bold")
                      ? "bold"
                      : "normal",
                    "font-italic": document.queryCommandState("italic")
                      ? "italic"
                      : "normal",
                    "font-underline": document.queryCommandState("underline")
                      ? "underline"
                      : "normal",
                    "font-subscript": document.queryCommandState("subscript")
                      ? "subscript"
                      : "normal",
                    "font-superscript": document.queryCommandState(
                      "superscript"
                    )
                      ? "superscript"
                      : "normal",
                    "font-strikethrough": document.queryCommandState(
                      "strikethrough"
                    )
                      ? "strikethrough"
                      : "normal",
                    "font-family":
                      document.queryCommandValue("fontname") ||
                      styleInfo["font-family"]
                  });
                } catch (e) {}
                // list-style-type to list-style(unordered, ordered)
                if (!rng.isOnList()) {
                  styleInfo["list-style"] = "none";
                } else {
                  var orderedTypes = [
                    "circle",
                    "disc",
                    "disc-leading-zero",
                    "square"
                  ];
                  var isUnordered =
                    orderedTypes.indexOf(styleInfo["list-style-type"]) > -1;
                  styleInfo["list-style"] = isUnordered
                    ? "unordered"
                    : "ordered";
                }
                var para = dom.ancestor(rng.sc, dom.isPara);
                if (para && para.style["line-height"]) {
                  styleInfo["line-height"] = para.style.lineHeight;
                } else {
                  var lineHeight =
                    parseInt(styleInfo["line-height"], 10) /
                    parseInt(styleInfo["font-size"], 10);
                  styleInfo["line-height"] = lineHeight.toFixed(1);
                }
                styleInfo.anchor =
                  rng.isOnAnchor() && dom.ancestor(rng.sc, dom.isAnchor);
                styleInfo.ancestors = dom.listAncestor(rng.sc, dom.isEditable);
                styleInfo.range = rng;
                return styleInfo;
              };
              return Style;
            })();

            var Bullet = /** @class */ (function() {
              function Bullet() {}
              /**
               * toggle ordered list
               */
              Bullet.prototype.insertOrderedList = function(editable) {
                this.toggleList("OL", editable);
              };
              /**
               * toggle unordered list
               */
              Bullet.prototype.insertUnorderedList = function(editable) {
                this.toggleList("UL", editable);
              };
              /**
               * indent
               */
              Bullet.prototype.indent = function(editable) {
                var _this = this;
                var rng = range.create(editable).wrapBodyInlineWithPara();
                var paras = rng.nodes(dom.isPara, { includeAncestor: true });
                var clustereds = lists.clusterBy(
                  paras,
                  func.peq2("parentNode")
                );
                $$1.each(clustereds, function(idx, paras) {
                  var head = lists.head(paras);
                  if (dom.isLi(head)) {
                    var previousList_1 = _this.findList(head.previousSibling);
                    if (previousList_1) {
                      paras.map(function(para) {
                        return previousList_1.appendChild(para);
                      });
                    } else {
                      _this.wrapList(paras, head.parentNode.nodeName);
                      paras
                        .map(function(para) {
                          return para.parentNode;
                        })
                        .map(function(para) {
                          return _this.appendToPrevious(para);
                        });
                    }
                  } else {
                    $$1.each(paras, function(idx, para) {
                      $$1(para).css("marginLeft", function(idx, val) {
                        return (parseInt(val, 10) || 0) + 25;
                      });
                    });
                  }
                });
                rng.select();
              };
              /**
               * outdent
               */
              Bullet.prototype.outdent = function(editable) {
                var _this = this;
                var rng = range.create(editable).wrapBodyInlineWithPara();
                var paras = rng.nodes(dom.isPara, { includeAncestor: true });
                var clustereds = lists.clusterBy(
                  paras,
                  func.peq2("parentNode")
                );
                $$1.each(clustereds, function(idx, paras) {
                  var head = lists.head(paras);
                  if (dom.isLi(head)) {
                    _this.releaseList([paras]);
                  } else {
                    $$1.each(paras, function(idx, para) {
                      $$1(para).css("marginLeft", function(idx, val) {
                        val = parseInt(val, 10) || 0;
                        return val > 25 ? val - 25 : "";
                      });
                    });
                  }
                });
                rng.select();
              };
              /**
               * toggle list
               *
               * @param {String} listName - OL or UL
               */
              Bullet.prototype.toggleList = function(listName, editable) {
                var _this = this;
                var rng = range.create(editable).wrapBodyInlineWithPara();
                var paras = rng.nodes(dom.isPara, { includeAncestor: true });
                var bookmark = rng.paraBookmark(paras);
                var clustereds = lists.clusterBy(
                  paras,
                  func.peq2("parentNode")
                );
                // paragraph to list
                if (lists.find(paras, dom.isPurePara)) {
                  var wrappedParas_1 = [];
                  $$1.each(clustereds, function(idx, paras) {
                    wrappedParas_1 = wrappedParas_1.concat(
                      _this.wrapList(paras, listName)
                    );
                  });
                  paras = wrappedParas_1;
                  // list to paragraph or change list style
                } else {
                  var diffLists = rng
                    .nodes(dom.isList, {
                      includeAncestor: true
                    })
                    .filter(function(listNode) {
                      return !$$1.nodeName(listNode, listName);
                    });
                  if (diffLists.length) {
                    $$1.each(diffLists, function(idx, listNode) {
                      dom.replace(listNode, listName);
                    });
                  } else {
                    paras = this.releaseList(clustereds, true);
                  }
                }
                range.createFromParaBookmark(bookmark, paras).select();
              };
              /**
               * @param {Node[]} paras
               * @param {String} listName
               * @return {Node[]}
               */
              Bullet.prototype.wrapList = function(paras, listName) {
                var head = lists.head(paras);
                var last = lists.last(paras);
                var prevList =
                  dom.isList(head.previousSibling) && head.previousSibling;
                var nextList = dom.isList(last.nextSibling) && last.nextSibling;
                var listNode =
                  prevList ||
                  dom.insertAfter(dom.create(listName || "UL"), last);
                // P to LI
                paras = paras.map(function(para) {
                  return dom.isPurePara(para) ? dom.replace(para, "LI") : para;
                });
                // append to list(<ul>, <ol>)
                dom.appendChildNodes(listNode, paras);
                if (nextList) {
                  dom.appendChildNodes(
                    listNode,
                    lists.from(nextList.childNodes)
                  );
                  dom.remove(nextList);
                }
                return paras;
              };
              /**
               * @method releaseList
               *
               * @param {Array[]} clustereds
               * @param {Boolean} isEscapseToBody
               * @return {Node[]}
               */
              Bullet.prototype.releaseList = function(
                clustereds,
                isEscapseToBody
              ) {
                var _this = this;
                var releasedParas = [];
                $$1.each(clustereds, function(idx, paras) {
                  var head = lists.head(paras);
                  var last = lists.last(paras);
                  var headList = isEscapseToBody
                    ? dom.lastAncestor(head, dom.isList)
                    : head.parentNode;
                  var parentItem = headList.parentNode;
                  if (headList.parentNode.nodeName === "LI") {
                    paras.map(function(para) {
                      var newList = _this.findNextSiblings(para);
                      if (parentItem.nextSibling) {
                        parentItem.parentNode.insertBefore(
                          para,
                          parentItem.nextSibling
                        );
                      } else {
                        parentItem.parentNode.appendChild(para);
                      }
                      if (newList.length) {
                        _this.wrapList(newList, headList.nodeName);
                        para.appendChild(newList[0].parentNode);
                      }
                    });
                    if (headList.children.length === 0) {
                      parentItem.removeChild(headList);
                    }
                    if (parentItem.childNodes.length === 0) {
                      parentItem.parentNode.removeChild(parentItem);
                    }
                  } else {
                    var lastList =
                      headList.childNodes.length > 1
                        ? dom.splitTree(
                            headList,
                            {
                              node: last.parentNode,
                              offset: dom.position(last) + 1
                            },
                            {
                              isSkipPaddingBlankHTML: true
                            }
                          )
                        : null;
                    var middleList = dom.splitTree(
                      headList,
                      {
                        node: head.parentNode,
                        offset: dom.position(head)
                      },
                      {
                        isSkipPaddingBlankHTML: true
                      }
                    );
                    paras = isEscapseToBody
                      ? dom.listDescendant(middleList, dom.isLi)
                      : lists.from(middleList.childNodes).filter(dom.isLi);
                    // LI to P
                    if (isEscapseToBody || !dom.isList(headList.parentNode)) {
                      paras = paras.map(function(para) {
                        return dom.replace(para, "P");
                      });
                    }
                    $$1.each(lists.from(paras).reverse(), function(idx, para) {
                      dom.insertAfter(para, headList);
                    });
                    // remove empty lists
                    var rootLists = lists.compact([
                      headList,
                      middleList,
                      lastList
                    ]);
                    $$1.each(rootLists, function(idx, rootList) {
                      var listNodes = [rootList].concat(
                        dom.listDescendant(rootList, dom.isList)
                      );
                      $$1.each(listNodes.reverse(), function(idx, listNode) {
                        if (!dom.nodeLength(listNode)) {
                          dom.remove(listNode, true);
                        }
                      });
                    });
                  }
                  releasedParas = releasedParas.concat(paras);
                });
                return releasedParas;
              };
              /**
               * @method appendToPrevious
               *
               * Appends list to previous list item, if
               * none exist it wraps the list in a new list item.
               *
               * @param {HTMLNode} ListItem
               * @return {HTMLNode}
               */
              Bullet.prototype.appendToPrevious = function(node) {
                return node.previousSibling
                  ? dom.appendChildNodes(node.previousSibling, [node])
                  : this.wrapList([node], "LI");
              };
              /**
               * @method findList
               *
               * Finds an existing list in list item
               *
               * @param {HTMLNode} ListItem
               * @return {Array[]}
               */
              Bullet.prototype.findList = function(node) {
                return node
                  ? lists.find(node.children, function(child) {
                      return ["OL", "UL"].indexOf(child.nodeName) > -1;
                    })
                  : null;
              };
              /**
               * @method findNextSiblings
               *
               * Finds all list item siblings that follow it
               *
               * @param {HTMLNode} ListItem
               * @return {HTMLNode}
               */
              Bullet.prototype.findNextSiblings = function(node) {
                var siblings = [];
                while (node.nextSibling) {
                  siblings.push(node.nextSibling);
                  node = node.nextSibling;
                }
                return siblings;
              };
              return Bullet;
            })();

            /**
             * @class editing.Typing
             *
             * Typing
             *
             */
            var Typing = /** @class */ (function() {
              function Typing(context) {
                // a Bullet instance to toggle lists off
                this.bullet = new Bullet();
                this.options = context.options;
              }
              /**
               * insert tab
               *
               * @param {WrappedRange} rng
               * @param {Number} tabsize
               */
              Typing.prototype.insertTab = function(rng, tabsize) {
                var tab = dom.createText(
                  new Array(tabsize + 1).join(dom.NBSP_CHAR)
                );
                rng = rng.deleteContents();
                rng.insertNode(tab, true);
                rng = range.create(tab, tabsize);
                rng.select();
              };
              /**
               * insert paragraph
               *
               * @param {jQuery} $editable
               * @param {WrappedRange} rng Can be used in unit tests to "mock" the range
               *
               * blockquoteBreakingLevel
               *   0 - No break, the new paragraph remains inside the quote
               *   1 - Break the first blockquote in the ancestors list
               *   2 - Break all blockquotes, so that the new paragraph is not quoted (this is the default)
               */
              Typing.prototype.insertParagraph = function(editable, rng) {
                rng = rng || range.create(editable);
                // deleteContents on range.
                rng = rng.deleteContents();
                // Wrap range if it needs to be wrapped by paragraph
                rng = rng.wrapBodyInlineWithPara();
                // finding paragraph
                var splitRoot = dom.ancestor(rng.sc, dom.isPara);
                var nextPara;
                // on paragraph: split paragraph
                if (splitRoot) {
                  // if it is an empty line with li
                  if (dom.isEmpty(splitRoot) && dom.isLi(splitRoot)) {
                    // toogle UL/OL and escape
                    this.bullet.toggleList(splitRoot.parentNode.nodeName);
                    return;
                  } else {
                    var blockquote = null;
                    if (this.options.blockquoteBreakingLevel === 1) {
                      blockquote = dom.ancestor(splitRoot, dom.isBlockquote);
                    } else if (this.options.blockquoteBreakingLevel === 2) {
                      blockquote = dom.lastAncestor(
                        splitRoot,
                        dom.isBlockquote
                      );
                    }
                    if (blockquote) {
                      // We're inside a blockquote and options ask us to break it
                      nextPara = $$1(dom.emptyPara)[0];
                      // If the split is right before a <br>, remove it so that there's no "empty line"
                      // after the split in the new blockquote created
                      if (
                        dom.isRightEdgePoint(rng.getStartPoint()) &&
                        dom.isBR(rng.sc.nextSibling)
                      ) {
                        $$1(rng.sc.nextSibling).remove();
                      }
                      var split = dom.splitTree(
                        blockquote,
                        rng.getStartPoint(),
                        { isDiscardEmptySplits: true }
                      );
                      if (split) {
                        split.parentNode.insertBefore(nextPara, split);
                      } else {
                        dom.insertAfter(nextPara, blockquote); // There's no split if we were at the end of the blockquote
                      }
                    } else {
                      nextPara = dom.splitTree(splitRoot, rng.getStartPoint());
                      // not a blockquote, just insert the paragraph
                      var emptyAnchors = dom.listDescendant(
                        splitRoot,
                        dom.isEmptyAnchor
                      );
                      emptyAnchors = emptyAnchors.concat(
                        dom.listDescendant(nextPara, dom.isEmptyAnchor)
                      );
                      $$1.each(emptyAnchors, function(idx, anchor) {
                        dom.remove(anchor);
                      });
                      // replace empty heading, pre or custom-made styleTag with P tag
                      if (
                        (dom.isHeading(nextPara) ||
                          dom.isPre(nextPara) ||
                          dom.isCustomStyleTag(nextPara)) &&
                        dom.isEmpty(nextPara)
                      ) {
                        nextPara = dom.replace(nextPara, "p");
                      }
                    }
                  }
                  // no paragraph: insert empty paragraph
                } else {
                  var next = rng.sc.childNodes[rng.so];
                  nextPara = $$1(dom.emptyPara)[0];
                  if (next) {
                    rng.sc.insertBefore(nextPara, next);
                  } else {
                    rng.sc.appendChild(nextPara);
                  }
                }
                range
                  .create(nextPara, 0)
                  .normalize()
                  .select()
                  .scrollIntoView(editable);
              };
              return Typing;
            })();

            /**
             * @class Create a virtual table to create what actions to do in change.
             * @param {object} startPoint Cell selected to apply change.
             * @param {enum} where  Where change will be applied Row or Col. Use enum: TableResultAction.where
             * @param {enum} action Action to be applied. Use enum: TableResultAction.requestAction
             * @param {object} domTable Dom element of table to make changes.
             */
            var TableResultAction = function(
              startPoint,
              where,
              action,
              domTable
            ) {
              var _startPoint = { colPos: 0, rowPos: 0 };
              var _virtualTable = [];
              var _actionCellList = [];
              /// ///////////////////////////////////////////
              // Private functions
              /// ///////////////////////////////////////////
              /**
               * Set the startPoint of action.
               */
              function setStartPoint() {
                if (
                  !startPoint ||
                  !startPoint.tagName ||
                  (startPoint.tagName.toLowerCase() !== "td" &&
                    startPoint.tagName.toLowerCase() !== "th")
                ) {
                  console.error(
                    "Impossible to identify start Cell point.",
                    startPoint
                  );
                  return;
                }
                _startPoint.colPos = startPoint.cellIndex;
                if (
                  !startPoint.parentElement ||
                  !startPoint.parentElement.tagName ||
                  startPoint.parentElement.tagName.toLowerCase() !== "tr"
                ) {
                  console.error(
                    "Impossible to identify start Row point.",
                    startPoint
                  );
                  return;
                }
                _startPoint.rowPos = startPoint.parentElement.rowIndex;
              }
              /**
               * Define virtual table position info object.
               *
               * @param {int} rowIndex Index position in line of virtual table.
               * @param {int} cellIndex Index position in column of virtual table.
               * @param {object} baseRow Row affected by this position.
               * @param {object} baseCell Cell affected by this position.
               * @param {bool} isSpan Inform if it is an span cell/row.
               */
              function setVirtualTablePosition(
                rowIndex,
                cellIndex,
                baseRow,
                baseCell,
                isRowSpan,
                isColSpan,
                isVirtualCell
              ) {
                var objPosition = {
                  baseRow: baseRow,
                  baseCell: baseCell,
                  isRowSpan: isRowSpan,
                  isColSpan: isColSpan,
                  isVirtual: isVirtualCell
                };
                if (!_virtualTable[rowIndex]) {
                  _virtualTable[rowIndex] = [];
                }
                _virtualTable[rowIndex][cellIndex] = objPosition;
              }
              /**
               * Create action cell object.
               *
               * @param {object} virtualTableCellObj Object of specific position on virtual table.
               * @param {enum} resultAction Action to be applied in that item.
               */
              function getActionCell(
                virtualTableCellObj,
                resultAction,
                virtualRowPosition,
                virtualColPosition
              ) {
                return {
                  baseCell: virtualTableCellObj.baseCell,
                  action: resultAction,
                  virtualTable: {
                    rowIndex: virtualRowPosition,
                    cellIndex: virtualColPosition
                  }
                };
              }
              /**
               * Recover free index of row to append Cell.
               *
               * @param {int} rowIndex Index of row to find free space.
               * @param {int} cellIndex Index of cell to find free space in table.
               */
              function recoverCellIndex(rowIndex, cellIndex) {
                if (!_virtualTable[rowIndex]) {
                  return cellIndex;
                }
                if (!_virtualTable[rowIndex][cellIndex]) {
                  return cellIndex;
                }
                var newCellIndex = cellIndex;
                while (_virtualTable[rowIndex][newCellIndex]) {
                  newCellIndex++;
                  if (!_virtualTable[rowIndex][newCellIndex]) {
                    return newCellIndex;
                  }
                }
              }
              /**
               * Recover info about row and cell and add information to virtual table.
               *
               * @param {object} row Row to recover information.
               * @param {object} cell Cell to recover information.
               */
              function addCellInfoToVirtual(row, cell) {
                var cellIndex = recoverCellIndex(row.rowIndex, cell.cellIndex);
                var cellHasColspan = cell.colSpan > 1;
                var cellHasRowspan = cell.rowSpan > 1;
                var isThisSelectedCell =
                  row.rowIndex === _startPoint.rowPos &&
                  cell.cellIndex === _startPoint.colPos;
                setVirtualTablePosition(
                  row.rowIndex,
                  cellIndex,
                  row,
                  cell,
                  cellHasRowspan,
                  cellHasColspan,
                  false
                );
                // Add span rows to virtual Table.
                var rowspanNumber = cell.attributes.rowSpan
                  ? parseInt(cell.attributes.rowSpan.value, 10)
                  : 0;
                if (rowspanNumber > 1) {
                  for (var rp = 1; rp < rowspanNumber; rp++) {
                    var rowspanIndex = row.rowIndex + rp;
                    adjustStartPoint(
                      rowspanIndex,
                      cellIndex,
                      cell,
                      isThisSelectedCell
                    );
                    setVirtualTablePosition(
                      rowspanIndex,
                      cellIndex,
                      row,
                      cell,
                      true,
                      cellHasColspan,
                      true
                    );
                  }
                }
                // Add span cols to virtual table.
                var colspanNumber = cell.attributes.colSpan
                  ? parseInt(cell.attributes.colSpan.value, 10)
                  : 0;
                if (colspanNumber > 1) {
                  for (var cp = 1; cp < colspanNumber; cp++) {
                    var cellspanIndex = recoverCellIndex(
                      row.rowIndex,
                      cellIndex + cp
                    );
                    adjustStartPoint(
                      row.rowIndex,
                      cellspanIndex,
                      cell,
                      isThisSelectedCell
                    );
                    setVirtualTablePosition(
                      row.rowIndex,
                      cellspanIndex,
                      row,
                      cell,
                      cellHasRowspan,
                      true,
                      true
                    );
                  }
                }
              }
              /**
               * Process validation and adjust of start point if needed
               *
               * @param {int} rowIndex
               * @param {int} cellIndex
               * @param {object} cell
               * @param {bool} isSelectedCell
               */
              function adjustStartPoint(
                rowIndex,
                cellIndex,
                cell,
                isSelectedCell
              ) {
                if (
                  rowIndex === _startPoint.rowPos &&
                  _startPoint.colPos >= cell.cellIndex &&
                  cell.cellIndex <= cellIndex &&
                  !isSelectedCell
                ) {
                  _startPoint.colPos++;
                }
              }
              /**
               * Create virtual table of cells with all cells, including span cells.
               */
              function createVirtualTable() {
                var rows = domTable.rows;
                for (var rowIndex = 0; rowIndex < rows.length; rowIndex++) {
                  var cells = rows[rowIndex].cells;
                  for (
                    var cellIndex = 0;
                    cellIndex < cells.length;
                    cellIndex++
                  ) {
                    addCellInfoToVirtual(rows[rowIndex], cells[cellIndex]);
                  }
                }
              }
              /**
               * Get action to be applied on the cell.
               *
               * @param {object} cell virtual table cell to apply action
               */
              function getDeleteResultActionToCell(cell) {
                switch (where) {
                  case TableResultAction.where.Column:
                    if (cell.isColSpan) {
                      return TableResultAction.resultAction.SubtractSpanCount;
                    }
                    break;
                  case TableResultAction.where.Row:
                    if (!cell.isVirtual && cell.isRowSpan) {
                      return TableResultAction.resultAction.AddCell;
                    } else if (cell.isRowSpan) {
                      return TableResultAction.resultAction.SubtractSpanCount;
                    }
                    break;
                }
                return TableResultAction.resultAction.RemoveCell;
              }
              /**
               * Get action to be applied on the cell.
               *
               * @param {object} cell virtual table cell to apply action
               */
              function getAddResultActionToCell(cell) {
                switch (where) {
                  case TableResultAction.where.Column:
                    if (cell.isColSpan) {
                      return TableResultAction.resultAction.SumSpanCount;
                    } else if (cell.isRowSpan && cell.isVirtual) {
                      return TableResultAction.resultAction.Ignore;
                    }
                    break;
                  case TableResultAction.where.Row:
                    if (cell.isRowSpan) {
                      return TableResultAction.resultAction.SumSpanCount;
                    } else if (cell.isColSpan && cell.isVirtual) {
                      return TableResultAction.resultAction.Ignore;
                    }
                    break;
                }
                return TableResultAction.resultAction.AddCell;
              }
              function init() {
                setStartPoint();
                createVirtualTable();
              }
              /// ///////////////////////////////////////////
              // Public functions
              /// ///////////////////////////////////////////
              /**
               * Recover array os what to do in table.
               */
              this.getActionList = function() {
                var fixedRow =
                  where === TableResultAction.where.Row
                    ? _startPoint.rowPos
                    : -1;
                var fixedCol =
                  where === TableResultAction.where.Column
                    ? _startPoint.colPos
                    : -1;
                var actualPosition = 0;
                var canContinue = true;
                while (canContinue) {
                  var rowPosition = fixedRow >= 0 ? fixedRow : actualPosition;
                  var colPosition = fixedCol >= 0 ? fixedCol : actualPosition;
                  var row = _virtualTable[rowPosition];
                  if (!row) {
                    canContinue = false;
                    return _actionCellList;
                  }
                  var cell = row[colPosition];
                  if (!cell) {
                    canContinue = false;
                    return _actionCellList;
                  }
                  // Define action to be applied in this cell
                  var resultAction = TableResultAction.resultAction.Ignore;
                  switch (action) {
                    case TableResultAction.requestAction.Add:
                      resultAction = getAddResultActionToCell(cell);
                      break;
                    case TableResultAction.requestAction.Delete:
                      resultAction = getDeleteResultActionToCell(cell);
                      break;
                  }
                  _actionCellList.push(
                    getActionCell(cell, resultAction, rowPosition, colPosition)
                  );
                  actualPosition++;
                }
                return _actionCellList;
              };
              init();
            };
            /**
             *
             * Where action occours enum.
             */
            TableResultAction.where = { Row: 0, Column: 1 };
            /**
             *
             * Requested action to apply enum.
             */
            TableResultAction.requestAction = { Add: 0, Delete: 1 };
            /**
             *
             * Result action to be executed enum.
             */
            TableResultAction.resultAction = {
              Ignore: 0,
              SubtractSpanCount: 1,
              RemoveCell: 2,
              AddCell: 3,
              SumSpanCount: 4
            };
            /**
             *
             * @class editing.Table
             *
             * Table
             *
             */
            var Table = /** @class */ (function() {
              function Table() {}
              /**
               * handle tab key
               *
               * @param {WrappedRange} rng
               * @param {Boolean} isShift
               */
              Table.prototype.tab = function(rng, isShift) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                var table = dom.ancestor(cell, dom.isTable);
                var cells = dom.listDescendant(table, dom.isCell);
                var nextCell = lists[isShift ? "prev" : "next"](cells, cell);
                if (nextCell) {
                  range.create(nextCell, 0).select();
                }
              };
              /**
               * Add a new row
               *
               * @param {WrappedRange} rng
               * @param {String} position (top/bottom)
               * @return {Node}
               */
              Table.prototype.addRow = function(rng, position) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                var currentTr = $$1(cell).closest("tr");
                var trAttributes = this.recoverAttributes(currentTr);
                var html = $$1("<tr" + trAttributes + "></tr>");
                var vTable = new TableResultAction(
                  cell,
                  TableResultAction.where.Row,
                  TableResultAction.requestAction.Add,
                  $$1(currentTr).closest("table")[0]
                );
                var actions = vTable.getActionList();
                for (var idCell = 0; idCell < actions.length; idCell++) {
                  var currentCell = actions[idCell];
                  var tdAttributes = this.recoverAttributes(
                    currentCell.baseCell
                  );
                  switch (currentCell.action) {
                    case TableResultAction.resultAction.AddCell:
                      html.append(
                        "<td" + tdAttributes + ">" + dom.blank + "</td>"
                      );
                      break;
                    case TableResultAction.resultAction.SumSpanCount:
                      if (position === "top") {
                        var baseCellTr = currentCell.baseCell.parent;
                        var isTopFromRowSpan =
                          (!baseCellTr
                            ? 0
                            : currentCell.baseCell.closest("tr").rowIndex) <=
                          currentTr[0].rowIndex;
                        if (isTopFromRowSpan) {
                          var newTd = $$1("<div></div>")
                            .append(
                              $$1(
                                "<td" + tdAttributes + ">" + dom.blank + "</td>"
                              ).removeAttr("rowspan")
                            )
                            .html();
                          html.append(newTd);
                          break;
                        }
                      }
                      var rowspanNumber = parseInt(
                        currentCell.baseCell.rowSpan,
                        10
                      );
                      rowspanNumber++;
                      currentCell.baseCell.setAttribute(
                        "rowSpan",
                        rowspanNumber
                      );
                      break;
                  }
                }
                if (position === "top") {
                  currentTr.before(html);
                } else {
                  var cellHasRowspan = cell.rowSpan > 1;
                  if (cellHasRowspan) {
                    var lastTrIndex =
                      currentTr[0].rowIndex + (cell.rowSpan - 2);
                    $$1(
                      $$1(currentTr)
                        .parent()
                        .find("tr")[lastTrIndex]
                    ).after($$1(html));
                    return;
                  }
                  currentTr.after(html);
                }
              };
              /**
               * Add a new col
               *
               * @param {WrappedRange} rng
               * @param {String} position (left/right)
               * @return {Node}
               */
              Table.prototype.addCol = function(rng, position) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                var row = $$1(cell).closest("tr");
                var rowsGroup = $$1(row).siblings();
                rowsGroup.push(row);
                var vTable = new TableResultAction(
                  cell,
                  TableResultAction.where.Column,
                  TableResultAction.requestAction.Add,
                  $$1(row).closest("table")[0]
                );
                var actions = vTable.getActionList();
                for (
                  var actionIndex = 0;
                  actionIndex < actions.length;
                  actionIndex++
                ) {
                  var currentCell = actions[actionIndex];
                  var tdAttributes = this.recoverAttributes(
                    currentCell.baseCell
                  );
                  switch (currentCell.action) {
                    case TableResultAction.resultAction.AddCell:
                      if (position === "right") {
                        $$1(currentCell.baseCell).after(
                          "<td" + tdAttributes + ">" + dom.blank + "</td>"
                        );
                      } else {
                        $$1(currentCell.baseCell).before(
                          "<td" + tdAttributes + ">" + dom.blank + "</td>"
                        );
                      }
                      break;
                    case TableResultAction.resultAction.SumSpanCount:
                      if (position === "right") {
                        var colspanNumber = parseInt(
                          currentCell.baseCell.colSpan,
                          10
                        );
                        colspanNumber++;
                        currentCell.baseCell.setAttribute(
                          "colSpan",
                          colspanNumber
                        );
                      } else {
                        $$1(currentCell.baseCell).before(
                          "<td" + tdAttributes + ">" + dom.blank + "</td>"
                        );
                      }
                      break;
                  }
                }
              };
              /*
               * Copy attributes from element.
               *
               * @param {object} Element to recover attributes.
               * @return {string} Copied string elements.
               */
              Table.prototype.recoverAttributes = function(el) {
                var resultStr = "";
                if (!el) {
                  return resultStr;
                }
                var attrList = el.attributes || [];
                for (var i = 0; i < attrList.length; i++) {
                  if (attrList[i].name.toLowerCase() === "id") {
                    continue;
                  }
                  if (attrList[i].specified) {
                    resultStr +=
                      " " + attrList[i].name + "='" + attrList[i].value + "'";
                  }
                }
                return resultStr;
              };
              /**
               * Delete current row
               *
               * @param {WrappedRange} rng
               * @return {Node}
               */
              Table.prototype.deleteRow = function(rng) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                var row = $$1(cell).closest("tr");
                var cellPos = row.children("td, th").index($$1(cell));
                var rowPos = row[0].rowIndex;
                var vTable = new TableResultAction(
                  cell,
                  TableResultAction.where.Row,
                  TableResultAction.requestAction.Delete,
                  $$1(row).closest("table")[0]
                );
                var actions = vTable.getActionList();
                for (
                  var actionIndex = 0;
                  actionIndex < actions.length;
                  actionIndex++
                ) {
                  if (!actions[actionIndex]) {
                    continue;
                  }
                  var baseCell = actions[actionIndex].baseCell;
                  var virtualPosition = actions[actionIndex].virtualTable;
                  var hasRowspan = baseCell.rowSpan && baseCell.rowSpan > 1;
                  var rowspanNumber = hasRowspan
                    ? parseInt(baseCell.rowSpan, 10)
                    : 0;
                  switch (actions[actionIndex].action) {
                    case TableResultAction.resultAction.Ignore:
                      continue;
                    case TableResultAction.resultAction.AddCell:
                      var nextRow = row.next("tr")[0];
                      if (!nextRow) {
                        continue;
                      }
                      var cloneRow = row[0].cells[cellPos];
                      if (hasRowspan) {
                        if (rowspanNumber > 2) {
                          rowspanNumber--;
                          nextRow.insertBefore(
                            cloneRow,
                            nextRow.cells[cellPos]
                          );
                          nextRow.cells[cellPos].setAttribute(
                            "rowSpan",
                            rowspanNumber
                          );
                          nextRow.cells[cellPos].innerHTML = "";
                        } else if (rowspanNumber === 2) {
                          nextRow.insertBefore(
                            cloneRow,
                            nextRow.cells[cellPos]
                          );
                          nextRow.cells[cellPos].removeAttribute("rowSpan");
                          nextRow.cells[cellPos].innerHTML = "";
                        }
                      }
                      continue;
                    case TableResultAction.resultAction.SubtractSpanCount:
                      if (hasRowspan) {
                        if (rowspanNumber > 2) {
                          rowspanNumber--;
                          baseCell.setAttribute("rowSpan", rowspanNumber);
                          if (
                            virtualPosition.rowIndex !== rowPos &&
                            baseCell.cellIndex === cellPos
                          ) {
                            baseCell.innerHTML = "";
                          }
                        } else if (rowspanNumber === 2) {
                          baseCell.removeAttribute("rowSpan");
                          if (
                            virtualPosition.rowIndex !== rowPos &&
                            baseCell.cellIndex === cellPos
                          ) {
                            baseCell.innerHTML = "";
                          }
                        }
                      }
                      continue;
                    case TableResultAction.resultAction.RemoveCell:
                      // Do not need remove cell because row will be deleted.
                      continue;
                  }
                }
                row.remove();
              };
              /**
               * Delete current col
               *
               * @param {WrappedRange} rng
               * @return {Node}
               */
              Table.prototype.deleteCol = function(rng) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                var row = $$1(cell).closest("tr");
                var cellPos = row.children("td, th").index($$1(cell));
                var vTable = new TableResultAction(
                  cell,
                  TableResultAction.where.Column,
                  TableResultAction.requestAction.Delete,
                  $$1(row).closest("table")[0]
                );
                var actions = vTable.getActionList();
                for (
                  var actionIndex = 0;
                  actionIndex < actions.length;
                  actionIndex++
                ) {
                  if (!actions[actionIndex]) {
                    continue;
                  }
                  switch (actions[actionIndex].action) {
                    case TableResultAction.resultAction.Ignore:
                      continue;
                    case TableResultAction.resultAction.SubtractSpanCount:
                      var baseCell = actions[actionIndex].baseCell;
                      var hasColspan = baseCell.colSpan && baseCell.colSpan > 1;
                      if (hasColspan) {
                        var colspanNumber = baseCell.colSpan
                          ? parseInt(baseCell.colSpan, 10)
                          : 0;
                        if (colspanNumber > 2) {
                          colspanNumber--;
                          baseCell.setAttribute("colSpan", colspanNumber);
                          if (baseCell.cellIndex === cellPos) {
                            baseCell.innerHTML = "";
                          }
                        } else if (colspanNumber === 2) {
                          baseCell.removeAttribute("colSpan");
                          if (baseCell.cellIndex === cellPos) {
                            baseCell.innerHTML = "";
                          }
                        }
                      }
                      continue;
                    case TableResultAction.resultAction.RemoveCell:
                      dom.remove(actions[actionIndex].baseCell, true);
                      continue;
                  }
                }
              };
              /**
               * create empty table element
               *
               * @param {Number} rowCount
               * @param {Number} colCount
               * @return {Node}
               */
              Table.prototype.createTable = function(
                colCount,
                rowCount,
                options
              ) {
                var tds = [];
                var tdHTML;
                for (var idxCol = 0; idxCol < colCount; idxCol++) {
                  tds.push("<td>" + dom.blank + "</td>");
                }
                tdHTML = tds.join("");
                var trs = [];
                var trHTML;
                for (var idxRow = 0; idxRow < rowCount; idxRow++) {
                  trs.push("<tr>" + tdHTML + "</tr>");
                }
                trHTML = trs.join("");
                var $table = $$1("<table>" + trHTML + "</table>");
                if (options && options.tableClassName) {
                  $table.addClass(options.tableClassName);
                }
                return $table[0];
              };
              /**
               * Delete current table
               *
               * @param {WrappedRange} rng
               * @return {Node}
               */
              Table.prototype.deleteTable = function(rng) {
                var cell = dom.ancestor(rng.commonAncestor(), dom.isCell);
                $$1(cell)
                  .closest("table")
                  .remove();
              };
              return Table;
            })();

            var KEY_BOGUS = "bogus";
            /**
             * @class Editor
             */
            var Editor = /** @class */ (function() {
              function Editor(context) {
                var _this = this;
                this.context = context;
                this.$note = context.layoutInfo.note;
                this.$editor = context.layoutInfo.editor;
                this.$editable = context.layoutInfo.editable;
                this.options = context.options;
                this.lang = this.options.langInfo;
                this.editable = this.$editable[0];
                this.lastRange = null;
                this.style = new Style();
                this.table = new Table();
                this.typing = new Typing(context);
                this.bullet = new Bullet();
                this.history = new History(this.$editable);
                this.context.memo("help.undo", this.lang.help.undo);
                this.context.memo("help.redo", this.lang.help.redo);
                this.context.memo("help.tab", this.lang.help.tab);
                this.context.memo("help.untab", this.lang.help.untab);
                this.context.memo(
                  "help.insertParagraph",
                  this.lang.help.insertParagraph
                );
                this.context.memo(
                  "help.insertOrderedList",
                  this.lang.help.insertOrderedList
                );
                this.context.memo(
                  "help.insertUnorderedList",
                  this.lang.help.insertUnorderedList
                );
                this.context.memo("help.indent", this.lang.help.indent);
                this.context.memo("help.outdent", this.lang.help.outdent);
                this.context.memo("help.formatPara", this.lang.help.formatPara);
                this.context.memo(
                  "help.insertHorizontalRule",
                  this.lang.help.insertHorizontalRule
                );
                this.context.memo("help.fontName", this.lang.help.fontName);
                // native commands(with execCommand), generate function for execCommand
                var commands = [
                  "bold",
                  "italic",
                  "underline",
                  "strikethrough",
                  "superscript",
                  "subscript",
                  "justifyLeft",
                  "justifyCenter",
                  "justifyRight",
                  "justifyFull",
                  "formatBlock",
                  "removeFormat",
                  "backColor"
                ];
                for (var idx = 0, len = commands.length; idx < len; idx++) {
                  this[commands[idx]] = (function(sCmd) {
                    return function(value) {
                      _this.beforeCommand();
                      document.execCommand(sCmd, false, value);
                      _this.afterCommand(true);
                    };
                  })(commands[idx]);
                  this.context.memo(
                    "help." + commands[idx],
                    this.lang.help[commands[idx]]
                  );
                }
                this.fontName = this.wrapCommand(function(value) {
                  return _this.fontStyling("font-family", "'" + value + "'");
                });
                this.fontSize = this.wrapCommand(function(value) {
                  return _this.fontStyling("font-size", value + "px");
                });
                for (var idx = 1; idx <= 6; idx++) {
                  this["formatH" + idx] = (function(idx) {
                    return function() {
                      _this.formatBlock("H" + idx);
                    };
                  })(idx);
                  this.context.memo(
                    "help.formatH" + idx,
                    this.lang.help["formatH" + idx]
                  );
                }
                this.insertParagraph = this.wrapCommand(function() {
                  _this.typing.insertParagraph(_this.editable);
                });
                this.insertOrderedList = this.wrapCommand(function() {
                  _this.bullet.insertOrderedList(_this.editable);
                });
                this.insertUnorderedList = this.wrapCommand(function() {
                  _this.bullet.insertUnorderedList(_this.editable);
                });
                this.indent = this.wrapCommand(function() {
                  _this.bullet.indent(_this.editable);
                });
                this.outdent = this.wrapCommand(function() {
                  _this.bullet.outdent(_this.editable);
                });
                /**
                 * insertNode
                 * insert node
                 * @param {Node} node
                 */
                this.insertNode = this.wrapCommand(function(node) {
                  if (_this.isLimited($$1(node).text().length)) {
                    return;
                  }
                  var rng = _this.getLastRange();
                  rng.insertNode(node);
                  range.createFromNodeAfter(node).select();
                  _this.setLastRange();
                });
                /**
                 * insert text
                 * @param {String} text
                 */
                this.insertText = this.wrapCommand(function(text) {
                  if (_this.isLimited(text.length)) {
                    return;
                  }
                  var rng = _this.getLastRange();
                  var textNode = rng.insertNode(dom.createText(text));
                  range.create(textNode, dom.nodeLength(textNode)).select();
                  _this.setLastRange();
                });
                /**
                 * paste HTML
                 * @param {String} markup
                 */
                this.pasteHTML = this.wrapCommand(function(markup) {
                  if (_this.isLimited(markup.length)) {
                    return;
                  }
                  markup = _this.context.invoke("codeview.purify", markup);
                  var contents = _this.getLastRange().pasteHTML(markup);
                  range.createFromNodeAfter(lists.last(contents)).select();
                  _this.setLastRange();
                });
                /**
                 * formatBlock
                 *
                 * @param {String} tagName
                 */
                this.formatBlock = this.wrapCommand(function(tagName, $target) {
                  var onApplyCustomStyle =
                    _this.options.callbacks.onApplyCustomStyle;
                  if (onApplyCustomStyle) {
                    onApplyCustomStyle.call(
                      _this,
                      $target,
                      _this.context,
                      _this.onFormatBlock
                    );
                  } else {
                    _this.onFormatBlock(tagName, $target);
                  }
                });
                /**
                 * insert horizontal rule
                 */
                this.insertHorizontalRule = this.wrapCommand(function() {
                  var hrNode = _this
                    .getLastRange()
                    .insertNode(dom.create("HR"));
                  if (hrNode.nextSibling) {
                    range
                      .create(hrNode.nextSibling, 0)
                      .normalize()
                      .select();
                    _this.setLastRange();
                  }
                });
                /**
                 * lineHeight
                 * @param {String} value
                 */
                this.lineHeight = this.wrapCommand(function(value) {
                  _this.style.stylePara(_this.getLastRange(), {
                    lineHeight: value
                  });
                });
                /**
                 * create link (command)
                 *
                 * @param {Object} linkInfo
                 */
                this.createLink = this.wrapCommand(function(linkInfo) {
                  var linkUrl = linkInfo.url;
                  var linkText = linkInfo.text;
                  var isNewWindow = linkInfo.isNewWindow;
                  var rng = linkInfo.range || _this.getLastRange();
                  var additionalTextLength =
                    linkText.length - rng.toString().length;
                  if (
                    additionalTextLength > 0 &&
                    _this.isLimited(additionalTextLength)
                  ) {
                    return;
                  }
                  var isTextChanged = rng.toString() !== linkText;
                  // handle spaced urls from input
                  if (typeof linkUrl === "string") {
                    linkUrl = linkUrl.trim();
                  }
                  if (_this.options.onCreateLink) {
                    linkUrl = _this.options.onCreateLink(linkUrl);
                  } else {
                    // if url doesn't have any protocol and not even a relative or a label, use http:// as default
                    linkUrl = /^([A-Za-z][A-Za-z0-9+-.]*\:|#|\/)/.test(linkUrl)
                      ? linkUrl
                      : "http://" + linkUrl;
                  }
                  var anchors = [];
                  if (isTextChanged) {
                    rng = rng.deleteContents();
                    var anchor = rng.insertNode(
                      $$1("<A>" + linkText + "</A>")[0]
                    );
                    anchors.push(anchor);
                  } else {
                    anchors = _this.style.styleNodes(rng, {
                      nodeName: "A",
                      expandClosestSibling: true,
                      onlyPartialContains: true
                    });
                  }
                  $$1.each(anchors, function(idx, anchor) {
                    $$1(anchor).attr("href", linkUrl);
                    if (isNewWindow) {
                      $$1(anchor).attr("target", "_blank");
                    } else {
                      $$1(anchor).removeAttr("target");
                    }
                  });
                  var startRange = range.createFromNodeBefore(
                    lists.head(anchors)
                  );
                  var startPoint = startRange.getStartPoint();
                  var endRange = range.createFromNodeAfter(lists.last(anchors));
                  var endPoint = endRange.getEndPoint();
                  range
                    .create(
                      startPoint.node,
                      startPoint.offset,
                      endPoint.node,
                      endPoint.offset
                    )
                    .select();
                  _this.setLastRange();
                });
                /**
                 * setting color
                 *
                 * @param {Object} sObjColor  color code
                 * @param {String} sObjColor.foreColor foreground color
                 * @param {String} sObjColor.backColor background color
                 */
                this.color = this.wrapCommand(function(colorInfo) {
                  var foreColor = colorInfo.foreColor;
                  var backColor = colorInfo.backColor;
                  if (foreColor) {
                    document.execCommand("foreColor", false, foreColor);
                  }
                  if (backColor) {
                    document.execCommand("backColor", false, backColor);
                  }
                });
                /**
                 * Set foreground color
                 *
                 * @param {String} colorCode foreground color code
                 */
                this.foreColor = this.wrapCommand(function(colorInfo) {
                  document.execCommand("styleWithCSS", false, true);
                  document.execCommand("foreColor", false, colorInfo);
                });
                /**
                 * insert Table
                 *
                 * @param {String} dimension of table (ex : "5x5")
                 */
                this.insertTable = this.wrapCommand(function(dim) {
                  var dimension = dim.split("x");
                  var rng = _this.getLastRange().deleteContents();
                  rng.insertNode(
                    _this.table.createTable(
                      dimension[0],
                      dimension[1],
                      _this.options
                    )
                  );
                });
                /**
                 * remove media object and Figure Elements if media object is img with Figure.
                 */
                this.removeMedia = this.wrapCommand(function() {
                  var $target = $$1(_this.restoreTarget()).parent();
                  if ($target.parent("figure").length) {
                    $target.parent("figure").remove();
                  } else {
                    $target = $$1(_this.restoreTarget()).detach();
                  }
                  _this.context.triggerEvent(
                    "media.delete",
                    $target,
                    _this.$editable
                  );
                });
                /**
                 * float me
                 *
                 * @param {String} value
                 */
                this.floatMe = this.wrapCommand(function(value) {
                  var $target = $$1(_this.restoreTarget());
                  $target.toggleClass("note-float-left", value === "left");
                  $target.toggleClass("note-float-right", value === "right");
                  $target.css("float", value === "none" ? "" : value);
                });
                /**
                 * resize overlay element
                 * @param {String} value
                 */
                this.resize = this.wrapCommand(function(value) {
                  var $target = $$1(_this.restoreTarget());
                  value = parseFloat(value);
                  if (value === 0) {
                    $target.css("width", "");
                  } else {
                    $target.css({
                      width: value * 100 + "%",
                      height: ""
                    });
                  }
                });
              }
              Editor.prototype.initialize = function() {
                var _this = this;
                // bind custom events
                this.$editable
                  .on("keydown", function(event) {
                    if (event.keyCode === key.code.ENTER) {
                      _this.context.triggerEvent("enter", event);
                    }
                    _this.context.triggerEvent("keydown", event);
                    if (!event.isDefaultPrevented()) {
                      if (_this.options.shortcuts) {
                        _this.handleKeyMap(event);
                      } else {
                        _this.preventDefaultEditableShortCuts(event);
                      }
                    }
                    if (_this.isLimited(1, event)) {
                      return false;
                    }
                  })
                  .on("keyup", function(event) {
                    _this.setLastRange();
                    _this.context.triggerEvent("keyup", event);
                  })
                  .on("focus", function(event) {
                    _this.setLastRange();
                    _this.context.triggerEvent("focus", event);
                  })
                  .on("blur", function(event) {
                    _this.context.triggerEvent("blur", event);
                  })
                  .on("mousedown", function(event) {
                    _this.context.triggerEvent("mousedown", event);
                  })
                  .on("mouseup", function(event) {
                    _this.setLastRange();
                    _this.context.triggerEvent("mouseup", event);
                  })
                  .on("scroll", function(event) {
                    _this.context.triggerEvent("scroll", event);
                  })
                  .on("paste", function(event) {
                    _this.setLastRange();
                    _this.context.triggerEvent("paste", event);
                  });
                this.$editable.attr("spellcheck", this.options.spellCheck);
                // init content before set event
                this.$editable.html(dom.html(this.$note) || dom.emptyPara);
                this.$editable.on(
                  env.inputEventName,
                  func.debounce(function() {
                    _this.context.triggerEvent(
                      "change",
                      _this.$editable.html(),
                      _this.$editable
                    );
                  }, 10)
                );
                this.$editor
                  .on("focusin", function(event) {
                    _this.context.triggerEvent("focusin", event);
                  })
                  .on("focusout", function(event) {
                    _this.context.triggerEvent("focusout", event);
                  });
                if (!this.options.airMode) {
                  if (this.options.width) {
                    this.$editor.outerWidth(this.options.width);
                  }
                  if (this.options.height) {
                    this.$editable.outerHeight(this.options.height);
                  }
                  if (this.options.maxHeight) {
                    this.$editable.css("max-height", this.options.maxHeight);
                  }
                  if (this.options.minHeight) {
                    this.$editable.css("min-height", this.options.minHeight);
                  }
                }
                this.history.recordUndo();
                this.setLastRange();
              };
              Editor.prototype.destroy = function() {
                this.$editable.off();
              };
              Editor.prototype.handleKeyMap = function(event) {
                var keyMap = this.options.keyMap[env.isMac ? "mac" : "pc"];
                var keys = [];
                if (event.metaKey) {
                  keys.push("CMD");
                }
                if (event.ctrlKey && !event.altKey) {
                  keys.push("CTRL");
                }
                if (event.shiftKey) {
                  keys.push("SHIFT");
                }
                var keyName = key.nameFromCode[event.keyCode];
                if (keyName) {
                  keys.push(keyName);
                }
                var eventName = keyMap[keys.join("+")];
                if (eventName) {
                  if (this.context.invoke(eventName) !== false) {
                    event.preventDefault();
                  }
                } else if (key.isEdit(event.keyCode)) {
                  this.afterCommand();
                }
              };
              Editor.prototype.preventDefaultEditableShortCuts = function(
                event
              ) {
                // B(Bold, 66) / I(Italic, 73) / U(Underline, 85)
                if (
                  (event.ctrlKey || event.metaKey) &&
                  lists.contains([66, 73, 85], event.keyCode)
                ) {
                  event.preventDefault();
                }
              };
              Editor.prototype.isLimited = function(pad, event) {
                pad = pad || 0;
                if (typeof event !== "undefined") {
                  if (
                    key.isMove(event.keyCode) ||
                    event.ctrlKey || event.metaKey ||
                    lists.contains(
                      [key.code.BACKSPACE, key.code.DELETE],
                      event.keyCode
                    )
                  ) {
                    return false;
                  }
                }
                if (this.options.maxTextLength > 0) {
                  if (
                    this.$editable.text().length + pad >=
                    this.options.maxTextLength
                  ) {
                    return true;
                  }
                }
                return false;
              };
              /**
               * create range
               * @return {WrappedRange}
               */
              Editor.prototype.createRange = function() {
                this.focus();
                this.setLastRange();
                return this.getLastRange();
              };
              Editor.prototype.setLastRange = function() {
                this.lastRange = range.create(this.editable);
              };
              Editor.prototype.getLastRange = function() {
                if (!this.lastRange) {
                  this.setLastRange();
                }
                return this.lastRange;
              };
              /**
               * saveRange
               *
               * save current range
               *
               * @param {Boolean} [thenCollapse=false]
               */
              Editor.prototype.saveRange = function(thenCollapse) {
                if (thenCollapse) {
                  this.getLastRange()
                    .collapse()
                    .select();
                }
              };
              /**
               * restoreRange
               *
               * restore lately range
               */
              Editor.prototype.restoreRange = function() {
                if (this.lastRange) {
                  this.lastRange.select();
                  this.focus();
                }
              };
              Editor.prototype.saveTarget = function(node) {
                this.$editable.data("target", node);
              };
              Editor.prototype.clearTarget = function() {
                this.$editable.removeData("target");
              };
              Editor.prototype.restoreTarget = function() {
                return this.$editable.data("target");
              };
              /**
               * currentStyle
               *
               * current style
               * @return {Object|Boolean} unfocus
               */
              Editor.prototype.currentStyle = function() {
                var rng = range.create();
                if (rng) {
                  rng = rng.normalize();
                }
                return rng
                  ? this.style.current(rng)
                  : this.style.fromNode(this.$editable);
              };
              /**
               * style from node
               *
               * @param {jQuery} $node
               * @return {Object}
               */
              Editor.prototype.styleFromNode = function($node) {
                return this.style.fromNode($node);
              };
              /**
               * undo
               */
              Editor.prototype.undo = function() {
                this.context.triggerEvent(
                  "before.command",
                  this.$editable.html()
                );
                this.history.undo();
                this.context.triggerEvent(
                  "change",
                  this.$editable.html(),
                  this.$editable
                );
              };
              /*
               * commit
               */
              Editor.prototype.commit = function() {
                this.context.triggerEvent(
                  "before.command",
                  this.$editable.html()
                );
                this.history.commit();
                this.context.triggerEvent(
                  "change",
                  this.$editable.html(),
                  this.$editable
                );
              };
              /**
               * redo
               */
              Editor.prototype.redo = function() {
                this.context.triggerEvent(
                  "before.command",
                  this.$editable.html()
                );
                this.history.redo();
                this.context.triggerEvent(
                  "change",
                  this.$editable.html(),
                  this.$editable
                );
              };
              /**
               * before command
               */
              Editor.prototype.beforeCommand = function() {
                this.context.triggerEvent(
                  "before.command",
                  this.$editable.html()
                );
                // keep focus on editable before command execution
                this.focus();
              };
              /**
               * after command
               * @param {Boolean} isPreventTrigger
               */
              Editor.prototype.afterCommand = function(isPreventTrigger) {
                this.normalizeContent();
                this.history.recordUndo();
                if (!isPreventTrigger) {
                  this.context.triggerEvent(
                    "change",
                    this.$editable.html(),
                    this.$editable
                  );
                }
              };
              /**
               * handle tab key
               */
              Editor.prototype.tab = function() {
                var rng = this.getLastRange();
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.table.tab(rng);
                } else {
                  if (this.options.tabSize === 0) {
                    return false;
                  }
                  if (!this.isLimited(this.options.tabSize)) {
                    this.beforeCommand();
                    this.typing.insertTab(rng, this.options.tabSize);
                    this.afterCommand();
                  }
                }
              };
              /**
               * handle shift+tab key
               */
              Editor.prototype.untab = function() {
                var rng = this.getLastRange();
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.table.tab(rng, true);
                } else {
                  if (this.options.tabSize === 0) {
                    return false;
                  }
                }
              };
              /**
               * run given function between beforeCommand and afterCommand
               */
              Editor.prototype.wrapCommand = function(fn) {
                return function() {
                  this.beforeCommand();
                  fn.apply(this, arguments);
                  this.afterCommand();
                };
              };
              /**
               * insert image
               *
               * @param {String} src
               * @param {String|Function} param
               * @return {Promise}
               */
              Editor.prototype.insertImage = function(src, param) {
                var _this = this;
                return createImage(src, param)
                  .then(function($image) {
                    _this.beforeCommand();
                    if (typeof param === "function") {
                      param($image);
                    } else {
                      if (typeof param === "string") {
                        $image.attr("data-filename", param);
                      }
                      $image.css(
                        "width",
                        Math.min(_this.$editable.width(), $image.width())
                      );
                    }
                    $image.show();
                    range.create(_this.editable).insertNode($image[0]);
                    range.createFromNodeAfter($image[0]).select();
                    _this.setLastRange();
                    _this.afterCommand();
                  })
                  .fail(function(e) {
                    _this.context.triggerEvent("image.upload.error", e);
                  });
              };
              /**
               * insertImages
               * @param {File[]} files
               */
              Editor.prototype.insertImagesAsDataURL = function(files) {
                var _this = this;
                $$1.each(files, function(idx, file) {
                  var filename = file.name;
                  if (
                    _this.options.maximumImageFileSize &&
                    _this.options.maximumImageFileSize < file.size
                  ) {
                    _this.context.triggerEvent(
                      "image.upload.error",
                      _this.lang.image.maximumFileSizeError
                    );
                  } else {
                    readFileAsDataURL(file)
                      .then(function(dataURL) {
                        return _this.insertImage(dataURL, filename);
                      })
                      .fail(function() {
                        _this.context.triggerEvent("image.upload.error");
                      });
                  }
                });
              };
              /**
               * insertImagesOrCallback
               * @param {File[]} files
               */
              Editor.prototype.insertImagesOrCallback = function(files) {
                var callbacks = this.options.callbacks;
                // If onImageUpload set,
                if (callbacks.onImageUpload) {
                  this.context.triggerEvent("image.upload", files);
                  // else insert Image as dataURL
                } else {
                  this.insertImagesAsDataURL(files);
                }
              };
              /**
               * return selected plain text
               * @return {String} text
               */
              Editor.prototype.getSelectedText = function() {
                var rng = this.getLastRange();
                // if range on anchor, expand range with anchor
                if (rng.isOnAnchor()) {
                  rng = range.createFromNode(
                    dom.ancestor(rng.sc, dom.isAnchor)
                  );
                }
                return rng.toString();
              };
              Editor.prototype.onFormatBlock = function(tagName, $target) {
                // [workaround] for MSIE, IE need `<`
                document.execCommand(
                  "FormatBlock",
                  false,
                  env.isMSIE ? "<" + tagName + ">" : tagName
                );
                // support custom class
                if ($target && $target.length) {
                  // find the exact element has given tagName
                  if (
                    $target[0].tagName.toUpperCase() !== tagName.toUpperCase()
                  ) {
                    $target = $target.find(tagName);
                  }
                  if ($target && $target.length) {
                    var className = $target[0].className || "";
                    if (className) {
                      var currentRange = this.createRange();
                      var $parent = $$1([
                        currentRange.sc,
                        currentRange.ec
                      ]).closest(tagName);
                      $parent.addClass(className);
                    }
                  }
                }
              };
              Editor.prototype.formatPara = function() {
                this.formatBlock("P");
              };
              Editor.prototype.fontStyling = function(target, value) {
                var rng = this.getLastRange();
                if (rng) {
                  var spans = this.style.styleNodes(rng);
                  $$1(spans).css(target, value);
                  // [workaround] added styled bogus span for style
                  //  - also bogus character needed for cursor position
                  if (rng.isCollapsed()) {
                    var firstSpan = lists.head(spans);
                    if (firstSpan && !dom.nodeLength(firstSpan)) {
                      firstSpan.innerHTML = dom.ZERO_WIDTH_NBSP_CHAR;
                      range.createFromNodeAfter(firstSpan.firstChild).select();
                      this.setLastRange();
                      this.$editable.data(KEY_BOGUS, firstSpan);
                    }
                  }
                }
              };
              /**
               * unlink
               *
               * @type command
               */
              Editor.prototype.unlink = function() {
                var rng = this.getLastRange();
                if (rng.isOnAnchor()) {
                  var anchor = dom.ancestor(rng.sc, dom.isAnchor);
                  rng = range.createFromNode(anchor);
                  rng.select();
                  this.setLastRange();
                  this.beforeCommand();
                  document.execCommand("unlink");
                  this.afterCommand();
                }
              };
              /**
               * returns link info
               *
               * @return {Object}
               * @return {WrappedRange} return.range
               * @return {String} return.text
               * @return {Boolean} [return.isNewWindow=true]
               * @return {String} [return.url=""]
               */
              Editor.prototype.getLinkInfo = function() {
                var rng = this.getLastRange().expand(dom.isAnchor);
                // Get the first anchor on range(for edit).
                var $anchor = $$1(lists.head(rng.nodes(dom.isAnchor)));
                var linkInfo = {
                  range: rng,
                  text: rng.toString(),
                  url: $anchor.length ? $anchor.attr("href") : ""
                };
                // When anchor exists,
                if ($anchor.length) {
                  // Set isNewWindow by checking its target.
                  linkInfo.isNewWindow = $anchor.attr("target") === "_blank";
                }
                return linkInfo;
              };
              Editor.prototype.addRow = function(position) {
                var rng = this.getLastRange(this.$editable);
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.beforeCommand();
                  this.table.addRow(rng, position);
                  this.afterCommand();
                }
              };
              Editor.prototype.addCol = function(position) {
                var rng = this.getLastRange(this.$editable);
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.beforeCommand();
                  this.table.addCol(rng, position);
                  this.afterCommand();
                }
              };
              Editor.prototype.deleteRow = function() {
                var rng = this.getLastRange(this.$editable);
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.beforeCommand();
                  this.table.deleteRow(rng);
                  this.afterCommand();
                }
              };
              Editor.prototype.deleteCol = function() {
                var rng = this.getLastRange(this.$editable);
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.beforeCommand();
                  this.table.deleteCol(rng);
                  this.afterCommand();
                }
              };
              Editor.prototype.deleteTable = function() {
                var rng = this.getLastRange(this.$editable);
                if (rng.isCollapsed() && rng.isOnCell()) {
                  this.beforeCommand();
                  this.table.deleteTable(rng);
                  this.afterCommand();
                }
              };
              /**
               * @param {Position} pos
               * @param {jQuery} $target - target element
               * @param {Boolean} [bKeepRatio] - keep ratio
               */
              Editor.prototype.resizeTo = function(pos, $target, bKeepRatio) {
                var imageSize;
                if (bKeepRatio) {
                  var newRatio = pos.y / pos.x;
                  var ratio = $target.data("ratio");
                  imageSize = {
                    width: ratio > newRatio ? pos.x : pos.y / ratio,
                    height: ratio > newRatio ? pos.x * ratio : pos.y
                  };
                } else {
                  imageSize = {
                    width: pos.x,
                    height: pos.y
                  };
                }
                $target.css(imageSize);
              };
              /**
               * returns whether editable area has focus or not.
               */
              Editor.prototype.hasFocus = function() {
                return this.$editable.is(":focus");
              };
              /**
               * set focus
               */
              Editor.prototype.focus = function() {
                // [workaround] Screen will move when page is scolled in IE.
                //  - do focus when not focused
                if (!this.hasFocus()) {
                  this.$editable.focus();
                }
              };
              /**
               * returns whether contents is empty or not.
               * @return {Boolean}
               */
              Editor.prototype.isEmpty = function() {
                return (
                  dom.isEmpty(this.$editable[0]) ||
                  dom.emptyPara === this.$editable.html()
                );
              };
              /**
               * Removes all contents and restores the editable instance to an _emptyPara_.
               */
              Editor.prototype.empty = function() {
                this.context.invoke("code", dom.emptyPara);
              };
              /**
               * normalize content
               */
              Editor.prototype.normalizeContent = function() {
                this.$editable[0].normalize();
              };
              return Editor;
            })();

            var Clipboard = /** @class */ (function() {
              function Clipboard(context) {
                this.context = context;
                this.$editable = context.layoutInfo.editable;
              }
              Clipboard.prototype.initialize = function() {
                this.$editable.on("paste", this.pasteByEvent.bind(this));
              };
              /**
               * paste by clipboard event
               *
               * @param {Event} event
               */
              Clipboard.prototype.pasteByEvent = function(event) {
                var clipboardData = event.originalEvent.clipboardData;
                if (
                  clipboardData &&
                  clipboardData.items &&
                  clipboardData.items.length
                ) {
                  // paste img file
                  var item =
                    clipboardData.items.length > 1
                      ? clipboardData.items[1]
                      : lists.head(clipboardData.items);
                  if (
                    item.kind === "file" &&
                    item.type.indexOf("image/") !== -1
                  ) {
                    this.context.invoke("editor.insertImagesOrCallback", [
                      item.getAsFile()
                    ]);
                  }
                  this.context.invoke("editor.afterCommand");
                }
              };
              return Clipboard;
            })();

            var Dropzone = /** @class */ (function() {
              function Dropzone(context) {
                this.context = context;
                this.$eventListener = $$1(document);
                this.$editor = context.layoutInfo.editor;
                this.$editable = context.layoutInfo.editable;
                this.options = context.options;
                this.lang = this.options.langInfo;
                this.documentEventHandlers = {};
                this.$dropzone = $$1(
                  [
                    '<div class="note-dropzone">',
                    '  <div class="note-dropzone-message"/>',
                    "</div>"
                  ].join("")
                ).prependTo(this.$editor);
              }
              /**
               * attach Drag and Drop Events
               */
              Dropzone.prototype.initialize = function() {
                if (this.options.disableDragAndDrop) {
                  // prevent default drop event
                  this.documentEventHandlers.onDrop = function(e) {
                    e.preventDefault();
                  };
                  // do not consider outside of dropzone
                  this.$eventListener = this.$dropzone;
                  this.$eventListener.on(
                    "drop",
                    this.documentEventHandlers.onDrop
                  );
                } else {
                  this.attachDragAndDropEvent();
                }
              };
              /**
               * attach Drag and Drop Events
               */
              Dropzone.prototype.attachDragAndDropEvent = function() {
                var _this = this;
                var collection = $$1();
                var $dropzoneMessage = this.$dropzone.find(
                  ".note-dropzone-message"
                );
                this.documentEventHandlers.onDragenter = function(e) {
                  var isCodeview = _this.context.invoke("codeview.isActivated");
                  var hasEditorSize =
                    _this.$editor.width() > 0 && _this.$editor.height() > 0;
                  if (!isCodeview && !collection.length && hasEditorSize) {
                    _this.$editor.addClass("dragover");
                    _this.$dropzone.width(_this.$editor.width());
                    _this.$dropzone.height(_this.$editor.height());
                    $dropzoneMessage.text(_this.lang.image.dragImageHere);
                  }
                  collection = collection.add(e.target);
                };
                this.documentEventHandlers.onDragleave = function(e) {
                  collection = collection.not(e.target);
                  if (!collection.length) {
                    _this.$editor.removeClass("dragover");
                  }
                };
                this.documentEventHandlers.onDrop = function() {
                  collection = $$1();
                  _this.$editor.removeClass("dragover");
                };
                // show dropzone on dragenter when dragging a object to document
                // -but only if the editor is visible, i.e. has a positive width and height
                this.$eventListener
                  .on("dragenter", this.documentEventHandlers.onDragenter)
                  .on("dragleave", this.documentEventHandlers.onDragleave)
                  .on("drop", this.documentEventHandlers.onDrop);
                // change dropzone's message on hover.
                this.$dropzone
                  .on("dragenter", function() {
                    _this.$dropzone.addClass("hover");
                    $dropzoneMessage.text(_this.lang.image.dropImage);
                  })
                  .on("dragleave", function() {
                    _this.$dropzone.removeClass("hover");
                    $dropzoneMessage.text(_this.lang.image.dragImageHere);
                  });
                // attach dropImage
                this.$dropzone
                  .on("drop", function(event) {
                    var dataTransfer = event.originalEvent.dataTransfer;
                    // stop the browser from opening the dropped content
                    event.preventDefault();
                    if (
                      dataTransfer &&
                      dataTransfer.files &&
                      dataTransfer.files.length
                    ) {
                      _this.$editable.focus();
                      _this.context.invoke(
                        "editor.insertImagesOrCallback",
                        dataTransfer.files
                      );
                    } else {
                      $$1.each(dataTransfer.types, function(idx, type) {
                        var content = dataTransfer.getData(type);
                        if (type.toLowerCase().indexOf("text") > -1) {
                          _this.context.invoke("editor.pasteHTML", content);
                        } else {
                          $$1(content).each(function(idx, item) {
                            _this.context.invoke("editor.insertNode", item);
                          });
                        }
                      });
                    }
                  })
                  .on("dragover", false); // prevent default dragover event
              };
              Dropzone.prototype.destroy = function() {
                var _this = this;
                Object.keys(this.documentEventHandlers).forEach(function(key) {
                  _this.$eventListener.off(
                    key.substr(2).toLowerCase(),
                    _this.documentEventHandlers[key]
                  );
                });
                this.documentEventHandlers = {};
              };
              return Dropzone;
            })();

            var CodeMirror;
            if (env.hasCodeMirror) {
              CodeMirror = window.CodeMirror;
            }
            /**
             * @class Codeview
             */
            var CodeView = /** @class */ (function() {
              function CodeView(context) {
                this.context = context;
                this.$editor = context.layoutInfo.editor;
                this.$editable = context.layoutInfo.editable;
                this.$codable = context.layoutInfo.codable;
                this.options = context.options;
              }
              CodeView.prototype.sync = function() {
                var isCodeview = this.isActivated();
                if (isCodeview && env.hasCodeMirror) {
                  this.$codable.data("cmEditor").save();
                }
              };
              /**
               * @return {Boolean}
               */
              CodeView.prototype.isActivated = function() {
                return this.$editor.hasClass("codeview");
              };
              /**
               * toggle codeview
               */
              CodeView.prototype.toggle = function() {
                if (this.isActivated()) {
                  this.deactivate();
                } else {
                  this.activate();
                }
                this.context.triggerEvent("codeview.toggled");
              };
              /**
               * purify input value
               * @param value
               * @returns {*}
               */
              CodeView.prototype.purify = function(value) {
                if (this.options.codeviewFilter) {
                  // filter code view regex
                  value = value.replace(this.options.codeviewFilterRegex, "");
                  // allow specific iframe tag
                  if (this.options.codeviewIframeFilter) {
                    var whitelist_1 = this.options.codeviewIframeWhitelistSrc.concat(
                      this.options.codeviewIframeWhitelistSrcBase
                    );
                    value = value.replace(
                      /(<iframe.*?>.*?(?:<\/iframe>)?)/gi,
                      function(tag) {
                        // remove if src attribute is duplicated
                        if (
                          /<.+src(?==?('|"|\s)?)[\s\S]+src(?=('|"|\s)?)[^>]*?>/i.test(
                            tag
                          )
                        ) {
                          return "";
                        }
                        for (
                          var _i = 0, whitelist_2 = whitelist_1;
                          _i < whitelist_2.length;
                          _i++
                        ) {
                          var src = whitelist_2[_i];
                          // pass if src is trusted
                          if (
                            new RegExp(
                              'src="(https?:)?//' +
                                src.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&") +
                                '/(.+)"'
                            ).test(tag)
                          ) {
                            return tag;
                          }
                        }
                        return "";
                      }
                    );
                  }
                }
                return value;
              };
              /**
               * activate code view
               */
              CodeView.prototype.activate = function() {
                var _this = this;
                this.$codable.val(
                  dom.html(this.$editable, this.options.prettifyHtml)
                );
                this.$codable.height(this.$editable.height());
                this.context.invoke("toolbar.updateCodeview", true);
                this.$editor.addClass("codeview");
                this.$codable.focus();
                // activate CodeMirror as codable
                if (env.hasCodeMirror) {
                  var cmEditor_1 = CodeMirror.fromTextArea(
                    this.$codable[0],
                    this.options.codemirror
                  );
                  // CodeMirror TernServer
                  if (this.options.codemirror.tern) {
                    var server_1 = new CodeMirror.TernServer(
                      this.options.codemirror.tern
                    );
                    cmEditor_1.ternServer = server_1;
                    cmEditor_1.on("cursorActivity", function(cm) {
                      server_1.updateArgHints(cm);
                    });
                  }
                  cmEditor_1.on("blur", function(event) {
                    _this.context.triggerEvent(
                      "blur.codeview",
                      cmEditor_1.getValue(),
                      event
                    );
                  });
                  cmEditor_1.on("change", function(event) {
                    _this.context.triggerEvent(
                      "change.codeview",
                      cmEditor_1.getValue(),
                      cmEditor_1
                    );
                  });
                  // CodeMirror hasn't Padding.
                  cmEditor_1.setSize(null, this.$editable.outerHeight());
                  this.$codable.data("cmEditor", cmEditor_1);
                } else {
                  this.$codable.on("blur", function(event) {
                    _this.context.triggerEvent(
                      "blur.codeview",
                      _this.$codable.val(),
                      event
                    );
                  });
                  this.$codable.on("input", function(event) {
                    _this.context.triggerEvent(
                      "change.codeview",
                      _this.$codable.val(),
                      _this.$codable
                    );
                  });
                }
              };
              /**
               * deactivate code view
               */
              CodeView.prototype.deactivate = function() {
                // deactivate CodeMirror as codable
                if (env.hasCodeMirror) {
                  var cmEditor = this.$codable.data("cmEditor");
                  this.$codable.val(cmEditor.getValue());
                  cmEditor.toTextArea();
                }
                var value = this.purify(
                  dom.value(this.$codable, this.options.prettifyHtml) ||
                    dom.emptyPara
                );
                var isChange = this.$editable.html() !== value;
                this.$editable.html(value);
                this.$editable.height(
                  this.options.height ? this.$codable.height() : "auto"
                );
                this.$editor.removeClass("codeview");
                if (isChange) {
                  this.context.triggerEvent(
                    "change",
                    this.$editable.html(),
                    this.$editable
                  );
                }
                this.$editable.focus();
                this.context.invoke("toolbar.updateCodeview", false);
              };
              CodeView.prototype.destroy = function() {
                if (this.isActivated()) {
                  this.deactivate();
                }
              };
              return CodeView;
            })();

            var EDITABLE_PADDING = 24;
            var Statusbar = /** @class */ (function() {
              function Statusbar(context) {
                this.$document = $$1(document);
                this.$statusbar = context.layoutInfo.statusbar;
                this.$editable = context.layoutInfo.editable;
                this.options = context.options;
              }
              Statusbar.prototype.initialize = function() {
                var _this = this;
                if (this.options.airMode || this.options.disableResizeEditor) {
                  this.destroy();
                  return;
                }
                this.$statusbar.on("mousedown", function(event) {
                  event.preventDefault();
                  event.stopPropagation();
                  var editableTop =
                    _this.$editable.offset().top - _this.$document.scrollTop();
                  var onMouseMove = function(event) {
                    var height =
                      event.clientY - (editableTop + EDITABLE_PADDING);
                    height =
                      _this.options.minheight > 0
                        ? Math.max(height, _this.options.minheight)
                        : height;
                    height =
                      _this.options.maxHeight > 0
                        ? Math.min(height, _this.options.maxHeight)
                        : height;
                    _this.$editable.height(height);
                  };
                  _this.$document
                    .on("mousemove", onMouseMove)
                    .one("mouseup", function() {
                      _this.$document.off("mousemove", onMouseMove);
                    });
                });
              };
              Statusbar.prototype.destroy = function() {
                this.$statusbar.off();
                this.$statusbar.addClass("locked");
              };
              return Statusbar;
            })();

            var Fullscreen = /** @class */ (function() {
              function Fullscreen(context) {
                var _this = this;
                this.context = context;
                this.$editor = context.layoutInfo.editor;
                this.$toolbar = context.layoutInfo.toolbar;
                this.$editable = context.layoutInfo.editable;
                this.$codable = context.layoutInfo.codable;
                this.$window = $$1(window);
                this.$scrollbar = $$1("html, body");
                this.onResize = function() {
                  _this.resizeTo({
                    h: _this.$window.height() - _this.$toolbar.outerHeight()
                  });
                };
              }
              Fullscreen.prototype.resizeTo = function(size) {
                this.$editable.css("height", size.h);
                this.$codable.css("height", size.h);
                if (this.$codable.data("cmeditor")) {
                  this.$codable.data("cmeditor").setsize(null, size.h);
                }
              };
              /**
               * toggle fullscreen
               */
              Fullscreen.prototype.toggle = function() {
                this.$editor.toggleClass("fullscreen");
                if (this.isFullscreen()) {
                  this.$editable.data(
                    "orgHeight",
                    this.$editable.css("height")
                  );
                  this.$editable.data(
                    "orgMaxHeight",
                    this.$editable.css("maxHeight")
                  );
                  this.$editable.css("maxHeight", "");
                  this.$window.on("resize", this.onResize).trigger("resize");
                  this.$scrollbar.css("overflow", "hidden");
                } else {
                  this.$window.off("resize", this.onResize);
                  this.resizeTo({ h: this.$editable.data("orgHeight") });
                  this.$editable.css(
                    "maxHeight",
                    this.$editable.css("orgMaxHeight")
                  );
                  this.$scrollbar.css("overflow", "visible");
                }
                this.context.invoke(
                  "toolbar.updateFullscreen",
                  this.isFullscreen()
                );
              };
              Fullscreen.prototype.isFullscreen = function() {
                return this.$editor.hasClass("fullscreen");
              };
              return Fullscreen;
            })();

            var Handle = /** @class */ (function() {
              function Handle(context) {
                var _this = this;
                this.context = context;
                this.$document = $$1(document);
                this.$editingArea = context.layoutInfo.editingArea;
                this.options = context.options;
                this.lang = this.options.langInfo;
                this.events = {
                  "summernote.mousedown": function(we, e) {
                    if (_this.update(e.target, e)) {
                      e.preventDefault();
                    }
                  },
                  "summernote.keyup summernote.scroll summernote.change summernote.dialog.shown": function() {
                    _this.update();
                  },
                  "summernote.disable": function() {
                    _this.hide();
                  },
                  "summernote.codeview.toggled": function() {
                    _this.update();
                  }
                };
              }
              Handle.prototype.initialize = function() {
                var _this = this;
                this.$handle = $$1(
                  [
                    '<div class="note-handle">',
                    '<div class="note-control-selection">',
                    '<div class="note-control-selection-bg"></div>',
                    '<div class="note-control-holder note-control-nw"></div>',
                    '<div class="note-control-holder note-control-ne"></div>',
                    '<div class="note-control-holder note-control-sw"></div>',
                    '<div class="',
                    this.options.disableResizeImage
                      ? "note-control-holder"
                      : "note-control-sizing",
                    ' note-control-se"></div>',
                    this.options.disableResizeImage
                      ? ""
                      : '<div class="note-control-selection-info"></div>',
                    "</div>",
                    "</div>"
                  ].join("")
                ).prependTo(this.$editingArea);
                this.$handle.on("mousedown", function(event) {
                  if (dom.isControlSizing(event.target)) {
                    event.preventDefault();
                    event.stopPropagation();
                    var $target_1 = _this.$handle
                      .find(".note-control-selection")
                      .data("target");
                    var posStart_1 = $target_1.offset();
                    var scrollTop_1 = _this.$document.scrollTop();
                    var onMouseMove_1 = function(event) {
                      _this.context.invoke(
                        "editor.resizeTo",
                        {
                          x: event.clientX - posStart_1.left,
                          y: event.clientY - (posStart_1.top - scrollTop_1)
                        },
                        $target_1,
                        !event.shiftKey
                      );
                      _this.update($target_1[0]);
                    };
                    _this.$document
                      .on("mousemove", onMouseMove_1)
                      .one("mouseup", function(e) {
                        e.preventDefault();
                        _this.$document.off("mousemove", onMouseMove_1);
                        _this.context.invoke("editor.afterCommand");
                      });
                    if (!$target_1.data("ratio")) {
                      // original ratio.
                      $target_1.data(
                        "ratio",
                        $target_1.height() / $target_1.width()
                      );
                    }
                  }
                });
                // Listen for scrolling on the handle overlay.
                this.$handle.on("wheel", function(e) {
                  e.preventDefault();
                  _this.update();
                });
              };
              Handle.prototype.destroy = function() {
                this.$handle.remove();
              };
              Handle.prototype.update = function(target, event) {
                if (this.context.isDisabled()) {
                  return false;
                }
                var isImage = dom.isImg(target);
                var $selection = this.$handle.find(".note-control-selection");
                this.context.invoke("imagePopover.update", target, event);
                if (isImage) {
                  var $image = $$1(target);
                  var position = $image.position();
                  var pos = {
                    left:
                      position.left + parseInt($image.css("marginLeft"), 10),
                    top: position.top + parseInt($image.css("marginTop"), 10)
                  };
                  // exclude margin
                  var imageSize = {
                    w: $image.outerWidth(false),
                    h: $image.outerHeight(false)
                  };
                  $selection
                    .css({
                      display: "block",
                      left: pos.left,
                      top: pos.top,
                      width: imageSize.w,
                      height: imageSize.h
                    })
                    .data("target", $image); // save current image element.
                  var origImageObj = new Image();
                  origImageObj.src = $image.attr("src");
                  var sizingText =
                    imageSize.w +
                    "x" +
                    imageSize.h +
                    " (" +
                    this.lang.image.original +
                    ": " +
                    origImageObj.width +
                    "x" +
                    origImageObj.height +
                    ")";
                  $selection
                    .find(".note-control-selection-info")
                    .text(sizingText);
                  this.context.invoke("editor.saveTarget", target);
                } else {
                  this.hide();
                }
                return isImage;
              };
              /**
               * hide
               *
               * @param {jQuery} $handle
               */
              Handle.prototype.hide = function() {
                this.context.invoke("editor.clearTarget");
                this.$handle.children().hide();
              };
              return Handle;
            })();

            var defaultScheme = "http://";
            var linkPattern = /^([A-Za-z][A-Za-z0-9+-.]*\:[\/]{2}|mailto:[A-Z0-9._%+-]+@)?(www\.)?(.+)$/i;
            var AutoLink = /** @class */ (function() {
              function AutoLink(context) {
                var _this = this;
                this.context = context;
                this.events = {
                  "summernote.keyup": function(we, e) {
                    if (!e.isDefaultPrevented()) {
                      _this.handleKeyup(e);
                    }
                  },
                  "summernote.keydown": function(we, e) {
                    _this.handleKeydown(e);
                  }
                };
              }
              AutoLink.prototype.initialize = function() {
                this.lastWordRange = null;
              };
              AutoLink.prototype.destroy = function() {
                this.lastWordRange = null;
              };
              AutoLink.prototype.replace = function() {
                if (!this.lastWordRange) {
                  return;
                }
                var keyword = this.lastWordRange.toString();
                var match = keyword.match(linkPattern);
                if (match && (match[1] || match[2])) {
                  var link = match[1] ? keyword : defaultScheme + keyword;
                  var node = $$1("<a />")
                    .html(keyword)
                    .attr("href", link)[0];
                  if (this.context.options.linkTargetBlank) {
                    $$1(node).attr("target", "_blank");
                  }
                  this.lastWordRange.insertNode(node);
                  this.lastWordRange = null;
                  this.context.invoke("editor.focus");
                }
              };
              AutoLink.prototype.handleKeydown = function(e) {
                if (
                  lists.contains([key.code.ENTER, key.code.SPACE], e.keyCode)
                ) {
                  var wordRange = this.context
                    .invoke("editor.createRange")
                    .getWordRange();
                  this.lastWordRange = wordRange;
                }
              };
              AutoLink.prototype.handleKeyup = function(e) {
                if (
                  lists.contains([key.code.ENTER, key.code.SPACE], e.keyCode)
                ) {
                  this.replace();
                }
              };
              return AutoLink;
            })();

            /**
             * textarea auto sync.
             */
            var AutoSync = /** @class */ (function() {
              function AutoSync(context) {
                var _this = this;
                this.$note = context.layoutInfo.note;
                this.events = {
                  "summernote.change": function() {
                    _this.$note.val(context.invoke("code"));
                  }
                };
              }
              AutoSync.prototype.shouldInitialize = function() {
                return dom.isTextarea(this.$note[0]);
              };
              return AutoSync;
            })();

            var AutoReplace = /** @class */ (function() {
              function AutoReplace(context) {
                var _this = this;
                this.context = context;
                this.options = context.options.replace || {};
                this.keys = [
                  key.code.ENTER,
                  key.code.SPACE,
                  key.code.PERIOD,
                  key.code.COMMA,
                  key.code.SEMICOLON,
                  key.code.SLASH
                ];
                this.previousKeydownCode = null;
                this.events = {
                  "summernote.keyup": function(we, e) {
                    if (!e.isDefaultPrevented()) {
                      _this.handleKeyup(e);
                    }
                  },
                  "summernote.keydown": function(we, e) {
                    _this.handleKeydown(e);
                  }
                };
              }
              AutoReplace.prototype.shouldInitialize = function() {
                return !!this.options.match;
              };
              AutoReplace.prototype.initialize = function() {
                this.lastWord = null;
              };
              AutoReplace.prototype.destroy = function() {
                this.lastWord = null;
              };
              AutoReplace.prototype.replace = function() {
                if (!this.lastWord) {
                  return;
                }
                var self = this;
                var keyword = this.lastWord.toString();
                this.options.match(keyword, function(match) {
                  if (match) {
                    var node = "";
                    if (typeof match === "string") {
                      node = dom.createText(match);
                    } else if (match instanceof jQuery) {
                      node = match[0];
                    } else if (match instanceof Node) {
                      node = match;
                    }
                    if (!node) return;
                    self.lastWord.insertNode(node);
                    self.lastWord = null;
                    self.context.invoke("editor.focus");
                  }
                });
              };
              AutoReplace.prototype.handleKeydown = function(e) {
                // this forces it to remember the last whole word, even if multiple termination keys are pressed
                // before the previous key is let go.
                if (
                  this.previousKeydownCode &&
                  lists.contains(this.keys, this.previousKeydownCode)
                ) {
                  this.previousKeydownCode = e.keyCode;
                  return;
                }
                if (lists.contains(this.keys, e.keyCode)) {
                  var wordRange = this.context
                    .invoke("editor.createRange")
                    .getWordRange();
                  this.lastWord = wordRange;
                }
                this.previousKeydownCode = e.keyCode;
              };
              AutoReplace.prototype.handleKeyup = function(e) {
                if (lists.contains(this.keys, e.keyCode)) {
                  this.replace();
                }
              };
              return AutoReplace;
            })();

            var Placeholder = /** @class */ (function() {
              function Placeholder(context) {
                var _this = this;
                this.context = context;
                this.$editingArea = context.layoutInfo.editingArea;
                this.options = context.options;
                this.events = {
                  "summernote.init summernote.change": function() {
                    _this.update();
                  },
                  "summernote.codeview.toggled": function() {
                    _this.update();
                  }
                };
              }
              Placeholder.prototype.shouldInitialize = function() {
                return !!this.options.placeholder;
              };
              Placeholder.prototype.initialize = function() {
                var _this = this;
                this.$placeholder = $$1('<div class="note-placeholder">');
                this.$placeholder
                  .on("click", function() {
                    _this.context.invoke("focus");
                  })
                  .html(this.options.placeholder)
                  .prependTo(this.$editingArea);
                this.update();
              };
              Placeholder.prototype.destroy = function() {
                this.$placeholder.remove();
              };
              Placeholder.prototype.update = function() {
                var isShow =
                  !this.context.invoke("codeview.isActivated") &&
                  this.context.invoke("editor.isEmpty");
                this.$placeholder.toggle(isShow);
              };
              return Placeholder;
            })();

            var Buttons = /** @class */ (function() {
              function Buttons(context) {
                this.ui = $$1.summernote.ui;
                this.context = context;
                this.$toolbar = context.layoutInfo.toolbar;
                this.options = context.options;
                this.lang = this.options.langInfo;
                this.invertedKeyMap = func.invertObject(
                  this.options.keyMap[env.isMac ? "mac" : "pc"]
                );
              }
              Buttons.prototype.representShortcut = function(editorMethod) {
                var shortcut = this.invertedKeyMap[editorMethod];
                if (!this.options.shortcuts || !shortcut) {
                  return "";
                }
                if (env.isMac) {
                  shortcut = shortcut.replace("CMD", "⌘").replace("SHIFT", "⇧");
                }
                shortcut = shortcut
                  .replace("BACKSLASH", "\\")
                  .replace("SLASH", "/")
                  .replace("LEFTBRACKET", "[")
                  .replace("RIGHTBRACKET", "]");
                return " (" + shortcut + ")";
              };
              Buttons.prototype.button = function(o) {
                if (!this.options.tooltip && o.tooltip) {
                  delete o.tooltip;
                }
                o.container = this.options.container;
                return this.ui.button(o);
              };
              Buttons.prototype.initialize = function() {
                this.addToolbarButtons();
                this.addImagePopoverButtons();
                this.addLinkPopoverButtons();
                this.addTablePopoverButtons();
                this.fontInstalledMap = {};
              };
              Buttons.prototype.destroy = function() {
                delete this.fontInstalledMap;
              };
              Buttons.prototype.isFontInstalled = function(name) {
                if (!this.fontInstalledMap.hasOwnProperty(name)) {
                  this.fontInstalledMap[name] =
                    env.isFontInstalled(name) ||
                    lists.contains(this.options.fontNamesIgnoreCheck, name);
                }
                return this.fontInstalledMap[name];
              };
              Buttons.prototype.isFontDeservedToAdd = function(name) {
                var genericFamilies = [
                  "sans-serif",
                  "serif",
                  "monospace",
                  "cursive",
                  "fantasy"
                ];
                name = name.toLowerCase();
                return (
                  name !== "" &&
                  this.isFontInstalled(name) &&
                  genericFamilies.indexOf(name) === -1
                );
              };
              Buttons.prototype.colorPalette = function(
                className,
                tooltip,
                backColor,
                foreColor
              ) {
                var _this = this;
                return this.ui
                  .buttonGroup({
                    className: "note-color " + className,
                    children: [
                      this.button({
                        className: "note-current-color-button",
                        contents: this.ui.icon(
                          this.options.icons.font + " note-recent-color"
                        ),
                        tooltip: tooltip,
                        click: function(e) {
                          var $button = $$1(e.currentTarget);
                          if (backColor && foreColor) {
                            _this.context.invoke("editor.color", {
                              backColor: $button.attr("data-backColor"),
                              foreColor: $button.attr("data-foreColor")
                            });
                          } else if (backColor) {
                            _this.context.invoke("editor.color", {
                              backColor: $button.attr("data-backColor")
                            });
                          } else if (foreColor) {
                            _this.context.invoke("editor.color", {
                              foreColor: $button.attr("data-foreColor")
                            });
                          }
                        },
                        callback: function($button) {
                          var $recentColor = $button.find(".note-recent-color");
                          if (backColor) {
                            $recentColor.css(
                              "background-color",
                              _this.options.colorButton.backColor
                            );
                            $button.attr(
                              "data-backColor",
                              _this.options.colorButton.backColor
                            );
                          }
                          if (foreColor) {
                            $recentColor.css(
                              "color",
                              _this.options.colorButton.foreColor
                            );
                            $button.attr(
                              "data-foreColor",
                              _this.options.colorButton.foreColor
                            );
                          } else {
                            $recentColor.css("color", "transparent");
                          }
                        }
                      }),
                      this.button({
                        className: "dropdown-toggle",
                        contents: this.ui.dropdownButtonContents(
                          "",
                          this.options
                        ),
                        tooltip: this.lang.color.more,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      this.ui.dropdown({
                        items:
                          (backColor
                            ? [
                                '<div class="note-palette">',
                                '  <div class="note-palette-title">' +
                                  this.lang.color.background +
                                  "</div>",
                                "  <div>",
                                '    <button type="button" class="note-color-reset btn btn-light" data-event="backColor" data-value="inherit">',
                                this.lang.color.transparent,
                                "    </button>",
                                "  </div>",
                                '  <div class="note-holder" data-event="backColor"/>',
                                "  <div>",
                                '    <button type="button" class="note-color-select btn" data-event="openPalette" data-value="backColorPicker">',
                                this.lang.color.cpSelect,
                                "    </button>",
                                '    <input type="color" id="backColorPicker" class="note-btn note-color-select-btn" value="' +
                                  this.options.colorButton.backColor +
                                  '" data-event="backColorPalette">',
                                "  </div>",
                                '  <div class="note-holder-custom" id="backColorPalette" data-event="backColor"/>',
                                "</div>"
                              ].join("")
                            : "") +
                          (foreColor
                            ? [
                                '<div class="note-palette">',
                                '  <div class="note-palette-title">' +
                                  this.lang.color.foreground +
                                  "</div>",
                                "  <div>",
                                '    <button type="button" class="note-color-reset btn btn-light" data-event="removeFormat" data-value="foreColor">',
                                this.lang.color.resetToDefault,
                                "    </button>",
                                "  </div>",
                                '  <div class="note-holder" data-event="foreColor"/>',
                                "  <div>",
                                '    <button type="button" class="note-color-select btn" data-event="openPalette" data-value="foreColorPicker">',
                                this.lang.color.cpSelect,
                                "    </button>",
                                '    <input type="color" id="foreColorPicker" class="note-btn note-color-select-btn" value="' +
                                  this.options.colorButton.foreColor +
                                  '" data-event="foreColorPalette">',
                                '  <div class="note-holder-custom" id="foreColorPalette" data-event="foreColor"/>',
                                "</div>"
                              ].join("")
                            : ""),
                        callback: function($dropdown) {
                          $dropdown
                            .find(".note-holder")
                            .each(function(idx, item) {
                              var $holder = $$1(item);
                              $holder.append(
                                _this.ui
                                  .palette({
                                    colors: _this.options.colors,
                                    colorsName: _this.options.colorsName,
                                    eventName: $holder.data("event"),
                                    container: _this.options.container,
                                    tooltip: _this.options.tooltip
                                  })
                                  .render()
                              );
                            });
                          /* TODO: do we have to record recent custom colors within cookies? */
                          var customColors = [
                            [
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF",
                              "#FFFFFF"
                            ]
                          ];
                          $dropdown
                            .find(".note-holder-custom")
                            .each(function(idx, item) {
                              var $holder = $$1(item);
                              $holder.append(
                                _this.ui
                                  .palette({
                                    colors: customColors,
                                    colorsName: customColors,
                                    eventName: $holder.data("event"),
                                    container: _this.options.container,
                                    tooltip: _this.options.tooltip
                                  })
                                  .render()
                              );
                            });
                          $dropdown
                            .find("input[type=color]")
                            .each(function(idx, item) {
                              $$1(item).change(function() {
                                var $chip = $dropdown
                                  .find("#" + $$1(this).data("event"))
                                  .find(".note-color-btn")
                                  .first();
                                var color = this.value.toUpperCase();
                                $chip
                                  .css("background-color", color)
                                  .attr("aria-label", color)
                                  .attr("data-value", color)
                                  .attr("data-original-title", color);
                                $chip.click();
                              });
                            });
                        },
                        click: function(event) {
                          event.stopPropagation();
                          var $parent = $$1("." + className);
                          var $button = $$1(event.target);
                          var eventName = $button.data("event");
                          var value = $button.attr("data-value");
                          if (eventName === "openPalette") {
                            var $picker = $parent.find("#" + value);
                            var $palette = $$1(
                              $parent
                                .find("#" + $picker.data("event"))
                                .find(".note-color-row")[0]
                            );
                            // Shift palette chips
                            var $chip = $palette
                              .find(".note-color-btn")
                              .last()
                              .detach();
                            // Set chip attributes
                            var color = $picker.val();
                            $chip
                              .css("background-color", color)
                              .attr("aria-label", color)
                              .attr("data-value", color)
                              .attr("data-original-title", color);
                            $palette.prepend($chip);
                            $picker.click();
                          } else if (
                            lists.contains(
                              ["backColor", "foreColor"],
                              eventName
                            )
                          ) {
                            var key =
                              eventName === "backColor"
                                ? "background-color"
                                : "color";
                            var $color = $button
                              .closest(".note-color")
                              .find(".note-recent-color");
                            var $currentButton = $button
                              .closest(".note-color")
                              .find(".note-current-color-button");
                            $color.css(key, value);
                            $currentButton.attr("data-" + eventName, value);
                            _this.context.invoke("editor." + eventName, value);
                          }
                        }
                      })
                    ]
                  })
                  .render();
              };
              Buttons.prototype.addToolbarButtons = function() {
                var _this = this;
                this.context.memo("button.style", function() {
                  return _this.ui
                    .buttonGroup([
                      _this.button({
                        className: "dropdown-toggle",
                        contents: _this.ui.dropdownButtonContents(
                          _this.ui.icon(_this.options.icons.magic),
                          _this.options
                        ),
                        tooltip: _this.lang.style.style,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      _this.ui.dropdown({
                        className: "dropdown-style",
                        items: _this.options.styleTags,
                        title: _this.lang.style.style,
                        template: function(item) {
                          if (typeof item === "string") {
                            item = {
                              tag: item,
                              title: _this.lang.style.hasOwnProperty(item)
                                ? _this.lang.style[item]
                                : item
                            };
                          }
                          var tag = item.tag;
                          var title = item.title;
                          var style = item.style
                            ? ' style="' + item.style + '" '
                            : "";
                          var className = item.className
                            ? ' class="' + item.className + '"'
                            : "";
                          return (
                            "<" +
                            tag +
                            style +
                            className +
                            ">" +
                            title +
                            "</" +
                            tag +
                            ">"
                          );
                        },
                        click: _this.context.createInvokeHandler(
                          "editor.formatBlock"
                        )
                      })
                    ])
                    .render();
                });
                var _loop_1 = function(styleIdx, styleLen) {
                  var item = this_1.options.styleTags[styleIdx];
                  this_1.context.memo("button.style." + item, function() {
                    return _this
                      .button({
                        className: "note-btn-style-" + item,
                        contents:
                          '<div data-value="' +
                          item +
                          '">' +
                          item.toUpperCase() +
                          "</div>",
                        tooltip: _this.lang.style[item],
                        click: _this.context.createInvokeHandler(
                          "editor.formatBlock"
                        )
                      })
                      .render();
                  });
                };
                var this_1 = this;
                for (
                  var styleIdx = 0, styleLen = this.options.styleTags.length;
                  styleIdx < styleLen;
                  styleIdx++
                ) {
                  _loop_1(styleIdx, styleLen);
                }
                this.context.memo("button.bold", function() {
                  return _this
                    .button({
                      className: "note-btn-bold",
                      contents: _this.ui.icon(_this.options.icons.bold),
                      tooltip:
                        _this.lang.font.bold + _this.representShortcut("bold"),
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.bold"
                      )
                    })
                    .render();
                });
                this.context.memo("button.italic", function() {
                  return _this
                    .button({
                      className: "note-btn-italic",
                      contents: _this.ui.icon(_this.options.icons.italic),
                      tooltip:
                        _this.lang.font.italic +
                        _this.representShortcut("italic"),
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.italic"
                      )
                    })
                    .render();
                });
                this.context.memo("button.underline", function() {
                  return _this
                    .button({
                      className: "note-btn-underline",
                      contents: _this.ui.icon(_this.options.icons.underline),
                      tooltip:
                        _this.lang.font.underline +
                        _this.representShortcut("underline"),
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.underline"
                      )
                    })
                    .render();
                });
                this.context.memo("button.clear", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.eraser),
                      tooltip:
                        _this.lang.font.clear +
                        _this.representShortcut("removeFormat"),
                      click: _this.context.createInvokeHandler(
                        "editor.removeFormat"
                      )
                    })
                    .render();
                });
                this.context.memo("button.strikethrough", function() {
                  return _this
                    .button({
                      className: "note-btn-strikethrough",
                      contents: _this.ui.icon(
                        _this.options.icons.strikethrough
                      ),
                      tooltip:
                        _this.lang.font.strikethrough +
                        _this.representShortcut("strikethrough"),
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.strikethrough"
                      )
                    })
                    .render();
                });
                this.context.memo("button.superscript", function() {
                  return _this
                    .button({
                      className: "note-btn-superscript",
                      contents: _this.ui.icon(_this.options.icons.superscript),
                      tooltip: _this.lang.font.superscript,
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.superscript"
                      )
                    })
                    .render();
                });
                this.context.memo("button.subscript", function() {
                  return _this
                    .button({
                      className: "note-btn-subscript",
                      contents: _this.ui.icon(_this.options.icons.subscript),
                      tooltip: _this.lang.font.subscript,
                      click: _this.context.createInvokeHandlerAndUpdateState(
                        "editor.subscript"
                      )
                    })
                    .render();
                });
                this.context.memo("button.fontname", function() {
                  var styleInfo = _this.context.invoke("editor.currentStyle");
                  // Add 'default' fonts into the fontnames array if not exist
                  $$1.each(styleInfo["font-family"].split(","), function(
                    idx,
                    fontname
                  ) {
                    fontname = fontname.trim().replace(/['"]+/g, "");
                    if (_this.isFontDeservedToAdd(fontname)) {
                      if (_this.options.fontNames.indexOf(fontname) === -1) {
                        _this.options.fontNames.push(fontname);
                      }
                    }
                  });
                  return _this.ui
                    .buttonGroup([
                      _this.button({
                        className: "dropdown-toggle",
                        contents: _this.ui.dropdownButtonContents(
                          '<span class="note-current-fontname"/>',
                          _this.options
                        ),
                        tooltip: _this.lang.font.name,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      _this.ui.dropdownCheck({
                        className: "dropdown-fontname",
                        checkClassName: _this.options.icons.menuCheck,
                        items: _this.options.fontNames.filter(
                          _this.isFontInstalled.bind(_this)
                        ),
                        title: _this.lang.font.name,
                        template: function(item) {
                          return (
                            "<span style=\"font-family: '" +
                            item +
                            "'\">" +
                            item +
                            "</span>"
                          );
                        },
                        click: _this.context.createInvokeHandlerAndUpdateState(
                          "editor.fontName"
                        )
                      })
                    ])
                    .render();
                });
                this.context.memo("button.fontsize", function() {
                  return _this.ui
                    .buttonGroup([
                      _this.button({
                        className: "dropdown-toggle",
                        contents: _this.ui.dropdownButtonContents(
                          '<span class="note-current-fontsize"/>',
                          _this.options
                        ),
                        tooltip: _this.lang.font.size,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      _this.ui.dropdownCheck({
                        className: "dropdown-fontsize",
                        checkClassName: _this.options.icons.menuCheck,
                        items: _this.options.fontSizes,
                        title: _this.lang.font.size,
                        click: _this.context.createInvokeHandlerAndUpdateState(
                          "editor.fontSize"
                        )
                      })
                    ])
                    .render();
                });
                this.context.memo("button.color", function() {
                  return _this.colorPalette(
                    "note-color-all",
                    _this.lang.color.recent,
                    true,
                    true
                  );
                });
                this.context.memo("button.forecolor", function() {
                  return _this.colorPalette(
                    "note-color-fore",
                    _this.lang.color.foreground,
                    false,
                    true
                  );
                });
                this.context.memo("button.backcolor", function() {
                  return _this.colorPalette(
                    "note-color-back",
                    _this.lang.color.background,
                    true,
                    false
                  );
                });
                this.context.memo("button.ul", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(
                        _this.options.icons.unorderedlist
                      ),
                      tooltip:
                        _this.lang.lists.unordered +
                        _this.representShortcut("insertUnorderedList"),
                      click: _this.context.createInvokeHandler(
                        "editor.insertUnorderedList"
                      )
                    })
                    .render();
                });
                this.context.memo("button.ol", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.orderedlist),
                      tooltip:
                        _this.lang.lists.ordered +
                        _this.representShortcut("insertOrderedList"),
                      click: _this.context.createInvokeHandler(
                        "editor.insertOrderedList"
                      )
                    })
                    .render();
                });
                var justifyLeft = this.button({
                  contents: this.ui.icon(this.options.icons.alignLeft),
                  tooltip:
                    this.lang.paragraph.left +
                    this.representShortcut("justifyLeft"),
                  click: this.context.createInvokeHandler("editor.justifyLeft")
                });
                var justifyCenter = this.button({
                  contents: this.ui.icon(this.options.icons.alignCenter),
                  tooltip:
                    this.lang.paragraph.center +
                    this.representShortcut("justifyCenter"),
                  click: this.context.createInvokeHandler(
                    "editor.justifyCenter"
                  )
                });
                var justifyRight = this.button({
                  contents: this.ui.icon(this.options.icons.alignRight),
                  tooltip:
                    this.lang.paragraph.right +
                    this.representShortcut("justifyRight"),
                  click: this.context.createInvokeHandler("editor.justifyRight")
                });
                var justifyFull = this.button({
                  contents: this.ui.icon(this.options.icons.alignJustify),
                  tooltip:
                    this.lang.paragraph.justify +
                    this.representShortcut("justifyFull"),
                  click: this.context.createInvokeHandler("editor.justifyFull")
                });
                var outdent = this.button({
                  contents: this.ui.icon(this.options.icons.outdent),
                  tooltip:
                    this.lang.paragraph.outdent +
                    this.representShortcut("outdent"),
                  click: this.context.createInvokeHandler("editor.outdent")
                });
                var indent = this.button({
                  contents: this.ui.icon(this.options.icons.indent),
                  tooltip:
                    this.lang.paragraph.indent +
                    this.representShortcut("indent"),
                  click: this.context.createInvokeHandler("editor.indent")
                });
                this.context.memo(
                  "button.justifyLeft",
                  func.invoke(justifyLeft, "render")
                );
                this.context.memo(
                  "button.justifyCenter",
                  func.invoke(justifyCenter, "render")
                );
                this.context.memo(
                  "button.justifyRight",
                  func.invoke(justifyRight, "render")
                );
                this.context.memo(
                  "button.justifyFull",
                  func.invoke(justifyFull, "render")
                );
                this.context.memo(
                  "button.outdent",
                  func.invoke(outdent, "render")
                );
                this.context.memo(
                  "button.indent",
                  func.invoke(indent, "render")
                );
                this.context.memo("button.paragraph", function() {
                  return _this.ui
                    .buttonGroup([
                      _this.button({
                        className: "dropdown-toggle",
                        contents: _this.ui.dropdownButtonContents(
                          _this.ui.icon(_this.options.icons.alignLeft),
                          _this.options
                        ),
                        tooltip: _this.lang.paragraph.paragraph,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      _this.ui.dropdown([
                        _this.ui.buttonGroup({
                          className: "note-align",
                          children: [
                            justifyLeft,
                            justifyCenter,
                            justifyRight,
                            justifyFull
                          ]
                        }),
                        _this.ui.buttonGroup({
                          className: "note-list",
                          children: [outdent, indent]
                        })
                      ])
                    ])
                    .render();
                });
                this.context.memo("button.height", function() {
                  return _this.ui
                    .buttonGroup([
                      _this.button({
                        className: "dropdown-toggle",
                        contents: _this.ui.dropdownButtonContents(
                          _this.ui.icon(_this.options.icons.textHeight),
                          _this.options
                        ),
                        tooltip: _this.lang.font.height,
                        data: {
                          toggle: "dropdown"
                        }
                      }),
                      _this.ui.dropdownCheck({
                        items: _this.options.lineHeights,
                        checkClassName: _this.options.icons.menuCheck,
                        className: "dropdown-line-height",
                        title: _this.lang.font.height,
                        click: _this.context.createInvokeHandler(
                          "editor.lineHeight"
                        )
                      })
                    ])
                    .render();
                });
                this.context.memo("button.table", function() {
                  return _this.ui
                    .buttonGroup(
                      [
                        _this.button({
                          className: "dropdown-toggle",
                          contents: _this.ui.dropdownButtonContents(
                            _this.ui.icon(_this.options.icons.table),
                            _this.options
                          ),
                          tooltip: _this.lang.table.table,
                          data: {
                            toggle: "dropdown"
                          }
                        }),
                        _this.ui.dropdown({
                          title: _this.lang.table.table,
                          className: "note-table",
                          items: [
                            '<div class="note-dimension-picker">',
                            '  <div class="note-dimension-picker-mousecatcher" data-event="insertTable" data-value="1x1"/>',
                            '  <div class="note-dimension-picker-highlighted"/>',
                            '  <div class="note-dimension-picker-unhighlighted"/>',
                            "</div>",
                            '<div class="note-dimension-display">1 x 1</div>'
                          ].join("")
                        })
                      ],
                      {
                        callback: function($node) {
                          var $catcher = $node.find(
                            ".note-dimension-picker-mousecatcher"
                          );
                          $catcher
                            .css({
                              width:
                                _this.options.insertTableMaxSize.col + "em",
                              height:
                                _this.options.insertTableMaxSize.row + "em"
                            })
                            .mousedown(
                              _this.context.createInvokeHandler(
                                "editor.insertTable"
                              )
                            )
                            .on(
                              "mousemove",
                              _this.tableMoveHandler.bind(_this)
                            );
                        }
                      }
                    )
                    .render();
                });
                this.context.memo("button.link", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.link),
                      tooltip:
                        _this.lang.link.link +
                        _this.representShortcut("linkDialog.show"),
                      click: _this.context.createInvokeHandler(
                        "linkDialog.show"
                      )
                    })
                    .render();
                });
                this.context.memo("button.picture", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.picture),
                      tooltip: _this.lang.image.image,
                      click: _this.context.createInvokeHandler(
                        "imageDialog.show"
                      )
                    })
                    .render();
                });
                this.context.memo("button.video", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.video),
                      tooltip: _this.lang.video.video,
                      click: _this.context.createInvokeHandler(
                        "videoDialog.show"
                      )
                    })
                    .render();
                });
                this.context.memo("button.hr", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.minus),
                      tooltip:
                        _this.lang.hr.insert +
                        _this.representShortcut("insertHorizontalRule"),
                      click: _this.context.createInvokeHandler(
                        "editor.insertHorizontalRule"
                      )
                    })
                    .render();
                });
                this.context.memo("button.fullscreen", function() {
                  return _this
                    .button({
                      className: "btn-fullscreen",
                      contents: _this.ui.icon(_this.options.icons.arrowsAlt),
                      tooltip: _this.lang.options.fullscreen,
                      click: _this.context.createInvokeHandler(
                        "fullscreen.toggle"
                      )
                    })
                    .render();
                });
                this.context.memo("button.codeview", function() {
                  return _this
                    .button({
                      className: "btn-codeview",
                      contents: _this.ui.icon(_this.options.icons.code),
                      tooltip: _this.lang.options.codeview,
                      click: _this.context.createInvokeHandler(
                        "codeview.toggle"
                      )
                    })
                    .render();
                });
                this.context.memo("button.redo", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.redo),
                      tooltip:
                        _this.lang.history.redo +
                        _this.representShortcut("redo"),
                      click: _this.context.createInvokeHandler("editor.redo")
                    })
                    .render();
                });
                this.context.memo("button.undo", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.undo),
                      tooltip:
                        _this.lang.history.undo +
                        _this.representShortcut("undo"),
                      click: _this.context.createInvokeHandler("editor.undo")
                    })
                    .render();
                });
                this.context.memo("button.help", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.question),
                      tooltip: _this.lang.options.help,
                      click: _this.context.createInvokeHandler(
                        "helpDialog.show"
                      )
                    })
                    .render();
                });
              };
              /**
               * image: [
               *   ['imageResize', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
               *   ['float', ['floatLeft', 'floatRight', 'floatNone']],
               *   ['remove', ['removeMedia']],
               * ],
               */
              Buttons.prototype.addImagePopoverButtons = function() {
                var _this = this;
                // Image Size Buttons
                this.context.memo("button.resizeFull", function() {
                  return _this
                    .button({
                      contents: '<span class="note-fontsize-10">100%</span>',
                      tooltip: _this.lang.image.resizeFull,
                      click: _this.context.createInvokeHandler(
                        "editor.resize",
                        "1"
                      )
                    })
                    .render();
                });
                this.context.memo("button.resizeHalf", function() {
                  return _this
                    .button({
                      contents: '<span class="note-fontsize-10">50%</span>',
                      tooltip: _this.lang.image.resizeHalf,
                      click: _this.context.createInvokeHandler(
                        "editor.resize",
                        "0.5"
                      )
                    })
                    .render();
                });
                this.context.memo("button.resizeQuarter", function() {
                  return _this
                    .button({
                      contents: '<span class="note-fontsize-10">25%</span>',
                      tooltip: _this.lang.image.resizeQuarter,
                      click: _this.context.createInvokeHandler(
                        "editor.resize",
                        "0.25"
                      )
                    })
                    .render();
                });
                this.context.memo("button.resizeNone", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.rollback),
                      tooltip: _this.lang.image.resizeNone,
                      click: _this.context.createInvokeHandler(
                        "editor.resize",
                        "0"
                      )
                    })
                    .render();
                });
                // Float Buttons
                this.context.memo("button.floatLeft", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.floatLeft),
                      tooltip: _this.lang.image.floatLeft,
                      click: _this.context.createInvokeHandler(
                        "editor.floatMe",
                        "left"
                      )
                    })
                    .render();
                });
                this.context.memo("button.floatRight", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.floatRight),
                      tooltip: _this.lang.image.floatRight,
                      click: _this.context.createInvokeHandler(
                        "editor.floatMe",
                        "right"
                      )
                    })
                    .render();
                });
                this.context.memo("button.floatNone", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.rollback),
                      tooltip: _this.lang.image.floatNone,
                      click: _this.context.createInvokeHandler(
                        "editor.floatMe",
                        "none"
                      )
                    })
                    .render();
                });
                // Remove Buttons
                this.context.memo("button.removeMedia", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.trash),
                      tooltip: _this.lang.image.remove,
                      click: _this.context.createInvokeHandler(
                        "editor.removeMedia"
                      )
                    })
                    .render();
                });
              };
              Buttons.prototype.addLinkPopoverButtons = function() {
                var _this = this;
                this.context.memo("button.linkDialogShow", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.link),
                      tooltip: _this.lang.link.edit,
                      click: _this.context.createInvokeHandler(
                        "linkDialog.show"
                      )
                    })
                    .render();
                });
                this.context.memo("button.unlink", function() {
                  return _this
                    .button({
                      contents: _this.ui.icon(_this.options.icons.unlink),
                      tooltip: _this.lang.link.unlink,
                      click: _this.context.createInvokeHandler("editor.unlink")
                    })
                    .render();
                });
              };
              /**
               * table : [
               *  ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
               *  ['delete', ['deleteRow', 'deleteCol', 'deleteTable']]
               * ],
               */
              Buttons.prototype.addTablePopoverButtons = function() {
                var _this = this;
                this.context.memo("button.addRowUp", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.rowAbove),
                      tooltip: _this.lang.table.addRowAbove,
                      click: _this.context.createInvokeHandler(
                        "editor.addRow",
                        "top"
                      )
                    })
                    .render();
                });
                this.context.memo("button.addRowDown", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.rowBelow),
                      tooltip: _this.lang.table.addRowBelow,
                      click: _this.context.createInvokeHandler(
                        "editor.addRow",
                        "bottom"
                      )
                    })
                    .render();
                });
                this.context.memo("button.addColLeft", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.colBefore),
                      tooltip: _this.lang.table.addColLeft,
                      click: _this.context.createInvokeHandler(
                        "editor.addCol",
                        "left"
                      )
                    })
                    .render();
                });
                this.context.memo("button.addColRight", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.colAfter),
                      tooltip: _this.lang.table.addColRight,
                      click: _this.context.createInvokeHandler(
                        "editor.addCol",
                        "right"
                      )
                    })
                    .render();
                });
                this.context.memo("button.deleteRow", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.rowRemove),
                      tooltip: _this.lang.table.delRow,
                      click: _this.context.createInvokeHandler(
                        "editor.deleteRow"
                      )
                    })
                    .render();
                });
                this.context.memo("button.deleteCol", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.colRemove),
                      tooltip: _this.lang.table.delCol,
                      click: _this.context.createInvokeHandler(
                        "editor.deleteCol"
                      )
                    })
                    .render();
                });
                this.context.memo("button.deleteTable", function() {
                  return _this
                    .button({
                      className: "btn-md",
                      contents: _this.ui.icon(_this.options.icons.trash),
                      tooltip: _this.lang.table.delTable,
                      click: _this.context.createInvokeHandler(
                        "editor.deleteTable"
                      )
                    })
                    .render();
                });
              };
              Buttons.prototype.build = function($container, groups) {
                for (
                  var groupIdx = 0, groupLen = groups.length;
                  groupIdx < groupLen;
                  groupIdx++
                ) {
                  var group = groups[groupIdx];
                  var groupName = Array.isArray(group) ? group[0] : group;
                  var buttons = Array.isArray(group)
                    ? group.length === 1
                      ? [group[0]]
                      : group[1]
                    : [group];
                  var $group = this.ui
                    .buttonGroup({
                      className: "note-" + groupName
                    })
                    .render();
                  for (var idx = 0, len = buttons.length; idx < len; idx++) {
                    var btn = this.context.memo("button." + buttons[idx]);
                    if (btn) {
                      $group.append(typeof btn === "function" ? btn() : btn);
                    }
                  }
                  $group.appendTo($container);
                }
              };
              /**
               * @param {jQuery} [$container]
               */
              Buttons.prototype.updateCurrentStyle = function($container) {
                var _this = this;
                var $cont = $container || this.$toolbar;
                var styleInfo = this.context.invoke("editor.currentStyle");
                this.updateBtnStates($cont, {
                  ".note-btn-bold": function() {
                    return styleInfo["font-bold"] === "bold";
                  },
                  ".note-btn-italic": function() {
                    return styleInfo["font-italic"] === "italic";
                  },
                  ".note-btn-underline": function() {
                    return styleInfo["font-underline"] === "underline";
                  },
                  ".note-btn-subscript": function() {
                    return styleInfo["font-subscript"] === "subscript";
                  },
                  ".note-btn-superscript": function() {
                    return styleInfo["font-superscript"] === "superscript";
                  },
                  ".note-btn-strikethrough": function() {
                    return styleInfo["font-strikethrough"] === "strikethrough";
                  }
                });
                if (styleInfo["font-family"]) {
                  var fontNames = styleInfo["font-family"]
                    .split(",")
                    .map(function(name) {
                      return name
                        .replace(/[\'\"]/g, "")
                        .replace(/\s+$/, "")
                        .replace(/^\s+/, "");
                    });
                  var fontName_1 = lists.find(
                    fontNames,
                    this.isFontInstalled.bind(this)
                  );
                  $cont.find(".dropdown-fontname a").each(function(idx, item) {
                    var $item = $$1(item);
                    // always compare string to avoid creating another func.
                    var isChecked =
                      $item.data("value") + "" === fontName_1 + "";
                    $item.toggleClass("checked", isChecked);
                  });
                  $cont
                    .find(".note-current-fontname")
                    .text(fontName_1)
                    .css("font-family", fontName_1);
                }
                if (styleInfo["font-size"]) {
                  var fontSize_1 = styleInfo["font-size"];
                  $cont.find(".dropdown-fontsize a").each(function(idx, item) {
                    var $item = $$1(item);
                    // always compare with string to avoid creating another func.
                    var isChecked =
                      $item.data("value") + "" === fontSize_1 + "";
                    $item.toggleClass("checked", isChecked);
                  });
                  $cont.find(".note-current-fontsize").text(fontSize_1);
                }
                if (styleInfo["line-height"]) {
                  var lineHeight_1 = styleInfo["line-height"];
                  $cont
                    .find(".dropdown-line-height li a")
                    .each(function(idx, item) {
                      // always compare with string to avoid creating another func.
                      var isChecked =
                        $$1(item).data("value") + "" === lineHeight_1 + "";
                      _this.className = isChecked ? "checked" : "";
                    });
                }
              };
              Buttons.prototype.updateBtnStates = function($container, infos) {
                var _this = this;
                $$1.each(infos, function(selector, pred) {
                  _this.ui.toggleBtnActive($container.find(selector), pred());
                });
              };
              Buttons.prototype.tableMoveHandler = function(event) {
                var PX_PER_EM = 18;
                var $picker = $$1(event.target.parentNode); // target is mousecatcher
                var $dimensionDisplay = $picker.next();
                var $catcher = $picker.find(
                  ".note-dimension-picker-mousecatcher"
                );
                var $highlighted = $picker.find(
                  ".note-dimension-picker-highlighted"
                );
                var $unhighlighted = $picker.find(
                  ".note-dimension-picker-unhighlighted"
                );
                var posOffset;
                // HTML5 with jQuery - e.offsetX is undefined in Firefox
                if (event.offsetX === undefined) {
                  var posCatcher = $$1(event.target).offset();
                  posOffset = {
                    x: event.pageX - posCatcher.left,
                    y: event.pageY - posCatcher.top
                  };
                } else {
                  posOffset = {
                    x: event.offsetX,
                    y: event.offsetY
                  };
                }
                var dim = {
                  c: Math.ceil(posOffset.x / PX_PER_EM) || 1,
                  r: Math.ceil(posOffset.y / PX_PER_EM) || 1
                };
                $highlighted.css({ width: dim.c + "em", height: dim.r + "em" });
                $catcher.data("value", dim.c + "x" + dim.r);
                if (dim.c > 3 && dim.c < this.options.insertTableMaxSize.col) {
                  $unhighlighted.css({ width: dim.c + 1 + "em" });
                }
                if (dim.r > 3 && dim.r < this.options.insertTableMaxSize.row) {
                  $unhighlighted.css({ height: dim.r + 1 + "em" });
                }
                $dimensionDisplay.html(dim.c + " x " + dim.r);
              };
              return Buttons;
            })();

            var Toolbar = /** @class */ (function() {
              function Toolbar(context) {
                this.context = context;
                this.$window = $$1(window);
                this.$document = $$1(document);
                this.ui = $$1.summernote.ui;
                this.$note = context.layoutInfo.note;
                this.$editor = context.layoutInfo.editor;
                this.$toolbar = context.layoutInfo.toolbar;
                this.$editable = context.layoutInfo.editable;
                this.$statusbar = context.layoutInfo.statusbar;
                this.options = context.options;
                this.isFollowing = false;
                this.followScroll = this.followScroll.bind(this);
              }
              Toolbar.prototype.shouldInitialize = function() {
                return !this.options.airMode;
              };
              Toolbar.prototype.initialize = function() {
                var _this = this;
                this.options.toolbar = this.options.toolbar || [];
                if (!this.options.toolbar.length) {
                  this.$toolbar.hide();
                } else {
                  this.context.invoke(
                    "buttons.build",
                    this.$toolbar,
                    this.options.toolbar
                  );
                }
                if (this.options.toolbarContainer) {
                  this.$toolbar.appendTo(this.options.toolbarContainer);
                }
                this.changeContainer(false);
                this.$note.on(
                  "summernote.keyup summernote.mouseup summernote.change",
                  function() {
                    _this.context.invoke("buttons.updateCurrentStyle");
                  }
                );
                this.context.invoke("buttons.updateCurrentStyle");
                if (this.options.followingToolbar) {
                  this.$window.on("scroll resize", this.followScroll);
                }
              };
              Toolbar.prototype.destroy = function() {
                this.$toolbar.children().remove();
                if (this.options.followingToolbar) {
                  this.$window.off("scroll resize", this.followScroll);
                }
              };
              Toolbar.prototype.followScroll = function() {
                if (this.$editor.hasClass("fullscreen")) {
                  return false;
                }
                var editorHeight = this.$editor.outerHeight();
                var editorWidth = this.$editor.width();
                var toolbarHeight = this.$toolbar.height();
                var statusbarHeight = this.$statusbar.height();
                // check if the web app is currently using another static bar
                var otherBarHeight = 0;
                if (this.options.otherStaticBar) {
                  otherBarHeight = $$1(
                    this.options.otherStaticBar
                  ).outerHeight();
                }
                var currentOffset = this.$document.scrollTop();
                var editorOffsetTop = this.$editor.offset().top;
                var editorOffsetBottom = editorOffsetTop + editorHeight;
                var activateOffset = editorOffsetTop - otherBarHeight;
                var deactivateOffsetBottom =
                  editorOffsetBottom -
                  otherBarHeight -
                  toolbarHeight -
                  statusbarHeight;
                if (
                  !this.isFollowing &&
                  currentOffset > activateOffset &&
                  currentOffset < deactivateOffsetBottom - toolbarHeight
                ) {
                  this.isFollowing = true;
                  this.$toolbar.css({
                    position: "fixed",
                    top: otherBarHeight,
                    width: editorWidth
                  });
                  this.$editable.css({
                    marginTop: this.$toolbar.height() + 5
                  });
                } else if (
                  this.isFollowing &&
                  (currentOffset < activateOffset ||
                    currentOffset > deactivateOffsetBottom)
                ) {
                  this.isFollowing = false;
                  this.$toolbar.css({
                    position: "relative",
                    top: 0,
                    width: "100%"
                  });
                  this.$editable.css({
                    marginTop: ""
                  });
                }
              };
              Toolbar.prototype.changeContainer = function(isFullscreen) {
                if (isFullscreen) {
                  this.$toolbar.prependTo(this.$editor);
                } else {
                  if (this.options.toolbarContainer) {
                    this.$toolbar.appendTo(this.options.toolbarContainer);
                  }
                }
                this.followScroll();
              };
              Toolbar.prototype.updateFullscreen = function(isFullscreen) {
                this.ui.toggleBtnActive(
                  this.$toolbar.find(".btn-fullscreen"),
                  isFullscreen
                );
                this.changeContainer(isFullscreen);
              };
              Toolbar.prototype.updateCodeview = function(isCodeview) {
                this.ui.toggleBtnActive(
                  this.$toolbar.find(".btn-codeview"),
                  isCodeview
                );
                if (isCodeview) {
                  this.deactivate();
                } else {
                  this.activate();
                }
              };
              Toolbar.prototype.activate = function(isIncludeCodeview) {
                var $btn = this.$toolbar.find("button");
                if (!isIncludeCodeview) {
                  $btn = $btn.not(".btn-codeview");
                }
                this.ui.toggleBtn($btn, true);
              };
              Toolbar.prototype.deactivate = function(isIncludeCodeview) {
                var $btn = this.$toolbar.find("button");
                if (!isIncludeCodeview) {
                  $btn = $btn.not(".btn-codeview");
                }
                this.ui.toggleBtn($btn, false);
              };
              return Toolbar;
            })();

            var LinkDialog = /** @class */ (function() {
              function LinkDialog(context) {
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.$body = $$1(document.body);
                this.$editor = context.layoutInfo.editor;
                this.options = context.options;
                this.lang = this.options.langInfo;
                context.memo(
                  "help.linkDialog.show",
                  this.options.langInfo.help["linkDialog.show"]
                );
              }
              LinkDialog.prototype.initialize = function() {
                var $container = this.options.dialogsInBody
                  ? this.$body
                  : this.$editor;
                var body = [
                  '<div class="form-group note-form-group">',
                  '<label class="note-form-label">' +
                    this.lang.link.textToDisplay +
                    "</label>",
                  '<input class="note-link-text form-control note-form-control note-input" type="text" />',
                  "</div>",
                  '<div class="form-group note-form-group">',
                  '<label class="note-form-label">' +
                    this.lang.link.url +
                    "</label>",
                  '<input class="note-link-url form-control note-form-control note-input" type="text" value="http://" />',
                  "</div>",
                  !this.options.disableLinkTarget
                    ? $$1("<div/>")
                        .append(
                          this.ui
                            .checkbox({
                              className: "sn-checkbox-open-in-new-window",
                              text: this.lang.link.openInNewWindow,
                              checked: true
                            })
                            .render()
                        )
                        .html()
                    : ""
                ].join("");
                var buttonClass =
                  "btn btn-primary note-btn note-btn-primary note-link-btn";
                var footer =
                  '<input type="button" href="#" class="' +
                  buttonClass +
                  '" value="' +
                  this.lang.link.insert +
                  '" disabled>';
                this.$dialog = this.ui
                  .dialog({
                    className: "link-dialog",
                    title: this.lang.link.insert,
                    fade: this.options.dialogsFade,
                    body: body,
                    footer: footer
                  })
                  .render()
                  .appendTo($container);
              };
              LinkDialog.prototype.destroy = function() {
                this.ui.hideDialog(this.$dialog);
                this.$dialog.remove();
              };
              LinkDialog.prototype.bindEnterKey = function($input, $btn) {
                $input.on("keypress", function(event) {
                  if (event.keyCode === key.code.ENTER) {
                    event.preventDefault();
                    $btn.trigger("click");
                  }
                });
              };
              /**
               * toggle update button
               */
              LinkDialog.prototype.toggleLinkBtn = function(
                $linkBtn,
                $linkText,
                $linkUrl
              ) {
                this.ui.toggleBtn($linkBtn, $linkText.val() && $linkUrl.val());
              };
              /**
               * Show link dialog and set event handlers on dialog controls.
               *
               * @param {Object} linkInfo
               * @return {Promise}
               */
              LinkDialog.prototype.showLinkDialog = function(linkInfo) {
                var _this = this;
                return $$1
                  .Deferred(function(deferred) {
                    var $linkText = _this.$dialog.find(".note-link-text");
                    var $linkUrl = _this.$dialog.find(".note-link-url");
                    var $linkBtn = _this.$dialog.find(".note-link-btn");
                    var $openInNewWindow = _this.$dialog.find(
                      ".sn-checkbox-open-in-new-window input[type=checkbox]"
                    );
                    _this.ui.onDialogShown(_this.$dialog, function() {
                      _this.context.triggerEvent("dialog.shown");
                      // If no url was given and given text is valid URL then copy that into URL Field
                      if (!linkInfo.url && func.isValidUrl(linkInfo.text)) {
                        linkInfo.url = linkInfo.text;
                      }
                      $linkText
                        .on("input paste propertychange", function() {
                          // If linktext was modified by input events,
                          // cloning text from linkUrl will be stopped.
                          linkInfo.text = $linkText.val();
                          _this.toggleLinkBtn($linkBtn, $linkText, $linkUrl);
                        })
                        .val(linkInfo.text);
                      $linkUrl
                        .on("input paste propertychange", function() {
                          // Display same text on `Text to display` as default
                          // when linktext has no text
                          if (!linkInfo.text) {
                            $linkText.val($linkUrl.val());
                          }
                          _this.toggleLinkBtn($linkBtn, $linkText, $linkUrl);
                        })
                        .val(linkInfo.url);
                      if (!env.isSupportTouch) {
                        $linkUrl.trigger("focus");
                      }
                      _this.toggleLinkBtn($linkBtn, $linkText, $linkUrl);
                      _this.bindEnterKey($linkUrl, $linkBtn);
                      _this.bindEnterKey($linkText, $linkBtn);
                      var isNewWindowChecked =
                        linkInfo.isNewWindow !== undefined
                          ? linkInfo.isNewWindow
                          : _this.context.options.linkTargetBlank;
                      $openInNewWindow.prop("checked", isNewWindowChecked);
                      $linkBtn.one("click", function(event) {
                        event.preventDefault();
                        deferred.resolve({
                          range: linkInfo.range,
                          url: $linkUrl.val(),
                          text: $linkText.val(),
                          isNewWindow: $openInNewWindow.is(":checked")
                        });
                        _this.ui.hideDialog(_this.$dialog);
                      });
                    });
                    _this.ui.onDialogHidden(_this.$dialog, function() {
                      // detach events
                      $linkText.off();
                      $linkUrl.off();
                      $linkBtn.off();
                      if (deferred.state() === "pending") {
                        deferred.reject();
                      }
                    });
                    _this.ui.showDialog(_this.$dialog);
                  })
                  .promise();
              };
              /**
               * @param {Object} layoutInfo
               */
              LinkDialog.prototype.show = function() {
                var _this = this;
                var linkInfo = this.context.invoke("editor.getLinkInfo");
                this.context.invoke("editor.saveRange");
                this.showLinkDialog(linkInfo)
                  .then(function(linkInfo) {
                    _this.context.invoke("editor.restoreRange");
                    _this.context.invoke("editor.createLink", linkInfo);
                  })
                  .fail(function() {
                    _this.context.invoke("editor.restoreRange");
                  });
              };
              return LinkDialog;
            })();

            var LinkPopover = /** @class */ (function() {
              function LinkPopover(context) {
                var _this = this;
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.options = context.options;
                this.events = {
                  "summernote.keyup summernote.mouseup summernote.change summernote.scroll": function() {
                    _this.update();
                  },
                  "summernote.disable summernote.dialog.shown": function() {
                    _this.hide();
                  }
                };
              }
              LinkPopover.prototype.shouldInitialize = function() {
                return !lists.isEmpty(this.options.popover.link);
              };
              LinkPopover.prototype.initialize = function() {
                this.$popover = this.ui
                  .popover({
                    className: "note-link-popover",
                    callback: function($node) {
                      var $content = $node.find(
                        ".popover-content,.note-popover-content"
                      );
                      $content.prepend(
                        '<span><a target="_blank"></a>&nbsp;</span>'
                      );
                    }
                  })
                  .render()
                  .appendTo(this.options.container);
                var $content = this.$popover.find(
                  ".popover-content,.note-popover-content"
                );
                this.context.invoke(
                  "buttons.build",
                  $content,
                  this.options.popover.link
                );
              };
              LinkPopover.prototype.destroy = function() {
                this.$popover.remove();
              };
              LinkPopover.prototype.update = function() {
                // Prevent focusing on editable when invoke('code') is executed
                if (!this.context.invoke("editor.hasFocus")) {
                  this.hide();
                  return;
                }
                var rng = this.context.invoke("editor.getLastRange");
                if (rng.isCollapsed() && rng.isOnAnchor()) {
                  var anchor = dom.ancestor(rng.sc, dom.isAnchor);
                  var href = $$1(anchor).attr("href");
                  this.$popover
                    .find("a")
                    .attr("href", href)
                    .html(href);
                  var pos = dom.posFromPlaceholder(anchor);
                  this.$popover.css({
                    display: "block",
                    left: pos.left,
                    top: pos.top
                  });
                } else {
                  this.hide();
                }
              };
              LinkPopover.prototype.hide = function() {
                this.$popover.hide();
              };
              return LinkPopover;
            })();

            var ImageDialog = /** @class */ (function() {
              function ImageDialog(context) {
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.$body = $$1(document.body);
                this.$editor = context.layoutInfo.editor;
                this.options = context.options;
                this.lang = this.options.langInfo;
              }
              ImageDialog.prototype.initialize = function() {
                var $container = this.options.dialogsInBody
                  ? this.$body
                  : this.$editor;
                var imageLimitation = "";
                if (this.options.maximumImageFileSize) {
                  var unit = Math.floor(
                    Math.log(this.options.maximumImageFileSize) / Math.log(1024)
                  );
                  var readableSize =
                    (
                      this.options.maximumImageFileSize / Math.pow(1024, unit)
                    ).toFixed(2) *
                      1 +
                    " " +
                    " KMGTP"[unit] +
                    "B";
                  imageLimitation =
                    "<small>" +
                    (this.lang.image.maximumFileSize + " : " + readableSize) +
                    "</small>";
                }
                var body = [
                  '<div class="form-group note-form-group note-group-select-from-files">',
                  '<label class="note-form-label">' +
                    this.lang.image.selectFromFiles +
                    "</label>",
                  '<input class="note-image-input form-control-file note-form-control note-input" ',
                  ' type="file" name="files" accept="image/*" multiple="multiple" />',
                  imageLimitation,
                  "</div>",
                  '<div class="form-group note-group-image-url" style="overflow:auto;">',
                  '<label class="note-form-label">' +
                    this.lang.image.url +
                    "</label>",
                  '<input class="note-image-url form-control note-form-control note-input ',
                  ' col-md-12" type="text" />',
                  "</div>"
                ].join("");
                var buttonClass =
                  "btn btn-primary note-btn note-btn-primary note-image-btn";
                var footer =
                  '<input type="button" href="#" class="' +
                  buttonClass +
                  '" value="' +
                  this.lang.image.insert +
                  '" disabled>';
                this.$dialog = this.ui
                  .dialog({
                    title: this.lang.image.insert,
                    fade: this.options.dialogsFade,
                    body: body,
                    footer: footer
                  })
                  .render()
                  .appendTo($container);
              };
              ImageDialog.prototype.destroy = function() {
                this.ui.hideDialog(this.$dialog);
                this.$dialog.remove();
              };
              ImageDialog.prototype.bindEnterKey = function($input, $btn) {
                $input.on("keypress", function(event) {
                  if (event.keyCode === key.code.ENTER) {
                    event.preventDefault();
                    $btn.trigger("click");
                  }
                });
              };
              ImageDialog.prototype.show = function() {
                var _this = this;
                this.context.invoke("editor.saveRange");
                this.showImageDialog()
                  .then(function(data) {
                    // [workaround] hide dialog before restore range for IE range focus
                    _this.ui.hideDialog(_this.$dialog);
                    _this.context.invoke("editor.restoreRange");
                    if (typeof data === "string") {
                      // image url
                      // If onImageLinkInsert set,
                      if (_this.options.callbacks.onImageLinkInsert) {
                        _this.context.triggerEvent("image.link.insert", data);
                      } else {
                        _this.context.invoke("editor.insertImage", data);
                      }
                    } else {
                      // array of files
                      _this.context.invoke(
                        "editor.insertImagesOrCallback",
                        data
                      );
                    }
                  })
                  .fail(function() {
                    _this.context.invoke("editor.restoreRange");
                  });
              };
              /**
               * show image dialog
               *
               * @param {jQuery} $dialog
               * @return {Promise}
               */
              ImageDialog.prototype.showImageDialog = function() {
                var _this = this;
                return $$1.Deferred(function(deferred) {
                  var $imageInput = _this.$dialog.find(".note-image-input");
                  var $imageUrl = _this.$dialog.find(".note-image-url");
                  var $imageBtn = _this.$dialog.find(".note-image-btn");
                  _this.ui.onDialogShown(_this.$dialog, function() {
                    _this.context.triggerEvent("dialog.shown");
                    // Cloning imageInput to clear element.
                    $imageInput.replaceWith(
                      $imageInput
                        .clone()
                        .on("change", function(event) {
                          deferred.resolve(
                            event.target.files || event.target.value
                          );
                        })
                        .val("")
                    );
                    $imageUrl
                      .on("input paste propertychange", function() {
                        _this.ui.toggleBtn($imageBtn, $imageUrl.val());
                      })
                      .val("");
                    if (!env.isSupportTouch) {
                      $imageUrl.trigger("focus");
                    }
                    $imageBtn.click(function(event) {
                      event.preventDefault();
                      deferred.resolve($imageUrl.val());
                    });
                    _this.bindEnterKey($imageUrl, $imageBtn);
                  });
                  _this.ui.onDialogHidden(_this.$dialog, function() {
                    $imageInput.off();
                    $imageUrl.off();
                    $imageBtn.off();
                    if (deferred.state() === "pending") {
                      deferred.reject();
                    }
                  });
                  _this.ui.showDialog(_this.$dialog);
                });
              };
              return ImageDialog;
            })();

            /**
             * Image popover module
             *  mouse events that show/hide popover will be handled by Handle.js.
             *  Handle.js will receive the events and invoke 'imagePopover.update'.
             */
            var ImagePopover = /** @class */ (function() {
              function ImagePopover(context) {
                var _this = this;
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.editable = context.layoutInfo.editable[0];
                this.options = context.options;
                this.events = {
                  "summernote.disable": function() {
                    _this.hide();
                  }
                };
              }
              ImagePopover.prototype.shouldInitialize = function() {
                return !lists.isEmpty(this.options.popover.image);
              };
              ImagePopover.prototype.initialize = function() {
                this.$popover = this.ui
                  .popover({
                    className: "note-image-popover"
                  })
                  .render()
                  .appendTo(this.options.container);
                var $content = this.$popover.find(
                  ".popover-content,.note-popover-content"
                );
                this.context.invoke(
                  "buttons.build",
                  $content,
                  this.options.popover.image
                );
              };
              ImagePopover.prototype.destroy = function() {
                this.$popover.remove();
              };
              ImagePopover.prototype.update = function(target, event) {
                if (dom.isImg(target)) {
                  var pos = dom.posFromPlaceholder(target);
                  var posEditor = dom.posFromPlaceholder(this.editable);
                  this.$popover.css({
                    display: "block",
                    left: this.options.popatmouse ? event.pageX - 20 : pos.left,
                    top: this.options.popatmouse
                      ? event.pageY
                      : Math.min(pos.top, posEditor.top)
                  });
                } else {
                  this.hide();
                }
              };
              ImagePopover.prototype.hide = function() {
                this.$popover.hide();
              };
              return ImagePopover;
            })();

            var TablePopover = /** @class */ (function() {
              function TablePopover(context) {
                var _this = this;
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.options = context.options;
                this.events = {
                  "summernote.mousedown": function(we, e) {
                    _this.update(e.target);
                  },
                  "summernote.keyup summernote.scroll summernote.change": function() {
                    _this.update();
                  },
                  "summernote.disable": function() {
                    _this.hide();
                  }
                };
              }
              TablePopover.prototype.shouldInitialize = function() {
                return !lists.isEmpty(this.options.popover.table);
              };
              TablePopover.prototype.initialize = function() {
                this.$popover = this.ui
                  .popover({
                    className: "note-table-popover"
                  })
                  .render()
                  .appendTo(this.options.container);
                var $content = this.$popover.find(
                  ".popover-content,.note-popover-content"
                );
                this.context.invoke(
                  "buttons.build",
                  $content,
                  this.options.popover.table
                );
                // [workaround] Disable Firefox's default table editor
                if (env.isFF) {
                  document.execCommand(
                    "enableInlineTableEditing",
                    false,
                    false
                  );
                }
              };
              TablePopover.prototype.destroy = function() {
                this.$popover.remove();
              };
              TablePopover.prototype.update = function(target) {
                if (this.context.isDisabled()) {
                  return false;
                }
                var isCell = dom.isCell(target);
                if (isCell) {
                  var pos = dom.posFromPlaceholder(target);
                  this.$popover.css({
                    display: "block",
                    left: pos.left,
                    top: pos.top
                  });
                } else {
                  this.hide();
                }
                return isCell;
              };
              TablePopover.prototype.hide = function() {
                this.$popover.hide();
              };
              return TablePopover;
            })();

            var VideoDialog = /** @class */ (function() {
              function VideoDialog(context) {
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.$body = $$1(document.body);
                this.$editor = context.layoutInfo.editor;
                this.options = context.options;
                this.lang = this.options.langInfo;
              }
              VideoDialog.prototype.initialize = function() {
                var $container = this.options.dialogsInBody
                  ? this.$body
                  : this.$editor;
                var body = [
                  '<div class="form-group note-form-group row-fluid">',
                  '<label class="note-form-label">' +
                    this.lang.video.url +
                    ' <small class="text-muted">' +
                    this.lang.video.providers +
                    "</small></label>",
                  '<input class="note-video-url form-control note-form-control note-input" type="text" />',
                  "</div>"
                ].join("");
                var buttonClass =
                  "btn btn-primary note-btn note-btn-primary note-video-btn";
                var footer =
                  '<input type="button" href="#" class="' +
                  buttonClass +
                  '" value="' +
                  this.lang.video.insert +
                  '" disabled>';
                this.$dialog = this.ui
                  .dialog({
                    title: this.lang.video.insert,
                    fade: this.options.dialogsFade,
                    body: body,
                    footer: footer
                  })
                  .render()
                  .appendTo($container);
              };
              VideoDialog.prototype.destroy = function() {
                this.ui.hideDialog(this.$dialog);
                this.$dialog.remove();
              };
              VideoDialog.prototype.bindEnterKey = function($input, $btn) {
                $input.on("keypress", function(event) {
                  if (event.keyCode === key.code.ENTER) {
                    event.preventDefault();
                    $btn.trigger("click");
                  }
                });
              };
              VideoDialog.prototype.createVideoNode = function(url) {
                // video url patterns(youtube, instagram, vimeo, dailymotion, youku, mp4, ogg, webm)
                var ytRegExp = /\/\/(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))([\w|-]{11})(?:(?:[\?&]t=)(\S+))?$/;
                var ytRegExpForStart = /^(?:(\d+)h)?(?:(\d+)m)?(?:(\d+)s)?$/;
                var ytMatch = url.match(ytRegExp);
                var igRegExp = /(?:www\.|\/\/)instagram\.com\/p\/(.[a-zA-Z0-9_-]*)/;
                var igMatch = url.match(igRegExp);
                var vRegExp = /\/\/vine\.co\/v\/([a-zA-Z0-9]+)/;
                var vMatch = url.match(vRegExp);
                var vimRegExp = /\/\/(player\.)?vimeo\.com\/([a-z]*\/)*(\d+)[?]?.*/;
                var vimMatch = url.match(vimRegExp);
                var dmRegExp = /.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/;
                var dmMatch = url.match(dmRegExp);
                var youkuRegExp = /\/\/v\.youku\.com\/v_show\/id_(\w+)=*\.html/;
                var youkuMatch = url.match(youkuRegExp);
                var qqRegExp = /\/\/v\.qq\.com.*?vid=(.+)/;
                var qqMatch = url.match(qqRegExp);
                var qqRegExp2 = /\/\/v\.qq\.com\/x?\/?(page|cover).*?\/([^\/]+)\.html\??.*/;
                var qqMatch2 = url.match(qqRegExp2);
                var mp4RegExp = /^.+.(mp4|m4v)$/;
                var mp4Match = url.match(mp4RegExp);
                var oggRegExp = /^.+.(ogg|ogv)$/;
                var oggMatch = url.match(oggRegExp);
                var webmRegExp = /^.+.(webm)$/;
                var webmMatch = url.match(webmRegExp);
                var fbRegExp = /(?:www\.|\/\/)facebook\.com\/([^\/]+)\/videos\/([0-9]+)/;
                var fbMatch = url.match(fbRegExp);
                var $video;
                if (ytMatch && ytMatch[1].length === 11) {
                  var youtubeId = ytMatch[1];
                  var start = 0;
                  if (typeof ytMatch[2] !== "undefined") {
                    var ytMatchForStart = ytMatch[2].match(ytRegExpForStart);
                    if (ytMatchForStart) {
                      for (
                        var n = [3600, 60, 1], i = 0, r = n.length;
                        i < r;
                        i++
                      ) {
                        start +=
                          typeof ytMatchForStart[i + 1] !== "undefined"
                            ? n[i] * parseInt(ytMatchForStart[i + 1], 10)
                            : 0;
                      }
                    }
                  }
                  $video = $$1("<iframe>")
                    .attr("frameborder", 0)
                    .attr(
                      "src",
                      "//www.youtube.com/embed/" +
                        youtubeId +
                        (start > 0 ? "?start=" + start : "")
                    )
                    .attr("width", "640")
                    .attr("height", "360");
                } else if (igMatch && igMatch[0].length) {
                  $video = $$1("<iframe>")
                    .attr("frameborder", 0)
                    .attr(
                      "src",
                      "https://instagram.com/p/" + igMatch[1] + "/embed/"
                    )
                    .attr("width", "612")
                    .attr("height", "710")
                    .attr("scrolling", "no")
                    .attr("allowtransparency", "true");
                } else if (vMatch && vMatch[0].length) {
                  $video = $$1("<iframe>")
                    .attr("frameborder", 0)
                    .attr("src", vMatch[0] + "/embed/simple")
                    .attr("width", "600")
                    .attr("height", "600")
                    .attr("class", "vine-embed");
                } else if (vimMatch && vimMatch[3].length) {
                  $video = $$1(
                    "<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>"
                  )
                    .attr("frameborder", 0)
                    .attr("src", "//player.vimeo.com/video/" + vimMatch[3])
                    .attr("width", "640")
                    .attr("height", "360");
                } else if (dmMatch && dmMatch[2].length) {
                  $video = $$1("<iframe>")
                    .attr("frameborder", 0)
                    .attr(
                      "src",
                      "//www.dailymotion.com/embed/video/" + dmMatch[2]
                    )
                    .attr("width", "640")
                    .attr("height", "360");
                } else if (youkuMatch && youkuMatch[1].length) {
                  $video = $$1(
                    "<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>"
                  )
                    .attr("frameborder", 0)
                    .attr("height", "498")
                    .attr("width", "510")
                    .attr("src", "//player.youku.com/embed/" + youkuMatch[1]);
                } else if (
                  (qqMatch && qqMatch[1].length) ||
                  (qqMatch2 && qqMatch2[2].length)
                ) {
                  var vid =
                    qqMatch && qqMatch[1].length ? qqMatch[1] : qqMatch2[2];
                  $video = $$1(
                    "<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>"
                  )
                    .attr("frameborder", 0)
                    .attr("height", "310")
                    .attr("width", "500")
                    .attr(
                      "src",
                      "http://v.qq.com/iframe/player.html?vid=" +
                        vid +
                        "&amp;auto=0"
                    );
                } else if (mp4Match || oggMatch || webmMatch) {
                  $video = $$1("<video controls>")
                    .attr("src", url)
                    .attr("width", "640")
                    .attr("height", "360");
                } else if (fbMatch && fbMatch[0].length) {
                  $video = $$1("<iframe>")
                    .attr("frameborder", 0)
                    .attr(
                      "src",
                      "https://www.facebook.com/plugins/video.php?href=" +
                        encodeURIComponent(fbMatch[0]) +
                        "&show_text=0&width=560"
                    )
                    .attr("width", "560")
                    .attr("height", "301")
                    .attr("scrolling", "no")
                    .attr("allowtransparency", "true");
                } else {
                  // this is not a known video link. Now what, Cat? Now what?
                  return false;
                }
                $video.addClass("note-video-clip");
                return $video[0];
              };
              VideoDialog.prototype.show = function() {
                var _this = this;
                var text = this.context.invoke("editor.getSelectedText");
                this.context.invoke("editor.saveRange");
                this.showVideoDialog(text)
                  .then(function(url) {
                    // [workaround] hide dialog before restore range for IE range focus
                    _this.ui.hideDialog(_this.$dialog);
                    _this.context.invoke("editor.restoreRange");
                    // build node
                    var $node = _this.createVideoNode(url);
                    if ($node) {
                      // insert video node
                      _this.context.invoke("editor.insertNode", $node);
                    }
                  })
                  .fail(function() {
                    _this.context.invoke("editor.restoreRange");
                  });
              };
              /**
               * show image dialog
               *
               * @param {jQuery} $dialog
               * @return {Promise}
               */
              VideoDialog.prototype.showVideoDialog = function(text) {
                var _this = this;
                return $$1.Deferred(function(deferred) {
                  var $videoUrl = _this.$dialog.find(".note-video-url");
                  var $videoBtn = _this.$dialog.find(".note-video-btn");
                  _this.ui.onDialogShown(_this.$dialog, function() {
                    _this.context.triggerEvent("dialog.shown");
                    $videoUrl.on("input paste propertychange", function() {
                      _this.ui.toggleBtn($videoBtn, $videoUrl.val());
                    });
                    if (!env.isSupportTouch) {
                      $videoUrl.trigger("focus");
                    }
                    $videoBtn.click(function(event) {
                      event.preventDefault();
                      deferred.resolve($videoUrl.val());
                    });
                    _this.bindEnterKey($videoUrl, $videoBtn);
                  });
                  _this.ui.onDialogHidden(_this.$dialog, function() {
                    $videoUrl.off();
                    $videoBtn.off();
                    if (deferred.state() === "pending") {
                      deferred.reject();
                    }
                  });
                  _this.ui.showDialog(_this.$dialog);
                });
              };
              return VideoDialog;
            })();

            var HelpDialog = /** @class */ (function() {
              function HelpDialog(context) {
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.$body = $$1(document.body);
                this.$editor = context.layoutInfo.editor;
                this.options = context.options;
                this.lang = this.options.langInfo;
              }
              HelpDialog.prototype.initialize = function() {
                var $container = this.options.dialogsInBody
                  ? this.$body
                  : this.$editor;
                var body = [
                  '<p class="text-center">',
                  '<a href="http://summernote.org/" target="_blank">Summernote 0.8.12</a> · ',
                  '<a href="https://github.com/summernote/summernote" target="_blank">Project</a> · ',
                  '<a href="https://github.com/summernote/summernote/issues" target="_blank">Issues</a>',
                  "</p>"
                ].join("");
                this.$dialog = this.ui
                  .dialog({
                    title: this.lang.options.help,
                    fade: this.options.dialogsFade,
                    body: this.createShortcutList(),
                    footer: body,
                    callback: function($node) {
                      $node.find(".modal-body,.note-modal-body").css({
                        "max-height": 300,
                        overflow: "scroll"
                      });
                    }
                  })
                  .render()
                  .appendTo($container);
              };
              HelpDialog.prototype.destroy = function() {
                this.ui.hideDialog(this.$dialog);
                this.$dialog.remove();
              };
              HelpDialog.prototype.createShortcutList = function() {
                var _this = this;
                var keyMap = this.options.keyMap[env.isMac ? "mac" : "pc"];
                return Object.keys(keyMap)
                  .map(function(key) {
                    var command = keyMap[key];
                    var $row = $$1('<div><div class="help-list-item"/></div>');
                    $row
                      .append(
                        $$1("<label><kbd>" + key + "</kdb></label>").css({
                          width: 180,
                          "margin-right": 10
                        })
                      )
                      .append(
                        $$1("<span/>").html(
                          _this.context.memo("help." + command) || command
                        )
                      );
                    return $row.html();
                  })
                  .join("");
              };
              /**
               * show help dialog
               *
               * @return {Promise}
               */
              HelpDialog.prototype.showHelpDialog = function() {
                var _this = this;
                return $$1
                  .Deferred(function(deferred) {
                    _this.ui.onDialogShown(_this.$dialog, function() {
                      _this.context.triggerEvent("dialog.shown");
                      deferred.resolve();
                    });
                    _this.ui.showDialog(_this.$dialog);
                  })
                  .promise();
              };
              HelpDialog.prototype.show = function() {
                var _this = this;
                this.context.invoke("editor.saveRange");
                this.showHelpDialog().then(function() {
                  _this.context.invoke("editor.restoreRange");
                });
              };
              return HelpDialog;
            })();

            var AIR_MODE_POPOVER_X_OFFSET = 20;
            var AirPopover = /** @class */ (function() {
              function AirPopover(context) {
                var _this = this;
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.options = context.options;
                this.events = {
                  "summernote.keyup summernote.mouseup summernote.scroll": function() {
                    _this.update();
                  },
                  "summernote.disable summernote.change summernote.dialog.shown": function() {
                    _this.hide();
                  },
                  "summernote.focusout": function(we, e) {
                    // [workaround] Firefox doesn't support relatedTarget on focusout
                    //  - Ignore hide action on focus out in FF.
                    if (env.isFF) {
                      return;
                    }
                    if (
                      !e.relatedTarget ||
                      !dom.ancestor(e.relatedTarget, func.eq(_this.$popover[0]))
                    ) {
                      _this.hide();
                    }
                  }
                };
              }
              AirPopover.prototype.shouldInitialize = function() {
                return (
                  this.options.airMode &&
                  !lists.isEmpty(this.options.popover.air)
                );
              };
              AirPopover.prototype.initialize = function() {
                this.$popover = this.ui
                  .popover({
                    className: "note-air-popover"
                  })
                  .render()
                  .appendTo(this.options.container);
                var $content = this.$popover.find(".popover-content");
                this.context.invoke(
                  "buttons.build",
                  $content,
                  this.options.popover.air
                );
              };
              AirPopover.prototype.destroy = function() {
                this.$popover.remove();
              };
              AirPopover.prototype.update = function() {
                var styleInfo = this.context.invoke("editor.currentStyle");
                if (styleInfo.range && !styleInfo.range.isCollapsed()) {
                  var rect = lists.last(styleInfo.range.getClientRects());
                  if (rect) {
                    var bnd = func.rect2bnd(rect);
                    this.$popover.css({
                      display: "block",
                      left:
                        Math.max(bnd.left + bnd.width / 2, 0) -
                        AIR_MODE_POPOVER_X_OFFSET,
                      top: bnd.top + bnd.height
                    });
                    this.context.invoke(
                      "buttons.updateCurrentStyle",
                      this.$popover
                    );
                  }
                } else {
                  this.hide();
                }
              };
              AirPopover.prototype.hide = function() {
                this.$popover.hide();
              };
              return AirPopover;
            })();

            var POPOVER_DIST = 5;
            var HintPopover = /** @class */ (function() {
              function HintPopover(context) {
                var _this = this;
                this.context = context;
                this.ui = $$1.summernote.ui;
                this.$editable = context.layoutInfo.editable;
                this.options = context.options;
                this.hint = this.options.hint || [];
                this.direction = this.options.hintDirection || "bottom";
                this.hints = Array.isArray(this.hint) ? this.hint : [this.hint];
                this.events = {
                  "summernote.keyup": function(we, e) {
                    if (!e.isDefaultPrevented()) {
                      _this.handleKeyup(e);
                    }
                  },
                  "summernote.keydown": function(we, e) {
                    _this.handleKeydown(e);
                  },
                  "summernote.disable summernote.dialog.shown": function() {
                    _this.hide();
                  }
                };
              }
              HintPopover.prototype.shouldInitialize = function() {
                return this.hints.length > 0;
              };
              HintPopover.prototype.initialize = function() {
                var _this = this;
                this.lastWordRange = null;
                this.$popover = this.ui
                  .popover({
                    className: "note-hint-popover",
                    hideArrow: true,
                    direction: ""
                  })
                  .render()
                  .appendTo(this.options.container);
                this.$popover.hide();
                this.$content = this.$popover.find(
                  ".popover-content,.note-popover-content"
                );
                this.$content.on("click", ".note-hint-item", function(e) {
                  _this.$content.find(".active").removeClass("active");
                  $$1(e.currentTarget).addClass("active");
                  _this.replace();
                });
              };
              HintPopover.prototype.destroy = function() {
                this.$popover.remove();
              };
              HintPopover.prototype.selectItem = function($item) {
                this.$content.find(".active").removeClass("active");
                $item.addClass("active");
                this.$content[0].scrollTop =
                  $item[0].offsetTop - this.$content.innerHeight() / 2;
              };
              HintPopover.prototype.moveDown = function() {
                var $current = this.$content.find(".note-hint-item.active");
                var $next = $current.next();
                if ($next.length) {
                  this.selectItem($next);
                } else {
                  var $nextGroup = $current.parent().next();
                  if (!$nextGroup.length) {
                    $nextGroup = this.$content.find(".note-hint-group").first();
                  }
                  this.selectItem($nextGroup.find(".note-hint-item").first());
                }
              };
              HintPopover.prototype.moveUp = function() {
                var $current = this.$content.find(".note-hint-item.active");
                var $prev = $current.prev();
                if ($prev.length) {
                  this.selectItem($prev);
                } else {
                  var $prevGroup = $current.parent().prev();
                  if (!$prevGroup.length) {
                    $prevGroup = this.$content.find(".note-hint-group").last();
                  }
                  this.selectItem($prevGroup.find(".note-hint-item").last());
                }
              };
              HintPopover.prototype.replace = function() {
                var $item = this.$content.find(".note-hint-item.active");
                if ($item.length) {
                  var node = this.nodeFromItem($item);
                  // XXX: consider to move codes to editor for recording redo/undo.
                  this.lastWordRange.insertNode(node);
                  range
                    .createFromNode(node)
                    .collapse()
                    .select();
                  this.lastWordRange = null;
                  this.hide();
                  this.context.triggerEvent(
                    "change",
                    this.$editable.html(),
                    this.$editable[0]
                  );
                  this.context.invoke("editor.focus");
                }
              };
              HintPopover.prototype.nodeFromItem = function($item) {
                var hint = this.hints[$item.data("index")];
                var item = $item.data("item");
                var node = hint.content ? hint.content(item) : item;
                if (typeof node === "string") {
                  node = dom.createText(node);
                }
                return node;
              };
              HintPopover.prototype.createItemTemplates = function(
                hintIdx,
                items
              ) {
                var hint = this.hints[hintIdx];
                return items.map(function(item, idx) {
                  var $item = $$1('<div class="note-hint-item"/>');
                  $item.append(hint.template ? hint.template(item) : item + "");
                  $item.data({
                    index: hintIdx,
                    item: item
                  });
                  return $item;
                });
              };
              HintPopover.prototype.handleKeydown = function(e) {
                if (!this.$popover.is(":visible")) {
                  return;
                }
                if (e.keyCode === key.code.ENTER) {
                  e.preventDefault();
                  this.replace();
                } else if (e.keyCode === key.code.UP) {
                  e.preventDefault();
                  this.moveUp();
                } else if (e.keyCode === key.code.DOWN) {
                  e.preventDefault();
                  this.moveDown();
                }
              };
              HintPopover.prototype.searchKeyword = function(
                index,
                keyword,
                callback
              ) {
                var hint = this.hints[index];
                if (hint && hint.match.test(keyword) && hint.search) {
                  var matches = hint.match.exec(keyword);
                  hint.search(matches[1], callback);
                } else {
                  callback();
                }
              };
              HintPopover.prototype.createGroup = function(idx, keyword) {
                var _this = this;
                var $group = $$1(
                  '<div class="note-hint-group note-hint-group-' + idx + '"/>'
                );
                this.searchKeyword(idx, keyword, function(items) {
                  items = items || [];
                  if (items.length) {
                    $group.html(_this.createItemTemplates(idx, items));
                    _this.show();
                  }
                });
                return $group;
              };
              HintPopover.prototype.handleKeyup = function(e) {
                var _this = this;
                if (
                  !lists.contains(
                    [key.code.ENTER, key.code.UP, key.code.DOWN],
                    e.keyCode
                  )
                ) {
                  var wordRange = this.context
                    .invoke("editor.getLastRange")
                    .getWordRange();
                  var keyword_1 = wordRange.toString();
                  if (this.hints.length && keyword_1) {
                    this.$content.empty();
                    var bnd = func.rect2bnd(
                      lists.last(wordRange.getClientRects())
                    );
                    if (bnd) {
                      this.$popover.hide();
                      this.lastWordRange = wordRange;
                      this.hints.forEach(function(hint, idx) {
                        if (hint.match.test(keyword_1)) {
                          _this
                            .createGroup(idx, keyword_1)
                            .appendTo(_this.$content);
                        }
                      });
                      // select first .note-hint-item
                      this.$content
                        .find(".note-hint-item:first")
                        .addClass("active");
                      // set position for popover after group is created
                      if (this.direction === "top") {
                        this.$popover.css({
                          left: bnd.left,
                          top:
                            bnd.top - this.$popover.outerHeight() - POPOVER_DIST
                        });
                      } else {
                        this.$popover.css({
                          left: bnd.left,
                          top: bnd.top + bnd.height + POPOVER_DIST
                        });
                      }
                    }
                  } else {
                    this.hide();
                  }
                }
              };
              HintPopover.prototype.show = function() {
                this.$popover.show();
              };
              HintPopover.prototype.hide = function() {
                this.$popover.hide();
              };
              return HintPopover;
            })();

            $$1.summernote = $$1.extend($$1.summernote, {
              version: "0.8.12",
              plugins: {},
              dom: dom,
              range: range,
              options: {
                langInfo: $$1.summernote.lang["en-US"],
                modules: {
                  editor: Editor,
                  clipboard: Clipboard,
                  dropzone: Dropzone,
                  codeview: CodeView,
                  statusbar: Statusbar,
                  fullscreen: Fullscreen,
                  handle: Handle,
                  // FIXME: HintPopover must be front of autolink
                  //  - Script error about range when Enter key is pressed on hint popover
                  hintPopover: HintPopover,
                  autoLink: AutoLink,
                  autoSync: AutoSync,
                  autoReplace: AutoReplace,
                  placeholder: Placeholder,
                  buttons: Buttons,
                  toolbar: Toolbar,
                  linkDialog: LinkDialog,
                  linkPopover: LinkPopover,
                  imageDialog: ImageDialog,
                  imagePopover: ImagePopover,
                  tablePopover: TablePopover,
                  videoDialog: VideoDialog,
                  helpDialog: HelpDialog,
                  airPopover: AirPopover
                },
                buttons: {},
                lang: "en-US",
                followingToolbar: false,
                otherStaticBar: "",
                // toolbar
                toolbar: [
                  ["style", ["style"]],
                  ["font", ["bold", "underline", "clear"]],
                  ["fontname", ["fontname"]],
                  ["color", ["color"]],
                  ["para", ["ul", "ol", "paragraph"]],
                  ["table", ["table"]],
                  ["insert", ["link", "picture", "video"]],
                  ["view", ["fullscreen", "codeview", "help"]]
                ],
                // popover
                popatmouse: true,
                popover: {
                  image: [
                    [
                      "resize",
                      [
                        "resizeFull",
                        "resizeHalf",
                        "resizeQuarter",
                        "resizeNone"
                      ]
                    ],
                    ["float", ["floatLeft", "floatRight", "floatNone"]],
                    ["remove", ["removeMedia"]]
                  ],
                  link: [["link", ["linkDialogShow", "unlink"]]],
                  table: [
                    [
                      "add",
                      ["addRowDown", "addRowUp", "addColLeft", "addColRight"]
                    ],
                    ["delete", ["deleteRow", "deleteCol", "deleteTable"]]
                  ],
                  air: [
                    ["color", ["color"]],
                    ["font", ["bold", "underline", "clear"]],
                    ["para", ["ul", "paragraph"]],
                    ["table", ["table"]],
                    ["insert", ["link", "picture"]]
                  ]
                },
                // air mode: inline editor
                airMode: false,
                width: null,
                height: null,
                linkTargetBlank: true,
                focus: false,
                tabSize: 4,
                styleWithSpan: true,
                shortcuts: true,
                textareaAutoSync: true,
                hintDirection: "bottom",
                tooltip: "auto",
                container: "body",
                maxTextLength: 0,
                blockquoteBreakingLevel: 2,
                spellCheck: true,
                styleTags: [
                  "p",
                  "blockquote",
                  "pre",
                  "h1",
                  "h2",
                  "h3",
                  "h4",
                  "h5",
                  "h6"
                ],
                fontNames: [
                  "Arial",
                  "Arial Black",
                  "Comic Sans MS",
                  "Courier New",
                  "Helvetica Neue",
                  "Helvetica",
                  "Impact",
                  "Lucida Grande",
                  "Tahoma",
                  "Times New Roman",
                  "Verdana"
                ],
                fontNamesIgnoreCheck: [],
                fontSizes: ["8", "9", "10", "11", "12", "14", "18", "24", "36"],
                // pallete colors(n x n)
                colors: [
                  [
                    "#000000",
                    "#424242",
                    "#636363",
                    "#9C9C94",
                    "#CEC6CE",
                    "#EFEFEF",
                    "#F7F7F7",
                    "#FFFFFF"
                  ],
                  [
                    "#FF0000",
                    "#FF9C00",
                    "#FFFF00",
                    "#00FF00",
                    "#00FFFF",
                    "#0000FF",
                    "#9C00FF",
                    "#FF00FF"
                  ],
                  [
                    "#F7C6CE",
                    "#FFE7CE",
                    "#FFEFC6",
                    "#D6EFD6",
                    "#CEDEE7",
                    "#CEE7F7",
                    "#D6D6E7",
                    "#E7D6DE"
                  ],
                  [
                    "#E79C9C",
                    "#FFC69C",
                    "#FFE79C",
                    "#B5D6A5",
                    "#A5C6CE",
                    "#9CC6EF",
                    "#B5A5D6",
                    "#D6A5BD"
                  ],
                  [
                    "#E76363",
                    "#F7AD6B",
                    "#FFD663",
                    "#94BD7B",
                    "#73A5AD",
                    "#6BADDE",
                    "#8C7BC6",
                    "#C67BA5"
                  ],
                  [
                    "#CE0000",
                    "#E79439",
                    "#EFC631",
                    "#6BA54A",
                    "#4A7B8C",
                    "#3984C6",
                    "#634AA5",
                    "#A54A7B"
                  ],
                  [
                    "#9C0000",
                    "#B56308",
                    "#BD9400",
                    "#397B21",
                    "#104A5A",
                    "#085294",
                    "#311873",
                    "#731842"
                  ],
                  [
                    "#630000",
                    "#7B3900",
                    "#846300",
                    "#295218",
                    "#083139",
                    "#003163",
                    "#21104A",
                    "#4A1031"
                  ]
                ],
                // http://chir.ag/projects/name-that-color/
                colorsName: [
                  [
                    "Black",
                    "Tundora",
                    "Dove Gray",
                    "Star Dust",
                    "Pale Slate",
                    "Gallery",
                    "Alabaster",
                    "White"
                  ],
                  [
                    "Red",
                    "Orange Peel",
                    "Yellow",
                    "Green",
                    "Cyan",
                    "Blue",
                    "Electric Violet",
                    "Magenta"
                  ],
                  [
                    "Azalea",
                    "Karry",
                    "Egg White",
                    "Zanah",
                    "Botticelli",
                    "Tropical Blue",
                    "Mischka",
                    "Twilight"
                  ],
                  [
                    "Tonys Pink",
                    "Peach Orange",
                    "Cream Brulee",
                    "Sprout",
                    "Casper",
                    "Perano",
                    "Cold Purple",
                    "Careys Pink"
                  ],
                  [
                    "Mandy",
                    "Rajah",
                    "Dandelion",
                    "Olivine",
                    "Gulf Stream",
                    "Viking",
                    "Blue Marguerite",
                    "Puce"
                  ],
                  [
                    "Guardsman Red",
                    "Fire Bush",
                    "Golden Dream",
                    "Chelsea Cucumber",
                    "Smalt Blue",
                    "Boston Blue",
                    "Butterfly Bush",
                    "Cadillac"
                  ],
                  [
                    "Sangria",
                    "Mai Tai",
                    "Buddha Gold",
                    "Forest Green",
                    "Eden",
                    "Venice Blue",
                    "Meteorite",
                    "Claret"
                  ],
                  [
                    "Rosewood",
                    "Cinnamon",
                    "Olive",
                    "Parsley",
                    "Tiber",
                    "Midnight Blue",
                    "Valentino",
                    "Loulou"
                  ]
                ],
                colorButton: {
                  foreColor: "#000000",
                  backColor: "#FFFF00"
                },
                lineHeights: [
                  "1.0",
                  "1.2",
                  "1.4",
                  "1.5",
                  "1.6",
                  "1.8",
                  "2.0",
                  "3.0"
                ],
                tableClassName: "table table-bordered",
                insertTableMaxSize: {
                  col: 10,
                  row: 10
                },
                dialogsInBody: false,
                dialogsFade: false,
                maximumImageFileSize: null,
                callbacks: {
                  onBeforeCommand: null,
                  onBlur: null,
                  onBlurCodeview: null,
                  onChange: null,
                  onChangeCodeview: null,
                  onDialogShown: null,
                  onEnter: null,
                  onFocus: null,
                  onImageLinkInsert: null,
                  onImageUpload: null,
                  onImageUploadError: null,
                  onInit: null,
                  onKeydown: null,
                  onKeyup: null,
                  onMousedown: null,
                  onMouseup: null,
                  onPaste: null,
                  onScroll: null
                },
                codemirror: {
                  mode: "text/html",
                  htmlMode: true,
                  lineNumbers: true
                },
                codeviewFilter: false,
                codeviewFilterRegex: /<\/*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|ilayer|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|t(?:itle|extarea)|xml)[^>]*?>/gi,
                codeviewIframeFilter: true,
                codeviewIframeWhitelistSrc: [],
                codeviewIframeWhitelistSrcBase: [
                  "www.youtube.com",
                  "www.youtube-nocookie.com",
                  "www.facebook.com",
                  "vine.co",
                  "instagram.com",
                  "player.vimeo.com",
                  "www.dailymotion.com",
                  "player.youku.com",
                  "v.qq.com"
                ],
                keyMap: {
                  pc: {
                    ENTER: "insertParagraph",
                    "CTRL+Z": "undo",
                    "CTRL+Y": "redo",
                    TAB: "tab",
                    "SHIFT+TAB": "untab",
                    "CTRL+B": "bold",
                    "CTRL+I": "italic",
                    "CTRL+U": "underline",
                    "CTRL+SHIFT+S": "strikethrough",
                    "CTRL+BACKSLASH": "removeFormat",
                    "CTRL+SHIFT+L": "justifyLeft",
                    "CTRL+SHIFT+E": "justifyCenter",
                    "CTRL+SHIFT+R": "justifyRight",
                    "CTRL+SHIFT+J": "justifyFull",
                    "CTRL+SHIFT+NUM7": "insertUnorderedList",
                    "CTRL+SHIFT+NUM8": "insertOrderedList",
                    "CTRL+LEFTBRACKET": "outdent",
                    "CTRL+RIGHTBRACKET": "indent",
                    "CTRL+NUM0": "formatPara",
                    "CTRL+NUM1": "formatH1",
                    "CTRL+NUM2": "formatH2",
                    "CTRL+NUM3": "formatH3",
                    "CTRL+NUM4": "formatH4",
                    "CTRL+NUM5": "formatH5",
                    "CTRL+NUM6": "formatH6",
                    "CTRL+ENTER": "insertHorizontalRule",
                    "CTRL+K": "linkDialog.show"
                  },
                  mac: {
                    ENTER: "insertParagraph",
                    "CMD+Z": "undo",
                    "CMD+SHIFT+Z": "redo",
                    TAB: "tab",
                    "SHIFT+TAB": "untab",
                    "CMD+B": "bold",
                    "CMD+I": "italic",
                    "CMD+U": "underline",
                    "CMD+SHIFT+S": "strikethrough",
                    "CMD+BACKSLASH": "removeFormat",
                    "CMD+SHIFT+L": "justifyLeft",
                    "CMD+SHIFT+E": "justifyCenter",
                    "CMD+SHIFT+R": "justifyRight",
                    "CMD+SHIFT+J": "justifyFull",
                    "CMD+SHIFT+NUM7": "insertUnorderedList",
                    "CMD+SHIFT+NUM8": "insertOrderedList",
                    "CMD+LEFTBRACKET": "outdent",
                    "CMD+RIGHTBRACKET": "indent",
                    "CMD+NUM0": "formatPara",
                    "CMD+NUM1": "formatH1",
                    "CMD+NUM2": "formatH2",
                    "CMD+NUM3": "formatH3",
                    "CMD+NUM4": "formatH4",
                    "CMD+NUM5": "formatH5",
                    "CMD+NUM6": "formatH6",
                    "CMD+ENTER": "insertHorizontalRule",
                    "CMD+K": "linkDialog.show"
                  }
                },
                icons: {
                  align: "note-icon-align",
                  alignCenter: "note-icon-align-center",
                  alignJustify: "note-icon-align-justify",
                  alignLeft: "note-icon-align-left",
                  alignRight: "note-icon-align-right",
                  rowBelow: "note-icon-row-below",
                  colBefore: "note-icon-col-before",
                  colAfter: "note-icon-col-after",
                  rowAbove: "note-icon-row-above",
                  rowRemove: "note-icon-row-remove",
                  colRemove: "note-icon-col-remove",
                  indent: "note-icon-align-indent",
                  outdent: "note-icon-align-outdent",
                  arrowsAlt: "note-icon-arrows-alt",
                  bold: "note-icon-bold",
                  caret: "note-icon-caret",
                  circle: "note-icon-circle",
                  close: "note-icon-close",
                  code: "note-icon-code",
                  eraser: "note-icon-eraser",
                  floatLeft: "note-icon-float-left",
                  floatRight: "note-icon-float-right",
                  font: "note-icon-font",
                  frame: "note-icon-frame",
                  italic: "note-icon-italic",
                  link: "note-icon-link",
                  unlink: "note-icon-chain-broken",
                  magic: "note-icon-magic",
                  menuCheck: "note-icon-menu-check",
                  minus: "note-icon-minus",
                  orderedlist: "note-icon-orderedlist",
                  pencil: "note-icon-pencil",
                  picture: "note-icon-picture",
                  question: "note-icon-question",
                  redo: "note-icon-redo",
                  rollback: "note-icon-rollback",
                  square: "note-icon-square",
                  strikethrough: "note-icon-strikethrough",
                  subscript: "note-icon-subscript",
                  superscript: "note-icon-superscript",
                  table: "note-icon-table",
                  textHeight: "note-icon-text-height",
                  trash: "note-icon-trash",
                  underline: "note-icon-underline",
                  undo: "note-icon-undo",
                  unorderedlist: "note-icon-unorderedlist",
                  video: "note-icon-video"
                }
              }
            });

            $$1.summernote = $$1.extend($$1.summernote, {
              ui: ui
            });
          });
          //# sourceMappingURL=summernote.js.map

          /* WEBPACK VAR INJECTION */
        }.call(
          this,
          __webpack_require__(
            /*! jquery */ "../../node_modules/jquery/dist/jquery.js"
          ),
          __webpack_require__(
            /*! jquery */ "../../node_modules/jquery/dist/jquery.js"
          )
        ));

        /***/
      },

    /***/ "../../node_modules/webpack/buildin/amd-options.js":
      /*!****************************************!*\
  !*** (webpack)/buildin/amd-options.js ***!
  \****************************************/
      /*! no static exports found */
      /***/ function(module, exports) {
        /* WEBPACK VAR INJECTION */ (function(__webpack_amd_options__) {
          /* globals __webpack_amd_options__ */
          module.exports = __webpack_amd_options__;

          /* WEBPACK VAR INJECTION */
        }.call(this, {}));

        /***/
      }
  }
]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vL2hvbWUvbW9maWluL2dvY29kZS9zcmMveTFjb21tb24vYXNzZXRzL25vZGVfbW9kdWxlcy9zdW1tZXJub3RlL2Rpc3Qvc3VtbWVybm90ZS1saXRlLmNzcz9mYmEwIiwid2VicGFjazovLy8vaG9tZS9tb2ZpaW4vZ29jb2RlL3NyYy95MWNvbW1vbi9hc3NldHMvbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzLmpzIiwid2VicGFjazovLy8vaG9tZS9tb2ZpaW4vZ29jb2RlL3NyYy95MWNvbW1vbi9hc3NldHMvbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9saWIvdXJscy5qcyIsIndlYnBhY2s6Ly8vL2hvbWUvbW9maWluL2dvY29kZS9zcmMveTFjb21tb24vYXNzZXRzL25vZGVfbW9kdWxlcy9zdW1tZXJub3RlL2Rpc3Qvc3VtbWVybm90ZS1saXRlLmNzcz9kZDNlIiwid2VicGFjazovLy8vaG9tZS9tb2ZpaW4vZ29jb2RlL3NyYy95MWNvbW1vbi9hc3NldHMvbm9kZV9tb2R1bGVzL3N1bW1lcm5vdGUvZGlzdC9zdW1tZXJub3RlLmpzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9hbWQtb3B0aW9ucy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQSx1Qzs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7O0FBRUEsY0FBYyxtQkFBTyxDQUFDLDJEQUFROztBQUU5QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUEsaUJBQWlCLG1CQUFtQjtBQUNwQztBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDOztBQUVBO0FBQ0EsbUJBQW1CLDJCQUEyQjs7QUFFOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGdCQUFnQixtQkFBbUI7QUFDbkM7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlCQUFpQiwyQkFBMkI7QUFDNUM7QUFDQTs7QUFFQSxRQUFRLHVCQUF1QjtBQUMvQjtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBLGlCQUFpQix1QkFBdUI7QUFDeEM7QUFDQTs7QUFFQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxnQkFBZ0IsaUJBQWlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjOztBQUVkLGtEQUFrRCxzQkFBc0I7QUFDeEU7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx1REFBdUQ7QUFDdkQ7O0FBRUEsNkJBQTZCLG1CQUFtQjs7QUFFaEQ7O0FBRUE7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7OztBQzVXQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsV0FBVyxFQUFFO0FBQ3JELHdDQUF3QyxXQUFXLEVBQUU7O0FBRXJEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0Esc0NBQXNDO0FBQ3RDLEdBQUc7QUFDSDtBQUNBLDhEQUE4RDtBQUM5RDs7QUFFQTtBQUNBO0FBQ0EsRUFBRTs7QUFFRjtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ3hGQTs7QUFFQTtBQUNBLGNBQWMsbUJBQU8sQ0FBQywyUUFBb0g7QUFDMUksNENBQTRDLFFBQVM7QUFDckQ7QUFDQTs7QUFFQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGFBQWEsbUJBQU8sQ0FBQyw2RkFBc0M7QUFDM0Q7QUFDQTtBQUNBLEdBQUcsS0FBVSxFQUFFLEU7Ozs7Ozs7Ozs7O0FDZGY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFLEtBQTRELFdBQVcsbUJBQU8sQ0FBQyx3REFBUTtBQUN6RixFQUFFLFNBQ2lEO0FBQ25ELENBQUMsdUJBQXVCOztBQUV4Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCwrQkFBK0IsOEJBQThCO0FBQzdELEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLCtCQUErQiw4QkFBOEI7QUFDN0QsR0FBRztBQUNIO0FBQ0E7QUFDQSx3REFBd0QsZUFBZTtBQUN2RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9EQUFvRCxlQUFlO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrSEFBK0g7QUFDL0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUgscUJBQXFCLEtBQTRCLElBQUksb0dBQVUsQ0FBQztBQUNoRTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNDQUFzQyxHQUFHLE9BQU8sR0FBRztBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsY0FBYyxPQUFPO0FBQ3JCLGNBQWMsT0FBTztBQUNyQixjQUFjLE9BQU87QUFDckIsY0FBYyxPQUFPO0FBQ3JCLGNBQWMsT0FBTztBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsU0FBUztBQUN0QixhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixjQUFjO0FBQ2Q7QUFDQTtBQUNBLCtDQUErQyxNQUFNLFFBQVEsSUFBSTtBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyxXQUFXO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLFdBQVc7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxNQUFNO0FBQ25CLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLGFBQWEsV0FBVztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsTUFBTTtBQUNuQixhQUFhLFNBQVM7QUFDdEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsTUFBTTtBQUNuQixhQUFhLFNBQVM7QUFDdEI7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLFdBQVc7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxNQUFNO0FBQ25CO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQyxXQUFXO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGNBQWMsUUFBUTtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsY0FBYyxRQUFRO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxLQUFLO0FBQ2xCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsU0FBUztBQUN0QixjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFO0FBQ2pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsS0FBSztBQUNsQixhQUFhLFNBQVM7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxLQUFLO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixHQUFHO0FBQzVCO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsS0FBSztBQUNsQixhQUFhLFNBQVM7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsU0FBUztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsV0FBVztBQUN2RTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxLQUFLO0FBQ2xCLGNBQWMsS0FBSztBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsS0FBSztBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxXQUFXO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsY0FBYztBQUMzQixjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxLQUFLO0FBQ2xCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsS0FBSztBQUNsQixhQUFhLEtBQUs7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsY0FBYztBQUMzQixhQUFhLEtBQUs7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsY0FBYztBQUMzQixhQUFhLEtBQUs7QUFDbEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsYUFBYSxRQUFRO0FBQ3JCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsYUFBYSxRQUFRO0FBQ3JCLGNBQWM7QUFDZDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsY0FBYztBQUMzQixjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsY0FBYztBQUMzQixhQUFhLFNBQVM7QUFDdEIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsU0FBUztBQUN0QixjQUFjO0FBQ2Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsYUFBYSxjQUFjO0FBQzNCLGFBQWEsU0FBUztBQUN0QixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxLQUFLO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsTUFBTTtBQUNuQjtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsU0FBUztBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGFBQWEsUUFBUTtBQUNyQixjQUFjLEtBQUs7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxjQUFjO0FBQzNCLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckIsYUFBYSxRQUFRO0FBQ3JCLGNBQWMsS0FBSztBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsTUFBTTtBQUNuQixhQUFhLFFBQVE7QUFDckIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdURBQXVELFNBQVM7QUFDaEU7QUFDQTtBQUNBLDZDQUE2QyxTQUFTO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEIsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsT0FBTztBQUNwQixjQUFjLEtBQUs7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsUUFBUTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEtBQUs7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBLFdBQVc7QUFDWCxxQ0FBcUMsb0RBQW9EO0FBQ3pGO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUNBQXFDLHFFQUFxRTtBQUMxRztBQUNBLGdEQUFnRDtBQUNoRCw2Q0FBNkM7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFFBQVE7QUFDckIsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiw0QkFBNEI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGNBQWM7QUFDM0IsY0FBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsS0FBSztBQUNwQixlQUFlLE9BQU87QUFDdEIsZUFBZSxLQUFLO0FBQ3BCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsY0FBYztBQUNuQyxxQkFBcUIsUUFBUTtBQUM3QjtBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVM7QUFDMUIsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLFFBQVE7QUFDekIsaUJBQWlCLFFBQVE7QUFDekIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IsUUFBUTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixTQUFTO0FBQzFCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLFNBQVM7QUFDMUIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QixpQkFBaUIsT0FBTztBQUN4QixpQkFBaUIsS0FBSztBQUN0QixpQkFBaUIsT0FBTztBQUN4QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRDQUE0QztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixLQUFLO0FBQ3RCLGlCQUFpQixPQUFPO0FBQ3hCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLE9BQU87QUFDeEIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0Esb0JBQW9CLE9BQU87QUFDM0Isb0JBQW9CLE9BQU87QUFDM0I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsS0FBSztBQUNsQixjQUFjLFFBQVE7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsY0FBYyxRQUFRO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxXQUFXO0FBQ1gsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixLQUFLLHNCQUFzQixNQUFNLHNCQUFzQjtBQUN0RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEM7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixPQUFPO0FBQ3pCLGtCQUFrQixNQUFNO0FBQ3hCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsYUFBYTtBQUM5QixpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixhQUFhO0FBQzlCLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixRQUFRO0FBQ3pCLGlCQUFpQixRQUFRO0FBQ3pCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixhQUFhO0FBQzlCLGtCQUFrQixPQUFPO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsd0JBQXdCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdELHlDQUF5QyxFQUFFO0FBQzNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdELHdCQUF3QixFQUFFO0FBQzFFLGdEQUFnRCxxQ0FBcUMsRUFBRTtBQUN2RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMsd0JBQXdCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUE2Qyx3QkFBd0I7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLE9BQU87QUFDeEIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsUUFBUTtBQUN6QixpQkFBaUIsUUFBUTtBQUN6QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsU0FBUztBQUMxQixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixTQUFTO0FBQzFCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsa0RBQWtELEVBQUU7QUFDaEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsU0FBUztBQUMxQixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixhQUFhO0FBQzlCLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixpQkFBaUIsYUFBYTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrRkFBa0YsNkJBQTZCO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0VBQWdFO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxLQUFLO0FBQ2xCLGFBQWEsS0FBSztBQUNsQixhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLElBQUk7QUFDckIsaUJBQWlCLElBQUk7QUFDckIsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLEtBQUs7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixLQUFLO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSTtBQUNyQixpQkFBaUIsSUFBSTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG9CQUFvQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG9CQUFvQjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSTtBQUNyQixpQkFBaUIsSUFBSTtBQUNyQixpQkFBaUIsT0FBTztBQUN4QixpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDLHdCQUF3QjtBQUN4RDtBQUNBLHFDQUFxQywwQkFBMEI7QUFDL0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkI7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0M7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsYUFBYTtBQUM5QixpQkFBaUIsUUFBUTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixhQUFhO0FBQzlCLGlCQUFpQixPQUFPO0FBQ3hCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLHlCQUF5QjtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLGFBQWE7QUFDOUIsaUJBQWlCLE9BQU87QUFDeEIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMsOEJBQThCO0FBQ2pFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixPQUFPO0FBQ3ZCLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLHFCQUFxQjtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsYUFBYTtBQUM5QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyw4QkFBOEI7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLGFBQWE7QUFDOUIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLDhCQUE4QjtBQUNqRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEIsaUJBQWlCLE9BQU87QUFDeEIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG1CQUFtQjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLG1CQUFtQjtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsYUFBYTtBQUM5QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELFdBQVc7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1gsMkJBQTJCLFVBQVU7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsS0FBSztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCLHFCQUFxQixPQUFPO0FBQzVCLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixPQUFPO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EscUJBQXFCLE9BQU87QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBLFdBQVc7QUFDWDtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixlQUFlO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixnQkFBZ0I7QUFDakMsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixPQUFPO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixrQkFBa0IsYUFBYTtBQUMvQixrQkFBa0IsT0FBTztBQUN6QixrQkFBa0IsUUFBUTtBQUMxQixrQkFBa0IsT0FBTztBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixTQUFTO0FBQzFCLGlCQUFpQixPQUFPO0FBQ3hCLGlCQUFpQixRQUFRO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsTUFBTTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVyx3QkFBd0I7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLHlCQUF5QjtBQUMxRjtBQUNBO0FBQ0EsbUdBQW1HO0FBQ25HO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsc0NBQXNDO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQixpREFBaUQ7QUFDakQ7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLHlCQUF5QjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLHFEQUFxRCxFQUFFO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQiwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0IsMkJBQTJCO0FBQzNCLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0M7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsZUFBZTtBQUNmO0FBQ0E7QUFDQSwwRUFBMEUscUJBQXFCO0FBQy9GO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixXQUFXO0FBQ1g7QUFDQTtBQUNBLDBEQUEwRCxxQkFBcUI7QUFDL0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZixxREFBcUQsV0FBVztBQUNoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQ7QUFDckQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0Qiw0Q0FBNEM7QUFDeEU7QUFDQTtBQUNBLGtDQUFrQywwQkFBMEI7QUFDNUQ7QUFDQTtBQUNBLGtDQUFrQywyQkFBMkI7QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQSxtQkFBbUI7QUFDbkIsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUVBQXVFO0FBQ3ZFO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRkFBaUY7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUE2QztBQUM3QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLG9IQUFvSCxHQUFHO0FBQ3ZIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0VBQXNFLE9BQU87QUFDN0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1RkFBdUY7QUFDdkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixPQUFPO0FBQ3hCLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLEdBQUc7O0FBRUgsQ0FBQztBQUNEOzs7Ozs7Ozs7Ozs7O0FDamxQQTtBQUNBIiwiZmlsZSI6ImpzL2NvbW1vbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCIvKlxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuXG52YXIgc3R5bGVzSW5Eb20gPSB7fTtcblxudmFyXHRtZW1vaXplID0gZnVuY3Rpb24gKGZuKSB7XG5cdHZhciBtZW1vO1xuXG5cdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKHR5cGVvZiBtZW1vID09PSBcInVuZGVmaW5lZFwiKSBtZW1vID0gZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0XHRyZXR1cm4gbWVtbztcblx0fTtcbn07XG5cbnZhciBpc09sZElFID0gbWVtb2l6ZShmdW5jdGlvbiAoKSB7XG5cdC8vIFRlc3QgZm9yIElFIDw9IDkgYXMgcHJvcG9zZWQgYnkgQnJvd3NlcmhhY2tzXG5cdC8vIEBzZWUgaHR0cDovL2Jyb3dzZXJoYWNrcy5jb20vI2hhY2stZTcxZDg2OTJmNjUzMzQxNzNmZWU3MTVjMjIyY2I4MDVcblx0Ly8gVGVzdHMgZm9yIGV4aXN0ZW5jZSBvZiBzdGFuZGFyZCBnbG9iYWxzIGlzIHRvIGFsbG93IHN0eWxlLWxvYWRlclxuXHQvLyB0byBvcGVyYXRlIGNvcnJlY3RseSBpbnRvIG5vbi1zdGFuZGFyZCBlbnZpcm9ubWVudHNcblx0Ly8gQHNlZSBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay1jb250cmliL3N0eWxlLWxvYWRlci9pc3N1ZXMvMTc3XG5cdHJldHVybiB3aW5kb3cgJiYgZG9jdW1lbnQgJiYgZG9jdW1lbnQuYWxsICYmICF3aW5kb3cuYXRvYjtcbn0pO1xuXG52YXIgZ2V0RWxlbWVudCA9IChmdW5jdGlvbiAoZm4pIHtcblx0dmFyIG1lbW8gPSB7fTtcblxuXHRyZXR1cm4gZnVuY3Rpb24oc2VsZWN0b3IpIHtcblx0XHRpZiAodHlwZW9mIG1lbW9bc2VsZWN0b3JdID09PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0XHR2YXIgc3R5bGVUYXJnZXQgPSBmbi5jYWxsKHRoaXMsIHNlbGVjdG9yKTtcblx0XHRcdC8vIFNwZWNpYWwgY2FzZSB0byByZXR1cm4gaGVhZCBvZiBpZnJhbWUgaW5zdGVhZCBvZiBpZnJhbWUgaXRzZWxmXG5cdFx0XHRpZiAoc3R5bGVUYXJnZXQgaW5zdGFuY2VvZiB3aW5kb3cuSFRNTElGcmFtZUVsZW1lbnQpIHtcblx0XHRcdFx0dHJ5IHtcblx0XHRcdFx0XHQvLyBUaGlzIHdpbGwgdGhyb3cgYW4gZXhjZXB0aW9uIGlmIGFjY2VzcyB0byBpZnJhbWUgaXMgYmxvY2tlZFxuXHRcdFx0XHRcdC8vIGR1ZSB0byBjcm9zcy1vcmlnaW4gcmVzdHJpY3Rpb25zXG5cdFx0XHRcdFx0c3R5bGVUYXJnZXQgPSBzdHlsZVRhcmdldC5jb250ZW50RG9jdW1lbnQuaGVhZDtcblx0XHRcdFx0fSBjYXRjaChlKSB7XG5cdFx0XHRcdFx0c3R5bGVUYXJnZXQgPSBudWxsO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0XHRtZW1vW3NlbGVjdG9yXSA9IHN0eWxlVGFyZ2V0O1xuXHRcdH1cblx0XHRyZXR1cm4gbWVtb1tzZWxlY3Rvcl1cblx0fTtcbn0pKGZ1bmN0aW9uICh0YXJnZXQpIHtcblx0cmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KVxufSk7XG5cbnZhciBzaW5nbGV0b24gPSBudWxsO1xudmFyXHRzaW5nbGV0b25Db3VudGVyID0gMDtcbnZhclx0c3R5bGVzSW5zZXJ0ZWRBdFRvcCA9IFtdO1xuXG52YXJcdGZpeFVybHMgPSByZXF1aXJlKFwiLi91cmxzXCIpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGxpc3QsIG9wdGlvbnMpIHtcblx0aWYgKHR5cGVvZiBERUJVRyAhPT0gXCJ1bmRlZmluZWRcIiAmJiBERUJVRykge1xuXHRcdGlmICh0eXBlb2YgZG9jdW1lbnQgIT09IFwib2JqZWN0XCIpIHRocm93IG5ldyBFcnJvcihcIlRoZSBzdHlsZS1sb2FkZXIgY2Fubm90IGJlIHVzZWQgaW4gYSBub24tYnJvd3NlciBlbnZpcm9ubWVudFwiKTtcblx0fVxuXG5cdG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuXG5cdG9wdGlvbnMuYXR0cnMgPSB0eXBlb2Ygb3B0aW9ucy5hdHRycyA9PT0gXCJvYmplY3RcIiA/IG9wdGlvbnMuYXR0cnMgOiB7fTtcblxuXHQvLyBGb3JjZSBzaW5nbGUtdGFnIHNvbHV0aW9uIG9uIElFNi05LCB3aGljaCBoYXMgYSBoYXJkIGxpbWl0IG9uIHRoZSAjIG9mIDxzdHlsZT5cblx0Ly8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxuXHRpZiAoIW9wdGlvbnMuc2luZ2xldG9uICYmIHR5cGVvZiBvcHRpb25zLnNpbmdsZXRvbiAhPT0gXCJib29sZWFuXCIpIG9wdGlvbnMuc2luZ2xldG9uID0gaXNPbGRJRSgpO1xuXG5cdC8vIEJ5IGRlZmF1bHQsIGFkZCA8c3R5bGU+IHRhZ3MgdG8gdGhlIDxoZWFkPiBlbGVtZW50XG5cdGlmICghb3B0aW9ucy5pbnNlcnRJbnRvKSBvcHRpb25zLmluc2VydEludG8gPSBcImhlYWRcIjtcblxuXHQvLyBCeSBkZWZhdWx0LCBhZGQgPHN0eWxlPiB0YWdzIHRvIHRoZSBib3R0b20gb2YgdGhlIHRhcmdldFxuXHRpZiAoIW9wdGlvbnMuaW5zZXJ0QXQpIG9wdGlvbnMuaW5zZXJ0QXQgPSBcImJvdHRvbVwiO1xuXG5cdHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMobGlzdCwgb3B0aW9ucyk7XG5cblx0YWRkU3R5bGVzVG9Eb20oc3R5bGVzLCBvcHRpb25zKTtcblxuXHRyZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG5cdFx0dmFyIG1heVJlbW92ZSA9IFtdO1xuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBpdGVtID0gc3R5bGVzW2ldO1xuXHRcdFx0dmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF07XG5cblx0XHRcdGRvbVN0eWxlLnJlZnMtLTtcblx0XHRcdG1heVJlbW92ZS5wdXNoKGRvbVN0eWxlKTtcblx0XHR9XG5cblx0XHRpZihuZXdMaXN0KSB7XG5cdFx0XHR2YXIgbmV3U3R5bGVzID0gbGlzdFRvU3R5bGVzKG5ld0xpc3QsIG9wdGlvbnMpO1xuXHRcdFx0YWRkU3R5bGVzVG9Eb20obmV3U3R5bGVzLCBvcHRpb25zKTtcblx0XHR9XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IG1heVJlbW92ZS5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGRvbVN0eWxlID0gbWF5UmVtb3ZlW2ldO1xuXG5cdFx0XHRpZihkb21TdHlsZS5yZWZzID09PSAwKSB7XG5cdFx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIGRvbVN0eWxlLnBhcnRzW2pdKCk7XG5cblx0XHRcdFx0ZGVsZXRlIHN0eWxlc0luRG9tW2RvbVN0eWxlLmlkXTtcblx0XHRcdH1cblx0XHR9XG5cdH07XG59O1xuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbSAoc3R5bGVzLCBvcHRpb25zKSB7XG5cdGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0dmFyIGl0ZW0gPSBzdHlsZXNbaV07XG5cdFx0dmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF07XG5cblx0XHRpZihkb21TdHlsZSkge1xuXHRcdFx0ZG9tU3R5bGUucmVmcysrO1xuXG5cdFx0XHRmb3IodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0ZG9tU3R5bGUucGFydHNbal0oaXRlbS5wYXJ0c1tqXSk7XG5cdFx0XHR9XG5cblx0XHRcdGZvcig7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG5cdFx0XHRcdGRvbVN0eWxlLnBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSwgb3B0aW9ucykpO1xuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR2YXIgcGFydHMgPSBbXTtcblxuXHRcdFx0Zm9yKHZhciBqID0gMDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0cGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdLCBvcHRpb25zKSk7XG5cdFx0XHR9XG5cblx0XHRcdHN0eWxlc0luRG9tW2l0ZW0uaWRdID0ge2lkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHN9O1xuXHRcdH1cblx0fVxufVxuXG5mdW5jdGlvbiBsaXN0VG9TdHlsZXMgKGxpc3QsIG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlcyA9IFtdO1xuXHR2YXIgbmV3U3R5bGVzID0ge307XG5cblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG5cdFx0dmFyIGl0ZW0gPSBsaXN0W2ldO1xuXHRcdHZhciBpZCA9IG9wdGlvbnMuYmFzZSA/IGl0ZW1bMF0gKyBvcHRpb25zLmJhc2UgOiBpdGVtWzBdO1xuXHRcdHZhciBjc3MgPSBpdGVtWzFdO1xuXHRcdHZhciBtZWRpYSA9IGl0ZW1bMl07XG5cdFx0dmFyIHNvdXJjZU1hcCA9IGl0ZW1bM107XG5cdFx0dmFyIHBhcnQgPSB7Y3NzOiBjc3MsIG1lZGlhOiBtZWRpYSwgc291cmNlTWFwOiBzb3VyY2VNYXB9O1xuXG5cdFx0aWYoIW5ld1N0eWxlc1tpZF0pIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7aWQ6IGlkLCBwYXJ0czogW3BhcnRdfSk7XG5cdFx0ZWxzZSBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydCk7XG5cdH1cblxuXHRyZXR1cm4gc3R5bGVzO1xufVxuXG5mdW5jdGlvbiBpbnNlcnRTdHlsZUVsZW1lbnQgKG9wdGlvbnMsIHN0eWxlKSB7XG5cdHZhciB0YXJnZXQgPSBnZXRFbGVtZW50KG9wdGlvbnMuaW5zZXJ0SW50bylcblxuXHRpZiAoIXRhcmdldCkge1xuXHRcdHRocm93IG5ldyBFcnJvcihcIkNvdWxkbid0IGZpbmQgYSBzdHlsZSB0YXJnZXQuIFRoaXMgcHJvYmFibHkgbWVhbnMgdGhhdCB0aGUgdmFsdWUgZm9yIHRoZSAnaW5zZXJ0SW50bycgcGFyYW1ldGVyIGlzIGludmFsaWQuXCIpO1xuXHR9XG5cblx0dmFyIGxhc3RTdHlsZUVsZW1lbnRJbnNlcnRlZEF0VG9wID0gc3R5bGVzSW5zZXJ0ZWRBdFRvcFtzdHlsZXNJbnNlcnRlZEF0VG9wLmxlbmd0aCAtIDFdO1xuXG5cdGlmIChvcHRpb25zLmluc2VydEF0ID09PSBcInRvcFwiKSB7XG5cdFx0aWYgKCFsYXN0U3R5bGVFbGVtZW50SW5zZXJ0ZWRBdFRvcCkge1xuXHRcdFx0dGFyZ2V0Lmluc2VydEJlZm9yZShzdHlsZSwgdGFyZ2V0LmZpcnN0Q2hpbGQpO1xuXHRcdH0gZWxzZSBpZiAobGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3AubmV4dFNpYmxpbmcpIHtcblx0XHRcdHRhcmdldC5pbnNlcnRCZWZvcmUoc3R5bGUsIGxhc3RTdHlsZUVsZW1lbnRJbnNlcnRlZEF0VG9wLm5leHRTaWJsaW5nKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGFyZ2V0LmFwcGVuZENoaWxkKHN0eWxlKTtcblx0XHR9XG5cdFx0c3R5bGVzSW5zZXJ0ZWRBdFRvcC5wdXNoKHN0eWxlKTtcblx0fSBlbHNlIGlmIChvcHRpb25zLmluc2VydEF0ID09PSBcImJvdHRvbVwiKSB7XG5cdFx0dGFyZ2V0LmFwcGVuZENoaWxkKHN0eWxlKTtcblx0fSBlbHNlIGlmICh0eXBlb2Ygb3B0aW9ucy5pbnNlcnRBdCA9PT0gXCJvYmplY3RcIiAmJiBvcHRpb25zLmluc2VydEF0LmJlZm9yZSkge1xuXHRcdHZhciBuZXh0U2libGluZyA9IGdldEVsZW1lbnQob3B0aW9ucy5pbnNlcnRJbnRvICsgXCIgXCIgKyBvcHRpb25zLmluc2VydEF0LmJlZm9yZSk7XG5cdFx0dGFyZ2V0Lmluc2VydEJlZm9yZShzdHlsZSwgbmV4dFNpYmxpbmcpO1xuXHR9IGVsc2Uge1xuXHRcdHRocm93IG5ldyBFcnJvcihcIltTdHlsZSBMb2FkZXJdXFxuXFxuIEludmFsaWQgdmFsdWUgZm9yIHBhcmFtZXRlciAnaW5zZXJ0QXQnICgnb3B0aW9ucy5pbnNlcnRBdCcpIGZvdW5kLlxcbiBNdXN0IGJlICd0b3AnLCAnYm90dG9tJywgb3IgT2JqZWN0LlxcbiAoaHR0cHM6Ly9naXRodWIuY29tL3dlYnBhY2stY29udHJpYi9zdHlsZS1sb2FkZXIjaW5zZXJ0YXQpXFxuXCIpO1xuXHR9XG59XG5cbmZ1bmN0aW9uIHJlbW92ZVN0eWxlRWxlbWVudCAoc3R5bGUpIHtcblx0aWYgKHN0eWxlLnBhcmVudE5vZGUgPT09IG51bGwpIHJldHVybiBmYWxzZTtcblx0c3R5bGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZSk7XG5cblx0dmFyIGlkeCA9IHN0eWxlc0luc2VydGVkQXRUb3AuaW5kZXhPZihzdHlsZSk7XG5cdGlmKGlkeCA+PSAwKSB7XG5cdFx0c3R5bGVzSW5zZXJ0ZWRBdFRvcC5zcGxpY2UoaWR4LCAxKTtcblx0fVxufVxuXG5mdW5jdGlvbiBjcmVhdGVTdHlsZUVsZW1lbnQgKG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcInN0eWxlXCIpO1xuXG5cdG9wdGlvbnMuYXR0cnMudHlwZSA9IFwidGV4dC9jc3NcIjtcblxuXHRhZGRBdHRycyhzdHlsZSwgb3B0aW9ucy5hdHRycyk7XG5cdGluc2VydFN0eWxlRWxlbWVudChvcHRpb25zLCBzdHlsZSk7XG5cblx0cmV0dXJuIHN0eWxlO1xufVxuXG5mdW5jdGlvbiBjcmVhdGVMaW5rRWxlbWVudCAob3B0aW9ucykge1xuXHR2YXIgbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJsaW5rXCIpO1xuXG5cdG9wdGlvbnMuYXR0cnMudHlwZSA9IFwidGV4dC9jc3NcIjtcblx0b3B0aW9ucy5hdHRycy5yZWwgPSBcInN0eWxlc2hlZXRcIjtcblxuXHRhZGRBdHRycyhsaW5rLCBvcHRpb25zLmF0dHJzKTtcblx0aW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMsIGxpbmspO1xuXG5cdHJldHVybiBsaW5rO1xufVxuXG5mdW5jdGlvbiBhZGRBdHRycyAoZWwsIGF0dHJzKSB7XG5cdE9iamVjdC5rZXlzKGF0dHJzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcblx0XHRlbC5zZXRBdHRyaWJ1dGUoa2V5LCBhdHRyc1trZXldKTtcblx0fSk7XG59XG5cbmZ1bmN0aW9uIGFkZFN0eWxlIChvYmosIG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlLCB1cGRhdGUsIHJlbW92ZSwgcmVzdWx0O1xuXG5cdC8vIElmIGEgdHJhbnNmb3JtIGZ1bmN0aW9uIHdhcyBkZWZpbmVkLCBydW4gaXQgb24gdGhlIGNzc1xuXHRpZiAob3B0aW9ucy50cmFuc2Zvcm0gJiYgb2JqLmNzcykge1xuXHQgICAgcmVzdWx0ID0gb3B0aW9ucy50cmFuc2Zvcm0ob2JqLmNzcyk7XG5cblx0ICAgIGlmIChyZXN1bHQpIHtcblx0ICAgIFx0Ly8gSWYgdHJhbnNmb3JtIHJldHVybnMgYSB2YWx1ZSwgdXNlIHRoYXQgaW5zdGVhZCBvZiB0aGUgb3JpZ2luYWwgY3NzLlxuXHQgICAgXHQvLyBUaGlzIGFsbG93cyBydW5uaW5nIHJ1bnRpbWUgdHJhbnNmb3JtYXRpb25zIG9uIHRoZSBjc3MuXG5cdCAgICBcdG9iai5jc3MgPSByZXN1bHQ7XG5cdCAgICB9IGVsc2Uge1xuXHQgICAgXHQvLyBJZiB0aGUgdHJhbnNmb3JtIGZ1bmN0aW9uIHJldHVybnMgYSBmYWxzeSB2YWx1ZSwgZG9uJ3QgYWRkIHRoaXMgY3NzLlxuXHQgICAgXHQvLyBUaGlzIGFsbG93cyBjb25kaXRpb25hbCBsb2FkaW5nIG9mIGNzc1xuXHQgICAgXHRyZXR1cm4gZnVuY3Rpb24oKSB7XG5cdCAgICBcdFx0Ly8gbm9vcFxuXHQgICAgXHR9O1xuXHQgICAgfVxuXHR9XG5cblx0aWYgKG9wdGlvbnMuc2luZ2xldG9uKSB7XG5cdFx0dmFyIHN0eWxlSW5kZXggPSBzaW5nbGV0b25Db3VudGVyKys7XG5cblx0XHRzdHlsZSA9IHNpbmdsZXRvbiB8fCAoc2luZ2xldG9uID0gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpKTtcblxuXHRcdHVwZGF0ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgZmFsc2UpO1xuXHRcdHJlbW92ZSA9IGFwcGx5VG9TaW5nbGV0b25UYWcuYmluZChudWxsLCBzdHlsZSwgc3R5bGVJbmRleCwgdHJ1ZSk7XG5cblx0fSBlbHNlIGlmIChcblx0XHRvYmouc291cmNlTWFwICYmXG5cdFx0dHlwZW9mIFVSTCA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIFVSTC5jcmVhdGVPYmplY3RVUkwgPT09IFwiZnVuY3Rpb25cIiAmJlxuXHRcdHR5cGVvZiBVUkwucmV2b2tlT2JqZWN0VVJMID09PSBcImZ1bmN0aW9uXCIgJiZcblx0XHR0eXBlb2YgQmxvYiA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIGJ0b2EgPT09IFwiZnVuY3Rpb25cIlxuXHQpIHtcblx0XHRzdHlsZSA9IGNyZWF0ZUxpbmtFbGVtZW50KG9wdGlvbnMpO1xuXHRcdHVwZGF0ZSA9IHVwZGF0ZUxpbmsuYmluZChudWxsLCBzdHlsZSwgb3B0aW9ucyk7XG5cdFx0cmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0cmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlKTtcblxuXHRcdFx0aWYoc3R5bGUuaHJlZikgVVJMLnJldm9rZU9iamVjdFVSTChzdHlsZS5ocmVmKTtcblx0XHR9O1xuXHR9IGVsc2Uge1xuXHRcdHN0eWxlID0gY3JlYXRlU3R5bGVFbGVtZW50KG9wdGlvbnMpO1xuXHRcdHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZSk7XG5cdFx0cmVtb3ZlID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0cmVtb3ZlU3R5bGVFbGVtZW50KHN0eWxlKTtcblx0XHR9O1xuXHR9XG5cblx0dXBkYXRlKG9iaik7XG5cblx0cmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlIChuZXdPYmopIHtcblx0XHRpZiAobmV3T2JqKSB7XG5cdFx0XHRpZiAoXG5cdFx0XHRcdG5ld09iai5jc3MgPT09IG9iai5jc3MgJiZcblx0XHRcdFx0bmV3T2JqLm1lZGlhID09PSBvYmoubWVkaWEgJiZcblx0XHRcdFx0bmV3T2JqLnNvdXJjZU1hcCA9PT0gb2JqLnNvdXJjZU1hcFxuXHRcdFx0KSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0dXBkYXRlKG9iaiA9IG5ld09iaik7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHJlbW92ZSgpO1xuXHRcdH1cblx0fTtcbn1cblxudmFyIHJlcGxhY2VUZXh0ID0gKGZ1bmN0aW9uICgpIHtcblx0dmFyIHRleHRTdG9yZSA9IFtdO1xuXG5cdHJldHVybiBmdW5jdGlvbiAoaW5kZXgsIHJlcGxhY2VtZW50KSB7XG5cdFx0dGV4dFN0b3JlW2luZGV4XSA9IHJlcGxhY2VtZW50O1xuXG5cdFx0cmV0dXJuIHRleHRTdG9yZS5maWx0ZXIoQm9vbGVhbikuam9pbignXFxuJyk7XG5cdH07XG59KSgpO1xuXG5mdW5jdGlvbiBhcHBseVRvU2luZ2xldG9uVGFnIChzdHlsZSwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG5cdHZhciBjc3MgPSByZW1vdmUgPyBcIlwiIDogb2JqLmNzcztcblxuXHRpZiAoc3R5bGUuc3R5bGVTaGVldCkge1xuXHRcdHN0eWxlLnN0eWxlU2hlZXQuY3NzVGV4dCA9IHJlcGxhY2VUZXh0KGluZGV4LCBjc3MpO1xuXHR9IGVsc2Uge1xuXHRcdHZhciBjc3NOb2RlID0gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY3NzKTtcblx0XHR2YXIgY2hpbGROb2RlcyA9IHN0eWxlLmNoaWxkTm9kZXM7XG5cblx0XHRpZiAoY2hpbGROb2Rlc1tpbmRleF0pIHN0eWxlLnJlbW92ZUNoaWxkKGNoaWxkTm9kZXNbaW5kZXhdKTtcblxuXHRcdGlmIChjaGlsZE5vZGVzLmxlbmd0aCkge1xuXHRcdFx0c3R5bGUuaW5zZXJ0QmVmb3JlKGNzc05vZGUsIGNoaWxkTm9kZXNbaW5kZXhdKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0c3R5bGUuYXBwZW5kQ2hpbGQoY3NzTm9kZSk7XG5cdFx0fVxuXHR9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcgKHN0eWxlLCBvYmopIHtcblx0dmFyIGNzcyA9IG9iai5jc3M7XG5cdHZhciBtZWRpYSA9IG9iai5tZWRpYTtcblxuXHRpZihtZWRpYSkge1xuXHRcdHN0eWxlLnNldEF0dHJpYnV0ZShcIm1lZGlhXCIsIG1lZGlhKVxuXHR9XG5cblx0aWYoc3R5bGUuc3R5bGVTaGVldCkge1xuXHRcdHN0eWxlLnN0eWxlU2hlZXQuY3NzVGV4dCA9IGNzcztcblx0fSBlbHNlIHtcblx0XHR3aGlsZShzdHlsZS5maXJzdENoaWxkKSB7XG5cdFx0XHRzdHlsZS5yZW1vdmVDaGlsZChzdHlsZS5maXJzdENoaWxkKTtcblx0XHR9XG5cblx0XHRzdHlsZS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKTtcblx0fVxufVxuXG5mdW5jdGlvbiB1cGRhdGVMaW5rIChsaW5rLCBvcHRpb25zLCBvYmopIHtcblx0dmFyIGNzcyA9IG9iai5jc3M7XG5cdHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwO1xuXG5cdC8qXG5cdFx0SWYgY29udmVydFRvQWJzb2x1dGVVcmxzIGlzbid0IGRlZmluZWQsIGJ1dCBzb3VyY2VtYXBzIGFyZSBlbmFibGVkXG5cdFx0YW5kIHRoZXJlIGlzIG5vIHB1YmxpY1BhdGggZGVmaW5lZCB0aGVuIGxldHMgdHVybiBjb252ZXJ0VG9BYnNvbHV0ZVVybHNcblx0XHRvbiBieSBkZWZhdWx0LiAgT3RoZXJ3aXNlIGRlZmF1bHQgdG8gdGhlIGNvbnZlcnRUb0Fic29sdXRlVXJscyBvcHRpb25cblx0XHRkaXJlY3RseVxuXHQqL1xuXHR2YXIgYXV0b0ZpeFVybHMgPSBvcHRpb25zLmNvbnZlcnRUb0Fic29sdXRlVXJscyA9PT0gdW5kZWZpbmVkICYmIHNvdXJjZU1hcDtcblxuXHRpZiAob3B0aW9ucy5jb252ZXJ0VG9BYnNvbHV0ZVVybHMgfHwgYXV0b0ZpeFVybHMpIHtcblx0XHRjc3MgPSBmaXhVcmxzKGNzcyk7XG5cdH1cblxuXHRpZiAoc291cmNlTWFwKSB7XG5cdFx0Ly8gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjY2MDM4NzVcblx0XHRjc3MgKz0gXCJcXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LFwiICsgYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSArIFwiICovXCI7XG5cdH1cblxuXHR2YXIgYmxvYiA9IG5ldyBCbG9iKFtjc3NdLCB7IHR5cGU6IFwidGV4dC9jc3NcIiB9KTtcblxuXHR2YXIgb2xkU3JjID0gbGluay5ocmVmO1xuXG5cdGxpbmsuaHJlZiA9IFVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XG5cblx0aWYob2xkU3JjKSBVUkwucmV2b2tlT2JqZWN0VVJMKG9sZFNyYyk7XG59XG4iLCJcbi8qKlxuICogV2hlbiBzb3VyY2UgbWFwcyBhcmUgZW5hYmxlZCwgYHN0eWxlLWxvYWRlcmAgdXNlcyBhIGxpbmsgZWxlbWVudCB3aXRoIGEgZGF0YS11cmkgdG9cbiAqIGVtYmVkIHRoZSBjc3Mgb24gdGhlIHBhZ2UuIFRoaXMgYnJlYWtzIGFsbCByZWxhdGl2ZSB1cmxzIGJlY2F1c2Ugbm93IHRoZXkgYXJlIHJlbGF0aXZlIHRvIGFcbiAqIGJ1bmRsZSBpbnN0ZWFkIG9mIHRoZSBjdXJyZW50IHBhZ2UuXG4gKlxuICogT25lIHNvbHV0aW9uIGlzIHRvIG9ubHkgdXNlIGZ1bGwgdXJscywgYnV0IHRoYXQgbWF5IGJlIGltcG9zc2libGUuXG4gKlxuICogSW5zdGVhZCwgdGhpcyBmdW5jdGlvbiBcImZpeGVzXCIgdGhlIHJlbGF0aXZlIHVybHMgdG8gYmUgYWJzb2x1dGUgYWNjb3JkaW5nIHRvIHRoZSBjdXJyZW50IHBhZ2UgbG9jYXRpb24uXG4gKlxuICogQSBydWRpbWVudGFyeSB0ZXN0IHN1aXRlIGlzIGxvY2F0ZWQgYXQgYHRlc3QvZml4VXJscy5qc2AgYW5kIGNhbiBiZSBydW4gdmlhIHRoZSBgbnBtIHRlc3RgIGNvbW1hbmQuXG4gKlxuICovXG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGNzcykge1xuICAvLyBnZXQgY3VycmVudCBsb2NhdGlvblxuICB2YXIgbG9jYXRpb24gPSB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdy5sb2NhdGlvbjtcblxuICBpZiAoIWxvY2F0aW9uKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFwiZml4VXJscyByZXF1aXJlcyB3aW5kb3cubG9jYXRpb25cIik7XG4gIH1cblxuXHQvLyBibGFuayBvciBudWxsP1xuXHRpZiAoIWNzcyB8fCB0eXBlb2YgY3NzICE9PSBcInN0cmluZ1wiKSB7XG5cdCAgcmV0dXJuIGNzcztcbiAgfVxuXG4gIHZhciBiYXNlVXJsID0gbG9jYXRpb24ucHJvdG9jb2wgKyBcIi8vXCIgKyBsb2NhdGlvbi5ob3N0O1xuICB2YXIgY3VycmVudERpciA9IGJhc2VVcmwgKyBsb2NhdGlvbi5wYXRobmFtZS5yZXBsYWNlKC9cXC9bXlxcL10qJC8sIFwiL1wiKTtcblxuXHQvLyBjb252ZXJ0IGVhY2ggdXJsKC4uLilcblx0Lypcblx0VGhpcyByZWd1bGFyIGV4cHJlc3Npb24gaXMganVzdCBhIHdheSB0byByZWN1cnNpdmVseSBtYXRjaCBicmFja2V0cyB3aXRoaW5cblx0YSBzdHJpbmcuXG5cblx0IC91cmxcXHMqXFwoICA9IE1hdGNoIG9uIHRoZSB3b3JkIFwidXJsXCIgd2l0aCBhbnkgd2hpdGVzcGFjZSBhZnRlciBpdCBhbmQgdGhlbiBhIHBhcmVuc1xuXHQgICAoICA9IFN0YXJ0IGEgY2FwdHVyaW5nIGdyb3VwXG5cdCAgICAgKD86ICA9IFN0YXJ0IGEgbm9uLWNhcHR1cmluZyBncm91cFxuXHQgICAgICAgICBbXikoXSAgPSBNYXRjaCBhbnl0aGluZyB0aGF0IGlzbid0IGEgcGFyZW50aGVzZXNcblx0ICAgICAgICAgfCAgPSBPUlxuXHQgICAgICAgICBcXCggID0gTWF0Y2ggYSBzdGFydCBwYXJlbnRoZXNlc1xuXHQgICAgICAgICAgICAgKD86ICA9IFN0YXJ0IGFub3RoZXIgbm9uLWNhcHR1cmluZyBncm91cHNcblx0ICAgICAgICAgICAgICAgICBbXikoXSsgID0gTWF0Y2ggYW55dGhpbmcgdGhhdCBpc24ndCBhIHBhcmVudGhlc2VzXG5cdCAgICAgICAgICAgICAgICAgfCAgPSBPUlxuXHQgICAgICAgICAgICAgICAgIFxcKCAgPSBNYXRjaCBhIHN0YXJ0IHBhcmVudGhlc2VzXG5cdCAgICAgICAgICAgICAgICAgICAgIFteKShdKiAgPSBNYXRjaCBhbnl0aGluZyB0aGF0IGlzbid0IGEgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICAgICBcXCkgID0gTWF0Y2ggYSBlbmQgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICkgID0gRW5kIEdyb3VwXG4gICAgICAgICAgICAgICpcXCkgPSBNYXRjaCBhbnl0aGluZyBhbmQgdGhlbiBhIGNsb3NlIHBhcmVuc1xuICAgICAgICAgICkgID0gQ2xvc2Ugbm9uLWNhcHR1cmluZyBncm91cFxuICAgICAgICAgICogID0gTWF0Y2ggYW55dGhpbmdcbiAgICAgICApICA9IENsb3NlIGNhcHR1cmluZyBncm91cFxuXHQgXFwpICA9IE1hdGNoIGEgY2xvc2UgcGFyZW5zXG5cblx0IC9naSAgPSBHZXQgYWxsIG1hdGNoZXMsIG5vdCB0aGUgZmlyc3QuICBCZSBjYXNlIGluc2Vuc2l0aXZlLlxuXHQgKi9cblx0dmFyIGZpeGVkQ3NzID0gY3NzLnJlcGxhY2UoL3VybFxccypcXCgoKD86W14pKF18XFwoKD86W14pKF0rfFxcKFteKShdKlxcKSkqXFwpKSopXFwpL2dpLCBmdW5jdGlvbihmdWxsTWF0Y2gsIG9yaWdVcmwpIHtcblx0XHQvLyBzdHJpcCBxdW90ZXMgKGlmIHRoZXkgZXhpc3QpXG5cdFx0dmFyIHVucXVvdGVkT3JpZ1VybCA9IG9yaWdVcmxcblx0XHRcdC50cmltKClcblx0XHRcdC5yZXBsYWNlKC9eXCIoLiopXCIkLywgZnVuY3Rpb24obywgJDEpeyByZXR1cm4gJDE7IH0pXG5cdFx0XHQucmVwbGFjZSgvXicoLiopJyQvLCBmdW5jdGlvbihvLCAkMSl7IHJldHVybiAkMTsgfSk7XG5cblx0XHQvLyBhbHJlYWR5IGEgZnVsbCB1cmw/IG5vIGNoYW5nZVxuXHRcdGlmICgvXigjfGRhdGE6fGh0dHA6XFwvXFwvfGh0dHBzOlxcL1xcL3xmaWxlOlxcL1xcL1xcLykvaS50ZXN0KHVucXVvdGVkT3JpZ1VybCkpIHtcblx0XHQgIHJldHVybiBmdWxsTWF0Y2g7XG5cdFx0fVxuXG5cdFx0Ly8gY29udmVydCB0aGUgdXJsIHRvIGEgZnVsbCB1cmxcblx0XHR2YXIgbmV3VXJsO1xuXG5cdFx0aWYgKHVucXVvdGVkT3JpZ1VybC5pbmRleE9mKFwiLy9cIikgPT09IDApIHtcblx0XHQgIFx0Ly9UT0RPOiBzaG91bGQgd2UgYWRkIHByb3RvY29sP1xuXHRcdFx0bmV3VXJsID0gdW5xdW90ZWRPcmlnVXJsO1xuXHRcdH0gZWxzZSBpZiAodW5xdW90ZWRPcmlnVXJsLmluZGV4T2YoXCIvXCIpID09PSAwKSB7XG5cdFx0XHQvLyBwYXRoIHNob3VsZCBiZSByZWxhdGl2ZSB0byB0aGUgYmFzZSB1cmxcblx0XHRcdG5ld1VybCA9IGJhc2VVcmwgKyB1bnF1b3RlZE9yaWdVcmw7IC8vIGFscmVhZHkgc3RhcnRzIHdpdGggJy8nXG5cdFx0fSBlbHNlIHtcblx0XHRcdC8vIHBhdGggc2hvdWxkIGJlIHJlbGF0aXZlIHRvIGN1cnJlbnQgZGlyZWN0b3J5XG5cdFx0XHRuZXdVcmwgPSBjdXJyZW50RGlyICsgdW5xdW90ZWRPcmlnVXJsLnJlcGxhY2UoL15cXC5cXC8vLCBcIlwiKTsgLy8gU3RyaXAgbGVhZGluZyAnLi8nXG5cdFx0fVxuXG5cdFx0Ly8gc2VuZCBiYWNrIHRoZSBmaXhlZCB1cmwoLi4uKVxuXHRcdHJldHVybiBcInVybChcIiArIEpTT04uc3RyaW5naWZ5KG5ld1VybCkgKyBcIilcIjtcblx0fSk7XG5cblx0Ly8gc2VuZCBiYWNrIHRoZSBmaXhlZCBjc3Ncblx0cmV0dXJuIGZpeGVkQ3NzO1xufTtcbiIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi9taW5pLWNzcy1leHRyYWN0LXBsdWdpbi9kaXN0L2xvYWRlci5qcz8/cmVmLS02LTEhLi4vLi4vY3NzLWxvYWRlci9pbmRleC5qcz8/cmVmLS02LTIhLi9zdW1tZXJub3RlLWxpdGUuY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBQcmVwYXJlIGNzc1RyYW5zZm9ybWF0aW9uXG52YXIgdHJhbnNmb3JtO1xuXG52YXIgb3B0aW9ucyA9IHtcImhtclwiOnRydWV9XG5vcHRpb25zLnRyYW5zZm9ybSA9IHRyYW5zZm9ybVxuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanNcIikoY29udGVudCwgb3B0aW9ucyk7XG5pZihjb250ZW50LmxvY2FscykgbW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2Fscztcbi8vIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnRcbmlmKG1vZHVsZS5ob3QpIHtcblx0Ly8gV2hlbiB0aGUgc3R5bGVzIGNoYW5nZSwgdXBkYXRlIHRoZSA8c3R5bGU+IHRhZ3Ncblx0aWYoIWNvbnRlbnQubG9jYWxzKSB7XG5cdFx0bW9kdWxlLmhvdC5hY2NlcHQoXCIhIS4uLy4uL21pbmktY3NzLWV4dHJhY3QtcGx1Z2luL2Rpc3QvbG9hZGVyLmpzPz9yZWYtLTYtMSEuLi8uLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTYtMiEuL3N1bW1lcm5vdGUtbGl0ZS5jc3NcIiwgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgbmV3Q29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uL21pbmktY3NzLWV4dHJhY3QtcGx1Z2luL2Rpc3QvbG9hZGVyLmpzPz9yZWYtLTYtMSEuLi8uLi9jc3MtbG9hZGVyL2luZGV4LmpzPz9yZWYtLTYtMiEuL3N1bW1lcm5vdGUtbGl0ZS5jc3NcIik7XG5cdFx0XHRpZih0eXBlb2YgbmV3Q29udGVudCA9PT0gJ3N0cmluZycpIG5ld0NvbnRlbnQgPSBbW21vZHVsZS5pZCwgbmV3Q29udGVudCwgJyddXTtcblx0XHRcdHVwZGF0ZShuZXdDb250ZW50KTtcblx0XHR9KTtcblx0fVxuXHQvLyBXaGVuIHRoZSBtb2R1bGUgaXMgZGlzcG9zZWQsIHJlbW92ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdG1vZHVsZS5ob3QuZGlzcG9zZShmdW5jdGlvbigpIHsgdXBkYXRlKCk7IH0pO1xufSIsIi8qKlxuICogU3VwZXIgc2ltcGxlIHd5c2l3eWcgZWRpdG9yIHYwLjguMTJcbiAqIGh0dHBzOi8vc3VtbWVybm90ZS5vcmdcbiAqXG4gKiBDb3B5cmlnaHQgMjAxMy0gQWxhbiBIb25nLiBhbmQgb3RoZXIgY29udHJpYnV0b3JzXG4gKiBzdW1tZXJub3RlIG1heSBiZSBmcmVlbHkgZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIE1JVCBsaWNlbnNlLlxuICpcbiAqIERhdGU6IDIwMTktMDUtMTZUMDg6MTZaXG4gKi9cbihmdW5jdGlvbiAoZ2xvYmFsLCBmYWN0b3J5KSB7XG4gIHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlICE9PSAndW5kZWZpbmVkJyA/IGZhY3RvcnkocmVxdWlyZSgnanF1ZXJ5JykpIDpcbiAgdHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kID8gZGVmaW5lKFsnanF1ZXJ5J10sIGZhY3RvcnkpIDpcbiAgKGdsb2JhbCA9IGdsb2JhbCB8fCBzZWxmLCBmYWN0b3J5KGdsb2JhbC5qUXVlcnkpKTtcbn0odGhpcywgZnVuY3Rpb24gKCQkMSkgeyAndXNlIHN0cmljdCc7XG5cbiAgJCQxID0gJCQxICYmICQkMS5oYXNPd25Qcm9wZXJ0eSgnZGVmYXVsdCcpID8gJCQxWydkZWZhdWx0J10gOiAkJDE7XG5cbiAgdmFyIFJlbmRlcmVyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gUmVuZGVyZXIobWFya3VwLCBjaGlsZHJlbiwgb3B0aW9ucywgY2FsbGJhY2spIHtcbiAgICAgICAgICB0aGlzLm1hcmt1cCA9IG1hcmt1cDtcbiAgICAgICAgICB0aGlzLmNoaWxkcmVuID0gY2hpbGRyZW47XG4gICAgICAgICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgICAgICAgICB0aGlzLmNhbGxiYWNrID0gY2FsbGJhY2s7XG4gICAgICB9XG4gICAgICBSZW5kZXJlci5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gKCRwYXJlbnQpIHtcbiAgICAgICAgICB2YXIgJG5vZGUgPSAkJDEodGhpcy5tYXJrdXApO1xuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmNvbnRlbnRzKSB7XG4gICAgICAgICAgICAgICRub2RlLmh0bWwodGhpcy5vcHRpb25zLmNvbnRlbnRzKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMuY2xhc3NOYW1lKSB7XG4gICAgICAgICAgICAgICRub2RlLmFkZENsYXNzKHRoaXMub3B0aW9ucy5jbGFzc05hbWUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5kYXRhKSB7XG4gICAgICAgICAgICAgICQkMS5lYWNoKHRoaXMub3B0aW9ucy5kYXRhLCBmdW5jdGlvbiAoaywgdikge1xuICAgICAgICAgICAgICAgICAgJG5vZGUuYXR0cignZGF0YS0nICsgaywgdik7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5jbGljaykge1xuICAgICAgICAgICAgICAkbm9kZS5vbignY2xpY2snLCB0aGlzLm9wdGlvbnMuY2xpY2spO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodGhpcy5jaGlsZHJlbikge1xuICAgICAgICAgICAgICB2YXIgJGNvbnRhaW5lcl8xID0gJG5vZGUuZmluZCgnLm5vdGUtY2hpbGRyZW4tY29udGFpbmVyJyk7XG4gICAgICAgICAgICAgIHRoaXMuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkLnJlbmRlcigkY29udGFpbmVyXzEubGVuZ3RoID8gJGNvbnRhaW5lcl8xIDogJG5vZGUpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHRoaXMuY2FsbGJhY2spIHtcbiAgICAgICAgICAgICAgdGhpcy5jYWxsYmFjaygkbm9kZSwgdGhpcy5vcHRpb25zKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMuY2FsbGJhY2spIHtcbiAgICAgICAgICAgICAgdGhpcy5vcHRpb25zLmNhbGxiYWNrKCRub2RlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCRwYXJlbnQpIHtcbiAgICAgICAgICAgICAgJHBhcmVudC5hcHBlbmQoJG5vZGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gJG5vZGU7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFJlbmRlcmVyO1xuICB9KCkpO1xuICB2YXIgcmVuZGVyZXIgPSB7XG4gICAgICBjcmVhdGU6IGZ1bmN0aW9uIChtYXJrdXAsIGNhbGxiYWNrKSB7XG4gICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgdmFyIG9wdGlvbnMgPSB0eXBlb2YgYXJndW1lbnRzWzFdID09PSAnb2JqZWN0JyA/IGFyZ3VtZW50c1sxXSA6IGFyZ3VtZW50c1swXTtcbiAgICAgICAgICAgICAgdmFyIGNoaWxkcmVuID0gQXJyYXkuaXNBcnJheShhcmd1bWVudHNbMF0pID8gYXJndW1lbnRzWzBdIDogW107XG4gICAgICAgICAgICAgIGlmIChvcHRpb25zICYmIG9wdGlvbnMuY2hpbGRyZW4pIHtcbiAgICAgICAgICAgICAgICAgIGNoaWxkcmVuID0gb3B0aW9ucy5jaGlsZHJlbjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4gbmV3IFJlbmRlcmVyKG1hcmt1cCwgY2hpbGRyZW4sIG9wdGlvbnMsIGNhbGxiYWNrKTtcbiAgICAgICAgICB9O1xuICAgICAgfVxuICB9O1xuXG4gIHZhciBlZGl0b3IgPSByZW5kZXJlci5jcmVhdGUoJzxkaXYgY2xhc3M9XCJub3RlLWVkaXRvciBub3RlLWZyYW1lIHBhbmVsIHBhbmVsLWRlZmF1bHRcIi8+Jyk7XG4gIHZhciB0b29sYmFyID0gcmVuZGVyZXIuY3JlYXRlKCc8ZGl2IGNsYXNzPVwibm90ZS10b29sYmFyIHBhbmVsLWhlYWRpbmdcIiByb2xlPVwidG9vbGJhclwiPjwvZGl2PjwvZGl2PicpO1xuICB2YXIgZWRpdGluZ0FyZWEgPSByZW5kZXJlci5jcmVhdGUoJzxkaXYgY2xhc3M9XCJub3RlLWVkaXRpbmctYXJlYVwiLz4nKTtcbiAgdmFyIGNvZGFibGUgPSByZW5kZXJlci5jcmVhdGUoJzx0ZXh0YXJlYSBjbGFzcz1cIm5vdGUtY29kYWJsZVwiIHJvbGU9XCJ0ZXh0Ym94XCIgYXJpYS1tdWx0aWxpbmU9XCJ0cnVlXCIvPicpO1xuICB2YXIgZWRpdGFibGUgPSByZW5kZXJlci5jcmVhdGUoJzxkaXYgY2xhc3M9XCJub3RlLWVkaXRhYmxlXCIgY29udGVudEVkaXRhYmxlPVwidHJ1ZVwiIHJvbGU9XCJ0ZXh0Ym94XCIgYXJpYS1tdWx0aWxpbmU9XCJ0cnVlXCIvPicpO1xuICB2YXIgc3RhdHVzYmFyID0gcmVuZGVyZXIuY3JlYXRlKFtcbiAgICAgICc8b3V0cHV0IGNsYXNzPVwibm90ZS1zdGF0dXMtb3V0cHV0XCIgYXJpYS1saXZlPVwicG9saXRlXCIvPicsXG4gICAgICAnPGRpdiBjbGFzcz1cIm5vdGUtc3RhdHVzYmFyXCIgcm9sZT1cInN0YXR1c1wiPicsXG4gICAgICAnICA8ZGl2IGNsYXNzPVwibm90ZS1yZXNpemViYXJcIiByb2xlPVwic2VwZXJhdG9yXCIgYXJpYS1vcmllbnRhdGlvbj1cImhvcml6b250YWxcIiBhcmlhLWxhYmVsPVwiUmVzaXplXCI+JyxcbiAgICAgICcgICAgPGRpdiBjbGFzcz1cIm5vdGUtaWNvbi1iYXJcIi8+JyxcbiAgICAgICcgICAgPGRpdiBjbGFzcz1cIm5vdGUtaWNvbi1iYXJcIi8+JyxcbiAgICAgICcgICAgPGRpdiBjbGFzcz1cIm5vdGUtaWNvbi1iYXJcIi8+JyxcbiAgICAgICcgIDwvZGl2PicsXG4gICAgICAnPC9kaXY+JyxcbiAgXS5qb2luKCcnKSk7XG4gIHZhciBhaXJFZGl0b3IgPSByZW5kZXJlci5jcmVhdGUoJzxkaXYgY2xhc3M9XCJub3RlLWVkaXRvclwiLz4nKTtcbiAgdmFyIGFpckVkaXRhYmxlID0gcmVuZGVyZXIuY3JlYXRlKFtcbiAgICAgICc8ZGl2IGNsYXNzPVwibm90ZS1lZGl0YWJsZVwiIGNvbnRlbnRFZGl0YWJsZT1cInRydWVcIiByb2xlPVwidGV4dGJveFwiIGFyaWEtbXVsdGlsaW5lPVwidHJ1ZVwiLz4nLFxuICAgICAgJzxvdXRwdXQgY2xhc3M9XCJub3RlLXN0YXR1cy1vdXRwdXRcIiBhcmlhLWxpdmU9XCJwb2xpdGVcIi8+JyxcbiAgXS5qb2luKCcnKSk7XG4gIHZhciBidXR0b25Hcm91cCA9IHJlbmRlcmVyLmNyZWF0ZSgnPGRpdiBjbGFzcz1cIm5vdGUtYnRuLWdyb3VwIGJ0bi1ncm91cFwiPicpO1xuICB2YXIgZHJvcGRvd24gPSByZW5kZXJlci5jcmVhdGUoJzx1bCBjbGFzcz1cImRyb3Bkb3duLW1lbnVcIiByb2xlPVwibGlzdFwiPicsIGZ1bmN0aW9uICgkbm9kZSwgb3B0aW9ucykge1xuICAgICAgdmFyIG1hcmt1cCA9IEFycmF5LmlzQXJyYXkob3B0aW9ucy5pdGVtcykgPyBvcHRpb25zLml0ZW1zLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgIHZhciB2YWx1ZSA9ICh0eXBlb2YgaXRlbSA9PT0gJ3N0cmluZycpID8gaXRlbSA6IChpdGVtLnZhbHVlIHx8ICcnKTtcbiAgICAgICAgICB2YXIgY29udGVudCA9IG9wdGlvbnMudGVtcGxhdGUgPyBvcHRpb25zLnRlbXBsYXRlKGl0ZW0pIDogaXRlbTtcbiAgICAgICAgICB2YXIgb3B0aW9uID0gKHR5cGVvZiBpdGVtID09PSAnb2JqZWN0JykgPyBpdGVtLm9wdGlvbiA6IHVuZGVmaW5lZDtcbiAgICAgICAgICB2YXIgZGF0YVZhbHVlID0gJ2RhdGEtdmFsdWU9XCInICsgdmFsdWUgKyAnXCInO1xuICAgICAgICAgIHZhciBkYXRhT3B0aW9uID0gKG9wdGlvbiAhPT0gdW5kZWZpbmVkKSA/ICcgZGF0YS1vcHRpb249XCInICsgb3B0aW9uICsgJ1wiJyA6ICcnO1xuICAgICAgICAgIHJldHVybiAnPGxpIHJvbGU9XCJsaXN0aXRlbVwiIGFyaWEtbGFiZWw9XCInICsgdmFsdWUgKyAnXCI+PGEgaHJlZj1cIiNcIiAnICsgKGRhdGFWYWx1ZSArIGRhdGFPcHRpb24pICsgJz4nICsgY29udGVudCArICc8L2E+PC9saT4nO1xuICAgICAgfSkuam9pbignJykgOiBvcHRpb25zLml0ZW1zO1xuICAgICAgJG5vZGUuaHRtbChtYXJrdXApLmF0dHIoeyAnYXJpYS1sYWJlbCc6IG9wdGlvbnMudGl0bGUgfSk7XG4gIH0pO1xuICB2YXIgZHJvcGRvd25CdXR0b25Db250ZW50cyA9IGZ1bmN0aW9uIChjb250ZW50cywgb3B0aW9ucykge1xuICAgICAgcmV0dXJuIGNvbnRlbnRzICsgJyAnICsgaWNvbihvcHRpb25zLmljb25zLmNhcmV0LCAnc3BhbicpO1xuICB9O1xuICB2YXIgZHJvcGRvd25DaGVjayA9IHJlbmRlcmVyLmNyZWF0ZSgnPHVsIGNsYXNzPVwiZHJvcGRvd24tbWVudSBub3RlLWNoZWNrXCIgcm9sZT1cImxpc3RcIj4nLCBmdW5jdGlvbiAoJG5vZGUsIG9wdGlvbnMpIHtcbiAgICAgIHZhciBtYXJrdXAgPSBBcnJheS5pc0FycmF5KG9wdGlvbnMuaXRlbXMpID8gb3B0aW9ucy5pdGVtcy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICB2YXIgdmFsdWUgPSAodHlwZW9mIGl0ZW0gPT09ICdzdHJpbmcnKSA/IGl0ZW0gOiAoaXRlbS52YWx1ZSB8fCAnJyk7XG4gICAgICAgICAgdmFyIGNvbnRlbnQgPSBvcHRpb25zLnRlbXBsYXRlID8gb3B0aW9ucy50ZW1wbGF0ZShpdGVtKSA6IGl0ZW07XG4gICAgICAgICAgcmV0dXJuICc8bGkgcm9sZT1cImxpc3RpdGVtXCIgYXJpYS1sYWJlbD1cIicgKyBpdGVtICsgJ1wiPjxhIGhyZWY9XCIjXCIgZGF0YS12YWx1ZT1cIicgKyB2YWx1ZSArICdcIj4nICsgaWNvbihvcHRpb25zLmNoZWNrQ2xhc3NOYW1lKSArICcgJyArIGNvbnRlbnQgKyAnPC9hPjwvbGk+JztcbiAgICAgIH0pLmpvaW4oJycpIDogb3B0aW9ucy5pdGVtcztcbiAgICAgICRub2RlLmh0bWwobWFya3VwKS5hdHRyKHsgJ2FyaWEtbGFiZWwnOiBvcHRpb25zLnRpdGxlIH0pO1xuICB9KTtcbiAgdmFyIHBhbGV0dGUgPSByZW5kZXJlci5jcmVhdGUoJzxkaXYgY2xhc3M9XCJub3RlLWNvbG9yLXBhbGV0dGVcIi8+JywgZnVuY3Rpb24gKCRub2RlLCBvcHRpb25zKSB7XG4gICAgICB2YXIgY29udGVudHMgPSBbXTtcbiAgICAgIGZvciAodmFyIHJvdyA9IDAsIHJvd1NpemUgPSBvcHRpb25zLmNvbG9ycy5sZW5ndGg7IHJvdyA8IHJvd1NpemU7IHJvdysrKSB7XG4gICAgICAgICAgdmFyIGV2ZW50TmFtZSA9IG9wdGlvbnMuZXZlbnROYW1lO1xuICAgICAgICAgIHZhciBjb2xvcnMgPSBvcHRpb25zLmNvbG9yc1tyb3ddO1xuICAgICAgICAgIHZhciBjb2xvcnNOYW1lID0gb3B0aW9ucy5jb2xvcnNOYW1lW3Jvd107XG4gICAgICAgICAgdmFyIGJ1dHRvbnMgPSBbXTtcbiAgICAgICAgICBmb3IgKHZhciBjb2wgPSAwLCBjb2xTaXplID0gY29sb3JzLmxlbmd0aDsgY29sIDwgY29sU2l6ZTsgY29sKyspIHtcbiAgICAgICAgICAgICAgdmFyIGNvbG9yID0gY29sb3JzW2NvbF07XG4gICAgICAgICAgICAgIHZhciBjb2xvck5hbWUgPSBjb2xvcnNOYW1lW2NvbF07XG4gICAgICAgICAgICAgIGJ1dHRvbnMucHVzaChbXG4gICAgICAgICAgICAgICAgICAnPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJub3RlLWNvbG9yLWJ0blwiJyxcbiAgICAgICAgICAgICAgICAgICdzdHlsZT1cImJhY2tncm91bmQtY29sb3I6JywgY29sb3IsICdcIiAnLFxuICAgICAgICAgICAgICAgICAgJ2RhdGEtZXZlbnQ9XCInLCBldmVudE5hbWUsICdcIiAnLFxuICAgICAgICAgICAgICAgICAgJ2RhdGEtdmFsdWU9XCInLCBjb2xvciwgJ1wiICcsXG4gICAgICAgICAgICAgICAgICAndGl0bGU9XCInLCBjb2xvck5hbWUsICdcIiAnLFxuICAgICAgICAgICAgICAgICAgJ2FyaWEtbGFiZWw9XCInLCBjb2xvck5hbWUsICdcIiAnLFxuICAgICAgICAgICAgICAgICAgJ2RhdGEtdG9nZ2xlPVwiYnV0dG9uXCIgdGFiaW5kZXg9XCItMVwiPjwvYnV0dG9uPicsXG4gICAgICAgICAgICAgIF0uam9pbignJykpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjb250ZW50cy5wdXNoKCc8ZGl2IGNsYXNzPVwibm90ZS1jb2xvci1yb3dcIj4nICsgYnV0dG9ucy5qb2luKCcnKSArICc8L2Rpdj4nKTtcbiAgICAgIH1cbiAgICAgICRub2RlLmh0bWwoY29udGVudHMuam9pbignJykpO1xuICAgICAgaWYgKG9wdGlvbnMudG9vbHRpcCkge1xuICAgICAgICAgICRub2RlLmZpbmQoJy5ub3RlLWNvbG9yLWJ0bicpLnRvb2x0aXAoe1xuICAgICAgICAgICAgICBjb250YWluZXI6IG9wdGlvbnMuY29udGFpbmVyLFxuICAgICAgICAgICAgICB0cmlnZ2VyOiAnaG92ZXInLFxuICAgICAgICAgICAgICBwbGFjZW1lbnQ6ICdib3R0b20nXG4gICAgICAgICAgfSk7XG4gICAgICB9XG4gIH0pO1xuICB2YXIgZGlhbG9nID0gcmVuZGVyZXIuY3JlYXRlKCc8ZGl2IGNsYXNzPVwibW9kYWxcIiBhcmlhLWhpZGRlbj1cImZhbHNlXCIgdGFiaW5kZXg9XCItMVwiIHJvbGU9XCJkaWFsb2dcIi8+JywgZnVuY3Rpb24gKCRub2RlLCBvcHRpb25zKSB7XG4gICAgICBpZiAob3B0aW9ucy5mYWRlKSB7XG4gICAgICAgICAgJG5vZGUuYWRkQ2xhc3MoJ2ZhZGUnKTtcbiAgICAgIH1cbiAgICAgICRub2RlLmF0dHIoe1xuICAgICAgICAgICdhcmlhLWxhYmVsJzogb3B0aW9ucy50aXRsZVxuICAgICAgfSk7XG4gICAgICAkbm9kZS5odG1sKFtcbiAgICAgICAgICAnPGRpdiBjbGFzcz1cIm1vZGFsLWRpYWxvZ1wiPicsXG4gICAgICAgICAgJyAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj4nLFxuICAgICAgICAgIChvcHRpb25zLnRpdGxlXG4gICAgICAgICAgICAgID8gJyAgICA8ZGl2IGNsYXNzPVwibW9kYWwtaGVhZGVyXCI+JyArXG4gICAgICAgICAgICAgICAgICAnICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIGRhdGEtZGlzbWlzcz1cIm1vZGFsXCIgYXJpYS1sYWJlbD1cIkNsb3NlXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+JnRpbWVzOzwvYnV0dG9uPicgK1xuICAgICAgICAgICAgICAgICAgJyAgICAgIDxoNCBjbGFzcz1cIm1vZGFsLXRpdGxlXCI+JyArIG9wdGlvbnMudGl0bGUgKyAnPC9oND4nICtcbiAgICAgICAgICAgICAgICAgICcgICAgPC9kaXY+JyA6ICcnKSxcbiAgICAgICAgICAnICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1ib2R5XCI+JyArIG9wdGlvbnMuYm9keSArICc8L2Rpdj4nLFxuICAgICAgICAgIChvcHRpb25zLmZvb3RlclxuICAgICAgICAgICAgICA/ICcgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWZvb3RlclwiPicgKyBvcHRpb25zLmZvb3RlciArICc8L2Rpdj4nIDogJycpLFxuICAgICAgICAgICcgIDwvZGl2PicsXG4gICAgICAgICAgJzwvZGl2PicsXG4gICAgICBdLmpvaW4oJycpKTtcbiAgfSk7XG4gIHZhciBwb3BvdmVyID0gcmVuZGVyZXIuY3JlYXRlKFtcbiAgICAgICc8ZGl2IGNsYXNzPVwibm90ZS1wb3BvdmVyIHBvcG92ZXIgaW5cIj4nLFxuICAgICAgJyAgPGRpdiBjbGFzcz1cImFycm93XCIvPicsXG4gICAgICAnICA8ZGl2IGNsYXNzPVwicG9wb3Zlci1jb250ZW50IG5vdGUtY2hpbGRyZW4tY29udGFpbmVyXCIvPicsXG4gICAgICAnPC9kaXY+JyxcbiAgXS5qb2luKCcnKSwgZnVuY3Rpb24gKCRub2RlLCBvcHRpb25zKSB7XG4gICAgICB2YXIgZGlyZWN0aW9uID0gdHlwZW9mIG9wdGlvbnMuZGlyZWN0aW9uICE9PSAndW5kZWZpbmVkJyA/IG9wdGlvbnMuZGlyZWN0aW9uIDogJ2JvdHRvbSc7XG4gICAgICAkbm9kZS5hZGRDbGFzcyhkaXJlY3Rpb24pO1xuICAgICAgaWYgKG9wdGlvbnMuaGlkZUFycm93KSB7XG4gICAgICAgICAgJG5vZGUuZmluZCgnLmFycm93JykuaGlkZSgpO1xuICAgICAgfVxuICB9KTtcbiAgdmFyIGNoZWNrYm94ID0gcmVuZGVyZXIuY3JlYXRlKCc8ZGl2IGNsYXNzPVwiY2hlY2tib3hcIj48L2Rpdj4nLCBmdW5jdGlvbiAoJG5vZGUsIG9wdGlvbnMpIHtcbiAgICAgICRub2RlLmh0bWwoW1xuICAgICAgICAgICc8bGFiZWwnICsgKG9wdGlvbnMuaWQgPyAnIGZvcj1cIicgKyBvcHRpb25zLmlkICsgJ1wiJyA6ICcnKSArICc+JyxcbiAgICAgICAgICAnIDxpbnB1dCByb2xlPVwiY2hlY2tib3hcIiB0eXBlPVwiY2hlY2tib3hcIicgKyAob3B0aW9ucy5pZCA/ICcgaWQ9XCInICsgb3B0aW9ucy5pZCArICdcIicgOiAnJyksXG4gICAgICAgICAgKG9wdGlvbnMuY2hlY2tlZCA/ICcgY2hlY2tlZCcgOiAnJyksXG4gICAgICAgICAgJyBhcmlhLWNoZWNrZWQ9XCInICsgKG9wdGlvbnMuY2hlY2tlZCA/ICd0cnVlJyA6ICdmYWxzZScpICsgJ1wiLz4nLFxuICAgICAgICAgIChvcHRpb25zLnRleHQgPyBvcHRpb25zLnRleHQgOiAnJyksXG4gICAgICAgICAgJzwvbGFiZWw+JyxcbiAgICAgIF0uam9pbignJykpO1xuICB9KTtcbiAgdmFyIGljb24gPSBmdW5jdGlvbiAoaWNvbkNsYXNzTmFtZSwgdGFnTmFtZSkge1xuICAgICAgdGFnTmFtZSA9IHRhZ05hbWUgfHwgJ2knO1xuICAgICAgcmV0dXJuICc8JyArIHRhZ05hbWUgKyAnIGNsYXNzPVwiJyArIGljb25DbGFzc05hbWUgKyAnXCIvPic7XG4gIH07XG4gIHZhciB1aSA9IHtcbiAgICAgIGVkaXRvcjogZWRpdG9yLFxuICAgICAgdG9vbGJhcjogdG9vbGJhcixcbiAgICAgIGVkaXRpbmdBcmVhOiBlZGl0aW5nQXJlYSxcbiAgICAgIGNvZGFibGU6IGNvZGFibGUsXG4gICAgICBlZGl0YWJsZTogZWRpdGFibGUsXG4gICAgICBzdGF0dXNiYXI6IHN0YXR1c2JhcixcbiAgICAgIGFpckVkaXRvcjogYWlyRWRpdG9yLFxuICAgICAgYWlyRWRpdGFibGU6IGFpckVkaXRhYmxlLFxuICAgICAgYnV0dG9uR3JvdXA6IGJ1dHRvbkdyb3VwLFxuICAgICAgZHJvcGRvd246IGRyb3Bkb3duLFxuICAgICAgZHJvcGRvd25CdXR0b25Db250ZW50czogZHJvcGRvd25CdXR0b25Db250ZW50cyxcbiAgICAgIGRyb3Bkb3duQ2hlY2s6IGRyb3Bkb3duQ2hlY2ssXG4gICAgICBwYWxldHRlOiBwYWxldHRlLFxuICAgICAgZGlhbG9nOiBkaWFsb2csXG4gICAgICBwb3BvdmVyOiBwb3BvdmVyLFxuICAgICAgY2hlY2tib3g6IGNoZWNrYm94LFxuICAgICAgaWNvbjogaWNvbixcbiAgICAgIG9wdGlvbnM6IHt9LFxuICAgICAgYnV0dG9uOiBmdW5jdGlvbiAoJG5vZGUsIG9wdGlvbnMpIHtcbiAgICAgICAgICByZXR1cm4gcmVuZGVyZXIuY3JlYXRlKCc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cIm5vdGUtYnRuIGJ0biBidG4tZGVmYXVsdCBidG4tc21cIiByb2xlPVwiYnV0dG9uXCIgdGFiaW5kZXg9XCItMVwiPicsIGZ1bmN0aW9uICgkbm9kZSwgb3B0aW9ucykge1xuICAgICAgICAgICAgICBpZiAob3B0aW9ucyAmJiBvcHRpb25zLnRvb2x0aXApIHtcbiAgICAgICAgICAgICAgICAgICRub2RlLmF0dHIoe1xuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBvcHRpb25zLnRvb2x0aXAsXG4gICAgICAgICAgICAgICAgICAgICAgJ2FyaWEtbGFiZWwnOiBvcHRpb25zLnRvb2x0aXBcbiAgICAgICAgICAgICAgICAgIH0pLnRvb2x0aXAoe1xuICAgICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogKG9wdGlvbnMuY29udGFpbmVyICE9PSB1bmRlZmluZWQpID8gb3B0aW9ucy5jb250YWluZXIgOiAnYm9keScsXG4gICAgICAgICAgICAgICAgICAgICAgdHJpZ2dlcjogJ2hvdmVyJyxcbiAgICAgICAgICAgICAgICAgICAgICBwbGFjZW1lbnQ6ICdib3R0b20nXG4gICAgICAgICAgICAgICAgICB9KS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICAgICQkMShlLmN1cnJlbnRUYXJnZXQpLnRvb2x0aXAoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSkoJG5vZGUsIG9wdGlvbnMpO1xuICAgICAgfSxcbiAgICAgIHRvZ2dsZUJ0bjogZnVuY3Rpb24gKCRidG4sIGlzRW5hYmxlKSB7XG4gICAgICAgICAgJGJ0bi50b2dnbGVDbGFzcygnZGlzYWJsZWQnLCAhaXNFbmFibGUpO1xuICAgICAgICAgICRidG4uYXR0cignZGlzYWJsZWQnLCAhaXNFbmFibGUpO1xuICAgICAgfSxcbiAgICAgIHRvZ2dsZUJ0bkFjdGl2ZTogZnVuY3Rpb24gKCRidG4sIGlzQWN0aXZlKSB7XG4gICAgICAgICAgJGJ0bi50b2dnbGVDbGFzcygnYWN0aXZlJywgaXNBY3RpdmUpO1xuICAgICAgfSxcbiAgICAgIG9uRGlhbG9nU2hvd246IGZ1bmN0aW9uICgkZGlhbG9nLCBoYW5kbGVyKSB7XG4gICAgICAgICAgJGRpYWxvZy5vbmUoJ3Nob3duLmJzLm1vZGFsJywgaGFuZGxlcik7XG4gICAgICB9LFxuICAgICAgb25EaWFsb2dIaWRkZW46IGZ1bmN0aW9uICgkZGlhbG9nLCBoYW5kbGVyKSB7XG4gICAgICAgICAgJGRpYWxvZy5vbmUoJ2hpZGRlbi5icy5tb2RhbCcsIGhhbmRsZXIpO1xuICAgICAgfSxcbiAgICAgIHNob3dEaWFsb2c6IGZ1bmN0aW9uICgkZGlhbG9nKSB7XG4gICAgICAgICAgJGRpYWxvZy5tb2RhbCgnc2hvdycpO1xuICAgICAgfSxcbiAgICAgIGhpZGVEaWFsb2c6IGZ1bmN0aW9uICgkZGlhbG9nKSB7XG4gICAgICAgICAgJGRpYWxvZy5tb2RhbCgnaGlkZScpO1xuICAgICAgfSxcbiAgICAgIGNyZWF0ZUxheW91dDogZnVuY3Rpb24gKCRub3RlLCBvcHRpb25zKSB7XG4gICAgICAgICAgdmFyICRlZGl0b3IgPSAob3B0aW9ucy5haXJNb2RlID8gdWkuYWlyRWRpdG9yKFtcbiAgICAgICAgICAgICAgdWkuZWRpdGluZ0FyZWEoW1xuICAgICAgICAgICAgICAgICAgdWkuYWlyRWRpdGFibGUoKSxcbiAgICAgICAgICAgICAgXSksXG4gICAgICAgICAgXSkgOiB1aS5lZGl0b3IoW1xuICAgICAgICAgICAgICB1aS50b29sYmFyKCksXG4gICAgICAgICAgICAgIHVpLmVkaXRpbmdBcmVhKFtcbiAgICAgICAgICAgICAgICAgIHVpLmNvZGFibGUoKSxcbiAgICAgICAgICAgICAgICAgIHVpLmVkaXRhYmxlKCksXG4gICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICB1aS5zdGF0dXNiYXIoKSxcbiAgICAgICAgICBdKSkucmVuZGVyKCk7XG4gICAgICAgICAgJGVkaXRvci5pbnNlcnRBZnRlcigkbm90ZSk7XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgbm90ZTogJG5vdGUsXG4gICAgICAgICAgICAgIGVkaXRvcjogJGVkaXRvcixcbiAgICAgICAgICAgICAgdG9vbGJhcjogJGVkaXRvci5maW5kKCcubm90ZS10b29sYmFyJyksXG4gICAgICAgICAgICAgIGVkaXRpbmdBcmVhOiAkZWRpdG9yLmZpbmQoJy5ub3RlLWVkaXRpbmctYXJlYScpLFxuICAgICAgICAgICAgICBlZGl0YWJsZTogJGVkaXRvci5maW5kKCcubm90ZS1lZGl0YWJsZScpLFxuICAgICAgICAgICAgICBjb2RhYmxlOiAkZWRpdG9yLmZpbmQoJy5ub3RlLWNvZGFibGUnKSxcbiAgICAgICAgICAgICAgc3RhdHVzYmFyOiAkZWRpdG9yLmZpbmQoJy5ub3RlLXN0YXR1c2JhcicpXG4gICAgICAgICAgfTtcbiAgICAgIH0sXG4gICAgICByZW1vdmVMYXlvdXQ6IGZ1bmN0aW9uICgkbm90ZSwgbGF5b3V0SW5mbykge1xuICAgICAgICAgICRub3RlLmh0bWwobGF5b3V0SW5mby5lZGl0YWJsZS5odG1sKCkpO1xuICAgICAgICAgIGxheW91dEluZm8uZWRpdG9yLnJlbW92ZSgpO1xuICAgICAgICAgICRub3RlLnNob3coKTtcbiAgICAgIH1cbiAgfTtcblxuICAkJDEuc3VtbWVybm90ZSA9ICQkMS5zdW1tZXJub3RlIHx8IHtcbiAgICAgIGxhbmc6IHt9XG4gIH07XG4gICQkMS5leHRlbmQoJCQxLnN1bW1lcm5vdGUubGFuZywge1xuICAgICAgJ2VuLVVTJzoge1xuICAgICAgICAgIGZvbnQ6IHtcbiAgICAgICAgICAgICAgYm9sZDogJ0JvbGQnLFxuICAgICAgICAgICAgICBpdGFsaWM6ICdJdGFsaWMnLFxuICAgICAgICAgICAgICB1bmRlcmxpbmU6ICdVbmRlcmxpbmUnLFxuICAgICAgICAgICAgICBjbGVhcjogJ1JlbW92ZSBGb250IFN0eWxlJyxcbiAgICAgICAgICAgICAgaGVpZ2h0OiAnTGluZSBIZWlnaHQnLFxuICAgICAgICAgICAgICBuYW1lOiAnRm9udCBGYW1pbHknLFxuICAgICAgICAgICAgICBzdHJpa2V0aHJvdWdoOiAnU3RyaWtldGhyb3VnaCcsXG4gICAgICAgICAgICAgIHN1YnNjcmlwdDogJ1N1YnNjcmlwdCcsXG4gICAgICAgICAgICAgIHN1cGVyc2NyaXB0OiAnU3VwZXJzY3JpcHQnLFxuICAgICAgICAgICAgICBzaXplOiAnRm9udCBTaXplJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaW1hZ2U6IHtcbiAgICAgICAgICAgICAgaW1hZ2U6ICdQaWN0dXJlJyxcbiAgICAgICAgICAgICAgaW5zZXJ0OiAnSW5zZXJ0IEltYWdlJyxcbiAgICAgICAgICAgICAgcmVzaXplRnVsbDogJ1Jlc2l6ZSBmdWxsJyxcbiAgICAgICAgICAgICAgcmVzaXplSGFsZjogJ1Jlc2l6ZSBoYWxmJyxcbiAgICAgICAgICAgICAgcmVzaXplUXVhcnRlcjogJ1Jlc2l6ZSBxdWFydGVyJyxcbiAgICAgICAgICAgICAgcmVzaXplTm9uZTogJ09yaWdpbmFsIHNpemUnLFxuICAgICAgICAgICAgICBmbG9hdExlZnQ6ICdGbG9hdCBMZWZ0JyxcbiAgICAgICAgICAgICAgZmxvYXRSaWdodDogJ0Zsb2F0IFJpZ2h0JyxcbiAgICAgICAgICAgICAgZmxvYXROb25lOiAnUmVtb3ZlIGZsb2F0JyxcbiAgICAgICAgICAgICAgc2hhcGVSb3VuZGVkOiAnU2hhcGU6IFJvdW5kZWQnLFxuICAgICAgICAgICAgICBzaGFwZUNpcmNsZTogJ1NoYXBlOiBDaXJjbGUnLFxuICAgICAgICAgICAgICBzaGFwZVRodW1ibmFpbDogJ1NoYXBlOiBUaHVtYm5haWwnLFxuICAgICAgICAgICAgICBzaGFwZU5vbmU6ICdTaGFwZTogTm9uZScsXG4gICAgICAgICAgICAgIGRyYWdJbWFnZUhlcmU6ICdEcmFnIGltYWdlIG9yIHRleHQgaGVyZScsXG4gICAgICAgICAgICAgIGRyb3BJbWFnZTogJ0Ryb3AgaW1hZ2Ugb3IgVGV4dCcsXG4gICAgICAgICAgICAgIHNlbGVjdEZyb21GaWxlczogJ1NlbGVjdCBmcm9tIGZpbGVzJyxcbiAgICAgICAgICAgICAgbWF4aW11bUZpbGVTaXplOiAnTWF4aW11bSBmaWxlIHNpemUnLFxuICAgICAgICAgICAgICBtYXhpbXVtRmlsZVNpemVFcnJvcjogJ01heGltdW0gZmlsZSBzaXplIGV4Y2VlZGVkLicsXG4gICAgICAgICAgICAgIHVybDogJ0ltYWdlIFVSTCcsXG4gICAgICAgICAgICAgIHJlbW92ZTogJ1JlbW92ZSBJbWFnZScsXG4gICAgICAgICAgICAgIG9yaWdpbmFsOiAnT3JpZ2luYWwnXG4gICAgICAgICAgfSxcbiAgICAgICAgICB2aWRlbzoge1xuICAgICAgICAgICAgICB2aWRlbzogJ1ZpZGVvJyxcbiAgICAgICAgICAgICAgdmlkZW9MaW5rOiAnVmlkZW8gTGluaycsXG4gICAgICAgICAgICAgIGluc2VydDogJ0luc2VydCBWaWRlbycsXG4gICAgICAgICAgICAgIHVybDogJ1ZpZGVvIFVSTCcsXG4gICAgICAgICAgICAgIHByb3ZpZGVyczogJyhZb3VUdWJlLCBWaW1lbywgVmluZSwgSW5zdGFncmFtLCBEYWlseU1vdGlvbiBvciBZb3VrdSknXG4gICAgICAgICAgfSxcbiAgICAgICAgICBsaW5rOiB7XG4gICAgICAgICAgICAgIGxpbms6ICdMaW5rJyxcbiAgICAgICAgICAgICAgaW5zZXJ0OiAnSW5zZXJ0IExpbmsnLFxuICAgICAgICAgICAgICB1bmxpbms6ICdVbmxpbmsnLFxuICAgICAgICAgICAgICBlZGl0OiAnRWRpdCcsXG4gICAgICAgICAgICAgIHRleHRUb0Rpc3BsYXk6ICdUZXh0IHRvIGRpc3BsYXknLFxuICAgICAgICAgICAgICB1cmw6ICdUbyB3aGF0IFVSTCBzaG91bGQgdGhpcyBsaW5rIGdvPycsXG4gICAgICAgICAgICAgIG9wZW5Jbk5ld1dpbmRvdzogJ09wZW4gaW4gbmV3IHdpbmRvdydcbiAgICAgICAgICB9LFxuICAgICAgICAgIHRhYmxlOiB7XG4gICAgICAgICAgICAgIHRhYmxlOiAnVGFibGUnLFxuICAgICAgICAgICAgICBhZGRSb3dBYm92ZTogJ0FkZCByb3cgYWJvdmUnLFxuICAgICAgICAgICAgICBhZGRSb3dCZWxvdzogJ0FkZCByb3cgYmVsb3cnLFxuICAgICAgICAgICAgICBhZGRDb2xMZWZ0OiAnQWRkIGNvbHVtbiBsZWZ0JyxcbiAgICAgICAgICAgICAgYWRkQ29sUmlnaHQ6ICdBZGQgY29sdW1uIHJpZ2h0JyxcbiAgICAgICAgICAgICAgZGVsUm93OiAnRGVsZXRlIHJvdycsXG4gICAgICAgICAgICAgIGRlbENvbDogJ0RlbGV0ZSBjb2x1bW4nLFxuICAgICAgICAgICAgICBkZWxUYWJsZTogJ0RlbGV0ZSB0YWJsZSdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGhyOiB7XG4gICAgICAgICAgICAgIGluc2VydDogJ0luc2VydCBIb3Jpem9udGFsIFJ1bGUnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBzdHlsZToge1xuICAgICAgICAgICAgICBzdHlsZTogJ1N0eWxlJyxcbiAgICAgICAgICAgICAgcDogJ05vcm1hbCcsXG4gICAgICAgICAgICAgIGJsb2NrcXVvdGU6ICdRdW90ZScsXG4gICAgICAgICAgICAgIHByZTogJ0NvZGUnLFxuICAgICAgICAgICAgICBoMTogJ0hlYWRlciAxJyxcbiAgICAgICAgICAgICAgaDI6ICdIZWFkZXIgMicsXG4gICAgICAgICAgICAgIGgzOiAnSGVhZGVyIDMnLFxuICAgICAgICAgICAgICBoNDogJ0hlYWRlciA0JyxcbiAgICAgICAgICAgICAgaDU6ICdIZWFkZXIgNScsXG4gICAgICAgICAgICAgIGg2OiAnSGVhZGVyIDYnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBsaXN0czoge1xuICAgICAgICAgICAgICB1bm9yZGVyZWQ6ICdVbm9yZGVyZWQgbGlzdCcsXG4gICAgICAgICAgICAgIG9yZGVyZWQ6ICdPcmRlcmVkIGxpc3QnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgICAgIGhlbHA6ICdIZWxwJyxcbiAgICAgICAgICAgICAgZnVsbHNjcmVlbjogJ0Z1bGwgU2NyZWVuJyxcbiAgICAgICAgICAgICAgY29kZXZpZXc6ICdDb2RlIFZpZXcnXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwYXJhZ3JhcGg6IHtcbiAgICAgICAgICAgICAgcGFyYWdyYXBoOiAnUGFyYWdyYXBoJyxcbiAgICAgICAgICAgICAgb3V0ZGVudDogJ091dGRlbnQnLFxuICAgICAgICAgICAgICBpbmRlbnQ6ICdJbmRlbnQnLFxuICAgICAgICAgICAgICBsZWZ0OiAnQWxpZ24gbGVmdCcsXG4gICAgICAgICAgICAgIGNlbnRlcjogJ0FsaWduIGNlbnRlcicsXG4gICAgICAgICAgICAgIHJpZ2h0OiAnQWxpZ24gcmlnaHQnLFxuICAgICAgICAgICAgICBqdXN0aWZ5OiAnSnVzdGlmeSBmdWxsJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAgY29sb3I6IHtcbiAgICAgICAgICAgICAgcmVjZW50OiAnUmVjZW50IENvbG9yJyxcbiAgICAgICAgICAgICAgbW9yZTogJ01vcmUgQ29sb3InLFxuICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAnQmFja2dyb3VuZCBDb2xvcicsXG4gICAgICAgICAgICAgIGZvcmVncm91bmQ6ICdGb3JlZ3JvdW5kIENvbG9yJyxcbiAgICAgICAgICAgICAgdHJhbnNwYXJlbnQ6ICdUcmFuc3BhcmVudCcsXG4gICAgICAgICAgICAgIHNldFRyYW5zcGFyZW50OiAnU2V0IHRyYW5zcGFyZW50JyxcbiAgICAgICAgICAgICAgcmVzZXQ6ICdSZXNldCcsXG4gICAgICAgICAgICAgIHJlc2V0VG9EZWZhdWx0OiAnUmVzZXQgdG8gZGVmYXVsdCcsXG4gICAgICAgICAgICAgIGNwU2VsZWN0OiAnU2VsZWN0J1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc2hvcnRjdXQ6IHtcbiAgICAgICAgICAgICAgc2hvcnRjdXRzOiAnS2V5Ym9hcmQgc2hvcnRjdXRzJyxcbiAgICAgICAgICAgICAgY2xvc2U6ICdDbG9zZScsXG4gICAgICAgICAgICAgIHRleHRGb3JtYXR0aW5nOiAnVGV4dCBmb3JtYXR0aW5nJyxcbiAgICAgICAgICAgICAgYWN0aW9uOiAnQWN0aW9uJyxcbiAgICAgICAgICAgICAgcGFyYWdyYXBoRm9ybWF0dGluZzogJ1BhcmFncmFwaCBmb3JtYXR0aW5nJyxcbiAgICAgICAgICAgICAgZG9jdW1lbnRTdHlsZTogJ0RvY3VtZW50IFN0eWxlJyxcbiAgICAgICAgICAgICAgZXh0cmFLZXlzOiAnRXh0cmEga2V5cydcbiAgICAgICAgICB9LFxuICAgICAgICAgIGhlbHA6IHtcbiAgICAgICAgICAgICAgJ2luc2VydFBhcmFncmFwaCc6ICdJbnNlcnQgUGFyYWdyYXBoJyxcbiAgICAgICAgICAgICAgJ3VuZG8nOiAnVW5kb2VzIHRoZSBsYXN0IGNvbW1hbmQnLFxuICAgICAgICAgICAgICAncmVkbyc6ICdSZWRvZXMgdGhlIGxhc3QgY29tbWFuZCcsXG4gICAgICAgICAgICAgICd0YWInOiAnVGFiJyxcbiAgICAgICAgICAgICAgJ3VudGFiJzogJ1VudGFiJyxcbiAgICAgICAgICAgICAgJ2JvbGQnOiAnU2V0IGEgYm9sZCBzdHlsZScsXG4gICAgICAgICAgICAgICdpdGFsaWMnOiAnU2V0IGEgaXRhbGljIHN0eWxlJyxcbiAgICAgICAgICAgICAgJ3VuZGVybGluZSc6ICdTZXQgYSB1bmRlcmxpbmUgc3R5bGUnLFxuICAgICAgICAgICAgICAnc3RyaWtldGhyb3VnaCc6ICdTZXQgYSBzdHJpa2V0aHJvdWdoIHN0eWxlJyxcbiAgICAgICAgICAgICAgJ3JlbW92ZUZvcm1hdCc6ICdDbGVhbiBhIHN0eWxlJyxcbiAgICAgICAgICAgICAgJ2p1c3RpZnlMZWZ0JzogJ1NldCBsZWZ0IGFsaWduJyxcbiAgICAgICAgICAgICAgJ2p1c3RpZnlDZW50ZXInOiAnU2V0IGNlbnRlciBhbGlnbicsXG4gICAgICAgICAgICAgICdqdXN0aWZ5UmlnaHQnOiAnU2V0IHJpZ2h0IGFsaWduJyxcbiAgICAgICAgICAgICAgJ2p1c3RpZnlGdWxsJzogJ1NldCBmdWxsIGFsaWduJyxcbiAgICAgICAgICAgICAgJ2luc2VydFVub3JkZXJlZExpc3QnOiAnVG9nZ2xlIHVub3JkZXJlZCBsaXN0JyxcbiAgICAgICAgICAgICAgJ2luc2VydE9yZGVyZWRMaXN0JzogJ1RvZ2dsZSBvcmRlcmVkIGxpc3QnLFxuICAgICAgICAgICAgICAnb3V0ZGVudCc6ICdPdXRkZW50IG9uIGN1cnJlbnQgcGFyYWdyYXBoJyxcbiAgICAgICAgICAgICAgJ2luZGVudCc6ICdJbmRlbnQgb24gY3VycmVudCBwYXJhZ3JhcGgnLFxuICAgICAgICAgICAgICAnZm9ybWF0UGFyYSc6ICdDaGFuZ2UgY3VycmVudCBibG9ja1xcJ3MgZm9ybWF0IGFzIGEgcGFyYWdyYXBoKFAgdGFnKScsXG4gICAgICAgICAgICAgICdmb3JtYXRIMSc6ICdDaGFuZ2UgY3VycmVudCBibG9ja1xcJ3MgZm9ybWF0IGFzIEgxJyxcbiAgICAgICAgICAgICAgJ2Zvcm1hdEgyJzogJ0NoYW5nZSBjdXJyZW50IGJsb2NrXFwncyBmb3JtYXQgYXMgSDInLFxuICAgICAgICAgICAgICAnZm9ybWF0SDMnOiAnQ2hhbmdlIGN1cnJlbnQgYmxvY2tcXCdzIGZvcm1hdCBhcyBIMycsXG4gICAgICAgICAgICAgICdmb3JtYXRINCc6ICdDaGFuZ2UgY3VycmVudCBibG9ja1xcJ3MgZm9ybWF0IGFzIEg0JyxcbiAgICAgICAgICAgICAgJ2Zvcm1hdEg1JzogJ0NoYW5nZSBjdXJyZW50IGJsb2NrXFwncyBmb3JtYXQgYXMgSDUnLFxuICAgICAgICAgICAgICAnZm9ybWF0SDYnOiAnQ2hhbmdlIGN1cnJlbnQgYmxvY2tcXCdzIGZvcm1hdCBhcyBINicsXG4gICAgICAgICAgICAgICdpbnNlcnRIb3Jpem9udGFsUnVsZSc6ICdJbnNlcnQgaG9yaXpvbnRhbCBydWxlJyxcbiAgICAgICAgICAgICAgJ2xpbmtEaWFsb2cuc2hvdyc6ICdTaG93IExpbmsgRGlhbG9nJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAgaGlzdG9yeToge1xuICAgICAgICAgICAgICB1bmRvOiAnVW5kbycsXG4gICAgICAgICAgICAgIHJlZG86ICdSZWRvJ1xuICAgICAgICAgIH0sXG4gICAgICAgICAgc3BlY2lhbENoYXI6IHtcbiAgICAgICAgICAgICAgc3BlY2lhbENoYXI6ICdTUEVDSUFMIENIQVJBQ1RFUlMnLFxuICAgICAgICAgICAgICBzZWxlY3Q6ICdTZWxlY3QgU3BlY2lhbCBjaGFyYWN0ZXJzJ1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfSk7XG5cbiAgdmFyIGlzU3VwcG9ydEFtZCA9IHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZDsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAvKipcbiAgICogcmV0dXJucyB3aGV0aGVyIGZvbnQgaXMgaW5zdGFsbGVkIG9yIG5vdC5cbiAgICpcbiAgICogQHBhcmFtIHtTdHJpbmd9IGZvbnROYW1lXG4gICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc0ZvbnRJbnN0YWxsZWQoZm9udE5hbWUpIHtcbiAgICAgIHZhciB0ZXN0Rm9udE5hbWUgPSBmb250TmFtZSA9PT0gJ0NvbWljIFNhbnMgTVMnID8gJ0NvdXJpZXIgTmV3JyA6ICdDb21pYyBTYW5zIE1TJztcbiAgICAgIHZhciB0ZXN0VGV4dCA9ICdtbW1tbW1tbW1td3d3d3cnO1xuICAgICAgdmFyIHRlc3RTaXplID0gJzIwMHB4JztcbiAgICAgIHZhciBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcbiAgICAgIHZhciBjb250ZXh0ID0gY2FudmFzLmdldENvbnRleHQoJzJkJyk7XG4gICAgICBjb250ZXh0LmZvbnQgPSB0ZXN0U2l6ZSArIFwiICdcIiArIHRlc3RGb250TmFtZSArIFwiJ1wiO1xuICAgICAgdmFyIG9yaWdpbmFsV2lkdGggPSBjb250ZXh0Lm1lYXN1cmVUZXh0KHRlc3RUZXh0KS53aWR0aDtcbiAgICAgIGNvbnRleHQuZm9udCA9IHRlc3RTaXplICsgXCIgJ1wiICsgZm9udE5hbWUgKyBcIicsICdcIiArIHRlc3RGb250TmFtZSArIFwiJ1wiO1xuICAgICAgdmFyIHdpZHRoID0gY29udGV4dC5tZWFzdXJlVGV4dCh0ZXN0VGV4dCkud2lkdGg7XG4gICAgICByZXR1cm4gb3JpZ2luYWxXaWR0aCAhPT0gd2lkdGg7XG4gIH1cbiAgdmFyIHVzZXJBZ2VudCA9IG5hdmlnYXRvci51c2VyQWdlbnQ7XG4gIHZhciBpc01TSUUgPSAvTVNJRXxUcmlkZW50L2kudGVzdCh1c2VyQWdlbnQpO1xuICB2YXIgYnJvd3NlclZlcnNpb247XG4gIGlmIChpc01TSUUpIHtcbiAgICAgIHZhciBtYXRjaGVzID0gL01TSUUgKFxcZCtbLl1cXGQrKS8uZXhlYyh1c2VyQWdlbnQpO1xuICAgICAgaWYgKG1hdGNoZXMpIHtcbiAgICAgICAgICBicm93c2VyVmVyc2lvbiA9IHBhcnNlRmxvYXQobWF0Y2hlc1sxXSk7XG4gICAgICB9XG4gICAgICBtYXRjaGVzID0gL1RyaWRlbnRcXC8uKnJ2OihbMC05XXsxLH1bLjAtOV17MCx9KS8uZXhlYyh1c2VyQWdlbnQpO1xuICAgICAgaWYgKG1hdGNoZXMpIHtcbiAgICAgICAgICBicm93c2VyVmVyc2lvbiA9IHBhcnNlRmxvYXQobWF0Y2hlc1sxXSk7XG4gICAgICB9XG4gIH1cbiAgdmFyIGlzRWRnZSA9IC9FZGdlXFwvXFxkKy8udGVzdCh1c2VyQWdlbnQpO1xuICB2YXIgaGFzQ29kZU1pcnJvciA9ICEhd2luZG93LkNvZGVNaXJyb3I7XG4gIHZhciBpc1N1cHBvcnRUb3VjaCA9ICgoJ29udG91Y2hzdGFydCcgaW4gd2luZG93KSB8fFxuICAgICAgKG5hdmlnYXRvci5NYXhUb3VjaFBvaW50cyA+IDApIHx8XG4gICAgICAobmF2aWdhdG9yLm1zTWF4VG91Y2hQb2ludHMgPiAwKSk7XG4gIC8vIFt3b3JrYXJvdW5kXSBJRSBkb2Vzbid0IGhhdmUgaW5wdXQgZXZlbnRzIGZvciBjb250ZW50RWRpdGFibGVcbiAgLy8gLSBzZWU6IGh0dHBzOi8vZ29vLmdsLzRiZkl2QVxuICB2YXIgaW5wdXRFdmVudE5hbWUgPSAoaXNNU0lFIHx8IGlzRWRnZSkgPyAnRE9NQ2hhcmFjdGVyRGF0YU1vZGlmaWVkIERPTVN1YnRyZWVNb2RpZmllZCBET01Ob2RlSW5zZXJ0ZWQnIDogJ2lucHV0JztcbiAgLyoqXG4gICAqIEBjbGFzcyBjb3JlLmVudlxuICAgKlxuICAgKiBPYmplY3Qgd2hpY2ggY2hlY2sgcGxhdGZvcm0gYW5kIGFnZW50XG4gICAqXG4gICAqIEBzaW5nbGV0b25cbiAgICogQGFsdGVybmF0ZUNsYXNzTmFtZSBlbnZcbiAgICovXG4gIHZhciBlbnYgPSB7XG4gICAgICBpc01hYzogbmF2aWdhdG9yLmFwcFZlcnNpb24uaW5kZXhPZignTWFjJykgPiAtMSxcbiAgICAgIGlzTVNJRTogaXNNU0lFLFxuICAgICAgaXNFZGdlOiBpc0VkZ2UsXG4gICAgICBpc0ZGOiAhaXNFZGdlICYmIC9maXJlZm94L2kudGVzdCh1c2VyQWdlbnQpLFxuICAgICAgaXNQaGFudG9tOiAvUGhhbnRvbUpTL2kudGVzdCh1c2VyQWdlbnQpLFxuICAgICAgaXNXZWJraXQ6ICFpc0VkZ2UgJiYgL3dlYmtpdC9pLnRlc3QodXNlckFnZW50KSxcbiAgICAgIGlzQ2hyb21lOiAhaXNFZGdlICYmIC9jaHJvbWUvaS50ZXN0KHVzZXJBZ2VudCksXG4gICAgICBpc1NhZmFyaTogIWlzRWRnZSAmJiAvc2FmYXJpL2kudGVzdCh1c2VyQWdlbnQpLFxuICAgICAgYnJvd3NlclZlcnNpb246IGJyb3dzZXJWZXJzaW9uLFxuICAgICAganF1ZXJ5VmVyc2lvbjogcGFyc2VGbG9hdCgkJDEuZm4uanF1ZXJ5KSxcbiAgICAgIGlzU3VwcG9ydEFtZDogaXNTdXBwb3J0QW1kLFxuICAgICAgaXNTdXBwb3J0VG91Y2g6IGlzU3VwcG9ydFRvdWNoLFxuICAgICAgaGFzQ29kZU1pcnJvcjogaGFzQ29kZU1pcnJvcixcbiAgICAgIGlzRm9udEluc3RhbGxlZDogaXNGb250SW5zdGFsbGVkLFxuICAgICAgaXNXM0NSYW5nZVN1cHBvcnQ6ICEhZG9jdW1lbnQuY3JlYXRlUmFuZ2UsXG4gICAgICBpbnB1dEV2ZW50TmFtZTogaW5wdXRFdmVudE5hbWVcbiAgfTtcblxuICAvKipcbiAgICogQGNsYXNzIGNvcmUuZnVuY1xuICAgKlxuICAgKiBmdW5jIHV0aWxzIChmb3IgaGlnaC1vcmRlciBmdW5jJ3MgYXJnKVxuICAgKlxuICAgKiBAc2luZ2xldG9uXG4gICAqIEBhbHRlcm5hdGVDbGFzc05hbWUgZnVuY1xuICAgKi9cbiAgZnVuY3Rpb24gZXEoaXRlbUEpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoaXRlbUIpIHtcbiAgICAgICAgICByZXR1cm4gaXRlbUEgPT09IGl0ZW1CO1xuICAgICAgfTtcbiAgfVxuICBmdW5jdGlvbiBlcTIoaXRlbUEsIGl0ZW1CKSB7XG4gICAgICByZXR1cm4gaXRlbUEgPT09IGl0ZW1CO1xuICB9XG4gIGZ1bmN0aW9uIHBlcTIocHJvcE5hbWUpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoaXRlbUEsIGl0ZW1CKSB7XG4gICAgICAgICAgcmV0dXJuIGl0ZW1BW3Byb3BOYW1lXSA9PT0gaXRlbUJbcHJvcE5hbWVdO1xuICAgICAgfTtcbiAgfVxuICBmdW5jdGlvbiBvaygpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICB9XG4gIGZ1bmN0aW9uIGZhaWwoKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gIH1cbiAgZnVuY3Rpb24gbm90KGYpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuICFmLmFwcGx5KGYsIGFyZ3VtZW50cyk7XG4gICAgICB9O1xuICB9XG4gIGZ1bmN0aW9uIGFuZChmQSwgZkIpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgIHJldHVybiBmQShpdGVtKSAmJiBmQihpdGVtKTtcbiAgICAgIH07XG4gIH1cbiAgZnVuY3Rpb24gc2VsZihhKSB7XG4gICAgICByZXR1cm4gYTtcbiAgfVxuICBmdW5jdGlvbiBpbnZva2Uob2JqLCBtZXRob2QpIHtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIG9ialttZXRob2RdLmFwcGx5KG9iaiwgYXJndW1lbnRzKTtcbiAgICAgIH07XG4gIH1cbiAgdmFyIGlkQ291bnRlciA9IDA7XG4gIC8qKlxuICAgKiBnZW5lcmF0ZSBhIGdsb2JhbGx5LXVuaXF1ZSBpZFxuICAgKlxuICAgKiBAcGFyYW0ge1N0cmluZ30gW3ByZWZpeF1cbiAgICovXG4gIGZ1bmN0aW9uIHVuaXF1ZUlkKHByZWZpeCkge1xuICAgICAgdmFyIGlkID0gKytpZENvdW50ZXIgKyAnJztcbiAgICAgIHJldHVybiBwcmVmaXggPyBwcmVmaXggKyBpZCA6IGlkO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIGJuZCAoYm91bmRzKSBmcm9tIHJlY3RcbiAgICpcbiAgICogLSBJRSBDb21wYXRpYmlsaXR5IElzc3VlOiBodHRwOi8vZ29vLmdsL3NSTE9Bb1xuICAgKiAtIFNjcm9sbCBJc3N1ZTogaHR0cDovL2dvby5nbC9zTmpVY1xuICAgKlxuICAgKiBAcGFyYW0ge1JlY3R9IHJlY3RcbiAgICogQHJldHVybiB7T2JqZWN0fSBib3VuZHNcbiAgICogQHJldHVybiB7TnVtYmVyfSBib3VuZHMudG9wXG4gICAqIEByZXR1cm4ge051bWJlcn0gYm91bmRzLmxlZnRcbiAgICogQHJldHVybiB7TnVtYmVyfSBib3VuZHMud2lkdGhcbiAgICogQHJldHVybiB7TnVtYmVyfSBib3VuZHMuaGVpZ2h0XG4gICAqL1xuICBmdW5jdGlvbiByZWN0MmJuZChyZWN0KSB7XG4gICAgICB2YXIgJGRvY3VtZW50ID0gJChkb2N1bWVudCk7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAgIHRvcDogcmVjdC50b3AgKyAkZG9jdW1lbnQuc2Nyb2xsVG9wKCksXG4gICAgICAgICAgbGVmdDogcmVjdC5sZWZ0ICsgJGRvY3VtZW50LnNjcm9sbExlZnQoKSxcbiAgICAgICAgICB3aWR0aDogcmVjdC5yaWdodCAtIHJlY3QubGVmdCxcbiAgICAgICAgICBoZWlnaHQ6IHJlY3QuYm90dG9tIC0gcmVjdC50b3BcbiAgICAgIH07XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgYSBjb3B5IG9mIHRoZSBvYmplY3Qgd2hlcmUgdGhlIGtleXMgaGF2ZSBiZWNvbWUgdGhlIHZhbHVlcyBhbmQgdGhlIHZhbHVlcyB0aGUga2V5cy5cbiAgICogQHBhcmFtIHtPYmplY3R9IG9ialxuICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAqL1xuICBmdW5jdGlvbiBpbnZlcnRPYmplY3Qob2JqKSB7XG4gICAgICB2YXIgaW52ZXJ0ZWQgPSB7fTtcbiAgICAgIGZvciAodmFyIGtleSBpbiBvYmopIHtcbiAgICAgICAgICBpZiAob2JqLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgaW52ZXJ0ZWRbb2JqW2tleV1dID0ga2V5O1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiBpbnZlcnRlZDtcbiAgfVxuICAvKipcbiAgICogQHBhcmFtIHtTdHJpbmd9IG5hbWVzcGFjZVxuICAgKiBAcGFyYW0ge1N0cmluZ30gW3ByZWZpeF1cbiAgICogQHJldHVybiB7U3RyaW5nfVxuICAgKi9cbiAgZnVuY3Rpb24gbmFtZXNwYWNlVG9DYW1lbChuYW1lc3BhY2UsIHByZWZpeCkge1xuICAgICAgcHJlZml4ID0gcHJlZml4IHx8ICcnO1xuICAgICAgcmV0dXJuIHByZWZpeCArIG5hbWVzcGFjZS5zcGxpdCgnLicpLm1hcChmdW5jdGlvbiAobmFtZSkge1xuICAgICAgICAgIHJldHVybiBuYW1lLnN1YnN0cmluZygwLCAxKS50b1VwcGVyQ2FzZSgpICsgbmFtZS5zdWJzdHJpbmcoMSk7XG4gICAgICB9KS5qb2luKCcnKTtcbiAgfVxuICAvKipcbiAgICogUmV0dXJucyBhIGZ1bmN0aW9uLCB0aGF0LCBhcyBsb25nIGFzIGl0IGNvbnRpbnVlcyB0byBiZSBpbnZva2VkLCB3aWxsIG5vdFxuICAgKiBiZSB0cmlnZ2VyZWQuIFRoZSBmdW5jdGlvbiB3aWxsIGJlIGNhbGxlZCBhZnRlciBpdCBzdG9wcyBiZWluZyBjYWxsZWQgZm9yXG4gICAqIE4gbWlsbGlzZWNvbmRzLiBJZiBgaW1tZWRpYXRlYCBpcyBwYXNzZWQsIHRyaWdnZXIgdGhlIGZ1bmN0aW9uIG9uIHRoZVxuICAgKiBsZWFkaW5nIGVkZ2UsIGluc3RlYWQgb2YgdGhlIHRyYWlsaW5nLlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBmdW5jXG4gICAqIEBwYXJhbSB7TnVtYmVyfSB3YWl0XG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gaW1tZWRpYXRlXG4gICAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICAgKi9cbiAgZnVuY3Rpb24gZGVib3VuY2UoZnVuYywgd2FpdCwgaW1tZWRpYXRlKSB7XG4gICAgICB2YXIgdGltZW91dDtcbiAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIGNvbnRleHQgPSB0aGlzO1xuICAgICAgICAgIHZhciBhcmdzID0gYXJndW1lbnRzO1xuICAgICAgICAgIHZhciBsYXRlciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgdGltZW91dCA9IG51bGw7XG4gICAgICAgICAgICAgIGlmICghaW1tZWRpYXRlKSB7XG4gICAgICAgICAgICAgICAgICBmdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgICAgICB2YXIgY2FsbE5vdyA9IGltbWVkaWF0ZSAmJiAhdGltZW91dDtcbiAgICAgICAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG4gICAgICAgICAgdGltZW91dCA9IHNldFRpbWVvdXQobGF0ZXIsIHdhaXQpO1xuICAgICAgICAgIGlmIChjYWxsTm93KSB7XG4gICAgICAgICAgICAgIGZ1bmMuYXBwbHkoY29udGV4dCwgYXJncyk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgfVxuICAvKipcbiAgICpcbiAgICogQHBhcmFtIHtTdHJpbmd9IHVybFxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNWYWxpZFVybCh1cmwpIHtcbiAgICAgIHZhciBleHByZXNzaW9uID0gL1stYS16QS1aMC05QDolLl9cXCt+Iz1dezIsMjU2fVxcLlthLXpdezIsNn1cXGIoWy1hLXpBLVowLTlAOiVfXFwrLn4jPyYvLz1dKikvZ2k7XG4gICAgICByZXR1cm4gZXhwcmVzc2lvbi50ZXN0KHVybCk7XG4gIH1cbiAgdmFyIGZ1bmMgPSB7XG4gICAgICBlcTogZXEsXG4gICAgICBlcTI6IGVxMixcbiAgICAgIHBlcTI6IHBlcTIsXG4gICAgICBvazogb2ssXG4gICAgICBmYWlsOiBmYWlsLFxuICAgICAgc2VsZjogc2VsZixcbiAgICAgIG5vdDogbm90LFxuICAgICAgYW5kOiBhbmQsXG4gICAgICBpbnZva2U6IGludm9rZSxcbiAgICAgIHVuaXF1ZUlkOiB1bmlxdWVJZCxcbiAgICAgIHJlY3QyYm5kOiByZWN0MmJuZCxcbiAgICAgIGludmVydE9iamVjdDogaW52ZXJ0T2JqZWN0LFxuICAgICAgbmFtZXNwYWNlVG9DYW1lbDogbmFtZXNwYWNlVG9DYW1lbCxcbiAgICAgIGRlYm91bmNlOiBkZWJvdW5jZSxcbiAgICAgIGlzVmFsaWRVcmw6IGlzVmFsaWRVcmxcbiAgfTtcblxuICAvKipcbiAgICogcmV0dXJucyB0aGUgZmlyc3QgaXRlbSBvZiBhbiBhcnJheS5cbiAgICpcbiAgICogQHBhcmFtIHtBcnJheX0gYXJyYXlcbiAgICovXG4gIGZ1bmN0aW9uIGhlYWQoYXJyYXkpIHtcbiAgICAgIHJldHVybiBhcnJheVswXTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB0aGUgbGFzdCBpdGVtIG9mIGFuIGFycmF5LlxuICAgKlxuICAgKiBAcGFyYW0ge0FycmF5fSBhcnJheVxuICAgKi9cbiAgZnVuY3Rpb24gbGFzdChhcnJheSkge1xuICAgICAgcmV0dXJuIGFycmF5W2FycmF5Lmxlbmd0aCAtIDFdO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIGV2ZXJ5dGhpbmcgYnV0IHRoZSBsYXN0IGVudHJ5IG9mIHRoZSBhcnJheS5cbiAgICpcbiAgICogQHBhcmFtIHtBcnJheX0gYXJyYXlcbiAgICovXG4gIGZ1bmN0aW9uIGluaXRpYWwoYXJyYXkpIHtcbiAgICAgIHJldHVybiBhcnJheS5zbGljZSgwLCBhcnJheS5sZW5ndGggLSAxKTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB0aGUgcmVzdCBvZiB0aGUgaXRlbXMgaW4gYW4gYXJyYXkuXG4gICAqXG4gICAqIEBwYXJhbSB7QXJyYXl9IGFycmF5XG4gICAqL1xuICBmdW5jdGlvbiB0YWlsKGFycmF5KSB7XG4gICAgICByZXR1cm4gYXJyYXkuc2xpY2UoMSk7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgaXRlbSBvZiBhcnJheVxuICAgKi9cbiAgZnVuY3Rpb24gZmluZChhcnJheSwgcHJlZCkge1xuICAgICAgZm9yICh2YXIgaWR4ID0gMCwgbGVuID0gYXJyYXkubGVuZ3RoOyBpZHggPCBsZW47IGlkeCsrKSB7XG4gICAgICAgICAgdmFyIGl0ZW0gPSBhcnJheVtpZHhdO1xuICAgICAgICAgIGlmIChwcmVkKGl0ZW0pKSB7XG4gICAgICAgICAgICAgIHJldHVybiBpdGVtO1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB0cnVlIGlmIGFsbCBvZiB0aGUgdmFsdWVzIGluIHRoZSBhcnJheSBwYXNzIHRoZSBwcmVkaWNhdGUgdHJ1dGggdGVzdC5cbiAgICovXG4gIGZ1bmN0aW9uIGFsbChhcnJheSwgcHJlZCkge1xuICAgICAgZm9yICh2YXIgaWR4ID0gMCwgbGVuID0gYXJyYXkubGVuZ3RoOyBpZHggPCBsZW47IGlkeCsrKSB7XG4gICAgICAgICAgaWYgKCFwcmVkKGFycmF5W2lkeF0pKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB0cnVlIGlmIHRoZSB2YWx1ZSBpcyBwcmVzZW50IGluIHRoZSBsaXN0LlxuICAgKi9cbiAgZnVuY3Rpb24gY29udGFpbnMoYXJyYXksIGl0ZW0pIHtcbiAgICAgIGlmIChhcnJheSAmJiBhcnJheS5sZW5ndGggJiYgaXRlbSkge1xuICAgICAgICAgIHJldHVybiBhcnJheS5pbmRleE9mKGl0ZW0pICE9PSAtMTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICAvKipcbiAgICogZ2V0IHN1bSBmcm9tIGEgbGlzdFxuICAgKlxuICAgKiBAcGFyYW0ge0FycmF5fSBhcnJheSAtIGFycmF5XG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IGZuIC0gaXRlcmF0b3JcbiAgICovXG4gIGZ1bmN0aW9uIHN1bShhcnJheSwgZm4pIHtcbiAgICAgIGZuID0gZm4gfHwgZnVuYy5zZWxmO1xuICAgICAgcmV0dXJuIGFycmF5LnJlZHVjZShmdW5jdGlvbiAobWVtbywgdikge1xuICAgICAgICAgIHJldHVybiBtZW1vICsgZm4odik7XG4gICAgICB9LCAwKTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyBhIGNvcHkgb2YgdGhlIGNvbGxlY3Rpb24gd2l0aCBhcnJheSB0eXBlLlxuICAgKiBAcGFyYW0ge0NvbGxlY3Rpb259IGNvbGxlY3Rpb24gLSBjb2xsZWN0aW9uIGVnKSBub2RlLmNoaWxkTm9kZXMsIC4uLlxuICAgKi9cbiAgZnVuY3Rpb24gZnJvbShjb2xsZWN0aW9uKSB7XG4gICAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgICB2YXIgbGVuZ3RoID0gY29sbGVjdGlvbi5sZW5ndGg7XG4gICAgICB2YXIgaWR4ID0gLTE7XG4gICAgICB3aGlsZSAoKytpZHggPCBsZW5ndGgpIHtcbiAgICAgICAgICByZXN1bHRbaWR4XSA9IGNvbGxlY3Rpb25baWR4XTtcbiAgICAgIH1cbiAgICAgIHJldHVybiByZXN1bHQ7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgd2hldGhlciBsaXN0IGlzIGVtcHR5IG9yIG5vdFxuICAgKi9cbiAgZnVuY3Rpb24gaXNFbXB0eShhcnJheSkge1xuICAgICAgcmV0dXJuICFhcnJheSB8fCAhYXJyYXkubGVuZ3RoO1xuICB9XG4gIC8qKlxuICAgKiBjbHVzdGVyIGVsZW1lbnRzIGJ5IHByZWRpY2F0ZSBmdW5jdGlvbi5cbiAgICpcbiAgICogQHBhcmFtIHtBcnJheX0gYXJyYXkgLSBhcnJheVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiAtIHByZWRpY2F0ZSBmdW5jdGlvbiBmb3IgY2x1c3RlciBydWxlXG4gICAqIEBwYXJhbSB7QXJyYXlbXX1cbiAgICovXG4gIGZ1bmN0aW9uIGNsdXN0ZXJCeShhcnJheSwgZm4pIHtcbiAgICAgIGlmICghYXJyYXkubGVuZ3RoKSB7XG4gICAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgfVxuICAgICAgdmFyIGFUYWlsID0gdGFpbChhcnJheSk7XG4gICAgICByZXR1cm4gYVRhaWwucmVkdWNlKGZ1bmN0aW9uIChtZW1vLCB2KSB7XG4gICAgICAgICAgdmFyIGFMYXN0ID0gbGFzdChtZW1vKTtcbiAgICAgICAgICBpZiAoZm4obGFzdChhTGFzdCksIHYpKSB7XG4gICAgICAgICAgICAgIGFMYXN0W2FMYXN0Lmxlbmd0aF0gPSB2O1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbWVtb1ttZW1vLmxlbmd0aF0gPSBbdl07XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBtZW1vO1xuICAgICAgfSwgW1toZWFkKGFycmF5KV1dKTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyBhIGNvcHkgb2YgdGhlIGFycmF5IHdpdGggYWxsIGZhbHNlIHZhbHVlcyByZW1vdmVkXG4gICAqXG4gICAqIEBwYXJhbSB7QXJyYXl9IGFycmF5IC0gYXJyYXlcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gZm4gLSBwcmVkaWNhdGUgZnVuY3Rpb24gZm9yIGNsdXN0ZXIgcnVsZVxuICAgKi9cbiAgZnVuY3Rpb24gY29tcGFjdChhcnJheSkge1xuICAgICAgdmFyIGFSZXN1bHQgPSBbXTtcbiAgICAgIGZvciAodmFyIGlkeCA9IDAsIGxlbiA9IGFycmF5Lmxlbmd0aDsgaWR4IDwgbGVuOyBpZHgrKykge1xuICAgICAgICAgIGlmIChhcnJheVtpZHhdKSB7XG4gICAgICAgICAgICAgIGFSZXN1bHQucHVzaChhcnJheVtpZHhdKTtcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4gYVJlc3VsdDtcbiAgfVxuICAvKipcbiAgICogcHJvZHVjZXMgYSBkdXBsaWNhdGUtZnJlZSB2ZXJzaW9uIG9mIHRoZSBhcnJheVxuICAgKlxuICAgKiBAcGFyYW0ge0FycmF5fSBhcnJheVxuICAgKi9cbiAgZnVuY3Rpb24gdW5pcXVlKGFycmF5KSB7XG4gICAgICB2YXIgcmVzdWx0cyA9IFtdO1xuICAgICAgZm9yICh2YXIgaWR4ID0gMCwgbGVuID0gYXJyYXkubGVuZ3RoOyBpZHggPCBsZW47IGlkeCsrKSB7XG4gICAgICAgICAgaWYgKCFjb250YWlucyhyZXN1bHRzLCBhcnJheVtpZHhdKSkge1xuICAgICAgICAgICAgICByZXN1bHRzLnB1c2goYXJyYXlbaWR4XSk7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHJlc3VsdHM7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgbmV4dCBpdGVtLlxuICAgKiBAcGFyYW0ge0FycmF5fSBhcnJheVxuICAgKi9cbiAgZnVuY3Rpb24gbmV4dChhcnJheSwgaXRlbSkge1xuICAgICAgaWYgKGFycmF5ICYmIGFycmF5Lmxlbmd0aCAmJiBpdGVtKSB7XG4gICAgICAgICAgdmFyIGlkeCA9IGFycmF5LmluZGV4T2YoaXRlbSk7XG4gICAgICAgICAgcmV0dXJuIGlkeCA9PT0gLTEgPyBudWxsIDogYXJyYXlbaWR4ICsgMV07XG4gICAgICB9XG4gICAgICByZXR1cm4gbnVsbDtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyBwcmV2IGl0ZW0uXG4gICAqIEBwYXJhbSB7QXJyYXl9IGFycmF5XG4gICAqL1xuICBmdW5jdGlvbiBwcmV2KGFycmF5LCBpdGVtKSB7XG4gICAgICBpZiAoYXJyYXkgJiYgYXJyYXkubGVuZ3RoICYmIGl0ZW0pIHtcbiAgICAgICAgICB2YXIgaWR4ID0gYXJyYXkuaW5kZXhPZihpdGVtKTtcbiAgICAgICAgICByZXR1cm4gaWR4ID09PSAtMSA/IG51bGwgOiBhcnJheVtpZHggLSAxXTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBudWxsO1xuICB9XG4gIC8qKlxuICAgKiBAY2xhc3MgY29yZS5saXN0XG4gICAqXG4gICAqIGxpc3QgdXRpbHNcbiAgICpcbiAgICogQHNpbmdsZXRvblxuICAgKiBAYWx0ZXJuYXRlQ2xhc3NOYW1lIGxpc3RcbiAgICovXG4gIHZhciBsaXN0cyA9IHtcbiAgICAgIGhlYWQ6IGhlYWQsXG4gICAgICBsYXN0OiBsYXN0LFxuICAgICAgaW5pdGlhbDogaW5pdGlhbCxcbiAgICAgIHRhaWw6IHRhaWwsXG4gICAgICBwcmV2OiBwcmV2LFxuICAgICAgbmV4dDogbmV4dCxcbiAgICAgIGZpbmQ6IGZpbmQsXG4gICAgICBjb250YWluczogY29udGFpbnMsXG4gICAgICBhbGw6IGFsbCxcbiAgICAgIHN1bTogc3VtLFxuICAgICAgZnJvbTogZnJvbSxcbiAgICAgIGlzRW1wdHk6IGlzRW1wdHksXG4gICAgICBjbHVzdGVyQnk6IGNsdXN0ZXJCeSxcbiAgICAgIGNvbXBhY3Q6IGNvbXBhY3QsXG4gICAgICB1bmlxdWU6IHVuaXF1ZVxuICB9O1xuXG4gIHZhciBOQlNQX0NIQVIgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKDE2MCk7XG4gIHZhciBaRVJPX1dJRFRIX05CU1BfQ0hBUiA9ICdcXHVmZWZmJztcbiAgLyoqXG4gICAqIEBtZXRob2QgaXNFZGl0YWJsZVxuICAgKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgbm9kZSBpcyBgbm90ZS1lZGl0YWJsZWAgb3Igbm90LlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzRWRpdGFibGUobm9kZSkge1xuICAgICAgcmV0dXJuIG5vZGUgJiYgJCQxKG5vZGUpLmhhc0NsYXNzKCdub3RlLWVkaXRhYmxlJyk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgaXNDb250cm9sU2l6aW5nXG4gICAqXG4gICAqIHJldHVybnMgd2hldGhlciBub2RlIGlzIGBub3RlLWNvbnRyb2wtc2l6aW5nYCBvciBub3QuXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNDb250cm9sU2l6aW5nKG5vZGUpIHtcbiAgICAgIHJldHVybiBub2RlICYmICQkMShub2RlKS5oYXNDbGFzcygnbm90ZS1jb250cm9sLXNpemluZycpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIG1ha2VQcmVkQnlOb2RlTmFtZVxuICAgKlxuICAgKiByZXR1cm5zIHByZWRpY2F0ZSB3aGljaCBqdWRnZSB3aGV0aGVyIG5vZGVOYW1lIGlzIHNhbWVcbiAgICpcbiAgICogQHBhcmFtIHtTdHJpbmd9IG5vZGVOYW1lXG4gICAqIEByZXR1cm4ge0Z1bmN0aW9ufVxuICAgKi9cbiAgZnVuY3Rpb24gbWFrZVByZWRCeU5vZGVOYW1lKG5vZGVOYW1lKSB7XG4gICAgICBub2RlTmFtZSA9IG5vZGVOYW1lLnRvVXBwZXJDYXNlKCk7XG4gICAgICByZXR1cm4gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZSAmJiBub2RlLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkgPT09IG5vZGVOYW1lO1xuICAgICAgfTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBpc1RleHRcbiAgICpcbiAgICpcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEByZXR1cm4ge0Jvb2xlYW59IHRydWUgaWYgbm9kZSdzIHR5cGUgaXMgdGV4dCgzKVxuICAgKi9cbiAgZnVuY3Rpb24gaXNUZXh0KG5vZGUpIHtcbiAgICAgIHJldHVybiBub2RlICYmIG5vZGUubm9kZVR5cGUgPT09IDM7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgaXNFbGVtZW50XG4gICAqXG4gICAqXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcmV0dXJuIHtCb29sZWFufSB0cnVlIGlmIG5vZGUncyB0eXBlIGlzIGVsZW1lbnQoMSlcbiAgICovXG4gIGZ1bmN0aW9uIGlzRWxlbWVudChub2RlKSB7XG4gICAgICByZXR1cm4gbm9kZSAmJiBub2RlLm5vZGVUeXBlID09PSAxO1xuICB9XG4gIC8qKlxuICAgKiBleCkgYnIsIGNvbCwgZW1iZWQsIGhyLCBpbWcsIGlucHV0LCAuLi5cbiAgICogQHNlZSBodHRwOi8vd3d3LnczLm9yZy9odG1sL3dnL2RyYWZ0cy9odG1sL21hc3Rlci9zeW50YXguaHRtbCN2b2lkLWVsZW1lbnRzXG4gICAqL1xuICBmdW5jdGlvbiBpc1ZvaWQobm9kZSkge1xuICAgICAgcmV0dXJuIG5vZGUgJiYgL15CUnxeSU1HfF5IUnxeSUZSQU1FfF5CVVRUT058XklOUFVUfF5BVURJT3xeVklERU98XkVNQkVELy50ZXN0KG5vZGUubm9kZU5hbWUudG9VcHBlckNhc2UoKSk7XG4gIH1cbiAgZnVuY3Rpb24gaXNQYXJhKG5vZGUpIHtcbiAgICAgIGlmIChpc0VkaXRhYmxlKG5vZGUpKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgLy8gQ2hyb21lKHYzMS4wKSwgRkYodjI1LjAuMSkgdXNlIERJViBmb3IgcGFyYWdyYXBoXG4gICAgICByZXR1cm4gbm9kZSAmJiAvXkRJVnxeUHxeTEl8XkhbMS03XS8udGVzdChub2RlLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkpO1xuICB9XG4gIGZ1bmN0aW9uIGlzSGVhZGluZyhub2RlKSB7XG4gICAgICByZXR1cm4gbm9kZSAmJiAvXkhbMS03XS8udGVzdChub2RlLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkpO1xuICB9XG4gIHZhciBpc1ByZSA9IG1ha2VQcmVkQnlOb2RlTmFtZSgnUFJFJyk7XG4gIHZhciBpc0xpID0gbWFrZVByZWRCeU5vZGVOYW1lKCdMSScpO1xuICBmdW5jdGlvbiBpc1B1cmVQYXJhKG5vZGUpIHtcbiAgICAgIHJldHVybiBpc1BhcmEobm9kZSkgJiYgIWlzTGkobm9kZSk7XG4gIH1cbiAgdmFyIGlzVGFibGUgPSBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ1RBQkxFJyk7XG4gIHZhciBpc0RhdGEgPSBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0RBVEEnKTtcbiAgZnVuY3Rpb24gaXNJbmxpbmUobm9kZSkge1xuICAgICAgcmV0dXJuICFpc0JvZHlDb250YWluZXIobm9kZSkgJiZcbiAgICAgICAgICAhaXNMaXN0KG5vZGUpICYmXG4gICAgICAgICAgIWlzSHIobm9kZSkgJiZcbiAgICAgICAgICAhaXNQYXJhKG5vZGUpICYmXG4gICAgICAgICAgIWlzVGFibGUobm9kZSkgJiZcbiAgICAgICAgICAhaXNCbG9ja3F1b3RlKG5vZGUpICYmXG4gICAgICAgICAgIWlzRGF0YShub2RlKTtcbiAgfVxuICBmdW5jdGlvbiBpc0xpc3Qobm9kZSkge1xuICAgICAgcmV0dXJuIG5vZGUgJiYgL15VTHxeT0wvLnRlc3Qobm9kZS5ub2RlTmFtZS50b1VwcGVyQ2FzZSgpKTtcbiAgfVxuICB2YXIgaXNIciA9IG1ha2VQcmVkQnlOb2RlTmFtZSgnSFInKTtcbiAgZnVuY3Rpb24gaXNDZWxsKG5vZGUpIHtcbiAgICAgIHJldHVybiBub2RlICYmIC9eVER8XlRILy50ZXN0KG5vZGUubm9kZU5hbWUudG9VcHBlckNhc2UoKSk7XG4gIH1cbiAgdmFyIGlzQmxvY2txdW90ZSA9IG1ha2VQcmVkQnlOb2RlTmFtZSgnQkxPQ0tRVU9URScpO1xuICBmdW5jdGlvbiBpc0JvZHlDb250YWluZXIobm9kZSkge1xuICAgICAgcmV0dXJuIGlzQ2VsbChub2RlKSB8fCBpc0Jsb2NrcXVvdGUobm9kZSkgfHwgaXNFZGl0YWJsZShub2RlKTtcbiAgfVxuICB2YXIgaXNBbmNob3IgPSBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0EnKTtcbiAgZnVuY3Rpb24gaXNQYXJhSW5saW5lKG5vZGUpIHtcbiAgICAgIHJldHVybiBpc0lubGluZShub2RlKSAmJiAhIWFuY2VzdG9yKG5vZGUsIGlzUGFyYSk7XG4gIH1cbiAgZnVuY3Rpb24gaXNCb2R5SW5saW5lKG5vZGUpIHtcbiAgICAgIHJldHVybiBpc0lubGluZShub2RlKSAmJiAhYW5jZXN0b3Iobm9kZSwgaXNQYXJhKTtcbiAgfVxuICB2YXIgaXNCb2R5ID0gbWFrZVByZWRCeU5vZGVOYW1lKCdCT0RZJyk7XG4gIC8qKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgbm9kZUIgaXMgY2xvc2VzdCBzaWJsaW5nIG9mIG5vZGVBXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZUFcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlQlxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNDbG9zZXN0U2libGluZyhub2RlQSwgbm9kZUIpIHtcbiAgICAgIHJldHVybiBub2RlQS5uZXh0U2libGluZyA9PT0gbm9kZUIgfHxcbiAgICAgICAgICBub2RlQS5wcmV2aW91c1NpYmxpbmcgPT09IG5vZGVCO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIGFycmF5IG9mIGNsb3Nlc3Qgc2libGluZ3Mgd2l0aCBub2RlXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBbcHJlZF0gLSBwcmVkaWNhdGUgZnVuY3Rpb25cbiAgICogQHJldHVybiB7Tm9kZVtdfVxuICAgKi9cbiAgZnVuY3Rpb24gd2l0aENsb3Nlc3RTaWJsaW5ncyhub2RlLCBwcmVkKSB7XG4gICAgICBwcmVkID0gcHJlZCB8fCBmdW5jLm9rO1xuICAgICAgdmFyIHNpYmxpbmdzID0gW107XG4gICAgICBpZiAobm9kZS5wcmV2aW91c1NpYmxpbmcgJiYgcHJlZChub2RlLnByZXZpb3VzU2libGluZykpIHtcbiAgICAgICAgICBzaWJsaW5ncy5wdXNoKG5vZGUucHJldmlvdXNTaWJsaW5nKTtcbiAgICAgIH1cbiAgICAgIHNpYmxpbmdzLnB1c2gobm9kZSk7XG4gICAgICBpZiAobm9kZS5uZXh0U2libGluZyAmJiBwcmVkKG5vZGUubmV4dFNpYmxpbmcpKSB7XG4gICAgICAgICAgc2libGluZ3MucHVzaChub2RlLm5leHRTaWJsaW5nKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBzaWJsaW5ncztcbiAgfVxuICAvKipcbiAgICogYmxhbmsgSFRNTCBmb3IgY3Vyc29yIHBvc2l0aW9uXG4gICAqIC0gW3dvcmthcm91bmRdIG9sZCBJRSBvbmx5IHdvcmtzIHdpdGggJm5ic3A7XG4gICAqIC0gW3dvcmthcm91bmRdIElFMTEgYW5kIG90aGVyIGJyb3dzZXIgd29ya3Mgd2l0aCBib2d1cyBiclxuICAgKi9cbiAgdmFyIGJsYW5rSFRNTCA9IGVudi5pc01TSUUgJiYgZW52LmJyb3dzZXJWZXJzaW9uIDwgMTEgPyAnJm5ic3A7JyA6ICc8YnI+JztcbiAgLyoqXG4gICAqIEBtZXRob2Qgbm9kZUxlbmd0aFxuICAgKlxuICAgKiByZXR1cm5zICN0ZXh0J3MgdGV4dCBzaXplIG9yIGVsZW1lbnQncyBjaGlsZE5vZGVzIHNpemVcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqL1xuICBmdW5jdGlvbiBub2RlTGVuZ3RoKG5vZGUpIHtcbiAgICAgIGlmIChpc1RleHQobm9kZSkpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZS5ub2RlVmFsdWUubGVuZ3RoO1xuICAgICAgfVxuICAgICAgaWYgKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZS5jaGlsZE5vZGVzLmxlbmd0aDtcbiAgICAgIH1cbiAgICAgIHJldHVybiAwO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgbm9kZSBpcyBlbXB0eSBvciBub3QuXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNFbXB0eSQxKG5vZGUpIHtcbiAgICAgIHZhciBsZW4gPSBub2RlTGVuZ3RoKG5vZGUpO1xuICAgICAgaWYgKGxlbiA9PT0gMCkge1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgICAgZWxzZSBpZiAoIWlzVGV4dChub2RlKSAmJiBsZW4gPT09IDEgJiYgbm9kZS5pbm5lckhUTUwgPT09IGJsYW5rSFRNTCkge1xuICAgICAgICAgIC8vIGV4KSA8cD48YnI+PC9wPiwgPHNwYW4+PGJyPjwvc3Bhbj5cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIGVsc2UgaWYgKGxpc3RzLmFsbChub2RlLmNoaWxkTm9kZXMsIGlzVGV4dCkgJiYgbm9kZS5pbm5lckhUTUwgPT09ICcnKSB7XG4gICAgICAgICAgLy8gZXgpIDxwPjwvcD4sIDxzcGFuPjwvc3Bhbj5cbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgfVxuICAvKipcbiAgICogcGFkZGluZyBibGFua0hUTUwgaWYgbm9kZSBpcyBlbXB0eSAoZm9yIGN1cnNvciBwb3NpdGlvbilcbiAgICovXG4gIGZ1bmN0aW9uIHBhZGRpbmdCbGFua0hUTUwobm9kZSkge1xuICAgICAgaWYgKCFpc1ZvaWQobm9kZSkgJiYgIW5vZGVMZW5ndGgobm9kZSkpIHtcbiAgICAgICAgICBub2RlLmlubmVySFRNTCA9IGJsYW5rSFRNTDtcbiAgICAgIH1cbiAgfVxuICAvKipcbiAgICogZmluZCBuZWFyZXN0IGFuY2VzdG9yIHByZWRpY2F0ZSBoaXRcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IHByZWQgLSBwcmVkaWNhdGUgZnVuY3Rpb25cbiAgICovXG4gIGZ1bmN0aW9uIGFuY2VzdG9yKG5vZGUsIHByZWQpIHtcbiAgICAgIHdoaWxlIChub2RlKSB7XG4gICAgICAgICAgaWYgKHByZWQobm9kZSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG5vZGU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChpc0VkaXRhYmxlKG5vZGUpKSB7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBub2RlID0gbm9kZS5wYXJlbnROb2RlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGw7XG4gIH1cbiAgLyoqXG4gICAqIGZpbmQgbmVhcmVzdCBhbmNlc3RvciBvbmx5IHNpbmdsZSBjaGlsZCBibG9vZCBsaW5lIGFuZCBwcmVkaWNhdGUgaGl0XG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcmVkIC0gcHJlZGljYXRlIGZ1bmN0aW9uXG4gICAqL1xuICBmdW5jdGlvbiBzaW5nbGVDaGlsZEFuY2VzdG9yKG5vZGUsIHByZWQpIHtcbiAgICAgIG5vZGUgPSBub2RlLnBhcmVudE5vZGU7XG4gICAgICB3aGlsZSAobm9kZSkge1xuICAgICAgICAgIGlmIChub2RlTGVuZ3RoKG5vZGUpICE9PSAxKSB7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAocHJlZChub2RlKSkge1xuICAgICAgICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGlzRWRpdGFibGUobm9kZSkpIHtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5vZGUgPSBub2RlLnBhcmVudE5vZGU7XG4gICAgICB9XG4gICAgICByZXR1cm4gbnVsbDtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyBuZXcgYXJyYXkgb2YgYW5jZXN0b3Igbm9kZXMgKHVudGlsIHByZWRpY2F0ZSBoaXQpLlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gW29wdGlvbmFsXSBwcmVkIC0gcHJlZGljYXRlIGZ1bmN0aW9uXG4gICAqL1xuICBmdW5jdGlvbiBsaXN0QW5jZXN0b3Iobm9kZSwgcHJlZCkge1xuICAgICAgcHJlZCA9IHByZWQgfHwgZnVuYy5mYWlsO1xuICAgICAgdmFyIGFuY2VzdG9ycyA9IFtdO1xuICAgICAgYW5jZXN0b3Iobm9kZSwgZnVuY3Rpb24gKGVsKSB7XG4gICAgICAgICAgaWYgKCFpc0VkaXRhYmxlKGVsKSkge1xuICAgICAgICAgICAgICBhbmNlc3RvcnMucHVzaChlbCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBwcmVkKGVsKTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIGFuY2VzdG9ycztcbiAgfVxuICAvKipcbiAgICogZmluZCBmYXJ0aGVzdCBhbmNlc3RvciBwcmVkaWNhdGUgaGl0XG4gICAqL1xuICBmdW5jdGlvbiBsYXN0QW5jZXN0b3Iobm9kZSwgcHJlZCkge1xuICAgICAgdmFyIGFuY2VzdG9ycyA9IGxpc3RBbmNlc3Rvcihub2RlKTtcbiAgICAgIHJldHVybiBsaXN0cy5sYXN0KGFuY2VzdG9ycy5maWx0ZXIocHJlZCkpO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIGNvbW1vbiBhbmNlc3RvciBub2RlIGJldHdlZW4gdHdvIG5vZGVzLlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVBXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZUJcbiAgICovXG4gIGZ1bmN0aW9uIGNvbW1vbkFuY2VzdG9yKG5vZGVBLCBub2RlQikge1xuICAgICAgdmFyIGFuY2VzdG9ycyA9IGxpc3RBbmNlc3Rvcihub2RlQSk7XG4gICAgICBmb3IgKHZhciBuID0gbm9kZUI7IG47IG4gPSBuLnBhcmVudE5vZGUpIHtcbiAgICAgICAgICBpZiAoYW5jZXN0b3JzLmluZGV4T2YobikgPiAtMSlcbiAgICAgICAgICAgICAgcmV0dXJuIG47XG4gICAgICB9XG4gICAgICByZXR1cm4gbnVsbDsgLy8gZGlmZmVyZW5jZSBkb2N1bWVudCBhcmVhXG4gIH1cbiAgLyoqXG4gICAqIGxpc3RpbmcgYWxsIHByZXZpb3VzIHNpYmxpbmdzICh1bnRpbCBwcmVkaWNhdGUgaGl0KS5cbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IFtvcHRpb25hbF0gcHJlZCAtIHByZWRpY2F0ZSBmdW5jdGlvblxuICAgKi9cbiAgZnVuY3Rpb24gbGlzdFByZXYobm9kZSwgcHJlZCkge1xuICAgICAgcHJlZCA9IHByZWQgfHwgZnVuYy5mYWlsO1xuICAgICAgdmFyIG5vZGVzID0gW107XG4gICAgICB3aGlsZSAobm9kZSkge1xuICAgICAgICAgIGlmIChwcmVkKG5vZGUpKSB7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBub2Rlcy5wdXNoKG5vZGUpO1xuICAgICAgICAgIG5vZGUgPSBub2RlLnByZXZpb3VzU2libGluZztcbiAgICAgIH1cbiAgICAgIHJldHVybiBub2RlcztcbiAgfVxuICAvKipcbiAgICogbGlzdGluZyBuZXh0IHNpYmxpbmdzICh1bnRpbCBwcmVkaWNhdGUgaGl0KS5cbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IFtwcmVkXSAtIHByZWRpY2F0ZSBmdW5jdGlvblxuICAgKi9cbiAgZnVuY3Rpb24gbGlzdE5leHQobm9kZSwgcHJlZCkge1xuICAgICAgcHJlZCA9IHByZWQgfHwgZnVuYy5mYWlsO1xuICAgICAgdmFyIG5vZGVzID0gW107XG4gICAgICB3aGlsZSAobm9kZSkge1xuICAgICAgICAgIGlmIChwcmVkKG5vZGUpKSB7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBub2Rlcy5wdXNoKG5vZGUpO1xuICAgICAgICAgIG5vZGUgPSBub2RlLm5leHRTaWJsaW5nO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG5vZGVzO1xuICB9XG4gIC8qKlxuICAgKiBsaXN0aW5nIGRlc2NlbmRhbnQgbm9kZXNcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IFtwcmVkXSAtIHByZWRpY2F0ZSBmdW5jdGlvblxuICAgKi9cbiAgZnVuY3Rpb24gbGlzdERlc2NlbmRhbnQobm9kZSwgcHJlZCkge1xuICAgICAgdmFyIGRlc2NlbmRhbnRzID0gW107XG4gICAgICBwcmVkID0gcHJlZCB8fCBmdW5jLm9rO1xuICAgICAgLy8gc3RhcnQgREZTKGRlcHRoIGZpcnN0IHNlYXJjaCkgd2l0aCBub2RlXG4gICAgICAoZnVuY3Rpb24gZm5XYWxrKGN1cnJlbnQpIHtcbiAgICAgICAgICBpZiAobm9kZSAhPT0gY3VycmVudCAmJiBwcmVkKGN1cnJlbnQpKSB7XG4gICAgICAgICAgICAgIGRlc2NlbmRhbnRzLnB1c2goY3VycmVudCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGZvciAodmFyIGlkeCA9IDAsIGxlbiA9IGN1cnJlbnQuY2hpbGROb2Rlcy5sZW5ndGg7IGlkeCA8IGxlbjsgaWR4KyspIHtcbiAgICAgICAgICAgICAgZm5XYWxrKGN1cnJlbnQuY2hpbGROb2Rlc1tpZHhdKTtcbiAgICAgICAgICB9XG4gICAgICB9KShub2RlKTtcbiAgICAgIHJldHVybiBkZXNjZW5kYW50cztcbiAgfVxuICAvKipcbiAgICogd3JhcCBub2RlIHdpdGggbmV3IHRhZy5cbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7Tm9kZX0gdGFnTmFtZSBvZiB3cmFwcGVyXG4gICAqIEByZXR1cm4ge05vZGV9IC0gd3JhcHBlclxuICAgKi9cbiAgZnVuY3Rpb24gd3JhcChub2RlLCB3cmFwcGVyTmFtZSkge1xuICAgICAgdmFyIHBhcmVudCA9IG5vZGUucGFyZW50Tm9kZTtcbiAgICAgIHZhciB3cmFwcGVyID0gJCQxKCc8JyArIHdyYXBwZXJOYW1lICsgJz4nKVswXTtcbiAgICAgIHBhcmVudC5pbnNlcnRCZWZvcmUod3JhcHBlciwgbm9kZSk7XG4gICAgICB3cmFwcGVyLmFwcGVuZENoaWxkKG5vZGUpO1xuICAgICAgcmV0dXJuIHdyYXBwZXI7XG4gIH1cbiAgLyoqXG4gICAqIGluc2VydCBub2RlIGFmdGVyIHByZWNlZGluZ1xuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHBhcmFtIHtOb2RlfSBwcmVjZWRpbmcgLSBwcmVkaWNhdGUgZnVuY3Rpb25cbiAgICovXG4gIGZ1bmN0aW9uIGluc2VydEFmdGVyKG5vZGUsIHByZWNlZGluZykge1xuICAgICAgdmFyIG5leHQgPSBwcmVjZWRpbmcubmV4dFNpYmxpbmc7XG4gICAgICB2YXIgcGFyZW50ID0gcHJlY2VkaW5nLnBhcmVudE5vZGU7XG4gICAgICBpZiAobmV4dCkge1xuICAgICAgICAgIHBhcmVudC5pbnNlcnRCZWZvcmUobm9kZSwgbmV4dCk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgICBwYXJlbnQuYXBwZW5kQ2hpbGQobm9kZSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gbm9kZTtcbiAgfVxuICAvKipcbiAgICogYXBwZW5kIGVsZW1lbnRzLlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHBhcmFtIHtDb2xsZWN0aW9ufSBhQ2hpbGRcbiAgICovXG4gIGZ1bmN0aW9uIGFwcGVuZENoaWxkTm9kZXMobm9kZSwgYUNoaWxkKSB7XG4gICAgICAkJDEuZWFjaChhQ2hpbGQsIGZ1bmN0aW9uIChpZHgsIGNoaWxkKSB7XG4gICAgICAgICAgbm9kZS5hcHBlbmRDaGlsZChjaGlsZCk7XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBub2RlO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgYm91bmRhcnlQb2ludCBpcyBsZWZ0IGVkZ2Ugb3Igbm90LlxuICAgKlxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc0xlZnRFZGdlUG9pbnQocG9pbnQpIHtcbiAgICAgIHJldHVybiBwb2ludC5vZmZzZXQgPT09IDA7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgd2hldGhlciBib3VuZGFyeVBvaW50IGlzIHJpZ2h0IGVkZ2Ugb3Igbm90LlxuICAgKlxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc1JpZ2h0RWRnZVBvaW50KHBvaW50KSB7XG4gICAgICByZXR1cm4gcG9pbnQub2Zmc2V0ID09PSBub2RlTGVuZ3RoKHBvaW50Lm5vZGUpO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgYm91bmRhcnlQb2ludCBpcyBlZGdlIG9yIG5vdC5cbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBwb2ludFxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNFZGdlUG9pbnQocG9pbnQpIHtcbiAgICAgIHJldHVybiBpc0xlZnRFZGdlUG9pbnQocG9pbnQpIHx8IGlzUmlnaHRFZGdlUG9pbnQocG9pbnQpO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIHdoZXRoZXIgbm9kZSBpcyBsZWZ0IGVkZ2Ugb2YgYW5jZXN0b3Igb3Igbm90LlxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHBhcmFtIHtOb2RlfSBhbmNlc3RvclxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNMZWZ0RWRnZU9mKG5vZGUsIGFuY2VzdG9yKSB7XG4gICAgICB3aGlsZSAobm9kZSAmJiBub2RlICE9PSBhbmNlc3Rvcikge1xuICAgICAgICAgIGlmIChwb3NpdGlvbihub2RlKSAhPT0gMCkge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5vZGUgPSBub2RlLnBhcmVudE5vZGU7XG4gICAgICB9XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB3aGV0aGVyIG5vZGUgaXMgcmlnaHQgZWRnZSBvZiBhbmNlc3RvciBvciBub3QuXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKiBAcGFyYW0ge05vZGV9IGFuY2VzdG9yXG4gICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAqL1xuICBmdW5jdGlvbiBpc1JpZ2h0RWRnZU9mKG5vZGUsIGFuY2VzdG9yKSB7XG4gICAgICBpZiAoIWFuY2VzdG9yKSB7XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgICAgd2hpbGUgKG5vZGUgJiYgbm9kZSAhPT0gYW5jZXN0b3IpIHtcbiAgICAgICAgICBpZiAocG9zaXRpb24obm9kZSkgIT09IG5vZGVMZW5ndGgobm9kZS5wYXJlbnROb2RlKSAtIDEpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBub2RlID0gbm9kZS5wYXJlbnROb2RlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRydWU7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgd2hldGhlciBwb2ludCBpcyBsZWZ0IGVkZ2Ugb2YgYW5jZXN0b3Igb3Igbm90LlxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAqIEBwYXJhbSB7Tm9kZX0gYW5jZXN0b3JcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzTGVmdEVkZ2VQb2ludE9mKHBvaW50LCBhbmNlc3Rvcikge1xuICAgICAgcmV0dXJuIGlzTGVmdEVkZ2VQb2ludChwb2ludCkgJiYgaXNMZWZ0RWRnZU9mKHBvaW50Lm5vZGUsIGFuY2VzdG9yKTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB3aGV0aGVyIHBvaW50IGlzIHJpZ2h0IGVkZ2Ugb2YgYW5jZXN0b3Igb3Igbm90LlxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAqIEBwYXJhbSB7Tm9kZX0gYW5jZXN0b3JcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzUmlnaHRFZGdlUG9pbnRPZihwb2ludCwgYW5jZXN0b3IpIHtcbiAgICAgIHJldHVybiBpc1JpZ2h0RWRnZVBvaW50KHBvaW50KSAmJiBpc1JpZ2h0RWRnZU9mKHBvaW50Lm5vZGUsIGFuY2VzdG9yKTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyBvZmZzZXQgZnJvbSBwYXJlbnQuXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gbm9kZVxuICAgKi9cbiAgZnVuY3Rpb24gcG9zaXRpb24obm9kZSkge1xuICAgICAgdmFyIG9mZnNldCA9IDA7XG4gICAgICB3aGlsZSAoKG5vZGUgPSBub2RlLnByZXZpb3VzU2libGluZykpIHtcbiAgICAgICAgICBvZmZzZXQgKz0gMTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBvZmZzZXQ7XG4gIH1cbiAgZnVuY3Rpb24gaGFzQ2hpbGRyZW4obm9kZSkge1xuICAgICAgcmV0dXJuICEhKG5vZGUgJiYgbm9kZS5jaGlsZE5vZGVzICYmIG5vZGUuY2hpbGROb2Rlcy5sZW5ndGgpO1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm5zIHByZXZpb3VzIGJvdW5kYXJ5UG9pbnRcbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBwb2ludFxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IGlzU2tpcElubmVyT2Zmc2V0XG4gICAqIEByZXR1cm4ge0JvdW5kYXJ5UG9pbnR9XG4gICAqL1xuICBmdW5jdGlvbiBwcmV2UG9pbnQocG9pbnQsIGlzU2tpcElubmVyT2Zmc2V0KSB7XG4gICAgICB2YXIgbm9kZTtcbiAgICAgIHZhciBvZmZzZXQ7XG4gICAgICBpZiAocG9pbnQub2Zmc2V0ID09PSAwKSB7XG4gICAgICAgICAgaWYgKGlzRWRpdGFibGUocG9pbnQubm9kZSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfVxuICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlLnBhcmVudE5vZGU7XG4gICAgICAgICAgb2Zmc2V0ID0gcG9zaXRpb24ocG9pbnQubm9kZSk7XG4gICAgICB9XG4gICAgICBlbHNlIGlmIChoYXNDaGlsZHJlbihwb2ludC5ub2RlKSkge1xuICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlLmNoaWxkTm9kZXNbcG9pbnQub2Zmc2V0IC0gMV07XG4gICAgICAgICAgb2Zmc2V0ID0gbm9kZUxlbmd0aChub2RlKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlO1xuICAgICAgICAgIG9mZnNldCA9IGlzU2tpcElubmVyT2Zmc2V0ID8gMCA6IHBvaW50Lm9mZnNldCAtIDE7XG4gICAgICB9XG4gICAgICByZXR1cm4ge1xuICAgICAgICAgIG5vZGU6IG5vZGUsXG4gICAgICAgICAgb2Zmc2V0OiBvZmZzZXRcbiAgICAgIH07XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgbmV4dCBib3VuZGFyeVBvaW50XG4gICAqXG4gICAqIEBwYXJhbSB7Qm91bmRhcnlQb2ludH0gcG9pbnRcbiAgICogQHBhcmFtIHtCb29sZWFufSBpc1NraXBJbm5lck9mZnNldFxuICAgKiBAcmV0dXJuIHtCb3VuZGFyeVBvaW50fVxuICAgKi9cbiAgZnVuY3Rpb24gbmV4dFBvaW50KHBvaW50LCBpc1NraXBJbm5lck9mZnNldCkge1xuICAgICAgdmFyIG5vZGUsIG9mZnNldDtcbiAgICAgIGlmIChub2RlTGVuZ3RoKHBvaW50Lm5vZGUpID09PSBwb2ludC5vZmZzZXQpIHtcbiAgICAgICAgICBpZiAoaXNFZGl0YWJsZShwb2ludC5ub2RlKSkge1xuICAgICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgICAgbm9kZSA9IHBvaW50Lm5vZGUucGFyZW50Tm9kZTtcbiAgICAgICAgICBvZmZzZXQgPSBwb3NpdGlvbihwb2ludC5ub2RlKSArIDE7XG4gICAgICB9XG4gICAgICBlbHNlIGlmIChoYXNDaGlsZHJlbihwb2ludC5ub2RlKSkge1xuICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlLmNoaWxkTm9kZXNbcG9pbnQub2Zmc2V0XTtcbiAgICAgICAgICBvZmZzZXQgPSAwO1xuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgICAgbm9kZSA9IHBvaW50Lm5vZGU7XG4gICAgICAgICAgb2Zmc2V0ID0gaXNTa2lwSW5uZXJPZmZzZXQgPyBub2RlTGVuZ3RoKHBvaW50Lm5vZGUpIDogcG9pbnQub2Zmc2V0ICsgMTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB7XG4gICAgICAgICAgbm9kZTogbm9kZSxcbiAgICAgICAgICBvZmZzZXQ6IG9mZnNldFxuICAgICAgfTtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB3aGV0aGVyIHBvaW50QSBhbmQgcG9pbnRCIGlzIHNhbWUgb3Igbm90LlxuICAgKlxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50QVxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50QlxuICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgKi9cbiAgZnVuY3Rpb24gaXNTYW1lUG9pbnQocG9pbnRBLCBwb2ludEIpIHtcbiAgICAgIHJldHVybiBwb2ludEEubm9kZSA9PT0gcG9pbnRCLm5vZGUgJiYgcG9pbnRBLm9mZnNldCA9PT0gcG9pbnRCLm9mZnNldDtcbiAgfVxuICAvKipcbiAgICogcmV0dXJucyB3aGV0aGVyIHBvaW50IGlzIHZpc2libGUgKGNhbiBzZXQgY3Vyc29yKSBvciBub3QuXG4gICAqXG4gICAqIEBwYXJhbSB7Qm91bmRhcnlQb2ludH0gcG9pbnRcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzVmlzaWJsZVBvaW50KHBvaW50KSB7XG4gICAgICBpZiAoaXNUZXh0KHBvaW50Lm5vZGUpIHx8ICFoYXNDaGlsZHJlbihwb2ludC5ub2RlKSB8fCBpc0VtcHR5JDEocG9pbnQubm9kZSkpIHtcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIHZhciBsZWZ0Tm9kZSA9IHBvaW50Lm5vZGUuY2hpbGROb2Rlc1twb2ludC5vZmZzZXQgLSAxXTtcbiAgICAgIHZhciByaWdodE5vZGUgPSBwb2ludC5ub2RlLmNoaWxkTm9kZXNbcG9pbnQub2Zmc2V0XTtcbiAgICAgIGlmICgoIWxlZnROb2RlIHx8IGlzVm9pZChsZWZ0Tm9kZSkpICYmICghcmlnaHROb2RlIHx8IGlzVm9pZChyaWdodE5vZGUpKSkge1xuICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIHByZXZQb2ludFV0aWxcbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBwb2ludFxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcmVkXG4gICAqIEByZXR1cm4ge0JvdW5kYXJ5UG9pbnR9XG4gICAqL1xuICBmdW5jdGlvbiBwcmV2UG9pbnRVbnRpbChwb2ludCwgcHJlZCkge1xuICAgICAgd2hpbGUgKHBvaW50KSB7XG4gICAgICAgICAgaWYgKHByZWQocG9pbnQpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBwb2ludDtcbiAgICAgICAgICB9XG4gICAgICAgICAgcG9pbnQgPSBwcmV2UG9pbnQocG9pbnQpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGw7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgbmV4dFBvaW50VW50aWxcbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBwb2ludFxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBwcmVkXG4gICAqIEByZXR1cm4ge0JvdW5kYXJ5UG9pbnR9XG4gICAqL1xuICBmdW5jdGlvbiBuZXh0UG9pbnRVbnRpbChwb2ludCwgcHJlZCkge1xuICAgICAgd2hpbGUgKHBvaW50KSB7XG4gICAgICAgICAgaWYgKHByZWQocG9pbnQpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBwb2ludDtcbiAgICAgICAgICB9XG4gICAgICAgICAgcG9pbnQgPSBuZXh0UG9pbnQocG9pbnQpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG51bGw7XG4gIH1cbiAgLyoqXG4gICAqIHJldHVybnMgd2hldGhlciBwb2ludCBoYXMgY2hhcmFjdGVyIG9yIG5vdC5cbiAgICpcbiAgICogQHBhcmFtIHtQb2ludH0gcG9pbnRcbiAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICovXG4gIGZ1bmN0aW9uIGlzQ2hhclBvaW50KHBvaW50KSB7XG4gICAgICBpZiAoIWlzVGV4dChwb2ludC5ub2RlKSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIH1cbiAgICAgIHZhciBjaCA9IHBvaW50Lm5vZGUubm9kZVZhbHVlLmNoYXJBdChwb2ludC5vZmZzZXQgLSAxKTtcbiAgICAgIHJldHVybiBjaCAmJiAoY2ggIT09ICcgJyAmJiBjaCAhPT0gTkJTUF9DSEFSKTtcbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCB3YWxrUG9pbnRcbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBzdGFydFBvaW50XG4gICAqIEBwYXJhbSB7Qm91bmRhcnlQb2ludH0gZW5kUG9pbnRcbiAgICogQHBhcmFtIHtGdW5jdGlvbn0gaGFuZGxlclxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IGlzU2tpcElubmVyT2Zmc2V0XG4gICAqL1xuICBmdW5jdGlvbiB3YWxrUG9pbnQoc3RhcnRQb2ludCwgZW5kUG9pbnQsIGhhbmRsZXIsIGlzU2tpcElubmVyT2Zmc2V0KSB7XG4gICAgICB2YXIgcG9pbnQgPSBzdGFydFBvaW50O1xuICAgICAgd2hpbGUgKHBvaW50KSB7XG4gICAgICAgICAgaGFuZGxlcihwb2ludCk7XG4gICAgICAgICAgaWYgKGlzU2FtZVBvaW50KHBvaW50LCBlbmRQb2ludCkpIHtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBpc1NraXBPZmZzZXQgPSBpc1NraXBJbm5lck9mZnNldCAmJlxuICAgICAgICAgICAgICBzdGFydFBvaW50Lm5vZGUgIT09IHBvaW50Lm5vZGUgJiZcbiAgICAgICAgICAgICAgZW5kUG9pbnQubm9kZSAhPT0gcG9pbnQubm9kZTtcbiAgICAgICAgICBwb2ludCA9IG5leHRQb2ludChwb2ludCwgaXNTa2lwT2Zmc2V0KTtcbiAgICAgIH1cbiAgfVxuICAvKipcbiAgICogQG1ldGhvZCBtYWtlT2Zmc2V0UGF0aFxuICAgKlxuICAgKiByZXR1cm4gb2Zmc2V0UGF0aChhcnJheSBvZiBvZmZzZXQpIGZyb20gYW5jZXN0b3JcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBhbmNlc3RvciAtIGFuY2VzdG9yIG5vZGVcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqL1xuICBmdW5jdGlvbiBtYWtlT2Zmc2V0UGF0aChhbmNlc3Rvciwgbm9kZSkge1xuICAgICAgdmFyIGFuY2VzdG9ycyA9IGxpc3RBbmNlc3Rvcihub2RlLCBmdW5jLmVxKGFuY2VzdG9yKSk7XG4gICAgICByZXR1cm4gYW5jZXN0b3JzLm1hcChwb3NpdGlvbikucmV2ZXJzZSgpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGZyb21PZmZzZXRQYXRoXG4gICAqXG4gICAqIHJldHVybiBlbGVtZW50IGZyb20gb2Zmc2V0UGF0aChhcnJheSBvZiBvZmZzZXQpXG4gICAqXG4gICAqIEBwYXJhbSB7Tm9kZX0gYW5jZXN0b3IgLSBhbmNlc3RvciBub2RlXG4gICAqIEBwYXJhbSB7YXJyYXl9IG9mZnNldHMgLSBvZmZzZXRQYXRoXG4gICAqL1xuICBmdW5jdGlvbiBmcm9tT2Zmc2V0UGF0aChhbmNlc3Rvciwgb2Zmc2V0cykge1xuICAgICAgdmFyIGN1cnJlbnQgPSBhbmNlc3RvcjtcbiAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBvZmZzZXRzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgICAgaWYgKGN1cnJlbnQuY2hpbGROb2Rlcy5sZW5ndGggPD0gb2Zmc2V0c1tpXSkge1xuICAgICAgICAgICAgICBjdXJyZW50ID0gY3VycmVudC5jaGlsZE5vZGVzW2N1cnJlbnQuY2hpbGROb2Rlcy5sZW5ndGggLSAxXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGN1cnJlbnQgPSBjdXJyZW50LmNoaWxkTm9kZXNbb2Zmc2V0c1tpXV07XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIGN1cnJlbnQ7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2Qgc3BsaXROb2RlXG4gICAqXG4gICAqIHNwbGl0IGVsZW1lbnQgb3IgI3RleHRcbiAgICpcbiAgICogQHBhcmFtIHtCb3VuZGFyeVBvaW50fSBwb2ludFxuICAgKiBAcGFyYW0ge09iamVjdH0gW29wdGlvbnNdXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gW29wdGlvbnMuaXNTa2lwUGFkZGluZ0JsYW5rSFRNTF0gLSBkZWZhdWx0OiBmYWxzZVxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IFtvcHRpb25zLmlzTm90U3BsaXRFZGdlUG9pbnRdIC0gZGVmYXVsdDogZmFsc2VcbiAgICogQHBhcmFtIHtCb29sZWFufSBbb3B0aW9ucy5pc0Rpc2NhcmRFbXB0eVNwbGl0c10gLSBkZWZhdWx0OiBmYWxzZVxuICAgKiBAcmV0dXJuIHtOb2RlfSByaWdodCBub2RlIG9mIGJvdW5kYXJ5UG9pbnRcbiAgICovXG4gIGZ1bmN0aW9uIHNwbGl0Tm9kZShwb2ludCwgb3B0aW9ucykge1xuICAgICAgdmFyIGlzU2tpcFBhZGRpbmdCbGFua0hUTUwgPSBvcHRpb25zICYmIG9wdGlvbnMuaXNTa2lwUGFkZGluZ0JsYW5rSFRNTDtcbiAgICAgIHZhciBpc05vdFNwbGl0RWRnZVBvaW50ID0gb3B0aW9ucyAmJiBvcHRpb25zLmlzTm90U3BsaXRFZGdlUG9pbnQ7XG4gICAgICB2YXIgaXNEaXNjYXJkRW1wdHlTcGxpdHMgPSBvcHRpb25zICYmIG9wdGlvbnMuaXNEaXNjYXJkRW1wdHlTcGxpdHM7XG4gICAgICBpZiAoaXNEaXNjYXJkRW1wdHlTcGxpdHMpIHtcbiAgICAgICAgICBpc1NraXBQYWRkaW5nQmxhbmtIVE1MID0gdHJ1ZTtcbiAgICAgIH1cbiAgICAgIC8vIGVkZ2UgY2FzZVxuICAgICAgaWYgKGlzRWRnZVBvaW50KHBvaW50KSAmJiAoaXNUZXh0KHBvaW50Lm5vZGUpIHx8IGlzTm90U3BsaXRFZGdlUG9pbnQpKSB7XG4gICAgICAgICAgaWYgKGlzTGVmdEVkZ2VQb2ludChwb2ludCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHBvaW50Lm5vZGU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYgKGlzUmlnaHRFZGdlUG9pbnQocG9pbnQpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBwb2ludC5ub2RlLm5leHRTaWJsaW5nO1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8vIHNwbGl0ICN0ZXh0XG4gICAgICBpZiAoaXNUZXh0KHBvaW50Lm5vZGUpKSB7XG4gICAgICAgICAgcmV0dXJuIHBvaW50Lm5vZGUuc3BsaXRUZXh0KHBvaW50Lm9mZnNldCk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgICB2YXIgY2hpbGROb2RlID0gcG9pbnQubm9kZS5jaGlsZE5vZGVzW3BvaW50Lm9mZnNldF07XG4gICAgICAgICAgdmFyIGNsb25lID0gaW5zZXJ0QWZ0ZXIocG9pbnQubm9kZS5jbG9uZU5vZGUoZmFsc2UpLCBwb2ludC5ub2RlKTtcbiAgICAgICAgICBhcHBlbmRDaGlsZE5vZGVzKGNsb25lLCBsaXN0TmV4dChjaGlsZE5vZGUpKTtcbiAgICAgICAgICBpZiAoIWlzU2tpcFBhZGRpbmdCbGFua0hUTUwpIHtcbiAgICAgICAgICAgICAgcGFkZGluZ0JsYW5rSFRNTChwb2ludC5ub2RlKTtcbiAgICAgICAgICAgICAgcGFkZGluZ0JsYW5rSFRNTChjbG9uZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChpc0Rpc2NhcmRFbXB0eVNwbGl0cykge1xuICAgICAgICAgICAgICBpZiAoaXNFbXB0eSQxKHBvaW50Lm5vZGUpKSB7XG4gICAgICAgICAgICAgICAgICByZW1vdmUocG9pbnQubm9kZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKGlzRW1wdHkkMShjbG9uZSkpIHtcbiAgICAgICAgICAgICAgICAgIHJlbW92ZShjbG9uZSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gcG9pbnQubm9kZS5uZXh0U2libGluZztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gY2xvbmU7XG4gICAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2Qgc3BsaXRUcmVlXG4gICAqXG4gICAqIHNwbGl0IHRyZWUgYnkgcG9pbnRcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSByb290IC0gc3BsaXQgcm9vdFxuICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc11cbiAgICogQHBhcmFtIHtCb29sZWFufSBbb3B0aW9ucy5pc1NraXBQYWRkaW5nQmxhbmtIVE1MXSAtIGRlZmF1bHQ6IGZhbHNlXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gW29wdGlvbnMuaXNOb3RTcGxpdEVkZ2VQb2ludF0gLSBkZWZhdWx0OiBmYWxzZVxuICAgKiBAcmV0dXJuIHtOb2RlfSByaWdodCBub2RlIG9mIGJvdW5kYXJ5UG9pbnRcbiAgICovXG4gIGZ1bmN0aW9uIHNwbGl0VHJlZShyb290LCBwb2ludCwgb3B0aW9ucykge1xuICAgICAgLy8gZXgpIFsjdGV4dCwgPHNwYW4+LCA8cD5dXG4gICAgICB2YXIgYW5jZXN0b3JzID0gbGlzdEFuY2VzdG9yKHBvaW50Lm5vZGUsIGZ1bmMuZXEocm9vdCkpO1xuICAgICAgaWYgKCFhbmNlc3RvcnMubGVuZ3RoKSB7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG4gICAgICBlbHNlIGlmIChhbmNlc3RvcnMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgcmV0dXJuIHNwbGl0Tm9kZShwb2ludCwgb3B0aW9ucyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gYW5jZXN0b3JzLnJlZHVjZShmdW5jdGlvbiAobm9kZSwgcGFyZW50KSB7XG4gICAgICAgICAgaWYgKG5vZGUgPT09IHBvaW50Lm5vZGUpIHtcbiAgICAgICAgICAgICAgbm9kZSA9IHNwbGl0Tm9kZShwb2ludCwgb3B0aW9ucyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBzcGxpdE5vZGUoe1xuICAgICAgICAgICAgICBub2RlOiBwYXJlbnQsXG4gICAgICAgICAgICAgIG9mZnNldDogbm9kZSA/IHBvc2l0aW9uKG5vZGUpIDogbm9kZUxlbmd0aChwYXJlbnQpXG4gICAgICAgICAgfSwgb3B0aW9ucyk7XG4gICAgICB9KTtcbiAgfVxuICAvKipcbiAgICogc3BsaXQgcG9pbnRcbiAgICpcbiAgICogQHBhcmFtIHtQb2ludH0gcG9pbnRcbiAgICogQHBhcmFtIHtCb29sZWFufSBpc0lubGluZVxuICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAqL1xuICBmdW5jdGlvbiBzcGxpdFBvaW50KHBvaW50LCBpc0lubGluZSkge1xuICAgICAgLy8gZmluZCBzcGxpdFJvb3QsIGNvbnRhaW5lclxuICAgICAgLy8gIC0gaW5saW5lOiBzcGxpdFJvb3QgaXMgYSBjaGlsZCBvZiBwYXJhZ3JhcGhcbiAgICAgIC8vICAtIGJsb2NrOiBzcGxpdFJvb3QgaXMgYSBjaGlsZCBvZiBib2R5Q29udGFpbmVyXG4gICAgICB2YXIgcHJlZCA9IGlzSW5saW5lID8gaXNQYXJhIDogaXNCb2R5Q29udGFpbmVyO1xuICAgICAgdmFyIGFuY2VzdG9ycyA9IGxpc3RBbmNlc3Rvcihwb2ludC5ub2RlLCBwcmVkKTtcbiAgICAgIHZhciB0b3BBbmNlc3RvciA9IGxpc3RzLmxhc3QoYW5jZXN0b3JzKSB8fCBwb2ludC5ub2RlO1xuICAgICAgdmFyIHNwbGl0Um9vdCwgY29udGFpbmVyO1xuICAgICAgaWYgKHByZWQodG9wQW5jZXN0b3IpKSB7XG4gICAgICAgICAgc3BsaXRSb290ID0gYW5jZXN0b3JzW2FuY2VzdG9ycy5sZW5ndGggLSAyXTtcbiAgICAgICAgICBjb250YWluZXIgPSB0b3BBbmNlc3RvcjtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICAgIHNwbGl0Um9vdCA9IHRvcEFuY2VzdG9yO1xuICAgICAgICAgIGNvbnRhaW5lciA9IHNwbGl0Um9vdC5wYXJlbnROb2RlO1xuICAgICAgfVxuICAgICAgLy8gaWYgc3BsaXRSb290IGlzIGV4aXN0cywgc3BsaXQgd2l0aCBzcGxpdFRyZWVcbiAgICAgIHZhciBwaXZvdCA9IHNwbGl0Um9vdCAmJiBzcGxpdFRyZWUoc3BsaXRSb290LCBwb2ludCwge1xuICAgICAgICAgIGlzU2tpcFBhZGRpbmdCbGFua0hUTUw6IGlzSW5saW5lLFxuICAgICAgICAgIGlzTm90U3BsaXRFZGdlUG9pbnQ6IGlzSW5saW5lXG4gICAgICB9KTtcbiAgICAgIC8vIGlmIGNvbnRhaW5lciBpcyBwb2ludC5ub2RlLCBmaW5kIHBpdm90IHdpdGggcG9pbnQub2Zmc2V0XG4gICAgICBpZiAoIXBpdm90ICYmIGNvbnRhaW5lciA9PT0gcG9pbnQubm9kZSkge1xuICAgICAgICAgIHBpdm90ID0gcG9pbnQubm9kZS5jaGlsZE5vZGVzW3BvaW50Lm9mZnNldF07XG4gICAgICB9XG4gICAgICByZXR1cm4ge1xuICAgICAgICAgIHJpZ2h0Tm9kZTogcGl2b3QsXG4gICAgICAgICAgY29udGFpbmVyOiBjb250YWluZXJcbiAgICAgIH07XG4gIH1cbiAgZnVuY3Rpb24gY3JlYXRlKG5vZGVOYW1lKSB7XG4gICAgICByZXR1cm4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChub2RlTmFtZSk7XG4gIH1cbiAgZnVuY3Rpb24gY3JlYXRlVGV4dCh0ZXh0KSB7XG4gICAgICByZXR1cm4gZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodGV4dCk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcmVtb3ZlXG4gICAqXG4gICAqIHJlbW92ZSBub2RlLCAoaXNSZW1vdmVDaGlsZDogcmVtb3ZlIGNoaWxkIG9yIG5vdClcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNSZW1vdmVDaGlsZFxuICAgKi9cbiAgZnVuY3Rpb24gcmVtb3ZlKG5vZGUsIGlzUmVtb3ZlQ2hpbGQpIHtcbiAgICAgIGlmICghbm9kZSB8fCAhbm9kZS5wYXJlbnROb2RlKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuICAgICAgaWYgKG5vZGUucmVtb3ZlTm9kZSkge1xuICAgICAgICAgIHJldHVybiBub2RlLnJlbW92ZU5vZGUoaXNSZW1vdmVDaGlsZCk7XG4gICAgICB9XG4gICAgICB2YXIgcGFyZW50ID0gbm9kZS5wYXJlbnROb2RlO1xuICAgICAgaWYgKCFpc1JlbW92ZUNoaWxkKSB7XG4gICAgICAgICAgdmFyIG5vZGVzID0gW107XG4gICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IG5vZGUuY2hpbGROb2Rlcy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgICAgICBub2Rlcy5wdXNoKG5vZGUuY2hpbGROb2Rlc1tpXSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBub2Rlcy5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICAgICAgICBwYXJlbnQuaW5zZXJ0QmVmb3JlKG5vZGVzW2ldLCBub2RlKTtcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICBwYXJlbnQucmVtb3ZlQ2hpbGQobm9kZSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcmVtb3ZlV2hpbGVcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IHByZWRcbiAgICovXG4gIGZ1bmN0aW9uIHJlbW92ZVdoaWxlKG5vZGUsIHByZWQpIHtcbiAgICAgIHdoaWxlIChub2RlKSB7XG4gICAgICAgICAgaWYgKGlzRWRpdGFibGUobm9kZSkgfHwgIXByZWQobm9kZSkpIHtcbiAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBwYXJlbnQgPSBub2RlLnBhcmVudE5vZGU7XG4gICAgICAgICAgcmVtb3ZlKG5vZGUpO1xuICAgICAgICAgIG5vZGUgPSBwYXJlbnQ7XG4gICAgICB9XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgcmVwbGFjZVxuICAgKlxuICAgKiByZXBsYWNlIG5vZGUgd2l0aCBwcm92aWRlZCBub2RlTmFtZVxuICAgKlxuICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICogQHBhcmFtIHtTdHJpbmd9IG5vZGVOYW1lXG4gICAqIEByZXR1cm4ge05vZGV9IC0gbmV3IG5vZGVcbiAgICovXG4gIGZ1bmN0aW9uIHJlcGxhY2Uobm9kZSwgbm9kZU5hbWUpIHtcbiAgICAgIGlmIChub2RlLm5vZGVOYW1lLnRvVXBwZXJDYXNlKCkgPT09IG5vZGVOYW1lLnRvVXBwZXJDYXNlKCkpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgIH1cbiAgICAgIHZhciBuZXdOb2RlID0gY3JlYXRlKG5vZGVOYW1lKTtcbiAgICAgIGlmIChub2RlLnN0eWxlLmNzc1RleHQpIHtcbiAgICAgICAgICBuZXdOb2RlLnN0eWxlLmNzc1RleHQgPSBub2RlLnN0eWxlLmNzc1RleHQ7XG4gICAgICB9XG4gICAgICBhcHBlbmRDaGlsZE5vZGVzKG5ld05vZGUsIGxpc3RzLmZyb20obm9kZS5jaGlsZE5vZGVzKSk7XG4gICAgICBpbnNlcnRBZnRlcihuZXdOb2RlLCBub2RlKTtcbiAgICAgIHJlbW92ZShub2RlKTtcbiAgICAgIHJldHVybiBuZXdOb2RlO1xuICB9XG4gIHZhciBpc1RleHRhcmVhID0gbWFrZVByZWRCeU5vZGVOYW1lKCdURVhUQVJFQScpO1xuICAvKipcbiAgICogQHBhcmFtIHtqUXVlcnl9ICRub2RlXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gW3N0cmlwTGluZWJyZWFrc10gLSBkZWZhdWx0OiBmYWxzZVxuICAgKi9cbiAgZnVuY3Rpb24gdmFsdWUoJG5vZGUsIHN0cmlwTGluZWJyZWFrcykge1xuICAgICAgdmFyIHZhbCA9IGlzVGV4dGFyZWEoJG5vZGVbMF0pID8gJG5vZGUudmFsKCkgOiAkbm9kZS5odG1sKCk7XG4gICAgICBpZiAoc3RyaXBMaW5lYnJlYWtzKSB7XG4gICAgICAgICAgcmV0dXJuIHZhbC5yZXBsYWNlKC9bXFxuXFxyXS9nLCAnJyk7XG4gICAgICB9XG4gICAgICByZXR1cm4gdmFsO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGh0bWxcbiAgICpcbiAgICogZ2V0IHRoZSBIVE1MIGNvbnRlbnRzIG9mIG5vZGVcbiAgICpcbiAgICogQHBhcmFtIHtqUXVlcnl9ICRub2RlXG4gICAqIEBwYXJhbSB7Qm9vbGVhbn0gW2lzTmV3bGluZU9uQmxvY2tdXG4gICAqL1xuICBmdW5jdGlvbiBodG1sKCRub2RlLCBpc05ld2xpbmVPbkJsb2NrKSB7XG4gICAgICB2YXIgbWFya3VwID0gdmFsdWUoJG5vZGUpO1xuICAgICAgaWYgKGlzTmV3bGluZU9uQmxvY2spIHtcbiAgICAgICAgICB2YXIgcmVnZXhUYWcgPSAvPChcXC8/KShcXGIoPyEhKVtePlxcc10qKSguKj8pKFxccypcXC8/PikvZztcbiAgICAgICAgICBtYXJrdXAgPSBtYXJrdXAucmVwbGFjZShyZWdleFRhZywgZnVuY3Rpb24gKG1hdGNoLCBlbmRTbGFzaCwgbmFtZSkge1xuICAgICAgICAgICAgICBuYW1lID0gbmFtZS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICB2YXIgaXNFbmRPZklubGluZUNvbnRhaW5lciA9IC9eRElWfF5URHxeVEh8XlB8XkxJfF5IWzEtN10vLnRlc3QobmFtZSkgJiZcbiAgICAgICAgICAgICAgICAgICEhZW5kU2xhc2g7XG4gICAgICAgICAgICAgIHZhciBpc0Jsb2NrTm9kZSA9IC9eQkxPQ0tRVU9URXxeVEFCTEV8XlRCT0RZfF5UUnxeSFJ8XlVMfF5PTC8udGVzdChuYW1lKTtcbiAgICAgICAgICAgICAgcmV0dXJuIG1hdGNoICsgKChpc0VuZE9mSW5saW5lQ29udGFpbmVyIHx8IGlzQmxvY2tOb2RlKSA/ICdcXG4nIDogJycpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIG1hcmt1cCA9IG1hcmt1cC50cmltKCk7XG4gICAgICB9XG4gICAgICByZXR1cm4gbWFya3VwO1xuICB9XG4gIGZ1bmN0aW9uIHBvc0Zyb21QbGFjZWhvbGRlcihwbGFjZWhvbGRlcikge1xuICAgICAgdmFyICRwbGFjZWhvbGRlciA9ICQkMShwbGFjZWhvbGRlcik7XG4gICAgICB2YXIgcG9zID0gJHBsYWNlaG9sZGVyLm9mZnNldCgpO1xuICAgICAgdmFyIGhlaWdodCA9ICRwbGFjZWhvbGRlci5vdXRlckhlaWdodCh0cnVlKTsgLy8gaW5jbHVkZSBtYXJnaW5cbiAgICAgIHJldHVybiB7XG4gICAgICAgICAgbGVmdDogcG9zLmxlZnQsXG4gICAgICAgICAgdG9wOiBwb3MudG9wICsgaGVpZ2h0XG4gICAgICB9O1xuICB9XG4gIGZ1bmN0aW9uIGF0dGFjaEV2ZW50cygkbm9kZSwgZXZlbnRzKSB7XG4gICAgICBPYmplY3Qua2V5cyhldmVudHMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICRub2RlLm9uKGtleSwgZXZlbnRzW2tleV0pO1xuICAgICAgfSk7XG4gIH1cbiAgZnVuY3Rpb24gZGV0YWNoRXZlbnRzKCRub2RlLCBldmVudHMpIHtcbiAgICAgIE9iamVjdC5rZXlzKGV2ZW50cykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgJG5vZGUub2ZmKGtleSwgZXZlbnRzW2tleV0pO1xuICAgICAgfSk7XG4gIH1cbiAgLyoqXG4gICAqIEBtZXRob2QgaXNDdXN0b21TdHlsZVRhZ1xuICAgKlxuICAgKiBhc3NlcnQgaWYgYSBub2RlIGNvbnRhaW5zIGEgXCJub3RlLXN0eWxldGFnXCIgY2xhc3MsXG4gICAqIHdoaWNoIGltcGxpZXMgdGhhdCdzIGEgY3VzdG9tLW1hZGUgc3R5bGUgdGFnIG5vZGVcbiAgICpcbiAgICogQHBhcmFtIHtOb2RlfSBhbiBIVE1MIERPTSBub2RlXG4gICAqL1xuICBmdW5jdGlvbiBpc0N1c3RvbVN0eWxlVGFnKG5vZGUpIHtcbiAgICAgIHJldHVybiBub2RlICYmICFpc1RleHQobm9kZSkgJiYgbGlzdHMuY29udGFpbnMobm9kZS5jbGFzc0xpc3QsICdub3RlLXN0eWxldGFnJyk7XG4gIH1cbiAgdmFyIGRvbSA9IHtcbiAgICAgIC8qKiBAcHJvcGVydHkge1N0cmluZ30gTkJTUF9DSEFSICovXG4gICAgICBOQlNQX0NIQVI6IE5CU1BfQ0hBUixcbiAgICAgIC8qKiBAcHJvcGVydHkge1N0cmluZ30gWkVST19XSURUSF9OQlNQX0NIQVIgKi9cbiAgICAgIFpFUk9fV0lEVEhfTkJTUF9DSEFSOiBaRVJPX1dJRFRIX05CU1BfQ0hBUixcbiAgICAgIC8qKiBAcHJvcGVydHkge1N0cmluZ30gYmxhbmsgKi9cbiAgICAgIGJsYW5rOiBibGFua0hUTUwsXG4gICAgICAvKiogQHByb3BlcnR5IHtTdHJpbmd9IGVtcHR5UGFyYSAqL1xuICAgICAgZW1wdHlQYXJhOiBcIjxwPlwiICsgYmxhbmtIVE1MICsgXCI8L3A+XCIsXG4gICAgICBtYWtlUHJlZEJ5Tm9kZU5hbWU6IG1ha2VQcmVkQnlOb2RlTmFtZSxcbiAgICAgIGlzRWRpdGFibGU6IGlzRWRpdGFibGUsXG4gICAgICBpc0NvbnRyb2xTaXppbmc6IGlzQ29udHJvbFNpemluZyxcbiAgICAgIGlzVGV4dDogaXNUZXh0LFxuICAgICAgaXNFbGVtZW50OiBpc0VsZW1lbnQsXG4gICAgICBpc1ZvaWQ6IGlzVm9pZCxcbiAgICAgIGlzUGFyYTogaXNQYXJhLFxuICAgICAgaXNQdXJlUGFyYTogaXNQdXJlUGFyYSxcbiAgICAgIGlzSGVhZGluZzogaXNIZWFkaW5nLFxuICAgICAgaXNJbmxpbmU6IGlzSW5saW5lLFxuICAgICAgaXNCbG9jazogZnVuYy5ub3QoaXNJbmxpbmUpLFxuICAgICAgaXNCb2R5SW5saW5lOiBpc0JvZHlJbmxpbmUsXG4gICAgICBpc0JvZHk6IGlzQm9keSxcbiAgICAgIGlzUGFyYUlubGluZTogaXNQYXJhSW5saW5lLFxuICAgICAgaXNQcmU6IGlzUHJlLFxuICAgICAgaXNMaXN0OiBpc0xpc3QsXG4gICAgICBpc1RhYmxlOiBpc1RhYmxlLFxuICAgICAgaXNEYXRhOiBpc0RhdGEsXG4gICAgICBpc0NlbGw6IGlzQ2VsbCxcbiAgICAgIGlzQmxvY2txdW90ZTogaXNCbG9ja3F1b3RlLFxuICAgICAgaXNCb2R5Q29udGFpbmVyOiBpc0JvZHlDb250YWluZXIsXG4gICAgICBpc0FuY2hvcjogaXNBbmNob3IsXG4gICAgICBpc0RpdjogbWFrZVByZWRCeU5vZGVOYW1lKCdESVYnKSxcbiAgICAgIGlzTGk6IGlzTGksXG4gICAgICBpc0JSOiBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0JSJyksXG4gICAgICBpc1NwYW46IG1ha2VQcmVkQnlOb2RlTmFtZSgnU1BBTicpLFxuICAgICAgaXNCOiBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0InKSxcbiAgICAgIGlzVTogbWFrZVByZWRCeU5vZGVOYW1lKCdVJyksXG4gICAgICBpc1M6IG1ha2VQcmVkQnlOb2RlTmFtZSgnUycpLFxuICAgICAgaXNJOiBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0knKSxcbiAgICAgIGlzSW1nOiBtYWtlUHJlZEJ5Tm9kZU5hbWUoJ0lNRycpLFxuICAgICAgaXNUZXh0YXJlYTogaXNUZXh0YXJlYSxcbiAgICAgIGlzRW1wdHk6IGlzRW1wdHkkMSxcbiAgICAgIGlzRW1wdHlBbmNob3I6IGZ1bmMuYW5kKGlzQW5jaG9yLCBpc0VtcHR5JDEpLFxuICAgICAgaXNDbG9zZXN0U2libGluZzogaXNDbG9zZXN0U2libGluZyxcbiAgICAgIHdpdGhDbG9zZXN0U2libGluZ3M6IHdpdGhDbG9zZXN0U2libGluZ3MsXG4gICAgICBub2RlTGVuZ3RoOiBub2RlTGVuZ3RoLFxuICAgICAgaXNMZWZ0RWRnZVBvaW50OiBpc0xlZnRFZGdlUG9pbnQsXG4gICAgICBpc1JpZ2h0RWRnZVBvaW50OiBpc1JpZ2h0RWRnZVBvaW50LFxuICAgICAgaXNFZGdlUG9pbnQ6IGlzRWRnZVBvaW50LFxuICAgICAgaXNMZWZ0RWRnZU9mOiBpc0xlZnRFZGdlT2YsXG4gICAgICBpc1JpZ2h0RWRnZU9mOiBpc1JpZ2h0RWRnZU9mLFxuICAgICAgaXNMZWZ0RWRnZVBvaW50T2Y6IGlzTGVmdEVkZ2VQb2ludE9mLFxuICAgICAgaXNSaWdodEVkZ2VQb2ludE9mOiBpc1JpZ2h0RWRnZVBvaW50T2YsXG4gICAgICBwcmV2UG9pbnQ6IHByZXZQb2ludCxcbiAgICAgIG5leHRQb2ludDogbmV4dFBvaW50LFxuICAgICAgaXNTYW1lUG9pbnQ6IGlzU2FtZVBvaW50LFxuICAgICAgaXNWaXNpYmxlUG9pbnQ6IGlzVmlzaWJsZVBvaW50LFxuICAgICAgcHJldlBvaW50VW50aWw6IHByZXZQb2ludFVudGlsLFxuICAgICAgbmV4dFBvaW50VW50aWw6IG5leHRQb2ludFVudGlsLFxuICAgICAgaXNDaGFyUG9pbnQ6IGlzQ2hhclBvaW50LFxuICAgICAgd2Fsa1BvaW50OiB3YWxrUG9pbnQsXG4gICAgICBhbmNlc3RvcjogYW5jZXN0b3IsXG4gICAgICBzaW5nbGVDaGlsZEFuY2VzdG9yOiBzaW5nbGVDaGlsZEFuY2VzdG9yLFxuICAgICAgbGlzdEFuY2VzdG9yOiBsaXN0QW5jZXN0b3IsXG4gICAgICBsYXN0QW5jZXN0b3I6IGxhc3RBbmNlc3RvcixcbiAgICAgIGxpc3ROZXh0OiBsaXN0TmV4dCxcbiAgICAgIGxpc3RQcmV2OiBsaXN0UHJldixcbiAgICAgIGxpc3REZXNjZW5kYW50OiBsaXN0RGVzY2VuZGFudCxcbiAgICAgIGNvbW1vbkFuY2VzdG9yOiBjb21tb25BbmNlc3RvcixcbiAgICAgIHdyYXA6IHdyYXAsXG4gICAgICBpbnNlcnRBZnRlcjogaW5zZXJ0QWZ0ZXIsXG4gICAgICBhcHBlbmRDaGlsZE5vZGVzOiBhcHBlbmRDaGlsZE5vZGVzLFxuICAgICAgcG9zaXRpb246IHBvc2l0aW9uLFxuICAgICAgaGFzQ2hpbGRyZW46IGhhc0NoaWxkcmVuLFxuICAgICAgbWFrZU9mZnNldFBhdGg6IG1ha2VPZmZzZXRQYXRoLFxuICAgICAgZnJvbU9mZnNldFBhdGg6IGZyb21PZmZzZXRQYXRoLFxuICAgICAgc3BsaXRUcmVlOiBzcGxpdFRyZWUsXG4gICAgICBzcGxpdFBvaW50OiBzcGxpdFBvaW50LFxuICAgICAgY3JlYXRlOiBjcmVhdGUsXG4gICAgICBjcmVhdGVUZXh0OiBjcmVhdGVUZXh0LFxuICAgICAgcmVtb3ZlOiByZW1vdmUsXG4gICAgICByZW1vdmVXaGlsZTogcmVtb3ZlV2hpbGUsXG4gICAgICByZXBsYWNlOiByZXBsYWNlLFxuICAgICAgaHRtbDogaHRtbCxcbiAgICAgIHZhbHVlOiB2YWx1ZSxcbiAgICAgIHBvc0Zyb21QbGFjZWhvbGRlcjogcG9zRnJvbVBsYWNlaG9sZGVyLFxuICAgICAgYXR0YWNoRXZlbnRzOiBhdHRhY2hFdmVudHMsXG4gICAgICBkZXRhY2hFdmVudHM6IGRldGFjaEV2ZW50cyxcbiAgICAgIGlzQ3VzdG9tU3R5bGVUYWc6IGlzQ3VzdG9tU3R5bGVUYWdcbiAgfTtcblxuICB2YXIgQ29udGV4dCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIC8qKlxuICAgICAgICogQHBhcmFtIHtqUXVlcnl9ICRub3RlXG4gICAgICAgKiBAcGFyYW0ge09iamVjdH0gb3B0aW9uc1xuICAgICAgICovXG4gICAgICBmdW5jdGlvbiBDb250ZXh0KCRub3RlLCBvcHRpb25zKSB7XG4gICAgICAgICAgdGhpcy51aSA9ICQkMS5zdW1tZXJub3RlLnVpO1xuICAgICAgICAgIHRoaXMuJG5vdGUgPSAkbm90ZTtcbiAgICAgICAgICB0aGlzLm1lbW9zID0ge307XG4gICAgICAgICAgdGhpcy5tb2R1bGVzID0ge307XG4gICAgICAgICAgdGhpcy5sYXlvdXRJbmZvID0ge307XG4gICAgICAgICAgdGhpcy5vcHRpb25zID0gb3B0aW9ucztcbiAgICAgICAgICB0aGlzLmluaXRpYWxpemUoKTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogY3JlYXRlIGxheW91dCBhbmQgaW5pdGlhbGl6ZSBtb2R1bGVzIGFuZCBvdGhlciByZXNvdXJjZXNcbiAgICAgICAqL1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLmxheW91dEluZm8gPSB0aGlzLnVpLmNyZWF0ZUxheW91dCh0aGlzLiRub3RlLCB0aGlzLm9wdGlvbnMpO1xuICAgICAgICAgIHRoaXMuX2luaXRpYWxpemUoKTtcbiAgICAgICAgICB0aGlzLiRub3RlLmhpZGUoKTtcbiAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGRlc3Ryb3kgbW9kdWxlcyBhbmQgb3RoZXIgcmVzb3VyY2VzIGFuZCByZW1vdmUgbGF5b3V0XG4gICAgICAgKi9cbiAgICAgIENvbnRleHQucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5fZGVzdHJveSgpO1xuICAgICAgICAgIHRoaXMuJG5vdGUucmVtb3ZlRGF0YSgnc3VtbWVybm90ZScpO1xuICAgICAgICAgIHRoaXMudWkucmVtb3ZlTGF5b3V0KHRoaXMuJG5vdGUsIHRoaXMubGF5b3V0SW5mbyk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBkZXN0b3J5IG1vZHVsZXMgYW5kIG90aGVyIHJlc291cmNlcyBhbmQgaW5pdGlhbGl6ZSBpdCBhZ2FpblxuICAgICAgICovXG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgZGlzYWJsZWQgPSB0aGlzLmlzRGlzYWJsZWQoKTtcbiAgICAgICAgICB0aGlzLmNvZGUoZG9tLmVtcHR5UGFyYSk7XG4gICAgICAgICAgdGhpcy5fZGVzdHJveSgpO1xuICAgICAgICAgIHRoaXMuX2luaXRpYWxpemUoKTtcbiAgICAgICAgICBpZiAoZGlzYWJsZWQpIHtcbiAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIENvbnRleHQucHJvdG90eXBlLl9pbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgLy8gYWRkIG9wdGlvbmFsIGJ1dHRvbnNcbiAgICAgICAgICB2YXIgYnV0dG9ucyA9ICQkMS5leHRlbmQoe30sIHRoaXMub3B0aW9ucy5idXR0b25zKTtcbiAgICAgICAgICBPYmplY3Qua2V5cyhidXR0b25zKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgICAgX3RoaXMubWVtbygnYnV0dG9uLicgKyBrZXksIGJ1dHRvbnNba2V5XSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyIG1vZHVsZXMgPSAkJDEuZXh0ZW5kKHt9LCB0aGlzLm9wdGlvbnMubW9kdWxlcywgJCQxLnN1bW1lcm5vdGUucGx1Z2lucyB8fCB7fSk7XG4gICAgICAgICAgLy8gYWRkIGFuZCBpbml0aWFsaXplIG1vZHVsZXNcbiAgICAgICAgICBPYmplY3Qua2V5cyhtb2R1bGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgICAgX3RoaXMubW9kdWxlKGtleSwgbW9kdWxlc1trZXldLCB0cnVlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBPYmplY3Qua2V5cyh0aGlzLm1vZHVsZXMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgICBfdGhpcy5pbml0aWFsaXplTW9kdWxlKGtleSk7XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUuX2Rlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICAvLyBkZXN0cm95IG1vZHVsZXMgd2l0aCByZXZlcnNlZCBvcmRlclxuICAgICAgICAgIE9iamVjdC5rZXlzKHRoaXMubW9kdWxlcykucmV2ZXJzZSgpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgICAgICBfdGhpcy5yZW1vdmVNb2R1bGUoa2V5KTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBPYmplY3Qua2V5cyh0aGlzLm1lbW9zKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgICAgX3RoaXMucmVtb3ZlTWVtbyhrZXkpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIHRyaWdnZXIgY3VzdG9tIG9uRGVzdHJveSBjYWxsYmFja1xuICAgICAgICAgIHRoaXMudHJpZ2dlckV2ZW50KCdkZXN0cm95JywgdGhpcyk7XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUuY29kZSA9IGZ1bmN0aW9uIChodG1sKSB7XG4gICAgICAgICAgdmFyIGlzQWN0aXZhdGVkID0gdGhpcy5pbnZva2UoJ2NvZGV2aWV3LmlzQWN0aXZhdGVkJyk7XG4gICAgICAgICAgaWYgKGh0bWwgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgICB0aGlzLmludm9rZSgnY29kZXZpZXcuc3luYycpO1xuICAgICAgICAgICAgICByZXR1cm4gaXNBY3RpdmF0ZWQgPyB0aGlzLmxheW91dEluZm8uY29kYWJsZS52YWwoKSA6IHRoaXMubGF5b3V0SW5mby5lZGl0YWJsZS5odG1sKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBpZiAoaXNBY3RpdmF0ZWQpIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMubGF5b3V0SW5mby5jb2RhYmxlLnZhbChodG1sKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMubGF5b3V0SW5mby5lZGl0YWJsZS5odG1sKGh0bWwpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRoaXMuJG5vdGUudmFsKGh0bWwpO1xuICAgICAgICAgICAgICB0aGlzLnRyaWdnZXJFdmVudCgnY2hhbmdlJywgaHRtbCwgdGhpcy5sYXlvdXRJbmZvLmVkaXRhYmxlKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUuaXNEaXNhYmxlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5sYXlvdXRJbmZvLmVkaXRhYmxlLmF0dHIoJ2NvbnRlbnRlZGl0YWJsZScpID09PSAnZmFsc2UnO1xuICAgICAgfTtcbiAgICAgIENvbnRleHQucHJvdG90eXBlLmVuYWJsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLmxheW91dEluZm8uZWRpdGFibGUuYXR0cignY29udGVudGVkaXRhYmxlJywgdHJ1ZSk7XG4gICAgICAgICAgdGhpcy5pbnZva2UoJ3Rvb2xiYXIuYWN0aXZhdGUnLCB0cnVlKTtcbiAgICAgICAgICB0aGlzLnRyaWdnZXJFdmVudCgnZGlzYWJsZScsIGZhbHNlKTtcbiAgICAgIH07XG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5kaXNhYmxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIC8vIGNsb3NlIGNvZGV2aWV3IGlmIGNvZGV2aWV3IGlzIG9wZW5kXG4gICAgICAgICAgaWYgKHRoaXMuaW52b2tlKCdjb2Rldmlldy5pc0FjdGl2YXRlZCcpKSB7XG4gICAgICAgICAgICAgIHRoaXMuaW52b2tlKCdjb2Rldmlldy5kZWFjdGl2YXRlJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMubGF5b3V0SW5mby5lZGl0YWJsZS5hdHRyKCdjb250ZW50ZWRpdGFibGUnLCBmYWxzZSk7XG4gICAgICAgICAgdGhpcy5pbnZva2UoJ3Rvb2xiYXIuZGVhY3RpdmF0ZScsIHRydWUpO1xuICAgICAgICAgIHRoaXMudHJpZ2dlckV2ZW50KCdkaXNhYmxlJywgdHJ1ZSk7XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUudHJpZ2dlckV2ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBuYW1lc3BhY2UgPSBsaXN0cy5oZWFkKGFyZ3VtZW50cyk7XG4gICAgICAgICAgdmFyIGFyZ3MgPSBsaXN0cy50YWlsKGxpc3RzLmZyb20oYXJndW1lbnRzKSk7XG4gICAgICAgICAgdmFyIGNhbGxiYWNrID0gdGhpcy5vcHRpb25zLmNhbGxiYWNrc1tmdW5jLm5hbWVzcGFjZVRvQ2FtZWwobmFtZXNwYWNlLCAnb24nKV07XG4gICAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgIGNhbGxiYWNrLmFwcGx5KHRoaXMuJG5vdGVbMF0sIGFyZ3MpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLiRub3RlLnRyaWdnZXIoJ3N1bW1lcm5vdGUuJyArIG5hbWVzcGFjZSwgYXJncyk7XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUuaW5pdGlhbGl6ZU1vZHVsZSA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICB2YXIgbW9kdWxlID0gdGhpcy5tb2R1bGVzW2tleV07XG4gICAgICAgICAgbW9kdWxlLnNob3VsZEluaXRpYWxpemUgPSBtb2R1bGUuc2hvdWxkSW5pdGlhbGl6ZSB8fCBmdW5jLm9rO1xuICAgICAgICAgIGlmICghbW9kdWxlLnNob3VsZEluaXRpYWxpemUoKSkge1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIGluaXRpYWxpemUgbW9kdWxlXG4gICAgICAgICAgaWYgKG1vZHVsZS5pbml0aWFsaXplKSB7XG4gICAgICAgICAgICAgIG1vZHVsZS5pbml0aWFsaXplKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIGF0dGFjaCBldmVudHNcbiAgICAgICAgICBpZiAobW9kdWxlLmV2ZW50cykge1xuICAgICAgICAgICAgICBkb20uYXR0YWNoRXZlbnRzKHRoaXMuJG5vdGUsIG1vZHVsZS5ldmVudHMpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5tb2R1bGUgPSBmdW5jdGlvbiAoa2V5LCBNb2R1bGVDbGFzcywgd2l0aG91dEludGlhbGl6ZSkge1xuICAgICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICAgIHJldHVybiB0aGlzLm1vZHVsZXNba2V5XTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5tb2R1bGVzW2tleV0gPSBuZXcgTW9kdWxlQ2xhc3ModGhpcyk7XG4gICAgICAgICAgaWYgKCF3aXRob3V0SW50aWFsaXplKSB7XG4gICAgICAgICAgICAgIHRoaXMuaW5pdGlhbGl6ZU1vZHVsZShrZXkpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5yZW1vdmVNb2R1bGUgPSBmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgdmFyIG1vZHVsZSA9IHRoaXMubW9kdWxlc1trZXldO1xuICAgICAgICAgIGlmIChtb2R1bGUuc2hvdWxkSW5pdGlhbGl6ZSgpKSB7XG4gICAgICAgICAgICAgIGlmIChtb2R1bGUuZXZlbnRzKSB7XG4gICAgICAgICAgICAgICAgICBkb20uZGV0YWNoRXZlbnRzKHRoaXMuJG5vdGUsIG1vZHVsZS5ldmVudHMpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmIChtb2R1bGUuZGVzdHJveSkge1xuICAgICAgICAgICAgICAgICAgbW9kdWxlLmRlc3Ryb3koKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBkZWxldGUgdGhpcy5tb2R1bGVzW2tleV07XG4gICAgICB9O1xuICAgICAgQ29udGV4dC5wcm90b3R5cGUubWVtbyA9IGZ1bmN0aW9uIChrZXksIG9iaikge1xuICAgICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICAgIHJldHVybiB0aGlzLm1lbW9zW2tleV07XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMubWVtb3Nba2V5XSA9IG9iajtcbiAgICAgIH07XG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5yZW1vdmVNZW1vID0gZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIGlmICh0aGlzLm1lbW9zW2tleV0gJiYgdGhpcy5tZW1vc1trZXldLmRlc3Ryb3kpIHtcbiAgICAgICAgICAgICAgdGhpcy5tZW1vc1trZXldLmRlc3Ryb3koKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZGVsZXRlIHRoaXMubWVtb3Nba2V5XTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIFNvbWUgYnV0dG9ucyBuZWVkIHRvIGNoYW5nZSB0aGVpciB2aXN1YWwgc3R5bGUgaW1tZWRpYXRlbHkgb25jZSB0aGV5IGdldCBwcmVzc2VkXG4gICAgICAgKi9cbiAgICAgIENvbnRleHQucHJvdG90eXBlLmNyZWF0ZUludm9rZUhhbmRsZXJBbmRVcGRhdGVTdGF0ZSA9IGZ1bmN0aW9uIChuYW1lc3BhY2UsIHZhbHVlKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIF90aGlzLmNyZWF0ZUludm9rZUhhbmRsZXIobmFtZXNwYWNlLCB2YWx1ZSkoZXZlbnQpO1xuICAgICAgICAgICAgICBfdGhpcy5pbnZva2UoJ2J1dHRvbnMudXBkYXRlQ3VycmVudFN0eWxlJyk7XG4gICAgICAgICAgfTtcbiAgICAgIH07XG4gICAgICBDb250ZXh0LnByb3RvdHlwZS5jcmVhdGVJbnZva2VIYW5kbGVyID0gZnVuY3Rpb24gKG5hbWVzcGFjZSwgdmFsdWUpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgdmFyICR0YXJnZXQgPSAkJDEoZXZlbnQudGFyZ2V0KTtcbiAgICAgICAgICAgICAgX3RoaXMuaW52b2tlKG5hbWVzcGFjZSwgdmFsdWUgfHwgJHRhcmdldC5jbG9zZXN0KCdbZGF0YS12YWx1ZV0nKS5kYXRhKCd2YWx1ZScpLCAkdGFyZ2V0KTtcbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIENvbnRleHQucHJvdG90eXBlLmludm9rZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgbmFtZXNwYWNlID0gbGlzdHMuaGVhZChhcmd1bWVudHMpO1xuICAgICAgICAgIHZhciBhcmdzID0gbGlzdHMudGFpbChsaXN0cy5mcm9tKGFyZ3VtZW50cykpO1xuICAgICAgICAgIHZhciBzcGxpdHMgPSBuYW1lc3BhY2Uuc3BsaXQoJy4nKTtcbiAgICAgICAgICB2YXIgaGFzU2VwYXJhdG9yID0gc3BsaXRzLmxlbmd0aCA+IDE7XG4gICAgICAgICAgdmFyIG1vZHVsZU5hbWUgPSBoYXNTZXBhcmF0b3IgJiYgbGlzdHMuaGVhZChzcGxpdHMpO1xuICAgICAgICAgIHZhciBtZXRob2ROYW1lID0gaGFzU2VwYXJhdG9yID8gbGlzdHMubGFzdChzcGxpdHMpIDogbGlzdHMuaGVhZChzcGxpdHMpO1xuICAgICAgICAgIHZhciBtb2R1bGUgPSB0aGlzLm1vZHVsZXNbbW9kdWxlTmFtZSB8fCAnZWRpdG9yJ107XG4gICAgICAgICAgaWYgKCFtb2R1bGVOYW1lICYmIHRoaXNbbWV0aG9kTmFtZV0pIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXNbbWV0aG9kTmFtZV0uYXBwbHkodGhpcywgYXJncyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYgKG1vZHVsZSAmJiBtb2R1bGVbbWV0aG9kTmFtZV0gJiYgbW9kdWxlLnNob3VsZEluaXRpYWxpemUoKSkge1xuICAgICAgICAgICAgICByZXR1cm4gbW9kdWxlW21ldGhvZE5hbWVdLmFwcGx5KG1vZHVsZSwgYXJncyk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIHJldHVybiBDb250ZXh0O1xuICB9KCkpO1xuXG4gICQkMS5mbi5leHRlbmQoe1xuICAgICAgLyoqXG4gICAgICAgKiBTdW1tZXJub3RlIEFQSVxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fFN0cmluZ31cbiAgICAgICAqIEByZXR1cm4ge3RoaXN9XG4gICAgICAgKi9cbiAgICAgIHN1bW1lcm5vdGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgdHlwZSA9ICQkMS50eXBlKGxpc3RzLmhlYWQoYXJndW1lbnRzKSk7XG4gICAgICAgICAgdmFyIGlzRXh0ZXJuYWxBUElDYWxsZWQgPSB0eXBlID09PSAnc3RyaW5nJztcbiAgICAgICAgICB2YXIgaGFzSW5pdE9wdGlvbnMgPSB0eXBlID09PSAnb2JqZWN0JztcbiAgICAgICAgICB2YXIgb3B0aW9ucyA9ICQkMS5leHRlbmQoe30sICQkMS5zdW1tZXJub3RlLm9wdGlvbnMsIGhhc0luaXRPcHRpb25zID8gbGlzdHMuaGVhZChhcmd1bWVudHMpIDoge30pO1xuICAgICAgICAgIC8vIFVwZGF0ZSBvcHRpb25zXG4gICAgICAgICAgb3B0aW9ucy5sYW5nSW5mbyA9ICQkMS5leHRlbmQodHJ1ZSwge30sICQkMS5zdW1tZXJub3RlLmxhbmdbJ2VuLVVTJ10sICQkMS5zdW1tZXJub3RlLmxhbmdbb3B0aW9ucy5sYW5nXSk7XG4gICAgICAgICAgb3B0aW9ucy5pY29ucyA9ICQkMS5leHRlbmQodHJ1ZSwge30sICQkMS5zdW1tZXJub3RlLm9wdGlvbnMuaWNvbnMsIG9wdGlvbnMuaWNvbnMpO1xuICAgICAgICAgIG9wdGlvbnMudG9vbHRpcCA9IG9wdGlvbnMudG9vbHRpcCA9PT0gJ2F1dG8nID8gIWVudi5pc1N1cHBvcnRUb3VjaCA6IG9wdGlvbnMudG9vbHRpcDtcbiAgICAgICAgICB0aGlzLmVhY2goZnVuY3Rpb24gKGlkeCwgbm90ZSkge1xuICAgICAgICAgICAgICB2YXIgJG5vdGUgPSAkJDEobm90ZSk7XG4gICAgICAgICAgICAgIGlmICghJG5vdGUuZGF0YSgnc3VtbWVybm90ZScpKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgY29udGV4dCA9IG5ldyBDb250ZXh0KCRub3RlLCBvcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICRub3RlLmRhdGEoJ3N1bW1lcm5vdGUnLCBjb250ZXh0KTtcbiAgICAgICAgICAgICAgICAgICRub3RlLmRhdGEoJ3N1bW1lcm5vdGUnKS50cmlnZ2VyRXZlbnQoJ2luaXQnLCBjb250ZXh0LmxheW91dEluZm8pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyICRub3RlID0gdGhpcy5maXJzdCgpO1xuICAgICAgICAgIGlmICgkbm90ZS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgdmFyIGNvbnRleHQgPSAkbm90ZS5kYXRhKCdzdW1tZXJub3RlJyk7XG4gICAgICAgICAgICAgIGlmIChpc0V4dGVybmFsQVBJQ2FsbGVkKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gY29udGV4dC5pbnZva2UuYXBwbHkoY29udGV4dCwgbGlzdHMuZnJvbShhcmd1bWVudHMpKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIGlmIChvcHRpb25zLmZvY3VzKSB7XG4gICAgICAgICAgICAgICAgICBjb250ZXh0Lmludm9rZSgnZWRpdG9yLmZvY3VzJyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9XG4gIH0pO1xuXG4gIC8qKlxuICAgKiByZXR1cm4gYm91bmRhcnlQb2ludCBmcm9tIFRleHRSYW5nZSwgaW5zcGlyZWQgYnkgQW5keSBOYSdzIEh1c2t5UmFuZ2UuanNcbiAgICpcbiAgICogQHBhcmFtIHtUZXh0UmFuZ2V9IHRleHRSYW5nZVxuICAgKiBAcGFyYW0ge0Jvb2xlYW59IGlzU3RhcnRcbiAgICogQHJldHVybiB7Qm91bmRhcnlQb2ludH1cbiAgICpcbiAgICogQHNlZSBodHRwOi8vbXNkbi5taWNyb3NvZnQuY29tL2VuLXVzL2xpYnJhcnkvaWUvbXM1MzU4NzIodj12cy44NSkuYXNweFxuICAgKi9cbiAgZnVuY3Rpb24gdGV4dFJhbmdlVG9Qb2ludCh0ZXh0UmFuZ2UsIGlzU3RhcnQpIHtcbiAgICAgIHZhciBjb250YWluZXIgPSB0ZXh0UmFuZ2UucGFyZW50RWxlbWVudCgpO1xuICAgICAgdmFyIG9mZnNldDtcbiAgICAgIHZhciB0ZXN0ZXIgPSBkb2N1bWVudC5ib2R5LmNyZWF0ZVRleHRSYW5nZSgpO1xuICAgICAgdmFyIHByZXZDb250YWluZXI7XG4gICAgICB2YXIgY2hpbGROb2RlcyA9IGxpc3RzLmZyb20oY29udGFpbmVyLmNoaWxkTm9kZXMpO1xuICAgICAgZm9yIChvZmZzZXQgPSAwOyBvZmZzZXQgPCBjaGlsZE5vZGVzLmxlbmd0aDsgb2Zmc2V0KyspIHtcbiAgICAgICAgICBpZiAoZG9tLmlzVGV4dChjaGlsZE5vZGVzW29mZnNldF0pKSB7XG4gICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0ZXN0ZXIubW92ZVRvRWxlbWVudFRleHQoY2hpbGROb2Rlc1tvZmZzZXRdKTtcbiAgICAgICAgICBpZiAodGVzdGVyLmNvbXBhcmVFbmRQb2ludHMoJ1N0YXJ0VG9TdGFydCcsIHRleHRSYW5nZSkgPj0gMCkge1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgICAgcHJldkNvbnRhaW5lciA9IGNoaWxkTm9kZXNbb2Zmc2V0XTtcbiAgICAgIH1cbiAgICAgIGlmIChvZmZzZXQgIT09IDAgJiYgZG9tLmlzVGV4dChjaGlsZE5vZGVzW29mZnNldCAtIDFdKSkge1xuICAgICAgICAgIHZhciB0ZXh0UmFuZ2VTdGFydCA9IGRvY3VtZW50LmJvZHkuY3JlYXRlVGV4dFJhbmdlKCk7XG4gICAgICAgICAgdmFyIGN1clRleHROb2RlID0gbnVsbDtcbiAgICAgICAgICB0ZXh0UmFuZ2VTdGFydC5tb3ZlVG9FbGVtZW50VGV4dChwcmV2Q29udGFpbmVyIHx8IGNvbnRhaW5lcik7XG4gICAgICAgICAgdGV4dFJhbmdlU3RhcnQuY29sbGFwc2UoIXByZXZDb250YWluZXIpO1xuICAgICAgICAgIGN1clRleHROb2RlID0gcHJldkNvbnRhaW5lciA/IHByZXZDb250YWluZXIubmV4dFNpYmxpbmcgOiBjb250YWluZXIuZmlyc3RDaGlsZDtcbiAgICAgICAgICB2YXIgcG9pbnRUZXN0ZXIgPSB0ZXh0UmFuZ2UuZHVwbGljYXRlKCk7XG4gICAgICAgICAgcG9pbnRUZXN0ZXIuc2V0RW5kUG9pbnQoJ1N0YXJ0VG9TdGFydCcsIHRleHRSYW5nZVN0YXJ0KTtcbiAgICAgICAgICB2YXIgdGV4dENvdW50ID0gcG9pbnRUZXN0ZXIudGV4dC5yZXBsYWNlKC9bXFxyXFxuXS9nLCAnJykubGVuZ3RoO1xuICAgICAgICAgIHdoaWxlICh0ZXh0Q291bnQgPiBjdXJUZXh0Tm9kZS5ub2RlVmFsdWUubGVuZ3RoICYmIGN1clRleHROb2RlLm5leHRTaWJsaW5nKSB7XG4gICAgICAgICAgICAgIHRleHRDb3VudCAtPSBjdXJUZXh0Tm9kZS5ub2RlVmFsdWUubGVuZ3RoO1xuICAgICAgICAgICAgICBjdXJUZXh0Tm9kZSA9IGN1clRleHROb2RlLm5leHRTaWJsaW5nO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBbd29ya2Fyb3VuZF0gZW5mb3JjZSBJRSB0byByZS1yZWZlcmVuY2UgY3VyVGV4dE5vZGUsIGhhY2tcbiAgICAgICAgICB2YXIgZHVtbXkgPSBjdXJUZXh0Tm9kZS5ub2RlVmFsdWU7IC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICAgICAgICBpZiAoaXNTdGFydCAmJiBjdXJUZXh0Tm9kZS5uZXh0U2libGluZyAmJiBkb20uaXNUZXh0KGN1clRleHROb2RlLm5leHRTaWJsaW5nKSAmJlxuICAgICAgICAgICAgICB0ZXh0Q291bnQgPT09IGN1clRleHROb2RlLm5vZGVWYWx1ZS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgdGV4dENvdW50IC09IGN1clRleHROb2RlLm5vZGVWYWx1ZS5sZW5ndGg7XG4gICAgICAgICAgICAgIGN1clRleHROb2RlID0gY3VyVGV4dE5vZGUubmV4dFNpYmxpbmc7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNvbnRhaW5lciA9IGN1clRleHROb2RlO1xuICAgICAgICAgIG9mZnNldCA9IHRleHRDb3VudDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB7XG4gICAgICAgICAgY29udDogY29udGFpbmVyLFxuICAgICAgICAgIG9mZnNldDogb2Zmc2V0XG4gICAgICB9O1xuICB9XG4gIC8qKlxuICAgKiByZXR1cm4gVGV4dFJhbmdlIGZyb20gYm91bmRhcnkgcG9pbnQgKGluc3BpcmVkIGJ5IGdvb2dsZSBjbG9zdXJlLWxpYnJhcnkpXG4gICAqIEBwYXJhbSB7Qm91bmRhcnlQb2ludH0gcG9pbnRcbiAgICogQHJldHVybiB7VGV4dFJhbmdlfVxuICAgKi9cbiAgZnVuY3Rpb24gcG9pbnRUb1RleHRSYW5nZShwb2ludCkge1xuICAgICAgdmFyIHRleHRSYW5nZUluZm8gPSBmdW5jdGlvbiAoY29udGFpbmVyLCBvZmZzZXQpIHtcbiAgICAgICAgICB2YXIgbm9kZSwgaXNDb2xsYXBzZVRvU3RhcnQ7XG4gICAgICAgICAgaWYgKGRvbS5pc1RleHQoY29udGFpbmVyKSkge1xuICAgICAgICAgICAgICB2YXIgcHJldlRleHROb2RlcyA9IGRvbS5saXN0UHJldihjb250YWluZXIsIGZ1bmMubm90KGRvbS5pc1RleHQpKTtcbiAgICAgICAgICAgICAgdmFyIHByZXZDb250YWluZXIgPSBsaXN0cy5sYXN0KHByZXZUZXh0Tm9kZXMpLnByZXZpb3VzU2libGluZztcbiAgICAgICAgICAgICAgbm9kZSA9IHByZXZDb250YWluZXIgfHwgY29udGFpbmVyLnBhcmVudE5vZGU7XG4gICAgICAgICAgICAgIG9mZnNldCArPSBsaXN0cy5zdW0obGlzdHMudGFpbChwcmV2VGV4dE5vZGVzKSwgZG9tLm5vZGVMZW5ndGgpO1xuICAgICAgICAgICAgICBpc0NvbGxhcHNlVG9TdGFydCA9ICFwcmV2Q29udGFpbmVyO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgbm9kZSA9IGNvbnRhaW5lci5jaGlsZE5vZGVzW29mZnNldF0gfHwgY29udGFpbmVyO1xuICAgICAgICAgICAgICBpZiAoZG9tLmlzVGV4dChub2RlKSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIHRleHRSYW5nZUluZm8obm9kZSwgMCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgb2Zmc2V0ID0gMDtcbiAgICAgICAgICAgICAgaXNDb2xsYXBzZVRvU3RhcnQgPSBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgbm9kZTogbm9kZSxcbiAgICAgICAgICAgICAgY29sbGFwc2VUb1N0YXJ0OiBpc0NvbGxhcHNlVG9TdGFydCxcbiAgICAgICAgICAgICAgb2Zmc2V0OiBvZmZzZXRcbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIHZhciB0ZXh0UmFuZ2UgPSBkb2N1bWVudC5ib2R5LmNyZWF0ZVRleHRSYW5nZSgpO1xuICAgICAgdmFyIGluZm8gPSB0ZXh0UmFuZ2VJbmZvKHBvaW50Lm5vZGUsIHBvaW50Lm9mZnNldCk7XG4gICAgICB0ZXh0UmFuZ2UubW92ZVRvRWxlbWVudFRleHQoaW5mby5ub2RlKTtcbiAgICAgIHRleHRSYW5nZS5jb2xsYXBzZShpbmZvLmNvbGxhcHNlVG9TdGFydCk7XG4gICAgICB0ZXh0UmFuZ2UubW92ZVN0YXJ0KCdjaGFyYWN0ZXInLCBpbmZvLm9mZnNldCk7XG4gICAgICByZXR1cm4gdGV4dFJhbmdlO1xuICB9XG4gIC8qKlxuICAgICAqIFdyYXBwZWQgUmFuZ2VcbiAgICAgKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7Tm9kZX0gc2MgLSBzdGFydCBjb250YWluZXJcbiAgICAgKiBAcGFyYW0ge051bWJlcn0gc28gLSBzdGFydCBvZmZzZXRcbiAgICAgKiBAcGFyYW0ge05vZGV9IGVjIC0gZW5kIGNvbnRhaW5lclxuICAgICAqIEBwYXJhbSB7TnVtYmVyfSBlbyAtIGVuZCBvZmZzZXRcbiAgICAgKi9cbiAgdmFyIFdyYXBwZWRSYW5nZSA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIFdyYXBwZWRSYW5nZShzYywgc28sIGVjLCBlbykge1xuICAgICAgICAgIHRoaXMuc2MgPSBzYztcbiAgICAgICAgICB0aGlzLnNvID0gc287XG4gICAgICAgICAgdGhpcy5lYyA9IGVjO1xuICAgICAgICAgIHRoaXMuZW8gPSBlbztcbiAgICAgICAgICAvLyBpc09uRWRpdGFibGU6IGp1ZGdlIHdoZXRoZXIgcmFuZ2UgaXMgb24gZWRpdGFibGUgb3Igbm90XG4gICAgICAgICAgdGhpcy5pc09uRWRpdGFibGUgPSB0aGlzLm1ha2VJc09uKGRvbS5pc0VkaXRhYmxlKTtcbiAgICAgICAgICAvLyBpc09uTGlzdDoganVkZ2Ugd2hldGhlciByYW5nZSBpcyBvbiBsaXN0IG5vZGUgb3Igbm90XG4gICAgICAgICAgdGhpcy5pc09uTGlzdCA9IHRoaXMubWFrZUlzT24oZG9tLmlzTGlzdCk7XG4gICAgICAgICAgLy8gaXNPbkFuY2hvcjoganVkZ2Ugd2hldGhlciByYW5nZSBpcyBvbiBhbmNob3Igbm9kZSBvciBub3RcbiAgICAgICAgICB0aGlzLmlzT25BbmNob3IgPSB0aGlzLm1ha2VJc09uKGRvbS5pc0FuY2hvcik7XG4gICAgICAgICAgLy8gaXNPbkNlbGw6IGp1ZGdlIHdoZXRoZXIgcmFuZ2UgaXMgb24gY2VsbCBub2RlIG9yIG5vdFxuICAgICAgICAgIHRoaXMuaXNPbkNlbGwgPSB0aGlzLm1ha2VJc09uKGRvbS5pc0NlbGwpO1xuICAgICAgICAgIC8vIGlzT25EYXRhOiBqdWRnZSB3aGV0aGVyIHJhbmdlIGlzIG9uIGRhdGEgbm9kZSBvciBub3RcbiAgICAgICAgICB0aGlzLmlzT25EYXRhID0gdGhpcy5tYWtlSXNPbihkb20uaXNEYXRhKTtcbiAgICAgIH1cbiAgICAgIC8vIG5hdGl2ZVJhbmdlOiBnZXQgbmF0aXZlUmFuZ2UgZnJvbSBzYywgc28sIGVjLCBlb1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5uYXRpdmVSYW5nZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAoZW52LmlzVzNDUmFuZ2VTdXBwb3J0KSB7XG4gICAgICAgICAgICAgIHZhciB3M2NSYW5nZSA9IGRvY3VtZW50LmNyZWF0ZVJhbmdlKCk7XG4gICAgICAgICAgICAgIHczY1JhbmdlLnNldFN0YXJ0KHRoaXMuc2MsIHRoaXMuc2MuZGF0YSAmJiB0aGlzLnNvID4gdGhpcy5zYy5kYXRhLmxlbmd0aCA/IDAgOiB0aGlzLnNvKTtcbiAgICAgICAgICAgICAgdzNjUmFuZ2Uuc2V0RW5kKHRoaXMuZWMsIHRoaXMuc2MuZGF0YSA/IE1hdGgubWluKHRoaXMuZW8sIHRoaXMuc2MuZGF0YS5sZW5ndGgpIDogdGhpcy5lbyk7XG4gICAgICAgICAgICAgIHJldHVybiB3M2NSYW5nZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHZhciB0ZXh0UmFuZ2UgPSBwb2ludFRvVGV4dFJhbmdlKHtcbiAgICAgICAgICAgICAgICAgIG5vZGU6IHRoaXMuc2MsXG4gICAgICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuc29cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHRleHRSYW5nZS5zZXRFbmRQb2ludCgnRW5kVG9FbmQnLCBwb2ludFRvVGV4dFJhbmdlKHtcbiAgICAgICAgICAgICAgICAgIG5vZGU6IHRoaXMuZWMsXG4gICAgICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuZW9cbiAgICAgICAgICAgICAgfSkpO1xuICAgICAgICAgICAgICByZXR1cm4gdGV4dFJhbmdlO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLmdldFBvaW50cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBzYzogdGhpcy5zYyxcbiAgICAgICAgICAgICAgc286IHRoaXMuc28sXG4gICAgICAgICAgICAgIGVjOiB0aGlzLmVjLFxuICAgICAgICAgICAgICBlbzogdGhpcy5lb1xuICAgICAgICAgIH07XG4gICAgICB9O1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5nZXRTdGFydFBvaW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIG5vZGU6IHRoaXMuc2MsXG4gICAgICAgICAgICAgIG9mZnNldDogdGhpcy5zb1xuICAgICAgICAgIH07XG4gICAgICB9O1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5nZXRFbmRQb2ludCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBub2RlOiB0aGlzLmVjLFxuICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuZW9cbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogc2VsZWN0IHVwZGF0ZSB2aXNpYmxlIHJhbmdlXG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuc2VsZWN0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBuYXRpdmVSbmcgPSB0aGlzLm5hdGl2ZVJhbmdlKCk7XG4gICAgICAgICAgaWYgKGVudi5pc1czQ1JhbmdlU3VwcG9ydCkge1xuICAgICAgICAgICAgICB2YXIgc2VsZWN0aW9uID0gZG9jdW1lbnQuZ2V0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICAgIGlmIChzZWxlY3Rpb24ucmFuZ2VDb3VudCA+IDApIHtcbiAgICAgICAgICAgICAgICAgIHNlbGVjdGlvbi5yZW1vdmVBbGxSYW5nZXMoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzZWxlY3Rpb24uYWRkUmFuZ2UobmF0aXZlUm5nKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIG5hdGl2ZVJuZy5zZWxlY3QoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBNb3ZlcyB0aGUgc2Nyb2xsYmFyIHRvIHN0YXJ0IGNvbnRhaW5lcihzYykgb2YgY3VycmVudCByYW5nZVxuICAgICAgICpcbiAgICAgICAqIEByZXR1cm4ge1dyYXBwZWRSYW5nZX1cbiAgICAgICAqL1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5zY3JvbGxJbnRvVmlldyA9IGZ1bmN0aW9uIChjb250YWluZXIpIHtcbiAgICAgICAgICB2YXIgaGVpZ2h0ID0gJCQxKGNvbnRhaW5lcikuaGVpZ2h0KCk7XG4gICAgICAgICAgaWYgKGNvbnRhaW5lci5zY3JvbGxUb3AgKyBoZWlnaHQgPCB0aGlzLnNjLm9mZnNldFRvcCkge1xuICAgICAgICAgICAgICBjb250YWluZXIuc2Nyb2xsVG9wICs9IE1hdGguYWJzKGNvbnRhaW5lci5zY3JvbGxUb3AgKyBoZWlnaHQgLSB0aGlzLnNjLm9mZnNldFRvcCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfVxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLm5vcm1hbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBAcGFyYW0ge0JvdW5kYXJ5UG9pbnR9IHBvaW50XG4gICAgICAgICAgICogQHBhcmFtIHtCb29sZWFufSBpc0xlZnRUb1JpZ2h0IC0gdHJ1ZTogcHJlZmVyIHRvIGNob29zZSByaWdodCBub2RlXG4gICAgICAgICAgICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC0gZmFsc2U6IHByZWZlciB0byBjaG9vc2UgbGVmdCBub2RlXG4gICAgICAgICAgICogQHJldHVybiB7Qm91bmRhcnlQb2ludH1cbiAgICAgICAgICAgKi9cbiAgICAgICAgICB2YXIgZ2V0VmlzaWJsZVBvaW50ID0gZnVuY3Rpb24gKHBvaW50LCBpc0xlZnRUb1JpZ2h0KSB7XG4gICAgICAgICAgICAgIC8vIEp1c3QgdXNlIHRoZSBnaXZlbiBwb2ludCBbWFhYOkFkaG9jXVxuICAgICAgICAgICAgICAvLyAgLSBjYXNlIDAxLiBpZiB0aGUgcG9pbnQgaXMgb24gdGhlIG1pZGRsZSBvZiB0aGUgbm9kZVxuICAgICAgICAgICAgICAvLyAgLSBjYXNlIDAyLiBpZiB0aGUgcG9pbnQgaXMgb24gdGhlIHJpZ2h0IGVkZ2UgYW5kIHByZWZlciB0byBjaG9vc2UgbGVmdCBub2RlXG4gICAgICAgICAgICAgIC8vICAtIGNhc2UgMDMuIGlmIHRoZSBwb2ludCBpcyBvbiB0aGUgbGVmdCBlZGdlIGFuZCBwcmVmZXIgdG8gY2hvb3NlIHJpZ2h0IG5vZGVcbiAgICAgICAgICAgICAgLy8gIC0gY2FzZSAwNC4gaWYgdGhlIHBvaW50IGlzIG9uIHRoZSByaWdodCBlZGdlIGFuZCBwcmVmZXIgdG8gY2hvb3NlIHJpZ2h0IG5vZGUgYnV0IHRoZSBub2RlIGlzIHZvaWRcbiAgICAgICAgICAgICAgLy8gIC0gY2FzZSAwNS4gaWYgdGhlIHBvaW50IGlzIG9uIHRoZSBsZWZ0IGVkZ2UgYW5kIHByZWZlciB0byBjaG9vc2UgbGVmdCBub2RlIGJ1dCB0aGUgbm9kZSBpcyB2b2lkXG4gICAgICAgICAgICAgIC8vICAtIGNhc2UgMDYuIGlmIHRoZSBwb2ludCBpcyBvbiB0aGUgYmxvY2sgbm9kZSBhbmQgdGhlcmUgaXMgbm8gY2hpbGRyZW5cbiAgICAgICAgICAgICAgaWYgKGRvbS5pc1Zpc2libGVQb2ludChwb2ludCkpIHtcbiAgICAgICAgICAgICAgICAgIGlmICghZG9tLmlzRWRnZVBvaW50KHBvaW50KSB8fFxuICAgICAgICAgICAgICAgICAgICAgIChkb20uaXNSaWdodEVkZ2VQb2ludChwb2ludCkgJiYgIWlzTGVmdFRvUmlnaHQpIHx8XG4gICAgICAgICAgICAgICAgICAgICAgKGRvbS5pc0xlZnRFZGdlUG9pbnQocG9pbnQpICYmIGlzTGVmdFRvUmlnaHQpIHx8XG4gICAgICAgICAgICAgICAgICAgICAgKGRvbS5pc1JpZ2h0RWRnZVBvaW50KHBvaW50KSAmJiBpc0xlZnRUb1JpZ2h0ICYmIGRvbS5pc1ZvaWQocG9pbnQubm9kZS5uZXh0U2libGluZykpIHx8XG4gICAgICAgICAgICAgICAgICAgICAgKGRvbS5pc0xlZnRFZGdlUG9pbnQocG9pbnQpICYmICFpc0xlZnRUb1JpZ2h0ICYmIGRvbS5pc1ZvaWQocG9pbnQubm9kZS5wcmV2aW91c1NpYmxpbmcpKSB8fFxuICAgICAgICAgICAgICAgICAgICAgIChkb20uaXNCbG9jayhwb2ludC5ub2RlKSAmJiBkb20uaXNFbXB0eShwb2ludC5ub2RlKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcG9pbnQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgLy8gcG9pbnQgb24gYmxvY2sncyBlZGdlXG4gICAgICAgICAgICAgIHZhciBibG9jayA9IGRvbS5hbmNlc3Rvcihwb2ludC5ub2RlLCBkb20uaXNCbG9jayk7XG4gICAgICAgICAgICAgIGlmICgoKGRvbS5pc0xlZnRFZGdlUG9pbnRPZihwb2ludCwgYmxvY2spIHx8IGRvbS5pc1ZvaWQoZG9tLnByZXZQb2ludChwb2ludCkubm9kZSkpICYmICFpc0xlZnRUb1JpZ2h0KSB8fFxuICAgICAgICAgICAgICAgICAgKChkb20uaXNSaWdodEVkZ2VQb2ludE9mKHBvaW50LCBibG9jaykgfHwgZG9tLmlzVm9pZChkb20ubmV4dFBvaW50KHBvaW50KS5ub2RlKSkgJiYgaXNMZWZ0VG9SaWdodCkpIHtcbiAgICAgICAgICAgICAgICAgIC8vIHJldHVybnMgcG9pbnQgYWxyZWFkeSBvbiB2aXNpYmxlIHBvaW50XG4gICAgICAgICAgICAgICAgICBpZiAoZG9tLmlzVmlzaWJsZVBvaW50KHBvaW50KSkge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBwb2ludDtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIC8vIHJldmVyc2UgZGlyZWN0aW9uXG4gICAgICAgICAgICAgICAgICBpc0xlZnRUb1JpZ2h0ID0gIWlzTGVmdFRvUmlnaHQ7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdmFyIG5leHRQb2ludCA9IGlzTGVmdFRvUmlnaHQgPyBkb20ubmV4dFBvaW50VW50aWwoZG9tLm5leHRQb2ludChwb2ludCksIGRvbS5pc1Zpc2libGVQb2ludClcbiAgICAgICAgICAgICAgICAgIDogZG9tLnByZXZQb2ludFVudGlsKGRvbS5wcmV2UG9pbnQocG9pbnQpLCBkb20uaXNWaXNpYmxlUG9pbnQpO1xuICAgICAgICAgICAgICByZXR1cm4gbmV4dFBvaW50IHx8IHBvaW50O1xuICAgICAgICAgIH07XG4gICAgICAgICAgdmFyIGVuZFBvaW50ID0gZ2V0VmlzaWJsZVBvaW50KHRoaXMuZ2V0RW5kUG9pbnQoKSwgZmFsc2UpO1xuICAgICAgICAgIHZhciBzdGFydFBvaW50ID0gdGhpcy5pc0NvbGxhcHNlZCgpID8gZW5kUG9pbnQgOiBnZXRWaXNpYmxlUG9pbnQodGhpcy5nZXRTdGFydFBvaW50KCksIHRydWUpO1xuICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKHN0YXJ0UG9pbnQubm9kZSwgc3RhcnRQb2ludC5vZmZzZXQsIGVuZFBvaW50Lm5vZGUsIGVuZFBvaW50Lm9mZnNldCk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiByZXR1cm5zIG1hdGNoZWQgbm9kZXMgb24gcmFuZ2VcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBbcHJlZF0gLSBwcmVkaWNhdGUgZnVuY3Rpb25cbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc11cbiAgICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gW29wdGlvbnMuaW5jbHVkZUFuY2VzdG9yXVxuICAgICAgICogQHBhcmFtIHtCb29sZWFufSBbb3B0aW9ucy5mdWxseUNvbnRhaW5zXVxuICAgICAgICogQHJldHVybiB7Tm9kZVtdfVxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLm5vZGVzID0gZnVuY3Rpb24gKHByZWQsIG9wdGlvbnMpIHtcbiAgICAgICAgICBwcmVkID0gcHJlZCB8fCBmdW5jLm9rO1xuICAgICAgICAgIHZhciBpbmNsdWRlQW5jZXN0b3IgPSBvcHRpb25zICYmIG9wdGlvbnMuaW5jbHVkZUFuY2VzdG9yO1xuICAgICAgICAgIHZhciBmdWxseUNvbnRhaW5zID0gb3B0aW9ucyAmJiBvcHRpb25zLmZ1bGx5Q29udGFpbnM7XG4gICAgICAgICAgLy8gVE9ETyBjb21wYXJlIHBvaW50cyBhbmQgc29ydFxuICAgICAgICAgIHZhciBzdGFydFBvaW50ID0gdGhpcy5nZXRTdGFydFBvaW50KCk7XG4gICAgICAgICAgdmFyIGVuZFBvaW50ID0gdGhpcy5nZXRFbmRQb2ludCgpO1xuICAgICAgICAgIHZhciBub2RlcyA9IFtdO1xuICAgICAgICAgIHZhciBsZWZ0RWRnZU5vZGVzID0gW107XG4gICAgICAgICAgZG9tLndhbGtQb2ludChzdGFydFBvaW50LCBlbmRQb2ludCwgZnVuY3Rpb24gKHBvaW50KSB7XG4gICAgICAgICAgICAgIGlmIChkb20uaXNFZGl0YWJsZShwb2ludC5ub2RlKSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHZhciBub2RlO1xuICAgICAgICAgICAgICBpZiAoZnVsbHlDb250YWlucykge1xuICAgICAgICAgICAgICAgICAgaWYgKGRvbS5pc0xlZnRFZGdlUG9pbnQocG9pbnQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgbGVmdEVkZ2VOb2Rlcy5wdXNoKHBvaW50Lm5vZGUpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgaWYgKGRvbS5pc1JpZ2h0RWRnZVBvaW50KHBvaW50KSAmJiBsaXN0cy5jb250YWlucyhsZWZ0RWRnZU5vZGVzLCBwb2ludC5ub2RlKSkge1xuICAgICAgICAgICAgICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYgKGluY2x1ZGVBbmNlc3Rvcikge1xuICAgICAgICAgICAgICAgICAgbm9kZSA9IGRvbS5hbmNlc3Rvcihwb2ludC5ub2RlLCBwcmVkKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgIG5vZGUgPSBwb2ludC5ub2RlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmIChub2RlICYmIHByZWQobm9kZSkpIHtcbiAgICAgICAgICAgICAgICAgIG5vZGVzLnB1c2gobm9kZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9LCB0cnVlKTtcbiAgICAgICAgICByZXR1cm4gbGlzdHMudW5pcXVlKG5vZGVzKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJldHVybnMgY29tbW9uQW5jZXN0b3Igb2YgcmFuZ2VcbiAgICAgICAqIEByZXR1cm4ge0VsZW1lbnR9IC0gY29tbW9uQW5jZXN0b3JcbiAgICAgICAqL1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5jb21tb25BbmNlc3RvciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gZG9tLmNvbW1vbkFuY2VzdG9yKHRoaXMuc2MsIHRoaXMuZWMpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmV0dXJucyBleHBhbmRlZCByYW5nZSBieSBwcmVkXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtGdW5jdGlvbn0gcHJlZCAtIHByZWRpY2F0ZSBmdW5jdGlvblxuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfVxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLmV4cGFuZCA9IGZ1bmN0aW9uIChwcmVkKSB7XG4gICAgICAgICAgdmFyIHN0YXJ0QW5jZXN0b3IgPSBkb20uYW5jZXN0b3IodGhpcy5zYywgcHJlZCk7XG4gICAgICAgICAgdmFyIGVuZEFuY2VzdG9yID0gZG9tLmFuY2VzdG9yKHRoaXMuZWMsIHByZWQpO1xuICAgICAgICAgIGlmICghc3RhcnRBbmNlc3RvciAmJiAhZW5kQW5jZXN0b3IpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2UodGhpcy5zYywgdGhpcy5zbywgdGhpcy5lYywgdGhpcy5lbyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBib3VuZGFyeVBvaW50cyA9IHRoaXMuZ2V0UG9pbnRzKCk7XG4gICAgICAgICAgaWYgKHN0YXJ0QW5jZXN0b3IpIHtcbiAgICAgICAgICAgICAgYm91bmRhcnlQb2ludHMuc2MgPSBzdGFydEFuY2VzdG9yO1xuICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5zbyA9IDA7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChlbmRBbmNlc3Rvcikge1xuICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5lYyA9IGVuZEFuY2VzdG9yO1xuICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5lbyA9IGRvbS5ub2RlTGVuZ3RoKGVuZEFuY2VzdG9yKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2UoYm91bmRhcnlQb2ludHMuc2MsIGJvdW5kYXJ5UG9pbnRzLnNvLCBib3VuZGFyeVBvaW50cy5lYywgYm91bmRhcnlQb2ludHMuZW8pO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQHBhcmFtIHtCb29sZWFufSBpc0NvbGxhcHNlVG9TdGFydFxuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfVxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLmNvbGxhcHNlID0gZnVuY3Rpb24gKGlzQ29sbGFwc2VUb1N0YXJ0KSB7XG4gICAgICAgICAgaWYgKGlzQ29sbGFwc2VUb1N0YXJ0KSB7XG4gICAgICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKHRoaXMuc2MsIHRoaXMuc28sIHRoaXMuc2MsIHRoaXMuc28pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2UodGhpcy5lYywgdGhpcy5lbywgdGhpcy5lYywgdGhpcy5lbyk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogc3BsaXRUZXh0IG9uIHJhbmdlXG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuc3BsaXRUZXh0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBpc1NhbWVDb250YWluZXIgPSB0aGlzLnNjID09PSB0aGlzLmVjO1xuICAgICAgICAgIHZhciBib3VuZGFyeVBvaW50cyA9IHRoaXMuZ2V0UG9pbnRzKCk7XG4gICAgICAgICAgaWYgKGRvbS5pc1RleHQodGhpcy5lYykgJiYgIWRvbS5pc0VkZ2VQb2ludCh0aGlzLmdldEVuZFBvaW50KCkpKSB7XG4gICAgICAgICAgICAgIHRoaXMuZWMuc3BsaXRUZXh0KHRoaXMuZW8pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZG9tLmlzVGV4dCh0aGlzLnNjKSAmJiAhZG9tLmlzRWRnZVBvaW50KHRoaXMuZ2V0U3RhcnRQb2ludCgpKSkge1xuICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5zYyA9IHRoaXMuc2Muc3BsaXRUZXh0KHRoaXMuc28pO1xuICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5zbyA9IDA7XG4gICAgICAgICAgICAgIGlmIChpc1NhbWVDb250YWluZXIpIHtcbiAgICAgICAgICAgICAgICAgIGJvdW5kYXJ5UG9pbnRzLmVjID0gYm91bmRhcnlQb2ludHMuc2M7XG4gICAgICAgICAgICAgICAgICBib3VuZGFyeVBvaW50cy5lbyA9IHRoaXMuZW8gLSB0aGlzLnNvO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKGJvdW5kYXJ5UG9pbnRzLnNjLCBib3VuZGFyeVBvaW50cy5zbywgYm91bmRhcnlQb2ludHMuZWMsIGJvdW5kYXJ5UG9pbnRzLmVvKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGRlbGV0ZSBjb250ZW50cyBvbiByYW5nZVxuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfVxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLmRlbGV0ZUNvbnRlbnRzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmICh0aGlzLmlzQ29sbGFwc2VkKCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBybmcgPSB0aGlzLnNwbGl0VGV4dCgpO1xuICAgICAgICAgIHZhciBub2RlcyA9IHJuZy5ub2RlcyhudWxsLCB7XG4gICAgICAgICAgICAgIGZ1bGx5Q29udGFpbnM6IHRydWVcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyBmaW5kIG5ldyBjdXJzb3IgcG9pbnRcbiAgICAgICAgICB2YXIgcG9pbnQgPSBkb20ucHJldlBvaW50VW50aWwocm5nLmdldFN0YXJ0UG9pbnQoKSwgZnVuY3Rpb24gKHBvaW50KSB7XG4gICAgICAgICAgICAgIHJldHVybiAhbGlzdHMuY29udGFpbnMobm9kZXMsIHBvaW50Lm5vZGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHZhciBlbXB0eVBhcmVudHMgPSBbXTtcbiAgICAgICAgICAkJDEuZWFjaChub2RlcywgZnVuY3Rpb24gKGlkeCwgbm9kZSkge1xuICAgICAgICAgICAgICAvLyBmaW5kIGVtcHR5IHBhcmVudHNcbiAgICAgICAgICAgICAgdmFyIHBhcmVudCA9IG5vZGUucGFyZW50Tm9kZTtcbiAgICAgICAgICAgICAgaWYgKHBvaW50Lm5vZGUgIT09IHBhcmVudCAmJiBkb20ubm9kZUxlbmd0aChwYXJlbnQpID09PSAxKSB7XG4gICAgICAgICAgICAgICAgICBlbXB0eVBhcmVudHMucHVzaChwYXJlbnQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGRvbS5yZW1vdmUobm9kZSwgZmFsc2UpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIHJlbW92ZSBlbXB0eSBwYXJlbnRzXG4gICAgICAgICAgJCQxLmVhY2goZW1wdHlQYXJlbnRzLCBmdW5jdGlvbiAoaWR4LCBub2RlKSB7XG4gICAgICAgICAgICAgIGRvbS5yZW1vdmUobm9kZSwgZmFsc2UpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKHBvaW50Lm5vZGUsIHBvaW50Lm9mZnNldCwgcG9pbnQubm9kZSwgcG9pbnQub2Zmc2V0KS5ub3JtYWxpemUoKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIG1ha2VJc09uOiByZXR1cm4gaXNPbihwcmVkKSBmdW5jdGlvblxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLm1ha2VJc09uID0gZnVuY3Rpb24gKHByZWQpIHtcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICB2YXIgYW5jZXN0b3IgPSBkb20uYW5jZXN0b3IodGhpcy5zYywgcHJlZCk7XG4gICAgICAgICAgICAgIHJldHVybiAhIWFuY2VzdG9yICYmIChhbmNlc3RvciA9PT0gZG9tLmFuY2VzdG9yKHRoaXMuZWMsIHByZWQpKTtcbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQHBhcmFtIHtGdW5jdGlvbn0gcHJlZFxuICAgICAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICAgICAqL1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5pc0xlZnRFZGdlT2YgPSBmdW5jdGlvbiAocHJlZCkge1xuICAgICAgICAgIGlmICghZG9tLmlzTGVmdEVkZ2VQb2ludCh0aGlzLmdldFN0YXJ0UG9pbnQoKSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgbm9kZSA9IGRvbS5hbmNlc3Rvcih0aGlzLnNjLCBwcmVkKTtcbiAgICAgICAgICByZXR1cm4gbm9kZSAmJiBkb20uaXNMZWZ0RWRnZU9mKHRoaXMuc2MsIG5vZGUpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmV0dXJucyB3aGV0aGVyIHJhbmdlIHdhcyBjb2xsYXBzZWQgb3Igbm90XG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuaXNDb2xsYXBzZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuc2MgPT09IHRoaXMuZWMgJiYgdGhpcy5zbyA9PT0gdGhpcy5lbztcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHdyYXAgaW5saW5lIG5vZGVzIHdoaWNoIGNoaWxkcmVuIG9mIGJvZHkgd2l0aCBwYXJhZ3JhcGhcbiAgICAgICAqXG4gICAgICAgKiBAcmV0dXJuIHtXcmFwcGVkUmFuZ2V9XG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUud3JhcEJvZHlJbmxpbmVXaXRoUGFyYSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAoZG9tLmlzQm9keUNvbnRhaW5lcih0aGlzLnNjKSAmJiBkb20uaXNFbXB0eSh0aGlzLnNjKSkge1xuICAgICAgICAgICAgICB0aGlzLnNjLmlubmVySFRNTCA9IGRvbS5lbXB0eVBhcmE7XG4gICAgICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKHRoaXMuc2MuZmlyc3RDaGlsZCwgMCwgdGhpcy5zYy5maXJzdENoaWxkLCAwKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLyoqXG4gICAgICAgICAgICogW3dvcmthcm91bmRdIGZpcmVmb3ggb2Z0ZW4gY3JlYXRlIHJhbmdlIG9uIG5vdCB2aXNpYmxlIHBvaW50LiBzbyBub3JtYWxpemUgaGVyZS5cbiAgICAgICAgICAgKiAgLSBmaXJlZm94OiB8PHA+dGV4dDwvcD58XG4gICAgICAgICAgICogIC0gY2hyb21lOiA8cD58dGV4dHw8L3A+XG4gICAgICAgICAgICovXG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMubm9ybWFsaXplKCk7XG4gICAgICAgICAgaWYgKGRvbS5pc1BhcmFJbmxpbmUodGhpcy5zYykgfHwgZG9tLmlzUGFyYSh0aGlzLnNjKSkge1xuICAgICAgICAgICAgICByZXR1cm4gcm5nO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBmaW5kIGlubGluZSB0b3AgYW5jZXN0b3JcbiAgICAgICAgICB2YXIgdG9wQW5jZXN0b3I7XG4gICAgICAgICAgaWYgKGRvbS5pc0lubGluZShybmcuc2MpKSB7XG4gICAgICAgICAgICAgIHZhciBhbmNlc3RvcnMgPSBkb20ubGlzdEFuY2VzdG9yKHJuZy5zYywgZnVuYy5ub3QoZG9tLmlzSW5saW5lKSk7XG4gICAgICAgICAgICAgIHRvcEFuY2VzdG9yID0gbGlzdHMubGFzdChhbmNlc3RvcnMpO1xuICAgICAgICAgICAgICBpZiAoIWRvbS5pc0lubGluZSh0b3BBbmNlc3RvcikpIHtcbiAgICAgICAgICAgICAgICAgIHRvcEFuY2VzdG9yID0gYW5jZXN0b3JzW2FuY2VzdG9ycy5sZW5ndGggLSAyXSB8fCBybmcuc2MuY2hpbGROb2Rlc1tybmcuc29dO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0b3BBbmNlc3RvciA9IHJuZy5zYy5jaGlsZE5vZGVzW3JuZy5zbyA+IDAgPyBybmcuc28gLSAxIDogMF07XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIHNpYmxpbmdzIG5vdCBpbiBwYXJhZ3JhcGhcbiAgICAgICAgICB2YXIgaW5saW5lU2libGluZ3MgPSBkb20ubGlzdFByZXYodG9wQW5jZXN0b3IsIGRvbS5pc1BhcmFJbmxpbmUpLnJldmVyc2UoKTtcbiAgICAgICAgICBpbmxpbmVTaWJsaW5ncyA9IGlubGluZVNpYmxpbmdzLmNvbmNhdChkb20ubGlzdE5leHQodG9wQW5jZXN0b3IubmV4dFNpYmxpbmcsIGRvbS5pc1BhcmFJbmxpbmUpKTtcbiAgICAgICAgICAvLyB3cmFwIHdpdGggcGFyYWdyYXBoXG4gICAgICAgICAgaWYgKGlubGluZVNpYmxpbmdzLmxlbmd0aCkge1xuICAgICAgICAgICAgICB2YXIgcGFyYSA9IGRvbS53cmFwKGxpc3RzLmhlYWQoaW5saW5lU2libGluZ3MpLCAncCcpO1xuICAgICAgICAgICAgICBkb20uYXBwZW5kQ2hpbGROb2RlcyhwYXJhLCBsaXN0cy50YWlsKGlubGluZVNpYmxpbmdzKSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiB0aGlzLm5vcm1hbGl6ZSgpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaW5zZXJ0IG5vZGUgYXQgY3VycmVudCBjdXJzb3JcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICAgICAqIEByZXR1cm4ge05vZGV9XG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuaW5zZXJ0Tm9kZSA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMud3JhcEJvZHlJbmxpbmVXaXRoUGFyYSgpLmRlbGV0ZUNvbnRlbnRzKCk7XG4gICAgICAgICAgdmFyIGluZm8gPSBkb20uc3BsaXRQb2ludChybmcuZ2V0U3RhcnRQb2ludCgpLCBkb20uaXNJbmxpbmUobm9kZSkpO1xuICAgICAgICAgIGlmIChpbmZvLnJpZ2h0Tm9kZSkge1xuICAgICAgICAgICAgICBpbmZvLnJpZ2h0Tm9kZS5wYXJlbnROb2RlLmluc2VydEJlZm9yZShub2RlLCBpbmZvLnJpZ2h0Tm9kZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBpbmZvLmNvbnRhaW5lci5hcHBlbmRDaGlsZChub2RlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIG5vZGU7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBpbnNlcnQgaHRtbCBhdCBjdXJyZW50IGN1cnNvclxuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLnBhc3RlSFRNTCA9IGZ1bmN0aW9uIChtYXJrdXApIHtcbiAgICAgICAgICB2YXIgY29udGVudHNDb250YWluZXIgPSAkJDEoJzxkaXY+PC9kaXY+JykuaHRtbChtYXJrdXApWzBdO1xuICAgICAgICAgIHZhciBjaGlsZE5vZGVzID0gbGlzdHMuZnJvbShjb250ZW50c0NvbnRhaW5lci5jaGlsZE5vZGVzKTtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy53cmFwQm9keUlubGluZVdpdGhQYXJhKCkuZGVsZXRlQ29udGVudHMoKTtcbiAgICAgICAgICBpZiAocm5nLnNvID4gMCkge1xuICAgICAgICAgICAgICBjaGlsZE5vZGVzID0gY2hpbGROb2Rlcy5yZXZlcnNlKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGNoaWxkTm9kZXMgPSBjaGlsZE5vZGVzLm1hcChmdW5jdGlvbiAoY2hpbGROb2RlKSB7XG4gICAgICAgICAgICAgIHJldHVybiBybmcuaW5zZXJ0Tm9kZShjaGlsZE5vZGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIGlmIChybmcuc28gPiAwKSB7XG4gICAgICAgICAgICAgIGNoaWxkTm9kZXMgPSBjaGlsZE5vZGVzLnJldmVyc2UoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGNoaWxkTm9kZXM7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiByZXR1cm5zIHRleHQgaW4gcmFuZ2VcbiAgICAgICAqXG4gICAgICAgKiBAcmV0dXJuIHtTdHJpbmd9XG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIG5hdGl2ZVJuZyA9IHRoaXMubmF0aXZlUmFuZ2UoKTtcbiAgICAgICAgICByZXR1cm4gZW52LmlzVzNDUmFuZ2VTdXBwb3J0ID8gbmF0aXZlUm5nLnRvU3RyaW5nKCkgOiBuYXRpdmVSbmcudGV4dDtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJldHVybnMgcmFuZ2UgZm9yIHdvcmQgYmVmb3JlIGN1cnNvclxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gW2ZpbmRBZnRlcl0gLSBmaW5kIGFmdGVyIGN1cnNvciwgZGVmYXVsdDogZmFsc2VcbiAgICAgICAqIEByZXR1cm4ge1dyYXBwZWRSYW5nZX1cbiAgICAgICAqL1xuICAgICAgV3JhcHBlZFJhbmdlLnByb3RvdHlwZS5nZXRXb3JkUmFuZ2UgPSBmdW5jdGlvbiAoZmluZEFmdGVyKSB7XG4gICAgICAgICAgdmFyIGVuZFBvaW50ID0gdGhpcy5nZXRFbmRQb2ludCgpO1xuICAgICAgICAgIGlmICghZG9tLmlzQ2hhclBvaW50KGVuZFBvaW50KSkge1xuICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIHN0YXJ0UG9pbnQgPSBkb20ucHJldlBvaW50VW50aWwoZW5kUG9pbnQsIGZ1bmN0aW9uIChwb2ludCkge1xuICAgICAgICAgICAgICByZXR1cm4gIWRvbS5pc0NoYXJQb2ludChwb2ludCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYgKGZpbmRBZnRlcikge1xuICAgICAgICAgICAgICBlbmRQb2ludCA9IGRvbS5uZXh0UG9pbnRVbnRpbChlbmRQb2ludCwgZnVuY3Rpb24gKHBvaW50KSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gIWRvbS5pc0NoYXJQb2ludChwb2ludCk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gbmV3IFdyYXBwZWRSYW5nZShzdGFydFBvaW50Lm5vZGUsIHN0YXJ0UG9pbnQub2Zmc2V0LCBlbmRQb2ludC5ub2RlLCBlbmRQb2ludC5vZmZzZXQpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogY3JlYXRlIG9mZnNldFBhdGggYm9va21hcmtcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge05vZGV9IGVkaXRhYmxlXG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuYm9va21hcmsgPSBmdW5jdGlvbiAoZWRpdGFibGUpIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICBzOiB7XG4gICAgICAgICAgICAgICAgICBwYXRoOiBkb20ubWFrZU9mZnNldFBhdGgoZWRpdGFibGUsIHRoaXMuc2MpLFxuICAgICAgICAgICAgICAgICAgb2Zmc2V0OiB0aGlzLnNvXG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIGU6IHtcbiAgICAgICAgICAgICAgICAgIHBhdGg6IGRvbS5tYWtlT2Zmc2V0UGF0aChlZGl0YWJsZSwgdGhpcy5lYyksXG4gICAgICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuZW9cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBjcmVhdGUgb2Zmc2V0UGF0aCBib29rbWFyayBiYXNlIG9uIHBhcmFncmFwaFxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7Tm9kZVtdfSBwYXJhc1xuICAgICAgICovXG4gICAgICBXcmFwcGVkUmFuZ2UucHJvdG90eXBlLnBhcmFCb29rbWFyayA9IGZ1bmN0aW9uIChwYXJhcykge1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIHM6IHtcbiAgICAgICAgICAgICAgICAgIHBhdGg6IGxpc3RzLnRhaWwoZG9tLm1ha2VPZmZzZXRQYXRoKGxpc3RzLmhlYWQocGFyYXMpLCB0aGlzLnNjKSksXG4gICAgICAgICAgICAgICAgICBvZmZzZXQ6IHRoaXMuc29cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgZToge1xuICAgICAgICAgICAgICAgICAgcGF0aDogbGlzdHMudGFpbChkb20ubWFrZU9mZnNldFBhdGgobGlzdHMubGFzdChwYXJhcyksIHRoaXMuZWMpKSxcbiAgICAgICAgICAgICAgICAgIG9mZnNldDogdGhpcy5lb1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGdldENsaWVudFJlY3RzXG4gICAgICAgKiBAcmV0dXJuIHtSZWN0W119XG4gICAgICAgKi9cbiAgICAgIFdyYXBwZWRSYW5nZS5wcm90b3R5cGUuZ2V0Q2xpZW50UmVjdHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIG5hdGl2ZVJuZyA9IHRoaXMubmF0aXZlUmFuZ2UoKTtcbiAgICAgICAgICByZXR1cm4gbmF0aXZlUm5nLmdldENsaWVudFJlY3RzKCk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFdyYXBwZWRSYW5nZTtcbiAgfSgpKTtcbiAgLyoqXG4gICAqIERhdGEgc3RydWN0dXJlXG4gICAqICAqIEJvdW5kYXJ5UG9pbnQ6IGEgcG9pbnQgb2YgZG9tIHRyZWVcbiAgICogICogQm91bmRhcnlQb2ludHM6IHR3byBib3VuZGFyeVBvaW50cyBjb3JyZXNwb25kaW5nIHRvIHRoZSBzdGFydCBhbmQgdGhlIGVuZCBvZiB0aGUgUmFuZ2VcbiAgICpcbiAgICogU2VlIHRvIGh0dHA6Ly93d3cudzMub3JnL1RSL0RPTS1MZXZlbC0yLVRyYXZlcnNhbC1SYW5nZS9yYW5nZXMuaHRtbCNMZXZlbC0yLVJhbmdlLVBvc2l0aW9uXG4gICAqL1xuICB2YXIgcmFuZ2UgPSB7XG4gICAgICAvKipcbiAgICAgICAqIGNyZWF0ZSBSYW5nZSBPYmplY3QgRnJvbSBhcmd1bWVudHMgb3IgQnJvd3NlciBTZWxlY3Rpb25cbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge05vZGV9IHNjIC0gc3RhcnQgY29udGFpbmVyXG4gICAgICAgKiBAcGFyYW0ge051bWJlcn0gc28gLSBzdGFydCBvZmZzZXRcbiAgICAgICAqIEBwYXJhbSB7Tm9kZX0gZWMgLSBlbmQgY29udGFpbmVyXG4gICAgICAgKiBAcGFyYW0ge051bWJlcn0gZW8gLSBlbmQgb2Zmc2V0XG4gICAgICAgKiBAcmV0dXJuIHtXcmFwcGVkUmFuZ2V9XG4gICAgICAgKi9cbiAgICAgIGNyZWF0ZTogZnVuY3Rpb24gKHNjLCBzbywgZWMsIGVvKSB7XG4gICAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2Uoc2MsIHNvLCBlYywgZW8pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAyKSB7IC8vIGNvbGxhcHNlZFxuICAgICAgICAgICAgICBlYyA9IHNjO1xuICAgICAgICAgICAgICBlbyA9IHNvO1xuICAgICAgICAgICAgICByZXR1cm4gbmV3IFdyYXBwZWRSYW5nZShzYywgc28sIGVjLCBlbyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB2YXIgd3JhcHBlZFJhbmdlID0gdGhpcy5jcmVhdGVGcm9tU2VsZWN0aW9uKCk7XG4gICAgICAgICAgICAgIGlmICghd3JhcHBlZFJhbmdlICYmIGFyZ3VtZW50cy5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgIHdyYXBwZWRSYW5nZSA9IHRoaXMuY3JlYXRlRnJvbU5vZGUoYXJndW1lbnRzWzBdKTtcbiAgICAgICAgICAgICAgICAgIHJldHVybiB3cmFwcGVkUmFuZ2UuY29sbGFwc2UoZG9tLmVtcHR5UGFyYSA9PT0gYXJndW1lbnRzWzBdLmlubmVySFRNTCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgcmV0dXJuIHdyYXBwZWRSYW5nZTtcbiAgICAgICAgICB9XG4gICAgICB9LFxuICAgICAgY3JlYXRlRnJvbVNlbGVjdGlvbjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBzYywgc28sIGVjLCBlbztcbiAgICAgICAgICBpZiAoZW52LmlzVzNDUmFuZ2VTdXBwb3J0KSB7XG4gICAgICAgICAgICAgIHZhciBzZWxlY3Rpb24gPSBkb2N1bWVudC5nZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgICAgaWYgKCFzZWxlY3Rpb24gfHwgc2VsZWN0aW9uLnJhbmdlQ291bnQgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2UgaWYgKGRvbS5pc0JvZHkoc2VsZWN0aW9uLmFuY2hvck5vZGUpKSB7XG4gICAgICAgICAgICAgICAgICAvLyBGaXJlZm94OiByZXR1cm5zIGVudGlyZSBib2R5IGFzIHJhbmdlIG9uIGluaXRpYWxpemF0aW9uLlxuICAgICAgICAgICAgICAgICAgLy8gV2Ugd29uJ3QgbmV2ZXIgbmVlZCBpdC5cbiAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHZhciBuYXRpdmVSbmcgPSBzZWxlY3Rpb24uZ2V0UmFuZ2VBdCgwKTtcbiAgICAgICAgICAgICAgc2MgPSBuYXRpdmVSbmcuc3RhcnRDb250YWluZXI7XG4gICAgICAgICAgICAgIHNvID0gbmF0aXZlUm5nLnN0YXJ0T2Zmc2V0O1xuICAgICAgICAgICAgICBlYyA9IG5hdGl2ZVJuZy5lbmRDb250YWluZXI7XG4gICAgICAgICAgICAgIGVvID0gbmF0aXZlUm5nLmVuZE9mZnNldDtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7IC8vIElFODogVGV4dFJhbmdlXG4gICAgICAgICAgICAgIHZhciB0ZXh0UmFuZ2UgPSBkb2N1bWVudC5zZWxlY3Rpb24uY3JlYXRlUmFuZ2UoKTtcbiAgICAgICAgICAgICAgdmFyIHRleHRSYW5nZUVuZCA9IHRleHRSYW5nZS5kdXBsaWNhdGUoKTtcbiAgICAgICAgICAgICAgdGV4dFJhbmdlRW5kLmNvbGxhcHNlKGZhbHNlKTtcbiAgICAgICAgICAgICAgdmFyIHRleHRSYW5nZVN0YXJ0ID0gdGV4dFJhbmdlO1xuICAgICAgICAgICAgICB0ZXh0UmFuZ2VTdGFydC5jb2xsYXBzZSh0cnVlKTtcbiAgICAgICAgICAgICAgdmFyIHN0YXJ0UG9pbnQgPSB0ZXh0UmFuZ2VUb1BvaW50KHRleHRSYW5nZVN0YXJ0LCB0cnVlKTtcbiAgICAgICAgICAgICAgdmFyIGVuZFBvaW50ID0gdGV4dFJhbmdlVG9Qb2ludCh0ZXh0UmFuZ2VFbmQsIGZhbHNlKTtcbiAgICAgICAgICAgICAgLy8gc2FtZSB2aXNpYmxlIHBvaW50IGNhc2U6IHJhbmdlIHdhcyBjb2xsYXBzZWQuXG4gICAgICAgICAgICAgIGlmIChkb20uaXNUZXh0KHN0YXJ0UG9pbnQubm9kZSkgJiYgZG9tLmlzTGVmdEVkZ2VQb2ludChzdGFydFBvaW50KSAmJlxuICAgICAgICAgICAgICAgICAgZG9tLmlzVGV4dE5vZGUoZW5kUG9pbnQubm9kZSkgJiYgZG9tLmlzUmlnaHRFZGdlUG9pbnQoZW5kUG9pbnQpICYmXG4gICAgICAgICAgICAgICAgICBlbmRQb2ludC5ub2RlLm5leHRTaWJsaW5nID09PSBzdGFydFBvaW50Lm5vZGUpIHtcbiAgICAgICAgICAgICAgICAgIHN0YXJ0UG9pbnQgPSBlbmRQb2ludDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzYyA9IHN0YXJ0UG9pbnQuY29udDtcbiAgICAgICAgICAgICAgc28gPSBzdGFydFBvaW50Lm9mZnNldDtcbiAgICAgICAgICAgICAgZWMgPSBlbmRQb2ludC5jb250O1xuICAgICAgICAgICAgICBlbyA9IGVuZFBvaW50Lm9mZnNldDtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2Uoc2MsIHNvLCBlYywgZW8pO1xuICAgICAgfSxcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZFxuICAgICAgICpcbiAgICAgICAqIGNyZWF0ZSBXcmFwcGVkUmFuZ2UgZnJvbSBub2RlXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAgICAgKiBAcmV0dXJuIHtXcmFwcGVkUmFuZ2V9XG4gICAgICAgKi9cbiAgICAgIGNyZWF0ZUZyb21Ob2RlOiBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgIHZhciBzYyA9IG5vZGU7XG4gICAgICAgICAgdmFyIHNvID0gMDtcbiAgICAgICAgICB2YXIgZWMgPSBub2RlO1xuICAgICAgICAgIHZhciBlbyA9IGRvbS5ub2RlTGVuZ3RoKGVjKTtcbiAgICAgICAgICAvLyBicm93c2VycyBjYW4ndCB0YXJnZXQgYSBwaWN0dXJlIG9yIHZvaWQgbm9kZVxuICAgICAgICAgIGlmIChkb20uaXNWb2lkKHNjKSkge1xuICAgICAgICAgICAgICBzbyA9IGRvbS5saXN0UHJldihzYykubGVuZ3RoIC0gMTtcbiAgICAgICAgICAgICAgc2MgPSBzYy5wYXJlbnROb2RlO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZG9tLmlzQlIoZWMpKSB7XG4gICAgICAgICAgICAgIGVvID0gZG9tLmxpc3RQcmV2KGVjKS5sZW5ndGggLSAxO1xuICAgICAgICAgICAgICBlYyA9IGVjLnBhcmVudE5vZGU7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYgKGRvbS5pc1ZvaWQoZWMpKSB7XG4gICAgICAgICAgICAgIGVvID0gZG9tLmxpc3RQcmV2KGVjKS5sZW5ndGg7XG4gICAgICAgICAgICAgIGVjID0gZWMucGFyZW50Tm9kZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlKHNjLCBzbywgZWMsIGVvKTtcbiAgICAgIH0sXG4gICAgICAvKipcbiAgICAgICAqIGNyZWF0ZSBXcmFwcGVkUmFuZ2UgZnJvbSBub2RlIGFmdGVyIHBvc2l0aW9uXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtOb2RlfSBub2RlXG4gICAgICAgKiBAcmV0dXJuIHtXcmFwcGVkUmFuZ2V9XG4gICAgICAgKi9cbiAgICAgIGNyZWF0ZUZyb21Ob2RlQmVmb3JlOiBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgIHJldHVybiB0aGlzLmNyZWF0ZUZyb21Ob2RlKG5vZGUpLmNvbGxhcHNlKHRydWUpO1xuICAgICAgfSxcbiAgICAgIC8qKlxuICAgICAgICogY3JlYXRlIFdyYXBwZWRSYW5nZSBmcm9tIG5vZGUgYWZ0ZXIgcG9zaXRpb25cbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICAgICAqIEByZXR1cm4ge1dyYXBwZWRSYW5nZX1cbiAgICAgICAqL1xuICAgICAgY3JlYXRlRnJvbU5vZGVBZnRlcjogZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVGcm9tTm9kZShub2RlKS5jb2xsYXBzZSgpO1xuICAgICAgfSxcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZFxuICAgICAgICpcbiAgICAgICAqIGNyZWF0ZSBXcmFwcGVkUmFuZ2UgZnJvbSBib29rbWFya1xuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7Tm9kZX0gZWRpdGFibGVcbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBib29rbWFya1xuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfVxuICAgICAgICovXG4gICAgICBjcmVhdGVGcm9tQm9va21hcms6IGZ1bmN0aW9uIChlZGl0YWJsZSwgYm9va21hcmspIHtcbiAgICAgICAgICB2YXIgc2MgPSBkb20uZnJvbU9mZnNldFBhdGgoZWRpdGFibGUsIGJvb2ttYXJrLnMucGF0aCk7XG4gICAgICAgICAgdmFyIHNvID0gYm9va21hcmsucy5vZmZzZXQ7XG4gICAgICAgICAgdmFyIGVjID0gZG9tLmZyb21PZmZzZXRQYXRoKGVkaXRhYmxlLCBib29rbWFyay5lLnBhdGgpO1xuICAgICAgICAgIHZhciBlbyA9IGJvb2ttYXJrLmUub2Zmc2V0O1xuICAgICAgICAgIHJldHVybiBuZXcgV3JhcHBlZFJhbmdlKHNjLCBzbywgZWMsIGVvKTtcbiAgICAgIH0sXG4gICAgICAvKipcbiAgICAgICAqIEBtZXRob2RcbiAgICAgICAqXG4gICAgICAgKiBjcmVhdGUgV3JhcHBlZFJhbmdlIGZyb20gcGFyYUJvb2ttYXJrXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtPYmplY3R9IGJvb2ttYXJrXG4gICAgICAgKiBAcGFyYW0ge05vZGVbXX0gcGFyYXNcbiAgICAgICAqIEByZXR1cm4ge1dyYXBwZWRSYW5nZX1cbiAgICAgICAqL1xuICAgICAgY3JlYXRlRnJvbVBhcmFCb29rbWFyazogZnVuY3Rpb24gKGJvb2ttYXJrLCBwYXJhcykge1xuICAgICAgICAgIHZhciBzbyA9IGJvb2ttYXJrLnMub2Zmc2V0O1xuICAgICAgICAgIHZhciBlbyA9IGJvb2ttYXJrLmUub2Zmc2V0O1xuICAgICAgICAgIHZhciBzYyA9IGRvbS5mcm9tT2Zmc2V0UGF0aChsaXN0cy5oZWFkKHBhcmFzKSwgYm9va21hcmsucy5wYXRoKTtcbiAgICAgICAgICB2YXIgZWMgPSBkb20uZnJvbU9mZnNldFBhdGgobGlzdHMubGFzdChwYXJhcyksIGJvb2ttYXJrLmUucGF0aCk7XG4gICAgICAgICAgcmV0dXJuIG5ldyBXcmFwcGVkUmFuZ2Uoc2MsIHNvLCBlYywgZW8pO1xuICAgICAgfVxuICB9O1xuXG4gIHZhciBLRVlfTUFQID0ge1xuICAgICAgJ0JBQ0tTUEFDRSc6IDgsXG4gICAgICAnVEFCJzogOSxcbiAgICAgICdFTlRFUic6IDEzLFxuICAgICAgJ1NQQUNFJzogMzIsXG4gICAgICAnREVMRVRFJzogNDYsXG4gICAgICAvLyBBcnJvd1xuICAgICAgJ0xFRlQnOiAzNyxcbiAgICAgICdVUCc6IDM4LFxuICAgICAgJ1JJR0hUJzogMzksXG4gICAgICAnRE9XTic6IDQwLFxuICAgICAgLy8gTnVtYmVyOiAwLTlcbiAgICAgICdOVU0wJzogNDgsXG4gICAgICAnTlVNMSc6IDQ5LFxuICAgICAgJ05VTTInOiA1MCxcbiAgICAgICdOVU0zJzogNTEsXG4gICAgICAnTlVNNCc6IDUyLFxuICAgICAgJ05VTTUnOiA1MyxcbiAgICAgICdOVU02JzogNTQsXG4gICAgICAnTlVNNyc6IDU1LFxuICAgICAgJ05VTTgnOiA1NixcbiAgICAgIC8vIEFscGhhYmV0OiBhLXpcbiAgICAgICdCJzogNjYsXG4gICAgICAnRSc6IDY5LFxuICAgICAgJ0knOiA3MyxcbiAgICAgICdKJzogNzQsXG4gICAgICAnSyc6IDc1LFxuICAgICAgJ0wnOiA3NixcbiAgICAgICdSJzogODIsXG4gICAgICAnUyc6IDgzLFxuICAgICAgJ1UnOiA4NSxcbiAgICAgICdWJzogODYsXG4gICAgICAnWSc6IDg5LFxuICAgICAgJ1onOiA5MCxcbiAgICAgICdTTEFTSCc6IDE5MSxcbiAgICAgICdMRUZUQlJBQ0tFVCc6IDIxOSxcbiAgICAgICdCQUNLU0xBU0gnOiAyMjAsXG4gICAgICAnUklHSFRCUkFDS0VUJzogMjIxXG4gIH07XG4gIC8qKlxuICAgKiBAY2xhc3MgY29yZS5rZXlcbiAgICpcbiAgICogT2JqZWN0IGZvciBrZXljb2Rlcy5cbiAgICpcbiAgICogQHNpbmdsZXRvblxuICAgKiBAYWx0ZXJuYXRlQ2xhc3NOYW1lIGtleVxuICAgKi9cbiAgdmFyIGtleSA9IHtcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZCBpc0VkaXRcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge051bWJlcn0ga2V5Q29kZVxuICAgICAgICogQHJldHVybiB7Qm9vbGVhbn1cbiAgICAgICAqL1xuICAgICAgaXNFZGl0OiBmdW5jdGlvbiAoa2V5Q29kZSkge1xuICAgICAgICAgIHJldHVybiBsaXN0cy5jb250YWlucyhbXG4gICAgICAgICAgICAgIEtFWV9NQVAuQkFDS1NQQUNFLFxuICAgICAgICAgICAgICBLRVlfTUFQLlRBQixcbiAgICAgICAgICAgICAgS0VZX01BUC5FTlRFUixcbiAgICAgICAgICAgICAgS0VZX01BUC5TUEFDRSxcbiAgICAgICAgICAgICAgS0VZX01BUC5ERUxFVEUsXG4gICAgICAgICAgXSwga2V5Q29kZSk7XG4gICAgICB9LFxuICAgICAgLyoqXG4gICAgICAgKiBAbWV0aG9kIGlzTW92ZVxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7TnVtYmVyfSBrZXlDb2RlXG4gICAgICAgKiBAcmV0dXJuIHtCb29sZWFufVxuICAgICAgICovXG4gICAgICBpc01vdmU6IGZ1bmN0aW9uIChrZXlDb2RlKSB7XG4gICAgICAgICAgcmV0dXJuIGxpc3RzLmNvbnRhaW5zKFtcbiAgICAgICAgICAgICAgS0VZX01BUC5MRUZULFxuICAgICAgICAgICAgICBLRVlfTUFQLlVQLFxuICAgICAgICAgICAgICBLRVlfTUFQLlJJR0hULFxuICAgICAgICAgICAgICBLRVlfTUFQLkRPV04sXG4gICAgICAgICAgXSwga2V5Q29kZSk7XG4gICAgICB9LFxuICAgICAgLyoqXG4gICAgICAgKiBAcHJvcGVydHkge09iamVjdH0gbmFtZUZyb21Db2RlXG4gICAgICAgKiBAcHJvcGVydHkge1N0cmluZ30gbmFtZUZyb21Db2RlLjggXCJCQUNLU1BBQ0VcIlxuICAgICAgICovXG4gICAgICBuYW1lRnJvbUNvZGU6IGZ1bmMuaW52ZXJ0T2JqZWN0KEtFWV9NQVApLFxuICAgICAgY29kZTogS0VZX01BUFxuICB9O1xuXG4gIC8qKlxuICAgKiBAbWV0aG9kIHJlYWRGaWxlQXNEYXRhVVJMXG4gICAqXG4gICAqIHJlYWQgY29udGVudHMgb2YgZmlsZSBhcyByZXByZXNlbnRpbmcgVVJMXG4gICAqXG4gICAqIEBwYXJhbSB7RmlsZX0gZmlsZVxuICAgKiBAcmV0dXJuIHtQcm9taXNlfSAtIHRoZW46IGRhdGFVcmxcbiAgICovXG4gIGZ1bmN0aW9uIHJlYWRGaWxlQXNEYXRhVVJMKGZpbGUpIHtcbiAgICAgIHJldHVybiAkJDEuRGVmZXJyZWQoZnVuY3Rpb24gKGRlZmVycmVkKSB7XG4gICAgICAgICAgJCQxLmV4dGVuZChuZXcgRmlsZVJlYWRlcigpLCB7XG4gICAgICAgICAgICAgIG9ubG9hZDogZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBkYXRhVVJMID0gZS50YXJnZXQucmVzdWx0O1xuICAgICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZShkYXRhVVJMKTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgb25lcnJvcjogZnVuY3Rpb24gKGVycikge1xuICAgICAgICAgICAgICAgICAgZGVmZXJyZWQucmVqZWN0KGVycik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KS5yZWFkQXNEYXRhVVJMKGZpbGUpO1xuICAgICAgfSkucHJvbWlzZSgpO1xuICB9XG4gIC8qKlxuICAgKiBAbWV0aG9kIGNyZWF0ZUltYWdlXG4gICAqXG4gICAqIGNyZWF0ZSBgPGltYWdlPmAgZnJvbSB1cmwgc3RyaW5nXG4gICAqXG4gICAqIEBwYXJhbSB7U3RyaW5nfSB1cmxcbiAgICogQHJldHVybiB7UHJvbWlzZX0gLSB0aGVuOiAkaW1hZ2VcbiAgICovXG4gIGZ1bmN0aW9uIGNyZWF0ZUltYWdlKHVybCkge1xuICAgICAgcmV0dXJuICQkMS5EZWZlcnJlZChmdW5jdGlvbiAoZGVmZXJyZWQpIHtcbiAgICAgICAgICB2YXIgJGltZyA9ICQkMSgnPGltZz4nKTtcbiAgICAgICAgICAkaW1nLm9uZSgnbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgJGltZy5vZmYoJ2Vycm9yIGFib3J0Jyk7XG4gICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoJGltZyk7XG4gICAgICAgICAgfSkub25lKCdlcnJvciBhYm9ydCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgJGltZy5vZmYoJ2xvYWQnKS5kZXRhY2goKTtcbiAgICAgICAgICAgICAgZGVmZXJyZWQucmVqZWN0KCRpbWcpO1xuICAgICAgICAgIH0pLmNzcyh7XG4gICAgICAgICAgICAgIGRpc3BsYXk6ICdub25lJ1xuICAgICAgICAgIH0pLmFwcGVuZFRvKGRvY3VtZW50LmJvZHkpLmF0dHIoJ3NyYycsIHVybCk7XG4gICAgICB9KS5wcm9taXNlKCk7XG4gIH1cblxuICB2YXIgSGlzdG9yeSA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEhpc3RvcnkoJGVkaXRhYmxlKSB7XG4gICAgICAgICAgdGhpcy5zdGFjayA9IFtdO1xuICAgICAgICAgIHRoaXMuc3RhY2tPZmZzZXQgPSAtMTtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZSA9ICRlZGl0YWJsZTtcbiAgICAgICAgICB0aGlzLmVkaXRhYmxlID0gJGVkaXRhYmxlWzBdO1xuICAgICAgfVxuICAgICAgSGlzdG9yeS5wcm90b3R5cGUubWFrZVNuYXBzaG90ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBybmcgPSByYW5nZS5jcmVhdGUodGhpcy5lZGl0YWJsZSk7XG4gICAgICAgICAgdmFyIGVtcHR5Qm9va21hcmsgPSB7IHM6IHsgcGF0aDogW10sIG9mZnNldDogMCB9LCBlOiB7IHBhdGg6IFtdLCBvZmZzZXQ6IDAgfSB9O1xuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLiRlZGl0YWJsZS5odG1sKCksXG4gICAgICAgICAgICAgIGJvb2ttYXJrOiAoKHJuZyAmJiBybmcuaXNPbkVkaXRhYmxlKCkpID8gcm5nLmJvb2ttYXJrKHRoaXMuZWRpdGFibGUpIDogZW1wdHlCb29rbWFyaylcbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIEhpc3RvcnkucHJvdG90eXBlLmFwcGx5U25hcHNob3QgPSBmdW5jdGlvbiAoc25hcHNob3QpIHtcbiAgICAgICAgICBpZiAoc25hcHNob3QuY29udGVudHMgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgdGhpcy4kZWRpdGFibGUuaHRtbChzbmFwc2hvdC5jb250ZW50cyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChzbmFwc2hvdC5ib29rbWFyayAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICByYW5nZS5jcmVhdGVGcm9tQm9va21hcmsodGhpcy5lZGl0YWJsZSwgc25hcHNob3QuYm9va21hcmspLnNlbGVjdCgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICogQG1ldGhvZCByZXdpbmRcbiAgICAgICogUmV3aW5kcyB0aGUgaGlzdG9yeSBzdGFjayBiYWNrIHRvIHRoZSBmaXJzdCBzbmFwc2hvdCB0YWtlbi5cbiAgICAgICogTGVhdmVzIHRoZSBzdGFjayBpbnRhY3QsIHNvIHRoYXQgXCJSZWRvXCIgY2FuIHN0aWxsIGJlIHVzZWQuXG4gICAgICAqL1xuICAgICAgSGlzdG9yeS5wcm90b3R5cGUucmV3aW5kID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIC8vIENyZWF0ZSBzbmFwIHNob3QgaWYgbm90IHlldCByZWNvcmRlZFxuICAgICAgICAgIGlmICh0aGlzLiRlZGl0YWJsZS5odG1sKCkgIT09IHRoaXMuc3RhY2tbdGhpcy5zdGFja09mZnNldF0uY29udGVudHMpIHtcbiAgICAgICAgICAgICAgdGhpcy5yZWNvcmRVbmRvKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIFJldHVybiB0byB0aGUgZmlyc3QgYXZhaWxhYmxlIHNuYXBzaG90LlxuICAgICAgICAgIHRoaXMuc3RhY2tPZmZzZXQgPSAwO1xuICAgICAgICAgIC8vIEFwcGx5IHRoYXQgc25hcHNob3QuXG4gICAgICAgICAgdGhpcy5hcHBseVNuYXBzaG90KHRoaXMuc3RhY2tbdGhpcy5zdGFja09mZnNldF0pO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgKiAgQG1ldGhvZCBjb21taXRcbiAgICAgICogIFJlc2V0cyBoaXN0b3J5IHN0YWNrLCBidXQga2VlcHMgY3VycmVudCBlZGl0b3IncyBjb250ZW50LlxuICAgICAgKi9cbiAgICAgIEhpc3RvcnkucHJvdG90eXBlLmNvbW1pdCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyBDbGVhciB0aGUgc3RhY2suXG4gICAgICAgICAgdGhpcy5zdGFjayA9IFtdO1xuICAgICAgICAgIC8vIFJlc3RvcmUgc3RhY2tPZmZzZXQgdG8gaXRzIG9yaWdpbmFsIHZhbHVlLlxuICAgICAgICAgIHRoaXMuc3RhY2tPZmZzZXQgPSAtMTtcbiAgICAgICAgICAvLyBSZWNvcmQgb3VyIGZpcnN0IHNuYXBzaG90IChvZiBub3RoaW5nKS5cbiAgICAgICAgICB0aGlzLnJlY29yZFVuZG8oKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICogQG1ldGhvZCByZXNldFxuICAgICAgKiBSZXNldHMgdGhlIGhpc3Rvcnkgc3RhY2sgY29tcGxldGVseTsgcmV2ZXJ0aW5nIHRvIGFuIGVtcHR5IGVkaXRvci5cbiAgICAgICovXG4gICAgICBIaXN0b3J5LnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyBDbGVhciB0aGUgc3RhY2suXG4gICAgICAgICAgdGhpcy5zdGFjayA9IFtdO1xuICAgICAgICAgIC8vIFJlc3RvcmUgc3RhY2tPZmZzZXQgdG8gaXRzIG9yaWdpbmFsIHZhbHVlLlxuICAgICAgICAgIHRoaXMuc3RhY2tPZmZzZXQgPSAtMTtcbiAgICAgICAgICAvLyBDbGVhciB0aGUgZWRpdGFibGUgYXJlYS5cbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5odG1sKCcnKTtcbiAgICAgICAgICAvLyBSZWNvcmQgb3VyIGZpcnN0IHNuYXBzaG90IChvZiBub3RoaW5nKS5cbiAgICAgICAgICB0aGlzLnJlY29yZFVuZG8oKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHVuZG9cbiAgICAgICAqL1xuICAgICAgSGlzdG9yeS5wcm90b3R5cGUudW5kbyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyBDcmVhdGUgc25hcCBzaG90IGlmIG5vdCB5ZXQgcmVjb3JkZWRcbiAgICAgICAgICBpZiAodGhpcy4kZWRpdGFibGUuaHRtbCgpICE9PSB0aGlzLnN0YWNrW3RoaXMuc3RhY2tPZmZzZXRdLmNvbnRlbnRzKSB7XG4gICAgICAgICAgICAgIHRoaXMucmVjb3JkVW5kbygpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodGhpcy5zdGFja09mZnNldCA+IDApIHtcbiAgICAgICAgICAgICAgdGhpcy5zdGFja09mZnNldC0tO1xuICAgICAgICAgICAgICB0aGlzLmFwcGx5U25hcHNob3QodGhpcy5zdGFja1t0aGlzLnN0YWNrT2Zmc2V0XSk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmVkb1xuICAgICAgICovXG4gICAgICBIaXN0b3J5LnByb3RvdHlwZS5yZWRvID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmICh0aGlzLnN0YWNrLmxlbmd0aCAtIDEgPiB0aGlzLnN0YWNrT2Zmc2V0KSB7XG4gICAgICAgICAgICAgIHRoaXMuc3RhY2tPZmZzZXQrKztcbiAgICAgICAgICAgICAgdGhpcy5hcHBseVNuYXBzaG90KHRoaXMuc3RhY2tbdGhpcy5zdGFja09mZnNldF0pO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJlY29yZGVkIHVuZG9cbiAgICAgICAqL1xuICAgICAgSGlzdG9yeS5wcm90b3R5cGUucmVjb3JkVW5kbyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLnN0YWNrT2Zmc2V0Kys7XG4gICAgICAgICAgLy8gV2FzaCBvdXQgc3RhY2sgYWZ0ZXIgc3RhY2tPZmZzZXRcbiAgICAgICAgICBpZiAodGhpcy5zdGFjay5sZW5ndGggPiB0aGlzLnN0YWNrT2Zmc2V0KSB7XG4gICAgICAgICAgICAgIHRoaXMuc3RhY2sgPSB0aGlzLnN0YWNrLnNsaWNlKDAsIHRoaXMuc3RhY2tPZmZzZXQpO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBDcmVhdGUgbmV3IHNuYXBzaG90IGFuZCBwdXNoIGl0IHRvIHRoZSBlbmRcbiAgICAgICAgICB0aGlzLnN0YWNrLnB1c2godGhpcy5tYWtlU25hcHNob3QoKSk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIEhpc3Rvcnk7XG4gIH0oKSk7XG5cbiAgdmFyIFN0eWxlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gU3R5bGUoKSB7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEBtZXRob2QgalF1ZXJ5Q1NTXG4gICAgICAgKlxuICAgICAgICogW3dvcmthcm91bmRdIGZvciBvbGQgalF1ZXJ5XG4gICAgICAgKiBwYXNzaW5nIGFuIGFycmF5IG9mIHN0eWxlIHByb3BlcnRpZXMgdG8gLmNzcygpXG4gICAgICAgKiB3aWxsIHJlc3VsdCBpbiBhbiBvYmplY3Qgb2YgcHJvcGVydHktdmFsdWUgcGFpcnMuXG4gICAgICAgKiAoY29tcGFiaWxpdHkgd2l0aCB2ZXJzaW9uIDwgMS45KVxuICAgICAgICpcbiAgICAgICAqIEBwcml2YXRlXG4gICAgICAgKiBAcGFyYW0gIHtqUXVlcnl9ICRvYmpcbiAgICAgICAqIEBwYXJhbSAge0FycmF5fSBwcm9wZXJ0eU5hbWVzIC0gQW4gYXJyYXkgb2Ygb25lIG9yIG1vcmUgQ1NTIHByb3BlcnRpZXMuXG4gICAgICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAgICAgKi9cbiAgICAgIFN0eWxlLnByb3RvdHlwZS5qUXVlcnlDU1MgPSBmdW5jdGlvbiAoJG9iaiwgcHJvcGVydHlOYW1lcykge1xuICAgICAgICAgIGlmIChlbnYuanF1ZXJ5VmVyc2lvbiA8IDEuOSkge1xuICAgICAgICAgICAgICB2YXIgcmVzdWx0XzEgPSB7fTtcbiAgICAgICAgICAgICAgJCQxLmVhY2gocHJvcGVydHlOYW1lcywgZnVuY3Rpb24gKGlkeCwgcHJvcGVydHlOYW1lKSB7XG4gICAgICAgICAgICAgICAgICByZXN1bHRfMVtwcm9wZXJ0eU5hbWVdID0gJG9iai5jc3MocHJvcGVydHlOYW1lKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHJldHVybiByZXN1bHRfMTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuICRvYmouY3NzKHByb3BlcnR5TmFtZXMpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmV0dXJucyBzdHlsZSBvYmplY3QgZnJvbSBub2RlXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtqUXVlcnl9ICRub2RlXG4gICAgICAgKiBAcmV0dXJuIHtPYmplY3R9XG4gICAgICAgKi9cbiAgICAgIFN0eWxlLnByb3RvdHlwZS5mcm9tTm9kZSA9IGZ1bmN0aW9uICgkbm9kZSkge1xuICAgICAgICAgIHZhciBwcm9wZXJ0aWVzID0gWydmb250LWZhbWlseScsICdmb250LXNpemUnLCAndGV4dC1hbGlnbicsICdsaXN0LXN0eWxlLXR5cGUnLCAnbGluZS1oZWlnaHQnXTtcbiAgICAgICAgICB2YXIgc3R5bGVJbmZvID0gdGhpcy5qUXVlcnlDU1MoJG5vZGUsIHByb3BlcnRpZXMpIHx8IHt9O1xuICAgICAgICAgIHN0eWxlSW5mb1snZm9udC1zaXplJ10gPSBwYXJzZUludChzdHlsZUluZm9bJ2ZvbnQtc2l6ZSddLCAxMCk7XG4gICAgICAgICAgcmV0dXJuIHN0eWxlSW5mbztcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHBhcmFncmFwaCBsZXZlbCBzdHlsZVxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7V3JhcHBlZFJhbmdlfSBybmdcbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBzdHlsZUluZm9cbiAgICAgICAqL1xuICAgICAgU3R5bGUucHJvdG90eXBlLnN0eWxlUGFyYSA9IGZ1bmN0aW9uIChybmcsIHN0eWxlSW5mbykge1xuICAgICAgICAgICQkMS5lYWNoKHJuZy5ub2Rlcyhkb20uaXNQYXJhLCB7XG4gICAgICAgICAgICAgIGluY2x1ZGVBbmNlc3RvcjogdHJ1ZVxuICAgICAgICAgIH0pLCBmdW5jdGlvbiAoaWR4LCBwYXJhKSB7XG4gICAgICAgICAgICAgICQkMShwYXJhKS5jc3Moc3R5bGVJbmZvKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGluc2VydCBhbmQgcmV0dXJucyBzdHlsZU5vZGVzIG9uIHJhbmdlLlxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7V3JhcHBlZFJhbmdlfSBybmdcbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBbb3B0aW9uc10gLSBvcHRpb25zIGZvciBzdHlsZU5vZGVzXG4gICAgICAgKiBAcGFyYW0ge1N0cmluZ30gW29wdGlvbnMubm9kZU5hbWVdIC0gZGVmYXVsdDogYFNQQU5gXG4gICAgICAgKiBAcGFyYW0ge0Jvb2xlYW59IFtvcHRpb25zLmV4cGFuZENsb3Nlc3RTaWJsaW5nXSAtIGRlZmF1bHQ6IGBmYWxzZWBcbiAgICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gW29wdGlvbnMub25seVBhcnRpYWxDb250YWluc10gLSBkZWZhdWx0OiBgZmFsc2VgXG4gICAgICAgKiBAcmV0dXJuIHtOb2RlW119XG4gICAgICAgKi9cbiAgICAgIFN0eWxlLnByb3RvdHlwZS5zdHlsZU5vZGVzID0gZnVuY3Rpb24gKHJuZywgb3B0aW9ucykge1xuICAgICAgICAgIHJuZyA9IHJuZy5zcGxpdFRleHQoKTtcbiAgICAgICAgICB2YXIgbm9kZU5hbWUgPSAob3B0aW9ucyAmJiBvcHRpb25zLm5vZGVOYW1lKSB8fCAnU1BBTic7XG4gICAgICAgICAgdmFyIGV4cGFuZENsb3Nlc3RTaWJsaW5nID0gISEob3B0aW9ucyAmJiBvcHRpb25zLmV4cGFuZENsb3Nlc3RTaWJsaW5nKTtcbiAgICAgICAgICB2YXIgb25seVBhcnRpYWxDb250YWlucyA9ICEhKG9wdGlvbnMgJiYgb3B0aW9ucy5vbmx5UGFydGlhbENvbnRhaW5zKTtcbiAgICAgICAgICBpZiAocm5nLmlzQ29sbGFwc2VkKCkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIFtybmcuaW5zZXJ0Tm9kZShkb20uY3JlYXRlKG5vZGVOYW1lKSldO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgcHJlZCA9IGRvbS5tYWtlUHJlZEJ5Tm9kZU5hbWUobm9kZU5hbWUpO1xuICAgICAgICAgIHZhciBub2RlcyA9IHJuZy5ub2Rlcyhkb20uaXNUZXh0LCB7XG4gICAgICAgICAgICAgIGZ1bGx5Q29udGFpbnM6IHRydWVcbiAgICAgICAgICB9KS5tYXAoZnVuY3Rpb24gKHRleHQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGRvbS5zaW5nbGVDaGlsZEFuY2VzdG9yKHRleHQsIHByZWQpIHx8IGRvbS53cmFwKHRleHQsIG5vZGVOYW1lKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBpZiAoZXhwYW5kQ2xvc2VzdFNpYmxpbmcpIHtcbiAgICAgICAgICAgICAgaWYgKG9ubHlQYXJ0aWFsQ29udGFpbnMpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBub2Rlc0luUmFuZ2VfMSA9IHJuZy5ub2RlcygpO1xuICAgICAgICAgICAgICAgICAgLy8gY29tcG9zZSB3aXRoIHBhcnRpYWwgY29udGFpbnMgcHJlZGljYXRpb25cbiAgICAgICAgICAgICAgICAgIHByZWQgPSBmdW5jLmFuZChwcmVkLCBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBsaXN0cy5jb250YWlucyhub2Rlc0luUmFuZ2VfMSwgbm9kZSk7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZXR1cm4gbm9kZXMubWFwKGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgc2libGluZ3MgPSBkb20ud2l0aENsb3Nlc3RTaWJsaW5ncyhub2RlLCBwcmVkKTtcbiAgICAgICAgICAgICAgICAgIHZhciBoZWFkID0gbGlzdHMuaGVhZChzaWJsaW5ncyk7XG4gICAgICAgICAgICAgICAgICB2YXIgdGFpbHMgPSBsaXN0cy50YWlsKHNpYmxpbmdzKTtcbiAgICAgICAgICAgICAgICAgICQkMS5lYWNoKHRhaWxzLCBmdW5jdGlvbiAoaWR4LCBlbGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgZG9tLmFwcGVuZENoaWxkTm9kZXMoaGVhZCwgZWxlbS5jaGlsZE5vZGVzKTtcbiAgICAgICAgICAgICAgICAgICAgICBkb20ucmVtb3ZlKGVsZW0pO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gbGlzdHMuaGVhZChzaWJsaW5ncyk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgcmV0dXJuIG5vZGVzO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGdldCBjdXJyZW50IHN0eWxlIG9uIGN1cnNvclxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7V3JhcHBlZFJhbmdlfSBybmdcbiAgICAgICAqIEByZXR1cm4ge09iamVjdH0gLSBvYmplY3QgY29udGFpbnMgc3R5bGUgcHJvcGVydGllcy5cbiAgICAgICAqL1xuICAgICAgU3R5bGUucHJvdG90eXBlLmN1cnJlbnQgPSBmdW5jdGlvbiAocm5nKSB7XG4gICAgICAgICAgdmFyICRjb250ID0gJCQxKCFkb20uaXNFbGVtZW50KHJuZy5zYykgPyBybmcuc2MucGFyZW50Tm9kZSA6IHJuZy5zYyk7XG4gICAgICAgICAgdmFyIHN0eWxlSW5mbyA9IHRoaXMuZnJvbU5vZGUoJGNvbnQpO1xuICAgICAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN0YXRlIGZvciB0b2dnbGUgc3RhdGVcbiAgICAgICAgICAvLyBbd29ya2Fyb3VuZF0gcHJldmVudCBGaXJlZm94IG5zcmVzdWx0OiBcIjB4ODAwMDQwMDUgKE5TX0VSUk9SX0ZBSUxVUkUpXCJcbiAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICBzdHlsZUluZm8gPSAkJDEuZXh0ZW5kKHN0eWxlSW5mbywge1xuICAgICAgICAgICAgICAgICAgJ2ZvbnQtYm9sZCc6IGRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN0YXRlKCdib2xkJykgPyAnYm9sZCcgOiAnbm9ybWFsJyxcbiAgICAgICAgICAgICAgICAgICdmb250LWl0YWxpYyc6IGRvY3VtZW50LnF1ZXJ5Q29tbWFuZFN0YXRlKCdpdGFsaWMnKSA/ICdpdGFsaWMnIDogJ25vcm1hbCcsXG4gICAgICAgICAgICAgICAgICAnZm9udC11bmRlcmxpbmUnOiBkb2N1bWVudC5xdWVyeUNvbW1hbmRTdGF0ZSgndW5kZXJsaW5lJykgPyAndW5kZXJsaW5lJyA6ICdub3JtYWwnLFxuICAgICAgICAgICAgICAgICAgJ2ZvbnQtc3Vic2NyaXB0JzogZG9jdW1lbnQucXVlcnlDb21tYW5kU3RhdGUoJ3N1YnNjcmlwdCcpID8gJ3N1YnNjcmlwdCcgOiAnbm9ybWFsJyxcbiAgICAgICAgICAgICAgICAgICdmb250LXN1cGVyc2NyaXB0JzogZG9jdW1lbnQucXVlcnlDb21tYW5kU3RhdGUoJ3N1cGVyc2NyaXB0JykgPyAnc3VwZXJzY3JpcHQnIDogJ25vcm1hbCcsXG4gICAgICAgICAgICAgICAgICAnZm9udC1zdHJpa2V0aHJvdWdoJzogZG9jdW1lbnQucXVlcnlDb21tYW5kU3RhdGUoJ3N0cmlrZXRocm91Z2gnKSA/ICdzdHJpa2V0aHJvdWdoJyA6ICdub3JtYWwnLFxuICAgICAgICAgICAgICAgICAgJ2ZvbnQtZmFtaWx5JzogZG9jdW1lbnQucXVlcnlDb21tYW5kVmFsdWUoJ2ZvbnRuYW1lJykgfHwgc3R5bGVJbmZvWydmb250LWZhbWlseSddXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBjYXRjaCAoZSkgeyB9XG4gICAgICAgICAgLy8gbGlzdC1zdHlsZS10eXBlIHRvIGxpc3Qtc3R5bGUodW5vcmRlcmVkLCBvcmRlcmVkKVxuICAgICAgICAgIGlmICghcm5nLmlzT25MaXN0KCkpIHtcbiAgICAgICAgICAgICAgc3R5bGVJbmZvWydsaXN0LXN0eWxlJ10gPSAnbm9uZSc7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB2YXIgb3JkZXJlZFR5cGVzID0gWydjaXJjbGUnLCAnZGlzYycsICdkaXNjLWxlYWRpbmctemVybycsICdzcXVhcmUnXTtcbiAgICAgICAgICAgICAgdmFyIGlzVW5vcmRlcmVkID0gb3JkZXJlZFR5cGVzLmluZGV4T2Yoc3R5bGVJbmZvWydsaXN0LXN0eWxlLXR5cGUnXSkgPiAtMTtcbiAgICAgICAgICAgICAgc3R5bGVJbmZvWydsaXN0LXN0eWxlJ10gPSBpc1Vub3JkZXJlZCA/ICd1bm9yZGVyZWQnIDogJ29yZGVyZWQnO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgcGFyYSA9IGRvbS5hbmNlc3Rvcihybmcuc2MsIGRvbS5pc1BhcmEpO1xuICAgICAgICAgIGlmIChwYXJhICYmIHBhcmEuc3R5bGVbJ2xpbmUtaGVpZ2h0J10pIHtcbiAgICAgICAgICAgICAgc3R5bGVJbmZvWydsaW5lLWhlaWdodCddID0gcGFyYS5zdHlsZS5saW5lSGVpZ2h0O1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdmFyIGxpbmVIZWlnaHQgPSBwYXJzZUludChzdHlsZUluZm9bJ2xpbmUtaGVpZ2h0J10sIDEwKSAvIHBhcnNlSW50KHN0eWxlSW5mb1snZm9udC1zaXplJ10sIDEwKTtcbiAgICAgICAgICAgICAgc3R5bGVJbmZvWydsaW5lLWhlaWdodCddID0gbGluZUhlaWdodC50b0ZpeGVkKDEpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBzdHlsZUluZm8uYW5jaG9yID0gcm5nLmlzT25BbmNob3IoKSAmJiBkb20uYW5jZXN0b3Iocm5nLnNjLCBkb20uaXNBbmNob3IpO1xuICAgICAgICAgIHN0eWxlSW5mby5hbmNlc3RvcnMgPSBkb20ubGlzdEFuY2VzdG9yKHJuZy5zYywgZG9tLmlzRWRpdGFibGUpO1xuICAgICAgICAgIHN0eWxlSW5mby5yYW5nZSA9IHJuZztcbiAgICAgICAgICByZXR1cm4gc3R5bGVJbmZvO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBTdHlsZTtcbiAgfSgpKTtcblxuICB2YXIgQnVsbGV0ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gQnVsbGV0KCkge1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiB0b2dnbGUgb3JkZXJlZCBsaXN0XG4gICAgICAgKi9cbiAgICAgIEJ1bGxldC5wcm90b3R5cGUuaW5zZXJ0T3JkZXJlZExpc3QgPSBmdW5jdGlvbiAoZWRpdGFibGUpIHtcbiAgICAgICAgICB0aGlzLnRvZ2dsZUxpc3QoJ09MJywgZWRpdGFibGUpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogdG9nZ2xlIHVub3JkZXJlZCBsaXN0XG4gICAgICAgKi9cbiAgICAgIEJ1bGxldC5wcm90b3R5cGUuaW5zZXJ0VW5vcmRlcmVkTGlzdCA9IGZ1bmN0aW9uIChlZGl0YWJsZSkge1xuICAgICAgICAgIHRoaXMudG9nZ2xlTGlzdCgnVUwnLCBlZGl0YWJsZSk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBpbmRlbnRcbiAgICAgICAqL1xuICAgICAgQnVsbGV0LnByb3RvdHlwZS5pbmRlbnQgPSBmdW5jdGlvbiAoZWRpdGFibGUpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHZhciBybmcgPSByYW5nZS5jcmVhdGUoZWRpdGFibGUpLndyYXBCb2R5SW5saW5lV2l0aFBhcmEoKTtcbiAgICAgICAgICB2YXIgcGFyYXMgPSBybmcubm9kZXMoZG9tLmlzUGFyYSwgeyBpbmNsdWRlQW5jZXN0b3I6IHRydWUgfSk7XG4gICAgICAgICAgdmFyIGNsdXN0ZXJlZHMgPSBsaXN0cy5jbHVzdGVyQnkocGFyYXMsIGZ1bmMucGVxMigncGFyZW50Tm9kZScpKTtcbiAgICAgICAgICAkJDEuZWFjaChjbHVzdGVyZWRzLCBmdW5jdGlvbiAoaWR4LCBwYXJhcykge1xuICAgICAgICAgICAgICB2YXIgaGVhZCA9IGxpc3RzLmhlYWQocGFyYXMpO1xuICAgICAgICAgICAgICBpZiAoZG9tLmlzTGkoaGVhZCkpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBwcmV2aW91c0xpc3RfMSA9IF90aGlzLmZpbmRMaXN0KGhlYWQucHJldmlvdXNTaWJsaW5nKTtcbiAgICAgICAgICAgICAgICAgIGlmIChwcmV2aW91c0xpc3RfMSkge1xuICAgICAgICAgICAgICAgICAgICAgIHBhcmFzXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoZnVuY3Rpb24gKHBhcmEpIHsgcmV0dXJuIHByZXZpb3VzTGlzdF8xLmFwcGVuZENoaWxkKHBhcmEpOyB9KTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLndyYXBMaXN0KHBhcmFzLCBoZWFkLnBhcmVudE5vZGUubm9kZU5hbWUpO1xuICAgICAgICAgICAgICAgICAgICAgIHBhcmFzXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoZnVuY3Rpb24gKHBhcmEpIHsgcmV0dXJuIHBhcmEucGFyZW50Tm9kZTsgfSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLm1hcChmdW5jdGlvbiAocGFyYSkgeyByZXR1cm4gX3RoaXMuYXBwZW5kVG9QcmV2aW91cyhwYXJhKTsgfSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAkJDEuZWFjaChwYXJhcywgZnVuY3Rpb24gKGlkeCwgcGFyYSkge1xuICAgICAgICAgICAgICAgICAgICAgICQkMShwYXJhKS5jc3MoJ21hcmdpbkxlZnQnLCBmdW5jdGlvbiAoaWR4LCB2YWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChwYXJzZUludCh2YWwsIDEwKSB8fCAwKSArIDI1O1xuICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICBybmcuc2VsZWN0KCk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBvdXRkZW50XG4gICAgICAgKi9cbiAgICAgIEJ1bGxldC5wcm90b3R5cGUub3V0ZGVudCA9IGZ1bmN0aW9uIChlZGl0YWJsZSkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdmFyIHJuZyA9IHJhbmdlLmNyZWF0ZShlZGl0YWJsZSkud3JhcEJvZHlJbmxpbmVXaXRoUGFyYSgpO1xuICAgICAgICAgIHZhciBwYXJhcyA9IHJuZy5ub2Rlcyhkb20uaXNQYXJhLCB7IGluY2x1ZGVBbmNlc3RvcjogdHJ1ZSB9KTtcbiAgICAgICAgICB2YXIgY2x1c3RlcmVkcyA9IGxpc3RzLmNsdXN0ZXJCeShwYXJhcywgZnVuYy5wZXEyKCdwYXJlbnROb2RlJykpO1xuICAgICAgICAgICQkMS5lYWNoKGNsdXN0ZXJlZHMsIGZ1bmN0aW9uIChpZHgsIHBhcmFzKSB7XG4gICAgICAgICAgICAgIHZhciBoZWFkID0gbGlzdHMuaGVhZChwYXJhcyk7XG4gICAgICAgICAgICAgIGlmIChkb20uaXNMaShoZWFkKSkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMucmVsZWFzZUxpc3QoW3BhcmFzXSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAkJDEuZWFjaChwYXJhcywgZnVuY3Rpb24gKGlkeCwgcGFyYSkge1xuICAgICAgICAgICAgICAgICAgICAgICQkMShwYXJhKS5jc3MoJ21hcmdpbkxlZnQnLCBmdW5jdGlvbiAoaWR4LCB2YWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsID0gKHBhcnNlSW50KHZhbCwgMTApIHx8IDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdmFsID4gMjUgPyB2YWwgLSAyNSA6ICcnO1xuICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICBybmcuc2VsZWN0KCk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiB0b2dnbGUgbGlzdFxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSBsaXN0TmFtZSAtIE9MIG9yIFVMXG4gICAgICAgKi9cbiAgICAgIEJ1bGxldC5wcm90b3R5cGUudG9nZ2xlTGlzdCA9IGZ1bmN0aW9uIChsaXN0TmFtZSwgZWRpdGFibGUpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHZhciBybmcgPSByYW5nZS5jcmVhdGUoZWRpdGFibGUpLndyYXBCb2R5SW5saW5lV2l0aFBhcmEoKTtcbiAgICAgICAgICB2YXIgcGFyYXMgPSBybmcubm9kZXMoZG9tLmlzUGFyYSwgeyBpbmNsdWRlQW5jZXN0b3I6IHRydWUgfSk7XG4gICAgICAgICAgdmFyIGJvb2ttYXJrID0gcm5nLnBhcmFCb29rbWFyayhwYXJhcyk7XG4gICAgICAgICAgdmFyIGNsdXN0ZXJlZHMgPSBsaXN0cy5jbHVzdGVyQnkocGFyYXMsIGZ1bmMucGVxMigncGFyZW50Tm9kZScpKTtcbiAgICAgICAgICAvLyBwYXJhZ3JhcGggdG8gbGlzdFxuICAgICAgICAgIGlmIChsaXN0cy5maW5kKHBhcmFzLCBkb20uaXNQdXJlUGFyYSkpIHtcbiAgICAgICAgICAgICAgdmFyIHdyYXBwZWRQYXJhc18xID0gW107XG4gICAgICAgICAgICAgICQkMS5lYWNoKGNsdXN0ZXJlZHMsIGZ1bmN0aW9uIChpZHgsIHBhcmFzKSB7XG4gICAgICAgICAgICAgICAgICB3cmFwcGVkUGFyYXNfMSA9IHdyYXBwZWRQYXJhc18xLmNvbmNhdChfdGhpcy53cmFwTGlzdChwYXJhcywgbGlzdE5hbWUpKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHBhcmFzID0gd3JhcHBlZFBhcmFzXzE7XG4gICAgICAgICAgICAgIC8vIGxpc3QgdG8gcGFyYWdyYXBoIG9yIGNoYW5nZSBsaXN0IHN0eWxlXG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB2YXIgZGlmZkxpc3RzID0gcm5nLm5vZGVzKGRvbS5pc0xpc3QsIHtcbiAgICAgICAgICAgICAgICAgIGluY2x1ZGVBbmNlc3RvcjogdHJ1ZVxuICAgICAgICAgICAgICB9KS5maWx0ZXIoZnVuY3Rpb24gKGxpc3ROb2RlKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gISQkMS5ub2RlTmFtZShsaXN0Tm9kZSwgbGlzdE5hbWUpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgaWYgKGRpZmZMaXN0cy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICQkMS5lYWNoKGRpZmZMaXN0cywgZnVuY3Rpb24gKGlkeCwgbGlzdE5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICBkb20ucmVwbGFjZShsaXN0Tm9kZSwgbGlzdE5hbWUpO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBwYXJhcyA9IHRoaXMucmVsZWFzZUxpc3QoY2x1c3RlcmVkcywgdHJ1ZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgcmFuZ2UuY3JlYXRlRnJvbVBhcmFCb29rbWFyayhib29rbWFyaywgcGFyYXMpLnNlbGVjdCgpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQHBhcmFtIHtOb2RlW119IHBhcmFzXG4gICAgICAgKiBAcGFyYW0ge1N0cmluZ30gbGlzdE5hbWVcbiAgICAgICAqIEByZXR1cm4ge05vZGVbXX1cbiAgICAgICAqL1xuICAgICAgQnVsbGV0LnByb3RvdHlwZS53cmFwTGlzdCA9IGZ1bmN0aW9uIChwYXJhcywgbGlzdE5hbWUpIHtcbiAgICAgICAgICB2YXIgaGVhZCA9IGxpc3RzLmhlYWQocGFyYXMpO1xuICAgICAgICAgIHZhciBsYXN0ID0gbGlzdHMubGFzdChwYXJhcyk7XG4gICAgICAgICAgdmFyIHByZXZMaXN0ID0gZG9tLmlzTGlzdChoZWFkLnByZXZpb3VzU2libGluZykgJiYgaGVhZC5wcmV2aW91c1NpYmxpbmc7XG4gICAgICAgICAgdmFyIG5leHRMaXN0ID0gZG9tLmlzTGlzdChsYXN0Lm5leHRTaWJsaW5nKSAmJiBsYXN0Lm5leHRTaWJsaW5nO1xuICAgICAgICAgIHZhciBsaXN0Tm9kZSA9IHByZXZMaXN0IHx8IGRvbS5pbnNlcnRBZnRlcihkb20uY3JlYXRlKGxpc3ROYW1lIHx8ICdVTCcpLCBsYXN0KTtcbiAgICAgICAgICAvLyBQIHRvIExJXG4gICAgICAgICAgcGFyYXMgPSBwYXJhcy5tYXAoZnVuY3Rpb24gKHBhcmEpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGRvbS5pc1B1cmVQYXJhKHBhcmEpID8gZG9tLnJlcGxhY2UocGFyYSwgJ0xJJykgOiBwYXJhO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIGFwcGVuZCB0byBsaXN0KDx1bD4sIDxvbD4pXG4gICAgICAgICAgZG9tLmFwcGVuZENoaWxkTm9kZXMobGlzdE5vZGUsIHBhcmFzKTtcbiAgICAgICAgICBpZiAobmV4dExpc3QpIHtcbiAgICAgICAgICAgICAgZG9tLmFwcGVuZENoaWxkTm9kZXMobGlzdE5vZGUsIGxpc3RzLmZyb20obmV4dExpc3QuY2hpbGROb2RlcykpO1xuICAgICAgICAgICAgICBkb20ucmVtb3ZlKG5leHRMaXN0KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHBhcmFzO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZCByZWxlYXNlTGlzdFxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7QXJyYXlbXX0gY2x1c3RlcmVkc1xuICAgICAgICogQHBhcmFtIHtCb29sZWFufSBpc0VzY2Fwc2VUb0JvZHlcbiAgICAgICAqIEByZXR1cm4ge05vZGVbXX1cbiAgICAgICAqL1xuICAgICAgQnVsbGV0LnByb3RvdHlwZS5yZWxlYXNlTGlzdCA9IGZ1bmN0aW9uIChjbHVzdGVyZWRzLCBpc0VzY2Fwc2VUb0JvZHkpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHZhciByZWxlYXNlZFBhcmFzID0gW107XG4gICAgICAgICAgJCQxLmVhY2goY2x1c3RlcmVkcywgZnVuY3Rpb24gKGlkeCwgcGFyYXMpIHtcbiAgICAgICAgICAgICAgdmFyIGhlYWQgPSBsaXN0cy5oZWFkKHBhcmFzKTtcbiAgICAgICAgICAgICAgdmFyIGxhc3QgPSBsaXN0cy5sYXN0KHBhcmFzKTtcbiAgICAgICAgICAgICAgdmFyIGhlYWRMaXN0ID0gaXNFc2NhcHNlVG9Cb2R5ID8gZG9tLmxhc3RBbmNlc3RvcihoZWFkLCBkb20uaXNMaXN0KSA6IGhlYWQucGFyZW50Tm9kZTtcbiAgICAgICAgICAgICAgdmFyIHBhcmVudEl0ZW0gPSBoZWFkTGlzdC5wYXJlbnROb2RlO1xuICAgICAgICAgICAgICBpZiAoaGVhZExpc3QucGFyZW50Tm9kZS5ub2RlTmFtZSA9PT0gJ0xJJykge1xuICAgICAgICAgICAgICAgICAgcGFyYXMubWFwKGZ1bmN0aW9uIChwYXJhKSB7XG4gICAgICAgICAgICAgICAgICAgICAgdmFyIG5ld0xpc3QgPSBfdGhpcy5maW5kTmV4dFNpYmxpbmdzKHBhcmEpO1xuICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnRJdGVtLm5leHRTaWJsaW5nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmVudEl0ZW0ucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUocGFyYSwgcGFyZW50SXRlbS5uZXh0U2libGluZyk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJlbnRJdGVtLnBhcmVudE5vZGUuYXBwZW5kQ2hpbGQocGFyYSk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGlmIChuZXdMaXN0Lmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy53cmFwTGlzdChuZXdMaXN0LCBoZWFkTGlzdC5ub2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHBhcmEuYXBwZW5kQ2hpbGQobmV3TGlzdFswXS5wYXJlbnROb2RlKTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgIGlmIChoZWFkTGlzdC5jaGlsZHJlbi5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICBwYXJlbnRJdGVtLnJlbW92ZUNoaWxkKGhlYWRMaXN0KTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGlmIChwYXJlbnRJdGVtLmNoaWxkTm9kZXMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgcGFyZW50SXRlbS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHBhcmVudEl0ZW0pO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgdmFyIGxhc3RMaXN0ID0gaGVhZExpc3QuY2hpbGROb2Rlcy5sZW5ndGggPiAxID8gZG9tLnNwbGl0VHJlZShoZWFkTGlzdCwge1xuICAgICAgICAgICAgICAgICAgICAgIG5vZGU6IGxhc3QucGFyZW50Tm9kZSxcbiAgICAgICAgICAgICAgICAgICAgICBvZmZzZXQ6IGRvbS5wb3NpdGlvbihsYXN0KSArIDFcbiAgICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgICBpc1NraXBQYWRkaW5nQmxhbmtIVE1MOiB0cnVlXG4gICAgICAgICAgICAgICAgICB9KSA6IG51bGw7XG4gICAgICAgICAgICAgICAgICB2YXIgbWlkZGxlTGlzdCA9IGRvbS5zcGxpdFRyZWUoaGVhZExpc3QsIHtcbiAgICAgICAgICAgICAgICAgICAgICBub2RlOiBoZWFkLnBhcmVudE5vZGUsXG4gICAgICAgICAgICAgICAgICAgICAgb2Zmc2V0OiBkb20ucG9zaXRpb24oaGVhZClcbiAgICAgICAgICAgICAgICAgIH0sIHtcbiAgICAgICAgICAgICAgICAgICAgICBpc1NraXBQYWRkaW5nQmxhbmtIVE1MOiB0cnVlXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgIHBhcmFzID0gaXNFc2NhcHNlVG9Cb2R5ID8gZG9tLmxpc3REZXNjZW5kYW50KG1pZGRsZUxpc3QsIGRvbS5pc0xpKVxuICAgICAgICAgICAgICAgICAgICAgIDogbGlzdHMuZnJvbShtaWRkbGVMaXN0LmNoaWxkTm9kZXMpLmZpbHRlcihkb20uaXNMaSk7XG4gICAgICAgICAgICAgICAgICAvLyBMSSB0byBQXG4gICAgICAgICAgICAgICAgICBpZiAoaXNFc2NhcHNlVG9Cb2R5IHx8ICFkb20uaXNMaXN0KGhlYWRMaXN0LnBhcmVudE5vZGUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgcGFyYXMgPSBwYXJhcy5tYXAoZnVuY3Rpb24gKHBhcmEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGRvbS5yZXBsYWNlKHBhcmEsICdQJyk7XG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAkJDEuZWFjaChsaXN0cy5mcm9tKHBhcmFzKS5yZXZlcnNlKCksIGZ1bmN0aW9uIChpZHgsIHBhcmEpIHtcbiAgICAgICAgICAgICAgICAgICAgICBkb20uaW5zZXJ0QWZ0ZXIocGFyYSwgaGVhZExpc3QpO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAvLyByZW1vdmUgZW1wdHkgbGlzdHNcbiAgICAgICAgICAgICAgICAgIHZhciByb290TGlzdHMgPSBsaXN0cy5jb21wYWN0KFtoZWFkTGlzdCwgbWlkZGxlTGlzdCwgbGFzdExpc3RdKTtcbiAgICAgICAgICAgICAgICAgICQkMS5lYWNoKHJvb3RMaXN0cywgZnVuY3Rpb24gKGlkeCwgcm9vdExpc3QpIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgbGlzdE5vZGVzID0gW3Jvb3RMaXN0XS5jb25jYXQoZG9tLmxpc3REZXNjZW5kYW50KHJvb3RMaXN0LCBkb20uaXNMaXN0KSk7XG4gICAgICAgICAgICAgICAgICAgICAgJCQxLmVhY2gobGlzdE5vZGVzLnJldmVyc2UoKSwgZnVuY3Rpb24gKGlkeCwgbGlzdE5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFkb20ubm9kZUxlbmd0aChsaXN0Tm9kZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvbS5yZW1vdmUobGlzdE5vZGUsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICByZWxlYXNlZFBhcmFzID0gcmVsZWFzZWRQYXJhcy5jb25jYXQocGFyYXMpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiByZWxlYXNlZFBhcmFzO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZCBhcHBlbmRUb1ByZXZpb3VzXG4gICAgICAgKlxuICAgICAgICogQXBwZW5kcyBsaXN0IHRvIHByZXZpb3VzIGxpc3QgaXRlbSwgaWZcbiAgICAgICAqIG5vbmUgZXhpc3QgaXQgd3JhcHMgdGhlIGxpc3QgaW4gYSBuZXcgbGlzdCBpdGVtLlxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7SFRNTE5vZGV9IExpc3RJdGVtXG4gICAgICAgKiBAcmV0dXJuIHtIVE1MTm9kZX1cbiAgICAgICAqL1xuICAgICAgQnVsbGV0LnByb3RvdHlwZS5hcHBlbmRUb1ByZXZpb3VzID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZS5wcmV2aW91c1NpYmxpbmdcbiAgICAgICAgICAgICAgPyBkb20uYXBwZW5kQ2hpbGROb2Rlcyhub2RlLnByZXZpb3VzU2libGluZywgW25vZGVdKVxuICAgICAgICAgICAgICA6IHRoaXMud3JhcExpc3QoW25vZGVdLCAnTEknKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIEBtZXRob2QgZmluZExpc3RcbiAgICAgICAqXG4gICAgICAgKiBGaW5kcyBhbiBleGlzdGluZyBsaXN0IGluIGxpc3QgaXRlbVxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7SFRNTE5vZGV9IExpc3RJdGVtXG4gICAgICAgKiBAcmV0dXJuIHtBcnJheVtdfVxuICAgICAgICovXG4gICAgICBCdWxsZXQucHJvdG90eXBlLmZpbmRMaXN0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICByZXR1cm4gbm9kZVxuICAgICAgICAgICAgICA/IGxpc3RzLmZpbmQobm9kZS5jaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7IHJldHVybiBbJ09MJywgJ1VMJ10uaW5kZXhPZihjaGlsZC5ub2RlTmFtZSkgPiAtMTsgfSlcbiAgICAgICAgICAgICAgOiBudWxsO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogQG1ldGhvZCBmaW5kTmV4dFNpYmxpbmdzXG4gICAgICAgKlxuICAgICAgICogRmluZHMgYWxsIGxpc3QgaXRlbSBzaWJsaW5ncyB0aGF0IGZvbGxvdyBpdFxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7SFRNTE5vZGV9IExpc3RJdGVtXG4gICAgICAgKiBAcmV0dXJuIHtIVE1MTm9kZX1cbiAgICAgICAqL1xuICAgICAgQnVsbGV0LnByb3RvdHlwZS5maW5kTmV4dFNpYmxpbmdzID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICB2YXIgc2libGluZ3MgPSBbXTtcbiAgICAgICAgICB3aGlsZSAobm9kZS5uZXh0U2libGluZykge1xuICAgICAgICAgICAgICBzaWJsaW5ncy5wdXNoKG5vZGUubmV4dFNpYmxpbmcpO1xuICAgICAgICAgICAgICBub2RlID0gbm9kZS5uZXh0U2libGluZztcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHNpYmxpbmdzO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBCdWxsZXQ7XG4gIH0oKSk7XG5cbiAgLyoqXG4gICAqIEBjbGFzcyBlZGl0aW5nLlR5cGluZ1xuICAgKlxuICAgKiBUeXBpbmdcbiAgICpcbiAgICovXG4gIHZhciBUeXBpbmcgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBUeXBpbmcoY29udGV4dCkge1xuICAgICAgICAgIC8vIGEgQnVsbGV0IGluc3RhbmNlIHRvIHRvZ2dsZSBsaXN0cyBvZmZcbiAgICAgICAgICB0aGlzLmJ1bGxldCA9IG5ldyBCdWxsZXQoKTtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIGluc2VydCB0YWJcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nXG4gICAgICAgKiBAcGFyYW0ge051bWJlcn0gdGFic2l6ZVxuICAgICAgICovXG4gICAgICBUeXBpbmcucHJvdG90eXBlLmluc2VydFRhYiA9IGZ1bmN0aW9uIChybmcsIHRhYnNpemUpIHtcbiAgICAgICAgICB2YXIgdGFiID0gZG9tLmNyZWF0ZVRleHQobmV3IEFycmF5KHRhYnNpemUgKyAxKS5qb2luKGRvbS5OQlNQX0NIQVIpKTtcbiAgICAgICAgICBybmcgPSBybmcuZGVsZXRlQ29udGVudHMoKTtcbiAgICAgICAgICBybmcuaW5zZXJ0Tm9kZSh0YWIsIHRydWUpO1xuICAgICAgICAgIHJuZyA9IHJhbmdlLmNyZWF0ZSh0YWIsIHRhYnNpemUpO1xuICAgICAgICAgIHJuZy5zZWxlY3QoKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGluc2VydCBwYXJhZ3JhcGhcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge2pRdWVyeX0gJGVkaXRhYmxlXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nIENhbiBiZSB1c2VkIGluIHVuaXQgdGVzdHMgdG8gXCJtb2NrXCIgdGhlIHJhbmdlXG4gICAgICAgKlxuICAgICAgICogYmxvY2txdW90ZUJyZWFraW5nTGV2ZWxcbiAgICAgICAqICAgMCAtIE5vIGJyZWFrLCB0aGUgbmV3IHBhcmFncmFwaCByZW1haW5zIGluc2lkZSB0aGUgcXVvdGVcbiAgICAgICAqICAgMSAtIEJyZWFrIHRoZSBmaXJzdCBibG9ja3F1b3RlIGluIHRoZSBhbmNlc3RvcnMgbGlzdFxuICAgICAgICogICAyIC0gQnJlYWsgYWxsIGJsb2NrcXVvdGVzLCBzbyB0aGF0IHRoZSBuZXcgcGFyYWdyYXBoIGlzIG5vdCBxdW90ZWQgKHRoaXMgaXMgdGhlIGRlZmF1bHQpXG4gICAgICAgKi9cbiAgICAgIFR5cGluZy5wcm90b3R5cGUuaW5zZXJ0UGFyYWdyYXBoID0gZnVuY3Rpb24gKGVkaXRhYmxlLCBybmcpIHtcbiAgICAgICAgICBybmcgPSBybmcgfHwgcmFuZ2UuY3JlYXRlKGVkaXRhYmxlKTtcbiAgICAgICAgICAvLyBkZWxldGVDb250ZW50cyBvbiByYW5nZS5cbiAgICAgICAgICBybmcgPSBybmcuZGVsZXRlQ29udGVudHMoKTtcbiAgICAgICAgICAvLyBXcmFwIHJhbmdlIGlmIGl0IG5lZWRzIHRvIGJlIHdyYXBwZWQgYnkgcGFyYWdyYXBoXG4gICAgICAgICAgcm5nID0gcm5nLndyYXBCb2R5SW5saW5lV2l0aFBhcmEoKTtcbiAgICAgICAgICAvLyBmaW5kaW5nIHBhcmFncmFwaFxuICAgICAgICAgIHZhciBzcGxpdFJvb3QgPSBkb20uYW5jZXN0b3Iocm5nLnNjLCBkb20uaXNQYXJhKTtcbiAgICAgICAgICB2YXIgbmV4dFBhcmE7XG4gICAgICAgICAgLy8gb24gcGFyYWdyYXBoOiBzcGxpdCBwYXJhZ3JhcGhcbiAgICAgICAgICBpZiAoc3BsaXRSb290KSB7XG4gICAgICAgICAgICAgIC8vIGlmIGl0IGlzIGFuIGVtcHR5IGxpbmUgd2l0aCBsaVxuICAgICAgICAgICAgICBpZiAoZG9tLmlzRW1wdHkoc3BsaXRSb290KSAmJiBkb20uaXNMaShzcGxpdFJvb3QpKSB7XG4gICAgICAgICAgICAgICAgICAvLyB0b29nbGUgVUwvT0wgYW5kIGVzY2FwZVxuICAgICAgICAgICAgICAgICAgdGhpcy5idWxsZXQudG9nZ2xlTGlzdChzcGxpdFJvb3QucGFyZW50Tm9kZS5ub2RlTmFtZSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICB2YXIgYmxvY2txdW90ZSA9IG51bGw7XG4gICAgICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmJsb2NrcXVvdGVCcmVha2luZ0xldmVsID09PSAxKSB7XG4gICAgICAgICAgICAgICAgICAgICAgYmxvY2txdW90ZSA9IGRvbS5hbmNlc3RvcihzcGxpdFJvb3QsIGRvbS5pc0Jsb2NrcXVvdGUpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5vcHRpb25zLmJsb2NrcXVvdGVCcmVha2luZ0xldmVsID09PSAyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgYmxvY2txdW90ZSA9IGRvbS5sYXN0QW5jZXN0b3Ioc3BsaXRSb290LCBkb20uaXNCbG9ja3F1b3RlKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGlmIChibG9ja3F1b3RlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gV2UncmUgaW5zaWRlIGEgYmxvY2txdW90ZSBhbmQgb3B0aW9ucyBhc2sgdXMgdG8gYnJlYWsgaXRcbiAgICAgICAgICAgICAgICAgICAgICBuZXh0UGFyYSA9ICQkMShkb20uZW1wdHlQYXJhKVswXTtcbiAgICAgICAgICAgICAgICAgICAgICAvLyBJZiB0aGUgc3BsaXQgaXMgcmlnaHQgYmVmb3JlIGEgPGJyPiwgcmVtb3ZlIGl0IHNvIHRoYXQgdGhlcmUncyBubyBcImVtcHR5IGxpbmVcIlxuICAgICAgICAgICAgICAgICAgICAgIC8vIGFmdGVyIHRoZSBzcGxpdCBpbiB0aGUgbmV3IGJsb2NrcXVvdGUgY3JlYXRlZFxuICAgICAgICAgICAgICAgICAgICAgIGlmIChkb20uaXNSaWdodEVkZ2VQb2ludChybmcuZ2V0U3RhcnRQb2ludCgpKSAmJiBkb20uaXNCUihybmcuc2MubmV4dFNpYmxpbmcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICQkMShybmcuc2MubmV4dFNpYmxpbmcpLnJlbW92ZSgpO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB2YXIgc3BsaXQgPSBkb20uc3BsaXRUcmVlKGJsb2NrcXVvdGUsIHJuZy5nZXRTdGFydFBvaW50KCksIHsgaXNEaXNjYXJkRW1wdHlTcGxpdHM6IHRydWUgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKHNwbGl0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHNwbGl0LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKG5leHRQYXJhLCBzcGxpdCk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBkb20uaW5zZXJ0QWZ0ZXIobmV4dFBhcmEsIGJsb2NrcXVvdGUpOyAvLyBUaGVyZSdzIG5vIHNwbGl0IGlmIHdlIHdlcmUgYXQgdGhlIGVuZCBvZiB0aGUgYmxvY2txdW90ZVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIG5leHRQYXJhID0gZG9tLnNwbGl0VHJlZShzcGxpdFJvb3QsIHJuZy5nZXRTdGFydFBvaW50KCkpO1xuICAgICAgICAgICAgICAgICAgICAgIC8vIG5vdCBhIGJsb2NrcXVvdGUsIGp1c3QgaW5zZXJ0IHRoZSBwYXJhZ3JhcGhcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgZW1wdHlBbmNob3JzID0gZG9tLmxpc3REZXNjZW5kYW50KHNwbGl0Um9vdCwgZG9tLmlzRW1wdHlBbmNob3IpO1xuICAgICAgICAgICAgICAgICAgICAgIGVtcHR5QW5jaG9ycyA9IGVtcHR5QW5jaG9ycy5jb25jYXQoZG9tLmxpc3REZXNjZW5kYW50KG5leHRQYXJhLCBkb20uaXNFbXB0eUFuY2hvcikpO1xuICAgICAgICAgICAgICAgICAgICAgICQkMS5lYWNoKGVtcHR5QW5jaG9ycywgZnVuY3Rpb24gKGlkeCwgYW5jaG9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGRvbS5yZW1vdmUoYW5jaG9yKTtcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAvLyByZXBsYWNlIGVtcHR5IGhlYWRpbmcsIHByZSBvciBjdXN0b20tbWFkZSBzdHlsZVRhZyB3aXRoIFAgdGFnXG4gICAgICAgICAgICAgICAgICAgICAgaWYgKChkb20uaXNIZWFkaW5nKG5leHRQYXJhKSB8fCBkb20uaXNQcmUobmV4dFBhcmEpIHx8IGRvbS5pc0N1c3RvbVN0eWxlVGFnKG5leHRQYXJhKSkgJiYgZG9tLmlzRW1wdHkobmV4dFBhcmEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIG5leHRQYXJhID0gZG9tLnJlcGxhY2UobmV4dFBhcmEsICdwJyk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC8vIG5vIHBhcmFncmFwaDogaW5zZXJ0IGVtcHR5IHBhcmFncmFwaFxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdmFyIG5leHQgPSBybmcuc2MuY2hpbGROb2Rlc1tybmcuc29dO1xuICAgICAgICAgICAgICBuZXh0UGFyYSA9ICQkMShkb20uZW1wdHlQYXJhKVswXTtcbiAgICAgICAgICAgICAgaWYgKG5leHQpIHtcbiAgICAgICAgICAgICAgICAgIHJuZy5zYy5pbnNlcnRCZWZvcmUobmV4dFBhcmEsIG5leHQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgcm5nLnNjLmFwcGVuZENoaWxkKG5leHRQYXJhKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByYW5nZS5jcmVhdGUobmV4dFBhcmEsIDApLm5vcm1hbGl6ZSgpLnNlbGVjdCgpLnNjcm9sbEludG9WaWV3KGVkaXRhYmxlKTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gVHlwaW5nO1xuICB9KCkpO1xuXG4gIC8qKlxuICAgKiBAY2xhc3MgQ3JlYXRlIGEgdmlydHVhbCB0YWJsZSB0byBjcmVhdGUgd2hhdCBhY3Rpb25zIHRvIGRvIGluIGNoYW5nZS5cbiAgICogQHBhcmFtIHtvYmplY3R9IHN0YXJ0UG9pbnQgQ2VsbCBzZWxlY3RlZCB0byBhcHBseSBjaGFuZ2UuXG4gICAqIEBwYXJhbSB7ZW51bX0gd2hlcmUgIFdoZXJlIGNoYW5nZSB3aWxsIGJlIGFwcGxpZWQgUm93IG9yIENvbC4gVXNlIGVudW06IFRhYmxlUmVzdWx0QWN0aW9uLndoZXJlXG4gICAqIEBwYXJhbSB7ZW51bX0gYWN0aW9uIEFjdGlvbiB0byBiZSBhcHBsaWVkLiBVc2UgZW51bTogVGFibGVSZXN1bHRBY3Rpb24ucmVxdWVzdEFjdGlvblxuICAgKiBAcGFyYW0ge29iamVjdH0gZG9tVGFibGUgRG9tIGVsZW1lbnQgb2YgdGFibGUgdG8gbWFrZSBjaGFuZ2VzLlxuICAgKi9cbiAgdmFyIFRhYmxlUmVzdWx0QWN0aW9uID0gZnVuY3Rpb24gKHN0YXJ0UG9pbnQsIHdoZXJlLCBhY3Rpb24sIGRvbVRhYmxlKSB7XG4gICAgICB2YXIgX3N0YXJ0UG9pbnQgPSB7ICdjb2xQb3MnOiAwLCAncm93UG9zJzogMCB9O1xuICAgICAgdmFyIF92aXJ0dWFsVGFibGUgPSBbXTtcbiAgICAgIHZhciBfYWN0aW9uQ2VsbExpc3QgPSBbXTtcbiAgICAgIC8vLyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgICAvLyBQcml2YXRlIGZ1bmN0aW9uc1xuICAgICAgLy8vIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAgIC8qKlxuICAgICAgICogU2V0IHRoZSBzdGFydFBvaW50IG9mIGFjdGlvbi5cbiAgICAgICAqL1xuICAgICAgZnVuY3Rpb24gc2V0U3RhcnRQb2ludCgpIHtcbiAgICAgICAgICBpZiAoIXN0YXJ0UG9pbnQgfHwgIXN0YXJ0UG9pbnQudGFnTmFtZSB8fCAoc3RhcnRQb2ludC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICd0ZCcgJiYgc3RhcnRQb2ludC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgIT09ICd0aCcpKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ltcG9zc2libGUgdG8gaWRlbnRpZnkgc3RhcnQgQ2VsbCBwb2ludC4nLCBzdGFydFBvaW50KTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBfc3RhcnRQb2ludC5jb2xQb3MgPSBzdGFydFBvaW50LmNlbGxJbmRleDtcbiAgICAgICAgICBpZiAoIXN0YXJ0UG9pbnQucGFyZW50RWxlbWVudCB8fCAhc3RhcnRQb2ludC5wYXJlbnRFbGVtZW50LnRhZ05hbWUgfHwgc3RhcnRQb2ludC5wYXJlbnRFbGVtZW50LnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPT0gJ3RyJykge1xuICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdJbXBvc3NpYmxlIHRvIGlkZW50aWZ5IHN0YXJ0IFJvdyBwb2ludC4nLCBzdGFydFBvaW50KTtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBfc3RhcnRQb2ludC5yb3dQb3MgPSBzdGFydFBvaW50LnBhcmVudEVsZW1lbnQucm93SW5kZXg7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIERlZmluZSB2aXJ0dWFsIHRhYmxlIHBvc2l0aW9uIGluZm8gb2JqZWN0LlxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7aW50fSByb3dJbmRleCBJbmRleCBwb3NpdGlvbiBpbiBsaW5lIG9mIHZpcnR1YWwgdGFibGUuXG4gICAgICAgKiBAcGFyYW0ge2ludH0gY2VsbEluZGV4IEluZGV4IHBvc2l0aW9uIGluIGNvbHVtbiBvZiB2aXJ0dWFsIHRhYmxlLlxuICAgICAgICogQHBhcmFtIHtvYmplY3R9IGJhc2VSb3cgUm93IGFmZmVjdGVkIGJ5IHRoaXMgcG9zaXRpb24uXG4gICAgICAgKiBAcGFyYW0ge29iamVjdH0gYmFzZUNlbGwgQ2VsbCBhZmZlY3RlZCBieSB0aGlzIHBvc2l0aW9uLlxuICAgICAgICogQHBhcmFtIHtib29sfSBpc1NwYW4gSW5mb3JtIGlmIGl0IGlzIGFuIHNwYW4gY2VsbC9yb3cuXG4gICAgICAgKi9cbiAgICAgIGZ1bmN0aW9uIHNldFZpcnR1YWxUYWJsZVBvc2l0aW9uKHJvd0luZGV4LCBjZWxsSW5kZXgsIGJhc2VSb3csIGJhc2VDZWxsLCBpc1Jvd1NwYW4sIGlzQ29sU3BhbiwgaXNWaXJ0dWFsQ2VsbCkge1xuICAgICAgICAgIHZhciBvYmpQb3NpdGlvbiA9IHtcbiAgICAgICAgICAgICAgJ2Jhc2VSb3cnOiBiYXNlUm93LFxuICAgICAgICAgICAgICAnYmFzZUNlbGwnOiBiYXNlQ2VsbCxcbiAgICAgICAgICAgICAgJ2lzUm93U3Bhbic6IGlzUm93U3BhbixcbiAgICAgICAgICAgICAgJ2lzQ29sU3Bhbic6IGlzQ29sU3BhbixcbiAgICAgICAgICAgICAgJ2lzVmlydHVhbCc6IGlzVmlydHVhbENlbGxcbiAgICAgICAgICB9O1xuICAgICAgICAgIGlmICghX3ZpcnR1YWxUYWJsZVtyb3dJbmRleF0pIHtcbiAgICAgICAgICAgICAgX3ZpcnR1YWxUYWJsZVtyb3dJbmRleF0gPSBbXTtcbiAgICAgICAgICB9XG4gICAgICAgICAgX3ZpcnR1YWxUYWJsZVtyb3dJbmRleF1bY2VsbEluZGV4XSA9IG9ialBvc2l0aW9uO1xuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBDcmVhdGUgYWN0aW9uIGNlbGwgb2JqZWN0LlxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSB2aXJ0dWFsVGFibGVDZWxsT2JqIE9iamVjdCBvZiBzcGVjaWZpYyBwb3NpdGlvbiBvbiB2aXJ0dWFsIHRhYmxlLlxuICAgICAgICogQHBhcmFtIHtlbnVtfSByZXN1bHRBY3Rpb24gQWN0aW9uIHRvIGJlIGFwcGxpZWQgaW4gdGhhdCBpdGVtLlxuICAgICAgICovXG4gICAgICBmdW5jdGlvbiBnZXRBY3Rpb25DZWxsKHZpcnR1YWxUYWJsZUNlbGxPYmosIHJlc3VsdEFjdGlvbiwgdmlydHVhbFJvd1Bvc2l0aW9uLCB2aXJ0dWFsQ29sUG9zaXRpb24pIHtcbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAnYmFzZUNlbGwnOiB2aXJ0dWFsVGFibGVDZWxsT2JqLmJhc2VDZWxsLFxuICAgICAgICAgICAgICAnYWN0aW9uJzogcmVzdWx0QWN0aW9uLFxuICAgICAgICAgICAgICAndmlydHVhbFRhYmxlJzoge1xuICAgICAgICAgICAgICAgICAgJ3Jvd0luZGV4JzogdmlydHVhbFJvd1Bvc2l0aW9uLFxuICAgICAgICAgICAgICAgICAgJ2NlbGxJbmRleCc6IHZpcnR1YWxDb2xQb3NpdGlvblxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmVjb3ZlciBmcmVlIGluZGV4IG9mIHJvdyB0byBhcHBlbmQgQ2VsbC5cbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge2ludH0gcm93SW5kZXggSW5kZXggb2Ygcm93IHRvIGZpbmQgZnJlZSBzcGFjZS5cbiAgICAgICAqIEBwYXJhbSB7aW50fSBjZWxsSW5kZXggSW5kZXggb2YgY2VsbCB0byBmaW5kIGZyZWUgc3BhY2UgaW4gdGFibGUuXG4gICAgICAgKi9cbiAgICAgIGZ1bmN0aW9uIHJlY292ZXJDZWxsSW5kZXgocm93SW5kZXgsIGNlbGxJbmRleCkge1xuICAgICAgICAgIGlmICghX3ZpcnR1YWxUYWJsZVtyb3dJbmRleF0pIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGNlbGxJbmRleDtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKCFfdmlydHVhbFRhYmxlW3Jvd0luZGV4XVtjZWxsSW5kZXhdKSB7XG4gICAgICAgICAgICAgIHJldHVybiBjZWxsSW5kZXg7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBuZXdDZWxsSW5kZXggPSBjZWxsSW5kZXg7XG4gICAgICAgICAgd2hpbGUgKF92aXJ0dWFsVGFibGVbcm93SW5kZXhdW25ld0NlbGxJbmRleF0pIHtcbiAgICAgICAgICAgICAgbmV3Q2VsbEluZGV4Kys7XG4gICAgICAgICAgICAgIGlmICghX3ZpcnR1YWxUYWJsZVtyb3dJbmRleF1bbmV3Q2VsbEluZGV4XSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ld0NlbGxJbmRleDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUmVjb3ZlciBpbmZvIGFib3V0IHJvdyBhbmQgY2VsbCBhbmQgYWRkIGluZm9ybWF0aW9uIHRvIHZpcnR1YWwgdGFibGUuXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtvYmplY3R9IHJvdyBSb3cgdG8gcmVjb3ZlciBpbmZvcm1hdGlvbi5cbiAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBjZWxsIENlbGwgdG8gcmVjb3ZlciBpbmZvcm1hdGlvbi5cbiAgICAgICAqL1xuICAgICAgZnVuY3Rpb24gYWRkQ2VsbEluZm9Ub1ZpcnR1YWwocm93LCBjZWxsKSB7XG4gICAgICAgICAgdmFyIGNlbGxJbmRleCA9IHJlY292ZXJDZWxsSW5kZXgocm93LnJvd0luZGV4LCBjZWxsLmNlbGxJbmRleCk7XG4gICAgICAgICAgdmFyIGNlbGxIYXNDb2xzcGFuID0gKGNlbGwuY29sU3BhbiA+IDEpO1xuICAgICAgICAgIHZhciBjZWxsSGFzUm93c3BhbiA9IChjZWxsLnJvd1NwYW4gPiAxKTtcbiAgICAgICAgICB2YXIgaXNUaGlzU2VsZWN0ZWRDZWxsID0gKHJvdy5yb3dJbmRleCA9PT0gX3N0YXJ0UG9pbnQucm93UG9zICYmIGNlbGwuY2VsbEluZGV4ID09PSBfc3RhcnRQb2ludC5jb2xQb3MpO1xuICAgICAgICAgIHNldFZpcnR1YWxUYWJsZVBvc2l0aW9uKHJvdy5yb3dJbmRleCwgY2VsbEluZGV4LCByb3csIGNlbGwsIGNlbGxIYXNSb3dzcGFuLCBjZWxsSGFzQ29sc3BhbiwgZmFsc2UpO1xuICAgICAgICAgIC8vIEFkZCBzcGFuIHJvd3MgdG8gdmlydHVhbCBUYWJsZS5cbiAgICAgICAgICB2YXIgcm93c3Bhbk51bWJlciA9IGNlbGwuYXR0cmlidXRlcy5yb3dTcGFuID8gcGFyc2VJbnQoY2VsbC5hdHRyaWJ1dGVzLnJvd1NwYW4udmFsdWUsIDEwKSA6IDA7XG4gICAgICAgICAgaWYgKHJvd3NwYW5OdW1iZXIgPiAxKSB7XG4gICAgICAgICAgICAgIGZvciAodmFyIHJwID0gMTsgcnAgPCByb3dzcGFuTnVtYmVyOyBycCsrKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgcm93c3BhbkluZGV4ID0gcm93LnJvd0luZGV4ICsgcnA7XG4gICAgICAgICAgICAgICAgICBhZGp1c3RTdGFydFBvaW50KHJvd3NwYW5JbmRleCwgY2VsbEluZGV4LCBjZWxsLCBpc1RoaXNTZWxlY3RlZENlbGwpO1xuICAgICAgICAgICAgICAgICAgc2V0VmlydHVhbFRhYmxlUG9zaXRpb24ocm93c3BhbkluZGV4LCBjZWxsSW5kZXgsIHJvdywgY2VsbCwgdHJ1ZSwgY2VsbEhhc0NvbHNwYW4sIHRydWUpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIC8vIEFkZCBzcGFuIGNvbHMgdG8gdmlydHVhbCB0YWJsZS5cbiAgICAgICAgICB2YXIgY29sc3Bhbk51bWJlciA9IGNlbGwuYXR0cmlidXRlcy5jb2xTcGFuID8gcGFyc2VJbnQoY2VsbC5hdHRyaWJ1dGVzLmNvbFNwYW4udmFsdWUsIDEwKSA6IDA7XG4gICAgICAgICAgaWYgKGNvbHNwYW5OdW1iZXIgPiAxKSB7XG4gICAgICAgICAgICAgIGZvciAodmFyIGNwID0gMTsgY3AgPCBjb2xzcGFuTnVtYmVyOyBjcCsrKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgY2VsbHNwYW5JbmRleCA9IHJlY292ZXJDZWxsSW5kZXgocm93LnJvd0luZGV4LCAoY2VsbEluZGV4ICsgY3ApKTtcbiAgICAgICAgICAgICAgICAgIGFkanVzdFN0YXJ0UG9pbnQocm93LnJvd0luZGV4LCBjZWxsc3BhbkluZGV4LCBjZWxsLCBpc1RoaXNTZWxlY3RlZENlbGwpO1xuICAgICAgICAgICAgICAgICAgc2V0VmlydHVhbFRhYmxlUG9zaXRpb24ocm93LnJvd0luZGV4LCBjZWxsc3BhbkluZGV4LCByb3csIGNlbGwsIGNlbGxIYXNSb3dzcGFuLCB0cnVlLCB0cnVlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogUHJvY2VzcyB2YWxpZGF0aW9uIGFuZCBhZGp1c3Qgb2Ygc3RhcnQgcG9pbnQgaWYgbmVlZGVkXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtpbnR9IHJvd0luZGV4XG4gICAgICAgKiBAcGFyYW0ge2ludH0gY2VsbEluZGV4XG4gICAgICAgKiBAcGFyYW0ge29iamVjdH0gY2VsbFxuICAgICAgICogQHBhcmFtIHtib29sfSBpc1NlbGVjdGVkQ2VsbFxuICAgICAgICovXG4gICAgICBmdW5jdGlvbiBhZGp1c3RTdGFydFBvaW50KHJvd0luZGV4LCBjZWxsSW5kZXgsIGNlbGwsIGlzU2VsZWN0ZWRDZWxsKSB7XG4gICAgICAgICAgaWYgKHJvd0luZGV4ID09PSBfc3RhcnRQb2ludC5yb3dQb3MgJiYgX3N0YXJ0UG9pbnQuY29sUG9zID49IGNlbGwuY2VsbEluZGV4ICYmIGNlbGwuY2VsbEluZGV4IDw9IGNlbGxJbmRleCAmJiAhaXNTZWxlY3RlZENlbGwpIHtcbiAgICAgICAgICAgICAgX3N0YXJ0UG9pbnQuY29sUG9zKys7XG4gICAgICAgICAgfVxuICAgICAgfVxuICAgICAgLyoqXG4gICAgICAgKiBDcmVhdGUgdmlydHVhbCB0YWJsZSBvZiBjZWxscyB3aXRoIGFsbCBjZWxscywgaW5jbHVkaW5nIHNwYW4gY2VsbHMuXG4gICAgICAgKi9cbiAgICAgIGZ1bmN0aW9uIGNyZWF0ZVZpcnR1YWxUYWJsZSgpIHtcbiAgICAgICAgICB2YXIgcm93cyA9IGRvbVRhYmxlLnJvd3M7XG4gICAgICAgICAgZm9yICh2YXIgcm93SW5kZXggPSAwOyByb3dJbmRleCA8IHJvd3MubGVuZ3RoOyByb3dJbmRleCsrKSB7XG4gICAgICAgICAgICAgIHZhciBjZWxscyA9IHJvd3Nbcm93SW5kZXhdLmNlbGxzO1xuICAgICAgICAgICAgICBmb3IgKHZhciBjZWxsSW5kZXggPSAwOyBjZWxsSW5kZXggPCBjZWxscy5sZW5ndGg7IGNlbGxJbmRleCsrKSB7XG4gICAgICAgICAgICAgICAgICBhZGRDZWxsSW5mb1RvVmlydHVhbChyb3dzW3Jvd0luZGV4XSwgY2VsbHNbY2VsbEluZGV4XSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIEdldCBhY3Rpb24gdG8gYmUgYXBwbGllZCBvbiB0aGUgY2VsbC5cbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge29iamVjdH0gY2VsbCB2aXJ0dWFsIHRhYmxlIGNlbGwgdG8gYXBwbHkgYWN0aW9uXG4gICAgICAgKi9cbiAgICAgIGZ1bmN0aW9uIGdldERlbGV0ZVJlc3VsdEFjdGlvblRvQ2VsbChjZWxsKSB7XG4gICAgICAgICAgc3dpdGNoICh3aGVyZSkge1xuICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLndoZXJlLkNvbHVtbjpcbiAgICAgICAgICAgICAgICAgIGlmIChjZWxsLmlzQ29sU3Bhbikge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24uU3VidHJhY3RTcGFuQ291bnQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgY2FzZSBUYWJsZVJlc3VsdEFjdGlvbi53aGVyZS5Sb3c6XG4gICAgICAgICAgICAgICAgICBpZiAoIWNlbGwuaXNWaXJ0dWFsICYmIGNlbGwuaXNSb3dTcGFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5BZGRDZWxsO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoY2VsbC5pc1Jvd1NwYW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gVGFibGVSZXN1bHRBY3Rpb24ucmVzdWx0QWN0aW9uLlN1YnRyYWN0U3BhbkNvdW50O1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24uUmVtb3ZlQ2VsbDtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogR2V0IGFjdGlvbiB0byBiZSBhcHBsaWVkIG9uIHRoZSBjZWxsLlxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBjZWxsIHZpcnR1YWwgdGFibGUgY2VsbCB0byBhcHBseSBhY3Rpb25cbiAgICAgICAqL1xuICAgICAgZnVuY3Rpb24gZ2V0QWRkUmVzdWx0QWN0aW9uVG9DZWxsKGNlbGwpIHtcbiAgICAgICAgICBzd2l0Y2ggKHdoZXJlKSB7XG4gICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUuQ29sdW1uOlxuICAgICAgICAgICAgICAgICAgaWYgKGNlbGwuaXNDb2xTcGFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5TdW1TcGFuQ291bnQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBlbHNlIGlmIChjZWxsLmlzUm93U3BhbiAmJiBjZWxsLmlzVmlydHVhbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24uSWdub3JlO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUuUm93OlxuICAgICAgICAgICAgICAgICAgaWYgKGNlbGwuaXNSb3dTcGFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5TdW1TcGFuQ291bnQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBlbHNlIGlmIChjZWxsLmlzQ29sU3BhbiAmJiBjZWxsLmlzVmlydHVhbCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24uSWdub3JlO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24uQWRkQ2VsbDtcbiAgICAgIH1cbiAgICAgIGZ1bmN0aW9uIGluaXQoKSB7XG4gICAgICAgICAgc2V0U3RhcnRQb2ludCgpO1xuICAgICAgICAgIGNyZWF0ZVZpcnR1YWxUYWJsZSgpO1xuICAgICAgfVxuICAgICAgLy8vIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9cbiAgICAgIC8vIFB1YmxpYyBmdW5jdGlvbnNcbiAgICAgIC8vLyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgICAvKipcbiAgICAgICAqIFJlY292ZXIgYXJyYXkgb3Mgd2hhdCB0byBkbyBpbiB0YWJsZS5cbiAgICAgICAqL1xuICAgICAgdGhpcy5nZXRBY3Rpb25MaXN0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBmaXhlZFJvdyA9ICh3aGVyZSA9PT0gVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUuUm93KSA/IF9zdGFydFBvaW50LnJvd1BvcyA6IC0xO1xuICAgICAgICAgIHZhciBmaXhlZENvbCA9ICh3aGVyZSA9PT0gVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUuQ29sdW1uKSA/IF9zdGFydFBvaW50LmNvbFBvcyA6IC0xO1xuICAgICAgICAgIHZhciBhY3R1YWxQb3NpdGlvbiA9IDA7XG4gICAgICAgICAgdmFyIGNhbkNvbnRpbnVlID0gdHJ1ZTtcbiAgICAgICAgICB3aGlsZSAoY2FuQ29udGludWUpIHtcbiAgICAgICAgICAgICAgdmFyIHJvd1Bvc2l0aW9uID0gKGZpeGVkUm93ID49IDApID8gZml4ZWRSb3cgOiBhY3R1YWxQb3NpdGlvbjtcbiAgICAgICAgICAgICAgdmFyIGNvbFBvc2l0aW9uID0gKGZpeGVkQ29sID49IDApID8gZml4ZWRDb2wgOiBhY3R1YWxQb3NpdGlvbjtcbiAgICAgICAgICAgICAgdmFyIHJvdyA9IF92aXJ0dWFsVGFibGVbcm93UG9zaXRpb25dO1xuICAgICAgICAgICAgICBpZiAoIXJvdykge1xuICAgICAgICAgICAgICAgICAgY2FuQ29udGludWUgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBfYWN0aW9uQ2VsbExpc3Q7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdmFyIGNlbGwgPSByb3dbY29sUG9zaXRpb25dO1xuICAgICAgICAgICAgICBpZiAoIWNlbGwpIHtcbiAgICAgICAgICAgICAgICAgIGNhbkNvbnRpbnVlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gX2FjdGlvbkNlbGxMaXN0O1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC8vIERlZmluZSBhY3Rpb24gdG8gYmUgYXBwbGllZCBpbiB0aGlzIGNlbGxcbiAgICAgICAgICAgICAgdmFyIHJlc3VsdEFjdGlvbiA9IFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5JZ25vcmU7XG4gICAgICAgICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlcXVlc3RBY3Rpb24uQWRkOlxuICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdEFjdGlvbiA9IGdldEFkZFJlc3VsdEFjdGlvblRvQ2VsbChjZWxsKTtcbiAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ucmVxdWVzdEFjdGlvbi5EZWxldGU6XG4gICAgICAgICAgICAgICAgICAgICAgcmVzdWx0QWN0aW9uID0gZ2V0RGVsZXRlUmVzdWx0QWN0aW9uVG9DZWxsKGNlbGwpO1xuICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIF9hY3Rpb25DZWxsTGlzdC5wdXNoKGdldEFjdGlvbkNlbGwoY2VsbCwgcmVzdWx0QWN0aW9uLCByb3dQb3NpdGlvbiwgY29sUG9zaXRpb24pKTtcbiAgICAgICAgICAgICAgYWN0dWFsUG9zaXRpb24rKztcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIF9hY3Rpb25DZWxsTGlzdDtcbiAgICAgIH07XG4gICAgICBpbml0KCk7XG4gIH07XG4gIC8qKlxuICAqXG4gICogV2hlcmUgYWN0aW9uIG9jY291cnMgZW51bS5cbiAgKi9cbiAgVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUgPSB7ICdSb3cnOiAwLCAnQ29sdW1uJzogMSB9O1xuICAvKipcbiAgKlxuICAqIFJlcXVlc3RlZCBhY3Rpb24gdG8gYXBwbHkgZW51bS5cbiAgKi9cbiAgVGFibGVSZXN1bHRBY3Rpb24ucmVxdWVzdEFjdGlvbiA9IHsgJ0FkZCc6IDAsICdEZWxldGUnOiAxIH07XG4gIC8qKlxuICAqXG4gICogUmVzdWx0IGFjdGlvbiB0byBiZSBleGVjdXRlZCBlbnVtLlxuICAqL1xuICBUYWJsZVJlc3VsdEFjdGlvbi5yZXN1bHRBY3Rpb24gPSB7ICdJZ25vcmUnOiAwLCAnU3VidHJhY3RTcGFuQ291bnQnOiAxLCAnUmVtb3ZlQ2VsbCc6IDIsICdBZGRDZWxsJzogMywgJ1N1bVNwYW5Db3VudCc6IDQgfTtcbiAgLyoqXG4gICAqXG4gICAqIEBjbGFzcyBlZGl0aW5nLlRhYmxlXG4gICAqXG4gICAqIFRhYmxlXG4gICAqXG4gICAqL1xuICB2YXIgVGFibGUgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBUYWJsZSgpIHtcbiAgICAgIH1cbiAgICAgIC8qKlxuICAgICAgICogaGFuZGxlIHRhYiBrZXlcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nXG4gICAgICAgKiBAcGFyYW0ge0Jvb2xlYW59IGlzU2hpZnRcbiAgICAgICAqL1xuICAgICAgVGFibGUucHJvdG90eXBlLnRhYiA9IGZ1bmN0aW9uIChybmcsIGlzU2hpZnQpIHtcbiAgICAgICAgICB2YXIgY2VsbCA9IGRvbS5hbmNlc3RvcihybmcuY29tbW9uQW5jZXN0b3IoKSwgZG9tLmlzQ2VsbCk7XG4gICAgICAgICAgdmFyIHRhYmxlID0gZG9tLmFuY2VzdG9yKGNlbGwsIGRvbS5pc1RhYmxlKTtcbiAgICAgICAgICB2YXIgY2VsbHMgPSBkb20ubGlzdERlc2NlbmRhbnQodGFibGUsIGRvbS5pc0NlbGwpO1xuICAgICAgICAgIHZhciBuZXh0Q2VsbCA9IGxpc3RzW2lzU2hpZnQgPyAncHJldicgOiAnbmV4dCddKGNlbGxzLCBjZWxsKTtcbiAgICAgICAgICBpZiAobmV4dENlbGwpIHtcbiAgICAgICAgICAgICAgcmFuZ2UuY3JlYXRlKG5leHRDZWxsLCAwKS5zZWxlY3QoKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBBZGQgYSBuZXcgcm93XG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtXcmFwcGVkUmFuZ2V9IHJuZ1xuICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHBvc2l0aW9uICh0b3AvYm90dG9tKVxuICAgICAgICogQHJldHVybiB7Tm9kZX1cbiAgICAgICAqL1xuICAgICAgVGFibGUucHJvdG90eXBlLmFkZFJvdyA9IGZ1bmN0aW9uIChybmcsIHBvc2l0aW9uKSB7XG4gICAgICAgICAgdmFyIGNlbGwgPSBkb20uYW5jZXN0b3Iocm5nLmNvbW1vbkFuY2VzdG9yKCksIGRvbS5pc0NlbGwpO1xuICAgICAgICAgIHZhciBjdXJyZW50VHIgPSAkJDEoY2VsbCkuY2xvc2VzdCgndHInKTtcbiAgICAgICAgICB2YXIgdHJBdHRyaWJ1dGVzID0gdGhpcy5yZWNvdmVyQXR0cmlidXRlcyhjdXJyZW50VHIpO1xuICAgICAgICAgIHZhciBodG1sID0gJCQxKCc8dHInICsgdHJBdHRyaWJ1dGVzICsgJz48L3RyPicpO1xuICAgICAgICAgIHZhciB2VGFibGUgPSBuZXcgVGFibGVSZXN1bHRBY3Rpb24oY2VsbCwgVGFibGVSZXN1bHRBY3Rpb24ud2hlcmUuUm93LCBUYWJsZVJlc3VsdEFjdGlvbi5yZXF1ZXN0QWN0aW9uLkFkZCwgJCQxKGN1cnJlbnRUcikuY2xvc2VzdCgndGFibGUnKVswXSk7XG4gICAgICAgICAgdmFyIGFjdGlvbnMgPSB2VGFibGUuZ2V0QWN0aW9uTGlzdCgpO1xuICAgICAgICAgIGZvciAodmFyIGlkQ2VsbCA9IDA7IGlkQ2VsbCA8IGFjdGlvbnMubGVuZ3RoOyBpZENlbGwrKykge1xuICAgICAgICAgICAgICB2YXIgY3VycmVudENlbGwgPSBhY3Rpb25zW2lkQ2VsbF07XG4gICAgICAgICAgICAgIHZhciB0ZEF0dHJpYnV0ZXMgPSB0aGlzLnJlY292ZXJBdHRyaWJ1dGVzKGN1cnJlbnRDZWxsLmJhc2VDZWxsKTtcbiAgICAgICAgICAgICAgc3dpdGNoIChjdXJyZW50Q2VsbC5hY3Rpb24pIHtcbiAgICAgICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ucmVzdWx0QWN0aW9uLkFkZENlbGw6XG4gICAgICAgICAgICAgICAgICAgICAgaHRtbC5hcHBlbmQoJzx0ZCcgKyB0ZEF0dHJpYnV0ZXMgKyAnPicgKyBkb20uYmxhbmsgKyAnPC90ZD4nKTtcbiAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ucmVzdWx0QWN0aW9uLlN1bVNwYW5Db3VudDpcbiAgICAgICAgICAgICAgICAgICAgICBpZiAocG9zaXRpb24gPT09ICd0b3AnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBiYXNlQ2VsbFRyID0gY3VycmVudENlbGwuYmFzZUNlbGwucGFyZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgaXNUb3BGcm9tUm93U3BhbiA9ICghYmFzZUNlbGxUciA/IDAgOiBjdXJyZW50Q2VsbC5iYXNlQ2VsbC5jbG9zZXN0KCd0cicpLnJvd0luZGV4KSA8PSBjdXJyZW50VHJbMF0ucm93SW5kZXg7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc1RvcEZyb21Sb3dTcGFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgbmV3VGQgPSAkJDEoJzxkaXY+PC9kaXY+JykuYXBwZW5kKCQkMSgnPHRkJyArIHRkQXR0cmlidXRlcyArICc+JyArIGRvbS5ibGFuayArICc8L3RkPicpLnJlbW92ZUF0dHIoJ3Jvd3NwYW4nKSkuaHRtbCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaHRtbC5hcHBlbmQobmV3VGQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgdmFyIHJvd3NwYW5OdW1iZXIgPSBwYXJzZUludChjdXJyZW50Q2VsbC5iYXNlQ2VsbC5yb3dTcGFuLCAxMCk7XG4gICAgICAgICAgICAgICAgICAgICAgcm93c3Bhbk51bWJlcisrO1xuICAgICAgICAgICAgICAgICAgICAgIGN1cnJlbnRDZWxsLmJhc2VDZWxsLnNldEF0dHJpYnV0ZSgncm93U3BhbicsIHJvd3NwYW5OdW1iZXIpO1xuICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChwb3NpdGlvbiA9PT0gJ3RvcCcpIHtcbiAgICAgICAgICAgICAgY3VycmVudFRyLmJlZm9yZShodG1sKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHZhciBjZWxsSGFzUm93c3BhbiA9IChjZWxsLnJvd1NwYW4gPiAxKTtcbiAgICAgICAgICAgICAgaWYgKGNlbGxIYXNSb3dzcGFuKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgbGFzdFRySW5kZXggPSBjdXJyZW50VHJbMF0ucm93SW5kZXggKyAoY2VsbC5yb3dTcGFuIC0gMik7XG4gICAgICAgICAgICAgICAgICAkJDEoJCQxKGN1cnJlbnRUcikucGFyZW50KCkuZmluZCgndHInKVtsYXN0VHJJbmRleF0pLmFmdGVyKCQkMShodG1sKSk7XG4gICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY3VycmVudFRyLmFmdGVyKGh0bWwpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIEFkZCBhIG5ldyBjb2xcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nXG4gICAgICAgKiBAcGFyYW0ge1N0cmluZ30gcG9zaXRpb24gKGxlZnQvcmlnaHQpXG4gICAgICAgKiBAcmV0dXJuIHtOb2RlfVxuICAgICAgICovXG4gICAgICBUYWJsZS5wcm90b3R5cGUuYWRkQ29sID0gZnVuY3Rpb24gKHJuZywgcG9zaXRpb24pIHtcbiAgICAgICAgICB2YXIgY2VsbCA9IGRvbS5hbmNlc3RvcihybmcuY29tbW9uQW5jZXN0b3IoKSwgZG9tLmlzQ2VsbCk7XG4gICAgICAgICAgdmFyIHJvdyA9ICQkMShjZWxsKS5jbG9zZXN0KCd0cicpO1xuICAgICAgICAgIHZhciByb3dzR3JvdXAgPSAkJDEocm93KS5zaWJsaW5ncygpO1xuICAgICAgICAgIHJvd3NHcm91cC5wdXNoKHJvdyk7XG4gICAgICAgICAgdmFyIHZUYWJsZSA9IG5ldyBUYWJsZVJlc3VsdEFjdGlvbihjZWxsLCBUYWJsZVJlc3VsdEFjdGlvbi53aGVyZS5Db2x1bW4sIFRhYmxlUmVzdWx0QWN0aW9uLnJlcXVlc3RBY3Rpb24uQWRkLCAkJDEocm93KS5jbG9zZXN0KCd0YWJsZScpWzBdKTtcbiAgICAgICAgICB2YXIgYWN0aW9ucyA9IHZUYWJsZS5nZXRBY3Rpb25MaXN0KCk7XG4gICAgICAgICAgZm9yICh2YXIgYWN0aW9uSW5kZXggPSAwOyBhY3Rpb25JbmRleCA8IGFjdGlvbnMubGVuZ3RoOyBhY3Rpb25JbmRleCsrKSB7XG4gICAgICAgICAgICAgIHZhciBjdXJyZW50Q2VsbCA9IGFjdGlvbnNbYWN0aW9uSW5kZXhdO1xuICAgICAgICAgICAgICB2YXIgdGRBdHRyaWJ1dGVzID0gdGhpcy5yZWNvdmVyQXR0cmlidXRlcyhjdXJyZW50Q2VsbC5iYXNlQ2VsbCk7XG4gICAgICAgICAgICAgIHN3aXRjaCAoY3VycmVudENlbGwuYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5BZGRDZWxsOlxuICAgICAgICAgICAgICAgICAgICAgIGlmIChwb3NpdGlvbiA9PT0gJ3JpZ2h0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAkJDEoY3VycmVudENlbGwuYmFzZUNlbGwpLmFmdGVyKCc8dGQnICsgdGRBdHRyaWJ1dGVzICsgJz4nICsgZG9tLmJsYW5rICsgJzwvdGQ+Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAkJDEoY3VycmVudENlbGwuYmFzZUNlbGwpLmJlZm9yZSgnPHRkJyArIHRkQXR0cmlidXRlcyArICc+JyArIGRvbS5ibGFuayArICc8L3RkPicpO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ucmVzdWx0QWN0aW9uLlN1bVNwYW5Db3VudDpcbiAgICAgICAgICAgICAgICAgICAgICBpZiAocG9zaXRpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNvbHNwYW5OdW1iZXIgPSBwYXJzZUludChjdXJyZW50Q2VsbC5iYXNlQ2VsbC5jb2xTcGFuLCAxMCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW5OdW1iZXIrKztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudENlbGwuYmFzZUNlbGwuc2V0QXR0cmlidXRlKCdjb2xTcGFuJywgY29sc3Bhbk51bWJlcik7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAkJDEoY3VycmVudENlbGwuYmFzZUNlbGwpLmJlZm9yZSgnPHRkJyArIHRkQXR0cmlidXRlcyArICc+JyArIGRvbS5ibGFuayArICc8L3RkPicpO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKlxuICAgICAgKiBDb3B5IGF0dHJpYnV0ZXMgZnJvbSBlbGVtZW50LlxuICAgICAgKlxuICAgICAgKiBAcGFyYW0ge29iamVjdH0gRWxlbWVudCB0byByZWNvdmVyIGF0dHJpYnV0ZXMuXG4gICAgICAqIEByZXR1cm4ge3N0cmluZ30gQ29waWVkIHN0cmluZyBlbGVtZW50cy5cbiAgICAgICovXG4gICAgICBUYWJsZS5wcm90b3R5cGUucmVjb3ZlckF0dHJpYnV0ZXMgPSBmdW5jdGlvbiAoZWwpIHtcbiAgICAgICAgICB2YXIgcmVzdWx0U3RyID0gJyc7XG4gICAgICAgICAgaWYgKCFlbCkge1xuICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0U3RyO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgYXR0ckxpc3QgPSBlbC5hdHRyaWJ1dGVzIHx8IFtdO1xuICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXR0ckxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgaWYgKGF0dHJMaXN0W2ldLm5hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ2lkJykge1xuICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKGF0dHJMaXN0W2ldLnNwZWNpZmllZCkge1xuICAgICAgICAgICAgICAgICAgcmVzdWx0U3RyICs9ICcgJyArIGF0dHJMaXN0W2ldLm5hbWUgKyAnPVxcJycgKyBhdHRyTGlzdFtpXS52YWx1ZSArICdcXCcnO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiByZXN1bHRTdHI7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBEZWxldGUgY3VycmVudCByb3dcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nXG4gICAgICAgKiBAcmV0dXJuIHtOb2RlfVxuICAgICAgICovXG4gICAgICBUYWJsZS5wcm90b3R5cGUuZGVsZXRlUm93ID0gZnVuY3Rpb24gKHJuZykge1xuICAgICAgICAgIHZhciBjZWxsID0gZG9tLmFuY2VzdG9yKHJuZy5jb21tb25BbmNlc3RvcigpLCBkb20uaXNDZWxsKTtcbiAgICAgICAgICB2YXIgcm93ID0gJCQxKGNlbGwpLmNsb3Nlc3QoJ3RyJyk7XG4gICAgICAgICAgdmFyIGNlbGxQb3MgPSByb3cuY2hpbGRyZW4oJ3RkLCB0aCcpLmluZGV4KCQkMShjZWxsKSk7XG4gICAgICAgICAgdmFyIHJvd1BvcyA9IHJvd1swXS5yb3dJbmRleDtcbiAgICAgICAgICB2YXIgdlRhYmxlID0gbmV3IFRhYmxlUmVzdWx0QWN0aW9uKGNlbGwsIFRhYmxlUmVzdWx0QWN0aW9uLndoZXJlLlJvdywgVGFibGVSZXN1bHRBY3Rpb24ucmVxdWVzdEFjdGlvbi5EZWxldGUsICQkMShyb3cpLmNsb3Nlc3QoJ3RhYmxlJylbMF0pO1xuICAgICAgICAgIHZhciBhY3Rpb25zID0gdlRhYmxlLmdldEFjdGlvbkxpc3QoKTtcbiAgICAgICAgICBmb3IgKHZhciBhY3Rpb25JbmRleCA9IDA7IGFjdGlvbkluZGV4IDwgYWN0aW9ucy5sZW5ndGg7IGFjdGlvbkluZGV4KyspIHtcbiAgICAgICAgICAgICAgaWYgKCFhY3Rpb25zW2FjdGlvbkluZGV4XSkge1xuICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdmFyIGJhc2VDZWxsID0gYWN0aW9uc1thY3Rpb25JbmRleF0uYmFzZUNlbGw7XG4gICAgICAgICAgICAgIHZhciB2aXJ0dWFsUG9zaXRpb24gPSBhY3Rpb25zW2FjdGlvbkluZGV4XS52aXJ0dWFsVGFibGU7XG4gICAgICAgICAgICAgIHZhciBoYXNSb3dzcGFuID0gKGJhc2VDZWxsLnJvd1NwYW4gJiYgYmFzZUNlbGwucm93U3BhbiA+IDEpO1xuICAgICAgICAgICAgICB2YXIgcm93c3Bhbk51bWJlciA9IChoYXNSb3dzcGFuKSA/IHBhcnNlSW50KGJhc2VDZWxsLnJvd1NwYW4sIDEwKSA6IDA7XG4gICAgICAgICAgICAgIHN3aXRjaCAoYWN0aW9uc1thY3Rpb25JbmRleF0uYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5JZ25vcmU6XG4gICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5BZGRDZWxsOlxuICAgICAgICAgICAgICAgICAgICAgIHZhciBuZXh0Um93ID0gcm93Lm5leHQoJ3RyJylbMF07XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKCFuZXh0Um93KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB2YXIgY2xvbmVSb3cgPSByb3dbMF0uY2VsbHNbY2VsbFBvc107XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKGhhc1Jvd3NwYW4pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJvd3NwYW5OdW1iZXIgPiAyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3dzcGFuTnVtYmVyLS07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0Um93Lmluc2VydEJlZm9yZShjbG9uZVJvdywgbmV4dFJvdy5jZWxsc1tjZWxsUG9zXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0Um93LmNlbGxzW2NlbGxQb3NdLnNldEF0dHJpYnV0ZSgncm93U3BhbicsIHJvd3NwYW5OdW1iZXIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dFJvdy5jZWxsc1tjZWxsUG9zXS5pbm5lckhUTUwgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChyb3dzcGFuTnVtYmVyID09PSAyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0Um93Lmluc2VydEJlZm9yZShjbG9uZVJvdywgbmV4dFJvdy5jZWxsc1tjZWxsUG9zXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0Um93LmNlbGxzW2NlbGxQb3NdLnJlbW92ZUF0dHJpYnV0ZSgncm93U3BhbicpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dFJvdy5jZWxsc1tjZWxsUG9zXS5pbm5lckhUTUwgPSAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICAgIGNhc2UgVGFibGVSZXN1bHRBY3Rpb24ucmVzdWx0QWN0aW9uLlN1YnRyYWN0U3BhbkNvdW50OlxuICAgICAgICAgICAgICAgICAgICAgIGlmIChoYXNSb3dzcGFuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyb3dzcGFuTnVtYmVyID4gMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93c3Bhbk51bWJlci0tO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuc2V0QXR0cmlidXRlKCdyb3dTcGFuJywgcm93c3Bhbk51bWJlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmlydHVhbFBvc2l0aW9uLnJvd0luZGV4ICE9PSByb3dQb3MgJiYgYmFzZUNlbGwuY2VsbEluZGV4ID09PSBjZWxsUG9zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAocm93c3Bhbk51bWJlciA9PT0gMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwucmVtb3ZlQXR0cmlidXRlKCdyb3dTcGFuJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodmlydHVhbFBvc2l0aW9uLnJvd0luZGV4ICE9PSByb3dQb3MgJiYgYmFzZUNlbGwuY2VsbEluZGV4ID09PSBjZWxsUG9zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5SZW1vdmVDZWxsOlxuICAgICAgICAgICAgICAgICAgICAgIC8vIERvIG5vdCBuZWVkIHJlbW92ZSBjZWxsIGJlY2F1c2Ugcm93IHdpbGwgYmUgZGVsZXRlZC5cbiAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByb3cucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBEZWxldGUgY3VycmVudCBjb2xcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge1dyYXBwZWRSYW5nZX0gcm5nXG4gICAgICAgKiBAcmV0dXJuIHtOb2RlfVxuICAgICAgICovXG4gICAgICBUYWJsZS5wcm90b3R5cGUuZGVsZXRlQ29sID0gZnVuY3Rpb24gKHJuZykge1xuICAgICAgICAgIHZhciBjZWxsID0gZG9tLmFuY2VzdG9yKHJuZy5jb21tb25BbmNlc3RvcigpLCBkb20uaXNDZWxsKTtcbiAgICAgICAgICB2YXIgcm93ID0gJCQxKGNlbGwpLmNsb3Nlc3QoJ3RyJyk7XG4gICAgICAgICAgdmFyIGNlbGxQb3MgPSByb3cuY2hpbGRyZW4oJ3RkLCB0aCcpLmluZGV4KCQkMShjZWxsKSk7XG4gICAgICAgICAgdmFyIHZUYWJsZSA9IG5ldyBUYWJsZVJlc3VsdEFjdGlvbihjZWxsLCBUYWJsZVJlc3VsdEFjdGlvbi53aGVyZS5Db2x1bW4sIFRhYmxlUmVzdWx0QWN0aW9uLnJlcXVlc3RBY3Rpb24uRGVsZXRlLCAkJDEocm93KS5jbG9zZXN0KCd0YWJsZScpWzBdKTtcbiAgICAgICAgICB2YXIgYWN0aW9ucyA9IHZUYWJsZS5nZXRBY3Rpb25MaXN0KCk7XG4gICAgICAgICAgZm9yICh2YXIgYWN0aW9uSW5kZXggPSAwOyBhY3Rpb25JbmRleCA8IGFjdGlvbnMubGVuZ3RoOyBhY3Rpb25JbmRleCsrKSB7XG4gICAgICAgICAgICAgIGlmICghYWN0aW9uc1thY3Rpb25JbmRleF0pIHtcbiAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHN3aXRjaCAoYWN0aW9uc1thY3Rpb25JbmRleF0uYWN0aW9uKSB7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5JZ25vcmU6XG4gICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5TdWJ0cmFjdFNwYW5Db3VudDpcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgYmFzZUNlbGwgPSBhY3Rpb25zW2FjdGlvbkluZGV4XS5iYXNlQ2VsbDtcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgaGFzQ29sc3BhbiA9IChiYXNlQ2VsbC5jb2xTcGFuICYmIGJhc2VDZWxsLmNvbFNwYW4gPiAxKTtcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoaGFzQ29sc3Bhbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY29sc3Bhbk51bWJlciA9IChiYXNlQ2VsbC5jb2xTcGFuKSA/IHBhcnNlSW50KGJhc2VDZWxsLmNvbFNwYW4sIDEwKSA6IDA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjb2xzcGFuTnVtYmVyID4gMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3Bhbk51bWJlci0tO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuc2V0QXR0cmlidXRlKCdjb2xTcGFuJywgY29sc3Bhbk51bWJlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmFzZUNlbGwuY2VsbEluZGV4ID09PSBjZWxsUG9zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoY29sc3Bhbk51bWJlciA9PT0gMikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwucmVtb3ZlQXR0cmlidXRlKCdjb2xTcGFuJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmFzZUNlbGwuY2VsbEluZGV4ID09PSBjZWxsUG9zKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYmFzZUNlbGwuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgICBjYXNlIFRhYmxlUmVzdWx0QWN0aW9uLnJlc3VsdEFjdGlvbi5SZW1vdmVDZWxsOlxuICAgICAgICAgICAgICAgICAgICAgIGRvbS5yZW1vdmUoYWN0aW9uc1thY3Rpb25JbmRleF0uYmFzZUNlbGwsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogY3JlYXRlIGVtcHR5IHRhYmxlIGVsZW1lbnRcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge051bWJlcn0gcm93Q291bnRcbiAgICAgICAqIEBwYXJhbSB7TnVtYmVyfSBjb2xDb3VudFxuICAgICAgICogQHJldHVybiB7Tm9kZX1cbiAgICAgICAqL1xuICAgICAgVGFibGUucHJvdG90eXBlLmNyZWF0ZVRhYmxlID0gZnVuY3Rpb24gKGNvbENvdW50LCByb3dDb3VudCwgb3B0aW9ucykge1xuICAgICAgICAgIHZhciB0ZHMgPSBbXTtcbiAgICAgICAgICB2YXIgdGRIVE1MO1xuICAgICAgICAgIGZvciAodmFyIGlkeENvbCA9IDA7IGlkeENvbCA8IGNvbENvdW50OyBpZHhDb2wrKykge1xuICAgICAgICAgICAgICB0ZHMucHVzaCgnPHRkPicgKyBkb20uYmxhbmsgKyAnPC90ZD4nKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGRIVE1MID0gdGRzLmpvaW4oJycpO1xuICAgICAgICAgIHZhciB0cnMgPSBbXTtcbiAgICAgICAgICB2YXIgdHJIVE1MO1xuICAgICAgICAgIGZvciAodmFyIGlkeFJvdyA9IDA7IGlkeFJvdyA8IHJvd0NvdW50OyBpZHhSb3crKykge1xuICAgICAgICAgICAgICB0cnMucHVzaCgnPHRyPicgKyB0ZEhUTUwgKyAnPC90cj4nKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdHJIVE1MID0gdHJzLmpvaW4oJycpO1xuICAgICAgICAgIHZhciAkdGFibGUgPSAkJDEoJzx0YWJsZT4nICsgdHJIVE1MICsgJzwvdGFibGU+Jyk7XG4gICAgICAgICAgaWYgKG9wdGlvbnMgJiYgb3B0aW9ucy50YWJsZUNsYXNzTmFtZSkge1xuICAgICAgICAgICAgICAkdGFibGUuYWRkQ2xhc3Mob3B0aW9ucy50YWJsZUNsYXNzTmFtZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiAkdGFibGVbMF07XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBEZWxldGUgY3VycmVudCB0YWJsZVxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7V3JhcHBlZFJhbmdlfSBybmdcbiAgICAgICAqIEByZXR1cm4ge05vZGV9XG4gICAgICAgKi9cbiAgICAgIFRhYmxlLnByb3RvdHlwZS5kZWxldGVUYWJsZSA9IGZ1bmN0aW9uIChybmcpIHtcbiAgICAgICAgICB2YXIgY2VsbCA9IGRvbS5hbmNlc3RvcihybmcuY29tbW9uQW5jZXN0b3IoKSwgZG9tLmlzQ2VsbCk7XG4gICAgICAgICAgJCQxKGNlbGwpLmNsb3Nlc3QoJ3RhYmxlJykucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFRhYmxlO1xuICB9KCkpO1xuXG4gIHZhciBLRVlfQk9HVVMgPSAnYm9ndXMnO1xuICAvKipcbiAgICogQGNsYXNzIEVkaXRvclxuICAgKi9cbiAgdmFyIEVkaXRvciA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEVkaXRvcihjb250ZXh0KSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMuJG5vdGUgPSBjb250ZXh0LmxheW91dEluZm8ubm90ZTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRhYmxlO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICAgICAgdGhpcy5lZGl0YWJsZSA9IHRoaXMuJGVkaXRhYmxlWzBdO1xuICAgICAgICAgIHRoaXMubGFzdFJhbmdlID0gbnVsbDtcbiAgICAgICAgICB0aGlzLnN0eWxlID0gbmV3IFN0eWxlKCk7XG4gICAgICAgICAgdGhpcy50YWJsZSA9IG5ldyBUYWJsZSgpO1xuICAgICAgICAgIHRoaXMudHlwaW5nID0gbmV3IFR5cGluZyhjb250ZXh0KTtcbiAgICAgICAgICB0aGlzLmJ1bGxldCA9IG5ldyBCdWxsZXQoKTtcbiAgICAgICAgICB0aGlzLmhpc3RvcnkgPSBuZXcgSGlzdG9yeSh0aGlzLiRlZGl0YWJsZSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2hlbHAudW5kbycsIHRoaXMubGFuZy5oZWxwLnVuZG8pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLnJlZG8nLCB0aGlzLmxhbmcuaGVscC5yZWRvKTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnaGVscC50YWInLCB0aGlzLmxhbmcuaGVscC50YWIpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLnVudGFiJywgdGhpcy5sYW5nLmhlbHAudW50YWIpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLmluc2VydFBhcmFncmFwaCcsIHRoaXMubGFuZy5oZWxwLmluc2VydFBhcmFncmFwaCk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2hlbHAuaW5zZXJ0T3JkZXJlZExpc3QnLCB0aGlzLmxhbmcuaGVscC5pbnNlcnRPcmRlcmVkTGlzdCk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2hlbHAuaW5zZXJ0VW5vcmRlcmVkTGlzdCcsIHRoaXMubGFuZy5oZWxwLmluc2VydFVub3JkZXJlZExpc3QpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLmluZGVudCcsIHRoaXMubGFuZy5oZWxwLmluZGVudCk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2hlbHAub3V0ZGVudCcsIHRoaXMubGFuZy5oZWxwLm91dGRlbnQpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLmZvcm1hdFBhcmEnLCB0aGlzLmxhbmcuaGVscC5mb3JtYXRQYXJhKTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnaGVscC5pbnNlcnRIb3Jpem9udGFsUnVsZScsIHRoaXMubGFuZy5oZWxwLmluc2VydEhvcml6b250YWxSdWxlKTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnaGVscC5mb250TmFtZScsIHRoaXMubGFuZy5oZWxwLmZvbnROYW1lKTtcbiAgICAgICAgICAvLyBuYXRpdmUgY29tbWFuZHMod2l0aCBleGVjQ29tbWFuZCksIGdlbmVyYXRlIGZ1bmN0aW9uIGZvciBleGVjQ29tbWFuZFxuICAgICAgICAgIHZhciBjb21tYW5kcyA9IFtcbiAgICAgICAgICAgICAgJ2JvbGQnLCAnaXRhbGljJywgJ3VuZGVybGluZScsICdzdHJpa2V0aHJvdWdoJywgJ3N1cGVyc2NyaXB0JywgJ3N1YnNjcmlwdCcsXG4gICAgICAgICAgICAgICdqdXN0aWZ5TGVmdCcsICdqdXN0aWZ5Q2VudGVyJywgJ2p1c3RpZnlSaWdodCcsICdqdXN0aWZ5RnVsbCcsXG4gICAgICAgICAgICAgICdmb3JtYXRCbG9jaycsICdyZW1vdmVGb3JtYXQnLCAnYmFja0NvbG9yJyxcbiAgICAgICAgICBdO1xuICAgICAgICAgIGZvciAodmFyIGlkeCA9IDAsIGxlbiA9IGNvbW1hbmRzLmxlbmd0aDsgaWR4IDwgbGVuOyBpZHgrKykge1xuICAgICAgICAgICAgICB0aGlzW2NvbW1hbmRzW2lkeF1dID0gKGZ1bmN0aW9uIChzQ21kKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMuYmVmb3JlQ29tbWFuZCgpO1xuICAgICAgICAgICAgICAgICAgICAgIGRvY3VtZW50LmV4ZWNDb21tYW5kKHNDbWQsIGZhbHNlLCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMuYWZ0ZXJDb21tYW5kKHRydWUpO1xuICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgfSkoY29tbWFuZHNbaWR4XSk7XG4gICAgICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLicgKyBjb21tYW5kc1tpZHhdLCB0aGlzLmxhbmcuaGVscFtjb21tYW5kc1tpZHhdXSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuZm9udE5hbWUgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuZm9udFN0eWxpbmcoJ2ZvbnQtZmFtaWx5JywgXCJcXCdcIiArIHZhbHVlICsgXCJcXCdcIik7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5mb250U2l6ZSA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5mb250U3R5bGluZygnZm9udC1zaXplJywgdmFsdWUgKyAncHgnKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBmb3IgKHZhciBpZHggPSAxOyBpZHggPD0gNjsgaWR4KyspIHtcbiAgICAgICAgICAgICAgdGhpc1snZm9ybWF0SCcgKyBpZHhdID0gKGZ1bmN0aW9uIChpZHgpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMuZm9ybWF0QmxvY2soJ0gnICsgaWR4KTtcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIH0pKGlkeCk7XG4gICAgICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdoZWxwLmZvcm1hdEgnICsgaWR4LCB0aGlzLmxhbmcuaGVscFsnZm9ybWF0SCcgKyBpZHhdKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5pbnNlcnRQYXJhZ3JhcGggPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMudHlwaW5nLmluc2VydFBhcmFncmFwaChfdGhpcy5lZGl0YWJsZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5pbnNlcnRPcmRlcmVkTGlzdCA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBfdGhpcy5idWxsZXQuaW5zZXJ0T3JkZXJlZExpc3QoX3RoaXMuZWRpdGFibGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuaW5zZXJ0VW5vcmRlcmVkTGlzdCA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBfdGhpcy5idWxsZXQuaW5zZXJ0VW5vcmRlcmVkTGlzdChfdGhpcy5lZGl0YWJsZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5pbmRlbnQgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuYnVsbGV0LmluZGVudChfdGhpcy5lZGl0YWJsZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5vdXRkZW50ID0gdGhpcy53cmFwQ29tbWFuZChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIF90aGlzLmJ1bGxldC5vdXRkZW50KF90aGlzLmVkaXRhYmxlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBpbnNlcnROb2RlXG4gICAgICAgICAgICogaW5zZXJ0IG5vZGVcbiAgICAgICAgICAgKiBAcGFyYW0ge05vZGV9IG5vZGVcbiAgICAgICAgICAgKi9cbiAgICAgICAgICB0aGlzLmluc2VydE5vZGUgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICAgICAgICAgIGlmIChfdGhpcy5pc0xpbWl0ZWQoJCQxKG5vZGUpLnRleHQoKS5sZW5ndGgpKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdmFyIHJuZyA9IF90aGlzLmdldExhc3RSYW5nZSgpO1xuICAgICAgICAgICAgICBybmcuaW5zZXJ0Tm9kZShub2RlKTtcbiAgICAgICAgICAgICAgcmFuZ2UuY3JlYXRlRnJvbU5vZGVBZnRlcihub2RlKS5zZWxlY3QoKTtcbiAgICAgICAgICAgICAgX3RoaXMuc2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLyoqXG4gICAgICAgICAgICogaW5zZXJ0IHRleHRcbiAgICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30gdGV4dFxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuaW5zZXJ0VGV4dCA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKHRleHQpIHtcbiAgICAgICAgICAgICAgaWYgKF90aGlzLmlzTGltaXRlZCh0ZXh0Lmxlbmd0aCkpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB2YXIgcm5nID0gX3RoaXMuZ2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgICAgIHZhciB0ZXh0Tm9kZSA9IHJuZy5pbnNlcnROb2RlKGRvbS5jcmVhdGVUZXh0KHRleHQpKTtcbiAgICAgICAgICAgICAgcmFuZ2UuY3JlYXRlKHRleHROb2RlLCBkb20ubm9kZUxlbmd0aCh0ZXh0Tm9kZSkpLnNlbGVjdCgpO1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBwYXN0ZSBIVE1MXG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IG1hcmt1cFxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMucGFzdGVIVE1MID0gdGhpcy53cmFwQ29tbWFuZChmdW5jdGlvbiAobWFya3VwKSB7XG4gICAgICAgICAgICAgIGlmIChfdGhpcy5pc0xpbWl0ZWQobWFya3VwLmxlbmd0aCkpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBtYXJrdXAgPSBfdGhpcy5jb250ZXh0Lmludm9rZSgnY29kZXZpZXcucHVyaWZ5JywgbWFya3VwKTtcbiAgICAgICAgICAgICAgdmFyIGNvbnRlbnRzID0gX3RoaXMuZ2V0TGFzdFJhbmdlKCkucGFzdGVIVE1MKG1hcmt1cCk7XG4gICAgICAgICAgICAgIHJhbmdlLmNyZWF0ZUZyb21Ob2RlQWZ0ZXIobGlzdHMubGFzdChjb250ZW50cykpLnNlbGVjdCgpO1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBmb3JtYXRCbG9ja1xuICAgICAgICAgICAqXG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHRhZ05hbWVcbiAgICAgICAgICAgKi9cbiAgICAgICAgICB0aGlzLmZvcm1hdEJsb2NrID0gdGhpcy53cmFwQ29tbWFuZChmdW5jdGlvbiAodGFnTmFtZSwgJHRhcmdldCkge1xuICAgICAgICAgICAgICB2YXIgb25BcHBseUN1c3RvbVN0eWxlID0gX3RoaXMub3B0aW9ucy5jYWxsYmFja3Mub25BcHBseUN1c3RvbVN0eWxlO1xuICAgICAgICAgICAgICBpZiAob25BcHBseUN1c3RvbVN0eWxlKSB7XG4gICAgICAgICAgICAgICAgICBvbkFwcGx5Q3VzdG9tU3R5bGUuY2FsbChfdGhpcywgJHRhcmdldCwgX3RoaXMuY29udGV4dCwgX3RoaXMub25Gb3JtYXRCbG9jayk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5vbkZvcm1hdEJsb2NrKHRhZ05hbWUsICR0YXJnZXQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLyoqXG4gICAgICAgICAgICogaW5zZXJ0IGhvcml6b250YWwgcnVsZVxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuaW5zZXJ0SG9yaXpvbnRhbFJ1bGUgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgdmFyIGhyTm9kZSA9IF90aGlzLmdldExhc3RSYW5nZSgpLmluc2VydE5vZGUoZG9tLmNyZWF0ZSgnSFInKSk7XG4gICAgICAgICAgICAgIGlmIChock5vZGUubmV4dFNpYmxpbmcpIHtcbiAgICAgICAgICAgICAgICAgIHJhbmdlLmNyZWF0ZShock5vZGUubmV4dFNpYmxpbmcsIDApLm5vcm1hbGl6ZSgpLnNlbGVjdCgpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuc2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBsaW5lSGVpZ2h0XG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlXG4gICAgICAgICAgICovXG4gICAgICAgICAgdGhpcy5saW5lSGVpZ2h0ID0gdGhpcy53cmFwQ29tbWFuZChmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgX3RoaXMuc3R5bGUuc3R5bGVQYXJhKF90aGlzLmdldExhc3RSYW5nZSgpLCB7XG4gICAgICAgICAgICAgICAgICBsaW5lSGVpZ2h0OiB2YWx1ZVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiBjcmVhdGUgbGluayAoY29tbWFuZClcbiAgICAgICAgICAgKlxuICAgICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBsaW5rSW5mb1xuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuY3JlYXRlTGluayA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKGxpbmtJbmZvKSB7XG4gICAgICAgICAgICAgIHZhciBsaW5rVXJsID0gbGlua0luZm8udXJsO1xuICAgICAgICAgICAgICB2YXIgbGlua1RleHQgPSBsaW5rSW5mby50ZXh0O1xuICAgICAgICAgICAgICB2YXIgaXNOZXdXaW5kb3cgPSBsaW5rSW5mby5pc05ld1dpbmRvdztcbiAgICAgICAgICAgICAgdmFyIHJuZyA9IGxpbmtJbmZvLnJhbmdlIHx8IF90aGlzLmdldExhc3RSYW5nZSgpO1xuICAgICAgICAgICAgICB2YXIgYWRkaXRpb25hbFRleHRMZW5ndGggPSBsaW5rVGV4dC5sZW5ndGggLSBybmcudG9TdHJpbmcoKS5sZW5ndGg7XG4gICAgICAgICAgICAgIGlmIChhZGRpdGlvbmFsVGV4dExlbmd0aCA+IDAgJiYgX3RoaXMuaXNMaW1pdGVkKGFkZGl0aW9uYWxUZXh0TGVuZ3RoKSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHZhciBpc1RleHRDaGFuZ2VkID0gcm5nLnRvU3RyaW5nKCkgIT09IGxpbmtUZXh0O1xuICAgICAgICAgICAgICAvLyBoYW5kbGUgc3BhY2VkIHVybHMgZnJvbSBpbnB1dFxuICAgICAgICAgICAgICBpZiAodHlwZW9mIGxpbmtVcmwgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgICBsaW5rVXJsID0gbGlua1VybC50cmltKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKF90aGlzLm9wdGlvbnMub25DcmVhdGVMaW5rKSB7XG4gICAgICAgICAgICAgICAgICBsaW5rVXJsID0gX3RoaXMub3B0aW9ucy5vbkNyZWF0ZUxpbmsobGlua1VybCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAvLyBpZiB1cmwgZG9lc24ndCBoYXZlIGFueSBwcm90b2NvbCBhbmQgbm90IGV2ZW4gYSByZWxhdGl2ZSBvciBhIGxhYmVsLCB1c2UgaHR0cDovLyBhcyBkZWZhdWx0XG4gICAgICAgICAgICAgICAgICBsaW5rVXJsID0gL14oW0EtWmEtel1bQS1aYS16MC05Ky0uXSpcXDp8I3xcXC8pLy50ZXN0KGxpbmtVcmwpXG4gICAgICAgICAgICAgICAgICAgICAgPyBsaW5rVXJsIDogJ2h0dHA6Ly8nICsgbGlua1VybDtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB2YXIgYW5jaG9ycyA9IFtdO1xuICAgICAgICAgICAgICBpZiAoaXNUZXh0Q2hhbmdlZCkge1xuICAgICAgICAgICAgICAgICAgcm5nID0gcm5nLmRlbGV0ZUNvbnRlbnRzKCk7XG4gICAgICAgICAgICAgICAgICB2YXIgYW5jaG9yID0gcm5nLmluc2VydE5vZGUoJCQxKCc8QT4nICsgbGlua1RleHQgKyAnPC9BPicpWzBdKTtcbiAgICAgICAgICAgICAgICAgIGFuY2hvcnMucHVzaChhbmNob3IpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgYW5jaG9ycyA9IF90aGlzLnN0eWxlLnN0eWxlTm9kZXMocm5nLCB7XG4gICAgICAgICAgICAgICAgICAgICAgbm9kZU5hbWU6ICdBJyxcbiAgICAgICAgICAgICAgICAgICAgICBleHBhbmRDbG9zZXN0U2libGluZzogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICBvbmx5UGFydGlhbENvbnRhaW5zOiB0cnVlXG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAkJDEuZWFjaChhbmNob3JzLCBmdW5jdGlvbiAoaWR4LCBhbmNob3IpIHtcbiAgICAgICAgICAgICAgICAgICQkMShhbmNob3IpLmF0dHIoJ2hyZWYnLCBsaW5rVXJsKTtcbiAgICAgICAgICAgICAgICAgIGlmIChpc05ld1dpbmRvdykge1xuICAgICAgICAgICAgICAgICAgICAgICQkMShhbmNob3IpLmF0dHIoJ3RhcmdldCcsICdfYmxhbmsnKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICQkMShhbmNob3IpLnJlbW92ZUF0dHIoJ3RhcmdldCcpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgdmFyIHN0YXJ0UmFuZ2UgPSByYW5nZS5jcmVhdGVGcm9tTm9kZUJlZm9yZShsaXN0cy5oZWFkKGFuY2hvcnMpKTtcbiAgICAgICAgICAgICAgdmFyIHN0YXJ0UG9pbnQgPSBzdGFydFJhbmdlLmdldFN0YXJ0UG9pbnQoKTtcbiAgICAgICAgICAgICAgdmFyIGVuZFJhbmdlID0gcmFuZ2UuY3JlYXRlRnJvbU5vZGVBZnRlcihsaXN0cy5sYXN0KGFuY2hvcnMpKTtcbiAgICAgICAgICAgICAgdmFyIGVuZFBvaW50ID0gZW5kUmFuZ2UuZ2V0RW5kUG9pbnQoKTtcbiAgICAgICAgICAgICAgcmFuZ2UuY3JlYXRlKHN0YXJ0UG9pbnQubm9kZSwgc3RhcnRQb2ludC5vZmZzZXQsIGVuZFBvaW50Lm5vZGUsIGVuZFBvaW50Lm9mZnNldCkuc2VsZWN0KCk7XG4gICAgICAgICAgICAgIF90aGlzLnNldExhc3RSYW5nZSgpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8qKlxuICAgICAgICAgICAqIHNldHRpbmcgY29sb3JcbiAgICAgICAgICAgKlxuICAgICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBzT2JqQ29sb3IgIGNvbG9yIGNvZGVcbiAgICAgICAgICAgKiBAcGFyYW0ge1N0cmluZ30gc09iakNvbG9yLmZvcmVDb2xvciBmb3JlZ3JvdW5kIGNvbG9yXG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHNPYmpDb2xvci5iYWNrQ29sb3IgYmFja2dyb3VuZCBjb2xvclxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuY29sb3IgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uIChjb2xvckluZm8pIHtcbiAgICAgICAgICAgICAgdmFyIGZvcmVDb2xvciA9IGNvbG9ySW5mby5mb3JlQ29sb3I7XG4gICAgICAgICAgICAgIHZhciBiYWNrQ29sb3IgPSBjb2xvckluZm8uYmFja0NvbG9yO1xuICAgICAgICAgICAgICBpZiAoZm9yZUNvbG9yKSB7XG4gICAgICAgICAgICAgICAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnZm9yZUNvbG9yJywgZmFsc2UsIGZvcmVDb2xvcik7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKGJhY2tDb2xvcikge1xuICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ2JhY2tDb2xvcicsIGZhbHNlLCBiYWNrQ29sb3IpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLyoqXG4gICAgICAgICAgICogU2V0IGZvcmVncm91bmQgY29sb3JcbiAgICAgICAgICAgKlxuICAgICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSBjb2xvckNvZGUgZm9yZWdyb3VuZCBjb2xvciBjb2RlXG4gICAgICAgICAgICovXG4gICAgICAgICAgdGhpcy5mb3JlQ29sb3IgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uIChjb2xvckluZm8pIHtcbiAgICAgICAgICAgICAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ3N0eWxlV2l0aENTUycsIGZhbHNlLCB0cnVlKTtcbiAgICAgICAgICAgICAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ2ZvcmVDb2xvcicsIGZhbHNlLCBjb2xvckluZm8pO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8qKlxuICAgICAgICAgICAqIGluc2VydCBUYWJsZVxuICAgICAgICAgICAqXG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IGRpbWVuc2lvbiBvZiB0YWJsZSAoZXggOiBcIjV4NVwiKVxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuaW5zZXJ0VGFibGUgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uIChkaW0pIHtcbiAgICAgICAgICAgICAgdmFyIGRpbWVuc2lvbiA9IGRpbS5zcGxpdCgneCcpO1xuICAgICAgICAgICAgICB2YXIgcm5nID0gX3RoaXMuZ2V0TGFzdFJhbmdlKCkuZGVsZXRlQ29udGVudHMoKTtcbiAgICAgICAgICAgICAgcm5nLmluc2VydE5vZGUoX3RoaXMudGFibGUuY3JlYXRlVGFibGUoZGltZW5zaW9uWzBdLCBkaW1lbnNpb25bMV0sIF90aGlzLm9wdGlvbnMpKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiByZW1vdmUgbWVkaWEgb2JqZWN0IGFuZCBGaWd1cmUgRWxlbWVudHMgaWYgbWVkaWEgb2JqZWN0IGlzIGltZyB3aXRoIEZpZ3VyZS5cbiAgICAgICAgICAgKi9cbiAgICAgICAgICB0aGlzLnJlbW92ZU1lZGlhID0gdGhpcy53cmFwQ29tbWFuZChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHZhciAkdGFyZ2V0ID0gJCQxKF90aGlzLnJlc3RvcmVUYXJnZXQoKSkucGFyZW50KCk7XG4gICAgICAgICAgICAgIGlmICgkdGFyZ2V0LnBhcmVudCgnZmlndXJlJykubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAkdGFyZ2V0LnBhcmVudCgnZmlndXJlJykucmVtb3ZlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAkdGFyZ2V0ID0gJCQxKF90aGlzLnJlc3RvcmVUYXJnZXQoKSkuZGV0YWNoKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ21lZGlhLmRlbGV0ZScsICR0YXJnZXQsIF90aGlzLiRlZGl0YWJsZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLyoqXG4gICAgICAgICAgICogZmxvYXQgbWVcbiAgICAgICAgICAgKlxuICAgICAgICAgICAqIEBwYXJhbSB7U3RyaW5nfSB2YWx1ZVxuICAgICAgICAgICAqL1xuICAgICAgICAgIHRoaXMuZmxvYXRNZSA9IHRoaXMud3JhcENvbW1hbmQoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgIHZhciAkdGFyZ2V0ID0gJCQxKF90aGlzLnJlc3RvcmVUYXJnZXQoKSk7XG4gICAgICAgICAgICAgICR0YXJnZXQudG9nZ2xlQ2xhc3MoJ25vdGUtZmxvYXQtbGVmdCcsIHZhbHVlID09PSAnbGVmdCcpO1xuICAgICAgICAgICAgICAkdGFyZ2V0LnRvZ2dsZUNsYXNzKCdub3RlLWZsb2F0LXJpZ2h0JywgdmFsdWUgPT09ICdyaWdodCcpO1xuICAgICAgICAgICAgICAkdGFyZ2V0LmNzcygnZmxvYXQnLCAodmFsdWUgPT09ICdub25lJyA/ICcnIDogdmFsdWUpKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvKipcbiAgICAgICAgICAgKiByZXNpemUgb3ZlcmxheSBlbGVtZW50XG4gICAgICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHZhbHVlXG4gICAgICAgICAgICovXG4gICAgICAgICAgdGhpcy5yZXNpemUgPSB0aGlzLndyYXBDb21tYW5kKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICB2YXIgJHRhcmdldCA9ICQkMShfdGhpcy5yZXN0b3JlVGFyZ2V0KCkpO1xuICAgICAgICAgICAgICB2YWx1ZSA9IHBhcnNlRmxvYXQodmFsdWUpO1xuICAgICAgICAgICAgICBpZiAodmFsdWUgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgICR0YXJnZXQuY3NzKCd3aWR0aCcsICcnKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICR0YXJnZXQuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogdmFsdWUgKiAxMDAgKyAnJScsXG4gICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAnJ1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIC8vIGJpbmQgY3VzdG9tIGV2ZW50c1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLm9uKCdrZXlkb3duJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIGlmIChldmVudC5rZXlDb2RlID09PSBrZXkuY29kZS5FTlRFUikge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2VudGVyJywgZXZlbnQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdrZXlkb3duJywgZXZlbnQpO1xuICAgICAgICAgICAgICBpZiAoIWV2ZW50LmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy5zaG9ydGN1dHMpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5oYW5kbGVLZXlNYXAoZXZlbnQpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMucHJldmVudERlZmF1bHRFZGl0YWJsZVNob3J0Q3V0cyhldmVudCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKF90aGlzLmlzTGltaXRlZCgxLCBldmVudCkpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLm9uKCdrZXl1cCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2tleXVwJywgZXZlbnQpO1xuICAgICAgICAgIH0pLm9uKCdmb2N1cycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2ZvY3VzJywgZXZlbnQpO1xuICAgICAgICAgIH0pLm9uKCdibHVyJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdibHVyJywgZXZlbnQpO1xuICAgICAgICAgIH0pLm9uKCdtb3VzZWRvd24nLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ21vdXNlZG93bicsIGV2ZW50KTtcbiAgICAgICAgICB9KS5vbignbW91c2V1cCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ21vdXNldXAnLCBldmVudCk7XG4gICAgICAgICAgfSkub24oJ3Njcm9sbCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnc2Nyb2xsJywgZXZlbnQpO1xuICAgICAgICAgIH0pLm9uKCdwYXN0ZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ3Bhc3RlJywgZXZlbnQpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmF0dHIoJ3NwZWxsY2hlY2snLCB0aGlzLm9wdGlvbnMuc3BlbGxDaGVjayk7XG4gICAgICAgICAgLy8gaW5pdCBjb250ZW50IGJlZm9yZSBzZXQgZXZlbnRcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5odG1sKGRvbS5odG1sKHRoaXMuJG5vdGUpIHx8IGRvbS5lbXB0eVBhcmEpO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLm9uKGVudi5pbnB1dEV2ZW50TmFtZSwgZnVuYy5kZWJvdW5jZShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdjaGFuZ2UnLCBfdGhpcy4kZWRpdGFibGUuaHRtbCgpLCBfdGhpcy4kZWRpdGFibGUpO1xuICAgICAgICAgIH0sIDEwKSk7XG4gICAgICAgICAgdGhpcy4kZWRpdG9yLm9uKCdmb2N1c2luJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdmb2N1c2luJywgZXZlbnQpO1xuICAgICAgICAgIH0pLm9uKCdmb2N1c291dCcsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnZm9jdXNvdXQnLCBldmVudCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYgKCF0aGlzLm9wdGlvbnMuYWlyTW9kZSkge1xuICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLndpZHRoKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLiRlZGl0b3Iub3V0ZXJXaWR0aCh0aGlzLm9wdGlvbnMud2lkdGgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuaGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5vdXRlckhlaWdodCh0aGlzLm9wdGlvbnMuaGVpZ2h0KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLm1heEhlaWdodCkge1xuICAgICAgICAgICAgICAgICAgdGhpcy4kZWRpdGFibGUuY3NzKCdtYXgtaGVpZ2h0JywgdGhpcy5vcHRpb25zLm1heEhlaWdodCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5taW5IZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmNzcygnbWluLWhlaWdodCcsIHRoaXMub3B0aW9ucy5taW5IZWlnaHQpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuaGlzdG9yeS5yZWNvcmRVbmRvKCk7XG4gICAgICAgICAgdGhpcy5zZXRMYXN0UmFuZ2UoKTtcbiAgICAgIH07XG4gICAgICBFZGl0b3IucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kZWRpdGFibGUub2ZmKCk7XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5oYW5kbGVLZXlNYXAgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICB2YXIga2V5TWFwID0gdGhpcy5vcHRpb25zLmtleU1hcFtlbnYuaXNNYWMgPyAnbWFjJyA6ICdwYyddO1xuICAgICAgICAgIHZhciBrZXlzID0gW107XG4gICAgICAgICAgaWYgKGV2ZW50Lm1ldGFLZXkpIHtcbiAgICAgICAgICAgICAga2V5cy5wdXNoKCdDTUQnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGV2ZW50LmN0cmxLZXkgJiYgIWV2ZW50LmFsdEtleSkge1xuICAgICAgICAgICAgICBrZXlzLnB1c2goJ0NUUkwnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKGV2ZW50LnNoaWZ0S2V5KSB7XG4gICAgICAgICAgICAgIGtleXMucHVzaCgnU0hJRlQnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIGtleU5hbWUgPSBrZXkubmFtZUZyb21Db2RlW2V2ZW50LmtleUNvZGVdO1xuICAgICAgICAgIGlmIChrZXlOYW1lKSB7XG4gICAgICAgICAgICAgIGtleXMucHVzaChrZXlOYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIGV2ZW50TmFtZSA9IGtleU1hcFtrZXlzLmpvaW4oJysnKV07XG4gICAgICAgICAgaWYgKGV2ZW50TmFtZSkge1xuICAgICAgICAgICAgICBpZiAodGhpcy5jb250ZXh0Lmludm9rZShldmVudE5hbWUpICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmIChrZXkuaXNFZGl0KGV2ZW50LmtleUNvZGUpKSB7XG4gICAgICAgICAgICAgIHRoaXMuYWZ0ZXJDb21tYW5kKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIEVkaXRvci5wcm90b3R5cGUucHJldmVudERlZmF1bHRFZGl0YWJsZVNob3J0Q3V0cyA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgIC8vIEIoQm9sZCwgNjYpIC8gSShJdGFsaWMsIDczKSAvIFUoVW5kZXJsaW5lLCA4NSlcbiAgICAgICAgICBpZiAoKGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQubWV0YUtleSkgJiZcbiAgICAgICAgICAgICAgbGlzdHMuY29udGFpbnMoWzY2LCA3MywgODVdLCBldmVudC5rZXlDb2RlKSkge1xuICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBFZGl0b3IucHJvdG90eXBlLmlzTGltaXRlZCA9IGZ1bmN0aW9uIChwYWQsIGV2ZW50KSB7XG4gICAgICAgICAgcGFkID0gcGFkIHx8IDA7XG4gICAgICAgICAgaWYgKHR5cGVvZiBldmVudCAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgaWYgKGtleS5pc01vdmUoZXZlbnQua2V5Q29kZSkgfHxcbiAgICAgICAgICAgICAgICAgIChldmVudC5jdHJsS2V5IHx8IGV2ZW50Lm1ldGFLZXkpIHx8XG4gICAgICAgICAgICAgICAgICBsaXN0cy5jb250YWlucyhba2V5LmNvZGUuQkFDS1NQQUNFLCBrZXkuY29kZS5ERUxFVEVdLCBldmVudC5rZXlDb2RlKSkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMubWF4VGV4dExlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgaWYgKCh0aGlzLiRlZGl0YWJsZS50ZXh0KCkubGVuZ3RoICsgcGFkKSA+PSB0aGlzLm9wdGlvbnMubWF4VGV4dExlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogY3JlYXRlIHJhbmdlXG4gICAgICAgKiBAcmV0dXJuIHtXcmFwcGVkUmFuZ2V9XG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuY3JlYXRlUmFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5mb2N1cygpO1xuICAgICAgICAgIHRoaXMuc2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TGFzdFJhbmdlKCk7XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5zZXRMYXN0UmFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5sYXN0UmFuZ2UgPSByYW5nZS5jcmVhdGUodGhpcy5lZGl0YWJsZSk7XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5nZXRMYXN0UmFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgaWYgKCF0aGlzLmxhc3RSYW5nZSkge1xuICAgICAgICAgICAgICB0aGlzLnNldExhc3RSYW5nZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdGhpcy5sYXN0UmFuZ2U7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBzYXZlUmFuZ2VcbiAgICAgICAqXG4gICAgICAgKiBzYXZlIGN1cnJlbnQgcmFuZ2VcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge0Jvb2xlYW59IFt0aGVuQ29sbGFwc2U9ZmFsc2VdXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuc2F2ZVJhbmdlID0gZnVuY3Rpb24gKHRoZW5Db2xsYXBzZSkge1xuICAgICAgICAgIGlmICh0aGVuQ29sbGFwc2UpIHtcbiAgICAgICAgICAgICAgdGhpcy5nZXRMYXN0UmFuZ2UoKS5jb2xsYXBzZSgpLnNlbGVjdCgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJlc3RvcmVSYW5nZVxuICAgICAgICpcbiAgICAgICAqIHJlc3RvcmUgbGF0ZWx5IHJhbmdlXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUucmVzdG9yZVJhbmdlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmICh0aGlzLmxhc3RSYW5nZSkge1xuICAgICAgICAgICAgICB0aGlzLmxhc3RSYW5nZS5zZWxlY3QoKTtcbiAgICAgICAgICAgICAgdGhpcy5mb2N1cygpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBFZGl0b3IucHJvdG90eXBlLnNhdmVUYXJnZXQgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmRhdGEoJ3RhcmdldCcsIG5vZGUpO1xuICAgICAgfTtcbiAgICAgIEVkaXRvci5wcm90b3R5cGUuY2xlYXJUYXJnZXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kZWRpdGFibGUucmVtb3ZlRGF0YSgndGFyZ2V0Jyk7XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5yZXN0b3JlVGFyZ2V0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLiRlZGl0YWJsZS5kYXRhKCd0YXJnZXQnKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGN1cnJlbnRTdHlsZVxuICAgICAgICpcbiAgICAgICAqIGN1cnJlbnQgc3R5bGVcbiAgICAgICAqIEByZXR1cm4ge09iamVjdHxCb29sZWFufSB1bmZvY3VzXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuY3VycmVudFN0eWxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBybmcgPSByYW5nZS5jcmVhdGUoKTtcbiAgICAgICAgICBpZiAocm5nKSB7XG4gICAgICAgICAgICAgIHJuZyA9IHJuZy5ub3JtYWxpemUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHJuZyA/IHRoaXMuc3R5bGUuY3VycmVudChybmcpIDogdGhpcy5zdHlsZS5mcm9tTm9kZSh0aGlzLiRlZGl0YWJsZSk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBzdHlsZSBmcm9tIG5vZGVcbiAgICAgICAqXG4gICAgICAgKiBAcGFyYW0ge2pRdWVyeX0gJG5vZGVcbiAgICAgICAqIEByZXR1cm4ge09iamVjdH1cbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5zdHlsZUZyb21Ob2RlID0gZnVuY3Rpb24gKCRub2RlKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuc3R5bGUuZnJvbU5vZGUoJG5vZGUpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogdW5kb1xuICAgICAgICovXG4gICAgICBFZGl0b3IucHJvdG90eXBlLnVuZG8gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnYmVmb3JlLmNvbW1hbmQnLCB0aGlzLiRlZGl0YWJsZS5odG1sKCkpO1xuICAgICAgICAgIHRoaXMuaGlzdG9yeS51bmRvKCk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnY2hhbmdlJywgdGhpcy4kZWRpdGFibGUuaHRtbCgpLCB0aGlzLiRlZGl0YWJsZSk7XG4gICAgICB9O1xuICAgICAgLypcbiAgICAgICogY29tbWl0XG4gICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5jb21taXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnYmVmb3JlLmNvbW1hbmQnLCB0aGlzLiRlZGl0YWJsZS5odG1sKCkpO1xuICAgICAgICAgIHRoaXMuaGlzdG9yeS5jb21taXQoKTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdjaGFuZ2UnLCB0aGlzLiRlZGl0YWJsZS5odG1sKCksIHRoaXMuJGVkaXRhYmxlKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJlZG9cbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5yZWRvID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2JlZm9yZS5jb21tYW5kJywgdGhpcy4kZWRpdGFibGUuaHRtbCgpKTtcbiAgICAgICAgICB0aGlzLmhpc3RvcnkucmVkbygpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2NoYW5nZScsIHRoaXMuJGVkaXRhYmxlLmh0bWwoKSwgdGhpcy4kZWRpdGFibGUpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogYmVmb3JlIGNvbW1hbmRcbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5iZWZvcmVDb21tYW5kID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2JlZm9yZS5jb21tYW5kJywgdGhpcy4kZWRpdGFibGUuaHRtbCgpKTtcbiAgICAgICAgICAvLyBrZWVwIGZvY3VzIG9uIGVkaXRhYmxlIGJlZm9yZSBjb21tYW5kIGV4ZWN1dGlvblxuICAgICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGFmdGVyIGNvbW1hbmRcbiAgICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gaXNQcmV2ZW50VHJpZ2dlclxuICAgICAgICovXG4gICAgICBFZGl0b3IucHJvdG90eXBlLmFmdGVyQ29tbWFuZCA9IGZ1bmN0aW9uIChpc1ByZXZlbnRUcmlnZ2VyKSB7XG4gICAgICAgICAgdGhpcy5ub3JtYWxpemVDb250ZW50KCk7XG4gICAgICAgICAgdGhpcy5oaXN0b3J5LnJlY29yZFVuZG8oKTtcbiAgICAgICAgICBpZiAoIWlzUHJldmVudFRyaWdnZXIpIHtcbiAgICAgICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnY2hhbmdlJywgdGhpcy4kZWRpdGFibGUuaHRtbCgpLCB0aGlzLiRlZGl0YWJsZSk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaGFuZGxlIHRhYiBrZXlcbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS50YWIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMuZ2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgaWYgKHJuZy5pc0NvbGxhcHNlZCgpICYmIHJuZy5pc09uQ2VsbCgpKSB7XG4gICAgICAgICAgICAgIHRoaXMudGFibGUudGFiKHJuZyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnRhYlNpemUgPT09IDApIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBpZiAoIXRoaXMuaXNMaW1pdGVkKHRoaXMub3B0aW9ucy50YWJTaXplKSkge1xuICAgICAgICAgICAgICAgICAgdGhpcy5iZWZvcmVDb21tYW5kKCk7XG4gICAgICAgICAgICAgICAgICB0aGlzLnR5cGluZy5pbnNlcnRUYWIocm5nLCB0aGlzLm9wdGlvbnMudGFiU2l6ZSk7XG4gICAgICAgICAgICAgICAgICB0aGlzLmFmdGVyQ29tbWFuZCgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaGFuZGxlIHNoaWZ0K3RhYiBrZXlcbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS51bnRhYiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy5nZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICBpZiAocm5nLmlzQ29sbGFwc2VkKCkgJiYgcm5nLmlzT25DZWxsKCkpIHtcbiAgICAgICAgICAgICAgdGhpcy50YWJsZS50YWIocm5nLCB0cnVlKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMudGFiU2l6ZSA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcnVuIGdpdmVuIGZ1bmN0aW9uIGJldHdlZW4gYmVmb3JlQ29tbWFuZCBhbmQgYWZ0ZXJDb21tYW5kXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUud3JhcENvbW1hbmQgPSBmdW5jdGlvbiAoZm4pIHtcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICB0aGlzLmJlZm9yZUNvbW1hbmQoKTtcbiAgICAgICAgICAgICAgZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbiAgICAgICAgICAgICAgdGhpcy5hZnRlckNvbW1hbmQoKTtcbiAgICAgICAgICB9O1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaW5zZXJ0IGltYWdlXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtTdHJpbmd9IHNyY1xuICAgICAgICogQHBhcmFtIHtTdHJpbmd8RnVuY3Rpb259IHBhcmFtXG4gICAgICAgKiBAcmV0dXJuIHtQcm9taXNlfVxuICAgICAgICovXG4gICAgICBFZGl0b3IucHJvdG90eXBlLmluc2VydEltYWdlID0gZnVuY3Rpb24gKHNyYywgcGFyYW0pIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHJldHVybiBjcmVhdGVJbWFnZShzcmMsIHBhcmFtKS50aGVuKGZ1bmN0aW9uICgkaW1hZ2UpIHtcbiAgICAgICAgICAgICAgX3RoaXMuYmVmb3JlQ29tbWFuZCgpO1xuICAgICAgICAgICAgICBpZiAodHlwZW9mIHBhcmFtID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICBwYXJhbSgkaW1hZ2UpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXJhbSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAkaW1hZ2UuYXR0cignZGF0YS1maWxlbmFtZScsIHBhcmFtKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICRpbWFnZS5jc3MoJ3dpZHRoJywgTWF0aC5taW4oX3RoaXMuJGVkaXRhYmxlLndpZHRoKCksICRpbWFnZS53aWR0aCgpKSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgJGltYWdlLnNob3coKTtcbiAgICAgICAgICAgICAgcmFuZ2UuY3JlYXRlKF90aGlzLmVkaXRhYmxlKS5pbnNlcnROb2RlKCRpbWFnZVswXSk7XG4gICAgICAgICAgICAgIHJhbmdlLmNyZWF0ZUZyb21Ob2RlQWZ0ZXIoJGltYWdlWzBdKS5zZWxlY3QoKTtcbiAgICAgICAgICAgICAgX3RoaXMuc2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgICAgIF90aGlzLmFmdGVyQ29tbWFuZCgpO1xuICAgICAgICAgIH0pLmZhaWwoZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2ltYWdlLnVwbG9hZC5lcnJvcicsIGUpO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaW5zZXJ0SW1hZ2VzXG4gICAgICAgKiBAcGFyYW0ge0ZpbGVbXX0gZmlsZXNcbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5pbnNlcnRJbWFnZXNBc0RhdGFVUkwgPSBmdW5jdGlvbiAoZmlsZXMpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgICQkMS5lYWNoKGZpbGVzLCBmdW5jdGlvbiAoaWR4LCBmaWxlKSB7XG4gICAgICAgICAgICAgIHZhciBmaWxlbmFtZSA9IGZpbGUubmFtZTtcbiAgICAgICAgICAgICAgaWYgKF90aGlzLm9wdGlvbnMubWF4aW11bUltYWdlRmlsZVNpemUgJiYgX3RoaXMub3B0aW9ucy5tYXhpbXVtSW1hZ2VGaWxlU2l6ZSA8IGZpbGUuc2l6ZSkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2ltYWdlLnVwbG9hZC5lcnJvcicsIF90aGlzLmxhbmcuaW1hZ2UubWF4aW11bUZpbGVTaXplRXJyb3IpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgcmVhZEZpbGVBc0RhdGFVUkwoZmlsZSkudGhlbihmdW5jdGlvbiAoZGF0YVVSTCkge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5pbnNlcnRJbWFnZShkYXRhVVJMLCBmaWxlbmFtZSk7XG4gICAgICAgICAgICAgICAgICB9KS5mYWlsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnaW1hZ2UudXBsb2FkLmVycm9yJyk7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogaW5zZXJ0SW1hZ2VzT3JDYWxsYmFja1xuICAgICAgICogQHBhcmFtIHtGaWxlW119IGZpbGVzXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuaW5zZXJ0SW1hZ2VzT3JDYWxsYmFjayA9IGZ1bmN0aW9uIChmaWxlcykge1xuICAgICAgICAgIHZhciBjYWxsYmFja3MgPSB0aGlzLm9wdGlvbnMuY2FsbGJhY2tzO1xuICAgICAgICAgIC8vIElmIG9uSW1hZ2VVcGxvYWQgc2V0LFxuICAgICAgICAgIGlmIChjYWxsYmFja3Mub25JbWFnZVVwbG9hZCkge1xuICAgICAgICAgICAgICB0aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdpbWFnZS51cGxvYWQnLCBmaWxlcyk7XG4gICAgICAgICAgICAgIC8vIGVsc2UgaW5zZXJ0IEltYWdlIGFzIGRhdGFVUkxcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuaW5zZXJ0SW1hZ2VzQXNEYXRhVVJMKGZpbGVzKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiByZXR1cm4gc2VsZWN0ZWQgcGxhaW4gdGV4dFxuICAgICAgICogQHJldHVybiB7U3RyaW5nfSB0ZXh0XG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuZ2V0U2VsZWN0ZWRUZXh0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBybmcgPSB0aGlzLmdldExhc3RSYW5nZSgpO1xuICAgICAgICAgIC8vIGlmIHJhbmdlIG9uIGFuY2hvciwgZXhwYW5kIHJhbmdlIHdpdGggYW5jaG9yXG4gICAgICAgICAgaWYgKHJuZy5pc09uQW5jaG9yKCkpIHtcbiAgICAgICAgICAgICAgcm5nID0gcmFuZ2UuY3JlYXRlRnJvbU5vZGUoZG9tLmFuY2VzdG9yKHJuZy5zYywgZG9tLmlzQW5jaG9yKSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBybmcudG9TdHJpbmcoKTtcbiAgICAgIH07XG4gICAgICBFZGl0b3IucHJvdG90eXBlLm9uRm9ybWF0QmxvY2sgPSBmdW5jdGlvbiAodGFnTmFtZSwgJHRhcmdldCkge1xuICAgICAgICAgIC8vIFt3b3JrYXJvdW5kXSBmb3IgTVNJRSwgSUUgbmVlZCBgPGBcbiAgICAgICAgICBkb2N1bWVudC5leGVjQ29tbWFuZCgnRm9ybWF0QmxvY2snLCBmYWxzZSwgZW52LmlzTVNJRSA/ICc8JyArIHRhZ05hbWUgKyAnPicgOiB0YWdOYW1lKTtcbiAgICAgICAgICAvLyBzdXBwb3J0IGN1c3RvbSBjbGFzc1xuICAgICAgICAgIGlmICgkdGFyZ2V0ICYmICR0YXJnZXQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgIC8vIGZpbmQgdGhlIGV4YWN0IGVsZW1lbnQgaGFzIGdpdmVuIHRhZ05hbWVcbiAgICAgICAgICAgICAgaWYgKCR0YXJnZXRbMF0udGFnTmFtZS50b1VwcGVyQ2FzZSgpICE9PSB0YWdOYW1lLnRvVXBwZXJDYXNlKCkpIHtcbiAgICAgICAgICAgICAgICAgICR0YXJnZXQgPSAkdGFyZ2V0LmZpbmQodGFnTmFtZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgaWYgKCR0YXJnZXQgJiYgJHRhcmdldC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBjbGFzc05hbWUgPSAkdGFyZ2V0WzBdLmNsYXNzTmFtZSB8fCAnJztcbiAgICAgICAgICAgICAgICAgIGlmIChjbGFzc05hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgY3VycmVudFJhbmdlID0gdGhpcy5jcmVhdGVSYW5nZSgpO1xuICAgICAgICAgICAgICAgICAgICAgIHZhciAkcGFyZW50ID0gJCQxKFtjdXJyZW50UmFuZ2Uuc2MsIGN1cnJlbnRSYW5nZS5lY10pLmNsb3Nlc3QodGFnTmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgJHBhcmVudC5hZGRDbGFzcyhjbGFzc05hbWUpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIEVkaXRvci5wcm90b3R5cGUuZm9ybWF0UGFyYSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLmZvcm1hdEJsb2NrKCdQJyk7XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5mb250U3R5bGluZyA9IGZ1bmN0aW9uICh0YXJnZXQsIHZhbHVlKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMuZ2V0TGFzdFJhbmdlKCk7XG4gICAgICAgICAgaWYgKHJuZykge1xuICAgICAgICAgICAgICB2YXIgc3BhbnMgPSB0aGlzLnN0eWxlLnN0eWxlTm9kZXMocm5nKTtcbiAgICAgICAgICAgICAgJCQxKHNwYW5zKS5jc3ModGFyZ2V0LCB2YWx1ZSk7XG4gICAgICAgICAgICAgIC8vIFt3b3JrYXJvdW5kXSBhZGRlZCBzdHlsZWQgYm9ndXMgc3BhbiBmb3Igc3R5bGVcbiAgICAgICAgICAgICAgLy8gIC0gYWxzbyBib2d1cyBjaGFyYWN0ZXIgbmVlZGVkIGZvciBjdXJzb3IgcG9zaXRpb25cbiAgICAgICAgICAgICAgaWYgKHJuZy5pc0NvbGxhcHNlZCgpKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgZmlyc3RTcGFuID0gbGlzdHMuaGVhZChzcGFucyk7XG4gICAgICAgICAgICAgICAgICBpZiAoZmlyc3RTcGFuICYmICFkb20ubm9kZUxlbmd0aChmaXJzdFNwYW4pKSB7XG4gICAgICAgICAgICAgICAgICAgICAgZmlyc3RTcGFuLmlubmVySFRNTCA9IGRvbS5aRVJPX1dJRFRIX05CU1BfQ0hBUjtcbiAgICAgICAgICAgICAgICAgICAgICByYW5nZS5jcmVhdGVGcm9tTm9kZUFmdGVyKGZpcnN0U3Bhbi5maXJzdENoaWxkKS5zZWxlY3QoKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldExhc3RSYW5nZSgpO1xuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmRhdGEoS0VZX0JPR1VTLCBmaXJzdFNwYW4pO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogdW5saW5rXG4gICAgICAgKlxuICAgICAgICogQHR5cGUgY29tbWFuZFxuICAgICAgICovXG4gICAgICBFZGl0b3IucHJvdG90eXBlLnVubGluayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy5nZXRMYXN0UmFuZ2UoKTtcbiAgICAgICAgICBpZiAocm5nLmlzT25BbmNob3IoKSkge1xuICAgICAgICAgICAgICB2YXIgYW5jaG9yID0gZG9tLmFuY2VzdG9yKHJuZy5zYywgZG9tLmlzQW5jaG9yKTtcbiAgICAgICAgICAgICAgcm5nID0gcmFuZ2UuY3JlYXRlRnJvbU5vZGUoYW5jaG9yKTtcbiAgICAgICAgICAgICAgcm5nLnNlbGVjdCgpO1xuICAgICAgICAgICAgICB0aGlzLnNldExhc3RSYW5nZSgpO1xuICAgICAgICAgICAgICB0aGlzLmJlZm9yZUNvbW1hbmQoKTtcbiAgICAgICAgICAgICAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ3VubGluaycpO1xuICAgICAgICAgICAgICB0aGlzLmFmdGVyQ29tbWFuZCgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHJldHVybnMgbGluayBpbmZvXG4gICAgICAgKlxuICAgICAgICogQHJldHVybiB7T2JqZWN0fVxuICAgICAgICogQHJldHVybiB7V3JhcHBlZFJhbmdlfSByZXR1cm4ucmFuZ2VcbiAgICAgICAqIEByZXR1cm4ge1N0cmluZ30gcmV0dXJuLnRleHRcbiAgICAgICAqIEByZXR1cm4ge0Jvb2xlYW59IFtyZXR1cm4uaXNOZXdXaW5kb3c9dHJ1ZV1cbiAgICAgICAqIEByZXR1cm4ge1N0cmluZ30gW3JldHVybi51cmw9XCJcIl1cbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5nZXRMaW5rSW5mbyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy5nZXRMYXN0UmFuZ2UoKS5leHBhbmQoZG9tLmlzQW5jaG9yKTtcbiAgICAgICAgICAvLyBHZXQgdGhlIGZpcnN0IGFuY2hvciBvbiByYW5nZShmb3IgZWRpdCkuXG4gICAgICAgICAgdmFyICRhbmNob3IgPSAkJDEobGlzdHMuaGVhZChybmcubm9kZXMoZG9tLmlzQW5jaG9yKSkpO1xuICAgICAgICAgIHZhciBsaW5rSW5mbyA9IHtcbiAgICAgICAgICAgICAgcmFuZ2U6IHJuZyxcbiAgICAgICAgICAgICAgdGV4dDogcm5nLnRvU3RyaW5nKCksXG4gICAgICAgICAgICAgIHVybDogJGFuY2hvci5sZW5ndGggPyAkYW5jaG9yLmF0dHIoJ2hyZWYnKSA6ICcnXG4gICAgICAgICAgfTtcbiAgICAgICAgICAvLyBXaGVuIGFuY2hvciBleGlzdHMsXG4gICAgICAgICAgaWYgKCRhbmNob3IubGVuZ3RoKSB7XG4gICAgICAgICAgICAgIC8vIFNldCBpc05ld1dpbmRvdyBieSBjaGVja2luZyBpdHMgdGFyZ2V0LlxuICAgICAgICAgICAgICBsaW5rSW5mby5pc05ld1dpbmRvdyA9ICRhbmNob3IuYXR0cigndGFyZ2V0JykgPT09ICdfYmxhbmsnO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gbGlua0luZm87XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5hZGRSb3cgPSBmdW5jdGlvbiAocG9zaXRpb24pIHtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy5nZXRMYXN0UmFuZ2UodGhpcy4kZWRpdGFibGUpO1xuICAgICAgICAgIGlmIChybmcuaXNDb2xsYXBzZWQoKSAmJiBybmcuaXNPbkNlbGwoKSkge1xuICAgICAgICAgICAgICB0aGlzLmJlZm9yZUNvbW1hbmQoKTtcbiAgICAgICAgICAgICAgdGhpcy50YWJsZS5hZGRSb3cocm5nLCBwb3NpdGlvbik7XG4gICAgICAgICAgICAgIHRoaXMuYWZ0ZXJDb21tYW5kKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIEVkaXRvci5wcm90b3R5cGUuYWRkQ29sID0gZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMuZ2V0TGFzdFJhbmdlKHRoaXMuJGVkaXRhYmxlKTtcbiAgICAgICAgICBpZiAocm5nLmlzQ29sbGFwc2VkKCkgJiYgcm5nLmlzT25DZWxsKCkpIHtcbiAgICAgICAgICAgICAgdGhpcy5iZWZvcmVDb21tYW5kKCk7XG4gICAgICAgICAgICAgIHRoaXMudGFibGUuYWRkQ29sKHJuZywgcG9zaXRpb24pO1xuICAgICAgICAgICAgICB0aGlzLmFmdGVyQ29tbWFuZCgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBFZGl0b3IucHJvdG90eXBlLmRlbGV0ZVJvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgcm5nID0gdGhpcy5nZXRMYXN0UmFuZ2UodGhpcy4kZWRpdGFibGUpO1xuICAgICAgICAgIGlmIChybmcuaXNDb2xsYXBzZWQoKSAmJiBybmcuaXNPbkNlbGwoKSkge1xuICAgICAgICAgICAgICB0aGlzLmJlZm9yZUNvbW1hbmQoKTtcbiAgICAgICAgICAgICAgdGhpcy50YWJsZS5kZWxldGVSb3cocm5nKTtcbiAgICAgICAgICAgICAgdGhpcy5hZnRlckNvbW1hbmQoKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5kZWxldGVDb2wgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMuZ2V0TGFzdFJhbmdlKHRoaXMuJGVkaXRhYmxlKTtcbiAgICAgICAgICBpZiAocm5nLmlzQ29sbGFwc2VkKCkgJiYgcm5nLmlzT25DZWxsKCkpIHtcbiAgICAgICAgICAgICAgdGhpcy5iZWZvcmVDb21tYW5kKCk7XG4gICAgICAgICAgICAgIHRoaXMudGFibGUuZGVsZXRlQ29sKHJuZyk7XG4gICAgICAgICAgICAgIHRoaXMuYWZ0ZXJDb21tYW5kKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIEVkaXRvci5wcm90b3R5cGUuZGVsZXRlVGFibGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIHJuZyA9IHRoaXMuZ2V0TGFzdFJhbmdlKHRoaXMuJGVkaXRhYmxlKTtcbiAgICAgICAgICBpZiAocm5nLmlzQ29sbGFwc2VkKCkgJiYgcm5nLmlzT25DZWxsKCkpIHtcbiAgICAgICAgICAgICAgdGhpcy5iZWZvcmVDb21tYW5kKCk7XG4gICAgICAgICAgICAgIHRoaXMudGFibGUuZGVsZXRlVGFibGUocm5nKTtcbiAgICAgICAgICAgICAgdGhpcy5hZnRlckNvbW1hbmQoKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBAcGFyYW0ge1Bvc2l0aW9ufSBwb3NcbiAgICAgICAqIEBwYXJhbSB7alF1ZXJ5fSAkdGFyZ2V0IC0gdGFyZ2V0IGVsZW1lbnRcbiAgICAgICAqIEBwYXJhbSB7Qm9vbGVhbn0gW2JLZWVwUmF0aW9dIC0ga2VlcCByYXRpb1xuICAgICAgICovXG4gICAgICBFZGl0b3IucHJvdG90eXBlLnJlc2l6ZVRvID0gZnVuY3Rpb24gKHBvcywgJHRhcmdldCwgYktlZXBSYXRpbykge1xuICAgICAgICAgIHZhciBpbWFnZVNpemU7XG4gICAgICAgICAgaWYgKGJLZWVwUmF0aW8pIHtcbiAgICAgICAgICAgICAgdmFyIG5ld1JhdGlvID0gcG9zLnkgLyBwb3MueDtcbiAgICAgICAgICAgICAgdmFyIHJhdGlvID0gJHRhcmdldC5kYXRhKCdyYXRpbycpO1xuICAgICAgICAgICAgICBpbWFnZVNpemUgPSB7XG4gICAgICAgICAgICAgICAgICB3aWR0aDogcmF0aW8gPiBuZXdSYXRpbyA/IHBvcy54IDogcG9zLnkgLyByYXRpbyxcbiAgICAgICAgICAgICAgICAgIGhlaWdodDogcmF0aW8gPiBuZXdSYXRpbyA/IHBvcy54ICogcmF0aW8gOiBwb3MueVxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgaW1hZ2VTaXplID0ge1xuICAgICAgICAgICAgICAgICAgd2lkdGg6IHBvcy54LFxuICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBwb3MueVxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICAkdGFyZ2V0LmNzcyhpbWFnZVNpemUpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmV0dXJucyB3aGV0aGVyIGVkaXRhYmxlIGFyZWEgaGFzIGZvY3VzIG9yIG5vdC5cbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5oYXNGb2N1cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy4kZWRpdGFibGUuaXMoJzpmb2N1cycpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogc2V0IGZvY3VzXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuZm9jdXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgLy8gW3dvcmthcm91bmRdIFNjcmVlbiB3aWxsIG1vdmUgd2hlbiBwYWdlIGlzIHNjb2xsZWQgaW4gSUUuXG4gICAgICAgICAgLy8gIC0gZG8gZm9jdXMgd2hlbiBub3QgZm9jdXNlZFxuICAgICAgICAgIGlmICghdGhpcy5oYXNGb2N1cygpKSB7XG4gICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmZvY3VzKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcmV0dXJucyB3aGV0aGVyIGNvbnRlbnRzIGlzIGVtcHR5IG9yIG5vdC5cbiAgICAgICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuaXNFbXB0eSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gZG9tLmlzRW1wdHkodGhpcy4kZWRpdGFibGVbMF0pIHx8IGRvbS5lbXB0eVBhcmEgPT09IHRoaXMuJGVkaXRhYmxlLmh0bWwoKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIFJlbW92ZXMgYWxsIGNvbnRlbnRzIGFuZCByZXN0b3JlcyB0aGUgZWRpdGFibGUgaW5zdGFuY2UgdG8gYW4gX2VtcHR5UGFyYV8uXG4gICAgICAgKi9cbiAgICAgIEVkaXRvci5wcm90b3R5cGUuZW1wdHkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnY29kZScsIGRvbS5lbXB0eVBhcmEpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogbm9ybWFsaXplIGNvbnRlbnRcbiAgICAgICAqL1xuICAgICAgRWRpdG9yLnByb3RvdHlwZS5ub3JtYWxpemVDb250ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlWzBdLm5vcm1hbGl6ZSgpO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBFZGl0b3I7XG4gIH0oKSk7XG5cbiAgdmFyIENsaXBib2FyZCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIENsaXBib2FyZChjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZSA9IGNvbnRleHQubGF5b3V0SW5mby5lZGl0YWJsZTtcbiAgICAgIH1cbiAgICAgIENsaXBib2FyZC5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5vbigncGFzdGUnLCB0aGlzLnBhc3RlQnlFdmVudC5iaW5kKHRoaXMpKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHBhc3RlIGJ5IGNsaXBib2FyZCBldmVudFxuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XG4gICAgICAgKi9cbiAgICAgIENsaXBib2FyZC5wcm90b3R5cGUucGFzdGVCeUV2ZW50ID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgdmFyIGNsaXBib2FyZERhdGEgPSBldmVudC5vcmlnaW5hbEV2ZW50LmNsaXBib2FyZERhdGE7XG4gICAgICAgICAgaWYgKGNsaXBib2FyZERhdGEgJiYgY2xpcGJvYXJkRGF0YS5pdGVtcyAmJiBjbGlwYm9hcmREYXRhLml0ZW1zLmxlbmd0aCkge1xuICAgICAgICAgICAgICAvLyBwYXN0ZSBpbWcgZmlsZVxuICAgICAgICAgICAgICB2YXIgaXRlbSA9IGNsaXBib2FyZERhdGEuaXRlbXMubGVuZ3RoID4gMSA/IGNsaXBib2FyZERhdGEuaXRlbXNbMV0gOiBsaXN0cy5oZWFkKGNsaXBib2FyZERhdGEuaXRlbXMpO1xuICAgICAgICAgICAgICBpZiAoaXRlbS5raW5kID09PSAnZmlsZScgJiYgaXRlbS50eXBlLmluZGV4T2YoJ2ltYWdlLycpICE9PSAtMSkge1xuICAgICAgICAgICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmluc2VydEltYWdlc09yQ2FsbGJhY2snLCBbaXRlbS5nZXRBc0ZpbGUoKV0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5hZnRlckNvbW1hbmQnKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgcmV0dXJuIENsaXBib2FyZDtcbiAgfSgpKTtcblxuICB2YXIgRHJvcHpvbmUgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBEcm9wem9uZShjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLiRldmVudExpc3RlbmVyID0gJCQxKGRvY3VtZW50KTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRhYmxlO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICAgICAgdGhpcy5kb2N1bWVudEV2ZW50SGFuZGxlcnMgPSB7fTtcbiAgICAgICAgICB0aGlzLiRkcm9wem9uZSA9ICQkMShbXG4gICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibm90ZS1kcm9wem9uZVwiPicsXG4gICAgICAgICAgICAgICcgIDxkaXYgY2xhc3M9XCJub3RlLWRyb3B6b25lLW1lc3NhZ2VcIi8+JyxcbiAgICAgICAgICAgICAgJzwvZGl2PicsXG4gICAgICAgICAgXS5qb2luKCcnKSkucHJlcGVuZFRvKHRoaXMuJGVkaXRvcik7XG4gICAgICB9XG4gICAgICAvKipcbiAgICAgICAqIGF0dGFjaCBEcmFnIGFuZCBEcm9wIEV2ZW50c1xuICAgICAgICovXG4gICAgICBEcm9wem9uZS5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmRpc2FibGVEcmFnQW5kRHJvcCkge1xuICAgICAgICAgICAgICAvLyBwcmV2ZW50IGRlZmF1bHQgZHJvcCBldmVudFxuICAgICAgICAgICAgICB0aGlzLmRvY3VtZW50RXZlbnRIYW5kbGVycy5vbkRyb3AgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAvLyBkbyBub3QgY29uc2lkZXIgb3V0c2lkZSBvZiBkcm9wem9uZVxuICAgICAgICAgICAgICB0aGlzLiRldmVudExpc3RlbmVyID0gdGhpcy4kZHJvcHpvbmU7XG4gICAgICAgICAgICAgIHRoaXMuJGV2ZW50TGlzdGVuZXIub24oJ2Ryb3AnLCB0aGlzLmRvY3VtZW50RXZlbnRIYW5kbGVycy5vbkRyb3ApO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdGhpcy5hdHRhY2hEcmFnQW5kRHJvcEV2ZW50KCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogYXR0YWNoIERyYWcgYW5kIERyb3AgRXZlbnRzXG4gICAgICAgKi9cbiAgICAgIERyb3B6b25lLnByb3RvdHlwZS5hdHRhY2hEcmFnQW5kRHJvcEV2ZW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdmFyIGNvbGxlY3Rpb24gPSAkJDEoKTtcbiAgICAgICAgICB2YXIgJGRyb3B6b25lTWVzc2FnZSA9IHRoaXMuJGRyb3B6b25lLmZpbmQoJy5ub3RlLWRyb3B6b25lLW1lc3NhZ2UnKTtcbiAgICAgICAgICB0aGlzLmRvY3VtZW50RXZlbnRIYW5kbGVycy5vbkRyYWdlbnRlciA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgIHZhciBpc0NvZGV2aWV3ID0gX3RoaXMuY29udGV4dC5pbnZva2UoJ2NvZGV2aWV3LmlzQWN0aXZhdGVkJyk7XG4gICAgICAgICAgICAgIHZhciBoYXNFZGl0b3JTaXplID0gX3RoaXMuJGVkaXRvci53aWR0aCgpID4gMCAmJiBfdGhpcy4kZWRpdG9yLmhlaWdodCgpID4gMDtcbiAgICAgICAgICAgICAgaWYgKCFpc0NvZGV2aWV3ICYmICFjb2xsZWN0aW9uLmxlbmd0aCAmJiBoYXNFZGl0b3JTaXplKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy4kZWRpdG9yLmFkZENsYXNzKCdkcmFnb3ZlcicpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuJGRyb3B6b25lLndpZHRoKF90aGlzLiRlZGl0b3Iud2lkdGgoKSk7XG4gICAgICAgICAgICAgICAgICBfdGhpcy4kZHJvcHpvbmUuaGVpZ2h0KF90aGlzLiRlZGl0b3IuaGVpZ2h0KCkpO1xuICAgICAgICAgICAgICAgICAgJGRyb3B6b25lTWVzc2FnZS50ZXh0KF90aGlzLmxhbmcuaW1hZ2UuZHJhZ0ltYWdlSGVyZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY29sbGVjdGlvbiA9IGNvbGxlY3Rpb24uYWRkKGUudGFyZ2V0KTtcbiAgICAgICAgICB9O1xuICAgICAgICAgIHRoaXMuZG9jdW1lbnRFdmVudEhhbmRsZXJzLm9uRHJhZ2xlYXZlID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICAgY29sbGVjdGlvbiA9IGNvbGxlY3Rpb24ubm90KGUudGFyZ2V0KTtcbiAgICAgICAgICAgICAgaWYgKCFjb2xsZWN0aW9uLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuJGVkaXRvci5yZW1vdmVDbGFzcygnZHJhZ292ZXInKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICAgICAgdGhpcy5kb2N1bWVudEV2ZW50SGFuZGxlcnMub25Ecm9wID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBjb2xsZWN0aW9uID0gJCQxKCk7XG4gICAgICAgICAgICAgIF90aGlzLiRlZGl0b3IucmVtb3ZlQ2xhc3MoJ2RyYWdvdmVyJyk7XG4gICAgICAgICAgfTtcbiAgICAgICAgICAvLyBzaG93IGRyb3B6b25lIG9uIGRyYWdlbnRlciB3aGVuIGRyYWdnaW5nIGEgb2JqZWN0IHRvIGRvY3VtZW50XG4gICAgICAgICAgLy8gLWJ1dCBvbmx5IGlmIHRoZSBlZGl0b3IgaXMgdmlzaWJsZSwgaS5lLiBoYXMgYSBwb3NpdGl2ZSB3aWR0aCBhbmQgaGVpZ2h0XG4gICAgICAgICAgdGhpcy4kZXZlbnRMaXN0ZW5lci5vbignZHJhZ2VudGVyJywgdGhpcy5kb2N1bWVudEV2ZW50SGFuZGxlcnMub25EcmFnZW50ZXIpXG4gICAgICAgICAgICAgIC5vbignZHJhZ2xlYXZlJywgdGhpcy5kb2N1bWVudEV2ZW50SGFuZGxlcnMub25EcmFnbGVhdmUpXG4gICAgICAgICAgICAgIC5vbignZHJvcCcsIHRoaXMuZG9jdW1lbnRFdmVudEhhbmRsZXJzLm9uRHJvcCk7XG4gICAgICAgICAgLy8gY2hhbmdlIGRyb3B6b25lJ3MgbWVzc2FnZSBvbiBob3Zlci5cbiAgICAgICAgICB0aGlzLiRkcm9wem9uZS5vbignZHJhZ2VudGVyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICBfdGhpcy4kZHJvcHpvbmUuYWRkQ2xhc3MoJ2hvdmVyJyk7XG4gICAgICAgICAgICAgICRkcm9wem9uZU1lc3NhZ2UudGV4dChfdGhpcy5sYW5nLmltYWdlLmRyb3BJbWFnZSk7XG4gICAgICAgICAgfSkub24oJ2RyYWdsZWF2ZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuJGRyb3B6b25lLnJlbW92ZUNsYXNzKCdob3ZlcicpO1xuICAgICAgICAgICAgICAkZHJvcHpvbmVNZXNzYWdlLnRleHQoX3RoaXMubGFuZy5pbWFnZS5kcmFnSW1hZ2VIZXJlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyBhdHRhY2ggZHJvcEltYWdlXG4gICAgICAgICAgdGhpcy4kZHJvcHpvbmUub24oJ2Ryb3AnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgdmFyIGRhdGFUcmFuc2ZlciA9IGV2ZW50Lm9yaWdpbmFsRXZlbnQuZGF0YVRyYW5zZmVyO1xuICAgICAgICAgICAgICAvLyBzdG9wIHRoZSBicm93c2VyIGZyb20gb3BlbmluZyB0aGUgZHJvcHBlZCBjb250ZW50XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgIGlmIChkYXRhVHJhbnNmZXIgJiYgZGF0YVRyYW5zZmVyLmZpbGVzICYmIGRhdGFUcmFuc2Zlci5maWxlcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLiRlZGl0YWJsZS5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5pbnNlcnRJbWFnZXNPckNhbGxiYWNrJywgZGF0YVRyYW5zZmVyLmZpbGVzKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICQkMS5lYWNoKGRhdGFUcmFuc2Zlci50eXBlcywgZnVuY3Rpb24gKGlkeCwgdHlwZSkge1xuICAgICAgICAgICAgICAgICAgICAgIHZhciBjb250ZW50ID0gZGF0YVRyYW5zZmVyLmdldERhdGEodHlwZSk7XG4gICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGUudG9Mb3dlckNhc2UoKS5pbmRleE9mKCd0ZXh0JykgPiAtMSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLnBhc3RlSFRNTCcsIGNvbnRlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJCQxKGNvbnRlbnQpLmVhY2goZnVuY3Rpb24gKGlkeCwgaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5pbnNlcnROb2RlJywgaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSkub24oJ2RyYWdvdmVyJywgZmFsc2UpOyAvLyBwcmV2ZW50IGRlZmF1bHQgZHJhZ292ZXIgZXZlbnRcbiAgICAgIH07XG4gICAgICBEcm9wem9uZS5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIE9iamVjdC5rZXlzKHRoaXMuZG9jdW1lbnRFdmVudEhhbmRsZXJzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICAgICAgX3RoaXMuJGV2ZW50TGlzdGVuZXIub2ZmKGtleS5zdWJzdHIoMikudG9Mb3dlckNhc2UoKSwgX3RoaXMuZG9jdW1lbnRFdmVudEhhbmRsZXJzW2tleV0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuZG9jdW1lbnRFdmVudEhhbmRsZXJzID0ge307XG4gICAgICB9O1xuICAgICAgcmV0dXJuIERyb3B6b25lO1xuICB9KCkpO1xuXG4gIHZhciBDb2RlTWlycm9yO1xuICBpZiAoZW52Lmhhc0NvZGVNaXJyb3IpIHtcbiAgICAgIENvZGVNaXJyb3IgPSB3aW5kb3cuQ29kZU1pcnJvcjtcbiAgfVxuICAvKipcbiAgICogQGNsYXNzIENvZGV2aWV3XG4gICAqL1xuICB2YXIgQ29kZVZpZXcgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBDb2RlVmlldyhjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRhYmxlO1xuICAgICAgICAgIHRoaXMuJGNvZGFibGUgPSBjb250ZXh0LmxheW91dEluZm8uY29kYWJsZTtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICB9XG4gICAgICBDb2RlVmlldy5wcm90b3R5cGUuc3luYyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgaXNDb2RldmlldyA9IHRoaXMuaXNBY3RpdmF0ZWQoKTtcbiAgICAgICAgICBpZiAoaXNDb2RldmlldyAmJiBlbnYuaGFzQ29kZU1pcnJvcikge1xuICAgICAgICAgICAgICB0aGlzLiRjb2RhYmxlLmRhdGEoJ2NtRWRpdG9yJykuc2F2ZSgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIEByZXR1cm4ge0Jvb2xlYW59XG4gICAgICAgKi9cbiAgICAgIENvZGVWaWV3LnByb3RvdHlwZS5pc0FjdGl2YXRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy4kZWRpdG9yLmhhc0NsYXNzKCdjb2RldmlldycpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogdG9nZ2xlIGNvZGV2aWV3XG4gICAgICAgKi9cbiAgICAgIENvZGVWaWV3LnByb3RvdHlwZS50b2dnbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgaWYgKHRoaXMuaXNBY3RpdmF0ZWQoKSkge1xuICAgICAgICAgICAgICB0aGlzLmRlYWN0aXZhdGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuYWN0aXZhdGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnY29kZXZpZXcudG9nZ2xlZCcpO1xuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogcHVyaWZ5IGlucHV0IHZhbHVlXG4gICAgICAgKiBAcGFyYW0gdmFsdWVcbiAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICovXG4gICAgICBDb2RlVmlldy5wcm90b3R5cGUucHVyaWZ5ID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb2Rldmlld0ZpbHRlcikge1xuICAgICAgICAgICAgICAvLyBmaWx0ZXIgY29kZSB2aWV3IHJlZ2V4XG4gICAgICAgICAgICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSh0aGlzLm9wdGlvbnMuY29kZXZpZXdGaWx0ZXJSZWdleCwgJycpO1xuICAgICAgICAgICAgICAvLyBhbGxvdyBzcGVjaWZpYyBpZnJhbWUgdGFnXG4gICAgICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuY29kZXZpZXdJZnJhbWVGaWx0ZXIpIHtcbiAgICAgICAgICAgICAgICAgIHZhciB3aGl0ZWxpc3RfMSA9IHRoaXMub3B0aW9ucy5jb2Rldmlld0lmcmFtZVdoaXRlbGlzdFNyYy5jb25jYXQodGhpcy5vcHRpb25zLmNvZGV2aWV3SWZyYW1lV2hpdGVsaXN0U3JjQmFzZSk7XG4gICAgICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoLyg8aWZyYW1lLio/Pi4qPyg/OjxcXC9pZnJhbWU+KT8pL2dpLCBmdW5jdGlvbiAodGFnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gcmVtb3ZlIGlmIHNyYyBhdHRyaWJ1dGUgaXMgZHVwbGljYXRlZFxuICAgICAgICAgICAgICAgICAgICAgIGlmICgvPC4rc3JjKD89PT8oJ3xcInxcXHMpPylbXFxzXFxTXStzcmMoPz0oJ3xcInxcXHMpPylbXj5dKj8+L2kudGVzdCh0YWcpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnJztcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwLCB3aGl0ZWxpc3RfMiA9IHdoaXRlbGlzdF8xOyBfaSA8IHdoaXRlbGlzdF8yLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3JjID0gd2hpdGVsaXN0XzJbX2ldO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBwYXNzIGlmIHNyYyBpcyB0cnVzdGVkXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmICgobmV3IFJlZ0V4cCgnc3JjPVwiKGh0dHBzPzopP1xcL1xcLycgKyBzcmMucmVwbGFjZSgvWy1cXC9cXFxcXiQqKz8uKCl8W1xcXXt9XS9nLCAnXFxcXCQmJykgKyAnXFwvKC4rKVwiJykpLnRlc3QodGFnKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRhZztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJyc7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gdmFsdWU7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBhY3RpdmF0ZSBjb2RlIHZpZXdcbiAgICAgICAqL1xuICAgICAgQ29kZVZpZXcucHJvdG90eXBlLmFjdGl2YXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy4kY29kYWJsZS52YWwoZG9tLmh0bWwodGhpcy4kZWRpdGFibGUsIHRoaXMub3B0aW9ucy5wcmV0dGlmeUh0bWwpKTtcbiAgICAgICAgICB0aGlzLiRjb2RhYmxlLmhlaWdodCh0aGlzLiRlZGl0YWJsZS5oZWlnaHQoKSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgndG9vbGJhci51cGRhdGVDb2RldmlldycsIHRydWUpO1xuICAgICAgICAgIHRoaXMuJGVkaXRvci5hZGRDbGFzcygnY29kZXZpZXcnKTtcbiAgICAgICAgICB0aGlzLiRjb2RhYmxlLmZvY3VzKCk7XG4gICAgICAgICAgLy8gYWN0aXZhdGUgQ29kZU1pcnJvciBhcyBjb2RhYmxlXG4gICAgICAgICAgaWYgKGVudi5oYXNDb2RlTWlycm9yKSB7XG4gICAgICAgICAgICAgIHZhciBjbUVkaXRvcl8xID0gQ29kZU1pcnJvci5mcm9tVGV4dEFyZWEodGhpcy4kY29kYWJsZVswXSwgdGhpcy5vcHRpb25zLmNvZGVtaXJyb3IpO1xuICAgICAgICAgICAgICAvLyBDb2RlTWlycm9yIFRlcm5TZXJ2ZXJcbiAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5jb2RlbWlycm9yLnRlcm4pIHtcbiAgICAgICAgICAgICAgICAgIHZhciBzZXJ2ZXJfMSA9IG5ldyBDb2RlTWlycm9yLlRlcm5TZXJ2ZXIodGhpcy5vcHRpb25zLmNvZGVtaXJyb3IudGVybik7XG4gICAgICAgICAgICAgICAgICBjbUVkaXRvcl8xLnRlcm5TZXJ2ZXIgPSBzZXJ2ZXJfMTtcbiAgICAgICAgICAgICAgICAgIGNtRWRpdG9yXzEub24oJ2N1cnNvckFjdGl2aXR5JywgZnVuY3Rpb24gKGNtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgc2VydmVyXzEudXBkYXRlQXJnSGludHMoY20pO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY21FZGl0b3JfMS5vbignYmx1cicsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2JsdXIuY29kZXZpZXcnLCBjbUVkaXRvcl8xLmdldFZhbHVlKCksIGV2ZW50KTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIGNtRWRpdG9yXzEub24oJ2NoYW5nZScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2NoYW5nZS5jb2RldmlldycsIGNtRWRpdG9yXzEuZ2V0VmFsdWUoKSwgY21FZGl0b3JfMSk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAvLyBDb2RlTWlycm9yIGhhc24ndCBQYWRkaW5nLlxuICAgICAgICAgICAgICBjbUVkaXRvcl8xLnNldFNpemUobnVsbCwgdGhpcy4kZWRpdGFibGUub3V0ZXJIZWlnaHQoKSk7XG4gICAgICAgICAgICAgIHRoaXMuJGNvZGFibGUuZGF0YSgnY21FZGl0b3InLCBjbUVkaXRvcl8xKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuJGNvZGFibGUub24oJ2JsdXInLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdibHVyLmNvZGV2aWV3JywgX3RoaXMuJGNvZGFibGUudmFsKCksIGV2ZW50KTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHRoaXMuJGNvZGFibGUub24oJ2lucHV0JywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnY2hhbmdlLmNvZGV2aWV3JywgX3RoaXMuJGNvZGFibGUudmFsKCksIF90aGlzLiRjb2RhYmxlKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIC8qKlxuICAgICAgICogZGVhY3RpdmF0ZSBjb2RlIHZpZXdcbiAgICAgICAqL1xuICAgICAgQ29kZVZpZXcucHJvdG90eXBlLmRlYWN0aXZhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgLy8gZGVhY3RpdmF0ZSBDb2RlTWlycm9yIGFzIGNvZGFibGVcbiAgICAgICAgICBpZiAoZW52Lmhhc0NvZGVNaXJyb3IpIHtcbiAgICAgICAgICAgICAgdmFyIGNtRWRpdG9yID0gdGhpcy4kY29kYWJsZS5kYXRhKCdjbUVkaXRvcicpO1xuICAgICAgICAgICAgICB0aGlzLiRjb2RhYmxlLnZhbChjbUVkaXRvci5nZXRWYWx1ZSgpKTtcbiAgICAgICAgICAgICAgY21FZGl0b3IudG9UZXh0QXJlYSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgdmFsdWUgPSB0aGlzLnB1cmlmeShkb20udmFsdWUodGhpcy4kY29kYWJsZSwgdGhpcy5vcHRpb25zLnByZXR0aWZ5SHRtbCkgfHwgZG9tLmVtcHR5UGFyYSk7XG4gICAgICAgICAgdmFyIGlzQ2hhbmdlID0gdGhpcy4kZWRpdGFibGUuaHRtbCgpICE9PSB2YWx1ZTtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5odG1sKHZhbHVlKTtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5oZWlnaHQodGhpcy5vcHRpb25zLmhlaWdodCA/IHRoaXMuJGNvZGFibGUuaGVpZ2h0KCkgOiAnYXV0bycpO1xuICAgICAgICAgIHRoaXMuJGVkaXRvci5yZW1vdmVDbGFzcygnY29kZXZpZXcnKTtcbiAgICAgICAgICBpZiAoaXNDaGFuZ2UpIHtcbiAgICAgICAgICAgICAgdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnY2hhbmdlJywgdGhpcy4kZWRpdGFibGUuaHRtbCgpLCB0aGlzLiRlZGl0YWJsZSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmZvY3VzKCk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgndG9vbGJhci51cGRhdGVDb2RldmlldycsIGZhbHNlKTtcbiAgICAgIH07XG4gICAgICBDb2RlVmlldy5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAodGhpcy5pc0FjdGl2YXRlZCgpKSB7XG4gICAgICAgICAgICAgIHRoaXMuZGVhY3RpdmF0ZSgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICByZXR1cm4gQ29kZVZpZXc7XG4gIH0oKSk7XG5cbiAgdmFyIEVESVRBQkxFX1BBRERJTkcgPSAyNDtcbiAgdmFyIFN0YXR1c2JhciA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIFN0YXR1c2Jhcihjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy4kZG9jdW1lbnQgPSAkJDEoZG9jdW1lbnQpO1xuICAgICAgICAgIHRoaXMuJHN0YXR1c2JhciA9IGNvbnRleHQubGF5b3V0SW5mby5zdGF0dXNiYXI7XG4gICAgICAgICAgdGhpcy4kZWRpdGFibGUgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdGFibGU7XG4gICAgICAgICAgdGhpcy5vcHRpb25zID0gY29udGV4dC5vcHRpb25zO1xuICAgICAgfVxuICAgICAgU3RhdHVzYmFyLnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5haXJNb2RlIHx8IHRoaXMub3B0aW9ucy5kaXNhYmxlUmVzaXplRWRpdG9yKSB7XG4gICAgICAgICAgICAgIHRoaXMuZGVzdHJveSgpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuJHN0YXR1c2Jhci5vbignbW91c2Vkb3duJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgICB2YXIgZWRpdGFibGVUb3AgPSBfdGhpcy4kZWRpdGFibGUub2Zmc2V0KCkudG9wIC0gX3RoaXMuJGRvY3VtZW50LnNjcm9sbFRvcCgpO1xuICAgICAgICAgICAgICB2YXIgb25Nb3VzZU1vdmUgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBoZWlnaHQgPSBldmVudC5jbGllbnRZIC0gKGVkaXRhYmxlVG9wICsgRURJVEFCTEVfUEFERElORyk7XG4gICAgICAgICAgICAgICAgICBoZWlnaHQgPSAoX3RoaXMub3B0aW9ucy5taW5oZWlnaHQgPiAwKSA/IE1hdGgubWF4KGhlaWdodCwgX3RoaXMub3B0aW9ucy5taW5oZWlnaHQpIDogaGVpZ2h0O1xuICAgICAgICAgICAgICAgICAgaGVpZ2h0ID0gKF90aGlzLm9wdGlvbnMubWF4SGVpZ2h0ID4gMCkgPyBNYXRoLm1pbihoZWlnaHQsIF90aGlzLm9wdGlvbnMubWF4SGVpZ2h0KSA6IGhlaWdodDtcbiAgICAgICAgICAgICAgICAgIF90aGlzLiRlZGl0YWJsZS5oZWlnaHQoaGVpZ2h0KTtcbiAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgX3RoaXMuJGRvY3VtZW50Lm9uKCdtb3VzZW1vdmUnLCBvbk1vdXNlTW92ZSkub25lKCdtb3VzZXVwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuJGRvY3VtZW50Lm9mZignbW91c2Vtb3ZlJywgb25Nb3VzZU1vdmUpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICBTdGF0dXNiYXIucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kc3RhdHVzYmFyLm9mZigpO1xuICAgICAgICAgIHRoaXMuJHN0YXR1c2Jhci5hZGRDbGFzcygnbG9ja2VkJyk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFN0YXR1c2JhcjtcbiAgfSgpKTtcblxuICB2YXIgRnVsbHNjcmVlbiA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEZ1bGxzY3JlZW4oY29udGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMuJHRvb2xiYXIgPSBjb250ZXh0LmxheW91dEluZm8udG9vbGJhcjtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZSA9IGNvbnRleHQubGF5b3V0SW5mby5lZGl0YWJsZTtcbiAgICAgICAgICB0aGlzLiRjb2RhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmNvZGFibGU7XG4gICAgICAgICAgdGhpcy4kd2luZG93ID0gJCQxKHdpbmRvdyk7XG4gICAgICAgICAgdGhpcy4kc2Nyb2xsYmFyID0gJCQxKCdodG1sLCBib2R5Jyk7XG4gICAgICAgICAgdGhpcy5vblJlc2l6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMucmVzaXplVG8oe1xuICAgICAgICAgICAgICAgICAgaDogX3RoaXMuJHdpbmRvdy5oZWlnaHQoKSAtIF90aGlzLiR0b29sYmFyLm91dGVySGVpZ2h0KClcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIEZ1bGxzY3JlZW4ucHJvdG90eXBlLnJlc2l6ZVRvID0gZnVuY3Rpb24gKHNpemUpIHtcbiAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5jc3MoJ2hlaWdodCcsIHNpemUuaCk7XG4gICAgICAgICAgdGhpcy4kY29kYWJsZS5jc3MoJ2hlaWdodCcsIHNpemUuaCk7XG4gICAgICAgICAgaWYgKHRoaXMuJGNvZGFibGUuZGF0YSgnY21lZGl0b3InKSkge1xuICAgICAgICAgICAgICB0aGlzLiRjb2RhYmxlLmRhdGEoJ2NtZWRpdG9yJykuc2V0c2l6ZShudWxsLCBzaXplLmgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHRvZ2dsZSBmdWxsc2NyZWVuXG4gICAgICAgKi9cbiAgICAgIEZ1bGxzY3JlZW4ucHJvdG90eXBlLnRvZ2dsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IudG9nZ2xlQ2xhc3MoJ2Z1bGxzY3JlZW4nKTtcbiAgICAgICAgICBpZiAodGhpcy5pc0Z1bGxzY3JlZW4oKSkge1xuICAgICAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5kYXRhKCdvcmdIZWlnaHQnLCB0aGlzLiRlZGl0YWJsZS5jc3MoJ2hlaWdodCcpKTtcbiAgICAgICAgICAgICAgdGhpcy4kZWRpdGFibGUuZGF0YSgnb3JnTWF4SGVpZ2h0JywgdGhpcy4kZWRpdGFibGUuY3NzKCdtYXhIZWlnaHQnKSk7XG4gICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmNzcygnbWF4SGVpZ2h0JywgJycpO1xuICAgICAgICAgICAgICB0aGlzLiR3aW5kb3cub24oJ3Jlc2l6ZScsIHRoaXMub25SZXNpemUpLnRyaWdnZXIoJ3Jlc2l6ZScpO1xuICAgICAgICAgICAgICB0aGlzLiRzY3JvbGxiYXIuY3NzKCdvdmVyZmxvdycsICdoaWRkZW4nKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHRoaXMuJHdpbmRvdy5vZmYoJ3Jlc2l6ZScsIHRoaXMub25SZXNpemUpO1xuICAgICAgICAgICAgICB0aGlzLnJlc2l6ZVRvKHsgaDogdGhpcy4kZWRpdGFibGUuZGF0YSgnb3JnSGVpZ2h0JykgfSk7XG4gICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmNzcygnbWF4SGVpZ2h0JywgdGhpcy4kZWRpdGFibGUuY3NzKCdvcmdNYXhIZWlnaHQnKSk7XG4gICAgICAgICAgICAgIHRoaXMuJHNjcm9sbGJhci5jc3MoJ292ZXJmbG93JywgJ3Zpc2libGUnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgndG9vbGJhci51cGRhdGVGdWxsc2NyZWVuJywgdGhpcy5pc0Z1bGxzY3JlZW4oKSk7XG4gICAgICB9O1xuICAgICAgRnVsbHNjcmVlbi5wcm90b3R5cGUuaXNGdWxsc2NyZWVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiB0aGlzLiRlZGl0b3IuaGFzQ2xhc3MoJ2Z1bGxzY3JlZW4nKTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gRnVsbHNjcmVlbjtcbiAgfSgpKTtcblxuICB2YXIgSGFuZGxlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gSGFuZGxlKGNvbnRleHQpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XG4gICAgICAgICAgdGhpcy4kZG9jdW1lbnQgPSAkJDEoZG9jdW1lbnQpO1xuICAgICAgICAgIHRoaXMuJGVkaXRpbmdBcmVhID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRpbmdBcmVhO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLm1vdXNlZG93bic6IGZ1bmN0aW9uICh3ZSwgZSkge1xuICAgICAgICAgICAgICAgICAgaWYgKF90aGlzLnVwZGF0ZShlLnRhcmdldCwgZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmtleXVwIHN1bW1lcm5vdGUuc2Nyb2xsIHN1bW1lcm5vdGUuY2hhbmdlIHN1bW1lcm5vdGUuZGlhbG9nLnNob3duJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMudXBkYXRlKCk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmRpc2FibGUnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5oaWRlKCk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmNvZGV2aWV3LnRvZ2dsZWQnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy51cGRhdGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9XG4gICAgICBIYW5kbGUucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLiRoYW5kbGUgPSAkJDEoW1xuICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm5vdGUtaGFuZGxlXCI+JyxcbiAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJub3RlLWNvbnRyb2wtc2VsZWN0aW9uXCI+JyxcbiAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJub3RlLWNvbnRyb2wtc2VsZWN0aW9uLWJnXCI+PC9kaXY+JyxcbiAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJub3RlLWNvbnRyb2wtaG9sZGVyIG5vdGUtY29udHJvbC1ud1wiPjwvZGl2PicsXG4gICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibm90ZS1jb250cm9sLWhvbGRlciBub3RlLWNvbnRyb2wtbmVcIj48L2Rpdj4nLFxuICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm5vdGUtY29udHJvbC1ob2xkZXIgbm90ZS1jb250cm9sLXN3XCI+PC9kaXY+JyxcbiAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCInLFxuICAgICAgICAgICAgICAodGhpcy5vcHRpb25zLmRpc2FibGVSZXNpemVJbWFnZSA/ICdub3RlLWNvbnRyb2wtaG9sZGVyJyA6ICdub3RlLWNvbnRyb2wtc2l6aW5nJyksXG4gICAgICAgICAgICAgICcgbm90ZS1jb250cm9sLXNlXCI+PC9kaXY+JyxcbiAgICAgICAgICAgICAgKHRoaXMub3B0aW9ucy5kaXNhYmxlUmVzaXplSW1hZ2UgPyAnJyA6ICc8ZGl2IGNsYXNzPVwibm90ZS1jb250cm9sLXNlbGVjdGlvbi1pbmZvXCI+PC9kaXY+JyksXG4gICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAnPC9kaXY+JyxcbiAgICAgICAgICBdLmpvaW4oJycpKS5wcmVwZW5kVG8odGhpcy4kZWRpdGluZ0FyZWEpO1xuICAgICAgICAgIHRoaXMuJGhhbmRsZS5vbignbW91c2Vkb3duJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIGlmIChkb20uaXNDb250cm9sU2l6aW5nKGV2ZW50LnRhcmdldCkpIHtcbiAgICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgIHZhciAkdGFyZ2V0XzEgPSBfdGhpcy4kaGFuZGxlLmZpbmQoJy5ub3RlLWNvbnRyb2wtc2VsZWN0aW9uJykuZGF0YSgndGFyZ2V0Jyk7XG4gICAgICAgICAgICAgICAgICB2YXIgcG9zU3RhcnRfMSA9ICR0YXJnZXRfMS5vZmZzZXQoKTtcbiAgICAgICAgICAgICAgICAgIHZhciBzY3JvbGxUb3BfMSA9IF90aGlzLiRkb2N1bWVudC5zY3JvbGxUb3AoKTtcbiAgICAgICAgICAgICAgICAgIHZhciBvbk1vdXNlTW92ZV8xID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5yZXNpemVUbycsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgeDogZXZlbnQuY2xpZW50WCAtIHBvc1N0YXJ0XzEubGVmdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgeTogZXZlbnQuY2xpZW50WSAtIChwb3NTdGFydF8xLnRvcCAtIHNjcm9sbFRvcF8xKVxuICAgICAgICAgICAgICAgICAgICAgIH0sICR0YXJnZXRfMSwgIWV2ZW50LnNoaWZ0S2V5KTtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy51cGRhdGUoJHRhcmdldF8xWzBdKTtcbiAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICBfdGhpcy4kZG9jdW1lbnRcbiAgICAgICAgICAgICAgICAgICAgICAub24oJ21vdXNlbW92ZScsIG9uTW91c2VNb3ZlXzEpXG4gICAgICAgICAgICAgICAgICAgICAgLm9uZSgnbW91c2V1cCcsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLiRkb2N1bWVudC5vZmYoJ21vdXNlbW92ZScsIG9uTW91c2VNb3ZlXzEpO1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuYWZ0ZXJDb21tYW5kJyk7XG4gICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgIGlmICghJHRhcmdldF8xLmRhdGEoJ3JhdGlvJykpIHsgLy8gb3JpZ2luYWwgcmF0aW8uXG4gICAgICAgICAgICAgICAgICAgICAgJHRhcmdldF8xLmRhdGEoJ3JhdGlvJywgJHRhcmdldF8xLmhlaWdodCgpIC8gJHRhcmdldF8xLndpZHRoKCkpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgLy8gTGlzdGVuIGZvciBzY3JvbGxpbmcgb24gdGhlIGhhbmRsZSBvdmVybGF5LlxuICAgICAgICAgIHRoaXMuJGhhbmRsZS5vbignd2hlZWwnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgIF90aGlzLnVwZGF0ZSgpO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEhhbmRsZS5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRoYW5kbGUucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgSGFuZGxlLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAodGFyZ2V0LCBldmVudCkge1xuICAgICAgICAgIGlmICh0aGlzLmNvbnRleHQuaXNEaXNhYmxlZCgpKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIGlzSW1hZ2UgPSBkb20uaXNJbWcodGFyZ2V0KTtcbiAgICAgICAgICB2YXIgJHNlbGVjdGlvbiA9IHRoaXMuJGhhbmRsZS5maW5kKCcubm90ZS1jb250cm9sLXNlbGVjdGlvbicpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2ltYWdlUG9wb3Zlci51cGRhdGUnLCB0YXJnZXQsIGV2ZW50KTtcbiAgICAgICAgICBpZiAoaXNJbWFnZSkge1xuICAgICAgICAgICAgICB2YXIgJGltYWdlID0gJCQxKHRhcmdldCk7XG4gICAgICAgICAgICAgIHZhciBwb3NpdGlvbiA9ICRpbWFnZS5wb3NpdGlvbigpO1xuICAgICAgICAgICAgICB2YXIgcG9zID0ge1xuICAgICAgICAgICAgICAgICAgbGVmdDogcG9zaXRpb24ubGVmdCArIHBhcnNlSW50KCRpbWFnZS5jc3MoJ21hcmdpbkxlZnQnKSwgMTApLFxuICAgICAgICAgICAgICAgICAgdG9wOiBwb3NpdGlvbi50b3AgKyBwYXJzZUludCgkaW1hZ2UuY3NzKCdtYXJnaW5Ub3AnKSwgMTApXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgIC8vIGV4Y2x1ZGUgbWFyZ2luXG4gICAgICAgICAgICAgIHZhciBpbWFnZVNpemUgPSB7XG4gICAgICAgICAgICAgICAgICB3OiAkaW1hZ2Uub3V0ZXJXaWR0aChmYWxzZSksXG4gICAgICAgICAgICAgICAgICBoOiAkaW1hZ2Uub3V0ZXJIZWlnaHQoZmFsc2UpXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICRzZWxlY3Rpb24uY3NzKHtcbiAgICAgICAgICAgICAgICAgIGRpc3BsYXk6ICdibG9jaycsXG4gICAgICAgICAgICAgICAgICBsZWZ0OiBwb3MubGVmdCxcbiAgICAgICAgICAgICAgICAgIHRvcDogcG9zLnRvcCxcbiAgICAgICAgICAgICAgICAgIHdpZHRoOiBpbWFnZVNpemUudyxcbiAgICAgICAgICAgICAgICAgIGhlaWdodDogaW1hZ2VTaXplLmhcbiAgICAgICAgICAgICAgfSkuZGF0YSgndGFyZ2V0JywgJGltYWdlKTsgLy8gc2F2ZSBjdXJyZW50IGltYWdlIGVsZW1lbnQuXG4gICAgICAgICAgICAgIHZhciBvcmlnSW1hZ2VPYmogPSBuZXcgSW1hZ2UoKTtcbiAgICAgICAgICAgICAgb3JpZ0ltYWdlT2JqLnNyYyA9ICRpbWFnZS5hdHRyKCdzcmMnKTtcbiAgICAgICAgICAgICAgdmFyIHNpemluZ1RleHQgPSBpbWFnZVNpemUudyArICd4JyArIGltYWdlU2l6ZS5oICsgJyAoJyArIHRoaXMubGFuZy5pbWFnZS5vcmlnaW5hbCArICc6ICcgKyBvcmlnSW1hZ2VPYmoud2lkdGggKyAneCcgKyBvcmlnSW1hZ2VPYmouaGVpZ2h0ICsgJyknO1xuICAgICAgICAgICAgICAkc2VsZWN0aW9uLmZpbmQoJy5ub3RlLWNvbnRyb2wtc2VsZWN0aW9uLWluZm8nKS50ZXh0KHNpemluZ1RleHQpO1xuICAgICAgICAgICAgICB0aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3Iuc2F2ZVRhcmdldCcsIHRhcmdldCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGlzSW1hZ2U7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBoaWRlXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtqUXVlcnl9ICRoYW5kbGVcbiAgICAgICAqL1xuICAgICAgSGFuZGxlLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5jbGVhclRhcmdldCcpO1xuICAgICAgICAgIHRoaXMuJGhhbmRsZS5jaGlsZHJlbigpLmhpZGUoKTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gSGFuZGxlO1xuICB9KCkpO1xuXG4gIHZhciBkZWZhdWx0U2NoZW1lID0gJ2h0dHA6Ly8nO1xuICB2YXIgbGlua1BhdHRlcm4gPSAvXihbQS1aYS16XVtBLVphLXowLTkrLS5dKlxcOltcXC9dezJ9fG1haWx0bzpbQS1aMC05Ll8lKy1dK0ApPyh3d3dcXC4pPyguKykkL2k7XG4gIHZhciBBdXRvTGluayA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEF1dG9MaW5rKGNvbnRleHQpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmtleXVwJzogZnVuY3Rpb24gKHdlLCBlKSB7XG4gICAgICAgICAgICAgICAgICBpZiAoIWUuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5oYW5kbGVLZXl1cChlKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUua2V5ZG93bic6IGZ1bmN0aW9uICh3ZSwgZSkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuaGFuZGxlS2V5ZG93bihlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9XG4gICAgICBBdXRvTGluay5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLmxhc3RXb3JkUmFuZ2UgPSBudWxsO1xuICAgICAgfTtcbiAgICAgIEF1dG9MaW5rLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMubGFzdFdvcmRSYW5nZSA9IG51bGw7XG4gICAgICB9O1xuICAgICAgQXV0b0xpbmsucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgaWYgKCF0aGlzLmxhc3RXb3JkUmFuZ2UpIHtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIga2V5d29yZCA9IHRoaXMubGFzdFdvcmRSYW5nZS50b1N0cmluZygpO1xuICAgICAgICAgIHZhciBtYXRjaCA9IGtleXdvcmQubWF0Y2gobGlua1BhdHRlcm4pO1xuICAgICAgICAgIGlmIChtYXRjaCAmJiAobWF0Y2hbMV0gfHwgbWF0Y2hbMl0pKSB7XG4gICAgICAgICAgICAgIHZhciBsaW5rID0gbWF0Y2hbMV0gPyBrZXl3b3JkIDogZGVmYXVsdFNjaGVtZSArIGtleXdvcmQ7XG4gICAgICAgICAgICAgIHZhciBub2RlID0gJCQxKCc8YSAvPicpLmh0bWwoa2V5d29yZCkuYXR0cignaHJlZicsIGxpbmspWzBdO1xuICAgICAgICAgICAgICBpZiAodGhpcy5jb250ZXh0Lm9wdGlvbnMubGlua1RhcmdldEJsYW5rKSB7XG4gICAgICAgICAgICAgICAgICAkJDEobm9kZSkuYXR0cigndGFyZ2V0JywgJ19ibGFuaycpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRoaXMubGFzdFdvcmRSYW5nZS5pbnNlcnROb2RlKG5vZGUpO1xuICAgICAgICAgICAgICB0aGlzLmxhc3RXb3JkUmFuZ2UgPSBudWxsO1xuICAgICAgICAgICAgICB0aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuZm9jdXMnKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgQXV0b0xpbmsucHJvdG90eXBlLmhhbmRsZUtleWRvd24gPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgIGlmIChsaXN0cy5jb250YWlucyhba2V5LmNvZGUuRU5URVIsIGtleS5jb2RlLlNQQUNFXSwgZS5rZXlDb2RlKSkge1xuICAgICAgICAgICAgICB2YXIgd29yZFJhbmdlID0gdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmNyZWF0ZVJhbmdlJykuZ2V0V29yZFJhbmdlKCk7XG4gICAgICAgICAgICAgIHRoaXMubGFzdFdvcmRSYW5nZSA9IHdvcmRSYW5nZTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgQXV0b0xpbmsucHJvdG90eXBlLmhhbmRsZUtleXVwID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICBpZiAobGlzdHMuY29udGFpbnMoW2tleS5jb2RlLkVOVEVSLCBrZXkuY29kZS5TUEFDRV0sIGUua2V5Q29kZSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5yZXBsYWNlKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIHJldHVybiBBdXRvTGluaztcbiAgfSgpKTtcblxuICAvKipcbiAgICogdGV4dGFyZWEgYXV0byBzeW5jLlxuICAgKi9cbiAgdmFyIEF1dG9TeW5jID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gQXV0b1N5bmMoY29udGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy4kbm90ZSA9IGNvbnRleHQubGF5b3V0SW5mby5ub3RlO1xuICAgICAgICAgIHRoaXMuZXZlbnRzID0ge1xuICAgICAgICAgICAgICAnc3VtbWVybm90ZS5jaGFuZ2UnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy4kbm90ZS52YWwoY29udGV4dC5pbnZva2UoJ2NvZGUnKSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9O1xuICAgICAgfVxuICAgICAgQXV0b1N5bmMucHJvdG90eXBlLnNob3VsZEluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIGRvbS5pc1RleHRhcmVhKHRoaXMuJG5vdGVbMF0pO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBBdXRvU3luYztcbiAgfSgpKTtcblxuICB2YXIgQXV0b1JlcGxhY2UgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBBdXRvUmVwbGFjZShjb250ZXh0KSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucy5yZXBsYWNlIHx8IHt9O1xuICAgICAgICAgIHRoaXMua2V5cyA9IFtrZXkuY29kZS5FTlRFUiwga2V5LmNvZGUuU1BBQ0UsIGtleS5jb2RlLlBFUklPRCwga2V5LmNvZGUuQ09NTUEsIGtleS5jb2RlLlNFTUlDT0xPTiwga2V5LmNvZGUuU0xBU0hdO1xuICAgICAgICAgIHRoaXMucHJldmlvdXNLZXlkb3duQ29kZSA9IG51bGw7XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmtleXVwJzogZnVuY3Rpb24gKHdlLCBlKSB7XG4gICAgICAgICAgICAgICAgICBpZiAoIWUuaXNEZWZhdWx0UHJldmVudGVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5oYW5kbGVLZXl1cChlKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUua2V5ZG93bic6IGZ1bmN0aW9uICh3ZSwgZSkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuaGFuZGxlS2V5ZG93bihlKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9XG4gICAgICBBdXRvUmVwbGFjZS5wcm90b3R5cGUuc2hvdWxkSW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gISF0aGlzLm9wdGlvbnMubWF0Y2g7XG4gICAgICB9O1xuICAgICAgQXV0b1JlcGxhY2UucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5sYXN0V29yZCA9IG51bGw7XG4gICAgICB9O1xuICAgICAgQXV0b1JlcGxhY2UucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5sYXN0V29yZCA9IG51bGw7XG4gICAgICB9O1xuICAgICAgQXV0b1JlcGxhY2UucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgaWYgKCF0aGlzLmxhc3RXb3JkKSB7XG4gICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgICAgICAgIHZhciBrZXl3b3JkID0gdGhpcy5sYXN0V29yZC50b1N0cmluZygpO1xuICAgICAgICAgIHRoaXMub3B0aW9ucy5tYXRjaChrZXl3b3JkLCBmdW5jdGlvbiAobWF0Y2gpIHtcbiAgICAgICAgICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgbm9kZSA9ICcnO1xuICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBtYXRjaCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICAgICAgICBub2RlID0gZG9tLmNyZWF0ZVRleHQobWF0Y2gpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgZWxzZSBpZiAobWF0Y2ggaW5zdGFuY2VvZiBqUXVlcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICBub2RlID0gbWF0Y2hbMF07XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBlbHNlIGlmIChtYXRjaCBpbnN0YW5jZW9mIE5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICBub2RlID0gbWF0Y2g7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICBpZiAoIW5vZGUpXG4gICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgc2VsZi5sYXN0V29yZC5pbnNlcnROb2RlKG5vZGUpO1xuICAgICAgICAgICAgICAgICAgc2VsZi5sYXN0V29yZCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICBzZWxmLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuZm9jdXMnKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEF1dG9SZXBsYWNlLnByb3RvdHlwZS5oYW5kbGVLZXlkb3duID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAvLyB0aGlzIGZvcmNlcyBpdCB0byByZW1lbWJlciB0aGUgbGFzdCB3aG9sZSB3b3JkLCBldmVuIGlmIG11bHRpcGxlIHRlcm1pbmF0aW9uIGtleXMgYXJlIHByZXNzZWRcbiAgICAgICAgICAvLyBiZWZvcmUgdGhlIHByZXZpb3VzIGtleSBpcyBsZXQgZ28uXG4gICAgICAgICAgaWYgKHRoaXMucHJldmlvdXNLZXlkb3duQ29kZSAmJiBsaXN0cy5jb250YWlucyh0aGlzLmtleXMsIHRoaXMucHJldmlvdXNLZXlkb3duQ29kZSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5wcmV2aW91c0tleWRvd25Db2RlID0gZS5rZXlDb2RlO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIGlmIChsaXN0cy5jb250YWlucyh0aGlzLmtleXMsIGUua2V5Q29kZSkpIHtcbiAgICAgICAgICAgICAgdmFyIHdvcmRSYW5nZSA9IHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5jcmVhdGVSYW5nZScpLmdldFdvcmRSYW5nZSgpO1xuICAgICAgICAgICAgICB0aGlzLmxhc3RXb3JkID0gd29yZFJhbmdlO1xuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnByZXZpb3VzS2V5ZG93bkNvZGUgPSBlLmtleUNvZGU7XG4gICAgICB9O1xuICAgICAgQXV0b1JlcGxhY2UucHJvdG90eXBlLmhhbmRsZUtleXVwID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICBpZiAobGlzdHMuY29udGFpbnModGhpcy5rZXlzLCBlLmtleUNvZGUpKSB7XG4gICAgICAgICAgICAgIHRoaXMucmVwbGFjZSgpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICByZXR1cm4gQXV0b1JlcGxhY2U7XG4gIH0oKSk7XG5cbiAgdmFyIFBsYWNlaG9sZGVyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gUGxhY2Vob2xkZXIoY29udGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLiRlZGl0aW5nQXJlYSA9IGNvbnRleHQubGF5b3V0SW5mby5lZGl0aW5nQXJlYTtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmluaXQgc3VtbWVybm90ZS5jaGFuZ2UnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy51cGRhdGUoKTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUuY29kZXZpZXcudG9nZ2xlZCc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLnVwZGF0ZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIFBsYWNlaG9sZGVyLnByb3RvdHlwZS5zaG91bGRJbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiAhIXRoaXMub3B0aW9ucy5wbGFjZWhvbGRlcjtcbiAgICAgIH07XG4gICAgICBQbGFjZWhvbGRlci5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuJHBsYWNlaG9sZGVyID0gJCQxKCc8ZGl2IGNsYXNzPVwibm90ZS1wbGFjZWhvbGRlclwiPicpO1xuICAgICAgICAgIHRoaXMuJHBsYWNlaG9sZGVyLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2ZvY3VzJyk7XG4gICAgICAgICAgfSkuaHRtbCh0aGlzLm9wdGlvbnMucGxhY2Vob2xkZXIpLnByZXBlbmRUbyh0aGlzLiRlZGl0aW5nQXJlYSk7XG4gICAgICAgICAgdGhpcy51cGRhdGUoKTtcbiAgICAgIH07XG4gICAgICBQbGFjZWhvbGRlci5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwbGFjZWhvbGRlci5yZW1vdmUoKTtcbiAgICAgIH07XG4gICAgICBQbGFjZWhvbGRlci5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBpc1Nob3cgPSAhdGhpcy5jb250ZXh0Lmludm9rZSgnY29kZXZpZXcuaXNBY3RpdmF0ZWQnKSAmJiB0aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuaXNFbXB0eScpO1xuICAgICAgICAgIHRoaXMuJHBsYWNlaG9sZGVyLnRvZ2dsZShpc1Nob3cpO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBQbGFjZWhvbGRlcjtcbiAgfSgpKTtcblxuICB2YXIgQnV0dG9ucyA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEJ1dHRvbnMoY29udGV4dCkge1xuICAgICAgICAgIHRoaXMudWkgPSAkJDEuc3VtbWVybm90ZS51aTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMuJHRvb2xiYXIgPSBjb250ZXh0LmxheW91dEluZm8udG9vbGJhcjtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICAgICAgdGhpcy5sYW5nID0gdGhpcy5vcHRpb25zLmxhbmdJbmZvO1xuICAgICAgICAgIHRoaXMuaW52ZXJ0ZWRLZXlNYXAgPSBmdW5jLmludmVydE9iamVjdCh0aGlzLm9wdGlvbnMua2V5TWFwW2Vudi5pc01hYyA/ICdtYWMnIDogJ3BjJ10pO1xuICAgICAgfVxuICAgICAgQnV0dG9ucy5wcm90b3R5cGUucmVwcmVzZW50U2hvcnRjdXQgPSBmdW5jdGlvbiAoZWRpdG9yTWV0aG9kKSB7XG4gICAgICAgICAgdmFyIHNob3J0Y3V0ID0gdGhpcy5pbnZlcnRlZEtleU1hcFtlZGl0b3JNZXRob2RdO1xuICAgICAgICAgIGlmICghdGhpcy5vcHRpb25zLnNob3J0Y3V0cyB8fCAhc2hvcnRjdXQpIHtcbiAgICAgICAgICAgICAgcmV0dXJuICcnO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZW52LmlzTWFjKSB7XG4gICAgICAgICAgICAgIHNob3J0Y3V0ID0gc2hvcnRjdXQucmVwbGFjZSgnQ01EJywgJ+KMmCcpLnJlcGxhY2UoJ1NISUZUJywgJ+KHpycpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBzaG9ydGN1dCA9IHNob3J0Y3V0LnJlcGxhY2UoJ0JBQ0tTTEFTSCcsICdcXFxcJylcbiAgICAgICAgICAgICAgLnJlcGxhY2UoJ1NMQVNIJywgJy8nKVxuICAgICAgICAgICAgICAucmVwbGFjZSgnTEVGVEJSQUNLRVQnLCAnWycpXG4gICAgICAgICAgICAgIC5yZXBsYWNlKCdSSUdIVEJSQUNLRVQnLCAnXScpO1xuICAgICAgICAgIHJldHVybiAnICgnICsgc2hvcnRjdXQgKyAnKSc7XG4gICAgICB9O1xuICAgICAgQnV0dG9ucy5wcm90b3R5cGUuYnV0dG9uID0gZnVuY3Rpb24gKG8pIHtcbiAgICAgICAgICBpZiAoIXRoaXMub3B0aW9ucy50b29sdGlwICYmIG8udG9vbHRpcCkge1xuICAgICAgICAgICAgICBkZWxldGUgby50b29sdGlwO1xuICAgICAgICAgIH1cbiAgICAgICAgICBvLmNvbnRhaW5lciA9IHRoaXMub3B0aW9ucy5jb250YWluZXI7XG4gICAgICAgICAgcmV0dXJuIHRoaXMudWkuYnV0dG9uKG8pO1xuICAgICAgfTtcbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy5hZGRUb29sYmFyQnV0dG9ucygpO1xuICAgICAgICAgIHRoaXMuYWRkSW1hZ2VQb3BvdmVyQnV0dG9ucygpO1xuICAgICAgICAgIHRoaXMuYWRkTGlua1BvcG92ZXJCdXR0b25zKCk7XG4gICAgICAgICAgdGhpcy5hZGRUYWJsZVBvcG92ZXJCdXR0b25zKCk7XG4gICAgICAgICAgdGhpcy5mb250SW5zdGFsbGVkTWFwID0ge307XG4gICAgICB9O1xuICAgICAgQnV0dG9ucy5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBkZWxldGUgdGhpcy5mb250SW5zdGFsbGVkTWFwO1xuICAgICAgfTtcbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLmlzRm9udEluc3RhbGxlZCA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgICAgaWYgKCF0aGlzLmZvbnRJbnN0YWxsZWRNYXAuaGFzT3duUHJvcGVydHkobmFtZSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5mb250SW5zdGFsbGVkTWFwW25hbWVdID0gZW52LmlzRm9udEluc3RhbGxlZChuYW1lKSB8fFxuICAgICAgICAgICAgICAgICAgbGlzdHMuY29udGFpbnModGhpcy5vcHRpb25zLmZvbnROYW1lc0lnbm9yZUNoZWNrLCBuYW1lKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRoaXMuZm9udEluc3RhbGxlZE1hcFtuYW1lXTtcbiAgICAgIH07XG4gICAgICBCdXR0b25zLnByb3RvdHlwZS5pc0ZvbnREZXNlcnZlZFRvQWRkID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgICB2YXIgZ2VuZXJpY0ZhbWlsaWVzID0gWydzYW5zLXNlcmlmJywgJ3NlcmlmJywgJ21vbm9zcGFjZScsICdjdXJzaXZlJywgJ2ZhbnRhc3knXTtcbiAgICAgICAgICBuYW1lID0gbmFtZS50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgIHJldHVybiAobmFtZSAhPT0gJycgJiYgdGhpcy5pc0ZvbnRJbnN0YWxsZWQobmFtZSkgJiYgZ2VuZXJpY0ZhbWlsaWVzLmluZGV4T2YobmFtZSkgPT09IC0xKTtcbiAgICAgIH07XG4gICAgICBCdXR0b25zLnByb3RvdHlwZS5jb2xvclBhbGV0dGUgPSBmdW5jdGlvbiAoY2xhc3NOYW1lLCB0b29sdGlwLCBiYWNrQ29sb3IsIGZvcmVDb2xvcikge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgcmV0dXJuIHRoaXMudWkuYnV0dG9uR3JvdXAoe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWNvbG9yICcgKyBjbGFzc05hbWUsXG4gICAgICAgICAgICAgIGNoaWxkcmVuOiBbXG4gICAgICAgICAgICAgICAgICB0aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS1jdXJyZW50LWNvbG9yLWJ1dHRvbicsXG4gICAgICAgICAgICAgICAgICAgICAgY29udGVudHM6IHRoaXMudWkuaWNvbih0aGlzLm9wdGlvbnMuaWNvbnMuZm9udCArICcgbm90ZS1yZWNlbnQtY29sb3InKSxcbiAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwOiB0b29sdGlwLFxuICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGJ1dHRvbiA9ICQkMShlLmN1cnJlbnRUYXJnZXQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoYmFja0NvbG9yICYmIGZvcmVDb2xvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5jb2xvcicsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrQ29sb3I6ICRidXR0b24uYXR0cignZGF0YS1iYWNrQ29sb3InKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JlQ29sb3I6ICRidXR0b24uYXR0cignZGF0YS1mb3JlQ29sb3InKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAoYmFja0NvbG9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmNvbG9yJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tDb2xvcjogJGJ1dHRvbi5hdHRyKCdkYXRhLWJhY2tDb2xvcicpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIGlmIChmb3JlQ29sb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuY29sb3InLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yZUNvbG9yOiAkYnV0dG9uLmF0dHIoJ2RhdGEtZm9yZUNvbG9yJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKCRidXR0b24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRyZWNlbnRDb2xvciA9ICRidXR0b24uZmluZCgnLm5vdGUtcmVjZW50LWNvbG9yJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChiYWNrQ29sb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyZWNlbnRDb2xvci5jc3MoJ2JhY2tncm91bmQtY29sb3InLCBfdGhpcy5vcHRpb25zLmNvbG9yQnV0dG9uLmJhY2tDb2xvcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkYnV0dG9uLmF0dHIoJ2RhdGEtYmFja0NvbG9yJywgX3RoaXMub3B0aW9ucy5jb2xvckJ1dHRvbi5iYWNrQ29sb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmb3JlQ29sb3IpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyZWNlbnRDb2xvci5jc3MoJ2NvbG9yJywgX3RoaXMub3B0aW9ucy5jb2xvckJ1dHRvbi5mb3JlQ29sb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGJ1dHRvbi5hdHRyKCdkYXRhLWZvcmVDb2xvcicsIF90aGlzLm9wdGlvbnMuY29sb3JCdXR0b24uZm9yZUNvbG9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRyZWNlbnRDb2xvci5jc3MoJ2NvbG9yJywgJ3RyYW5zcGFyZW50Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIHRoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdkcm9wZG93bi10b2dnbGUnLFxuICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLnVpLmRyb3Bkb3duQnV0dG9uQ29udGVudHMoJycsIHRoaXMub3B0aW9ucyksXG4gICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcDogdGhpcy5sYW5nLmNvbG9yLm1vcmUsXG4gICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGU6ICdkcm9wZG93bidcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIHRoaXMudWkuZHJvcGRvd24oe1xuICAgICAgICAgICAgICAgICAgICAgIGl0ZW1zOiAoYmFja0NvbG9yID8gW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm5vdGUtcGFsZXR0ZVwiPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDxkaXYgY2xhc3M9XCJub3RlLXBhbGV0dGUtdGl0bGVcIj4nICsgdGhpcy5sYW5nLmNvbG9yLmJhY2tncm91bmQgKyAnPC9kaXY+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwibm90ZS1jb2xvci1yZXNldCBidG4gYnRuLWxpZ2h0XCIgZGF0YS1ldmVudD1cImJhY2tDb2xvclwiIGRhdGEtdmFsdWU9XCJpbmhlcml0XCI+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sYW5nLmNvbG9yLnRyYW5zcGFyZW50LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDwvYnV0dG9uPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDxkaXYgY2xhc3M9XCJub3RlLWhvbGRlclwiIGRhdGEtZXZlbnQ9XCJiYWNrQ29sb3JcIi8+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwibm90ZS1jb2xvci1zZWxlY3QgYnRuXCIgZGF0YS1ldmVudD1cIm9wZW5QYWxldHRlXCIgZGF0YS12YWx1ZT1cImJhY2tDb2xvclBpY2tlclwiPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGFuZy5jb2xvci5jcFNlbGVjdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgICA8L2J1dHRvbj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDxpbnB1dCB0eXBlPVwiY29sb3JcIiBpZD1cImJhY2tDb2xvclBpY2tlclwiIGNsYXNzPVwibm90ZS1idG4gbm90ZS1jb2xvci1zZWxlY3QtYnRuXCIgdmFsdWU9XCInICsgdGhpcy5vcHRpb25zLmNvbG9yQnV0dG9uLmJhY2tDb2xvciArICdcIiBkYXRhLWV2ZW50PVwiYmFja0NvbG9yUGFsZXR0ZVwiPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDxkaXYgY2xhc3M9XCJub3RlLWhvbGRlci1jdXN0b21cIiBpZD1cImJhY2tDb2xvclBhbGV0dGVcIiBkYXRhLWV2ZW50PVwiYmFja0NvbG9yXCIvPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgIF0uam9pbignJykgOiAnJykgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICAoZm9yZUNvbG9yID8gW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJub3RlLXBhbGV0dGVcIj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdiBjbGFzcz1cIm5vdGUtcGFsZXR0ZS10aXRsZVwiPicgKyB0aGlzLmxhbmcuY29sb3IuZm9yZWdyb3VuZCArICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cIm5vdGUtY29sb3ItcmVzZXQgYnRuIGJ0bi1saWdodFwiIGRhdGEtZXZlbnQ9XCJyZW1vdmVGb3JtYXRcIiBkYXRhLXZhbHVlPVwiZm9yZUNvbG9yXCI+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGFuZy5jb2xvci5yZXNldFRvRGVmYXVsdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcgICAgPC9idXR0b24+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICcgIDwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICA8ZGl2IGNsYXNzPVwibm90ZS1ob2xkZXJcIiBkYXRhLWV2ZW50PVwiZm9yZUNvbG9yXCIvPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICA8ZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwibm90ZS1jb2xvci1zZWxlY3QgYnRuXCIgZGF0YS1ldmVudD1cIm9wZW5QYWxldHRlXCIgZGF0YS12YWx1ZT1cImZvcmVDb2xvclBpY2tlclwiPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxhbmcuY29sb3IuY3BTZWxlY3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDwvYnV0dG9uPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICAgIDxpbnB1dCB0eXBlPVwiY29sb3JcIiBpZD1cImZvcmVDb2xvclBpY2tlclwiIGNsYXNzPVwibm90ZS1idG4gbm90ZS1jb2xvci1zZWxlY3QtYnRuXCIgdmFsdWU9XCInICsgdGhpcy5vcHRpb25zLmNvbG9yQnV0dG9uLmZvcmVDb2xvciArICdcIiBkYXRhLWV2ZW50PVwiZm9yZUNvbG9yUGFsZXR0ZVwiPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnICA8ZGl2IGNsYXNzPVwibm90ZS1ob2xkZXItY3VzdG9tXCIgaWQ9XCJmb3JlQ29sb3JQYWxldHRlXCIgZGF0YS1ldmVudD1cImZvcmVDb2xvclwiLz4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJzwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIF0uam9pbignJykgOiAnJyksXG4gICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uICgkZHJvcGRvd24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJGRyb3Bkb3duLmZpbmQoJy5ub3RlLWhvbGRlcicpLmVhY2goZnVuY3Rpb24gKGlkeCwgaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRob2xkZXIgPSAkJDEoaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkaG9sZGVyLmFwcGVuZChfdGhpcy51aS5wYWxldHRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcnM6IF90aGlzLm9wdGlvbnMuY29sb3JzLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yc05hbWU6IF90aGlzLm9wdGlvbnMuY29sb3JzTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudE5hbWU6ICRob2xkZXIuZGF0YSgnZXZlbnQnKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250YWluZXI6IF90aGlzLm9wdGlvbnMuY29udGFpbmVyLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLm9wdGlvbnMudG9vbHRpcFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkucmVuZGVyKCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLyogVE9ETzogZG8gd2UgaGF2ZSB0byByZWNvcmQgcmVjZW50IGN1c3RvbSBjb2xvcnMgd2l0aGluIGNvb2tpZXM/ICovXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjdXN0b21Db2xvcnMgPSBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBbJyNGRkZGRkYnLCAnI0ZGRkZGRicsICcjRkZGRkZGJywgJyNGRkZGRkYnLCAnI0ZGRkZGRicsICcjRkZGRkZGJywgJyNGRkZGRkYnLCAnI0ZGRkZGRiddLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAkZHJvcGRvd24uZmluZCgnLm5vdGUtaG9sZGVyLWN1c3RvbScpLmVhY2goZnVuY3Rpb24gKGlkeCwgaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRob2xkZXIgPSAkJDEoaXRlbSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkaG9sZGVyLmFwcGVuZChfdGhpcy51aS5wYWxldHRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcnM6IGN1c3RvbUNvbG9ycyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcnNOYW1lOiBjdXN0b21Db2xvcnMsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXZlbnROYW1lOiAkaG9sZGVyLmRhdGEoJ2V2ZW50JyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyOiBfdGhpcy5vcHRpb25zLmNvbnRhaW5lcixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5vcHRpb25zLnRvb2x0aXBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLnJlbmRlcigpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICRkcm9wZG93bi5maW5kKCdpbnB1dFt0eXBlPWNvbG9yXScpLmVhY2goZnVuY3Rpb24gKGlkeCwgaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJCQxKGl0ZW0pLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRjaGlwID0gJGRyb3Bkb3duLmZpbmQoJyMnICsgJCQxKHRoaXMpLmRhdGEoJ2V2ZW50JykpLmZpbmQoJy5ub3RlLWNvbG9yLWJ0bicpLmZpcnN0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNvbG9yID0gdGhpcy52YWx1ZS50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjaGlwLmNzcygnYmFja2dyb3VuZC1jb2xvcicsIGNvbG9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignYXJpYS1sYWJlbCcsIGNvbG9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignZGF0YS12YWx1ZScsIGNvbG9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignZGF0YS1vcmlnaW5hbC10aXRsZScsIGNvbG9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY2hpcC5jbGljaygpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRwYXJlbnQgPSAkJDEoJy4nICsgY2xhc3NOYW1lKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyICRidXR0b24gPSAkJDEoZXZlbnQudGFyZ2V0KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGV2ZW50TmFtZSA9ICRidXR0b24uZGF0YSgnZXZlbnQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHZhbHVlID0gJGJ1dHRvbi5hdHRyKCdkYXRhLXZhbHVlJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChldmVudE5hbWUgPT09ICdvcGVuUGFsZXR0ZScpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkcGlja2VyID0gJHBhcmVudC5maW5kKCcjJyArIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkcGFsZXR0ZSA9ICQkMSgkcGFyZW50LmZpbmQoJyMnICsgJHBpY2tlci5kYXRhKCdldmVudCcpKS5maW5kKCcubm90ZS1jb2xvci1yb3cnKVswXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTaGlmdCBwYWxldHRlIGNoaXBzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGNoaXAgPSAkcGFsZXR0ZS5maW5kKCcubm90ZS1jb2xvci1idG4nKS5sYXN0KCkuZGV0YWNoKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBTZXQgY2hpcCBhdHRyaWJ1dGVzXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY29sb3IgPSAkcGlja2VyLnZhbCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJGNoaXAuY3NzKCdiYWNrZ3JvdW5kLWNvbG9yJywgY29sb3IpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FyaWEtbGFiZWwnLCBjb2xvcilcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuYXR0cignZGF0YS12YWx1ZScsIGNvbG9yKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5hdHRyKCdkYXRhLW9yaWdpbmFsLXRpdGxlJywgY29sb3IpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHBhbGV0dGUucHJlcGVuZCgkY2hpcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkcGlja2VyLmNsaWNrKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgZWxzZSBpZiAobGlzdHMuY29udGFpbnMoWydiYWNrQ29sb3InLCAnZm9yZUNvbG9yJ10sIGV2ZW50TmFtZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBrZXkgPSBldmVudE5hbWUgPT09ICdiYWNrQ29sb3InID8gJ2JhY2tncm91bmQtY29sb3InIDogJ2NvbG9yJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkY29sb3IgPSAkYnV0dG9uLmNsb3Nlc3QoJy5ub3RlLWNvbG9yJykuZmluZCgnLm5vdGUtcmVjZW50LWNvbG9yJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgJGN1cnJlbnRCdXR0b24gPSAkYnV0dG9uLmNsb3Nlc3QoJy5ub3RlLWNvbG9yJykuZmluZCgnLm5vdGUtY3VycmVudC1jb2xvci1idXR0b24nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRjb2xvci5jc3Moa2V5LCB2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkY3VycmVudEJ1dHRvbi5hdHRyKCdkYXRhLScgKyBldmVudE5hbWUsIHZhbHVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuJyArIGV2ZW50TmFtZSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgIH07XG4gICAgICBCdXR0b25zLnByb3RvdHlwZS5hZGRUb29sYmFyQnV0dG9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uc3R5bGUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy51aS5idXR0b25Hcm91cChbXG4gICAgICAgICAgICAgICAgICBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2Ryb3Bkb3duLXRvZ2dsZScsXG4gICAgICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmRyb3Bkb3duQnV0dG9uQ29udGVudHMoX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLm1hZ2ljKSwgX3RoaXMub3B0aW9ucyksXG4gICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5zdHlsZS5zdHlsZSxcbiAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRvZ2dsZTogJ2Ryb3Bkb3duJ1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgX3RoaXMudWkuZHJvcGRvd24oe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2Ryb3Bkb3duLXN0eWxlJyxcbiAgICAgICAgICAgICAgICAgICAgICBpdGVtczogX3RoaXMub3B0aW9ucy5zdHlsZVRhZ3MsXG4gICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90aGlzLmxhbmcuc3R5bGUuc3R5bGUsXG4gICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGU6IGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgaXRlbSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSB7IHRhZzogaXRlbSwgdGl0bGU6IChfdGhpcy5sYW5nLnN0eWxlLmhhc093blByb3BlcnR5KGl0ZW0pID8gX3RoaXMubGFuZy5zdHlsZVtpdGVtXSA6IGl0ZW0pIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRhZyA9IGl0ZW0udGFnO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgdGl0bGUgPSBpdGVtLnRpdGxlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3R5bGUgPSBpdGVtLnN0eWxlID8gJyBzdHlsZT1cIicgKyBpdGVtLnN0eWxlICsgJ1wiICcgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGNsYXNzTmFtZSA9IGl0ZW0uY2xhc3NOYW1lID8gJyBjbGFzcz1cIicgKyBpdGVtLmNsYXNzTmFtZSArICdcIicgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8JyArIHRhZyArIHN0eWxlICsgY2xhc3NOYW1lICsgJz4nICsgdGl0bGUgKyAnPC8nICsgdGFnICsgJz4nO1xuICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmZvcm1hdEJsb2NrJylcbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBdKS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB2YXIgX2xvb3BfMSA9IGZ1bmN0aW9uIChzdHlsZUlkeCwgc3R5bGVMZW4pIHtcbiAgICAgICAgICAgICAgdmFyIGl0ZW0gPSB0aGlzXzEub3B0aW9ucy5zdHlsZVRhZ3Nbc3R5bGVJZHhdO1xuICAgICAgICAgICAgICB0aGlzXzEuY29udGV4dC5tZW1vKCdidXR0b24uc3R5bGUuJyArIGl0ZW0sIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ25vdGUtYnRuLXN0eWxlLScgKyBpdGVtLFxuICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiAnPGRpdiBkYXRhLXZhbHVlPVwiJyArIGl0ZW0gKyAnXCI+JyArIGl0ZW0udG9VcHBlckNhc2UoKSArICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuc3R5bGVbaXRlbV0sXG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmZvcm1hdEJsb2NrJylcbiAgICAgICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9O1xuICAgICAgICAgIHZhciB0aGlzXzEgPSB0aGlzO1xuICAgICAgICAgIGZvciAodmFyIHN0eWxlSWR4ID0gMCwgc3R5bGVMZW4gPSB0aGlzLm9wdGlvbnMuc3R5bGVUYWdzLmxlbmd0aDsgc3R5bGVJZHggPCBzdHlsZUxlbjsgc3R5bGVJZHgrKykge1xuICAgICAgICAgICAgICBfbG9vcF8xKHN0eWxlSWR4LCBzdHlsZUxlbik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uYm9sZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWJ0bi1ib2xkJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuYm9sZCksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmZvbnQuYm9sZCArIF90aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdib2xkJyksXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyQW5kVXBkYXRlU3RhdGUoJ2VkaXRvci5ib2xkJylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5pdGFsaWMnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS1idG4taXRhbGljJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuaXRhbGljKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuZm9udC5pdGFsaWMgKyBfdGhpcy5yZXByZXNlbnRTaG9ydGN1dCgnaXRhbGljJyksXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyQW5kVXBkYXRlU3RhdGUoJ2VkaXRvci5pdGFsaWMnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLnVuZGVybGluZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWJ0bi11bmRlcmxpbmUnLFxuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy51bmRlcmxpbmUpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5mb250LnVuZGVybGluZSArIF90aGlzLnJlcHJlc2VudFNob3J0Y3V0KCd1bmRlcmxpbmUnKSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXJBbmRVcGRhdGVTdGF0ZSgnZWRpdG9yLnVuZGVybGluZScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uY2xlYXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5lcmFzZXIpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5mb250LmNsZWFyICsgX3RoaXMucmVwcmVzZW50U2hvcnRjdXQoJ3JlbW92ZUZvcm1hdCcpLFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLnJlbW92ZUZvcm1hdCcpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uc3RyaWtldGhyb3VnaCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWJ0bi1zdHJpa2V0aHJvdWdoJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuc3RyaWtldGhyb3VnaCksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmZvbnQuc3RyaWtldGhyb3VnaCArIF90aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdzdHJpa2V0aHJvdWdoJyksXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyQW5kVXBkYXRlU3RhdGUoJ2VkaXRvci5zdHJpa2V0aHJvdWdoJylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5zdXBlcnNjcmlwdCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWJ0bi1zdXBlcnNjcmlwdCcsXG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnN1cGVyc2NyaXB0KSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuZm9udC5zdXBlcnNjcmlwdCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXJBbmRVcGRhdGVTdGF0ZSgnZWRpdG9yLnN1cGVyc2NyaXB0JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5zdWJzY3JpcHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS1idG4tc3Vic2NyaXB0JyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuc3Vic2NyaXB0KSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuZm9udC5zdWJzY3JpcHQsXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyQW5kVXBkYXRlU3RhdGUoJ2VkaXRvci5zdWJzY3JpcHQnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmZvbnRuYW1lJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICB2YXIgc3R5bGVJbmZvID0gX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5jdXJyZW50U3R5bGUnKTtcbiAgICAgICAgICAgICAgLy8gQWRkICdkZWZhdWx0JyBmb250cyBpbnRvIHRoZSBmb250bmFtZXMgYXJyYXkgaWYgbm90IGV4aXN0XG4gICAgICAgICAgICAgICQkMS5lYWNoKHN0eWxlSW5mb1snZm9udC1mYW1pbHknXS5zcGxpdCgnLCcpLCBmdW5jdGlvbiAoaWR4LCBmb250bmFtZSkge1xuICAgICAgICAgICAgICAgICAgZm9udG5hbWUgPSBmb250bmFtZS50cmltKCkucmVwbGFjZSgvWydcIl0rL2csICcnKTtcbiAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy5pc0ZvbnREZXNlcnZlZFRvQWRkKGZvbnRuYW1lKSkge1xuICAgICAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLmZvbnROYW1lcy5pbmRleE9mKGZvbnRuYW1lKSA9PT0gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgX3RoaXMub3B0aW9ucy5mb250TmFtZXMucHVzaChmb250bmFtZSk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLnVpLmJ1dHRvbkdyb3VwKFtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnZHJvcGRvd24tdG9nZ2xlJyxcbiAgICAgICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuZHJvcGRvd25CdXR0b25Db250ZW50cygnPHNwYW4gY2xhc3M9XCJub3RlLWN1cnJlbnQtZm9udG5hbWVcIi8+JywgX3RoaXMub3B0aW9ucyksXG4gICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5mb250Lm5hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGU6ICdkcm9wZG93bidcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIF90aGlzLnVpLmRyb3Bkb3duQ2hlY2soe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2Ryb3Bkb3duLWZvbnRuYW1lJyxcbiAgICAgICAgICAgICAgICAgICAgICBjaGVja0NsYXNzTmFtZTogX3RoaXMub3B0aW9ucy5pY29ucy5tZW51Q2hlY2ssXG4gICAgICAgICAgICAgICAgICAgICAgaXRlbXM6IF90aGlzLm9wdGlvbnMuZm9udE5hbWVzLmZpbHRlcihfdGhpcy5pc0ZvbnRJbnN0YWxsZWQuYmluZChfdGhpcykpLFxuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdGhpcy5sYW5nLmZvbnQubmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICc8c3BhbiBzdHlsZT1cImZvbnQtZmFtaWx5OiBcXCcnICsgaXRlbSArICdcXCdcIj4nICsgaXRlbSArICc8L3NwYW4+JztcbiAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXJBbmRVcGRhdGVTdGF0ZSgnZWRpdG9yLmZvbnROYW1lJylcbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBdKS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmZvbnRzaXplJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMudWkuYnV0dG9uR3JvdXAoW1xuICAgICAgICAgICAgICAgICAgX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdkcm9wZG93bi10b2dnbGUnLFxuICAgICAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5kcm9wZG93bkJ1dHRvbkNvbnRlbnRzKCc8c3BhbiBjbGFzcz1cIm5vdGUtY3VycmVudC1mb250c2l6ZVwiLz4nLCBfdGhpcy5vcHRpb25zKSxcbiAgICAgICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmZvbnQuc2l6ZSxcbiAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRvZ2dsZTogJ2Ryb3Bkb3duJ1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgX3RoaXMudWkuZHJvcGRvd25DaGVjayh7XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnZHJvcGRvd24tZm9udHNpemUnLFxuICAgICAgICAgICAgICAgICAgICAgIGNoZWNrQ2xhc3NOYW1lOiBfdGhpcy5vcHRpb25zLmljb25zLm1lbnVDaGVjayxcbiAgICAgICAgICAgICAgICAgICAgICBpdGVtczogX3RoaXMub3B0aW9ucy5mb250U2l6ZXMsXG4gICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90aGlzLmxhbmcuZm9udC5zaXplLFxuICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXJBbmRVcGRhdGVTdGF0ZSgnZWRpdG9yLmZvbnRTaXplJylcbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICBdKS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmNvbG9yJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuY29sb3JQYWxldHRlKCdub3RlLWNvbG9yLWFsbCcsIF90aGlzLmxhbmcuY29sb3IucmVjZW50LCB0cnVlLCB0cnVlKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmZvcmVjb2xvcicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmNvbG9yUGFsZXR0ZSgnbm90ZS1jb2xvci1mb3JlJywgX3RoaXMubGFuZy5jb2xvci5mb3JlZ3JvdW5kLCBmYWxzZSwgdHJ1ZSk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5iYWNrY29sb3InLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5jb2xvclBhbGV0dGUoJ25vdGUtY29sb3ItYmFjaycsIF90aGlzLmxhbmcuY29sb3IuYmFja2dyb3VuZCwgdHJ1ZSwgZmFsc2UpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24udWwnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy51bm9yZGVyZWRsaXN0KSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcubGlzdHMudW5vcmRlcmVkICsgX3RoaXMucmVwcmVzZW50U2hvcnRjdXQoJ2luc2VydFVub3JkZXJlZExpc3QnKSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5pbnNlcnRVbm9yZGVyZWRMaXN0JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5vbCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLm9yZGVyZWRsaXN0KSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcubGlzdHMub3JkZXJlZCArIF90aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdpbnNlcnRPcmRlcmVkTGlzdCcpLFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmluc2VydE9yZGVyZWRMaXN0JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyIGp1c3RpZnlMZWZ0ID0gdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICBjb250ZW50czogdGhpcy51aS5pY29uKHRoaXMub3B0aW9ucy5pY29ucy5hbGlnbkxlZnQpLFxuICAgICAgICAgICAgICB0b29sdGlwOiB0aGlzLmxhbmcucGFyYWdyYXBoLmxlZnQgKyB0aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdqdXN0aWZ5TGVmdCcpLFxuICAgICAgICAgICAgICBjbGljazogdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5qdXN0aWZ5TGVmdCcpXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyIGp1c3RpZnlDZW50ZXIgPSB0aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLnVpLmljb24odGhpcy5vcHRpb25zLmljb25zLmFsaWduQ2VudGVyKSxcbiAgICAgICAgICAgICAgdG9vbHRpcDogdGhpcy5sYW5nLnBhcmFncmFwaC5jZW50ZXIgKyB0aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdqdXN0aWZ5Q2VudGVyJyksXG4gICAgICAgICAgICAgIGNsaWNrOiB0aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmp1c3RpZnlDZW50ZXInKVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHZhciBqdXN0aWZ5UmlnaHQgPSB0aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLnVpLmljb24odGhpcy5vcHRpb25zLmljb25zLmFsaWduUmlnaHQpLFxuICAgICAgICAgICAgICB0b29sdGlwOiB0aGlzLmxhbmcucGFyYWdyYXBoLnJpZ2h0ICsgdGhpcy5yZXByZXNlbnRTaG9ydGN1dCgnanVzdGlmeVJpZ2h0JyksXG4gICAgICAgICAgICAgIGNsaWNrOiB0aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmp1c3RpZnlSaWdodCcpXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyIGp1c3RpZnlGdWxsID0gdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICBjb250ZW50czogdGhpcy51aS5pY29uKHRoaXMub3B0aW9ucy5pY29ucy5hbGlnbkp1c3RpZnkpLFxuICAgICAgICAgICAgICB0b29sdGlwOiB0aGlzLmxhbmcucGFyYWdyYXBoLmp1c3RpZnkgKyB0aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdqdXN0aWZ5RnVsbCcpLFxuICAgICAgICAgICAgICBjbGljazogdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5qdXN0aWZ5RnVsbCcpXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdmFyIG91dGRlbnQgPSB0aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLnVpLmljb24odGhpcy5vcHRpb25zLmljb25zLm91dGRlbnQpLFxuICAgICAgICAgICAgICB0b29sdGlwOiB0aGlzLmxhbmcucGFyYWdyYXBoLm91dGRlbnQgKyB0aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdvdXRkZW50JyksXG4gICAgICAgICAgICAgIGNsaWNrOiB0aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLm91dGRlbnQnKVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIHZhciBpbmRlbnQgPSB0aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgIGNvbnRlbnRzOiB0aGlzLnVpLmljb24odGhpcy5vcHRpb25zLmljb25zLmluZGVudCksXG4gICAgICAgICAgICAgIHRvb2x0aXA6IHRoaXMubGFuZy5wYXJhZ3JhcGguaW5kZW50ICsgdGhpcy5yZXByZXNlbnRTaG9ydGN1dCgnaW5kZW50JyksXG4gICAgICAgICAgICAgIGNsaWNrOiB0aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmluZGVudCcpXG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5qdXN0aWZ5TGVmdCcsIGZ1bmMuaW52b2tlKGp1c3RpZnlMZWZ0LCAncmVuZGVyJykpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uanVzdGlmeUNlbnRlcicsIGZ1bmMuaW52b2tlKGp1c3RpZnlDZW50ZXIsICdyZW5kZXInKSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5qdXN0aWZ5UmlnaHQnLCBmdW5jLmludm9rZShqdXN0aWZ5UmlnaHQsICdyZW5kZXInKSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5qdXN0aWZ5RnVsbCcsIGZ1bmMuaW52b2tlKGp1c3RpZnlGdWxsLCAncmVuZGVyJykpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ub3V0ZGVudCcsIGZ1bmMuaW52b2tlKG91dGRlbnQsICdyZW5kZXInKSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5pbmRlbnQnLCBmdW5jLmludm9rZShpbmRlbnQsICdyZW5kZXInKSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5wYXJhZ3JhcGgnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy51aS5idXR0b25Hcm91cChbXG4gICAgICAgICAgICAgICAgICBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2Ryb3Bkb3duLXRvZ2dsZScsXG4gICAgICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmRyb3Bkb3duQnV0dG9uQ29udGVudHMoX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLmFsaWduTGVmdCksIF90aGlzLm9wdGlvbnMpLFxuICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcucGFyYWdyYXBoLnBhcmFncmFwaCxcbiAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRvZ2dsZTogJ2Ryb3Bkb3duJ1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgX3RoaXMudWkuZHJvcGRvd24oW1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLnVpLmJ1dHRvbkdyb3VwKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS1hbGlnbicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuOiBbanVzdGlmeUxlZnQsIGp1c3RpZnlDZW50ZXIsIGp1c3RpZnlSaWdodCwganVzdGlmeUZ1bGxdXG4gICAgICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMudWkuYnV0dG9uR3JvdXAoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWxpc3QnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBjaGlsZHJlbjogW291dGRlbnQsIGluZGVudF1cbiAgICAgICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIF0pLFxuICAgICAgICAgICAgICBdKS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmhlaWdodCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLnVpLmJ1dHRvbkdyb3VwKFtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnZHJvcGRvd24tdG9nZ2xlJyxcbiAgICAgICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuZHJvcGRvd25CdXR0b25Db250ZW50cyhfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMudGV4dEhlaWdodCksIF90aGlzLm9wdGlvbnMpLFxuICAgICAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuZm9udC5oZWlnaHQsXG4gICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0b2dnbGU6ICdkcm9wZG93bidcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgICAgICAgIF90aGlzLnVpLmRyb3Bkb3duQ2hlY2soe1xuICAgICAgICAgICAgICAgICAgICAgIGl0ZW1zOiBfdGhpcy5vcHRpb25zLmxpbmVIZWlnaHRzLFxuICAgICAgICAgICAgICAgICAgICAgIGNoZWNrQ2xhc3NOYW1lOiBfdGhpcy5vcHRpb25zLmljb25zLm1lbnVDaGVjayxcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdkcm9wZG93bi1saW5lLWhlaWdodCcsXG4gICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IF90aGlzLmxhbmcuZm9udC5oZWlnaHQsXG4gICAgICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmxpbmVIZWlnaHQnKVxuICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24udGFibGUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy51aS5idXR0b25Hcm91cChbXG4gICAgICAgICAgICAgICAgICBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2Ryb3Bkb3duLXRvZ2dsZScsXG4gICAgICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmRyb3Bkb3duQnV0dG9uQ29udGVudHMoX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnRhYmxlKSwgX3RoaXMub3B0aW9ucyksXG4gICAgICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy50YWJsZS50YWJsZSxcbiAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRvZ2dsZTogJ2Ryb3Bkb3duJ1xuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgICAgICAgX3RoaXMudWkuZHJvcGRvd24oe1xuICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBfdGhpcy5sYW5nLnRhYmxlLnRhYmxlLFxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ25vdGUtdGFibGUnLFxuICAgICAgICAgICAgICAgICAgICAgIGl0ZW1zOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwibm90ZS1kaW1lbnNpb24tcGlja2VyXCI+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdiBjbGFzcz1cIm5vdGUtZGltZW5zaW9uLXBpY2tlci1tb3VzZWNhdGNoZXJcIiBkYXRhLWV2ZW50PVwiaW5zZXJ0VGFibGVcIiBkYXRhLXZhbHVlPVwiMXgxXCIvPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICcgIDxkaXYgY2xhc3M9XCJub3RlLWRpbWVuc2lvbi1waWNrZXItaGlnaGxpZ2h0ZWRcIi8+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJyAgPGRpdiBjbGFzcz1cIm5vdGUtZGltZW5zaW9uLXBpY2tlci11bmhpZ2hsaWdodGVkXCIvPicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cIm5vdGUtZGltZW5zaW9uLWRpc3BsYXlcIj4xIHggMTwvZGl2PicsXG4gICAgICAgICAgICAgICAgICAgICAgXS5qb2luKCcnKVxuICAgICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICAgIF0sIHtcbiAgICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoJG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICB2YXIgJGNhdGNoZXIgPSAkbm9kZS5maW5kKCcubm90ZS1kaW1lbnNpb24tcGlja2VyLW1vdXNlY2F0Y2hlcicpO1xuICAgICAgICAgICAgICAgICAgICAgICRjYXRjaGVyLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBfdGhpcy5vcHRpb25zLmluc2VydFRhYmxlTWF4U2l6ZS5jb2wgKyAnZW0nLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IF90aGlzLm9wdGlvbnMuaW5zZXJ0VGFibGVNYXhTaXplLnJvdyArICdlbSdcbiAgICAgICAgICAgICAgICAgICAgICB9KS5tb3VzZWRvd24oX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyKCdlZGl0b3IuaW5zZXJ0VGFibGUnKSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLm9uKCdtb3VzZW1vdmUnLCBfdGhpcy50YWJsZU1vdmVIYW5kbGVyLmJpbmQoX3RoaXMpKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5saW5rJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMubGluayksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmxpbmsubGluayArIF90aGlzLnJlcHJlc2VudFNob3J0Y3V0KCdsaW5rRGlhbG9nLnNob3cnKSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2xpbmtEaWFsb2cuc2hvdycpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucGljdHVyZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnBpY3R1cmUpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5pbWFnZS5pbWFnZSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2ltYWdlRGlhbG9nLnNob3cnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLnZpZGVvJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMudmlkZW8pLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy52aWRlby52aWRlbyxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ3ZpZGVvRGlhbG9nLnNob3cnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmhyJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMubWludXMpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5oci5pbnNlcnQgKyBfdGhpcy5yZXByZXNlbnRTaG9ydGN1dCgnaW5zZXJ0SG9yaXpvbnRhbFJ1bGUnKSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5pbnNlcnRIb3Jpem9udGFsUnVsZScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uZnVsbHNjcmVlbicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdidG4tZnVsbHNjcmVlbicsXG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLmFycm93c0FsdCksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLm9wdGlvbnMuZnVsbHNjcmVlbixcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2Z1bGxzY3JlZW4udG9nZ2xlJylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5jb2RldmlldycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdidG4tY29kZXZpZXcnLFxuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5jb2RlKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcub3B0aW9ucy5jb2RldmlldyxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2NvZGV2aWV3LnRvZ2dsZScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucmVkbycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnJlZG8pLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5oaXN0b3J5LnJlZG8gKyBfdGhpcy5yZXByZXNlbnRTaG9ydGN1dCgncmVkbycpLFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLnJlZG8nKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLnVuZG8nLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy51bmRvKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuaGlzdG9yeS51bmRvICsgX3RoaXMucmVwcmVzZW50U2hvcnRjdXQoJ3VuZG8nKSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci51bmRvJylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5oZWxwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMucXVlc3Rpb24pLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5vcHRpb25zLmhlbHAsXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyKCdoZWxwRGlhbG9nLnNob3cnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIGltYWdlOiBbXG4gICAgICAgKiAgIFsnaW1hZ2VSZXNpemUnLCBbJ3Jlc2l6ZUZ1bGwnLCAncmVzaXplSGFsZicsICdyZXNpemVRdWFydGVyJywgJ3Jlc2l6ZU5vbmUnXV0sXG4gICAgICAgKiAgIFsnZmxvYXQnLCBbJ2Zsb2F0TGVmdCcsICdmbG9hdFJpZ2h0JywgJ2Zsb2F0Tm9uZSddXSxcbiAgICAgICAqICAgWydyZW1vdmUnLCBbJ3JlbW92ZU1lZGlhJ11dLFxuICAgICAgICogXSxcbiAgICAgICAqL1xuICAgICAgQnV0dG9ucy5wcm90b3R5cGUuYWRkSW1hZ2VQb3BvdmVyQnV0dG9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIC8vIEltYWdlIFNpemUgQnV0dG9uc1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucmVzaXplRnVsbCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogJzxzcGFuIGNsYXNzPVwibm90ZS1mb250c2l6ZS0xMFwiPjEwMCU8L3NwYW4+JyxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuaW1hZ2UucmVzaXplRnVsbCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5yZXNpemUnLCAnMScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucmVzaXplSGFsZicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogJzxzcGFuIGNsYXNzPVwibm90ZS1mb250c2l6ZS0xMFwiPjUwJTwvc3Bhbj4nLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5pbWFnZS5yZXNpemVIYWxmLFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLnJlc2l6ZScsICcwLjUnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLnJlc2l6ZVF1YXJ0ZXInLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6ICc8c3BhbiBjbGFzcz1cIm5vdGUtZm9udHNpemUtMTBcIj4yNSU8L3NwYW4+JyxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuaW1hZ2UucmVzaXplUXVhcnRlcixcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5yZXNpemUnLCAnMC4yNScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucmVzaXplTm9uZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnJvbGxiYWNrKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuaW1hZ2UucmVzaXplTm9uZSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5yZXNpemUnLCAnMCcpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIC8vIEZsb2F0IEJ1dHRvbnNcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmZsb2F0TGVmdCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLmZsb2F0TGVmdCksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmltYWdlLmZsb2F0TGVmdCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5mbG9hdE1lJywgJ2xlZnQnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmZsb2F0UmlnaHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5mbG9hdFJpZ2h0KSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcuaW1hZ2UuZmxvYXRSaWdodCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5mbG9hdE1lJywgJ3JpZ2h0JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5mbG9hdE5vbmUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5yb2xsYmFjayksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmltYWdlLmZsb2F0Tm9uZSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5mbG9hdE1lJywgJ25vbmUnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyBSZW1vdmUgQnV0dG9uc1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ucmVtb3ZlTWVkaWEnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy50cmFzaCksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLmltYWdlLnJlbW92ZSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5yZW1vdmVNZWRpYScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLmFkZExpbmtQb3BvdmVyQnV0dG9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24ubGlua0RpYWxvZ1Nob3cnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5saW5rKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcubGluay5lZGl0LFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignbGlua0RpYWxvZy5zaG93JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi51bmxpbmsnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy51bmxpbmspLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy5saW5rLnVubGluayxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci51bmxpbmsnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHRhYmxlIDogW1xuICAgICAgICogIFsnYWRkJywgWydhZGRSb3dEb3duJywgJ2FkZFJvd1VwJywgJ2FkZENvbExlZnQnLCAnYWRkQ29sUmlnaHQnXV0sXG4gICAgICAgKiAgWydkZWxldGUnLCBbJ2RlbGV0ZVJvdycsICdkZWxldGVDb2wnLCAnZGVsZXRlVGFibGUnXV1cbiAgICAgICAqIF0sXG4gICAgICAgKi9cbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLmFkZFRhYmxlUG9wb3ZlckJ1dHRvbnMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmFkZFJvd1VwJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuYnV0dG9uKHtcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2J0bi1tZCcsXG4gICAgICAgICAgICAgICAgICBjb250ZW50czogX3RoaXMudWkuaWNvbihfdGhpcy5vcHRpb25zLmljb25zLnJvd0Fib3ZlKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcudGFibGUuYWRkUm93QWJvdmUsXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyKCdlZGl0b3IuYWRkUm93JywgJ3RvcCcpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uYWRkUm93RG93bicsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdidG4tbWQnLFxuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5yb3dCZWxvdyksXG4gICAgICAgICAgICAgICAgICB0b29sdGlwOiBfdGhpcy5sYW5nLnRhYmxlLmFkZFJvd0JlbG93LFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmFkZFJvdycsICdib3R0b20nKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmFkZENvbExlZnQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLW1kJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuY29sQmVmb3JlKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcudGFibGUuYWRkQ29sTGVmdCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5hZGRDb2wnLCAnbGVmdCcpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uYWRkQ29sUmlnaHQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLW1kJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuY29sQWZ0ZXIpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy50YWJsZS5hZGRDb2xSaWdodCxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5hZGRDb2wnLCAncmlnaHQnKVxuICAgICAgICAgICAgICB9KS5yZW5kZXIoKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQubWVtbygnYnV0dG9uLmRlbGV0ZVJvdycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmJ1dHRvbih7XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICdidG4tbWQnLFxuICAgICAgICAgICAgICAgICAgY29udGVudHM6IF90aGlzLnVpLmljb24oX3RoaXMub3B0aW9ucy5pY29ucy5yb3dSZW1vdmUpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy50YWJsZS5kZWxSb3csXG4gICAgICAgICAgICAgICAgICBjbGljazogX3RoaXMuY29udGV4dC5jcmVhdGVJbnZva2VIYW5kbGVyKCdlZGl0b3IuZGVsZXRlUm93JylcbiAgICAgICAgICAgICAgfSkucmVuZGVyKCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lm1lbW8oJ2J1dHRvbi5kZWxldGVDb2wnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLW1kJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMuY29sUmVtb3ZlKSxcbiAgICAgICAgICAgICAgICAgIHRvb2x0aXA6IF90aGlzLmxhbmcudGFibGUuZGVsQ29sLFxuICAgICAgICAgICAgICAgICAgY2xpY2s6IF90aGlzLmNvbnRleHQuY3JlYXRlSW52b2tlSGFuZGxlcignZWRpdG9yLmRlbGV0ZUNvbCcpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uZGVsZXRlVGFibGUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5idXR0b24oe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYnRuLW1kJyxcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnRzOiBfdGhpcy51aS5pY29uKF90aGlzLm9wdGlvbnMuaWNvbnMudHJhc2gpLFxuICAgICAgICAgICAgICAgICAgdG9vbHRpcDogX3RoaXMubGFuZy50YWJsZS5kZWxUYWJsZSxcbiAgICAgICAgICAgICAgICAgIGNsaWNrOiBfdGhpcy5jb250ZXh0LmNyZWF0ZUludm9rZUhhbmRsZXIoJ2VkaXRvci5kZWxldGVUYWJsZScpXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLmJ1aWxkID0gZnVuY3Rpb24gKCRjb250YWluZXIsIGdyb3Vwcykge1xuICAgICAgICAgIGZvciAodmFyIGdyb3VwSWR4ID0gMCwgZ3JvdXBMZW4gPSBncm91cHMubGVuZ3RoOyBncm91cElkeCA8IGdyb3VwTGVuOyBncm91cElkeCsrKSB7XG4gICAgICAgICAgICAgIHZhciBncm91cCA9IGdyb3Vwc1tncm91cElkeF07XG4gICAgICAgICAgICAgIHZhciBncm91cE5hbWUgPSBBcnJheS5pc0FycmF5KGdyb3VwKSA/IGdyb3VwWzBdIDogZ3JvdXA7XG4gICAgICAgICAgICAgIHZhciBidXR0b25zID0gQXJyYXkuaXNBcnJheShncm91cCkgPyAoKGdyb3VwLmxlbmd0aCA9PT0gMSkgPyBbZ3JvdXBbMF1dIDogZ3JvdXBbMV0pIDogW2dyb3VwXTtcbiAgICAgICAgICAgICAgdmFyICRncm91cCA9IHRoaXMudWkuYnV0dG9uR3JvdXAoe1xuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS0nICsgZ3JvdXBOYW1lXG4gICAgICAgICAgICAgIH0pLnJlbmRlcigpO1xuICAgICAgICAgICAgICBmb3IgKHZhciBpZHggPSAwLCBsZW4gPSBidXR0b25zLmxlbmd0aDsgaWR4IDwgbGVuOyBpZHgrKykge1xuICAgICAgICAgICAgICAgICAgdmFyIGJ0biA9IHRoaXMuY29udGV4dC5tZW1vKCdidXR0b24uJyArIGJ1dHRvbnNbaWR4XSk7XG4gICAgICAgICAgICAgICAgICBpZiAoYnRuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgJGdyb3VwLmFwcGVuZCh0eXBlb2YgYnRuID09PSAnZnVuY3Rpb24nID8gYnRuKCkgOiBidG4pO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICRncm91cC5hcHBlbmRUbygkY29udGFpbmVyKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBAcGFyYW0ge2pRdWVyeX0gWyRjb250YWluZXJdXG4gICAgICAgKi9cbiAgICAgIEJ1dHRvbnMucHJvdG90eXBlLnVwZGF0ZUN1cnJlbnRTdHlsZSA9IGZ1bmN0aW9uICgkY29udGFpbmVyKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB2YXIgJGNvbnQgPSAkY29udGFpbmVyIHx8IHRoaXMuJHRvb2xiYXI7XG4gICAgICAgICAgdmFyIHN0eWxlSW5mbyA9IHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5jdXJyZW50U3R5bGUnKTtcbiAgICAgICAgICB0aGlzLnVwZGF0ZUJ0blN0YXRlcygkY29udCwge1xuICAgICAgICAgICAgICAnLm5vdGUtYnRuLWJvbGQnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gc3R5bGVJbmZvWydmb250LWJvbGQnXSA9PT0gJ2JvbGQnO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnLm5vdGUtYnRuLWl0YWxpYyc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBzdHlsZUluZm9bJ2ZvbnQtaXRhbGljJ10gPT09ICdpdGFsaWMnO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnLm5vdGUtYnRuLXVuZGVybGluZSc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBzdHlsZUluZm9bJ2ZvbnQtdW5kZXJsaW5lJ10gPT09ICd1bmRlcmxpbmUnO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnLm5vdGUtYnRuLXN1YnNjcmlwdCc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIHJldHVybiBzdHlsZUluZm9bJ2ZvbnQtc3Vic2NyaXB0J10gPT09ICdzdWJzY3JpcHQnO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnLm5vdGUtYnRuLXN1cGVyc2NyaXB0JzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgcmV0dXJuIHN0eWxlSW5mb1snZm9udC1zdXBlcnNjcmlwdCddID09PSAnc3VwZXJzY3JpcHQnO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnLm5vdGUtYnRuLXN0cmlrZXRocm91Z2gnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gc3R5bGVJbmZvWydmb250LXN0cmlrZXRocm91Z2gnXSA9PT0gJ3N0cmlrZXRocm91Z2gnO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgaWYgKHN0eWxlSW5mb1snZm9udC1mYW1pbHknXSkge1xuICAgICAgICAgICAgICB2YXIgZm9udE5hbWVzID0gc3R5bGVJbmZvWydmb250LWZhbWlseSddLnNwbGl0KCcsJykubWFwKGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgICAgICAgICAgICByZXR1cm4gbmFtZS5yZXBsYWNlKC9bXFwnXFxcIl0vZywgJycpXG4gICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL1xccyskLywgJycpXG4gICAgICAgICAgICAgICAgICAgICAgLnJlcGxhY2UoL15cXHMrLywgJycpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgdmFyIGZvbnROYW1lXzEgPSBsaXN0cy5maW5kKGZvbnROYW1lcywgdGhpcy5pc0ZvbnRJbnN0YWxsZWQuYmluZCh0aGlzKSk7XG4gICAgICAgICAgICAgICRjb250LmZpbmQoJy5kcm9wZG93bi1mb250bmFtZSBhJykuZWFjaChmdW5jdGlvbiAoaWR4LCBpdGVtKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgJGl0ZW0gPSAkJDEoaXRlbSk7XG4gICAgICAgICAgICAgICAgICAvLyBhbHdheXMgY29tcGFyZSBzdHJpbmcgdG8gYXZvaWQgY3JlYXRpbmcgYW5vdGhlciBmdW5jLlxuICAgICAgICAgICAgICAgICAgdmFyIGlzQ2hlY2tlZCA9ICgkaXRlbS5kYXRhKCd2YWx1ZScpICsgJycpID09PSAoZm9udE5hbWVfMSArICcnKTtcbiAgICAgICAgICAgICAgICAgICRpdGVtLnRvZ2dsZUNsYXNzKCdjaGVja2VkJywgaXNDaGVja2VkKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICRjb250LmZpbmQoJy5ub3RlLWN1cnJlbnQtZm9udG5hbWUnKS50ZXh0KGZvbnROYW1lXzEpLmNzcygnZm9udC1mYW1pbHknLCBmb250TmFtZV8xKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgaWYgKHN0eWxlSW5mb1snZm9udC1zaXplJ10pIHtcbiAgICAgICAgICAgICAgdmFyIGZvbnRTaXplXzEgPSBzdHlsZUluZm9bJ2ZvbnQtc2l6ZSddO1xuICAgICAgICAgICAgICAkY29udC5maW5kKCcuZHJvcGRvd24tZm9udHNpemUgYScpLmVhY2goZnVuY3Rpb24gKGlkeCwgaXRlbSkge1xuICAgICAgICAgICAgICAgICAgdmFyICRpdGVtID0gJCQxKGl0ZW0pO1xuICAgICAgICAgICAgICAgICAgLy8gYWx3YXlzIGNvbXBhcmUgd2l0aCBzdHJpbmcgdG8gYXZvaWQgY3JlYXRpbmcgYW5vdGhlciBmdW5jLlxuICAgICAgICAgICAgICAgICAgdmFyIGlzQ2hlY2tlZCA9ICgkaXRlbS5kYXRhKCd2YWx1ZScpICsgJycpID09PSAoZm9udFNpemVfMSArICcnKTtcbiAgICAgICAgICAgICAgICAgICRpdGVtLnRvZ2dsZUNsYXNzKCdjaGVja2VkJywgaXNDaGVja2VkKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICRjb250LmZpbmQoJy5ub3RlLWN1cnJlbnQtZm9udHNpemUnKS50ZXh0KGZvbnRTaXplXzEpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoc3R5bGVJbmZvWydsaW5lLWhlaWdodCddKSB7XG4gICAgICAgICAgICAgIHZhciBsaW5lSGVpZ2h0XzEgPSBzdHlsZUluZm9bJ2xpbmUtaGVpZ2h0J107XG4gICAgICAgICAgICAgICRjb250LmZpbmQoJy5kcm9wZG93bi1saW5lLWhlaWdodCBsaSBhJykuZWFjaChmdW5jdGlvbiAoaWR4LCBpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAvLyBhbHdheXMgY29tcGFyZSB3aXRoIHN0cmluZyB0byBhdm9pZCBjcmVhdGluZyBhbm90aGVyIGZ1bmMuXG4gICAgICAgICAgICAgICAgICB2YXIgaXNDaGVja2VkID0gKCQkMShpdGVtKS5kYXRhKCd2YWx1ZScpICsgJycpID09PSAobGluZUhlaWdodF8xICsgJycpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY2xhc3NOYW1lID0gaXNDaGVja2VkID8gJ2NoZWNrZWQnIDogJyc7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBCdXR0b25zLnByb3RvdHlwZS51cGRhdGVCdG5TdGF0ZXMgPSBmdW5jdGlvbiAoJGNvbnRhaW5lciwgaW5mb3MpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgICQkMS5lYWNoKGluZm9zLCBmdW5jdGlvbiAoc2VsZWN0b3IsIHByZWQpIHtcbiAgICAgICAgICAgICAgX3RoaXMudWkudG9nZ2xlQnRuQWN0aXZlKCRjb250YWluZXIuZmluZChzZWxlY3RvciksIHByZWQoKSk7XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgQnV0dG9ucy5wcm90b3R5cGUudGFibGVNb3ZlSGFuZGxlciA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgIHZhciBQWF9QRVJfRU0gPSAxODtcbiAgICAgICAgICB2YXIgJHBpY2tlciA9ICQkMShldmVudC50YXJnZXQucGFyZW50Tm9kZSk7IC8vIHRhcmdldCBpcyBtb3VzZWNhdGNoZXJcbiAgICAgICAgICB2YXIgJGRpbWVuc2lvbkRpc3BsYXkgPSAkcGlja2VyLm5leHQoKTtcbiAgICAgICAgICB2YXIgJGNhdGNoZXIgPSAkcGlja2VyLmZpbmQoJy5ub3RlLWRpbWVuc2lvbi1waWNrZXItbW91c2VjYXRjaGVyJyk7XG4gICAgICAgICAgdmFyICRoaWdobGlnaHRlZCA9ICRwaWNrZXIuZmluZCgnLm5vdGUtZGltZW5zaW9uLXBpY2tlci1oaWdobGlnaHRlZCcpO1xuICAgICAgICAgIHZhciAkdW5oaWdobGlnaHRlZCA9ICRwaWNrZXIuZmluZCgnLm5vdGUtZGltZW5zaW9uLXBpY2tlci11bmhpZ2hsaWdodGVkJyk7XG4gICAgICAgICAgdmFyIHBvc09mZnNldDtcbiAgICAgICAgICAvLyBIVE1MNSB3aXRoIGpRdWVyeSAtIGUub2Zmc2V0WCBpcyB1bmRlZmluZWQgaW4gRmlyZWZveFxuICAgICAgICAgIGlmIChldmVudC5vZmZzZXRYID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgdmFyIHBvc0NhdGNoZXIgPSAkJDEoZXZlbnQudGFyZ2V0KS5vZmZzZXQoKTtcbiAgICAgICAgICAgICAgcG9zT2Zmc2V0ID0ge1xuICAgICAgICAgICAgICAgICAgeDogZXZlbnQucGFnZVggLSBwb3NDYXRjaGVyLmxlZnQsXG4gICAgICAgICAgICAgICAgICB5OiBldmVudC5wYWdlWSAtIHBvc0NhdGNoZXIudG9wXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICBwb3NPZmZzZXQgPSB7XG4gICAgICAgICAgICAgICAgICB4OiBldmVudC5vZmZzZXRYLFxuICAgICAgICAgICAgICAgICAgeTogZXZlbnQub2Zmc2V0WVxuICAgICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICB2YXIgZGltID0ge1xuICAgICAgICAgICAgICBjOiBNYXRoLmNlaWwocG9zT2Zmc2V0LnggLyBQWF9QRVJfRU0pIHx8IDEsXG4gICAgICAgICAgICAgIHI6IE1hdGguY2VpbChwb3NPZmZzZXQueSAvIFBYX1BFUl9FTSkgfHwgMVxuICAgICAgICAgIH07XG4gICAgICAgICAgJGhpZ2hsaWdodGVkLmNzcyh7IHdpZHRoOiBkaW0uYyArICdlbScsIGhlaWdodDogZGltLnIgKyAnZW0nIH0pO1xuICAgICAgICAgICRjYXRjaGVyLmRhdGEoJ3ZhbHVlJywgZGltLmMgKyAneCcgKyBkaW0ucik7XG4gICAgICAgICAgaWYgKGRpbS5jID4gMyAmJiBkaW0uYyA8IHRoaXMub3B0aW9ucy5pbnNlcnRUYWJsZU1heFNpemUuY29sKSB7XG4gICAgICAgICAgICAgICR1bmhpZ2hsaWdodGVkLmNzcyh7IHdpZHRoOiBkaW0uYyArIDEgKyAnZW0nIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZGltLnIgPiAzICYmIGRpbS5yIDwgdGhpcy5vcHRpb25zLmluc2VydFRhYmxlTWF4U2l6ZS5yb3cpIHtcbiAgICAgICAgICAgICAgJHVuaGlnaGxpZ2h0ZWQuY3NzKHsgaGVpZ2h0OiBkaW0uciArIDEgKyAnZW0nIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICAkZGltZW5zaW9uRGlzcGxheS5odG1sKGRpbS5jICsgJyB4ICcgKyBkaW0ucik7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIEJ1dHRvbnM7XG4gIH0oKSk7XG5cbiAgdmFyIFRvb2xiYXIgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBUb29sYmFyKGNvbnRleHQpIHtcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMuJHdpbmRvdyA9ICQkMSh3aW5kb3cpO1xuICAgICAgICAgIHRoaXMuJGRvY3VtZW50ID0gJCQxKGRvY3VtZW50KTtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kbm90ZSA9IGNvbnRleHQubGF5b3V0SW5mby5ub3RlO1xuICAgICAgICAgIHRoaXMuJGVkaXRvciA9IGNvbnRleHQubGF5b3V0SW5mby5lZGl0b3I7XG4gICAgICAgICAgdGhpcy4kdG9vbGJhciA9IGNvbnRleHQubGF5b3V0SW5mby50b29sYmFyO1xuICAgICAgICAgIHRoaXMuJGVkaXRhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRhYmxlO1xuICAgICAgICAgIHRoaXMuJHN0YXR1c2JhciA9IGNvbnRleHQubGF5b3V0SW5mby5zdGF0dXNiYXI7XG4gICAgICAgICAgdGhpcy5vcHRpb25zID0gY29udGV4dC5vcHRpb25zO1xuICAgICAgICAgIHRoaXMuaXNGb2xsb3dpbmcgPSBmYWxzZTtcbiAgICAgICAgICB0aGlzLmZvbGxvd1Njcm9sbCA9IHRoaXMuZm9sbG93U2Nyb2xsLmJpbmQodGhpcyk7XG4gICAgICB9XG4gICAgICBUb29sYmFyLnByb3RvdHlwZS5zaG91bGRJbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiAhdGhpcy5vcHRpb25zLmFpck1vZGU7XG4gICAgICB9O1xuICAgICAgVG9vbGJhci5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMub3B0aW9ucy50b29sYmFyID0gdGhpcy5vcHRpb25zLnRvb2xiYXIgfHwgW107XG4gICAgICAgICAgaWYgKCF0aGlzLm9wdGlvbnMudG9vbGJhci5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgdGhpcy4kdG9vbGJhci5oaWRlKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmNvbnRleHQuaW52b2tlKCdidXR0b25zLmJ1aWxkJywgdGhpcy4kdG9vbGJhciwgdGhpcy5vcHRpb25zLnRvb2xiYXIpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLnRvb2xiYXJDb250YWluZXIpIHtcbiAgICAgICAgICAgICAgdGhpcy4kdG9vbGJhci5hcHBlbmRUbyh0aGlzLm9wdGlvbnMudG9vbGJhckNvbnRhaW5lcik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuY2hhbmdlQ29udGFpbmVyKGZhbHNlKTtcbiAgICAgICAgICB0aGlzLiRub3RlLm9uKCdzdW1tZXJub3RlLmtleXVwIHN1bW1lcm5vdGUubW91c2V1cCBzdW1tZXJub3RlLmNoYW5nZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2J1dHRvbnMudXBkYXRlQ3VycmVudFN0eWxlJyk7XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnYnV0dG9ucy51cGRhdGVDdXJyZW50U3R5bGUnKTtcbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmZvbGxvd2luZ1Rvb2xiYXIpIHtcbiAgICAgICAgICAgICAgdGhpcy4kd2luZG93Lm9uKCdzY3JvbGwgcmVzaXplJywgdGhpcy5mb2xsb3dTY3JvbGwpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBUb29sYmFyLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHRvb2xiYXIuY2hpbGRyZW4oKS5yZW1vdmUoKTtcbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLmZvbGxvd2luZ1Rvb2xiYXIpIHtcbiAgICAgICAgICAgICAgdGhpcy4kd2luZG93Lm9mZignc2Nyb2xsIHJlc2l6ZScsIHRoaXMuZm9sbG93U2Nyb2xsKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgVG9vbGJhci5wcm90b3R5cGUuZm9sbG93U2Nyb2xsID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIGlmICh0aGlzLiRlZGl0b3IuaGFzQ2xhc3MoJ2Z1bGxzY3JlZW4nKSkge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBlZGl0b3JIZWlnaHQgPSB0aGlzLiRlZGl0b3Iub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgICB2YXIgZWRpdG9yV2lkdGggPSB0aGlzLiRlZGl0b3Iud2lkdGgoKTtcbiAgICAgICAgICB2YXIgdG9vbGJhckhlaWdodCA9IHRoaXMuJHRvb2xiYXIuaGVpZ2h0KCk7XG4gICAgICAgICAgdmFyIHN0YXR1c2JhckhlaWdodCA9IHRoaXMuJHN0YXR1c2Jhci5oZWlnaHQoKTtcbiAgICAgICAgICAvLyBjaGVjayBpZiB0aGUgd2ViIGFwcCBpcyBjdXJyZW50bHkgdXNpbmcgYW5vdGhlciBzdGF0aWMgYmFyXG4gICAgICAgICAgdmFyIG90aGVyQmFySGVpZ2h0ID0gMDtcbiAgICAgICAgICBpZiAodGhpcy5vcHRpb25zLm90aGVyU3RhdGljQmFyKSB7XG4gICAgICAgICAgICAgIG90aGVyQmFySGVpZ2h0ID0gJCQxKHRoaXMub3B0aW9ucy5vdGhlclN0YXRpY0Jhcikub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdmFyIGN1cnJlbnRPZmZzZXQgPSB0aGlzLiRkb2N1bWVudC5zY3JvbGxUb3AoKTtcbiAgICAgICAgICB2YXIgZWRpdG9yT2Zmc2V0VG9wID0gdGhpcy4kZWRpdG9yLm9mZnNldCgpLnRvcDtcbiAgICAgICAgICB2YXIgZWRpdG9yT2Zmc2V0Qm90dG9tID0gZWRpdG9yT2Zmc2V0VG9wICsgZWRpdG9ySGVpZ2h0O1xuICAgICAgICAgIHZhciBhY3RpdmF0ZU9mZnNldCA9IGVkaXRvck9mZnNldFRvcCAtIG90aGVyQmFySGVpZ2h0O1xuICAgICAgICAgIHZhciBkZWFjdGl2YXRlT2Zmc2V0Qm90dG9tID0gZWRpdG9yT2Zmc2V0Qm90dG9tIC0gb3RoZXJCYXJIZWlnaHQgLSB0b29sYmFySGVpZ2h0IC0gc3RhdHVzYmFySGVpZ2h0O1xuICAgICAgICAgIGlmICghdGhpcy5pc0ZvbGxvd2luZyAmJlxuICAgICAgICAgICAgICAoY3VycmVudE9mZnNldCA+IGFjdGl2YXRlT2Zmc2V0KSAmJiAoY3VycmVudE9mZnNldCA8IGRlYWN0aXZhdGVPZmZzZXRCb3R0b20gLSB0b29sYmFySGVpZ2h0KSkge1xuICAgICAgICAgICAgICB0aGlzLmlzRm9sbG93aW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgdGhpcy4kdG9vbGJhci5jc3Moe1xuICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICAgICAgICAgICAgICB0b3A6IG90aGVyQmFySGVpZ2h0LFxuICAgICAgICAgICAgICAgICAgd2lkdGg6IGVkaXRvcldpZHRoXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICB0aGlzLiRlZGl0YWJsZS5jc3Moe1xuICAgICAgICAgICAgICAgICAgbWFyZ2luVG9wOiB0aGlzLiR0b29sYmFyLmhlaWdodCgpICsgNVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAodGhpcy5pc0ZvbGxvd2luZyAmJlxuICAgICAgICAgICAgICAoKGN1cnJlbnRPZmZzZXQgPCBhY3RpdmF0ZU9mZnNldCkgfHwgKGN1cnJlbnRPZmZzZXQgPiBkZWFjdGl2YXRlT2Zmc2V0Qm90dG9tKSkpIHtcbiAgICAgICAgICAgICAgdGhpcy5pc0ZvbGxvd2luZyA9IGZhbHNlO1xuICAgICAgICAgICAgICB0aGlzLiR0b29sYmFyLmNzcyh7XG4gICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgICAgICAgICAgICAgIHRvcDogMCxcbiAgICAgICAgICAgICAgICAgIHdpZHRoOiAnMTAwJSdcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIHRoaXMuJGVkaXRhYmxlLmNzcyh7XG4gICAgICAgICAgICAgICAgICBtYXJnaW5Ub3A6ICcnXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBUb29sYmFyLnByb3RvdHlwZS5jaGFuZ2VDb250YWluZXIgPSBmdW5jdGlvbiAoaXNGdWxsc2NyZWVuKSB7XG4gICAgICAgICAgaWYgKGlzRnVsbHNjcmVlbikge1xuICAgICAgICAgICAgICB0aGlzLiR0b29sYmFyLnByZXBlbmRUbyh0aGlzLiRlZGl0b3IpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy50b29sYmFyQ29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLiR0b29sYmFyLmFwcGVuZFRvKHRoaXMub3B0aW9ucy50b29sYmFyQ29udGFpbmVyKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLmZvbGxvd1Njcm9sbCgpO1xuICAgICAgfTtcbiAgICAgIFRvb2xiYXIucHJvdG90eXBlLnVwZGF0ZUZ1bGxzY3JlZW4gPSBmdW5jdGlvbiAoaXNGdWxsc2NyZWVuKSB7XG4gICAgICAgICAgdGhpcy51aS50b2dnbGVCdG5BY3RpdmUodGhpcy4kdG9vbGJhci5maW5kKCcuYnRuLWZ1bGxzY3JlZW4nKSwgaXNGdWxsc2NyZWVuKTtcbiAgICAgICAgICB0aGlzLmNoYW5nZUNvbnRhaW5lcihpc0Z1bGxzY3JlZW4pO1xuICAgICAgfTtcbiAgICAgIFRvb2xiYXIucHJvdG90eXBlLnVwZGF0ZUNvZGV2aWV3ID0gZnVuY3Rpb24gKGlzQ29kZXZpZXcpIHtcbiAgICAgICAgICB0aGlzLnVpLnRvZ2dsZUJ0bkFjdGl2ZSh0aGlzLiR0b29sYmFyLmZpbmQoJy5idG4tY29kZXZpZXcnKSwgaXNDb2Rldmlldyk7XG4gICAgICAgICAgaWYgKGlzQ29kZXZpZXcpIHtcbiAgICAgICAgICAgICAgdGhpcy5kZWFjdGl2YXRlKCk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmFjdGl2YXRlKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIFRvb2xiYXIucHJvdG90eXBlLmFjdGl2YXRlID0gZnVuY3Rpb24gKGlzSW5jbHVkZUNvZGV2aWV3KSB7XG4gICAgICAgICAgdmFyICRidG4gPSB0aGlzLiR0b29sYmFyLmZpbmQoJ2J1dHRvbicpO1xuICAgICAgICAgIGlmICghaXNJbmNsdWRlQ29kZXZpZXcpIHtcbiAgICAgICAgICAgICAgJGJ0biA9ICRidG4ubm90KCcuYnRuLWNvZGV2aWV3Jyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMudWkudG9nZ2xlQnRuKCRidG4sIHRydWUpO1xuICAgICAgfTtcbiAgICAgIFRvb2xiYXIucHJvdG90eXBlLmRlYWN0aXZhdGUgPSBmdW5jdGlvbiAoaXNJbmNsdWRlQ29kZXZpZXcpIHtcbiAgICAgICAgICB2YXIgJGJ0biA9IHRoaXMuJHRvb2xiYXIuZmluZCgnYnV0dG9uJyk7XG4gICAgICAgICAgaWYgKCFpc0luY2x1ZGVDb2Rldmlldykge1xuICAgICAgICAgICAgICAkYnRuID0gJGJ0bi5ub3QoJy5idG4tY29kZXZpZXcnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy51aS50b2dnbGVCdG4oJGJ0biwgZmFsc2UpO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBUb29sYmFyO1xuICB9KCkpO1xuXG4gIHZhciBMaW5rRGlhbG9nID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gTGlua0RpYWxvZyhjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kYm9keSA9ICQkMShkb2N1bWVudC5ib2R5KTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICAgICAgY29udGV4dC5tZW1vKCdoZWxwLmxpbmtEaWFsb2cuc2hvdycsIHRoaXMub3B0aW9ucy5sYW5nSW5mby5oZWxwWydsaW5rRGlhbG9nLnNob3cnXSk7XG4gICAgICB9XG4gICAgICBMaW5rRGlhbG9nLnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciAkY29udGFpbmVyID0gdGhpcy5vcHRpb25zLmRpYWxvZ3NJbkJvZHkgPyB0aGlzLiRib2R5IDogdGhpcy4kZWRpdG9yO1xuICAgICAgICAgIHZhciBib2R5ID0gW1xuICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgbm90ZS1mb3JtLWdyb3VwXCI+JyxcbiAgICAgICAgICAgICAgXCI8bGFiZWwgY2xhc3M9XFxcIm5vdGUtZm9ybS1sYWJlbFxcXCI+XCIgKyB0aGlzLmxhbmcubGluay50ZXh0VG9EaXNwbGF5ICsgXCI8L2xhYmVsPlwiLFxuICAgICAgICAgICAgICAnPGlucHV0IGNsYXNzPVwibm90ZS1saW5rLXRleHQgZm9ybS1jb250cm9sIG5vdGUtZm9ybS1jb250cm9sIG5vdGUtaW5wdXRcIiB0eXBlPVwidGV4dFwiIC8+JyxcbiAgICAgICAgICAgICAgJzwvZGl2PicsXG4gICAgICAgICAgICAgICc8ZGl2IGNsYXNzPVwiZm9ybS1ncm91cCBub3RlLWZvcm0tZ3JvdXBcIj4nLFxuICAgICAgICAgICAgICBcIjxsYWJlbCBjbGFzcz1cXFwibm90ZS1mb3JtLWxhYmVsXFxcIj5cIiArIHRoaXMubGFuZy5saW5rLnVybCArIFwiPC9sYWJlbD5cIixcbiAgICAgICAgICAgICAgJzxpbnB1dCBjbGFzcz1cIm5vdGUtbGluay11cmwgZm9ybS1jb250cm9sIG5vdGUtZm9ybS1jb250cm9sIG5vdGUtaW5wdXRcIiB0eXBlPVwidGV4dFwiIHZhbHVlPVwiaHR0cDovL1wiIC8+JyxcbiAgICAgICAgICAgICAgJzwvZGl2PicsXG4gICAgICAgICAgICAgICF0aGlzLm9wdGlvbnMuZGlzYWJsZUxpbmtUYXJnZXRcbiAgICAgICAgICAgICAgICAgID8gJCQxKCc8ZGl2Lz4nKS5hcHBlbmQodGhpcy51aS5jaGVja2JveCh7XG4gICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnc24tY2hlY2tib3gtb3Blbi1pbi1uZXctd2luZG93JyxcbiAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiB0aGlzLmxhbmcubGluay5vcGVuSW5OZXdXaW5kb3csXG4gICAgICAgICAgICAgICAgICAgICAgY2hlY2tlZDogdHJ1ZVxuICAgICAgICAgICAgICAgICAgfSkucmVuZGVyKCkpLmh0bWwoKVxuICAgICAgICAgICAgICAgICAgOiAnJyxcbiAgICAgICAgICBdLmpvaW4oJycpO1xuICAgICAgICAgIHZhciBidXR0b25DbGFzcyA9ICdidG4gYnRuLXByaW1hcnkgbm90ZS1idG4gbm90ZS1idG4tcHJpbWFyeSBub3RlLWxpbmstYnRuJztcbiAgICAgICAgICB2YXIgZm9vdGVyID0gXCI8aW5wdXQgdHlwZT1cXFwiYnV0dG9uXFxcIiBocmVmPVxcXCIjXFxcIiBjbGFzcz1cXFwiXCIgKyBidXR0b25DbGFzcyArIFwiXFxcIiB2YWx1ZT1cXFwiXCIgKyB0aGlzLmxhbmcubGluay5pbnNlcnQgKyBcIlxcXCIgZGlzYWJsZWQ+XCI7XG4gICAgICAgICAgdGhpcy4kZGlhbG9nID0gdGhpcy51aS5kaWFsb2coe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6ICdsaW5rLWRpYWxvZycsXG4gICAgICAgICAgICAgIHRpdGxlOiB0aGlzLmxhbmcubGluay5pbnNlcnQsXG4gICAgICAgICAgICAgIGZhZGU6IHRoaXMub3B0aW9ucy5kaWFsb2dzRmFkZSxcbiAgICAgICAgICAgICAgYm9keTogYm9keSxcbiAgICAgICAgICAgICAgZm9vdGVyOiBmb290ZXJcbiAgICAgICAgICB9KS5yZW5kZXIoKS5hcHBlbmRUbygkY29udGFpbmVyKTtcbiAgICAgIH07XG4gICAgICBMaW5rRGlhbG9nLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMudWkuaGlkZURpYWxvZyh0aGlzLiRkaWFsb2cpO1xuICAgICAgICAgIHRoaXMuJGRpYWxvZy5yZW1vdmUoKTtcbiAgICAgIH07XG4gICAgICBMaW5rRGlhbG9nLnByb3RvdHlwZS5iaW5kRW50ZXJLZXkgPSBmdW5jdGlvbiAoJGlucHV0LCAkYnRuKSB7XG4gICAgICAgICAgJGlucHV0Lm9uKCdrZXlwcmVzcycsIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0ga2V5LmNvZGUuRU5URVIpIHtcbiAgICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAkYnRuLnRyaWdnZXIoJ2NsaWNrJyk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHRvZ2dsZSB1cGRhdGUgYnV0dG9uXG4gICAgICAgKi9cbiAgICAgIExpbmtEaWFsb2cucHJvdG90eXBlLnRvZ2dsZUxpbmtCdG4gPSBmdW5jdGlvbiAoJGxpbmtCdG4sICRsaW5rVGV4dCwgJGxpbmtVcmwpIHtcbiAgICAgICAgICB0aGlzLnVpLnRvZ2dsZUJ0bigkbGlua0J0biwgJGxpbmtUZXh0LnZhbCgpICYmICRsaW5rVXJsLnZhbCgpKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIFNob3cgbGluayBkaWFsb2cgYW5kIHNldCBldmVudCBoYW5kbGVycyBvbiBkaWFsb2cgY29udHJvbHMuXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtPYmplY3R9IGxpbmtJbmZvXG4gICAgICAgKiBAcmV0dXJuIHtQcm9taXNlfVxuICAgICAgICovXG4gICAgICBMaW5rRGlhbG9nLnByb3RvdHlwZS5zaG93TGlua0RpYWxvZyA9IGZ1bmN0aW9uIChsaW5rSW5mbykge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgcmV0dXJuICQkMS5EZWZlcnJlZChmdW5jdGlvbiAoZGVmZXJyZWQpIHtcbiAgICAgICAgICAgICAgdmFyICRsaW5rVGV4dCA9IF90aGlzLiRkaWFsb2cuZmluZCgnLm5vdGUtbGluay10ZXh0Jyk7XG4gICAgICAgICAgICAgIHZhciAkbGlua1VybCA9IF90aGlzLiRkaWFsb2cuZmluZCgnLm5vdGUtbGluay11cmwnKTtcbiAgICAgICAgICAgICAgdmFyICRsaW5rQnRuID0gX3RoaXMuJGRpYWxvZy5maW5kKCcubm90ZS1saW5rLWJ0bicpO1xuICAgICAgICAgICAgICB2YXIgJG9wZW5Jbk5ld1dpbmRvdyA9IF90aGlzLiRkaWFsb2dcbiAgICAgICAgICAgICAgICAgIC5maW5kKCcuc24tY2hlY2tib3gtb3Blbi1pbi1uZXctd2luZG93IGlucHV0W3R5cGU9Y2hlY2tib3hdJyk7XG4gICAgICAgICAgICAgIF90aGlzLnVpLm9uRGlhbG9nU2hvd24oX3RoaXMuJGRpYWxvZywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2RpYWxvZy5zaG93bicpO1xuICAgICAgICAgICAgICAgICAgLy8gSWYgbm8gdXJsIHdhcyBnaXZlbiBhbmQgZ2l2ZW4gdGV4dCBpcyB2YWxpZCBVUkwgdGhlbiBjb3B5IHRoYXQgaW50byBVUkwgRmllbGRcbiAgICAgICAgICAgICAgICAgIGlmICghbGlua0luZm8udXJsICYmIGZ1bmMuaXNWYWxpZFVybChsaW5rSW5mby50ZXh0KSkge1xuICAgICAgICAgICAgICAgICAgICAgIGxpbmtJbmZvLnVybCA9IGxpbmtJbmZvLnRleHQ7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAkbGlua1RleHQub24oJ2lucHV0IHBhc3RlIHByb3BlcnR5Y2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgIC8vIElmIGxpbmt0ZXh0IHdhcyBtb2RpZmllZCBieSBpbnB1dCBldmVudHMsXG4gICAgICAgICAgICAgICAgICAgICAgLy8gY2xvbmluZyB0ZXh0IGZyb20gbGlua1VybCB3aWxsIGJlIHN0b3BwZWQuXG4gICAgICAgICAgICAgICAgICAgICAgbGlua0luZm8udGV4dCA9ICRsaW5rVGV4dC52YWwoKTtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy50b2dnbGVMaW5rQnRuKCRsaW5rQnRuLCAkbGlua1RleHQsICRsaW5rVXJsKTtcbiAgICAgICAgICAgICAgICAgIH0pLnZhbChsaW5rSW5mby50ZXh0KTtcbiAgICAgICAgICAgICAgICAgICRsaW5rVXJsLm9uKCdpbnB1dCBwYXN0ZSBwcm9wZXJ0eWNoYW5nZScsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAvLyBEaXNwbGF5IHNhbWUgdGV4dCBvbiBgVGV4dCB0byBkaXNwbGF5YCBhcyBkZWZhdWx0XG4gICAgICAgICAgICAgICAgICAgICAgLy8gd2hlbiBsaW5rdGV4dCBoYXMgbm8gdGV4dFxuICAgICAgICAgICAgICAgICAgICAgIGlmICghbGlua0luZm8udGV4dCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAkbGlua1RleHQudmFsKCRsaW5rVXJsLnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMudG9nZ2xlTGlua0J0bigkbGlua0J0biwgJGxpbmtUZXh0LCAkbGlua1VybCk7XG4gICAgICAgICAgICAgICAgICB9KS52YWwobGlua0luZm8udXJsKTtcbiAgICAgICAgICAgICAgICAgIGlmICghZW52LmlzU3VwcG9ydFRvdWNoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgJGxpbmtVcmwudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIF90aGlzLnRvZ2dsZUxpbmtCdG4oJGxpbmtCdG4sICRsaW5rVGV4dCwgJGxpbmtVcmwpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuYmluZEVudGVyS2V5KCRsaW5rVXJsLCAkbGlua0J0bik7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5iaW5kRW50ZXJLZXkoJGxpbmtUZXh0LCAkbGlua0J0bik7XG4gICAgICAgICAgICAgICAgICB2YXIgaXNOZXdXaW5kb3dDaGVja2VkID0gbGlua0luZm8uaXNOZXdXaW5kb3cgIT09IHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgICAgICAgID8gbGlua0luZm8uaXNOZXdXaW5kb3cgOiBfdGhpcy5jb250ZXh0Lm9wdGlvbnMubGlua1RhcmdldEJsYW5rO1xuICAgICAgICAgICAgICAgICAgJG9wZW5Jbk5ld1dpbmRvdy5wcm9wKCdjaGVja2VkJywgaXNOZXdXaW5kb3dDaGVja2VkKTtcbiAgICAgICAgICAgICAgICAgICRsaW5rQnRuLm9uZSgnY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICByYW5nZTogbGlua0luZm8ucmFuZ2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHVybDogJGxpbmtVcmwudmFsKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRleHQ6ICRsaW5rVGV4dC52YWwoKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaXNOZXdXaW5kb3c6ICRvcGVuSW5OZXdXaW5kb3cuaXMoJzpjaGVja2VkJylcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy51aS5oaWRlRGlhbG9nKF90aGlzLiRkaWFsb2cpO1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICBfdGhpcy51aS5vbkRpYWxvZ0hpZGRlbihfdGhpcy4kZGlhbG9nLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAvLyBkZXRhY2ggZXZlbnRzXG4gICAgICAgICAgICAgICAgICAkbGlua1RleHQub2ZmKCk7XG4gICAgICAgICAgICAgICAgICAkbGlua1VybC5vZmYoKTtcbiAgICAgICAgICAgICAgICAgICRsaW5rQnRuLm9mZigpO1xuICAgICAgICAgICAgICAgICAgaWYgKGRlZmVycmVkLnN0YXRlKCkgPT09ICdwZW5kaW5nJykge1xuICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdCgpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgX3RoaXMudWkuc2hvd0RpYWxvZyhfdGhpcy4kZGlhbG9nKTtcbiAgICAgICAgICB9KS5wcm9taXNlKCk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBAcGFyYW0ge09iamVjdH0gbGF5b3V0SW5mb1xuICAgICAgICovXG4gICAgICBMaW5rRGlhbG9nLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdmFyIGxpbmtJbmZvID0gdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmdldExpbmtJbmZvJyk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLnNhdmVSYW5nZScpO1xuICAgICAgICAgIHRoaXMuc2hvd0xpbmtEaWFsb2cobGlua0luZm8pLnRoZW4oZnVuY3Rpb24gKGxpbmtJbmZvKSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IucmVzdG9yZVJhbmdlJyk7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuY3JlYXRlTGluaycsIGxpbmtJbmZvKTtcbiAgICAgICAgICB9KS5mYWlsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5yZXN0b3JlUmFuZ2UnKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gTGlua0RpYWxvZztcbiAgfSgpKTtcblxuICB2YXIgTGlua1BvcG92ZXIgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBMaW5rUG9wb3Zlcihjb250ZXh0KSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMudWkgPSAkJDEuc3VtbWVybm90ZS51aTtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmtleXVwIHN1bW1lcm5vdGUubW91c2V1cCBzdW1tZXJub3RlLmNoYW5nZSBzdW1tZXJub3RlLnNjcm9sbCc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLnVwZGF0ZSgpO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAnc3VtbWVybm90ZS5kaXNhYmxlIHN1bW1lcm5vdGUuZGlhbG9nLnNob3duJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuaGlkZSgpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIExpbmtQb3BvdmVyLnByb3RvdHlwZS5zaG91bGRJbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHJldHVybiAhbGlzdHMuaXNFbXB0eSh0aGlzLm9wdGlvbnMucG9wb3Zlci5saW5rKTtcbiAgICAgIH07XG4gICAgICBMaW5rUG9wb3Zlci5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwb3BvdmVyID0gdGhpcy51aS5wb3BvdmVyKHtcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnbm90ZS1saW5rLXBvcG92ZXInLFxuICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKCRub2RlKSB7XG4gICAgICAgICAgICAgICAgICB2YXIgJGNvbnRlbnQgPSAkbm9kZS5maW5kKCcucG9wb3Zlci1jb250ZW50LC5ub3RlLXBvcG92ZXItY29udGVudCcpO1xuICAgICAgICAgICAgICAgICAgJGNvbnRlbnQucHJlcGVuZCgnPHNwYW4+PGEgdGFyZ2V0PVwiX2JsYW5rXCI+PC9hPiZuYnNwOzwvc3Bhbj4nKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLnJlbmRlcigpLmFwcGVuZFRvKHRoaXMub3B0aW9ucy5jb250YWluZXIpO1xuICAgICAgICAgIHZhciAkY29udGVudCA9IHRoaXMuJHBvcG92ZXIuZmluZCgnLnBvcG92ZXItY29udGVudCwubm90ZS1wb3BvdmVyLWNvbnRlbnQnKTtcbiAgICAgICAgICB0aGlzLmNvbnRleHQuaW52b2tlKCdidXR0b25zLmJ1aWxkJywgJGNvbnRlbnQsIHRoaXMub3B0aW9ucy5wb3BvdmVyLmxpbmspO1xuICAgICAgfTtcbiAgICAgIExpbmtQb3BvdmVyLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgTGlua1BvcG92ZXIucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyBQcmV2ZW50IGZvY3VzaW5nIG9uIGVkaXRhYmxlIHdoZW4gaW52b2tlKCdjb2RlJykgaXMgZXhlY3V0ZWRcbiAgICAgICAgICBpZiAoIXRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5oYXNGb2N1cycpKSB7XG4gICAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBybmcgPSB0aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuZ2V0TGFzdFJhbmdlJyk7XG4gICAgICAgICAgaWYgKHJuZy5pc0NvbGxhcHNlZCgpICYmIHJuZy5pc09uQW5jaG9yKCkpIHtcbiAgICAgICAgICAgICAgdmFyIGFuY2hvciA9IGRvbS5hbmNlc3Rvcihybmcuc2MsIGRvbS5pc0FuY2hvcik7XG4gICAgICAgICAgICAgIHZhciBocmVmID0gJCQxKGFuY2hvcikuYXR0cignaHJlZicpO1xuICAgICAgICAgICAgICB0aGlzLiRwb3BvdmVyLmZpbmQoJ2EnKS5hdHRyKCdocmVmJywgaHJlZikuaHRtbChocmVmKTtcbiAgICAgICAgICAgICAgdmFyIHBvcyA9IGRvbS5wb3NGcm9tUGxhY2Vob2xkZXIoYW5jaG9yKTtcbiAgICAgICAgICAgICAgdGhpcy4kcG9wb3Zlci5jc3Moe1xuICAgICAgICAgICAgICAgICAgZGlzcGxheTogJ2Jsb2NrJyxcbiAgICAgICAgICAgICAgICAgIGxlZnQ6IHBvcy5sZWZ0LFxuICAgICAgICAgICAgICAgICAgdG9wOiBwb3MudG9wXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdGhpcy5oaWRlKCk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIExpbmtQb3BvdmVyLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIuaGlkZSgpO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBMaW5rUG9wb3ZlcjtcbiAgfSgpKTtcblxuICB2YXIgSW1hZ2VEaWFsb2cgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBJbWFnZURpYWxvZyhjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kYm9keSA9ICQkMShkb2N1bWVudC5ib2R5KTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICB9XG4gICAgICBJbWFnZURpYWxvZy5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgJGNvbnRhaW5lciA9IHRoaXMub3B0aW9ucy5kaWFsb2dzSW5Cb2R5ID8gdGhpcy4kYm9keSA6IHRoaXMuJGVkaXRvcjtcbiAgICAgICAgICB2YXIgaW1hZ2VMaW1pdGF0aW9uID0gJyc7XG4gICAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5tYXhpbXVtSW1hZ2VGaWxlU2l6ZSkge1xuICAgICAgICAgICAgICB2YXIgdW5pdCA9IE1hdGguZmxvb3IoTWF0aC5sb2codGhpcy5vcHRpb25zLm1heGltdW1JbWFnZUZpbGVTaXplKSAvIE1hdGgubG9nKDEwMjQpKTtcbiAgICAgICAgICAgICAgdmFyIHJlYWRhYmxlU2l6ZSA9ICh0aGlzLm9wdGlvbnMubWF4aW11bUltYWdlRmlsZVNpemUgLyBNYXRoLnBvdygxMDI0LCB1bml0KSkudG9GaXhlZCgyKSAqIDEgK1xuICAgICAgICAgICAgICAgICAgJyAnICsgJyBLTUdUUCdbdW5pdF0gKyAnQic7XG4gICAgICAgICAgICAgIGltYWdlTGltaXRhdGlvbiA9IFwiPHNtYWxsPlwiICsgKHRoaXMubGFuZy5pbWFnZS5tYXhpbXVtRmlsZVNpemUgKyAnIDogJyArIHJlYWRhYmxlU2l6ZSkgKyBcIjwvc21hbGw+XCI7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBib2R5ID0gW1xuICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgbm90ZS1mb3JtLWdyb3VwIG5vdGUtZ3JvdXAtc2VsZWN0LWZyb20tZmlsZXNcIj4nLFxuICAgICAgICAgICAgICAnPGxhYmVsIGNsYXNzPVwibm90ZS1mb3JtLWxhYmVsXCI+JyArIHRoaXMubGFuZy5pbWFnZS5zZWxlY3RGcm9tRmlsZXMgKyAnPC9sYWJlbD4nLFxuICAgICAgICAgICAgICAnPGlucHV0IGNsYXNzPVwibm90ZS1pbWFnZS1pbnB1dCBmb3JtLWNvbnRyb2wtZmlsZSBub3RlLWZvcm0tY29udHJvbCBub3RlLWlucHV0XCIgJyxcbiAgICAgICAgICAgICAgJyB0eXBlPVwiZmlsZVwiIG5hbWU9XCJmaWxlc1wiIGFjY2VwdD1cImltYWdlLypcIiBtdWx0aXBsZT1cIm11bHRpcGxlXCIgLz4nLFxuICAgICAgICAgICAgICBpbWFnZUxpbWl0YXRpb24sXG4gICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgICAgICAnPGRpdiBjbGFzcz1cImZvcm0tZ3JvdXAgbm90ZS1ncm91cC1pbWFnZS11cmxcIiBzdHlsZT1cIm92ZXJmbG93OmF1dG87XCI+JyxcbiAgICAgICAgICAgICAgJzxsYWJlbCBjbGFzcz1cIm5vdGUtZm9ybS1sYWJlbFwiPicgKyB0aGlzLmxhbmcuaW1hZ2UudXJsICsgJzwvbGFiZWw+JyxcbiAgICAgICAgICAgICAgJzxpbnB1dCBjbGFzcz1cIm5vdGUtaW1hZ2UtdXJsIGZvcm0tY29udHJvbCBub3RlLWZvcm0tY29udHJvbCBub3RlLWlucHV0ICcsXG4gICAgICAgICAgICAgICcgY29sLW1kLTEyXCIgdHlwZT1cInRleHRcIiAvPicsXG4gICAgICAgICAgICAgICc8L2Rpdj4nLFxuICAgICAgICAgIF0uam9pbignJyk7XG4gICAgICAgICAgdmFyIGJ1dHRvbkNsYXNzID0gJ2J0biBidG4tcHJpbWFyeSBub3RlLWJ0biBub3RlLWJ0bi1wcmltYXJ5IG5vdGUtaW1hZ2UtYnRuJztcbiAgICAgICAgICB2YXIgZm9vdGVyID0gXCI8aW5wdXQgdHlwZT1cXFwiYnV0dG9uXFxcIiBocmVmPVxcXCIjXFxcIiBjbGFzcz1cXFwiXCIgKyBidXR0b25DbGFzcyArIFwiXFxcIiB2YWx1ZT1cXFwiXCIgKyB0aGlzLmxhbmcuaW1hZ2UuaW5zZXJ0ICsgXCJcXFwiIGRpc2FibGVkPlwiO1xuICAgICAgICAgIHRoaXMuJGRpYWxvZyA9IHRoaXMudWkuZGlhbG9nKHtcbiAgICAgICAgICAgICAgdGl0bGU6IHRoaXMubGFuZy5pbWFnZS5pbnNlcnQsXG4gICAgICAgICAgICAgIGZhZGU6IHRoaXMub3B0aW9ucy5kaWFsb2dzRmFkZSxcbiAgICAgICAgICAgICAgYm9keTogYm9keSxcbiAgICAgICAgICAgICAgZm9vdGVyOiBmb290ZXJcbiAgICAgICAgICB9KS5yZW5kZXIoKS5hcHBlbmRUbygkY29udGFpbmVyKTtcbiAgICAgIH07XG4gICAgICBJbWFnZURpYWxvZy5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLnVpLmhpZGVEaWFsb2codGhpcy4kZGlhbG9nKTtcbiAgICAgICAgICB0aGlzLiRkaWFsb2cucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgSW1hZ2VEaWFsb2cucHJvdG90eXBlLmJpbmRFbnRlcktleSA9IGZ1bmN0aW9uICgkaW5wdXQsICRidG4pIHtcbiAgICAgICAgICAkaW5wdXQub24oJ2tleXByZXNzJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgICAgICAgIGlmIChldmVudC5rZXlDb2RlID09PSBrZXkuY29kZS5FTlRFUikge1xuICAgICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICRidG4udHJpZ2dlcignY2xpY2snKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEltYWdlRGlhbG9nLnByb3RvdHlwZS5zaG93ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLnNhdmVSYW5nZScpO1xuICAgICAgICAgIHRoaXMuc2hvd0ltYWdlRGlhbG9nKCkudGhlbihmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAvLyBbd29ya2Fyb3VuZF0gaGlkZSBkaWFsb2cgYmVmb3JlIHJlc3RvcmUgcmFuZ2UgZm9yIElFIHJhbmdlIGZvY3VzXG4gICAgICAgICAgICAgIF90aGlzLnVpLmhpZGVEaWFsb2coX3RoaXMuJGRpYWxvZyk7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IucmVzdG9yZVJhbmdlJyk7XG4gICAgICAgICAgICAgIGlmICh0eXBlb2YgZGF0YSA9PT0gJ3N0cmluZycpIHsgLy8gaW1hZ2UgdXJsXG4gICAgICAgICAgICAgICAgICAvLyBJZiBvbkltYWdlTGlua0luc2VydCBzZXQsXG4gICAgICAgICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy5jYWxsYmFja3Mub25JbWFnZUxpbmtJbnNlcnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0LnRyaWdnZXJFdmVudCgnaW1hZ2UubGluay5pbnNlcnQnLCBkYXRhKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuaW5zZXJ0SW1hZ2UnLCBkYXRhKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBlbHNlIHsgLy8gYXJyYXkgb2YgZmlsZXNcbiAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuaW5zZXJ0SW1hZ2VzT3JDYWxsYmFjaycsIGRhdGEpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSkuZmFpbChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IucmVzdG9yZVJhbmdlJyk7XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgLyoqXG4gICAgICAgKiBzaG93IGltYWdlIGRpYWxvZ1xuICAgICAgICpcbiAgICAgICAqIEBwYXJhbSB7alF1ZXJ5fSAkZGlhbG9nXG4gICAgICAgKiBAcmV0dXJuIHtQcm9taXNlfVxuICAgICAgICovXG4gICAgICBJbWFnZURpYWxvZy5wcm90b3R5cGUuc2hvd0ltYWdlRGlhbG9nID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgcmV0dXJuICQkMS5EZWZlcnJlZChmdW5jdGlvbiAoZGVmZXJyZWQpIHtcbiAgICAgICAgICAgICAgdmFyICRpbWFnZUlucHV0ID0gX3RoaXMuJGRpYWxvZy5maW5kKCcubm90ZS1pbWFnZS1pbnB1dCcpO1xuICAgICAgICAgICAgICB2YXIgJGltYWdlVXJsID0gX3RoaXMuJGRpYWxvZy5maW5kKCcubm90ZS1pbWFnZS11cmwnKTtcbiAgICAgICAgICAgICAgdmFyICRpbWFnZUJ0biA9IF90aGlzLiRkaWFsb2cuZmluZCgnLm5vdGUtaW1hZ2UtYnRuJyk7XG4gICAgICAgICAgICAgIF90aGlzLnVpLm9uRGlhbG9nU2hvd24oX3RoaXMuJGRpYWxvZywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2RpYWxvZy5zaG93bicpO1xuICAgICAgICAgICAgICAgICAgLy8gQ2xvbmluZyBpbWFnZUlucHV0IHRvIGNsZWFyIGVsZW1lbnQuXG4gICAgICAgICAgICAgICAgICAkaW1hZ2VJbnB1dC5yZXBsYWNlV2l0aCgkaW1hZ2VJbnB1dC5jbG9uZSgpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKGV2ZW50LnRhcmdldC5maWxlcyB8fCBldmVudC50YXJnZXQudmFsdWUpO1xuICAgICAgICAgICAgICAgICAgfSkudmFsKCcnKSk7XG4gICAgICAgICAgICAgICAgICAkaW1hZ2VVcmwub24oJ2lucHV0IHBhc3RlIHByb3BlcnR5Y2hhbmdlJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgIF90aGlzLnVpLnRvZ2dsZUJ0bigkaW1hZ2VCdG4sICRpbWFnZVVybC52YWwoKSk7XG4gICAgICAgICAgICAgICAgICB9KS52YWwoJycpO1xuICAgICAgICAgICAgICAgICAgaWYgKCFlbnYuaXNTdXBwb3J0VG91Y2gpIHtcbiAgICAgICAgICAgICAgICAgICAgICAkaW1hZ2VVcmwudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICRpbWFnZUJ0bi5jbGljayhmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoJGltYWdlVXJsLnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuYmluZEVudGVyS2V5KCRpbWFnZVVybCwgJGltYWdlQnRuKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIF90aGlzLnVpLm9uRGlhbG9nSGlkZGVuKF90aGlzLiRkaWFsb2csIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICRpbWFnZUlucHV0Lm9mZigpO1xuICAgICAgICAgICAgICAgICAgJGltYWdlVXJsLm9mZigpO1xuICAgICAgICAgICAgICAgICAgJGltYWdlQnRuLm9mZigpO1xuICAgICAgICAgICAgICAgICAgaWYgKGRlZmVycmVkLnN0YXRlKCkgPT09ICdwZW5kaW5nJykge1xuICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdCgpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgX3RoaXMudWkuc2hvd0RpYWxvZyhfdGhpcy4kZGlhbG9nKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gSW1hZ2VEaWFsb2c7XG4gIH0oKSk7XG5cbiAgLyoqXG4gICAqIEltYWdlIHBvcG92ZXIgbW9kdWxlXG4gICAqICBtb3VzZSBldmVudHMgdGhhdCBzaG93L2hpZGUgcG9wb3ZlciB3aWxsIGJlIGhhbmRsZWQgYnkgSGFuZGxlLmpzLlxuICAgKiAgSGFuZGxlLmpzIHdpbGwgcmVjZWl2ZSB0aGUgZXZlbnRzIGFuZCBpbnZva2UgJ2ltYWdlUG9wb3Zlci51cGRhdGUnLlxuICAgKi9cbiAgdmFyIEltYWdlUG9wb3ZlciA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIEltYWdlUG9wb3Zlcihjb250ZXh0KSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMudWkgPSAkJDEuc3VtbWVybm90ZS51aTtcbiAgICAgICAgICB0aGlzLmVkaXRhYmxlID0gY29udGV4dC5sYXlvdXRJbmZvLmVkaXRhYmxlWzBdO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmV2ZW50cyA9IHtcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUuZGlzYWJsZSc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmhpZGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9XG4gICAgICBJbWFnZVBvcG92ZXIucHJvdG90eXBlLnNob3VsZEluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuICFsaXN0cy5pc0VtcHR5KHRoaXMub3B0aW9ucy5wb3BvdmVyLmltYWdlKTtcbiAgICAgIH07XG4gICAgICBJbWFnZVBvcG92ZXIucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kcG9wb3ZlciA9IHRoaXMudWkucG9wb3Zlcih7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogJ25vdGUtaW1hZ2UtcG9wb3ZlcidcbiAgICAgICAgICB9KS5yZW5kZXIoKS5hcHBlbmRUbyh0aGlzLm9wdGlvbnMuY29udGFpbmVyKTtcbiAgICAgICAgICB2YXIgJGNvbnRlbnQgPSB0aGlzLiRwb3BvdmVyLmZpbmQoJy5wb3BvdmVyLWNvbnRlbnQsLm5vdGUtcG9wb3Zlci1jb250ZW50Jyk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnYnV0dG9ucy5idWlsZCcsICRjb250ZW50LCB0aGlzLm9wdGlvbnMucG9wb3Zlci5pbWFnZSk7XG4gICAgICB9O1xuICAgICAgSW1hZ2VQb3BvdmVyLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgSW1hZ2VQb3BvdmVyLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAodGFyZ2V0LCBldmVudCkge1xuICAgICAgICAgIGlmIChkb20uaXNJbWcodGFyZ2V0KSkge1xuICAgICAgICAgICAgICB2YXIgcG9zID0gZG9tLnBvc0Zyb21QbGFjZWhvbGRlcih0YXJnZXQpO1xuICAgICAgICAgICAgICB2YXIgcG9zRWRpdG9yID0gZG9tLnBvc0Zyb21QbGFjZWhvbGRlcih0aGlzLmVkaXRhYmxlKTtcbiAgICAgICAgICAgICAgdGhpcy4kcG9wb3Zlci5jc3Moe1xuICAgICAgICAgICAgICAgICAgZGlzcGxheTogJ2Jsb2NrJyxcbiAgICAgICAgICAgICAgICAgIGxlZnQ6IHRoaXMub3B0aW9ucy5wb3BhdG1vdXNlID8gZXZlbnQucGFnZVggLSAyMCA6IHBvcy5sZWZ0LFxuICAgICAgICAgICAgICAgICAgdG9wOiB0aGlzLm9wdGlvbnMucG9wYXRtb3VzZSA/IGV2ZW50LnBhZ2VZIDogTWF0aC5taW4ocG9zLnRvcCwgcG9zRWRpdG9yLnRvcClcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgSW1hZ2VQb3BvdmVyLnByb3RvdHlwZS5oaWRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIuaGlkZSgpO1xuICAgICAgfTtcbiAgICAgIHJldHVybiBJbWFnZVBvcG92ZXI7XG4gIH0oKSk7XG5cbiAgdmFyIFRhYmxlUG9wb3ZlciA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAgIGZ1bmN0aW9uIFRhYmxlUG9wb3Zlcihjb250ZXh0KSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICAgICAgICAgIHRoaXMudWkgPSAkJDEuc3VtbWVybm90ZS51aTtcbiAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBjb250ZXh0Lm9wdGlvbnM7XG4gICAgICAgICAgdGhpcy5ldmVudHMgPSB7XG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLm1vdXNlZG93bic6IGZ1bmN0aW9uICh3ZSwgZSkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMudXBkYXRlKGUudGFyZ2V0KTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUua2V5dXAgc3VtbWVybm90ZS5zY3JvbGwgc3VtbWVybm90ZS5jaGFuZ2UnOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy51cGRhdGUoKTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUuZGlzYWJsZSc6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmhpZGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH07XG4gICAgICB9XG4gICAgICBUYWJsZVBvcG92ZXIucHJvdG90eXBlLnNob3VsZEluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuICFsaXN0cy5pc0VtcHR5KHRoaXMub3B0aW9ucy5wb3BvdmVyLnRhYmxlKTtcbiAgICAgIH07XG4gICAgICBUYWJsZVBvcG92ZXIucHJvdG90eXBlLmluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kcG9wb3ZlciA9IHRoaXMudWkucG9wb3Zlcih7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogJ25vdGUtdGFibGUtcG9wb3ZlcidcbiAgICAgICAgICB9KS5yZW5kZXIoKS5hcHBlbmRUbyh0aGlzLm9wdGlvbnMuY29udGFpbmVyKTtcbiAgICAgICAgICB2YXIgJGNvbnRlbnQgPSB0aGlzLiRwb3BvdmVyLmZpbmQoJy5wb3BvdmVyLWNvbnRlbnQsLm5vdGUtcG9wb3Zlci1jb250ZW50Jyk7XG4gICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnYnV0dG9ucy5idWlsZCcsICRjb250ZW50LCB0aGlzLm9wdGlvbnMucG9wb3Zlci50YWJsZSk7XG4gICAgICAgICAgLy8gW3dvcmthcm91bmRdIERpc2FibGUgRmlyZWZveCdzIGRlZmF1bHQgdGFibGUgZWRpdG9yXG4gICAgICAgICAgaWYgKGVudi5pc0ZGKSB7XG4gICAgICAgICAgICAgIGRvY3VtZW50LmV4ZWNDb21tYW5kKCdlbmFibGVJbmxpbmVUYWJsZUVkaXRpbmcnLCBmYWxzZSwgZmFsc2UpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBUYWJsZVBvcG92ZXIucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kcG9wb3Zlci5yZW1vdmUoKTtcbiAgICAgIH07XG4gICAgICBUYWJsZVBvcG92ZXIucHJvdG90eXBlLnVwZGF0ZSA9IGZ1bmN0aW9uICh0YXJnZXQpIHtcbiAgICAgICAgICBpZiAodGhpcy5jb250ZXh0LmlzRGlzYWJsZWQoKSkge1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgIHZhciBpc0NlbGwgPSBkb20uaXNDZWxsKHRhcmdldCk7XG4gICAgICAgICAgaWYgKGlzQ2VsbCkge1xuICAgICAgICAgICAgICB2YXIgcG9zID0gZG9tLnBvc0Zyb21QbGFjZWhvbGRlcih0YXJnZXQpO1xuICAgICAgICAgICAgICB0aGlzLiRwb3BvdmVyLmNzcyh7XG4gICAgICAgICAgICAgICAgICBkaXNwbGF5OiAnYmxvY2snLFxuICAgICAgICAgICAgICAgICAgbGVmdDogcG9zLmxlZnQsXG4gICAgICAgICAgICAgICAgICB0b3A6IHBvcy50b3BcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGlzQ2VsbDtcbiAgICAgIH07XG4gICAgICBUYWJsZVBvcG92ZXIucHJvdG90eXBlLmhpZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdGhpcy4kcG9wb3Zlci5oaWRlKCk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFRhYmxlUG9wb3ZlcjtcbiAgfSgpKTtcblxuICB2YXIgVmlkZW9EaWFsb2cgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBWaWRlb0RpYWxvZyhjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kYm9keSA9ICQkMShkb2N1bWVudC5ib2R5KTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICB9XG4gICAgICBWaWRlb0RpYWxvZy5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgJGNvbnRhaW5lciA9IHRoaXMub3B0aW9ucy5kaWFsb2dzSW5Cb2R5ID8gdGhpcy4kYm9keSA6IHRoaXMuJGVkaXRvcjtcbiAgICAgICAgICB2YXIgYm9keSA9IFtcbiAgICAgICAgICAgICAgJzxkaXYgY2xhc3M9XCJmb3JtLWdyb3VwIG5vdGUtZm9ybS1ncm91cCByb3ctZmx1aWRcIj4nLFxuICAgICAgICAgICAgICBcIjxsYWJlbCBjbGFzcz1cXFwibm90ZS1mb3JtLWxhYmVsXFxcIj5cIiArIHRoaXMubGFuZy52aWRlby51cmwgKyBcIiA8c21hbGwgY2xhc3M9XFxcInRleHQtbXV0ZWRcXFwiPlwiICsgdGhpcy5sYW5nLnZpZGVvLnByb3ZpZGVycyArIFwiPC9zbWFsbD48L2xhYmVsPlwiLFxuICAgICAgICAgICAgICAnPGlucHV0IGNsYXNzPVwibm90ZS12aWRlby11cmwgZm9ybS1jb250cm9sIG5vdGUtZm9ybS1jb250cm9sIG5vdGUtaW5wdXRcIiB0eXBlPVwidGV4dFwiIC8+JyxcbiAgICAgICAgICAgICAgJzwvZGl2PicsXG4gICAgICAgICAgXS5qb2luKCcnKTtcbiAgICAgICAgICB2YXIgYnV0dG9uQ2xhc3MgPSAnYnRuIGJ0bi1wcmltYXJ5IG5vdGUtYnRuIG5vdGUtYnRuLXByaW1hcnkgbm90ZS12aWRlby1idG4nO1xuICAgICAgICAgIHZhciBmb290ZXIgPSBcIjxpbnB1dCB0eXBlPVxcXCJidXR0b25cXFwiIGhyZWY9XFxcIiNcXFwiIGNsYXNzPVxcXCJcIiArIGJ1dHRvbkNsYXNzICsgXCJcXFwiIHZhbHVlPVxcXCJcIiArIHRoaXMubGFuZy52aWRlby5pbnNlcnQgKyBcIlxcXCIgZGlzYWJsZWQ+XCI7XG4gICAgICAgICAgdGhpcy4kZGlhbG9nID0gdGhpcy51aS5kaWFsb2coe1xuICAgICAgICAgICAgICB0aXRsZTogdGhpcy5sYW5nLnZpZGVvLmluc2VydCxcbiAgICAgICAgICAgICAgZmFkZTogdGhpcy5vcHRpb25zLmRpYWxvZ3NGYWRlLFxuICAgICAgICAgICAgICBib2R5OiBib2R5LFxuICAgICAgICAgICAgICBmb290ZXI6IGZvb3RlclxuICAgICAgICAgIH0pLnJlbmRlcigpLmFwcGVuZFRvKCRjb250YWluZXIpO1xuICAgICAgfTtcbiAgICAgIFZpZGVvRGlhbG9nLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMudWkuaGlkZURpYWxvZyh0aGlzLiRkaWFsb2cpO1xuICAgICAgICAgIHRoaXMuJGRpYWxvZy5yZW1vdmUoKTtcbiAgICAgIH07XG4gICAgICBWaWRlb0RpYWxvZy5wcm90b3R5cGUuYmluZEVudGVyS2V5ID0gZnVuY3Rpb24gKCRpbnB1dCwgJGJ0bikge1xuICAgICAgICAgICRpbnB1dC5vbigna2V5cHJlc3MnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgPT09IGtleS5jb2RlLkVOVEVSKSB7XG4gICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgJGJ0bi50cmlnZ2VyKCdjbGljaycpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgVmlkZW9EaWFsb2cucHJvdG90eXBlLmNyZWF0ZVZpZGVvTm9kZSA9IGZ1bmN0aW9uICh1cmwpIHtcbiAgICAgICAgICAvLyB2aWRlbyB1cmwgcGF0dGVybnMoeW91dHViZSwgaW5zdGFncmFtLCB2aW1lbywgZGFpbHltb3Rpb24sIHlvdWt1LCBtcDQsIG9nZywgd2VibSlcbiAgICAgICAgICB2YXIgeXRSZWdFeHAgPSAvXFwvXFwvKD86d3d3XFwuKT8oPzp5b3V0dVxcLmJlXFwvfHlvdXR1YmVcXC5jb21cXC8oPzplbWJlZFxcL3x2XFwvfHdhdGNoXFw/dj18d2F0Y2hcXD8uKyZ2PSkpKFtcXHd8LV17MTF9KSg/Oig/OltcXD8mXXQ9KShcXFMrKSk/JC87XG4gICAgICAgICAgdmFyIHl0UmVnRXhwRm9yU3RhcnQgPSAvXig/OihcXGQrKWgpPyg/OihcXGQrKW0pPyg/OihcXGQrKXMpPyQvO1xuICAgICAgICAgIHZhciB5dE1hdGNoID0gdXJsLm1hdGNoKHl0UmVnRXhwKTtcbiAgICAgICAgICB2YXIgaWdSZWdFeHAgPSAvKD86d3d3XFwufFxcL1xcLylpbnN0YWdyYW1cXC5jb21cXC9wXFwvKC5bYS16QS1aMC05Xy1dKikvO1xuICAgICAgICAgIHZhciBpZ01hdGNoID0gdXJsLm1hdGNoKGlnUmVnRXhwKTtcbiAgICAgICAgICB2YXIgdlJlZ0V4cCA9IC9cXC9cXC92aW5lXFwuY29cXC92XFwvKFthLXpBLVowLTldKykvO1xuICAgICAgICAgIHZhciB2TWF0Y2ggPSB1cmwubWF0Y2godlJlZ0V4cCk7XG4gICAgICAgICAgdmFyIHZpbVJlZ0V4cCA9IC9cXC9cXC8ocGxheWVyXFwuKT92aW1lb1xcLmNvbVxcLyhbYS16XSpcXC8pKihcXGQrKVs/XT8uKi87XG4gICAgICAgICAgdmFyIHZpbU1hdGNoID0gdXJsLm1hdGNoKHZpbVJlZ0V4cCk7XG4gICAgICAgICAgdmFyIGRtUmVnRXhwID0gLy4rZGFpbHltb3Rpb24uY29tXFwvKHZpZGVvfGh1YilcXC8oW15fXSspW14jXSooI3ZpZGVvPShbXl8mXSspKT8vO1xuICAgICAgICAgIHZhciBkbU1hdGNoID0gdXJsLm1hdGNoKGRtUmVnRXhwKTtcbiAgICAgICAgICB2YXIgeW91a3VSZWdFeHAgPSAvXFwvXFwvdlxcLnlvdWt1XFwuY29tXFwvdl9zaG93XFwvaWRfKFxcdyspPSpcXC5odG1sLztcbiAgICAgICAgICB2YXIgeW91a3VNYXRjaCA9IHVybC5tYXRjaCh5b3VrdVJlZ0V4cCk7XG4gICAgICAgICAgdmFyIHFxUmVnRXhwID0gL1xcL1xcL3ZcXC5xcVxcLmNvbS4qP3ZpZD0oLispLztcbiAgICAgICAgICB2YXIgcXFNYXRjaCA9IHVybC5tYXRjaChxcVJlZ0V4cCk7XG4gICAgICAgICAgdmFyIHFxUmVnRXhwMiA9IC9cXC9cXC92XFwucXFcXC5jb21cXC94P1xcLz8ocGFnZXxjb3ZlcikuKj9cXC8oW15cXC9dKylcXC5odG1sXFw/Py4qLztcbiAgICAgICAgICB2YXIgcXFNYXRjaDIgPSB1cmwubWF0Y2gocXFSZWdFeHAyKTtcbiAgICAgICAgICB2YXIgbXA0UmVnRXhwID0gL14uKy4obXA0fG00dikkLztcbiAgICAgICAgICB2YXIgbXA0TWF0Y2ggPSB1cmwubWF0Y2gobXA0UmVnRXhwKTtcbiAgICAgICAgICB2YXIgb2dnUmVnRXhwID0gL14uKy4ob2dnfG9ndikkLztcbiAgICAgICAgICB2YXIgb2dnTWF0Y2ggPSB1cmwubWF0Y2gob2dnUmVnRXhwKTtcbiAgICAgICAgICB2YXIgd2VibVJlZ0V4cCA9IC9eLisuKHdlYm0pJC87XG4gICAgICAgICAgdmFyIHdlYm1NYXRjaCA9IHVybC5tYXRjaCh3ZWJtUmVnRXhwKTtcbiAgICAgICAgICB2YXIgZmJSZWdFeHAgPSAvKD86d3d3XFwufFxcL1xcLylmYWNlYm9va1xcLmNvbVxcLyhbXlxcL10rKVxcL3ZpZGVvc1xcLyhbMC05XSspLztcbiAgICAgICAgICB2YXIgZmJNYXRjaCA9IHVybC5tYXRjaChmYlJlZ0V4cCk7XG4gICAgICAgICAgdmFyICR2aWRlbztcbiAgICAgICAgICBpZiAoeXRNYXRjaCAmJiB5dE1hdGNoWzFdLmxlbmd0aCA9PT0gMTEpIHtcbiAgICAgICAgICAgICAgdmFyIHlvdXR1YmVJZCA9IHl0TWF0Y2hbMV07XG4gICAgICAgICAgICAgIHZhciBzdGFydCA9IDA7XG4gICAgICAgICAgICAgIGlmICh0eXBlb2YgeXRNYXRjaFsyXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgIHZhciB5dE1hdGNoRm9yU3RhcnQgPSB5dE1hdGNoWzJdLm1hdGNoKHl0UmVnRXhwRm9yU3RhcnQpO1xuICAgICAgICAgICAgICAgICAgaWYgKHl0TWF0Y2hGb3JTdGFydCkge1xuICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIG4gPSBbMzYwMCwgNjAsIDFdLCBpID0gMCwgciA9IG4ubGVuZ3RoOyBpIDwgcjsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0ICs9ICh0eXBlb2YgeXRNYXRjaEZvclN0YXJ0W2kgKyAxXSAhPT0gJ3VuZGVmaW5lZCcgPyBuW2ldICogcGFyc2VJbnQoeXRNYXRjaEZvclN0YXJ0W2kgKyAxXSwgMTApIDogMCk7XG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICR2aWRlbyA9ICQkMSgnPGlmcmFtZT4nKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ2ZyYW1lYm9yZGVyJywgMClcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmMnLCAnLy93d3cueW91dHViZS5jb20vZW1iZWQvJyArIHlvdXR1YmVJZCArIChzdGFydCA+IDAgPyAnP3N0YXJ0PScgKyBzdGFydCA6ICcnKSlcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCd3aWR0aCcsICc2NDAnKS5hdHRyKCdoZWlnaHQnLCAnMzYwJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYgKGlnTWF0Y2ggJiYgaWdNYXRjaFswXS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgJHZpZGVvID0gJCQxKCc8aWZyYW1lPicpXG4gICAgICAgICAgICAgICAgICAuYXR0cignZnJhbWVib3JkZXInLCAwKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ3NyYycsICdodHRwczovL2luc3RhZ3JhbS5jb20vcC8nICsgaWdNYXRjaFsxXSArICcvZW1iZWQvJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCd3aWR0aCcsICc2MTInKS5hdHRyKCdoZWlnaHQnLCAnNzEwJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdzY3JvbGxpbmcnLCAnbm8nKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ2FsbG93dHJhbnNwYXJlbmN5JywgJ3RydWUnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAodk1hdGNoICYmIHZNYXRjaFswXS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgJHZpZGVvID0gJCQxKCc8aWZyYW1lPicpXG4gICAgICAgICAgICAgICAgICAuYXR0cignZnJhbWVib3JkZXInLCAwKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ3NyYycsIHZNYXRjaFswXSArICcvZW1iZWQvc2ltcGxlJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCd3aWR0aCcsICc2MDAnKS5hdHRyKCdoZWlnaHQnLCAnNjAwJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdjbGFzcycsICd2aW5lLWVtYmVkJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2UgaWYgKHZpbU1hdGNoICYmIHZpbU1hdGNoWzNdLmxlbmd0aCkge1xuICAgICAgICAgICAgICAkdmlkZW8gPSAkJDEoJzxpZnJhbWUgd2Via2l0YWxsb3dmdWxsc2NyZWVuIG1vemFsbG93ZnVsbHNjcmVlbiBhbGxvd2Z1bGxzY3JlZW4+JylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdmcmFtZWJvcmRlcicsIDApXG4gICAgICAgICAgICAgICAgICAuYXR0cignc3JjJywgJy8vcGxheWVyLnZpbWVvLmNvbS92aWRlby8nICsgdmltTWF0Y2hbM10pXG4gICAgICAgICAgICAgICAgICAuYXR0cignd2lkdGgnLCAnNjQwJykuYXR0cignaGVpZ2h0JywgJzM2MCcpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmIChkbU1hdGNoICYmIGRtTWF0Y2hbMl0ubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICR2aWRlbyA9ICQkMSgnPGlmcmFtZT4nKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ2ZyYW1lYm9yZGVyJywgMClcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmMnLCAnLy93d3cuZGFpbHltb3Rpb24uY29tL2VtYmVkL3ZpZGVvLycgKyBkbU1hdGNoWzJdKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ3dpZHRoJywgJzY0MCcpLmF0dHIoJ2hlaWdodCcsICczNjAnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAoeW91a3VNYXRjaCAmJiB5b3VrdU1hdGNoWzFdLmxlbmd0aCkge1xuICAgICAgICAgICAgICAkdmlkZW8gPSAkJDEoJzxpZnJhbWUgd2Via2l0YWxsb3dmdWxsc2NyZWVuIG1vemFsbG93ZnVsbHNjcmVlbiBhbGxvd2Z1bGxzY3JlZW4+JylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdmcmFtZWJvcmRlcicsIDApXG4gICAgICAgICAgICAgICAgICAuYXR0cignaGVpZ2h0JywgJzQ5OCcpXG4gICAgICAgICAgICAgICAgICAuYXR0cignd2lkdGgnLCAnNTEwJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdzcmMnLCAnLy9wbGF5ZXIueW91a3UuY29tL2VtYmVkLycgKyB5b3VrdU1hdGNoWzFdKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAoKHFxTWF0Y2ggJiYgcXFNYXRjaFsxXS5sZW5ndGgpIHx8IChxcU1hdGNoMiAmJiBxcU1hdGNoMlsyXS5sZW5ndGgpKSB7XG4gICAgICAgICAgICAgIHZhciB2aWQgPSAoKHFxTWF0Y2ggJiYgcXFNYXRjaFsxXS5sZW5ndGgpID8gcXFNYXRjaFsxXSA6IHFxTWF0Y2gyWzJdKTtcbiAgICAgICAgICAgICAgJHZpZGVvID0gJCQxKCc8aWZyYW1lIHdlYmtpdGFsbG93ZnVsbHNjcmVlbiBtb3phbGxvd2Z1bGxzY3JlZW4gYWxsb3dmdWxsc2NyZWVuPicpXG4gICAgICAgICAgICAgICAgICAuYXR0cignZnJhbWVib3JkZXInLCAwKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ2hlaWdodCcsICczMTAnKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ3dpZHRoJywgJzUwMCcpXG4gICAgICAgICAgICAgICAgICAuYXR0cignc3JjJywgJ2h0dHA6Ly92LnFxLmNvbS9pZnJhbWUvcGxheWVyLmh0bWw/dmlkPScgKyB2aWQgKyAnJmFtcDthdXRvPTAnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAobXA0TWF0Y2ggfHwgb2dnTWF0Y2ggfHwgd2VibU1hdGNoKSB7XG4gICAgICAgICAgICAgICR2aWRlbyA9ICQkMSgnPHZpZGVvIGNvbnRyb2xzPicpXG4gICAgICAgICAgICAgICAgICAuYXR0cignc3JjJywgdXJsKVxuICAgICAgICAgICAgICAgICAgLmF0dHIoJ3dpZHRoJywgJzY0MCcpLmF0dHIoJ2hlaWdodCcsICczNjAnKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAoZmJNYXRjaCAmJiBmYk1hdGNoWzBdLmxlbmd0aCkge1xuICAgICAgICAgICAgICAkdmlkZW8gPSAkJDEoJzxpZnJhbWU+JylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdmcmFtZWJvcmRlcicsIDApXG4gICAgICAgICAgICAgICAgICAuYXR0cignc3JjJywgJ2h0dHBzOi8vd3d3LmZhY2Vib29rLmNvbS9wbHVnaW5zL3ZpZGVvLnBocD9ocmVmPScgKyBlbmNvZGVVUklDb21wb25lbnQoZmJNYXRjaFswXSkgKyAnJnNob3dfdGV4dD0wJndpZHRoPTU2MCcpXG4gICAgICAgICAgICAgICAgICAuYXR0cignd2lkdGgnLCAnNTYwJykuYXR0cignaGVpZ2h0JywgJzMwMScpXG4gICAgICAgICAgICAgICAgICAuYXR0cignc2Nyb2xsaW5nJywgJ25vJylcbiAgICAgICAgICAgICAgICAgIC5hdHRyKCdhbGxvd3RyYW5zcGFyZW5jeScsICd0cnVlJyk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAvLyB0aGlzIGlzIG5vdCBhIGtub3duIHZpZGVvIGxpbmsuIE5vdyB3aGF0LCBDYXQ/IE5vdyB3aGF0P1xuICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgfVxuICAgICAgICAgICR2aWRlby5hZGRDbGFzcygnbm90ZS12aWRlby1jbGlwJyk7XG4gICAgICAgICAgcmV0dXJuICR2aWRlb1swXTtcbiAgICAgIH07XG4gICAgICBWaWRlb0RpYWxvZy5wcm90b3R5cGUuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHZhciB0ZXh0ID0gdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmdldFNlbGVjdGVkVGV4dCcpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5zYXZlUmFuZ2UnKTtcbiAgICAgICAgICB0aGlzLnNob3dWaWRlb0RpYWxvZyh0ZXh0KS50aGVuKGZ1bmN0aW9uICh1cmwpIHtcbiAgICAgICAgICAgICAgLy8gW3dvcmthcm91bmRdIGhpZGUgZGlhbG9nIGJlZm9yZSByZXN0b3JlIHJhbmdlIGZvciBJRSByYW5nZSBmb2N1c1xuICAgICAgICAgICAgICBfdGhpcy51aS5oaWRlRGlhbG9nKF90aGlzLiRkaWFsb2cpO1xuICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLnJlc3RvcmVSYW5nZScpO1xuICAgICAgICAgICAgICAvLyBidWlsZCBub2RlXG4gICAgICAgICAgICAgIHZhciAkbm9kZSA9IF90aGlzLmNyZWF0ZVZpZGVvTm9kZSh1cmwpO1xuICAgICAgICAgICAgICBpZiAoJG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgIC8vIGluc2VydCB2aWRlbyBub2RlXG4gICAgICAgICAgICAgICAgICBfdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmluc2VydE5vZGUnLCAkbm9kZSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KS5mYWlsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5yZXN0b3JlUmFuZ2UnKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHNob3cgaW1hZ2UgZGlhbG9nXG4gICAgICAgKlxuICAgICAgICogQHBhcmFtIHtqUXVlcnl9ICRkaWFsb2dcbiAgICAgICAqIEByZXR1cm4ge1Byb21pc2V9XG4gICAgICAgKi9cbiAgICAgIFZpZGVvRGlhbG9nLnByb3RvdHlwZS5zaG93VmlkZW9EaWFsb2cgPSBmdW5jdGlvbiAodGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgcmV0dXJuICQkMS5EZWZlcnJlZChmdW5jdGlvbiAoZGVmZXJyZWQpIHtcbiAgICAgICAgICAgICAgdmFyICR2aWRlb1VybCA9IF90aGlzLiRkaWFsb2cuZmluZCgnLm5vdGUtdmlkZW8tdXJsJyk7XG4gICAgICAgICAgICAgIHZhciAkdmlkZW9CdG4gPSBfdGhpcy4kZGlhbG9nLmZpbmQoJy5ub3RlLXZpZGVvLWJ0bicpO1xuICAgICAgICAgICAgICBfdGhpcy51aS5vbkRpYWxvZ1Nob3duKF90aGlzLiRkaWFsb2csIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdkaWFsb2cuc2hvd24nKTtcbiAgICAgICAgICAgICAgICAgICR2aWRlb1VybC5vbignaW5wdXQgcGFzdGUgcHJvcGVydHljaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMudWkudG9nZ2xlQnRuKCR2aWRlb0J0biwgJHZpZGVvVXJsLnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgaWYgKCFlbnYuaXNTdXBwb3J0VG91Y2gpIHtcbiAgICAgICAgICAgICAgICAgICAgICAkdmlkZW9VcmwudHJpZ2dlcignZm9jdXMnKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICR2aWRlb0J0bi5jbGljayhmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUoJHZpZGVvVXJsLnZhbCgpKTtcbiAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuYmluZEVudGVyS2V5KCR2aWRlb1VybCwgJHZpZGVvQnRuKTtcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIF90aGlzLnVpLm9uRGlhbG9nSGlkZGVuKF90aGlzLiRkaWFsb2csIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICR2aWRlb1VybC5vZmYoKTtcbiAgICAgICAgICAgICAgICAgICR2aWRlb0J0bi5vZmYoKTtcbiAgICAgICAgICAgICAgICAgIGlmIChkZWZlcnJlZC5zdGF0ZSgpID09PSAncGVuZGluZycpIHtcbiAgICAgICAgICAgICAgICAgICAgICBkZWZlcnJlZC5yZWplY3QoKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIF90aGlzLnVpLnNob3dEaWFsb2coX3RoaXMuJGRpYWxvZyk7XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIFZpZGVvRGlhbG9nO1xuICB9KCkpO1xuXG4gIHZhciBIZWxwRGlhbG9nID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gSGVscERpYWxvZyhjb250ZXh0KSB7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kYm9keSA9ICQkMShkb2N1bWVudC5ib2R5KTtcbiAgICAgICAgICB0aGlzLiRlZGl0b3IgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdG9yO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmxhbmcgPSB0aGlzLm9wdGlvbnMubGFuZ0luZm87XG4gICAgICB9XG4gICAgICBIZWxwRGlhbG9nLnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciAkY29udGFpbmVyID0gdGhpcy5vcHRpb25zLmRpYWxvZ3NJbkJvZHkgPyB0aGlzLiRib2R5IDogdGhpcy4kZWRpdG9yO1xuICAgICAgICAgIHZhciBib2R5ID0gW1xuICAgICAgICAgICAgICAnPHAgY2xhc3M9XCJ0ZXh0LWNlbnRlclwiPicsXG4gICAgICAgICAgICAgICc8YSBocmVmPVwiaHR0cDovL3N1bW1lcm5vdGUub3JnL1wiIHRhcmdldD1cIl9ibGFua1wiPlN1bW1lcm5vdGUgMC44LjEyPC9hPiDCtyAnLFxuICAgICAgICAgICAgICAnPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9zdW1tZXJub3RlL3N1bW1lcm5vdGVcIiB0YXJnZXQ9XCJfYmxhbmtcIj5Qcm9qZWN0PC9hPiDCtyAnLFxuICAgICAgICAgICAgICAnPGEgaHJlZj1cImh0dHBzOi8vZ2l0aHViLmNvbS9zdW1tZXJub3RlL3N1bW1lcm5vdGUvaXNzdWVzXCIgdGFyZ2V0PVwiX2JsYW5rXCI+SXNzdWVzPC9hPicsXG4gICAgICAgICAgICAgICc8L3A+JyxcbiAgICAgICAgICBdLmpvaW4oJycpO1xuICAgICAgICAgIHRoaXMuJGRpYWxvZyA9IHRoaXMudWkuZGlhbG9nKHtcbiAgICAgICAgICAgICAgdGl0bGU6IHRoaXMubGFuZy5vcHRpb25zLmhlbHAsXG4gICAgICAgICAgICAgIGZhZGU6IHRoaXMub3B0aW9ucy5kaWFsb2dzRmFkZSxcbiAgICAgICAgICAgICAgYm9keTogdGhpcy5jcmVhdGVTaG9ydGN1dExpc3QoKSxcbiAgICAgICAgICAgICAgZm9vdGVyOiBib2R5LFxuICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKCRub2RlKSB7XG4gICAgICAgICAgICAgICAgICAkbm9kZS5maW5kKCcubW9kYWwtYm9keSwubm90ZS1tb2RhbC1ib2R5JykuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAnbWF4LWhlaWdodCc6IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgICAnb3ZlcmZsb3cnOiAnc2Nyb2xsJ1xuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9KS5yZW5kZXIoKS5hcHBlbmRUbygkY29udGFpbmVyKTtcbiAgICAgIH07XG4gICAgICBIZWxwRGlhbG9nLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMudWkuaGlkZURpYWxvZyh0aGlzLiRkaWFsb2cpO1xuICAgICAgICAgIHRoaXMuJGRpYWxvZy5yZW1vdmUoKTtcbiAgICAgIH07XG4gICAgICBIZWxwRGlhbG9nLnByb3RvdHlwZS5jcmVhdGVTaG9ydGN1dExpc3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgICB2YXIga2V5TWFwID0gdGhpcy5vcHRpb25zLmtleU1hcFtlbnYuaXNNYWMgPyAnbWFjJyA6ICdwYyddO1xuICAgICAgICAgIHJldHVybiBPYmplY3Qua2V5cyhrZXlNYXApLm1hcChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICAgIHZhciBjb21tYW5kID0ga2V5TWFwW2tleV07XG4gICAgICAgICAgICAgIHZhciAkcm93ID0gJCQxKCc8ZGl2PjxkaXYgY2xhc3M9XCJoZWxwLWxpc3QtaXRlbVwiLz48L2Rpdj4nKTtcbiAgICAgICAgICAgICAgJHJvdy5hcHBlbmQoJCQxKCc8bGFiZWw+PGtiZD4nICsga2V5ICsgJzwva2RiPjwvbGFiZWw+JykuY3NzKHtcbiAgICAgICAgICAgICAgICAgICd3aWR0aCc6IDE4MCxcbiAgICAgICAgICAgICAgICAgICdtYXJnaW4tcmlnaHQnOiAxMFxuICAgICAgICAgICAgICB9KSkuYXBwZW5kKCQkMSgnPHNwYW4vPicpLmh0bWwoX3RoaXMuY29udGV4dC5tZW1vKCdoZWxwLicgKyBjb21tYW5kKSB8fCBjb21tYW5kKSk7XG4gICAgICAgICAgICAgIHJldHVybiAkcm93Lmh0bWwoKTtcbiAgICAgICAgICB9KS5qb2luKCcnKTtcbiAgICAgIH07XG4gICAgICAvKipcbiAgICAgICAqIHNob3cgaGVscCBkaWFsb2dcbiAgICAgICAqXG4gICAgICAgKiBAcmV0dXJuIHtQcm9taXNlfVxuICAgICAgICovXG4gICAgICBIZWxwRGlhbG9nLnByb3RvdHlwZS5zaG93SGVscERpYWxvZyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHJldHVybiAkJDEuRGVmZXJyZWQoZnVuY3Rpb24gKGRlZmVycmVkKSB7XG4gICAgICAgICAgICAgIF90aGlzLnVpLm9uRGlhbG9nU2hvd24oX3RoaXMuJGRpYWxvZywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMuY29udGV4dC50cmlnZ2VyRXZlbnQoJ2RpYWxvZy5zaG93bicpO1xuICAgICAgICAgICAgICAgICAgZGVmZXJyZWQucmVzb2x2ZSgpO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgX3RoaXMudWkuc2hvd0RpYWxvZyhfdGhpcy4kZGlhbG9nKTtcbiAgICAgICAgICB9KS5wcm9taXNlKCk7XG4gICAgICB9O1xuICAgICAgSGVscERpYWxvZy5wcm90b3R5cGUuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2VkaXRvci5zYXZlUmFuZ2UnKTtcbiAgICAgICAgICB0aGlzLnNob3dIZWxwRGlhbG9nKCkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgIF90aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IucmVzdG9yZVJhbmdlJyk7XG4gICAgICAgICAgfSk7XG4gICAgICB9O1xuICAgICAgcmV0dXJuIEhlbHBEaWFsb2c7XG4gIH0oKSk7XG5cbiAgdmFyIEFJUl9NT0RFX1BPUE9WRVJfWF9PRkZTRVQgPSAyMDtcbiAgdmFyIEFpclBvcG92ZXIgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgICBmdW5jdGlvbiBBaXJQb3BvdmVyKGNvbnRleHQpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMuY29udGV4dCA9IGNvbnRleHQ7XG4gICAgICAgICAgdGhpcy51aSA9ICQkMS5zdW1tZXJub3RlLnVpO1xuICAgICAgICAgIHRoaXMub3B0aW9ucyA9IGNvbnRleHQub3B0aW9ucztcbiAgICAgICAgICB0aGlzLmV2ZW50cyA9IHtcbiAgICAgICAgICAgICAgJ3N1bW1lcm5vdGUua2V5dXAgc3VtbWVybm90ZS5tb3VzZXVwIHN1bW1lcm5vdGUuc2Nyb2xsJzogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgX3RoaXMudXBkYXRlKCk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmRpc2FibGUgc3VtbWVybm90ZS5jaGFuZ2Ugc3VtbWVybm90ZS5kaWFsb2cuc2hvd24nOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5oaWRlKCk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmZvY3Vzb3V0JzogZnVuY3Rpb24gKHdlLCBlKSB7XG4gICAgICAgICAgICAgICAgICAvLyBbd29ya2Fyb3VuZF0gRmlyZWZveCBkb2Vzbid0IHN1cHBvcnQgcmVsYXRlZFRhcmdldCBvbiBmb2N1c291dFxuICAgICAgICAgICAgICAgICAgLy8gIC0gSWdub3JlIGhpZGUgYWN0aW9uIG9uIGZvY3VzIG91dCBpbiBGRi5cbiAgICAgICAgICAgICAgICAgIGlmIChlbnYuaXNGRikge1xuICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIGlmICghZS5yZWxhdGVkVGFyZ2V0IHx8ICFkb20uYW5jZXN0b3IoZS5yZWxhdGVkVGFyZ2V0LCBmdW5jLmVxKF90aGlzLiRwb3BvdmVyWzBdKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5oaWRlKCk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9O1xuICAgICAgfVxuICAgICAgQWlyUG9wb3Zlci5wcm90b3R5cGUuc2hvdWxkSW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLmFpck1vZGUgJiYgIWxpc3RzLmlzRW1wdHkodGhpcy5vcHRpb25zLnBvcG92ZXIuYWlyKTtcbiAgICAgIH07XG4gICAgICBBaXJQb3BvdmVyLnByb3RvdHlwZS5pbml0aWFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIgPSB0aGlzLnVpLnBvcG92ZXIoe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6ICdub3RlLWFpci1wb3BvdmVyJ1xuICAgICAgICAgIH0pLnJlbmRlcigpLmFwcGVuZFRvKHRoaXMub3B0aW9ucy5jb250YWluZXIpO1xuICAgICAgICAgIHZhciAkY29udGVudCA9IHRoaXMuJHBvcG92ZXIuZmluZCgnLnBvcG92ZXItY29udGVudCcpO1xuICAgICAgICAgIHRoaXMuY29udGV4dC5pbnZva2UoJ2J1dHRvbnMuYnVpbGQnLCAkY29udGVudCwgdGhpcy5vcHRpb25zLnBvcG92ZXIuYWlyKTtcbiAgICAgIH07XG4gICAgICBBaXJQb3BvdmVyLnByb3RvdHlwZS5kZXN0cm95ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuJHBvcG92ZXIucmVtb3ZlKCk7XG4gICAgICB9O1xuICAgICAgQWlyUG9wb3Zlci5wcm90b3R5cGUudXBkYXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHZhciBzdHlsZUluZm8gPSB0aGlzLmNvbnRleHQuaW52b2tlKCdlZGl0b3IuY3VycmVudFN0eWxlJyk7XG4gICAgICAgICAgaWYgKHN0eWxlSW5mby5yYW5nZSAmJiAhc3R5bGVJbmZvLnJhbmdlLmlzQ29sbGFwc2VkKCkpIHtcbiAgICAgICAgICAgICAgdmFyIHJlY3QgPSBsaXN0cy5sYXN0KHN0eWxlSW5mby5yYW5nZS5nZXRDbGllbnRSZWN0cygpKTtcbiAgICAgICAgICAgICAgaWYgKHJlY3QpIHtcbiAgICAgICAgICAgICAgICAgIHZhciBibmQgPSBmdW5jLnJlY3QyYm5kKHJlY3QpO1xuICAgICAgICAgICAgICAgICAgdGhpcy4kcG9wb3Zlci5jc3Moe1xuICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6ICdibG9jaycsXG4gICAgICAgICAgICAgICAgICAgICAgbGVmdDogTWF0aC5tYXgoYm5kLmxlZnQgKyBibmQud2lkdGggLyAyLCAwKSAtIEFJUl9NT0RFX1BPUE9WRVJfWF9PRkZTRVQsXG4gICAgICAgICAgICAgICAgICAgICAgdG9wOiBibmQudG9wICsgYm5kLmhlaWdodFxuICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHQuaW52b2tlKCdidXR0b25zLnVwZGF0ZUN1cnJlbnRTdHlsZScsIHRoaXMuJHBvcG92ZXIpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgQWlyUG9wb3Zlci5wcm90b3R5cGUuaGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwb3BvdmVyLmhpZGUoKTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gQWlyUG9wb3ZlcjtcbiAgfSgpKTtcblxuICB2YXIgUE9QT1ZFUl9ESVNUID0gNTtcbiAgdmFyIEhpbnRQb3BvdmVyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgICAgZnVuY3Rpb24gSGludFBvcG92ZXIoY29udGV4dCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdGhpcy5jb250ZXh0ID0gY29udGV4dDtcbiAgICAgICAgICB0aGlzLnVpID0gJCQxLnN1bW1lcm5vdGUudWk7XG4gICAgICAgICAgdGhpcy4kZWRpdGFibGUgPSBjb250ZXh0LmxheW91dEluZm8uZWRpdGFibGU7XG4gICAgICAgICAgdGhpcy5vcHRpb25zID0gY29udGV4dC5vcHRpb25zO1xuICAgICAgICAgIHRoaXMuaGludCA9IHRoaXMub3B0aW9ucy5oaW50IHx8IFtdO1xuICAgICAgICAgIHRoaXMuZGlyZWN0aW9uID0gdGhpcy5vcHRpb25zLmhpbnREaXJlY3Rpb24gfHwgJ2JvdHRvbSc7XG4gICAgICAgICAgdGhpcy5oaW50cyA9IEFycmF5LmlzQXJyYXkodGhpcy5oaW50KSA/IHRoaXMuaGludCA6IFt0aGlzLmhpbnRdO1xuICAgICAgICAgIHRoaXMuZXZlbnRzID0ge1xuICAgICAgICAgICAgICAnc3VtbWVybm90ZS5rZXl1cCc6IGZ1bmN0aW9uICh3ZSwgZSkge1xuICAgICAgICAgICAgICAgICAgaWYgKCFlLmlzRGVmYXVsdFByZXZlbnRlZCgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgX3RoaXMuaGFuZGxlS2V5dXAoZSk7XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmtleWRvd24nOiBmdW5jdGlvbiAod2UsIGUpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzLmhhbmRsZUtleWRvd24oZSk7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICdzdW1tZXJub3RlLmRpc2FibGUgc3VtbWVybm90ZS5kaWFsb2cuc2hvd24nOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpcy5oaWRlKCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICB9O1xuICAgICAgfVxuICAgICAgSGludFBvcG92ZXIucHJvdG90eXBlLnNob3VsZEluaXRpYWxpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgcmV0dXJuIHRoaXMuaGludHMubGVuZ3RoID4gMDtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuaW5pdGlhbGl6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICAgIHRoaXMubGFzdFdvcmRSYW5nZSA9IG51bGw7XG4gICAgICAgICAgdGhpcy4kcG9wb3ZlciA9IHRoaXMudWkucG9wb3Zlcih7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogJ25vdGUtaGludC1wb3BvdmVyJyxcbiAgICAgICAgICAgICAgaGlkZUFycm93OiB0cnVlLFxuICAgICAgICAgICAgICBkaXJlY3Rpb246ICcnXG4gICAgICAgICAgfSkucmVuZGVyKCkuYXBwZW5kVG8odGhpcy5vcHRpb25zLmNvbnRhaW5lcik7XG4gICAgICAgICAgdGhpcy4kcG9wb3Zlci5oaWRlKCk7XG4gICAgICAgICAgdGhpcy4kY29udGVudCA9IHRoaXMuJHBvcG92ZXIuZmluZCgnLnBvcG92ZXItY29udGVudCwubm90ZS1wb3BvdmVyLWNvbnRlbnQnKTtcbiAgICAgICAgICB0aGlzLiRjb250ZW50Lm9uKCdjbGljaycsICcubm90ZS1oaW50LWl0ZW0nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgICBfdGhpcy4kY29udGVudC5maW5kKCcuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAkJDEoZS5jdXJyZW50VGFyZ2V0KS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgIF90aGlzLnJlcGxhY2UoKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuZGVzdHJveSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwb3BvdmVyLnJlbW92ZSgpO1xuICAgICAgfTtcbiAgICAgIEhpbnRQb3BvdmVyLnByb3RvdHlwZS5zZWxlY3RJdGVtID0gZnVuY3Rpb24gKCRpdGVtKSB7XG4gICAgICAgICAgdGhpcy4kY29udGVudC5maW5kKCcuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICRpdGVtLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICB0aGlzLiRjb250ZW50WzBdLnNjcm9sbFRvcCA9ICRpdGVtWzBdLm9mZnNldFRvcCAtICh0aGlzLiRjb250ZW50LmlubmVySGVpZ2h0KCkgLyAyKTtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUubW92ZURvd24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyICRjdXJyZW50ID0gdGhpcy4kY29udGVudC5maW5kKCcubm90ZS1oaW50LWl0ZW0uYWN0aXZlJyk7XG4gICAgICAgICAgdmFyICRuZXh0ID0gJGN1cnJlbnQubmV4dCgpO1xuICAgICAgICAgIGlmICgkbmV4dC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgdGhpcy5zZWxlY3RJdGVtKCRuZXh0KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgIHZhciAkbmV4dEdyb3VwID0gJGN1cnJlbnQucGFyZW50KCkubmV4dCgpO1xuICAgICAgICAgICAgICBpZiAoISRuZXh0R3JvdXAubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAkbmV4dEdyb3VwID0gdGhpcy4kY29udGVudC5maW5kKCcubm90ZS1oaW50LWdyb3VwJykuZmlyc3QoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB0aGlzLnNlbGVjdEl0ZW0oJG5leHRHcm91cC5maW5kKCcubm90ZS1oaW50LWl0ZW0nKS5maXJzdCgpKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgSGludFBvcG92ZXIucHJvdG90eXBlLm1vdmVVcCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB2YXIgJGN1cnJlbnQgPSB0aGlzLiRjb250ZW50LmZpbmQoJy5ub3RlLWhpbnQtaXRlbS5hY3RpdmUnKTtcbiAgICAgICAgICB2YXIgJHByZXYgPSAkY3VycmVudC5wcmV2KCk7XG4gICAgICAgICAgaWYgKCRwcmV2Lmxlbmd0aCkge1xuICAgICAgICAgICAgICB0aGlzLnNlbGVjdEl0ZW0oJHByZXYpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdmFyICRwcmV2R3JvdXAgPSAkY3VycmVudC5wYXJlbnQoKS5wcmV2KCk7XG4gICAgICAgICAgICAgIGlmICghJHByZXZHcm91cC5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICRwcmV2R3JvdXAgPSB0aGlzLiRjb250ZW50LmZpbmQoJy5ub3RlLWhpbnQtZ3JvdXAnKS5sYXN0KCk7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdGhpcy5zZWxlY3RJdGVtKCRwcmV2R3JvdXAuZmluZCgnLm5vdGUtaGludC1pdGVtJykubGFzdCgpKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgSGludFBvcG92ZXIucHJvdG90eXBlLnJlcGxhY2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyICRpdGVtID0gdGhpcy4kY29udGVudC5maW5kKCcubm90ZS1oaW50LWl0ZW0uYWN0aXZlJyk7XG4gICAgICAgICAgaWYgKCRpdGVtLmxlbmd0aCkge1xuICAgICAgICAgICAgICB2YXIgbm9kZSA9IHRoaXMubm9kZUZyb21JdGVtKCRpdGVtKTtcbiAgICAgICAgICAgICAgLy8gWFhYOiBjb25zaWRlciB0byBtb3ZlIGNvZGVzIHRvIGVkaXRvciBmb3IgcmVjb3JkaW5nIHJlZG8vdW5kby5cbiAgICAgICAgICAgICAgdGhpcy5sYXN0V29yZFJhbmdlLmluc2VydE5vZGUobm9kZSk7XG4gICAgICAgICAgICAgIHJhbmdlLmNyZWF0ZUZyb21Ob2RlKG5vZGUpLmNvbGxhcHNlKCkuc2VsZWN0KCk7XG4gICAgICAgICAgICAgIHRoaXMubGFzdFdvcmRSYW5nZSA9IG51bGw7XG4gICAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xuICAgICAgICAgICAgICB0aGlzLmNvbnRleHQudHJpZ2dlckV2ZW50KCdjaGFuZ2UnLCB0aGlzLiRlZGl0YWJsZS5odG1sKCksIHRoaXMuJGVkaXRhYmxlWzBdKTtcbiAgICAgICAgICAgICAgdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmZvY3VzJyk7XG4gICAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIEhpbnRQb3BvdmVyLnByb3RvdHlwZS5ub2RlRnJvbUl0ZW0gPSBmdW5jdGlvbiAoJGl0ZW0pIHtcbiAgICAgICAgICB2YXIgaGludCA9IHRoaXMuaGludHNbJGl0ZW0uZGF0YSgnaW5kZXgnKV07XG4gICAgICAgICAgdmFyIGl0ZW0gPSAkaXRlbS5kYXRhKCdpdGVtJyk7XG4gICAgICAgICAgdmFyIG5vZGUgPSBoaW50LmNvbnRlbnQgPyBoaW50LmNvbnRlbnQoaXRlbSkgOiBpdGVtO1xuICAgICAgICAgIGlmICh0eXBlb2Ygbm9kZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgbm9kZSA9IGRvbS5jcmVhdGVUZXh0KG5vZGUpO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gbm9kZTtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuY3JlYXRlSXRlbVRlbXBsYXRlcyA9IGZ1bmN0aW9uIChoaW50SWR4LCBpdGVtcykge1xuICAgICAgICAgIHZhciBoaW50ID0gdGhpcy5oaW50c1toaW50SWR4XTtcbiAgICAgICAgICByZXR1cm4gaXRlbXMubWFwKGZ1bmN0aW9uIChpdGVtLCBpZHgpIHtcbiAgICAgICAgICAgICAgdmFyICRpdGVtID0gJCQxKCc8ZGl2IGNsYXNzPVwibm90ZS1oaW50LWl0ZW1cIi8+Jyk7XG4gICAgICAgICAgICAgICRpdGVtLmFwcGVuZChoaW50LnRlbXBsYXRlID8gaGludC50ZW1wbGF0ZShpdGVtKSA6IGl0ZW0gKyAnJyk7XG4gICAgICAgICAgICAgICRpdGVtLmRhdGEoe1xuICAgICAgICAgICAgICAgICAgJ2luZGV4JzogaGludElkeCxcbiAgICAgICAgICAgICAgICAgICdpdGVtJzogaXRlbVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgcmV0dXJuICRpdGVtO1xuICAgICAgICAgIH0pO1xuICAgICAgfTtcbiAgICAgIEhpbnRQb3BvdmVyLnByb3RvdHlwZS5oYW5kbGVLZXlkb3duID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICBpZiAoIXRoaXMuJHBvcG92ZXIuaXMoJzp2aXNpYmxlJykpIHtcbiAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZS5rZXlDb2RlID09PSBrZXkuY29kZS5FTlRFUikge1xuICAgICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgIHRoaXMucmVwbGFjZSgpO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIGlmIChlLmtleUNvZGUgPT09IGtleS5jb2RlLlVQKSB7XG4gICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgdGhpcy5tb3ZlVXAoKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgZWxzZSBpZiAoZS5rZXlDb2RlID09PSBrZXkuY29kZS5ET1dOKSB7XG4gICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgdGhpcy5tb3ZlRG93bigpO1xuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuc2VhcmNoS2V5d29yZCA9IGZ1bmN0aW9uIChpbmRleCwga2V5d29yZCwgY2FsbGJhY2spIHtcbiAgICAgICAgICB2YXIgaGludCA9IHRoaXMuaGludHNbaW5kZXhdO1xuICAgICAgICAgIGlmIChoaW50ICYmIGhpbnQubWF0Y2gudGVzdChrZXl3b3JkKSAmJiBoaW50LnNlYXJjaCkge1xuICAgICAgICAgICAgICB2YXIgbWF0Y2hlcyA9IGhpbnQubWF0Y2guZXhlYyhrZXl3b3JkKTtcbiAgICAgICAgICAgICAgaGludC5zZWFyY2gobWF0Y2hlc1sxXSwgY2FsbGJhY2spO1xuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgY2FsbGJhY2soKTtcbiAgICAgICAgICB9XG4gICAgICB9O1xuICAgICAgSGludFBvcG92ZXIucHJvdG90eXBlLmNyZWF0ZUdyb3VwID0gZnVuY3Rpb24gKGlkeCwga2V5d29yZCkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgdmFyICRncm91cCA9ICQkMSgnPGRpdiBjbGFzcz1cIm5vdGUtaGludC1ncm91cCBub3RlLWhpbnQtZ3JvdXAtJyArIGlkeCArICdcIi8+Jyk7XG4gICAgICAgICAgdGhpcy5zZWFyY2hLZXl3b3JkKGlkeCwga2V5d29yZCwgZnVuY3Rpb24gKGl0ZW1zKSB7XG4gICAgICAgICAgICAgIGl0ZW1zID0gaXRlbXMgfHwgW107XG4gICAgICAgICAgICAgIGlmIChpdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICAgICRncm91cC5odG1sKF90aGlzLmNyZWF0ZUl0ZW1UZW1wbGF0ZXMoaWR4LCBpdGVtcykpO1xuICAgICAgICAgICAgICAgICAgX3RoaXMuc2hvdygpO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSk7XG4gICAgICAgICAgcmV0dXJuICRncm91cDtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuaGFuZGxlS2V5dXAgPSBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgICAgaWYgKCFsaXN0cy5jb250YWlucyhba2V5LmNvZGUuRU5URVIsIGtleS5jb2RlLlVQLCBrZXkuY29kZS5ET1dOXSwgZS5rZXlDb2RlKSkge1xuICAgICAgICAgICAgICB2YXIgd29yZFJhbmdlID0gdGhpcy5jb250ZXh0Lmludm9rZSgnZWRpdG9yLmdldExhc3RSYW5nZScpLmdldFdvcmRSYW5nZSgpO1xuICAgICAgICAgICAgICB2YXIga2V5d29yZF8xID0gd29yZFJhbmdlLnRvU3RyaW5nKCk7XG4gICAgICAgICAgICAgIGlmICh0aGlzLmhpbnRzLmxlbmd0aCAmJiBrZXl3b3JkXzEpIHtcbiAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnRlbnQuZW1wdHkoKTtcbiAgICAgICAgICAgICAgICAgIHZhciBibmQgPSBmdW5jLnJlY3QyYm5kKGxpc3RzLmxhc3Qod29yZFJhbmdlLmdldENsaWVudFJlY3RzKCkpKTtcbiAgICAgICAgICAgICAgICAgIGlmIChibmQpIHtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRwb3BvdmVyLmhpZGUoKTtcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxhc3RXb3JkUmFuZ2UgPSB3b3JkUmFuZ2U7XG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5oaW50cy5mb3JFYWNoKGZ1bmN0aW9uIChoaW50LCBpZHgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGhpbnQubWF0Y2gudGVzdChrZXl3b3JkXzEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBfdGhpcy5jcmVhdGVHcm91cChpZHgsIGtleXdvcmRfMSkuYXBwZW5kVG8oX3RoaXMuJGNvbnRlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgLy8gc2VsZWN0IGZpcnN0IC5ub3RlLWhpbnQtaXRlbVxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJGNvbnRlbnQuZmluZCgnLm5vdGUtaGludC1pdGVtOmZpcnN0JykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICAgIC8vIHNldCBwb3NpdGlvbiBmb3IgcG9wb3ZlciBhZnRlciBncm91cCBpcyBjcmVhdGVkXG4gICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZGlyZWN0aW9uID09PSAndG9wJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLiRwb3BvdmVyLmNzcyh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiBibmQubGVmdCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogYm5kLnRvcCAtIHRoaXMuJHBvcG92ZXIub3V0ZXJIZWlnaHQoKSAtIFBPUE9WRVJfRElTVFxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuJHBvcG92ZXIuY3NzKHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IGJuZC5sZWZ0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiBibmQudG9wICsgYm5kLmhlaWdodCArIFBPUE9WRVJfRElTVFxuICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuc2hvdyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwb3BvdmVyLnNob3coKTtcbiAgICAgIH07XG4gICAgICBIaW50UG9wb3Zlci5wcm90b3R5cGUuaGlkZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLiRwb3BvdmVyLmhpZGUoKTtcbiAgICAgIH07XG4gICAgICByZXR1cm4gSGludFBvcG92ZXI7XG4gIH0oKSk7XG5cbiAgJCQxLnN1bW1lcm5vdGUgPSAkJDEuZXh0ZW5kKCQkMS5zdW1tZXJub3RlLCB7XG4gICAgICB2ZXJzaW9uOiAnMC44LjEyJyxcbiAgICAgIHBsdWdpbnM6IHt9LFxuICAgICAgZG9tOiBkb20sXG4gICAgICByYW5nZTogcmFuZ2UsXG4gICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgbGFuZ0luZm86ICQkMS5zdW1tZXJub3RlLmxhbmdbJ2VuLVVTJ10sXG4gICAgICAgICAgbW9kdWxlczoge1xuICAgICAgICAgICAgICAnZWRpdG9yJzogRWRpdG9yLFxuICAgICAgICAgICAgICAnY2xpcGJvYXJkJzogQ2xpcGJvYXJkLFxuICAgICAgICAgICAgICAnZHJvcHpvbmUnOiBEcm9wem9uZSxcbiAgICAgICAgICAgICAgJ2NvZGV2aWV3JzogQ29kZVZpZXcsXG4gICAgICAgICAgICAgICdzdGF0dXNiYXInOiBTdGF0dXNiYXIsXG4gICAgICAgICAgICAgICdmdWxsc2NyZWVuJzogRnVsbHNjcmVlbixcbiAgICAgICAgICAgICAgJ2hhbmRsZSc6IEhhbmRsZSxcbiAgICAgICAgICAgICAgLy8gRklYTUU6IEhpbnRQb3BvdmVyIG11c3QgYmUgZnJvbnQgb2YgYXV0b2xpbmtcbiAgICAgICAgICAgICAgLy8gIC0gU2NyaXB0IGVycm9yIGFib3V0IHJhbmdlIHdoZW4gRW50ZXIga2V5IGlzIHByZXNzZWQgb24gaGludCBwb3BvdmVyXG4gICAgICAgICAgICAgICdoaW50UG9wb3Zlcic6IEhpbnRQb3BvdmVyLFxuICAgICAgICAgICAgICAnYXV0b0xpbmsnOiBBdXRvTGluayxcbiAgICAgICAgICAgICAgJ2F1dG9TeW5jJzogQXV0b1N5bmMsXG4gICAgICAgICAgICAgICdhdXRvUmVwbGFjZSc6IEF1dG9SZXBsYWNlLFxuICAgICAgICAgICAgICAncGxhY2Vob2xkZXInOiBQbGFjZWhvbGRlcixcbiAgICAgICAgICAgICAgJ2J1dHRvbnMnOiBCdXR0b25zLFxuICAgICAgICAgICAgICAndG9vbGJhcic6IFRvb2xiYXIsXG4gICAgICAgICAgICAgICdsaW5rRGlhbG9nJzogTGlua0RpYWxvZyxcbiAgICAgICAgICAgICAgJ2xpbmtQb3BvdmVyJzogTGlua1BvcG92ZXIsXG4gICAgICAgICAgICAgICdpbWFnZURpYWxvZyc6IEltYWdlRGlhbG9nLFxuICAgICAgICAgICAgICAnaW1hZ2VQb3BvdmVyJzogSW1hZ2VQb3BvdmVyLFxuICAgICAgICAgICAgICAndGFibGVQb3BvdmVyJzogVGFibGVQb3BvdmVyLFxuICAgICAgICAgICAgICAndmlkZW9EaWFsb2cnOiBWaWRlb0RpYWxvZyxcbiAgICAgICAgICAgICAgJ2hlbHBEaWFsb2cnOiBIZWxwRGlhbG9nLFxuICAgICAgICAgICAgICAnYWlyUG9wb3Zlcic6IEFpclBvcG92ZXJcbiAgICAgICAgICB9LFxuICAgICAgICAgIGJ1dHRvbnM6IHt9LFxuICAgICAgICAgIGxhbmc6ICdlbi1VUycsXG4gICAgICAgICAgZm9sbG93aW5nVG9vbGJhcjogZmFsc2UsXG4gICAgICAgICAgb3RoZXJTdGF0aWNCYXI6ICcnLFxuICAgICAgICAgIC8vIHRvb2xiYXJcbiAgICAgICAgICB0b29sYmFyOiBbXG4gICAgICAgICAgICAgIFsnc3R5bGUnLCBbJ3N0eWxlJ11dLFxuICAgICAgICAgICAgICBbJ2ZvbnQnLCBbJ2JvbGQnLCAndW5kZXJsaW5lJywgJ2NsZWFyJ11dLFxuICAgICAgICAgICAgICBbJ2ZvbnRuYW1lJywgWydmb250bmFtZSddXSxcbiAgICAgICAgICAgICAgWydjb2xvcicsIFsnY29sb3InXV0sXG4gICAgICAgICAgICAgIFsncGFyYScsIFsndWwnLCAnb2wnLCAncGFyYWdyYXBoJ11dLFxuICAgICAgICAgICAgICBbJ3RhYmxlJywgWyd0YWJsZSddXSxcbiAgICAgICAgICAgICAgWydpbnNlcnQnLCBbJ2xpbmsnLCAncGljdHVyZScsICd2aWRlbyddXSxcbiAgICAgICAgICAgICAgWyd2aWV3JywgWydmdWxsc2NyZWVuJywgJ2NvZGV2aWV3JywgJ2hlbHAnXV0sXG4gICAgICAgICAgXSxcbiAgICAgICAgICAvLyBwb3BvdmVyXG4gICAgICAgICAgcG9wYXRtb3VzZTogdHJ1ZSxcbiAgICAgICAgICBwb3BvdmVyOiB7XG4gICAgICAgICAgICAgIGltYWdlOiBbXG4gICAgICAgICAgICAgICAgICBbJ3Jlc2l6ZScsIFsncmVzaXplRnVsbCcsICdyZXNpemVIYWxmJywgJ3Jlc2l6ZVF1YXJ0ZXInLCAncmVzaXplTm9uZSddXSxcbiAgICAgICAgICAgICAgICAgIFsnZmxvYXQnLCBbJ2Zsb2F0TGVmdCcsICdmbG9hdFJpZ2h0JywgJ2Zsb2F0Tm9uZSddXSxcbiAgICAgICAgICAgICAgICAgIFsncmVtb3ZlJywgWydyZW1vdmVNZWRpYSddXSxcbiAgICAgICAgICAgICAgXSxcbiAgICAgICAgICAgICAgbGluazogW1xuICAgICAgICAgICAgICAgICAgWydsaW5rJywgWydsaW5rRGlhbG9nU2hvdycsICd1bmxpbmsnXV0sXG4gICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgIHRhYmxlOiBbXG4gICAgICAgICAgICAgICAgICBbJ2FkZCcsIFsnYWRkUm93RG93bicsICdhZGRSb3dVcCcsICdhZGRDb2xMZWZ0JywgJ2FkZENvbFJpZ2h0J11dLFxuICAgICAgICAgICAgICAgICAgWydkZWxldGUnLCBbJ2RlbGV0ZVJvdycsICdkZWxldGVDb2wnLCAnZGVsZXRlVGFibGUnXV0sXG4gICAgICAgICAgICAgIF0sXG4gICAgICAgICAgICAgIGFpcjogW1xuICAgICAgICAgICAgICAgICAgWydjb2xvcicsIFsnY29sb3InXV0sXG4gICAgICAgICAgICAgICAgICBbJ2ZvbnQnLCBbJ2JvbGQnLCAndW5kZXJsaW5lJywgJ2NsZWFyJ11dLFxuICAgICAgICAgICAgICAgICAgWydwYXJhJywgWyd1bCcsICdwYXJhZ3JhcGgnXV0sXG4gICAgICAgICAgICAgICAgICBbJ3RhYmxlJywgWyd0YWJsZSddXSxcbiAgICAgICAgICAgICAgICAgIFsnaW5zZXJ0JywgWydsaW5rJywgJ3BpY3R1cmUnXV0sXG4gICAgICAgICAgICAgIF1cbiAgICAgICAgICB9LFxuICAgICAgICAgIC8vIGFpciBtb2RlOiBpbmxpbmUgZWRpdG9yXG4gICAgICAgICAgYWlyTW9kZTogZmFsc2UsXG4gICAgICAgICAgd2lkdGg6IG51bGwsXG4gICAgICAgICAgaGVpZ2h0OiBudWxsLFxuICAgICAgICAgIGxpbmtUYXJnZXRCbGFuazogdHJ1ZSxcbiAgICAgICAgICBmb2N1czogZmFsc2UsXG4gICAgICAgICAgdGFiU2l6ZTogNCxcbiAgICAgICAgICBzdHlsZVdpdGhTcGFuOiB0cnVlLFxuICAgICAgICAgIHNob3J0Y3V0czogdHJ1ZSxcbiAgICAgICAgICB0ZXh0YXJlYUF1dG9TeW5jOiB0cnVlLFxuICAgICAgICAgIGhpbnREaXJlY3Rpb246ICdib3R0b20nLFxuICAgICAgICAgIHRvb2x0aXA6ICdhdXRvJyxcbiAgICAgICAgICBjb250YWluZXI6ICdib2R5JyxcbiAgICAgICAgICBtYXhUZXh0TGVuZ3RoOiAwLFxuICAgICAgICAgIGJsb2NrcXVvdGVCcmVha2luZ0xldmVsOiAyLFxuICAgICAgICAgIHNwZWxsQ2hlY2s6IHRydWUsXG4gICAgICAgICAgc3R5bGVUYWdzOiBbJ3AnLCAnYmxvY2txdW90ZScsICdwcmUnLCAnaDEnLCAnaDInLCAnaDMnLCAnaDQnLCAnaDUnLCAnaDYnXSxcbiAgICAgICAgICBmb250TmFtZXM6IFtcbiAgICAgICAgICAgICAgJ0FyaWFsJywgJ0FyaWFsIEJsYWNrJywgJ0NvbWljIFNhbnMgTVMnLCAnQ291cmllciBOZXcnLFxuICAgICAgICAgICAgICAnSGVsdmV0aWNhIE5ldWUnLCAnSGVsdmV0aWNhJywgJ0ltcGFjdCcsICdMdWNpZGEgR3JhbmRlJyxcbiAgICAgICAgICAgICAgJ1RhaG9tYScsICdUaW1lcyBOZXcgUm9tYW4nLCAnVmVyZGFuYScsXG4gICAgICAgICAgXSxcbiAgICAgICAgICBmb250TmFtZXNJZ25vcmVDaGVjazogW10sXG4gICAgICAgICAgZm9udFNpemVzOiBbJzgnLCAnOScsICcxMCcsICcxMScsICcxMicsICcxNCcsICcxOCcsICcyNCcsICczNiddLFxuICAgICAgICAgIC8vIHBhbGxldGUgY29sb3JzKG4geCBuKVxuICAgICAgICAgIGNvbG9yczogW1xuICAgICAgICAgICAgICBbJyMwMDAwMDAnLCAnIzQyNDI0MicsICcjNjM2MzYzJywgJyM5QzlDOTQnLCAnI0NFQzZDRScsICcjRUZFRkVGJywgJyNGN0Y3RjcnLCAnI0ZGRkZGRiddLFxuICAgICAgICAgICAgICBbJyNGRjAwMDAnLCAnI0ZGOUMwMCcsICcjRkZGRjAwJywgJyMwMEZGMDAnLCAnIzAwRkZGRicsICcjMDAwMEZGJywgJyM5QzAwRkYnLCAnI0ZGMDBGRiddLFxuICAgICAgICAgICAgICBbJyNGN0M2Q0UnLCAnI0ZGRTdDRScsICcjRkZFRkM2JywgJyNENkVGRDYnLCAnI0NFREVFNycsICcjQ0VFN0Y3JywgJyNENkQ2RTcnLCAnI0U3RDZERSddLFxuICAgICAgICAgICAgICBbJyNFNzlDOUMnLCAnI0ZGQzY5QycsICcjRkZFNzlDJywgJyNCNUQ2QTUnLCAnI0E1QzZDRScsICcjOUNDNkVGJywgJyNCNUE1RDYnLCAnI0Q2QTVCRCddLFxuICAgICAgICAgICAgICBbJyNFNzYzNjMnLCAnI0Y3QUQ2QicsICcjRkZENjYzJywgJyM5NEJEN0InLCAnIzczQTVBRCcsICcjNkJBRERFJywgJyM4QzdCQzYnLCAnI0M2N0JBNSddLFxuICAgICAgICAgICAgICBbJyNDRTAwMDAnLCAnI0U3OTQzOScsICcjRUZDNjMxJywgJyM2QkE1NEEnLCAnIzRBN0I4QycsICcjMzk4NEM2JywgJyM2MzRBQTUnLCAnI0E1NEE3QiddLFxuICAgICAgICAgICAgICBbJyM5QzAwMDAnLCAnI0I1NjMwOCcsICcjQkQ5NDAwJywgJyMzOTdCMjEnLCAnIzEwNEE1QScsICcjMDg1Mjk0JywgJyMzMTE4NzMnLCAnIzczMTg0MiddLFxuICAgICAgICAgICAgICBbJyM2MzAwMDAnLCAnIzdCMzkwMCcsICcjODQ2MzAwJywgJyMyOTUyMTgnLCAnIzA4MzEzOScsICcjMDAzMTYzJywgJyMyMTEwNEEnLCAnIzRBMTAzMSddLFxuICAgICAgICAgIF0sXG4gICAgICAgICAgLy8gaHR0cDovL2NoaXIuYWcvcHJvamVjdHMvbmFtZS10aGF0LWNvbG9yL1xuICAgICAgICAgIGNvbG9yc05hbWU6IFtcbiAgICAgICAgICAgICAgWydCbGFjaycsICdUdW5kb3JhJywgJ0RvdmUgR3JheScsICdTdGFyIER1c3QnLCAnUGFsZSBTbGF0ZScsICdHYWxsZXJ5JywgJ0FsYWJhc3RlcicsICdXaGl0ZSddLFxuICAgICAgICAgICAgICBbJ1JlZCcsICdPcmFuZ2UgUGVlbCcsICdZZWxsb3cnLCAnR3JlZW4nLCAnQ3lhbicsICdCbHVlJywgJ0VsZWN0cmljIFZpb2xldCcsICdNYWdlbnRhJ10sXG4gICAgICAgICAgICAgIFsnQXphbGVhJywgJ0thcnJ5JywgJ0VnZyBXaGl0ZScsICdaYW5haCcsICdCb3R0aWNlbGxpJywgJ1Ryb3BpY2FsIEJsdWUnLCAnTWlzY2hrYScsICdUd2lsaWdodCddLFxuICAgICAgICAgICAgICBbJ1RvbnlzIFBpbmsnLCAnUGVhY2ggT3JhbmdlJywgJ0NyZWFtIEJydWxlZScsICdTcHJvdXQnLCAnQ2FzcGVyJywgJ1BlcmFubycsICdDb2xkIFB1cnBsZScsICdDYXJleXMgUGluayddLFxuICAgICAgICAgICAgICBbJ01hbmR5JywgJ1JhamFoJywgJ0RhbmRlbGlvbicsICdPbGl2aW5lJywgJ0d1bGYgU3RyZWFtJywgJ1Zpa2luZycsICdCbHVlIE1hcmd1ZXJpdGUnLCAnUHVjZSddLFxuICAgICAgICAgICAgICBbJ0d1YXJkc21hbiBSZWQnLCAnRmlyZSBCdXNoJywgJ0dvbGRlbiBEcmVhbScsICdDaGVsc2VhIEN1Y3VtYmVyJywgJ1NtYWx0IEJsdWUnLCAnQm9zdG9uIEJsdWUnLCAnQnV0dGVyZmx5IEJ1c2gnLCAnQ2FkaWxsYWMnXSxcbiAgICAgICAgICAgICAgWydTYW5ncmlhJywgJ01haSBUYWknLCAnQnVkZGhhIEdvbGQnLCAnRm9yZXN0IEdyZWVuJywgJ0VkZW4nLCAnVmVuaWNlIEJsdWUnLCAnTWV0ZW9yaXRlJywgJ0NsYXJldCddLFxuICAgICAgICAgICAgICBbJ1Jvc2V3b29kJywgJ0Npbm5hbW9uJywgJ09saXZlJywgJ1BhcnNsZXknLCAnVGliZXInLCAnTWlkbmlnaHQgQmx1ZScsICdWYWxlbnRpbm8nLCAnTG91bG91J10sXG4gICAgICAgICAgXSxcbiAgICAgICAgICBjb2xvckJ1dHRvbjoge1xuICAgICAgICAgICAgICBmb3JlQ29sb3I6ICcjMDAwMDAwJyxcbiAgICAgICAgICAgICAgYmFja0NvbG9yOiAnI0ZGRkYwMCdcbiAgICAgICAgICB9LFxuICAgICAgICAgIGxpbmVIZWlnaHRzOiBbJzEuMCcsICcxLjInLCAnMS40JywgJzEuNScsICcxLjYnLCAnMS44JywgJzIuMCcsICczLjAnXSxcbiAgICAgICAgICB0YWJsZUNsYXNzTmFtZTogJ3RhYmxlIHRhYmxlLWJvcmRlcmVkJyxcbiAgICAgICAgICBpbnNlcnRUYWJsZU1heFNpemU6IHtcbiAgICAgICAgICAgICAgY29sOiAxMCxcbiAgICAgICAgICAgICAgcm93OiAxMFxuICAgICAgICAgIH0sXG4gICAgICAgICAgZGlhbG9nc0luQm9keTogZmFsc2UsXG4gICAgICAgICAgZGlhbG9nc0ZhZGU6IGZhbHNlLFxuICAgICAgICAgIG1heGltdW1JbWFnZUZpbGVTaXplOiBudWxsLFxuICAgICAgICAgIGNhbGxiYWNrczoge1xuICAgICAgICAgICAgICBvbkJlZm9yZUNvbW1hbmQ6IG51bGwsXG4gICAgICAgICAgICAgIG9uQmx1cjogbnVsbCxcbiAgICAgICAgICAgICAgb25CbHVyQ29kZXZpZXc6IG51bGwsXG4gICAgICAgICAgICAgIG9uQ2hhbmdlOiBudWxsLFxuICAgICAgICAgICAgICBvbkNoYW5nZUNvZGV2aWV3OiBudWxsLFxuICAgICAgICAgICAgICBvbkRpYWxvZ1Nob3duOiBudWxsLFxuICAgICAgICAgICAgICBvbkVudGVyOiBudWxsLFxuICAgICAgICAgICAgICBvbkZvY3VzOiBudWxsLFxuICAgICAgICAgICAgICBvbkltYWdlTGlua0luc2VydDogbnVsbCxcbiAgICAgICAgICAgICAgb25JbWFnZVVwbG9hZDogbnVsbCxcbiAgICAgICAgICAgICAgb25JbWFnZVVwbG9hZEVycm9yOiBudWxsLFxuICAgICAgICAgICAgICBvbkluaXQ6IG51bGwsXG4gICAgICAgICAgICAgIG9uS2V5ZG93bjogbnVsbCxcbiAgICAgICAgICAgICAgb25LZXl1cDogbnVsbCxcbiAgICAgICAgICAgICAgb25Nb3VzZWRvd246IG51bGwsXG4gICAgICAgICAgICAgIG9uTW91c2V1cDogbnVsbCxcbiAgICAgICAgICAgICAgb25QYXN0ZTogbnVsbCxcbiAgICAgICAgICAgICAgb25TY3JvbGw6IG51bGxcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNvZGVtaXJyb3I6IHtcbiAgICAgICAgICAgICAgbW9kZTogJ3RleHQvaHRtbCcsXG4gICAgICAgICAgICAgIGh0bWxNb2RlOiB0cnVlLFxuICAgICAgICAgICAgICBsaW5lTnVtYmVyczogdHJ1ZVxuICAgICAgICAgIH0sXG4gICAgICAgICAgY29kZXZpZXdGaWx0ZXI6IGZhbHNlLFxuICAgICAgICAgIGNvZGV2aWV3RmlsdGVyUmVnZXg6IC88XFwvKig/OmFwcGxldHxiKD86YXNlfGdzb3VuZHxsaW5rKXxlbWJlZHxmcmFtZSg/OnNldCk/fGlsYXllcnxsKD86YXllcnxpbmspfG1ldGF8b2JqZWN0fHMoPzpjcmlwdHx0eWxlKXx0KD86aXRsZXxleHRhcmVhKXx4bWwpW14+XSo/Pi9naSxcbiAgICAgICAgICBjb2Rldmlld0lmcmFtZUZpbHRlcjogdHJ1ZSxcbiAgICAgICAgICBjb2Rldmlld0lmcmFtZVdoaXRlbGlzdFNyYzogW10sXG4gICAgICAgICAgY29kZXZpZXdJZnJhbWVXaGl0ZWxpc3RTcmNCYXNlOiBbXG4gICAgICAgICAgICAgICd3d3cueW91dHViZS5jb20nLFxuICAgICAgICAgICAgICAnd3d3LnlvdXR1YmUtbm9jb29raWUuY29tJyxcbiAgICAgICAgICAgICAgJ3d3dy5mYWNlYm9vay5jb20nLFxuICAgICAgICAgICAgICAndmluZS5jbycsXG4gICAgICAgICAgICAgICdpbnN0YWdyYW0uY29tJyxcbiAgICAgICAgICAgICAgJ3BsYXllci52aW1lby5jb20nLFxuICAgICAgICAgICAgICAnd3d3LmRhaWx5bW90aW9uLmNvbScsXG4gICAgICAgICAgICAgICdwbGF5ZXIueW91a3UuY29tJyxcbiAgICAgICAgICAgICAgJ3YucXEuY29tJyxcbiAgICAgICAgICBdLFxuICAgICAgICAgIGtleU1hcDoge1xuICAgICAgICAgICAgICBwYzoge1xuICAgICAgICAgICAgICAgICAgJ0VOVEVSJzogJ2luc2VydFBhcmFncmFwaCcsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtaJzogJ3VuZG8nLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrWSc6ICdyZWRvJyxcbiAgICAgICAgICAgICAgICAgICdUQUInOiAndGFiJyxcbiAgICAgICAgICAgICAgICAgICdTSElGVCtUQUInOiAndW50YWInLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrQic6ICdib2xkJyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK0knOiAnaXRhbGljJyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1UnOiAndW5kZXJsaW5lJyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1NISUZUK1MnOiAnc3RyaWtldGhyb3VnaCcsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtCQUNLU0xBU0gnOiAncmVtb3ZlRm9ybWF0JyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1NISUZUK0wnOiAnanVzdGlmeUxlZnQnLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrU0hJRlQrRSc6ICdqdXN0aWZ5Q2VudGVyJyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1NISUZUK1InOiAnanVzdGlmeVJpZ2h0JyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1NISUZUK0onOiAnanVzdGlmeUZ1bGwnLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrU0hJRlQrTlVNNyc6ICdpbnNlcnRVbm9yZGVyZWRMaXN0JyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK1NISUZUK05VTTgnOiAnaW5zZXJ0T3JkZXJlZExpc3QnLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrTEVGVEJSQUNLRVQnOiAnb3V0ZGVudCcsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtSSUdIVEJSQUNLRVQnOiAnaW5kZW50JyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK05VTTAnOiAnZm9ybWF0UGFyYScsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtOVU0xJzogJ2Zvcm1hdEgxJyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK05VTTInOiAnZm9ybWF0SDInLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrTlVNMyc6ICdmb3JtYXRIMycsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtOVU00JzogJ2Zvcm1hdEg0JyxcbiAgICAgICAgICAgICAgICAgICdDVFJMK05VTTUnOiAnZm9ybWF0SDUnLFxuICAgICAgICAgICAgICAgICAgJ0NUUkwrTlVNNic6ICdmb3JtYXRINicsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtFTlRFUic6ICdpbnNlcnRIb3Jpem9udGFsUnVsZScsXG4gICAgICAgICAgICAgICAgICAnQ1RSTCtLJzogJ2xpbmtEaWFsb2cuc2hvdydcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgbWFjOiB7XG4gICAgICAgICAgICAgICAgICAnRU5URVInOiAnaW5zZXJ0UGFyYWdyYXBoJyxcbiAgICAgICAgICAgICAgICAgICdDTUQrWic6ICd1bmRvJyxcbiAgICAgICAgICAgICAgICAgICdDTUQrU0hJRlQrWic6ICdyZWRvJyxcbiAgICAgICAgICAgICAgICAgICdUQUInOiAndGFiJyxcbiAgICAgICAgICAgICAgICAgICdTSElGVCtUQUInOiAndW50YWInLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtCJzogJ2JvbGQnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtJJzogJ2l0YWxpYycsXG4gICAgICAgICAgICAgICAgICAnQ01EK1UnOiAndW5kZXJsaW5lJyxcbiAgICAgICAgICAgICAgICAgICdDTUQrU0hJRlQrUyc6ICdzdHJpa2V0aHJvdWdoJyxcbiAgICAgICAgICAgICAgICAgICdDTUQrQkFDS1NMQVNIJzogJ3JlbW92ZUZvcm1hdCcsXG4gICAgICAgICAgICAgICAgICAnQ01EK1NISUZUK0wnOiAnanVzdGlmeUxlZnQnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtTSElGVCtFJzogJ2p1c3RpZnlDZW50ZXInLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtTSElGVCtSJzogJ2p1c3RpZnlSaWdodCcsXG4gICAgICAgICAgICAgICAgICAnQ01EK1NISUZUK0onOiAnanVzdGlmeUZ1bGwnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtTSElGVCtOVU03JzogJ2luc2VydFVub3JkZXJlZExpc3QnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtTSElGVCtOVU04JzogJ2luc2VydE9yZGVyZWRMaXN0JyxcbiAgICAgICAgICAgICAgICAgICdDTUQrTEVGVEJSQUNLRVQnOiAnb3V0ZGVudCcsXG4gICAgICAgICAgICAgICAgICAnQ01EK1JJR0hUQlJBQ0tFVCc6ICdpbmRlbnQnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtOVU0wJzogJ2Zvcm1hdFBhcmEnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtOVU0xJzogJ2Zvcm1hdEgxJyxcbiAgICAgICAgICAgICAgICAgICdDTUQrTlVNMic6ICdmb3JtYXRIMicsXG4gICAgICAgICAgICAgICAgICAnQ01EK05VTTMnOiAnZm9ybWF0SDMnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtOVU00JzogJ2Zvcm1hdEg0JyxcbiAgICAgICAgICAgICAgICAgICdDTUQrTlVNNSc6ICdmb3JtYXRINScsXG4gICAgICAgICAgICAgICAgICAnQ01EK05VTTYnOiAnZm9ybWF0SDYnLFxuICAgICAgICAgICAgICAgICAgJ0NNRCtFTlRFUic6ICdpbnNlcnRIb3Jpem9udGFsUnVsZScsXG4gICAgICAgICAgICAgICAgICAnQ01EK0snOiAnbGlua0RpYWxvZy5zaG93J1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBpY29uczoge1xuICAgICAgICAgICAgICAnYWxpZ24nOiAnbm90ZS1pY29uLWFsaWduJyxcbiAgICAgICAgICAgICAgJ2FsaWduQ2VudGVyJzogJ25vdGUtaWNvbi1hbGlnbi1jZW50ZXInLFxuICAgICAgICAgICAgICAnYWxpZ25KdXN0aWZ5JzogJ25vdGUtaWNvbi1hbGlnbi1qdXN0aWZ5JyxcbiAgICAgICAgICAgICAgJ2FsaWduTGVmdCc6ICdub3RlLWljb24tYWxpZ24tbGVmdCcsXG4gICAgICAgICAgICAgICdhbGlnblJpZ2h0JzogJ25vdGUtaWNvbi1hbGlnbi1yaWdodCcsXG4gICAgICAgICAgICAgICdyb3dCZWxvdyc6ICdub3RlLWljb24tcm93LWJlbG93JyxcbiAgICAgICAgICAgICAgJ2NvbEJlZm9yZSc6ICdub3RlLWljb24tY29sLWJlZm9yZScsXG4gICAgICAgICAgICAgICdjb2xBZnRlcic6ICdub3RlLWljb24tY29sLWFmdGVyJyxcbiAgICAgICAgICAgICAgJ3Jvd0Fib3ZlJzogJ25vdGUtaWNvbi1yb3ctYWJvdmUnLFxuICAgICAgICAgICAgICAncm93UmVtb3ZlJzogJ25vdGUtaWNvbi1yb3ctcmVtb3ZlJyxcbiAgICAgICAgICAgICAgJ2NvbFJlbW92ZSc6ICdub3RlLWljb24tY29sLXJlbW92ZScsXG4gICAgICAgICAgICAgICdpbmRlbnQnOiAnbm90ZS1pY29uLWFsaWduLWluZGVudCcsXG4gICAgICAgICAgICAgICdvdXRkZW50JzogJ25vdGUtaWNvbi1hbGlnbi1vdXRkZW50JyxcbiAgICAgICAgICAgICAgJ2Fycm93c0FsdCc6ICdub3RlLWljb24tYXJyb3dzLWFsdCcsXG4gICAgICAgICAgICAgICdib2xkJzogJ25vdGUtaWNvbi1ib2xkJyxcbiAgICAgICAgICAgICAgJ2NhcmV0JzogJ25vdGUtaWNvbi1jYXJldCcsXG4gICAgICAgICAgICAgICdjaXJjbGUnOiAnbm90ZS1pY29uLWNpcmNsZScsXG4gICAgICAgICAgICAgICdjbG9zZSc6ICdub3RlLWljb24tY2xvc2UnLFxuICAgICAgICAgICAgICAnY29kZSc6ICdub3RlLWljb24tY29kZScsXG4gICAgICAgICAgICAgICdlcmFzZXInOiAnbm90ZS1pY29uLWVyYXNlcicsXG4gICAgICAgICAgICAgICdmbG9hdExlZnQnOiAnbm90ZS1pY29uLWZsb2F0LWxlZnQnLFxuICAgICAgICAgICAgICAnZmxvYXRSaWdodCc6ICdub3RlLWljb24tZmxvYXQtcmlnaHQnLFxuICAgICAgICAgICAgICAnZm9udCc6ICdub3RlLWljb24tZm9udCcsXG4gICAgICAgICAgICAgICdmcmFtZSc6ICdub3RlLWljb24tZnJhbWUnLFxuICAgICAgICAgICAgICAnaXRhbGljJzogJ25vdGUtaWNvbi1pdGFsaWMnLFxuICAgICAgICAgICAgICAnbGluayc6ICdub3RlLWljb24tbGluaycsXG4gICAgICAgICAgICAgICd1bmxpbmsnOiAnbm90ZS1pY29uLWNoYWluLWJyb2tlbicsXG4gICAgICAgICAgICAgICdtYWdpYyc6ICdub3RlLWljb24tbWFnaWMnLFxuICAgICAgICAgICAgICAnbWVudUNoZWNrJzogJ25vdGUtaWNvbi1tZW51LWNoZWNrJyxcbiAgICAgICAgICAgICAgJ21pbnVzJzogJ25vdGUtaWNvbi1taW51cycsXG4gICAgICAgICAgICAgICdvcmRlcmVkbGlzdCc6ICdub3RlLWljb24tb3JkZXJlZGxpc3QnLFxuICAgICAgICAgICAgICAncGVuY2lsJzogJ25vdGUtaWNvbi1wZW5jaWwnLFxuICAgICAgICAgICAgICAncGljdHVyZSc6ICdub3RlLWljb24tcGljdHVyZScsXG4gICAgICAgICAgICAgICdxdWVzdGlvbic6ICdub3RlLWljb24tcXVlc3Rpb24nLFxuICAgICAgICAgICAgICAncmVkbyc6ICdub3RlLWljb24tcmVkbycsXG4gICAgICAgICAgICAgICdyb2xsYmFjayc6ICdub3RlLWljb24tcm9sbGJhY2snLFxuICAgICAgICAgICAgICAnc3F1YXJlJzogJ25vdGUtaWNvbi1zcXVhcmUnLFxuICAgICAgICAgICAgICAnc3RyaWtldGhyb3VnaCc6ICdub3RlLWljb24tc3RyaWtldGhyb3VnaCcsXG4gICAgICAgICAgICAgICdzdWJzY3JpcHQnOiAnbm90ZS1pY29uLXN1YnNjcmlwdCcsXG4gICAgICAgICAgICAgICdzdXBlcnNjcmlwdCc6ICdub3RlLWljb24tc3VwZXJzY3JpcHQnLFxuICAgICAgICAgICAgICAndGFibGUnOiAnbm90ZS1pY29uLXRhYmxlJyxcbiAgICAgICAgICAgICAgJ3RleHRIZWlnaHQnOiAnbm90ZS1pY29uLXRleHQtaGVpZ2h0JyxcbiAgICAgICAgICAgICAgJ3RyYXNoJzogJ25vdGUtaWNvbi10cmFzaCcsXG4gICAgICAgICAgICAgICd1bmRlcmxpbmUnOiAnbm90ZS1pY29uLXVuZGVybGluZScsXG4gICAgICAgICAgICAgICd1bmRvJzogJ25vdGUtaWNvbi11bmRvJyxcbiAgICAgICAgICAgICAgJ3Vub3JkZXJlZGxpc3QnOiAnbm90ZS1pY29uLXVub3JkZXJlZGxpc3QnLFxuICAgICAgICAgICAgICAndmlkZW8nOiAnbm90ZS1pY29uLXZpZGVvJ1xuICAgICAgICAgIH1cbiAgICAgIH1cbiAgfSk7XG5cbiAgJCQxLnN1bW1lcm5vdGUgPSAkJDEuZXh0ZW5kKCQkMS5zdW1tZXJub3RlLCB7XG4gICAgICB1aTogdWlcbiAgfSk7XG5cbn0pKTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXN1bW1lcm5vdGUuanMubWFwXG4iLCIvKiBnbG9iYWxzIF9fd2VicGFja19hbWRfb3B0aW9uc19fICovXG5tb2R1bGUuZXhwb3J0cyA9IF9fd2VicGFja19hbWRfb3B0aW9uc19fO1xuIl0sInNvdXJjZVJvb3QiOiIifQ==
