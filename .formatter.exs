[
  import_deps: [:ecto, :phoenix],
  inputs: [
    "*.{ex,exs}",
    "{config,lib,test}/**/*.{ex,exs}",
    "mix.exs"
  ],
  subdirectories: ["priv/*/migrations"]
]
