defmodule MokaWeb.DocView do
  use MokaWeb, :view

  def render("styles.forms.html", assigns) do
    """
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/dracula.min.css">
    """
  end

  def render("scripts.forms.html", assigns) do
    """
    <script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.6/showdown.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
    <script>
    $(document).ready(function() {
      var classMap = {
        table: 'table table-striped table-bordered'
      }

      var bindings = Object.keys(classMap)
        .map(key => ({
          type: 'output',
          regex: new RegExp(`<${key}>`, 'g'),
          replace: `<${key} class="${classMap[key]}">`
        }));


      let converter = new showdown.Converter({
        extensions: [...bindings],
        tables: true
      });
      $('.jsShowdown').each(function(i, block) {
        var el = $(block);
        var html = converter.makeHtml(el.text());
        el.html(html);
      })

      $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
      });
    });
    </script>
    """
  end
end
