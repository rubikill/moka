defmodule MokaWeb.ViewHelper do
  @moduledoc """
  MokaWeb.ViewHelper
  """

  use Phoenix.HTML
  import MokaWeb.Gettext
  alias MokaWeb.Router.Helpers, as: Routes

  def render_input(form, field, opts \\ [])

  def render_input(form, field, opts) do
    using = opts[:using] || Phoenix.HTML.Form.input_type(form, field)

    content_tag :div, class: "form-group #{input_state_class(form, field)}" do
      label_opts = [class: "form-label"]

      label_opts =
        case using do
          :file_input -> Keyword.put(label_opts, :class, "custom-file-label")
          :multiple_file_input -> Keyword.put(label_opts, :class, "custom-file-label")
          :image_input -> Keyword.put(label_opts, :class, "custom-file-label")
          _ -> label_opts
        end

      l =
        label form, field, label_opts do
          required_span =
            if opts[:required] do
              content_tag :span, class: "form-required" do
                "*"
              end
            else
              ""
            end

          label_text = opts[:label] || humanize(field)
          [label_text, required_span]
        end

      value = opts[:value]

      input_opts = opts[:input_opts] || []
      input_opts = input_opts ++ [class: "form-control"]

      input_opts =
        if is_nil(value) do
          input_opts
        else
          input_opts ++ [value: value]
        end

      id = opts[:id]

      input_opts =
        if is_nil(id) do
          input_opts
        else
          input_opts ++ [id: id]
        end

      i = _render_input(using, form, field, input_opts, opts)
      e = MokaWeb.ErrorHelpers.error_tag(form, field) || ""

      case using do
        :hidden_input ->
          [i, e]

        :file_input ->
          d =
            content_tag :div, class: "custom-file" do
              [i, l]
            end

          [d, e]

        :multiple_file_input ->
          d =
            content_tag :div, class: "custom-file" do
              [i, l]
            end

          [d, e]

        :image_input ->
          d =
            content_tag :div, class: "custom-file" do
              [i, l]
            end

          {:safe, si} = i

          img_id =
            si
            |> Enum.at(2)
            |> Enum.at(1)
            |> Enum.at(4)

          img =
            apply(Phoenix.HTML.Tag, :img_tag, [
              "#",
              [class: "img-responsive mt-5", data: [id: img_id]]
            ])

          w_hidden = hidden_input(form, :"#{field}[width]")
          h_hidden = hidden_input(form, :"#{field}[height]")

          [[d, e], img, w_hidden, h_hidden]

        :checkbox ->
          d =
            content_tag :label, class: "custom-switch" do
              indicator = content_tag(:span, "", class: "custom-switch-indicator")

              description =
                content_tag :span, class: "custom-switch-description" do
                  opts[:description] || ""
                end

              [i, indicator, description]
            end

          [l, d, e]

        _ ->
          [l, i, e]
      end
    end
  end

  defp input_state_class(form, field) do
    cond do
      # The form was not yet submitted
      !form.source.action ->
        ""

      form.errors[field] ->
        "is-invalid"

      true ->
        "is-valid"
    end
  end

  # Implement clauses below for custom inputs.
  defp _render_input(:timepicker, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "form-control datepicker-here")
      |> Keyword.put(:"data-timepicker", "true")
      |> Keyword.put(:"data-language", "en")
      |> Keyword.put(:"data-date-format", input_opts[:format] || "yyyy-mm-dd")
      |> Keyword.put(:"data-time-format", "hh:ii:00")

    apply(Phoenix.HTML.Form, :text_input, [form, field, input_opts])
  end

  defp _render_input(:datepicker, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "form-control datepicker-here")
      |> Keyword.put(:"data-date-format", input_opts[:format] || "yyyy/mm/dd")

    apply(Phoenix.HTML.Form, :text_input, [form, field, input_opts])
  end

  defp _render_input(:datetimepicker, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "form-control datepicker-here")
      |> Keyword.put(:"data-timepicker", "true")
      |> Keyword.put(:"data-language", "en")
      |> Keyword.put(:"data-date-format", input_opts[:format] || "yyyy-mm-dd")
      |> Keyword.put(:"data-time-format", "hh:ii:00")

    apply(Phoenix.HTML.Form, :text_input, [form, field, input_opts])
  end

  defp _render_input(:checkbox, form, field, input_opts, _opts) do
    input_opts = Keyword.put(input_opts, :class, "custom-switch-input")
    apply(Phoenix.HTML.Form, :checkbox, [form, field, input_opts])
  end

  defp _render_input(:select, form, field, input_opts, opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "form-control custom-select")
      |> Keyword.put(:"data-toggle", "selectize")

    apply(Phoenix.HTML.Form, :select, [form, field, opts[:options], input_opts])
  end

  defp _render_input(:multiple_select, form, field, input_opts, opts) do
    input_opts = Keyword.put(input_opts, :class, "form-control js-choice")
    apply(Phoenix.HTML.Form, :multiple_select, [form, field, opts[:options], input_opts])
  end

  defp _render_input(:text_editor, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "form-control summernote")

    apply(Phoenix.HTML.Form, :textarea, [form, field, input_opts])
  end

  defp _render_input(:multiple_file_input, form, field, input_opts, _opts) do
    input_opts = Keyword.put(input_opts, :multiple, true)
    apply(Phoenix.HTML.Form, :file_input, [form, field, input_opts])
  end

  defp _render_input(:file_input, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "custom-file-input")

    apply(Phoenix.HTML.Form, :file_input, [form, field, input_opts])
  end

  defp _render_input(:image_input, form, field, input_opts, _opts) do
    input_opts =
      input_opts
      |> Keyword.put(:class, "custom-file-input img-input")

    apply(Phoenix.HTML.Form, :file_input, [form, field, input_opts])
  end

  defp _render_input(type, form, field, input_opts, _opts) do
    apply(Phoenix.HTML.Form, type, [form, field, input_opts])
  end

  def form_group(form, field, do: block) do
    content_tag(
      :div,
      [block, MokaWeb.ErrorHelpers.error_tag(form, field)],
      class: "form-group #{input_state_class(form, field)}"
    )
  end

  def has_error?(form_data, field) do
    if Keyword.has_key?(form_data.errors, field) do
      "has-error"
    end
  end

  def status_class(class_name) do
    cond do
      class_name in [true, "published", "finished", "accepted"] -> "label-success"
      class_name in [false, "closed", "cancelled", "rejected"] -> "label-danger"
      class_name in ["draft", "pending"] -> "label-info"
      class_name in ["processing"] -> "label-primary"
      class_name in ["running"] -> "label-warning"
      true -> "label-inverse"
    end
  end

  def truncate(text, len) do
    String.slice(text, 0, len)
  end

  def percent(devisor, devidend) do
    if devidend > 0, do: Float.round(100 * devisor / devidend, 2), else: 0
  end

  def float2string(f, d \\ 0) do
    # ~r/\B(?=(\d{3})+\b)/ is use for format with semi
    :erlang.float_to_binary(f, decimals: d)
    |> String.replace(~r/\B(?=(\d{3})+\b)/, ",")
  end

  def ellipsis(text, len \\ 100, delimiter \\ "...")

  def ellipsis(text, len, delimiter) do
    if String.length(text) > len do
      truncate(text, len + String.length(delimiter)) <> delimiter
    else
      text
    end
  end

  def reduce_number(value \\ 0)

  def reduce_number(value) when is_binary(value) do
    case Integer.parse(value) do
      {number, _} ->
        reduce_number(number)

      :error ->
        value
    end
  end

  def reduce_number(nil), do: "-"
  def reduce_number(value) when value < 1_000, do: "#{value}"

  def reduce_number(value) when value >= 1_000 and value < 1_000_000,
    do: "#{Float.floor(value / 1_000, 2) |> int_or_float}K"

  def reduce_number(value) when value >= 1_000_000 do
    "#{Float.floor(value / 1_000_000, 2) |> int_or_float}M"
  end

  def int_or_float(value) when is_integer(value), do: value

  def int_or_float(value) when is_float(value) do
    if value == Float.round(value, 0), do: round(value), else: value
  end

  @spec nav_state(Plug.Conn.t(), atom, list) :: any
  def nav_state(conn, controller, actions) when is_list(actions) do
    if conn.private.phoenix_controller == controller and conn.private.phoenix_action in actions do
      "active"
    end
  end

  @spec nav_state(Plug.Conn.t(), list) :: any
  def nav_state(conn, controllers) do
    if conn.private.phoenix_controller in controllers do
      "active"
    end
  end

  def get_menus(conn, prefix \\ "")

  def get_menus(conn, prefix) do
    [
      %{
        name: gettext("Dashboard"),
        href: prefix <> Routes.page_path(conn, :index),
        has_child: false,
        icon_class: "fe fe-home",
        childs: [
          %{
            name: gettext("Dashboard"),
            href: prefix <> Routes.page_path(conn, :index),
            has_child: false,
            childs: []
          }
        ]
      }
    ]
  end

  def render_value(nil, field) do
    ""
  end

  # Handle for case fields = [:a]
  def render_value(item, field) when is_atom(field) do
    render_value(item, {field, []})
  end

  # Handle for case fields = [[:a]]
  def render_value(item, fields) when is_list(fields) do
    render_value(item, {fields, []})
  end

  def render_value(nil, {field, opts}) do
    ""
  end

  # Handle for case fields = [{:a, []}]
  def render_value(item, {field, opts}) when is_atom(field) do
    render_function_full = opts[:render_function_full]
    render_function = opts[:render_function]

    value = Map.get(item, field, "")

    cond do
      !is_nil(render_function_full) -> render_function_full.({item, value})
      !is_nil(render_function) -> render_function.(value)
      true -> value
    end
  end

  # Handle for case fields = [{[:a], []}]
  def render_value(item, {[h | []] = fields, opts}) when is_list(fields) do
    render_value(item, {h, opts})
  end

  def render_value(item, {[h | t] = fields, opts}) when is_list(fields) do
    render_value(Map.get(item, h), {t, opts})
  end

  def render_value(item, _) do
    ""
  end

  def render_title(field) when is_atom(field) do
    render_title({field, []})
  end

  def render_title(fields) when is_list(fields) do
    render_title({fields, []})
  end

  def render_title({field, opts}) when is_atom(field) do
    if opts[:label] do
      opts[:label]
    else
      field |> Atom.to_string() |> Phoenix.Naming.humanize()
    end
  end

  def render_title({fields, opts}) when is_list(fields) do
    opts[:label] ||
      fields
      |> Enum.map(&Phoenix.Naming.humanize/1)
      |> Enum.join(" ")
  end

  def render_title(_) do
    ""
  end

  def render_img(src) do
    src
    |> Phoenix.HTML.Tag.img_tag(
      class: "img-thumbnail",
      style: "max-height: 50px; max-width: 100px;"
    )
    |> Phoenix.HTML.raw()
  end

  defp append_buitin_fields(fields) do
    fields ++ [:id, :inserted_at, :updated_at]
  end

  def get_fields(%{fields: fields} = assigns, default_fields) when is_binary(fields) do
    fields
    |> String.split(",", trim: true)
    |> Enum.map(&String.to_atom/1)
    |> append_buitin_fields()
  end

  def get_fields(%{fields: fields} = assigns, default_fields) when is_nil(fields) do
    default_fields |> append_buitin_fields()
  end

  def get_fields(%{fields: field} = assigns, default_fields) when is_atom(field) do
    [field] |> append_buitin_fields()
  end

  def get_fields(%{fields: fields} = assigns, default_fields) when is_list(fields) do
    fields |> append_buitin_fields()
  end

  def get_fields(assigns, default_fields) do
    default_fields |> append_buitin_fields()
  end

  def render_view(conn, module, template) do
    assigns = Map.put(conn.assigns, :fields, conn.params["fields"])
    {:ok, Phoenix.View.render(module, template, assigns)}
  end
end
