defmodule MokaWeb.MediaView do
  use MokaWeb, :view

  alias MokaWeb.MediaView

  def render("list_media.json", %{media: media}) do
    %{data: render_many(media, MediaView, "media.json")}
  end

  def render("show_media.json", %{media: media}) do
    %{data: render_one(media, MediaView, "media.json")}
  end

  def render("media.json", %{media: media}) do
    %{
      url: media.path.url
    }
  end
end
