defmodule MokaWeb.PageController do
  use MokaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def sign_in(conn, params) do
    conn
    |> put_layout({MokaWeb.LayoutView, :unauth})
    |> render("sign_in.html")
  end
end
