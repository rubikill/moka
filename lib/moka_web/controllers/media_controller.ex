defmodule MokaWeb.MediaController do
  use MokaWeb, :controller
  alias Moka.Uploader.Media

  def create(conn, params) do
    changeset = Media.changeset(%Media{}, params)
    with {:ok, data} <- Moka.Ecto.changeset_valid?(changeset) do
      {:ok, media} = Moka.Uploader.create_media(params)

      conn
      |> put_status(:ok)
      |> render("show_media.json", media: media)
    end
  end
end
