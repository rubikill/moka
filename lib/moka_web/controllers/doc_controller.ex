defmodule MokaWeb.DocController do
  use MokaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def forms(conn, _params) do
    render(conn, "forms.html")
  end

  def detail(conn, _params) do
    conn
    |> assign(:item, %Moka.Form{
      id: 1,
      email: "Name 1",
      name: "Name 1",
      avatar: %{
        url:
          "https://68.media.tumblr.com/7d65a925636d6e3df94e2ebe30667c29/tumblr_nq1zg0MEn51qg6rkio1_500.jpg"
      }
    })
    |> assign(:fields, [
      :id,
      :email,
      :name,
      {[:avatar, :url], render_function: fn i -> MokaWeb.ViewHelper.render_img(i) end}
    ])
    |> assign(:path, fn a, b, c -> "#" end)
    |> put_view(MokaWeb.PageView)
    |> render("show.html")
  end

  def listing_with_paging(conn, _params) do
    entries =
      Enum.map(1..10, fn i ->
        %Moka.Form{
          id: i,
          email: "abc#{i}@gmail.com",
          name:
            "Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai Name dai oi la dai #{
              i
            }",
          avatar: %{
            url:
              "https://68.media.tumblr.com/7d65a925636d6e3df94e2ebe30667c29/tumblr_nq1zg0MEn51qg6rkio1_500.jpg"
          }
        }
      end)

    conn
    |> assign(:title, "Title")
    |> assign(:fields, [
      {:name, render_function: fn name -> MokaWeb.ViewHelper.ellipsis(name, 100) end},
      :email,
      {
        [:avatar, :url],
        render_function_full: fn {i, url} ->
          url
          |> Phoenix.HTML.Link.link(to: url)
          |> Phoenix.HTML.raw()
        end
      }
    ])
    |> assign(:path, fn a, b, c -> "#" end)
    |> assign(:entries, entries)
    |> assign(:pagination, %{
      entries: entries,
      size: 10,
      total: 100,
      page: 1,
      total_pages: 1
    })
    |> assign(:filter_by, [
      %{
        name: "status",
        using: :select,
        options: ["a", "b", "c"]
      }
    ])
    |> assign(:extra_actions, [
      [
        label: "Ahihi",
        action: fn i -> "#" end
      ],
      [
        label: fn i ->
          IO.inspect i
          "Ahuhu"
        end,
        action: fn i -> "#" end
      ],
    ])
    |> put_view(MokaWeb.PageView)
    |> render("listing.html")
  end
end
