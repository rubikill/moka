defmodule MokaWeb.Router do
  use MokaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MokaWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/sign_in", PageController, :sign_in

    scope "/doc" do
      get "/", DocController, :index
      get "/forms", DocController, :forms
      post "/forms", DocController, :forms
      get "/listing_with_paging", DocController, :listing_with_paging
      get "/detail", DocController, :detail
    end
  end

  scope "/api", MokaWeb do
    pipe_through :api

    scope "/admin" do
      post "/upload_photo", MediaController, :create
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", MokaWeb do
  #   pipe_through :api
  # end
end
