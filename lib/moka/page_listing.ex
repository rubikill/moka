defmodule Moka.PageListing do
  @moduledoc """
  Moka.PageListing
  """

  defstruct conn: %Plug.Conn{},
            entries: [],
            pagination: %Moka.Paginator{},
            loadmore: %Moka.Loadmore{},
            path: &__MODULE__.f/3,
            fields: [],
            title: "Title",
            filter_by: []

  def f(a, b, c) do
    "#"
  end
end
