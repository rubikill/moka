defmodule Moka.Ecto do
  @moduledoc """
  Moka.Ecto
  """
  import Ecto.Changeset
  require Moka.Gettext
  require Logger

  defmacro __using__(opts) do
    gettext_module = Keyword.get(opts, :gettext_module)

    quote do
      import unquote(__MODULE__)
      import unquote(Moka.Ecto.__get_module__(gettext_module, Moka.Gettext))
    end
  end

  def __get_module__(module, replaced_module) do
    if module do
      module
    else
      replaced_module
    end
  end

  defmacro def_enum(prefix, values) do
    quote bind_quoted: [prefix: prefix, values: values] do
      def unquote(:"#{prefix}_enum")(), do: unquote(values)

      Enum.map(values, fn value ->
        def unquote(:"#{prefix}_#{value}")() do
          gettext(unquote(value))
          unquote(value)
        end
      end)
    end
  end

  def require_one(changeset, keys) do
    invalid =
      Enum.reduce(keys, true, fn key, acc ->
        value = get_field(changeset, key)
        value in [nil, "", [], %{}] and acc
      end)

    if invalid do
      Enum.reduce(keys, changeset, fn key, acc ->
        add_error(acc, key, "At least one of these keys #{inspect(keys)} must be set")
      end)
    else
      changeset
    end
  end

  def require_not_or_both(changeset, key1, key2) do
    value1 = get_field(changeset, key1)
    value2 = get_field(changeset, key2)

    if (is_nil(value1) and is_nil(value2)) or (not is_nil(value1) and not is_nil(value2)) do
      changeset
    else
      add_error(changeset, key1, "this combination of #{key1} and #{key2} is not allowed")
      |> add_error(key2, "this combination of #{key1} and #{key2} is not allowed")
    end
  end

  def cast_date(changeset, params, field, format \\ "{YYYY}/{0M}/{0D}", opts \\ [])

  def cast_date(changeset, params, field, format, opts) do
    _parse_date(changeset, params, field, format, opts)
  end

  def cast_datetime(
        changeset,
        params,
        field,
        format \\ "{YYYY}-{0M}-{0D}T{h24}:{m}:{s}{Z}",
        opts \\ []
      )

  def cast_datetime(changeset, params, field, format, opts) do
    _parse_date(changeset, params, field, format, opts)
  end

  defp _parse_date(changeset, params, fields, format, opts) when is_list(fields) do
    Enum.reduce(fields, changeset, &_parse_date(&2, params, &1, format, opts))
  end

  defp _parse_date(changeset, params, field, format, _opts) when is_atom(field) do
    value = Map.get(params, field) || Map.get(params, to_string(field))

    if value in [nil, ""] do
      changeset
    else
      case Timex.parse(value, format) do
        {:ok, parsed_value} ->
          dt = Timex.to_naive_datetime(parsed_value) |> DateTime.from_naive!("Etc/UTC")
          Ecto.Changeset.put_change(changeset, field, dt)

        {:error, message} ->
          Ecto.Changeset.add_error(changeset, field, "Invalid format")
      end
    end
  end

  def clean_upload(changeset, field, uploader) do
    change = get_change(changeset, field)
    value = Map.get(changeset.data, field)

    cond do
      changeset.valid? and not is_nil(change) and not is_nil(value) ->
        apply(uploader, :delete, [value.identifier])

      not changeset.valid? and is_map(change) ->
        apply(uploader, :delete, [change.identifier])

      true ->
        nil
    end

    changeset
  end

  def changeset_valid?(changeset) do
    if changeset.valid? do
      {:ok, Ecto.Changeset.apply_changes(changeset)}
    else
      {:error, :validation_fail, changeset}
    end
  end
end
