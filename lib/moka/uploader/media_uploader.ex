defmodule Moka.Uploader.MediaUploader do
  @moduledoc """
  Moka.Loadmore
  """

  @default_url "https://68.media.tumblr.com/7d65a925636d6e3df94e2ebe30667c29/tumblr_nq1zg0MEn51qg6rkio1_500.jpg"

  use Moka.Uploader.Type,
    uploader: Moka.Uploader.Base,
    base_dir: "media",
    default_url: @default_url

  def get_default_url do
    @default_url
  end
end
