defmodule Moka.Uploader.Type do
  @moduledoc """
  this is prototype for an uploader style.
  everything has been set up already.

  Usage:
      use Mads.UploaderType, opts

  - Options
    - :uploader [required]: Uploader that do store file, you can modify above uploader as your need
    - :base_dir [optional]: base directory you want to store file on the storage. You can change or build directory inside the uploader

  You should define a new type only if you want to store file under a specific directory on your storage.
  """
  defmacro __using__(opts) do
    {uploader, opts} = Keyword.pop(opts, :uploader)
    base = Keyword.get(opts, :base_dir)
    default_url = Keyword.get(opts, :default_url)

    quote do
      @behaviour Ecto.Type
      def type, do: :string

      def cast(%{identifier: _} = file) do
        {:ok, file}
      end

      def cast(file) do
        with true <- unquote(uploader).validate(file),
             {:ok, %{identifier: identifier}} <-
               unquote(uploader).store(file, base_dir: unquote(base)) do
          url =
            case unquote(uploader).url(identifier) do
              {:ok, url} -> url
              _ -> :unavailable
            end

          {:ok,
           %{
             identifier: identifier,
             url: url
           }}
        else
          _ -> :error
        end
      end

      def load(identifier) do
        url =
          case unquote(uploader).url(identifier) do
            {:ok, url} -> url || unquote(default_url)
            _ -> unquote(default_url)
          end

        {:ok,
         %{
           identifier: identifier,
           url: url
         }}
      end

      def dump(%{identifier: identifier}), do: {:ok, identifier}

      def dump(_), do: :error

      def default do
        %{identifier: "", url: unquote(default_url)}
      end

      def embed_as(_), do: :self
      def equal?(term1, term2), do: term1 == term2
    end
  end
end
