defmodule Moka.Uploader.Media do
  @moduledoc """
  Moka.Uploader.Media
  """

  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :common
  schema "media" do
    field :path, Moka.Uploader.MediaUploader
    field :filename, :string
    field :content_type, :string

    timestamps()
  end

  @doc false
  def changeset(media, attrs) do
    media
    |> cast(attrs, [:path])
    |> fetch_media_info(attrs)
    |> validate_required([:path])
  end

  defp fetch_media_info(changeset, attrs) do
    case attrs["path"] do
      %Plug.Upload{content_type: content_type, filename: filename} ->
        changeset
        |> put_change(:content_type, content_type)
        |> put_change(:filename, filename)

      _ ->
        changeset
    end
  end
end
