defmodule Moka.PageNew do
  @moduledoc """
  Moka.PageNew
  """

  defstruct name: "John", age: 27
end
