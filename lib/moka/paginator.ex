defmodule Moka.Paginator do
  @moduledoc """
  Moka.Paginator
  """

  import Ecto.Query
  import Ecto.Changeset

  @default_values %{page: 1, size: 20}
  def default_values, do: @default_values

  @default_types %{
    page: :integer,
    size: :integer
  }
  def default_types, do: @default_types

  defstruct Map.to_list(@default_values)

  def __changeset__, do: @default_types

  def validate(changeset) do
    changeset
    |> validate_number(:page, greater_than_or_equal_to: 1)
    |> validate_number(:size, less_than_or_equal_to: 1000)
    |> validate_number(:size, greater_than_or_equal_to: 1)
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, Map.keys(@default_values))
    |> validate()
  end

  def cast(params \\ %{}) do
    changeset(%__MODULE__{}, params)
    |> validate()
  end

  def new(query, repo, params) do
    changesetz = changeset(%__MODULE__{}, params)

    if changesetz.valid?() do
      data = apply_changes(changesetz)
      size = data.size || 20

      total_entries =
        query
        |> exclude(:group_by)
        |> exclude(:order_by)
        |> exclude(:preload)
        |> exclude(:select)
        |> select([i], count(i.id))
        |> limit(1)
        |> repo.one()

      total_pages = Float.ceil(total_entries / size) |> round()

      offset = size * (data.page - 1)

      entries =
        query
        |> limit(^size)
        |> offset(^offset)
        |> repo.all()

      %{
        entries: entries,
        page: data.page,
        size: size,
        total_entries: total_entries,
        total_pages: total_pages
      }
    else
      {:error, :validation_failed, changesetz}
    end
  end
end
