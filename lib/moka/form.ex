defmodule Moka.Form do
  @moduledoc """
  Moka.Form
  """

  use Ecto.Schema
  import Ecto.Changeset

  @type t :: Ecto.Schema.t()

  @fields ~w|id name age email accepts_conditions gender birthday avatar bio|a
  @primary_key false

  embedded_schema do
    field :id, :integer
    field :name, :string
    field :age, :integer
    field :email, :string
    field :accepts_conditions, :boolean
    field :gender, :string
    field :birthday, :utc_datetime
    field :avatar, :string
    field :bio, :string
  end

  @doc false
  def changeset(model, attrs \\ %{})

  def changeset(model, attrs) do
    model
    |> cast(attrs, @fields)
    |> validate_required(~w|name email accepts_conditions|a)
  end

  def fields do
    [
      {:name, :string, required: true},
      {:age, :integer, required: true},
      {:email, :string, required: true},
      {:accepts_conditions, :boolean, required: true},
      {:gender, :string, required: true, using: :select, options: ["male", "female"]},
      {:birthday, :utc_datetime, required: true, using: :datetimepicker},
      {:avatar, :string, required: false, using: :image_input},
      {:bio, :string,
       required: false, using: :text_editor, input_opts: ["data-url": "/api/admin/upload_photo"]}
    ]
  end
end
