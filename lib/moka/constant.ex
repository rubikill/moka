defmodule Moka.Constant do
  @moduledoc """
  Moka.Constant
  """

  @email_regex ~r/[a-zA-Z0-9\-_.+]+@[a-zA-Z0-9_\-.]+\..+/
  def email_regex, do: @email_regex
  @phone_regex ~r/\b\d{3}[-.]?\d{3}[-.]?\d{4,5}\b/
  def phone_regex, do: @phone_regex
  @default_date_format "{YYYY}-{0M}-{0D}"
  def default_date_format, do: @default_date_format
  @default_datetime_format "{YYYY}-{0M}-{0D}T{h24}:{m}:{s}Z"
  def default_datetime_format, do: @default_datetime_format
end
