defmodule Moka.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      Moka.Repo
    ]

    children =
      if Application.get_env(:moka, :endpoint) do
        [MokaWeb.Endpoint | children]
      else
        children
      end

    # children = [
    #   # Start the Ecto repository
    #   # Moka.Repo,
    #   # Start the endpoint when the application starts
    #   MokaWeb.Endpoint
    #   # Starts a worker by calling: Moka.Worker.start_link(arg)
    #   # {Moka.Worker, arg},
    # ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Moka.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MokaWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
