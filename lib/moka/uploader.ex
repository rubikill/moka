defmodule Moka.Uploader do
  @moduledoc """
  Moka keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  alias Moka.Repo
  alias Moka.Uploader.Media

  def create_media(attr \\ %{}) do
    %Media{}
    |> Media.changeset(attr)
    |> Repo.insert()
  end
end
