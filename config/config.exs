# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :moka,
  ecto_repos: [Moka.Repo]

config :moka,
  endpoint: true

# Configures the endpoint
config :moka, MokaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/JiUr3hzCqeGnLrcL+W3W4LhSKWEdE1nNAVnK3hIi/iKb21HUbBaDwACwxpsBYSO",
  render_errors: [view: MokaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Moka.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :belt, Belt.Provider.S3,
  default: [
    bucket: System.get_env("AWS_S3_BUCKET"),
    virtual_host: true,
    host: "s3.#{System.get_env("AWS_REGION")}.amazonaws.com",
    access_key_id: System.get_env("AWS_ACCESS_KEY_ID"),
    secret_access_key: System.get_env("AWS_SECRET_ACCESS_KEY"),
    region: System.get_env("AWS_REGION")
  ],
  storage_dir: System.get_env("AWS_S3_STORAGE_DIR")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
